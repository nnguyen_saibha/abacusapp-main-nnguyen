package gov.ofda.abacus.model.oim;

import java.util.Hashtable;

import java.util.Map;

import javax.security.auth.login.LoginException;

import oracle.adf.share.logging.ADFLogger;


import oracle.iam.platform.OIMClient;

import oracle.security.jps.JpsException;
import oracle.security.jps.service.JpsServiceLocator;
import oracle.security.jps.service.ServiceLocator;
import oracle.security.jps.service.credstore.CredStoreException;
import oracle.security.jps.service.credstore.CredentialExpiredException;
import oracle.security.jps.service.credstore.CredentialStore;
import oracle.security.jps.service.credstore.GenericCredential;
import oracle.security.jps.service.credstore.PasswordCredential;

public class OIMMyClient {
        protected OIMClient _oimClient=null;
        private static ADFLogger logger = ADFLogger.createADFLogger(OIMMyClient.class);
    /*
     * http://www.iamidm.com/2013/02/oim-11g-r2-api-usage-java-code-to.html
     */
    public OIMMyClient() {
        Hashtable<Object, Object> env = new Hashtable<Object, Object>();
            ServiceLocator locator = JpsServiceLocator.getServiceLocator();
            CredentialStore store = null;
            PasswordCredential pcred = null;
            GenericCredential gcred=null;
            try {
                store = locator.lookup(CredentialStore.class);
                pcred = (PasswordCredential)store.getCredential("OIM", "Password");
                gcred=(GenericCredential)store.getCredential("OIM", "URL");
                
            } catch (CredentialExpiredException e) {
                logger.log(e.toString());
            } catch (CredStoreException e) {
                logger.log(e.toString());
            } catch (JpsException e) {
                logger.log(e.toString());
            }
            Map<String,char[]> t=(Map<String,char[]>)gcred.getCredential();
            char tc[]=t.get("providerURL");
            String url="";
            for(char c: tc)
                url+=c;
        env.put(oracle.iam.platform.OIMClient.JAVA_NAMING_FACTORY_INITIAL, "weblogic.jndi.WLInitialContextFactory");
        env.put(oracle.iam.platform.OIMClient.JAVA_NAMING_PROVIDER_URL, url);        //Update localhost with your OIM machine IP
        System.setProperty("java.security.auth.login.config", "/u01/app/config/authwl.conf");   //Update path of authwl.conf file according to your environment
       //System.setProperty("java.security.auth.login.config", "C:\\JDeveloper\\jars\\oimclient\\conf\\authwl.conf");   //Update path of authwl.conf file according to your environment
        System.setProperty("OIM.AppServerType", "wls");  
        System.setProperty("APPSERVER_TYPE", "wls");
        _oimClient = new OIMClient(env);
         try {                        
               _oimClient.login(pcred.getName(),pcred.getPassword());         //Update password of Admin with your environment password
             } catch (LoginException e) {
                logger.log(logger.ERROR, "Error connecting to OIM"+e);
            }            
          
        }


    }

           

