package gov.ofda.abacus.model;

import java.io.Serializable;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.binding.OperationBinding;
import javax.faces.application.NavigationHandler;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.nav.RichButton;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class SectorDetailsEventBean implements Serializable{
    @SuppressWarnings("compatibility:-3255816036069061132")
    private static final long serialVersionUID = 1L;

    public SectorDetailsEventBean() {
        super();
    }
    public void publishRollbackChangesEvent() {
        
    }
    public void handleRollbackChangesEvent() {
        RichButton rbutton = (RichButton) resolveExpression("#{viewScope.manageSectorBean.rollbackChangesButton}");
            FacesContext context=FacesContext.getCurrentInstance();
            ExtendedRenderKitService service =Service.getRenderKitService(context, ExtendedRenderKitService.class);
            service.addScript(context, "var link = AdfPage.PAGE.findComponentByAbsoluteId('"+rbutton.getClientId(context)+"'); AdfActionEvent.queue(link,true);");
        }
     public static Object resolveExpression(String expression) {
         FacesContext facesContext = FacesContext.getCurrentInstance();
         Application app = facesContext.getApplication();
         ExpressionFactory elFactory = app.getExpressionFactory();
         ELContext elContext = facesContext.getELContext();
         ValueExpression valueExp = 
             elFactory.createValueExpression(elContext, expression, 
                                             Object.class);
         return valueExp.getValue(elContext);
     }
     public void publishSubSectorChangesEvent() {
     }
     public void handleSubSectorChangesEvent() {
         DCBindingContainer bindingContainer =
             (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
         OperationBinding ob = null;
         ob = bindingContainer.getOperationBinding("ExecutePasSubsectorInterventionValid");
         ob.execute();
         ob = bindingContainer.getOperationBinding("ExecutePassInterventions");
         ob.execute();
         ob = bindingContainer.getOperationBinding("ExecuteCommodity");
         ob.execute();
         ob = bindingContainer.getOperationBinding("ExecuteModality");
         ob.execute();
     }     
 }

