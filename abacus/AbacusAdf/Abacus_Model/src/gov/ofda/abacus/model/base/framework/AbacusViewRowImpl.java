package gov.ofda.abacus.model.base.framework;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import oracle.adf.share.ADFContext;

import oracle.jbo.JboException;
import oracle.jbo.server.ViewRowImpl;

public class AbacusViewRowImpl extends ViewRowImpl {
    private List userRoles=null;
    public AbacusViewRowImpl() {
        super();
    }

    public void setUserRoles(List userRoles) {
        this.userRoles = userRoles;
    }

    public List getUserRoles() {
        if(userRoles==null) {
            userRoles=new ArrayList();
            PreparedStatement st = null;
            ResultSet rs=null;
            try {
                st = getDBTransaction().createPreparedStatement("select abacus_role from abacus_user_details where userid=?", 0);
                st.setString(1,ADFContext.getCurrent().getSecurityContext().getUserName().toUpperCase());
                rs=st.executeQuery();
                while(rs.next()) {
                    userRoles.add(rs.getString(1));
                }
                rs.close();
                st.close();
            } catch (SQLException e) {
                throw new JboException(e);
            }
        }
        return userRoles;
    }
}
