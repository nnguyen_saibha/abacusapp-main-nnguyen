package gov.ofda.abacus.model;

import java.io.Serializable;


public class NewPAS implements Serializable{
    @SuppressWarnings("compatibility:-3276046406061523148")
    private static final long serialVersionUID = 1L;
    private String sectorCode;
    private String assistanceTypeCode;
    public NewPAS(String sectorCode) {
        super();
        this.sectorCode=sectorCode;
    }

    public void setSectorCode(String sectorCode) {
        this.sectorCode = sectorCode;
    }

    public String getSectorCode() {
        return sectorCode;
    }

    public void setAssistanceTypeCode(String assistanceTypeCode) {
        this.assistanceTypeCode = assistanceTypeCode;
    }

    public String getAssistanceTypeCode() {
        return assistanceTypeCode;
    }
}

