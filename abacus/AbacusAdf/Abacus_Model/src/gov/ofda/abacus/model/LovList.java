package gov.ofda.abacus.model;

import java.io.Serializable;

public class LovList implements Serializable{
    @SuppressWarnings("compatibility:621277086557163855")
    private static final long serialVersionUID = 1L;
    private Long code;
    private String name;
    private Integer level;
    private String region;
    private String country;
    private String adminLevel1;
    private String adminLevel2;
    private String adminLevel3;

    public LovList(Long code,String name,Integer level) {
        super();
        this.code=code;
        this.name=name;
        this.level=level;
    }
    public LovList(Long code,String name,Integer level,String region,String country,String adminLevel1,String adminLevel2,String adminLevel3) {
        super();
        this.code=code;
        this.name=name;
        this.level=level;
        this.region=region;
        this.country=country;
        this.adminLevel1=adminLevel1;
        this.adminLevel2=adminLevel2;
        this.adminLevel3=adminLevel3;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Long getCode() {
        return code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean equals(Object obj) {
      if (obj instanceof LovList)
          if(((LovList)obj).getCode()==this.getCode())
              return true;
      return false;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getLevel() {
        return level;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setAdminLevel1(String adminLevel1) {
        this.adminLevel1 = adminLevel1;
    }

    public String getAdminLevel1() {
        return adminLevel1;
    }

    public void setAdminLevel2(String adminLevel2) {
        this.adminLevel2 = adminLevel2;
    }

    public String getAdminLevel2() {
        return adminLevel2;
    }

    public void setAdminLevel3(String adminLevel3) {
        this.adminLevel3 = adminLevel3;
    }

    public String getAdminLevel3() {
        return adminLevel3;
    }
}
