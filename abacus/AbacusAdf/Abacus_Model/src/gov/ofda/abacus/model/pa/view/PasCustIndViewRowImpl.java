package gov.ofda.abacus.model.pa.view;

import gov.ofda.abacus.model.pa.entity.PasCustIndImpl;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Feb 21 09:41:05 EST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PasCustIndViewRowImpl extends ViewRowImpl {


    public static final int ENTITY_PASCUSTIND = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        PasCustIndId {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getPasCustIndId();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setPasCustIndId((DBSequence) value);
            }
        }
        ,
        PasSubsectorId {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getPasSubsectorId();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setPasSubsectorId((BigDecimal) value);
            }
        }
        ,
        IndicatorName {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getIndicatorName();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setIndicatorName((String) value);
            }
        }
        ,
        UomValue1 {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getUomValue1();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setUomValue1((BigDecimal) value);
            }
        }
        ,
        UomValue2 {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getUomValue2();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setUomValue2((BigDecimal) value);
            }
        }
        ,
        IndTypeLevel {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getIndTypeLevel();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setIndTypeLevel((String) value);
            }
        }
        ,
        Comments {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getComments();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setComments((String) value);
            }
        }
        ,
        Filler1 {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getFiller1();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setFiller1((String) value);
            }
        }
        ,
        Filler2 {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getFiller2();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setFiller2((String) value);
            }
        }
        ,
        Filler3 {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getFiller3();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setFiller3((String) value);
            }
        }
        ,
        UpdId {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getUpdId();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setUpdId((String) value);
            }
        }
        ,
        UpdDate {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getUpdDate();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        CreatedId {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getCreatedId();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setCreatedId((String) value);
            }
        }
        ,
        CreatedDate {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getCreatedDate();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        IsEditable {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getIsEditable();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        IndicatorType {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getIndicatorType();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setIndicatorType((String) value);
            }
        }
        ,
        PasSubsectorView {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getPasSubsectorView();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        CustIndDisagTypeLOV1 {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getCustIndDisagTypeLOV1();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        CommonLookupLOV1 {
            public Object get(PasCustIndViewRowImpl obj) {
                return obj.getCommonLookupLOV1();
            }

            public void put(PasCustIndViewRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public abstract Object get(PasCustIndViewRowImpl object);

        public abstract void put(PasCustIndViewRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public boolean isAttributeUpdateable(int i) {
        if (!this.getIsEditable())
            return false;
        else
            return super.isAttributeUpdateable(i);
    }


    public static final int PASCUSTINDID = AttributesEnum.PasCustIndId.index();
    public static final int PASSUBSECTORID = AttributesEnum.PasSubsectorId.index();
    public static final int INDICATORNAME = AttributesEnum.IndicatorName.index();
    public static final int UOMVALUE1 = AttributesEnum.UomValue1.index();
    public static final int UOMVALUE2 = AttributesEnum.UomValue2.index();
    public static final int INDTYPELEVEL = AttributesEnum.IndTypeLevel.index();
    public static final int COMMENTS = AttributesEnum.Comments.index();
    public static final int FILLER1 = AttributesEnum.Filler1.index();
    public static final int FILLER2 = AttributesEnum.Filler2.index();
    public static final int FILLER3 = AttributesEnum.Filler3.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int CREATEDID = AttributesEnum.CreatedId.index();
    public static final int CREATEDDATE = AttributesEnum.CreatedDate.index();
    public static final int ISEDITABLE = AttributesEnum.IsEditable.index();
    public static final int INDICATORTYPE = AttributesEnum.IndicatorType.index();
    public static final int PASSUBSECTORVIEW = AttributesEnum.PasSubsectorView.index();
    public static final int CUSTINDDISAGTYPELOV1 = AttributesEnum.CustIndDisagTypeLOV1.index();
    public static final int COMMONLOOKUPLOV1 = AttributesEnum.CommonLookupLOV1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PasCustIndViewRowImpl() {
    }

    /**
     * Gets PasCustInd entity object.
     * @return the PasCustInd
     */
    public PasCustIndImpl getPasCustInd() {
        return (PasCustIndImpl) getEntity(ENTITY_PASCUSTIND);
    }

    /**
     * Gets the attribute value for PAS_CUST_IND_ID using the alias name PasCustIndId.
     * @return the PAS_CUST_IND_ID
     */
    public DBSequence getPasCustIndId() {
        return (DBSequence) getAttributeInternal(PASCUSTINDID);
    }

    /**
     * Sets <code>value</code> as attribute value for PAS_CUST_IND_ID using the alias name PasCustIndId.
     * @param value value to set the PAS_CUST_IND_ID
     */
    public void setPasCustIndId(DBSequence value) {
        setAttributeInternal(PASCUSTINDID, value);
    }

    /**
     * Gets the attribute value for PAS_SUBSECTOR_ID using the alias name PasSubsectorId.
     * @return the PAS_SUBSECTOR_ID
     */
    public BigDecimal getPasSubsectorId() {
        return (BigDecimal) getAttributeInternal(PASSUBSECTORID);
    }

    /**
     * Sets <code>value</code> as attribute value for PAS_SUBSECTOR_ID using the alias name PasSubsectorId.
     * @param value value to set the PAS_SUBSECTOR_ID
     */
    public void setPasSubsectorId(BigDecimal value) {
        setAttributeInternal(PASSUBSECTORID, value);
    }

    /**
     * Gets the attribute value for INDICATOR_NAME using the alias name IndicatorName.
     * @return the INDICATOR_NAME
     */
    public String getIndicatorName() {
        return (String) getAttributeInternal(INDICATORNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for INDICATOR_NAME using the alias name IndicatorName.
     * @param value value to set the INDICATOR_NAME
     */
    public void setIndicatorName(String value) {
        setAttributeInternal(INDICATORNAME, value);
    }

    /**
     * Gets the attribute value for UOM_VALUE1 using the alias name UomValue1.
     * @return the UOM_VALUE1
     */
    public BigDecimal getUomValue1() {
        return (BigDecimal) getAttributeInternal(UOMVALUE1);
    }

    /**
     * Sets <code>value</code> as attribute value for UOM_VALUE1 using the alias name UomValue1.
     * @param value value to set the UOM_VALUE1
     */
    public void setUomValue1(BigDecimal value) {
        setAttributeInternal(UOMVALUE1, value);
    }

    /**
     * Gets the attribute value for UOM_VALUE2 using the alias name UomValue2.
     * @return the UOM_VALUE2
     */
    public BigDecimal getUomValue2() {
        return (BigDecimal) getAttributeInternal(UOMVALUE2);
    }

    /**
     * Sets <code>value</code> as attribute value for UOM_VALUE2 using the alias name UomValue2.
     * @param value value to set the UOM_VALUE2
     */
    public void setUomValue2(BigDecimal value) {
        setAttributeInternal(UOMVALUE2, value);
    }

    /**
     * Gets the attribute value for IND_TYPE_LEVEL using the alias name IndTypeLevel.
     * @return the IND_TYPE_LEVEL
     */
    public String getIndTypeLevel() {
        return (String) getAttributeInternal(INDTYPELEVEL);
    }

    /**
     * Sets <code>value</code> as attribute value for IND_TYPE_LEVEL using the alias name IndTypeLevel.
     * @param value value to set the IND_TYPE_LEVEL
     */
    public void setIndTypeLevel(String value) {
        setAttributeInternal(INDTYPELEVEL, value);
    }

    /**
     * Gets the attribute value for COMMENTS using the alias name Comments.
     * @return the COMMENTS
     */
    public String getComments() {
        return (String) getAttributeInternal(COMMENTS);
    }

    /**
     * Sets <code>value</code> as attribute value for COMMENTS using the alias name Comments.
     * @param value value to set the COMMENTS
     */
    public void setComments(String value) {
        setAttributeInternal(COMMENTS, value);
    }

    /**
     * Gets the attribute value for FILLER1 using the alias name Filler1.
     * @return the FILLER1
     */
    public String getFiller1() {
        return (String) getAttributeInternal(FILLER1);
    }

    /**
     * Sets <code>value</code> as attribute value for FILLER1 using the alias name Filler1.
     * @param value value to set the FILLER1
     */
    public void setFiller1(String value) {
        setAttributeInternal(FILLER1, value);
    }

    /**
     * Gets the attribute value for FILLER2 using the alias name Filler2.
     * @return the FILLER2
     */
    public String getFiller2() {
        return (String) getAttributeInternal(FILLER2);
    }

    /**
     * Sets <code>value</code> as attribute value for FILLER2 using the alias name Filler2.
     * @param value value to set the FILLER2
     */
    public void setFiller2(String value) {
        setAttributeInternal(FILLER2, value);
    }

    /**
     * Gets the attribute value for FILLER3 using the alias name Filler3.
     * @return the FILLER3
     */
    public String getFiller3() {
        return (String) getAttributeInternal(FILLER3);
    }

    /**
     * Sets <code>value</code> as attribute value for FILLER3 using the alias name Filler3.
     * @param value value to set the FILLER3
     */
    public void setFiller3(String value) {
        setAttributeInternal(FILLER3, value);
    }

    /**
     * Gets the attribute value for UPD_ID using the alias name UpdId.
     * @return the UPD_ID
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Sets <code>value</code> as attribute value for UPD_ID using the alias name UpdId.
     * @param value value to set the UPD_ID
     */
    public void setUpdId(String value) {
        setAttributeInternal(UPDID, value);
    }

    /**
     * Gets the attribute value for UPD_DATE using the alias name UpdDate.
     * @return the UPD_DATE
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }

    /**
     * Gets the attribute value for CREATED_ID using the alias name CreatedId.
     * @return the CREATED_ID
     */
    public String getCreatedId() {
        return (String) getAttributeInternal(CREATEDID);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_ID using the alias name CreatedId.
     * @param value value to set the CREATED_ID
     */
    public void setCreatedId(String value) {
        setAttributeInternal(CREATEDID, value);
    }

    /**
     * Gets the attribute value for CREATED_DATE using the alias name CreatedDate.
     * @return the CREATED_DATE
     */
    public Timestamp getCreatedDate() {
        return (Timestamp) getAttributeInternal(CREATEDDATE);
    }

    /**
     * Gets the attribute value for the calculated attribute IsEditable.
     * @return the IsEditable
     */
    public Boolean getIsEditable() {
        Row r = this.getPasSubsectorView();
        if (r != null)
            return (Boolean)r.getAttribute("IsEditable");
        else
            return true;
    }

    /**
     * Gets the attribute value for INDICATOR_TYPE using the alias name IndicatorType.
     * @return the INDICATOR_TYPE
     */
    public String getIndicatorType() {
        return (String) getAttributeInternal(INDICATORTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for INDICATOR_TYPE using the alias name IndicatorType.
     * @param value value to set the INDICATOR_TYPE
     */
    public void setIndicatorType(String value) {
        setAttributeInternal(INDICATORTYPE, value);
    }

    /**
     * Gets the associated <code>Row</code> using master-detail link PasSubsectorView.
     */
    public Row getPasSubsectorView() {
        return (Row) getAttributeInternal(PASSUBSECTORVIEW);
    }

    /**
     * Sets the master-detail link PasSubsectorView between this object and <code>value</code>.
     */
    public void setPasSubsectorView(Row value) {
        setAttributeInternal(PASSUBSECTORVIEW, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> CustIndDisagTypeLOV1.
     */
    public RowSet getCustIndDisagTypeLOV1() {
        return (RowSet) getAttributeInternal(CUSTINDDISAGTYPELOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> CommonLookupLOV1.
     */
    public RowSet getCommonLookupLOV1() {
        return (RowSet) getAttributeInternal(COMMONLOOKUPLOV1);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

