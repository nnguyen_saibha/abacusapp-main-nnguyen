/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *      Portions Copyright 2011 Oracle America, Inc.
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at
 * trunk/openptk/resource/legal-notices/OpenPTK.LICENSE
 * or https://openptk.dev.java.net/OpenPTK.LICENSE.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the reference to
 * trunk/openptk/resource/legal-notices/OpenPTK.LICENSE. If applicable,
 * add the following below this CDDL HEADER, with the fields enclosed
 * by brackets "[]" replaced with your own identifying information:
 *      Portions Copyright [yyyy] [name of copyright owner]
 *
 */

package gov.ofda.abacus.model.oim;

import gov.ofda.abacus.model.ResultMessage;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import oracle.adf.share.logging.ADFLogger;

import oracle.iam.identity.exception.RoleGrantException;
import oracle.iam.identity.exception.RoleGrantRevokeException;
import oracle.iam.identity.exception.RoleSearchException;
import oracle.iam.identity.exception.UserMembershipException;
import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.rolemgmt.api.RoleManager;
import oracle.iam.identity.rolemgmt.api.RoleManagerConstants;
import oracle.iam.identity.rolemgmt.vo.Role;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;

public class OIMRoleManager extends OIMMyClient{
    RoleManager rmgr = null;
    private static ADFLogger logger = ADFLogger.createADFLogger(OIMRoleManager.class);
    public OIMRoleManager() {
        super();
        
    }
    public String getRoleKey(String roleName)
     //----------------------------------------------------------------
     {
        String roleKey = null;
        RoleManager rmgr = null;
        List<Role> roles = null;
        SearchCriteria criteria = null;
        /*
         * Use the "roleName" to obtain the internal "roleKey"
         */
        rmgr = this.getRoleManager();
        criteria = new SearchCriteria(RoleManagerConstants.ROLE_NAME, roleName, SearchCriteria.Operator.EQUAL);
        try {
            roles = rmgr.search(criteria, null, null);
        } catch (RoleSearchException e) {
            return null;
         
        }
        if(roles!=null && !roles.isEmpty())
            roleKey = (String)roles.get(0).getAttribute(RoleManagerConstants.ROLE_KEY);
        return roleKey;
     }
    public RoleManager getRoleManager() {
        RoleManager rmgr=null;
        rmgr = _oimClient.getService(RoleManager.class);
        return rmgr;
    }
    public String getUserKey(String userID)
     //----------------------------------------------------------------
     {
        String userKey = null;
        UserManager umgr = null;
        List<User> users = null;
        SearchCriteria criteria = null;
        /*
         * Use the "roleName" to obtain the internal "roleKey"
         */
        umgr = _oimClient.getService(UserManager.class);
        criteria = new SearchCriteria("User Login", userID, SearchCriteria.Operator.EQUAL);
        try {
            users = umgr.search(criteria, null, null);
        } catch (UserSearchException e) {
            return null;
        }
        if (users != null && !users.isEmpty())
               {
                userKey = users.get(0).getId();
               }
        return userKey;
     }
    public ResultMessage grantRoleToUser(String role_name,String userID) {
        try {
                Set<String> uKeys = new HashSet<String>();
                String userKey=null;
                String roleKey=null;
                userKey=getUserKey(userID);
                roleKey=getRoleKey(role_name);
                if(userKey==null)
                    return new ResultMessage(false,"User not found in OIM "+userID);
                if(roleKey==null)
                    return new ResultMessage(false,"Role not found in OIM "+role_name);
                uKeys.add(userKey);
            rmgr = this.getRoleManager();
                if(!rmgr.isRoleGranted(roleKey, userKey, true))
                    {
                        rmgr.grantRole(getRoleKey(role_name),uKeys);
                        return new ResultMessage(true,role_name+" role added to user "+userID);
                    }
                else
                    return new ResultMessage(true,"Role already exists");

            } catch (ValidationFailedException e) {
                e.printStackTrace();
                if("IAM-3056133".equals(e.getErrorCode()))
                    return new ResultMessage(true,"Role already exists");
                logger.log(logger.ERROR,"Role grant validation exception. "+e);
                return new ResultMessage(false,"Role grant validation exception. "+e.getMessage());
            } catch (RoleGrantException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"Role Grant Exception.");
                return new ResultMessage(false,"Role Grant Exception. "+e);
            } catch (UserMembershipException e) {
            return new ResultMessage(false,"User membership exception. "+e);
        }
    }
    public ResultMessage revokeRoleToUser(String role_name,String userID) {
        try {
            Set<String> uKeys = new HashSet<String>();
            String userKey=null;
            String roleKey=null;
            userKey=getUserKey(userID);
            roleKey=getRoleKey(role_name);
            if(userKey==null)
                return new ResultMessage(false,"User not found in OIM "+userID);
            if(roleKey==null)
                return new ResultMessage(false,"Role not found in OIM "+role_name);
            uKeys.add(userKey);
            rmgr = this.getRoleManager();
            if(rmgr.isRoleGranted(roleKey, userKey, true))
                {
                rmgr.revokeRoleGrant(getRoleKey(role_name),uKeys);
                return new ResultMessage(true,role_name+" role revoked for user "+userID);
                }
            else
                return new ResultMessage(false,"User does not have role "+role_name);
            } catch (ValidationFailedException e) {
                e.printStackTrace();
                if("IAM-3056134".equals(e.getErrorCode()))
                    return new ResultMessage(false,"User does not have role "+role_name);
                logger.log(logger.ERROR,"role revoke validation exception. "+e);
                return new ResultMessage(false,"Role revoke validation exception. "+e.getMessage());
            } catch (RoleGrantRevokeException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"Role revoke Exception.");
                return new ResultMessage(false,"Role revoke Exception. "+e);
            } catch (UserMembershipException e) {
                return new ResultMessage(false,"User membership exception. "+e);
        }
    }
    public boolean isRoleGranted(String role_name,String userID) {
        try {
            String userKey=null;
            String roleKey=null;
            userKey=getUserKey(userID);
            roleKey=getRoleKey(role_name);
            if(userKey==null)
                return false;
            if(roleKey==null)
                return false;
            rmgr = this.getRoleManager();
            return rmgr.isRoleGranted(roleKey, userKey, true);
            } catch (UserMembershipException e) {
             return false;
        }
    }    
}
