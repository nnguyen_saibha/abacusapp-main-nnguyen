package gov.ofda.abacus.model.base.framework;

import oracle.jbo.server.EntityImpl;

public class AbacusEntityImpl extends EntityImpl {
    public AbacusEntityImpl() {
        super();
    }
    public void remove() {
        refresh(REFRESH_WITH_DB_FORGET_CHANGES);
        super.remove();
    }
}
