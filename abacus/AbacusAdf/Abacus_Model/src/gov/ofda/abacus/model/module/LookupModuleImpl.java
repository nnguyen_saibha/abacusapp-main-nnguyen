package gov.ofda.abacus.model.module;


import gov.ofda.abacus.model.module.common.LookupModule;

import java.util.ArrayList;
import java.util.List;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.Row;
import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaRow;
import oracle.jbo.ViewObject;
import oracle.jbo.server.ApplicationModuleImpl;
import oracle.jbo.server.ViewLinkImpl;
import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Feb 08 11:13:59 EST 2013
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class LookupModuleImpl extends ApplicationModuleImpl implements LookupModule {
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.model.module.LookupModuleImpl.class);

    /**
     * This is the default constructor (do not remove).
     */
    public LookupModuleImpl() {
    }
    /**
     * Gets all user roles from AbacusUserDetailsView and keeps the list in session as roles
     * @param userId
     * @return
     */
    public List setUserInfo(String userId) {
        List rtn = new ArrayList();
        ViewObject vo = this.getAbacusUserDetailsView1();
        vo.setWhereClause("userid=:p_userid");
        vo.defineNamedWhereClauseParam("p_userid", userId.toUpperCase(), null);
        vo.executeQuery();
        RowIterator ri = vo.createRowSetIterator(null);
        while (ri.hasNext()) {
            Row r = ri.next();
            rtn.add(r.getAttribute("AbacusRole").toString());
        }
        vo.removeNamedWhereClauseParam("p_userid");
        vo.setWhereClause(null);
        return rtn;
    }

    /**
     * Container's getter for IndicatorLookupView1.
     * @return IndicatorLookupView1
     */
    public ViewObjectImpl getIndicatorLookupView1() {
        return (ViewObjectImpl)findViewObject("IndicatorLookupView1");
    }

    /**
     * Container's getter for IndTypeLookupView1.
     * @return IndTypeLookupView1
     */
    public ViewObjectImpl getIndTypeLookupView1() {
        return (ViewObjectImpl)findViewObject("IndTypeLookupView1");
    }

    /**
     * Container's getter for SLovActiveView1.
     * @return SLovActiveView1
     */
    public ViewObjectImpl getSLovActiveView1() {
        return (ViewObjectImpl)findViewObject("SLovActiveView1");
    }


    /**
     * Container's getter for LocationLookupCountryLevelTree1.
     * @return LocationLookupCountryLevelTree1
     */
    public ViewObjectImpl getLocationLookupCountryLevelTree1() {
        return (ViewObjectImpl)findViewObject("LocationLookupCountryLevelTree1");
    }

    /**
     * Container's getter for CountryLookupView1.
     * @return CountryLookupView1
     */
    public ViewObjectImpl getCountryLookupView1() {
        return (ViewObjectImpl)findViewObject("CountryLookupView1");
    }

    /**
     * Container's getter for RegionLookupView1.
     * @return RegionLookupView1
     */
    public ViewObjectImpl getRegionLookupView1() {
        return (ViewObjectImpl)findViewObject("RegionLookupView1");
    }

    /**
     * In the Add/edit locaiton, LocationCountryLookupView1 displays the countries.
     * This method filters the countries (list of value) drop down for the selected region code.
     *
     * @param regionCode
     * @return
     */
    public String setRegionCode(String regionCode){
        logger.log("Region code in impl"+regionCode);
        ViewObject vo = this.getLocationCountryLookupView1();
        vo.setWhereClause("region_code='" + regionCode+"'");
        logger.log("region_code=" + regionCode);
        vo.executeQuery();
        logger.log("Fetched row count: " + vo.getEstimatedRowCount());
        return null;
    }
    /**
     * setLocationCountry sets the Country code for the LocationLookupCountryLevelTree.
     * @param countryCode
     * @return Location Code for the selected country
     */
    public String setLocationCountry(String countryCode) {
        ViewObject vo = this.getLocationLookupCountryLevelTree1();
        vo.setWhereClause("country_code=" + countryCode);
        logger.log("country_code: " + countryCode);
        vo.executeQuery();
        logger.log("Fetched row count: " + vo.getEstimatedRowCount());
        RowSetIterator rsi = vo.createRowSetIterator(null);
        if (rsi.hasNext())
            return (rsi.next().getAttribute(0).toString());
        return null;
    }
    /**
     * setCountryCode sets the Country code for the LocationViewLookupUIAdmin1 which is a search lookup
     * @param countryCode
     * @return Location Code for the selected country
     */
    public String setCountryCode(String countryCode) {
        ViewObject vo = this.getLocationViewLookupUIAdmin1();
        vo.setWhereClause("country_code=" + countryCode);
        vo.executeQuery();
        logger.log("Search country code: " + countryCode);
        RowSetIterator rsi = vo.createRowSetIterator(null);
        if (rsi.hasNext())
            return (rsi.next().getAttribute(0).toString());
        return null;

    }

    /**
     * checkIndCodeInPASI checks whether the indicator code is present in the PAS_INDICATORS table.
     * @param indicatorCode
     * @return yes - if indicator code is used
     *         no - if indicator code is not used
     */
    public String checkIndCodeInPASI(String indicatorCode) {
        String x = "No";
        logger.log("checkIndCodeInPASI IndicatorCode : " + indicatorCode);

        ViewObject vo = this.getPasIndicatorsView1();
        ViewCriteria vc = vo.createViewCriteria();
        ViewCriteriaRow vcr = vc.createViewCriteriaRow();
        vcr.setAttribute("IndicatorCode", indicatorCode);
        vc.addElement(vcr);
        vo.applyViewCriteria(vc, true);
        vo.executeQuery();
        if (vo.getEstimatedRowCount() > 0) {
            x = "Yes";
        }
        logger.log("Output: " + x);
        return x;

    }

    public String checkLocCodeInPASL(String locationCode) {
        String x = "No";
        logger.log("checkLocCodeInPASL LocationCode : " + locationCode);
        ViewObject vo = this.getPasLocationsView1();
        ViewCriteria vc = vo.createViewCriteria();
        ViewCriteriaRow vcr = vc.createViewCriteriaRow();
        vcr.setAttribute("LocationCode", locationCode);
        vc.addElement(vcr);
        vo.applyViewCriteria(vc, true);
        vo.executeQuery();
        if (vo.getEstimatedRowCount() > 0) {
            x = "Yes";
        }
        logger.log("Output: " + x);
        return x;

    }

    /**
     * Container's getter for ActiveSubsectorLookupView1.
     * @return ActiveSubsectorLookupView1
     */
    public ViewObjectImpl getActiveSubsectorLookupView1() {
        return (ViewObjectImpl)findViewObject("ActiveSubsectorLookupView1");
    }


    /**
     * Container's getter for LocationLookup2View2.
     * @return LocationLookup2View2
     */
    public ViewObjectImpl getLocationViewLookupUIAdmin1() {
        return (ViewObjectImpl)findViewObject("LocationViewLookupUIAdmin1");
    }

    /**
     * Container's getter for PasIndicatorsView1.
     * @return PasIndicatorsView1
     */
    public ViewObjectImpl getPasIndicatorsView1() {
        return (ViewObjectImpl)findViewObject("PasIndicatorsView1");
    }

    /**
     * Container's getter for LocationViewLookupUI1.
     * @return LocationViewLookupUI1
     */
    public ViewObjectImpl getLocationViewLookupUI1() {
        return (ViewObjectImpl)findViewObject("LocationViewLookupUI1");
    }

    /**
     * Container's getter for LocationCountryLookupView1.
     * @return LocationCountryLookupView1
     */
    public ViewObjectImpl getLocationCountryLookupView1() {
        return (ViewObjectImpl)findViewObject("LocationCountryLookupView1");
    }


    /**
     * Container's getter for AbacusUserDetailsView2.
     * @return AbacusUserDetailsView2
     */
    public ViewObjectImpl getAbacusUserDetailsView1() {
        return (ViewObjectImpl)findViewObject("AbacusUserDetailsView1");
    }
    /**
         * Container's getter for PasLocationsView1.
         * @return PasLocationsView1
         */
        public ViewObjectImpl getPasLocationsView1() {
            return (ViewObjectImpl)findViewObject("PasLocationsView1");
        }

    /**
     * Container's getter for IndIndtypeLinkView2.
     * @return IndIndtypeLinkView2
     */
    public ViewObjectImpl getIndIndtypeLinkView1() {
        return (ViewObjectImpl)findViewObject("IndIndtypeLinkView1");
    }

    /**
     * Container's getter for IndicatorViewLookupUIIndIndTypeLink1.
     * @return IndicatorViewLookupUIIndIndTypeLink1
     */
    public ViewLinkImpl getIndicatorViewLookupUIIndIndTypeLink1() {
        return (ViewLinkImpl)findViewLink("IndicatorViewLookupUIIndIndTypeLink1");
    }

    /**
     * Container's getter for IndTypeValueLookupforIndicatorLookupView1.
     * @return IndTypeValueLookupforIndicatorLookupView1
     */
    public ViewObjectImpl getIndTypeValueLookupforIndicatorLookupView1() {
        return (ViewObjectImpl)findViewObject("IndTypeValueLookupforIndicatorLookupView1");
    }

    /**
     * Container's getter for IndTypeValueIndIndTypeLink2.
     * @return IndTypeValueIndIndTypeLink2
     */
    public ViewLinkImpl getIndTypeValueIndIndTypeLink2() {
        return (ViewLinkImpl)findViewLink("IndTypeValueIndIndTypeLink2");
    }

    /**
     * Container's getter for LocationViewLookupUIMap1.
     * @return LocationViewLookupUIMap1
     */
    public ViewObjectImpl getLocationViewLookupUIMap1() {
        return (ViewObjectImpl)findViewObject("LocationViewLookupUIMap1");
    }

    /**
     * Container's getter for LocationViewLookupUIMapLink1.
     * @return LocationViewLookupUIMapLink1
     */
    public ViewLinkImpl getLocationViewLookupUIMapLink1() {
        return (ViewLinkImpl)findViewLink("LocationViewLookupUIMapLink1");
    }
}
