package gov.ofda.abacus.model.constant;

public class Constants {
    public static final String CODEBASE_APPEND_STRING = "/forms/java";
    public static final String SERVER_APPEND_URL_1 = "/forms/lservlet?ifcfs=";
    public static final String SERVER_APPEND_URL_2 = "/forms/frmservlet?config=";
    public static final String SERVER_APPEND_URL_3 = "&#38;ifsessid=formsapp.154&#38;acceptLanguage=en-US,en;q=0.8";
    
    public static final String LOGO_APPEND_URL = "/Abacusapp/abacus.bmp";
}
