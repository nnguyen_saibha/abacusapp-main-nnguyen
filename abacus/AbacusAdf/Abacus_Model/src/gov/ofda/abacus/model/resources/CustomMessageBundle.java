package gov.ofda.abacus.model.resources;

import java.util.ListResourceBundle;

public class CustomMessageBundle extends ListResourceBundle {
    private static final Object[][] sMessageStrings = new String[][] {{ "LOCATION_LOOKUP_PARENT_LC_FK","Location is referred and cannot be deleted."},
                                                                      {"25014","Another user has changed the data. Please try again "},
                                                                      { "FK_PA_RG_ID_CODE","Restricted good(s) used in INA. To remove, first remove it from INA."},
                                                                      {"FK_PASECTOR_PA_PURPOSE_ID","Unexpected Issue. Please cancel your changes and try again. If adding new Purpose, make sure Purpose Name is entered."}};

    /**Return String Identifiers and corresponding Messages in a two-dimensional array.
     */
    protected Object[][] getContents() {
        return sMessageStrings;
    }
}
