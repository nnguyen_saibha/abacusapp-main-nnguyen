package gov.ofda.abacus.model.pa.entity;

import gov.ofda.abacus.model.base.framework.AbacusEntityImpl;

import gov.ofda.abacus.model.pa.view.PasIndicatorsViewRowImpl;

import java.math.BigDecimal;

import java.util.Date;

import oracle.adf.share.ADFContext;

import oracle.jbo.Key;
import oracle.jbo.RowIterator;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.TransactionEvent;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Nov 06 12:16:08 EST 2012
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PasiIndTypeImpl extends AbacusEntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. Do not modify.
     */
    public enum AttributesEnum {
        PasiIndTypeId {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getPasiIndTypeId();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setPasiIndTypeId((DBSequence)value);
            }
        }
        ,
        ParentPasiIndTypeId {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getParentPasiIndTypeId();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setParentPasiIndTypeId((BigDecimal)value);
            }
        }
        ,
        PasIndicatorsId {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getPasIndicatorsId();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setPasIndicatorsId((BigDecimal)value);
            }
        }
        ,
        IndTypeId {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getIndTypeId();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setIndTypeId((BigDecimal)value);
            }
        }
        ,
        IndTypevalueId {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getIndTypevalueId();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setIndTypevalueId((BigDecimal)value);
            }
        }
        ,
        IndTypeLevel {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getIndTypeLevel();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setIndTypeLevel((BigDecimal)value);
            }
        }
        ,
        UomValue1 {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getUomValue1();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setUomValue1((BigDecimal)value);
            }
        }
        ,
        UomValue2 {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getUomValue2();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setUomValue2((BigDecimal)value);
            }
        }
        ,
        Comments {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getComments();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setComments((String)value);
            }
        }
        ,
        UpdId {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getUpdId();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setUpdId((String)value);
            }
        }
        ,
        UpdDate {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getUpdDate();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setUpdDate((Timestamp)value);
            }
        }
        ,
        Active {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getActive();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setActive((String)value);
            }
        }
        ,
        PasiIndType {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getPasiIndType();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        ParentPasiIndTypeIdPasiIndType {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getParentPasiIndTypeIdPasiIndType();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setParentPasiIndTypeIdPasiIndType((PasiIndTypeImpl)value);
            }
        }
        ,
        PasIndicators {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getPasIndicators();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setPasIndicators((PasIndicatorsImpl)value);
            }
        }
        ,
        PasIndicatorsView {
            public Object get(PasiIndTypeImpl obj) {
                return obj.getPasIndicatorsView();
            }

            public void put(PasiIndTypeImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static int firstIndex = 0;

        public abstract Object get(PasiIndTypeImpl object);

        public abstract void put(PasiIndTypeImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    public static final int PASIINDTYPEID = AttributesEnum.PasiIndTypeId.index();
    public static final int PARENTPASIINDTYPEID = AttributesEnum.ParentPasiIndTypeId.index();
    public static final int PASINDICATORSID = AttributesEnum.PasIndicatorsId.index();
    public static final int INDTYPEID = AttributesEnum.IndTypeId.index();
    public static final int INDTYPEVALUEID = AttributesEnum.IndTypevalueId.index();
    public static final int INDTYPELEVEL = AttributesEnum.IndTypeLevel.index();
    public static final int UOMVALUE1 = AttributesEnum.UomValue1.index();
    public static final int UOMVALUE2 = AttributesEnum.UomValue2.index();
    public static final int COMMENTS = AttributesEnum.Comments.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int ACTIVE = AttributesEnum.Active.index();
    public static final int PASIINDTYPE = AttributesEnum.PasiIndType.index();
    public static final int PARENTPASIINDTYPEIDPASIINDTYPE = AttributesEnum.ParentPasiIndTypeIdPasiIndType.index();
    public static final int PASINDICATORS = AttributesEnum.PasIndicators.index();
    public static final int PASINDICATORSVIEW = AttributesEnum.PasIndicatorsView.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PasiIndTypeImpl() {
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("gov.ofda.abacus.model.pa.entity.PasiIndType");
    }

    /**
     * Gets the attribute value for PasiIndTypeId, using the alias name PasiIndTypeId.
     * @return the value of PasiIndTypeId
     */
    public DBSequence getPasiIndTypeId() {
        return (DBSequence)getAttributeInternal(PASIINDTYPEID);
    }

    /**
     * Sets <code>value</code> as the attribute value for PasiIndTypeId.
     * @param value value to set the PasiIndTypeId
     */
    public void setPasiIndTypeId(DBSequence value) {
        setAttributeInternal(PASIINDTYPEID, value);
    }

    /**
     * Gets the attribute value for ParentPasiIndTypeId, using the alias name ParentPasiIndTypeId.
     * @return the value of ParentPasiIndTypeId
     */
    public BigDecimal getParentPasiIndTypeId() {
        return (BigDecimal)getAttributeInternal(PARENTPASIINDTYPEID);
    }

    /**
     * Sets <code>value</code> as the attribute value for ParentPasiIndTypeId.
     * @param value value to set the ParentPasiIndTypeId
     */
    public void setParentPasiIndTypeId(BigDecimal value) {
        setAttributeInternal(PARENTPASIINDTYPEID, value);
    }

    /**
     * Gets the attribute value for PasIndicatorsId, using the alias name PasIndicatorsId.
     * @return the value of PasIndicatorsId
     */
    public BigDecimal getPasIndicatorsId() {
        return (BigDecimal)getAttributeInternal(PASINDICATORSID);
    }

    /**
     * Sets <code>value</code> as the attribute value for PasIndicatorsId.
     * @param value value to set the PasIndicatorsId
     */
    public void setPasIndicatorsId(BigDecimal value) {
        setAttributeInternal(PASINDICATORSID, value);
    }

    /**
     * Gets the attribute value for IndTypeId, using the alias name IndTypeId.
     * @return the value of IndTypeId
     */
    public BigDecimal getIndTypeId() {
        return (BigDecimal)getAttributeInternal(INDTYPEID);
    }

    /**
     * Sets <code>value</code> as the attribute value for IndTypeId.
     * @param value value to set the IndTypeId
     */
    public void setIndTypeId(BigDecimal value) {
        setAttributeInternal(INDTYPEID, value);
    }

    /**
     * Gets the attribute value for IndTypevalueId, using the alias name IndTypevalueId.
     * @return the value of IndTypevalueId
     */
    public BigDecimal getIndTypevalueId() {
        return (BigDecimal)getAttributeInternal(INDTYPEVALUEID);
    }

    /**
     * Sets <code>value</code> as the attribute value for IndTypevalueId.
     * @param value value to set the IndTypevalueId
     */
    public void setIndTypevalueId(BigDecimal value) {
        setAttributeInternal(INDTYPEVALUEID, value);
    }

    /**
     * Gets the attribute value for IndTypeLevel, using the alias name IndTypeLevel.
     * @return the value of IndTypeLevel
     */
    public BigDecimal getIndTypeLevel() {
        return (BigDecimal)getAttributeInternal(INDTYPELEVEL);
    }

    /**
     * Sets <code>value</code> as the attribute value for IndTypeLevel.
     * @param value value to set the IndTypeLevel
     */
    public void setIndTypeLevel(BigDecimal value) {
        setAttributeInternal(INDTYPELEVEL, value);
    }

    /**
     * Gets the attribute value for UomValue1, using the alias name UomValue1.
     * @return the value of UomValue1
     */
    public BigDecimal getUomValue1() {
        return (BigDecimal)getAttributeInternal(UOMVALUE1);
    }

    /**
     * Sets <code>value</code> as the attribute value for UomValue1.
     * @param value value to set the UomValue1
     */
    public void setUomValue1(BigDecimal value) {
        setAttributeInternal(UOMVALUE1, value);
    }

    /**
     * Gets the attribute value for UomValue2, using the alias name UomValue2.
     * @return the value of UomValue2
     */
    public BigDecimal getUomValue2() {
        return (BigDecimal)getAttributeInternal(UOMVALUE2);
    }

    /**
     * Sets <code>value</code> as the attribute value for UomValue2.
     * @param value value to set the UomValue2
     */
    public void setUomValue2(BigDecimal value) {
        setAttributeInternal(UOMVALUE2, value);
    }

    /**
     * Gets the attribute value for Comments, using the alias name Comments.
     * @return the value of Comments
     */
    public String getComments() {
        return (String)getAttributeInternal(COMMENTS);
    }

    /**
     * Sets <code>value</code> as the attribute value for Comments.
     * @param value value to set the Comments
     */
    public void setComments(String value) {
        setAttributeInternal(COMMENTS, value);
    }

    /**
     * Gets the attribute value for UpdId, using the alias name UpdId.
     * @return the value of UpdId
     */
    public String getUpdId() {
        return (String)getAttributeInternal(UPDID);
    }

    /**
     * Sets <code>value</code> as the attribute value for UpdId.
     * @param value value to set the UpdId
     */
    public void setUpdId(String value) {
        setAttributeInternal(UPDID, value);
    }

    /**
     * Gets the attribute value for UpdDate, using the alias name UpdDate.
     * @return the value of UpdDate
     */
    public Timestamp getUpdDate() {
        return (Timestamp)getAttributeInternal(UPDDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for UpdDate.
     * @param value value to set the UpdDate
     */
    public void setUpdDate(Timestamp value) {
        setAttributeInternal(UPDDATE, value);
    }

    /**
     * Gets the attribute value for Active, using the alias name Active.
     * @return the value of Active
     */
    public String getActive() {
        return (String)getAttributeInternal(ACTIVE);
    }

    /**
     * Sets <code>value</code> as the attribute value for Active.
     * @param value value to set the Active
     */
    public void setActive(String value) {
        setAttributeInternal(ACTIVE, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }

    /**
     * @return the associated entity oracle.jbo.RowIterator.
     */
    public RowIterator getPasiIndType() {
        return (RowIterator)getAttributeInternal(PASIINDTYPE);
    }

    /**
     * @return the associated entity PasiIndTypeImpl.
     */
    public PasiIndTypeImpl getParentPasiIndTypeIdPasiIndType() {
        return (PasiIndTypeImpl)getAttributeInternal(PARENTPASIINDTYPEIDPASIINDTYPE);
    }

    /**
     * Sets <code>value</code> as the associated entity PasiIndTypeImpl.
     */
    public void setParentPasiIndTypeIdPasiIndType(PasiIndTypeImpl value) {
        setAttributeInternal(PARENTPASIINDTYPEIDPASIINDTYPE, value);
    }

    /**
     * @return the associated entity gov.ofda.abacus.model.base.framework.AbacusEntityImpl.
     */
    public PasIndicatorsImpl getPasIndicators() {
        return (PasIndicatorsImpl)getAttributeInternal(PASINDICATORS);
    }

    /**
     * Sets <code>value</code> as the associated entity gov.ofda.abacus.model.base.framework.AbacusEntityImpl.
     */
    public void setPasIndicators(PasIndicatorsImpl value) {
        setAttributeInternal(PASINDICATORS, value);
    }

    /**
     * Uses the link FkTPasiIndTypeIndicatorsLink to return rows of PasiIndTypeView
     */
    public PasIndicatorsViewRowImpl getPasIndicatorsView() {
        return (PasIndicatorsViewRowImpl)getAttributeInternal(PASINDICATORSVIEW);
    }


    /**
     * @param pasiIndTypeId key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(DBSequence pasiIndTypeId) {
        return new Key(new Object[]{pasiIndTypeId});
    }

    /**
     * Add locking logic here.
     */
    public void lock() {
        super.lock();
    }

    /**
     * Custom DML update/insert/delete logic here.
     * @param operation the operation type
     * @param e the transaction event
     */
    protected void doDML(int operation, TransactionEvent e) {
        super.doDML(operation, e);
    }
    protected void prepareForDML(int operation,TransactionEvent e){
        super.prepareForDML(operation, e);
        if(operation==DML_UPDATE || operation==DML_INSERT)
        {
         String userID=ADFContext.getCurrent().getSecurityContext().getUserName().toUpperCase();
         this.setUpdId(userID);
        }
    }
}
