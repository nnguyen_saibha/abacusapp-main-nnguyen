package gov.ofda.abacus.model.module;

import gov.ofda.abacus.model.constant.Constants;


import gov.ofda.abacus.model.module.common.LOVModule;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import oracle.jbo.Row;
import oracle.jbo.server.ApplicationModuleImpl;
import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Nov 12 12:48:08 EST 2012
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class LOVModuleImpl extends ApplicationModuleImpl implements LOVModule {
    /**
     * This is the default constructor (do not remove).
     */
    public LOVModuleImpl() {
    }

    /**
     * Container's getter for ActionTypeLookupView1.
     * @return ActionTypeLookupView1
     */
    public ViewObjectImpl getActionTypeLookupView1() {
        return (ViewObjectImpl)findViewObject("ActionTypeLookupView1");
    }

    /**
     * Container's getter for AwardeeLookupView1.
     * @return AwardeeLookupView1
     */
    public ViewObjectImpl getAwardeeLookupView1() {
        return (ViewObjectImpl)findViewObject("AwardeeLookupView1");
    }

    /**
     * Container's getter for FundingActionLookupView1.
     * @return FundingActionLookupView1
     */
    public ViewObjectImpl getFundingActionLookupView1() {
        return (ViewObjectImpl)findViewObject("FundingActionLookupView1");
    }


    /**
     * Container's getter for IndTypevalueLookupView2.
     * @return IndTypevalueLookupView2
     */
    public ViewObjectImpl getIndTypevalueLookupView2() {
        return (ViewObjectImpl)findViewObject("IndTypevalueLookupView2");
    }


    /**
     * Container's getter for ProjectLookupView1.
     * @return ProjectLookupView1
     */
    public ViewObjectImpl getProjectLookupView1() {
        return (ViewObjectImpl)findViewObject("ProjectLookupView1");
    }

    /**
     * Container's getter for RegionLookupView1.
     * @return RegionLookupView1
     */
    public ViewObjectImpl getRegionLookupView1() {
        return (ViewObjectImpl)findViewObject("RegionLookupView1");
    }

    /**
     * Container's getter for SectorLookupView1.
     * @return SectorLookupView1
     */
    public ViewObjectImpl getSectorLookupView1() {
        return (ViewObjectImpl)findViewObject("SectorLookupView1");
    }

    /**
     * Container's getter for SubsectorLookupView1.
     * @return SubsectorLookupView1
     */
    public ViewObjectImpl getSubsectorLookupView1() {
        return (ViewObjectImpl)findViewObject("SubsectorLookupView1");
    }

    /**
     * Container's getter for UomLookupView1.
     * @return UomLookupView1
     */
    public ViewObjectImpl getUomLookupView1() {
        return (ViewObjectImpl)findViewObject("UomLookupView1");
    }

    /**
     * Container's getter for CountryLookupView1.
     * @return CountryLookupView1
     */
    public ViewObjectImpl getCountryLookupView1() {
        return (ViewObjectImpl)findViewObject("CountryLookupView1");
    }

    /**
     * Container's getter for AbacusConstants1.
     * @return AbacusConstants1
     */
    public ViewObjectImpl getAbacusConstants1() {
        return (ViewObjectImpl)findViewObject("AbacusConstants1");
    }

    public Map retrieveInstanceAttributes() {
        Map<String, String> attributeMap = null;
        ViewObjectImpl vo = this.getAbacusConstants1();
        vo.executeQuery();
        Row row = null;
        String hostName = null;
        String instanceName = null;
        String protocol = null;
        String hostURL = null;
        String codeBase = null;
        String logoURL = null;
        String serverURL = null;
        String appVersion = null;
        String trainingURL = null;
        while (vo.hasNext()) {
            row = vo.next();
            if ("HOSTURL".equalsIgnoreCase((String)row.getAttribute("Code"))) {
                hostName = (String)row.getAttribute("CodeValue");
            } else if ("INSTANCE".equalsIgnoreCase((String)row.getAttribute("Code"))) {
                instanceName = (String)row.getAttribute("CodeValue");
            } else if ("PROTOCOL".equalsIgnoreCase((String)row.getAttribute("Code"))) {
                protocol = (String)row.getAttribute("CodeValue");
            } else if ("VERSION".equalsIgnoreCase((String)row.getAttribute("CodeType"))) {
                appVersion = (String)row.getAttribute("CodeValue");
            }
        }
        if (hostName != null && instanceName != null && protocol != null) {
            attributeMap = new HashMap<String, String>();
            hostURL = retrieveURL(protocol, hostName);
            codeBase = constructCodeBaseURL(hostURL);
            logoURL = constructLogoURL(hostURL);
            serverURL = constructServerURL(hostURL, instanceName);
            trainingURL = constructServerURL(hostURL, "OFDAU11g");
            attributeMap.put("codebase", codeBase);
            attributeMap.put("logo", logoURL);
            attributeMap.put("serverURL", serverURL);
            attributeMap.put("trainingURL", trainingURL);
            attributeMap.put("instance", instanceName);
            attributeMap.put("hostURL", hostURL);
            attributeMap.put("appName", "Abacus");
            attributeMap.put("appVersion", appVersion);
            if ("OFDAP".equals(instanceName) || "OFDAPG".equals(instanceName)) {
                attributeMap.put("colorScheme", "Teal");
                attributeMap.put("ssoMode", "webgate");
                attributeMap.put("serverArgs",
                                 "escapeParams=true module=WELCOME.fmx userid=  debug=no host= port= obr=no record= tracegroup= log= term= ssoProxyConnect=yes");
            } else {
                if ("OFDAU".equals(instanceName)) {
                    attributeMap.put("colorScheme", "Blue");
                    attributeMap.put("ssoMode", "webgate");
                    attributeMap.put("serverArgs",
                                     "escapeParams=true module=WELCOME.fmx userid=  debug=no host= port= obr=no record= tracegroup= log= term= ssoProxyConnect=yes");
                }
                else {
                    attributeMap.put("colorScheme", "SWAN");
                    attributeMap.put("ssoMode", "none");
                    attributeMap.put("serverArgs",
                                     "escapeParams=true module=WELCOME_NONSSO.fmx userid=  debug=no host= port= obr=no record= tracegroup= log= term= ssoProxyConnect=no");
                }
            }

        }
        return attributeMap;
    }

    /**
     *
     * @param
     * @return
     */
    private String retrieveURL(String protocol, String hostName) {
        String hostURL = null;
        StringBuffer sb = null;
        if (protocol != null && hostName != null) {
            sb = new StringBuffer();
            sb.append(protocol);
            sb.append("://");
            sb.append(hostName);
            hostURL = sb.toString();
        }
        return hostURL;
    }

    private String constructCodeBaseURL(String hostURL) {
        String codeBaseURL = null;
        StringBuffer sb = null;
        if (hostURL != null) {
            sb = new StringBuffer();
            sb.append(hostURL);
            sb.append(Constants.CODEBASE_APPEND_STRING);
            codeBaseURL = sb.toString();
        }
        return codeBaseURL;
    }

    private String constructLogoURL(String hostURL) {
        String logoURL = null;
        StringBuffer sb = null;
        if (hostURL != null) {
            sb = new StringBuffer();
            sb.append(hostURL);
            sb.append(Constants.LOGO_APPEND_URL);
            logoURL = sb.toString();
        }
        return logoURL;
    }

    private String constructServerURL(String hostURL, String instanceName) {
        String serverURL = null;
        StringBuffer sb = null;
        if (hostURL != null && instanceName != null) {
            sb = new StringBuffer();
            sb.append(hostURL);
            sb.append(Constants.SERVER_APPEND_URL_1);
            sb.append(hostURL);
            sb.append(Constants.SERVER_APPEND_URL_2);
            sb.append(instanceName);
            sb.append(Constants.SERVER_APPEND_URL_3);
            serverURL = sb.toString();
        }
        return serverURL;
    }


    /**
     * Container's getter for RgLookupView1.
     * @return RgLookupView1
     */
    public ViewObjectImpl getRgLookupView1() {
        return (ViewObjectImpl)findViewObject("RgLookupView1");
    }


    /**
     * Container's getter for IndTypeLookupView1.
     * @return IndTypeLookupView1
     */
    public ViewObjectImpl getIndTypeLookupView1() {
        return (ViewObjectImpl)findViewObject("IndTypeLookupView1");
    }
}
