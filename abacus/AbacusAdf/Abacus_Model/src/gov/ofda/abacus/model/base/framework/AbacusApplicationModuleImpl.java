package gov.ofda.abacus.model.base.framework;

import oracle.jbo.server.ApplicationModuleImpl;

public class AbacusApplicationModuleImpl extends ApplicationModuleImpl {
    public AbacusApplicationModuleImpl() {
        super();
    }
}
