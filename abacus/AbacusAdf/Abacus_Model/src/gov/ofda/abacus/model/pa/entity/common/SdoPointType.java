package gov.ofda.abacus.model.pa.entity.common;

import java.lang.Integer;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.sql.SQLException;

import oracle.jbo.JboException;
import oracle.jbo.StructureDef;
import oracle.jbo.domain.DatumFactory;
import oracle.jbo.domain.DomainAttributeDef;
import oracle.jbo.domain.DomainInterface;
import oracle.jbo.domain.DomainOwnerInterface;
import oracle.jbo.domain.DomainStructureDef;
import oracle.jbo.domain.Struct;

import oracle.sql.*;


// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Nov 06 11:41:30 EST 2012
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SdoPointType extends Struct {


    static ORADataFactory[] mCustDatFac = null;
    static int[] mSQLTypes = null;
    static DomainStructureDef mStructureDef = null;
    static ORADataFactory fac;

    public SdoPointType(Datum value) throws SQLException {
        super(value);
    }

    public SdoPointType() throws SQLException {
    }

    public static ORADataFactory getORADataFactory() {
        if (fac == null) {
            class facClass implements ORADataFactory {
                public ORAData create(Datum d, int sql_type_code) throws SQLException {
                    if (d != null) {
                        return new SdoPointType(d);
                    }
                    return null;
                }
            }
            fac = new facClass();
        }
        return fac;
    }

    public int[] getAttrSQLTypes() {
        if (mSQLTypes == null) {
            mSQLTypes = buildAttrSQLTypes();
        }
        return mSQLTypes;
    }

    public StructureDef getStructureDef() {
        return mStructureDef;
    }

    public String getColumnType() {
        return "SDO_POINT_TYPE";
    }

    public Integer getX() {
        return (Integer) getAttribute(0);
    }

    public void setX(Integer value) {
        setAttribute(0, value);
    }

    public Integer getY() {
        return (Integer) getAttribute(1);
    }

    public void setY(Integer value) {
        setAttribute(1, value);
    }

    public Integer getZ() {
        return (Integer) getAttribute(2);
    }

    public void setZ(Integer value) {
        setAttribute(2, value);
    }

    public void initStructureDef() {
        DomainAttributeDef[] attrs = new DomainAttributeDef[3];
        if (mStructureDef == null) {
            attrs[0] = new DomainAttributeDef("X", "X", 0, Integer.class, 2, "NUMERIC", 0, 0, false);
            attrs[1] = new DomainAttributeDef("Y", "Y", 1, Integer.class, 2, "NUMERIC", 0, 0, false);
            attrs[2] = new DomainAttributeDef("Z", "Z", 2, Integer.class, 2, "NUMERIC", 0, 0, false);
            mStructureDef = new DomainStructureDef(attrs);
        }
    }

    public ORADataFactory[] getAttrORADataFactories() {
        if (mCustDatFac == null) {
            mCustDatFac = new ORADataFactory[3];
            mCustDatFac[0] = null;
            mCustDatFac[1] = null;
            mCustDatFac[2] = null;
        }
        return mCustDatFac;
    }
}
