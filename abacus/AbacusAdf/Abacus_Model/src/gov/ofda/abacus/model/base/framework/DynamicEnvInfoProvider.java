package gov.ofda.abacus.model.base.framework;

import java.util.Hashtable;
import java.util.Map;

import oracle.adf.share.ADFContext;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.client.Configuration;
import oracle.jbo.common.ampool.EnvInfoProvider;

public class DynamicEnvInfoProvider implements EnvInfoProvider {
    private static ADFLogger logger = ADFLogger.createADFLogger(DynamicEnvInfoProvider.class);
    public DynamicEnvInfoProvider() {
        super();
    }

    /**
     *http://andrejusb.blogspot.com/2012/08/sample-application-for-switching.html
     * @param infoType
     * @param env
     * @return
     *
     */
    @Override
    public Object getInfo(String infoType, Object env) {
        Map session = null;
        Object dsName = null;
        if (EnvInfoProvider.INFO_TYPE_JDBC_PROPERTIES.equals(infoType)) {
            session = ADFContext.getCurrent().getSessionScope();
            if (session != null) {
                dsName = session.get("dataSource");               
                if (dsName != null) {
                    if (((Hashtable)env).containsKey(Configuration.JDBC_DS_NAME)) {
                        ((Hashtable)env).put(Configuration.JDBC_DS_NAME, dsName);
                    }
                }
            }
        }
        return null;
    }


    @Override
    public int getNumOfRetries() {
        return 0;
    }

    @Override
    public void modifyInitialContext(Object object) {
    }
}
