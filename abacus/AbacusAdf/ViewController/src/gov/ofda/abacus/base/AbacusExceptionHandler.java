package gov.ofda.abacus.base;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.context.ExceptionHandler;
/**
 * AbacusExceptionHandler.java
 * Purpose: To handle exceptions and suppress them.
 * @author Kartheek Atluri
 * @version 1.0 09/09/2012
 */
public class AbacusExceptionHandler extends ExceptionHandler {
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.base.AbacusExceptionHandler.class);

    /**
     * Class Constructor
     */
    public AbacusExceptionHandler() {
        super();
    }

    /**
     * handles exceptions.
     * @param facesContext
     * @param throwable
     * @param phaseId
     * @throws Throwable
     */
    @Override
    public void handleException(FacesContext facesContext, Throwable throwable, PhaseId phaseId) throws Throwable {
        String error_msg;
        
        error_msg=throwable.getMessage();
        //Handle Active data exception when user tries import to excel and then tries to navigate.
        if(error_msg != null &&
            error_msg.indexOf("ADF_FACES-60003") > -1) {
            logger.log(logger.TRACE,"Handled: ADF_FACES-60003");
        }
        else
        {
            //Default ADFc exception handler
            throw throwable;
        }
    }
}
