package gov.ofda.abacus.base;
import gov.ofda.abacus.base.UIControl;

import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

/**
 * PrintBean.java
 * Purpose: To override print behavior.
 * @author Kartheek Atluri
 * @version 1.0 09/09/2012
 */
public class PrintBean extends UIControl{
    public PrintBean() {
    }

    /**
     * To open print window when user click on print preview.
     * @param phaseEvent
     */
    public void beforePhaseMethod(PhaseEvent phaseEvent) {
        //only perform action if RENDER_RESPONSE phase is reached
         if (phaseEvent.getPhaseId() == PhaseId.RENDER_RESPONSE){
           FacesContext fctx = FacesContext.getCurrentInstance();
           //check internal request parameter
           Map requestMap = fctx.getExternalContext().getRequestMap();
           Object showPrintableBehavior =                   
        requestMap.get("oracle.adfinternal.view.faces.el.PrintablePage");            
           if(showPrintableBehavior != null){
             if (Boolean.TRUE == showPrintableBehavior){
                ExtendedRenderKitService erks = null;
                erks = Service.getRenderKitService(
                                  fctx,ExtendedRenderKitService.class);
                //invoke JavaScript from the server
                erks.addScript(fctx, "window.print();");
             }
           }            
         }
    }
}
