package gov.ofda.abacus.base;

import java.util.Iterator;

import java.util.List;

import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.model.SelectItem;

import oracle.adf.controller.ControllerContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.data.RichTree;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.nav.RichCommandImageLink;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.QueryEvent;
import oracle.adf.view.rich.model.FilterableQueryDescriptor;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowNotFoundException;
import oracle.jbo.RowSetIterator;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import oracle.jbo.uicli.binding.JUCtrlHierTypeBinding;
import oracle.jbo.uicli.binding.JUIteratorBinding;

import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


/**
 * UIControl.java
 * Purpose: Used as to provide common methods that all backing beans use by extending this class.
 *
 * @author Kartheek Atluri
 */
public class UIControl {
    private Long random;

    public UIControl() {
        super();
    }

    /**
     * Returns Row's Key of the selected row in table t
     * @param t
     * @return Key of selected row
     */
    public Key getRowKey(RichTable t) {
        Object _selectedRowData1 = t.getSelectedRowData();
        JUCtrlHierNodeBinding _nodeBinding1 = (JUCtrlHierNodeBinding) _selectedRowData1;
        if (_nodeBinding1 != null)
            return (_nodeBinding1.getRowKey());
        return null;
    }

    /**
     * Sets the selected row in table t to the row with key k
     * @param t
     * @param k
     */
    public void setCurrentRow(RichTable t, Key k) {
        CollectionModel _tableModle1 = (CollectionModel) t.getValue();
        JUCtrlHierBinding _tableBinding1 = (JUCtrlHierBinding) _tableModle1.getWrappedData();
        DCIteratorBinding _tableIteratorBinding1 = _tableBinding1.getDCIteratorBinding();

        try {
            _tableIteratorBinding1.setCurrentRowWithKey(k.toStringFormat(true));
        } catch (RowNotFoundException e) {

        } catch (Exception e) {
        }
        return;
    }

    /**
     * Return if the current task flow is dirty (unsaved changes).
     * @return
     */
    public boolean isDataDirty() {
        ControllerContext ctx = ControllerContext.getInstance();
        return ctx.getCurrentViewPort().getTaskFlowContext().isDataDirty();
    }

    public Long getRandom() {
        random = Math.round(Math.random() * 10000);
        return random;
    }

    public void execCmdAndFocus(ActionEvent event) {
        // Add event code here...
        RichCommandImageLink cl = (RichCommandImageLink) event.getSource();
        String execCmd = (String) cl.getAttributes().get("execCmd");
        String focusId = (String) cl.getAttributes().get("focusId");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding oper1 = bindings.getOperationBinding(execCmd);
        oper1.execute();
        //compose JavaScript to be executed on the client
        StringBuilder script = new StringBuilder();
        //use client id to ensure component is found if located in
        //naming container
        script.append("var fvar = ");
        script.append("AdfPage.PAGE.findComponentByAbsoluteId");
        script.append("('" + focusId + "');");
        script.append("if(fvar != null){");
        script.append("fvar.focus();");
        script.append("}");
        //invoke JavaScript
        writeJavaScriptToClient(script.toString());
    }
    //generic, reusable helper method to call JavaScript on a client

    private void writeJavaScriptToClient(String script) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExtendedRenderKitService erks = null;
        erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
        erks.addScript(fctx, script);
    }

    /**
     * Called by each row in the tables
     * Return color depending on rowStatus
     * Green    - if new
     * Yellow   - if modified
     * Grey     - if Submitted
     * @return
     */
    public String getRowColor() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExpressionFactory ef = ctx.getApplication().getExpressionFactory();
        ValueExpression ve = ef.createValueExpression(ctx.getELContext(), "#{row}", JUCtrlHierNodeBinding.class);
        JUCtrlHierNodeBinding node = (JUCtrlHierNodeBinding) ve.getValue(ctx.getELContext());
        Row row = node.getRow();

        OperationBinding getRowStatusBinding = bindings.getOperationBinding("getRowStatus");
        getRowStatusBinding.getParamsMap().put("row", row);
        String rwStatus = (String) getRowStatusBinding.execute();
        if (rwStatus.equals("New"))
            return "background-color:#99FF99";
        else if (rwStatus.equals("Modified"))
            return "background-color:#FFFF99";
        else
            return "";

    }

    /**
     * Custom managed bean method that takes a SelectEvent input argument
     * to generically set the current row corresponding to the selected row
     * in the tree. Note that this method is a way to replace "makeCurrent"
     * EL expression (#{bindings.<tree binding>. treeModel.makeCurrent}that
     * Oracle JDeveloper adds to the tree component SelectionListener
     * property when dragging a collection from the Data Controls panel.
     * Using this custom selection listener allows developers to add pre-
     * and post processing instructions. For example, you may enforce PPR
     * on a specific item after a new tree node has been  selected. This
     * methods performs the following steps
     *
     * i.   get access to the tree component
     * ii. get access to the ADF tree binding
     * iii. set the current row on the ADF binding
     * iv. get the information about target iterators to synchronize
     * v.   synchronize target iterator
     *
     * @param selectionEvent object passed in by ADF Faces when configuring
     *  this method to become the selection listener
     *
     * @author Frank Nimphius
     */
    public String onTreeTableSelect(SelectionEvent selectionEvent) {

        RichTreeTable tree1 = (RichTreeTable) selectionEvent.getSource();
        RowKeySet rks2 = selectionEvent.getAddedSet();
        String nodeStuctureDefname = null;
        //iterate over the contained keys. Though for a single selection use
        //case we only expect one entry in here
        Iterator rksIterator = rks2.iterator();
        //support single row selection case
        if (rksIterator.hasNext()) {
            //get the tree node key, which is a List of path entries describing
            //the location of the node in the tree including its parents nodes
            List key = (List) rksIterator.next();
            //get the ADF tree binding to work with
            JUCtrlHierBinding treeBinding = null;
            //The Trinidad CollectionModel is used to provide data to trees and
            //tables. In the ADF binding case, it contains the tree binding as
            //wrapped data
            treeBinding = (JUCtrlHierBinding) ((CollectionModel) tree1.getValue()).getWrappedData();
            //find the node identified by the node path from the ADF binding
            //layer. Note that we don't need to know about the name of the tree
            //binding in the PageDef file because
            //all information is provided
            JUCtrlHierNodeBinding nodeBinding = nodeBinding = treeBinding.findNodeByKeyPath(key);
            //the current row is set on the iterator binding. Because all
            //bindings have an internal reference to their iterator usage,
            //the iterator can be queried from the ADF binding object
            DCIteratorBinding _treeIteratorBinding = null;
            _treeIteratorBinding = treeBinding.getDCIteratorBinding();
            JUIteratorBinding iterator = nodeBinding.getIteratorBinding();
            String keyStr = nodeBinding.getRowKey().toStringFormat(true);
            iterator.setCurrentRowWithKey(keyStr);
            //get selected node type information
            JUCtrlHierTypeBinding typeBinding = nodeBinding.getHierTypeBinding();
            //The tree node rule may have a target iterator defined. Target
            //iterators are configured using the Target Data Source entry in
            //the tree node edit dialog
            //and allow developers to declaratively synchronize an independent
            //iterator binding with the node selection in the tree.
            //
            nodeStuctureDefname = nodeBinding.getHierTypeBinding().getStructureDefName();
            String targetIteratorSpelString = typeBinding.getTargetIterator();

            //chances are that the target iterator option is not configured. We
            //avoid NPE by checking this condition

            if (targetIteratorSpelString != null && !targetIteratorSpelString.isEmpty()) {

                //resolve SPEL string for target iterator
                DCIteratorBinding targetIterator = resolveTargetIterWithSpel(targetIteratorSpelString);
                //synchronize the row in the target iterator
                Key rowKey = nodeBinding.getCurrentRow().getKey();
                if (rowKey != null && targetIterator != null) {
                    targetIterator.setCurrentRowWithKey(rowKey.toStringFormat(true));
                }

            }
        }
        return nodeStuctureDefname;
    }


    /**
     * Custom managed bean method that takes a SelectEvent input argument
     * to generically set the current row corresponding to the selected row
     * in the tree. Note that this method is a way to replace "makeCurrent"
     * EL expression (#{bindings.<tree binding>. treeModel.makeCurrent}that
     * Oracle JDeveloper adds to the tree component SelectionListener
     * property when dragging a collection from the Data Controls panel.
     * Using this custom selection listener allows developers to add pre-
     * and post processing instructions. For example, you may enforce PPR
     * on a specific item after a new tree node has been  selected. This
     * methods performs the following steps
     *
     * i.   get access to the tree component
     * ii. get access to the ADF tree binding
     * iii. set the current row on the ADF binding
     * iv. get the information about target iterators to synchronize
     * v.   synchronize target iterator
     *
     * @param selectionEvent object passed in by ADF Faces when configuring
     *  this method to become the selection listener
     *
     * @author Frank Nimphius
     */
    public String onTreeTableSelectDirty(SelectionEvent selectionEvent) {

        RichTreeTable tree1 = (RichTreeTable) selectionEvent.getSource();
        RowKeySet rks2 = selectionEvent.getRemovedSet();
        String nodeStuctureDefname = null;
        //iterate over the contained keys. Though for a single selection use
        //case we only expect one entry in here
        Iterator rksIterator = rks2.iterator();
        //support single row selection case
        if (rksIterator.hasNext()) {
            //get the tree node key, which is a List of path entries describing
            //the location of the node in the tree including its parents nodes
            List key = (List) rksIterator.next();
            //get the ADF tree binding to work with
            JUCtrlHierBinding treeBinding = null;
            //The Trinidad CollectionModel is used to provide data to trees and
            //tables. In the ADF binding case, it contains the tree binding as
            //wrapped data
            treeBinding = (JUCtrlHierBinding) ((CollectionModel) tree1.getValue()).getWrappedData();
            //find the node identified by the node path from the ADF binding
            //layer. Note that we don't need to know about the name of the tree
            //binding in the PageDef file because
            //all information is provided
            JUCtrlHierNodeBinding nodeBinding = nodeBinding = treeBinding.findNodeByKeyPath(key);
            //the current row is set on the iterator binding. Because all
            //bindings have an internal reference to their iterator usage,
            //the iterator can be queried from the ADF binding object
            DCIteratorBinding _treeIteratorBinding = null;
            _treeIteratorBinding = treeBinding.getDCIteratorBinding();
            JUIteratorBinding iterator = nodeBinding.getIteratorBinding();
            String keyStr = nodeBinding.getRowKey().toStringFormat(true);
            iterator.setCurrentRowWithKey(keyStr);
            //get selected node type information
            JUCtrlHierTypeBinding typeBinding = nodeBinding.getHierTypeBinding();
            //The tree node rule may have a target iterator defined. Target
            //iterators are configured using the Target Data Source entry in
            //the tree node edit dialog
            //and allow developers to declaratively synchronize an independent
            //iterator binding with the node selection in the tree.
            //
            nodeStuctureDefname = nodeBinding.getHierTypeBinding().getStructureDefName();
            String targetIteratorSpelString = typeBinding.getTargetIterator();

            //chances are that the target iterator option is not configured. We
            //avoid NPE by checking this condition

            if (targetIteratorSpelString != null && !targetIteratorSpelString.isEmpty()) {

                //resolve SPEL string for target iterator
                DCIteratorBinding targetIterator = resolveTargetIterWithSpel(targetIteratorSpelString);
                //synchronize the row in the target iterator
                Key rowKey = nodeBinding.getCurrentRow().getKey();
                if (rowKey != null && targetIterator != null) {
                    targetIterator.setCurrentRowWithKey(rowKey.toStringFormat(true));
                }

            }
        }
        return nodeStuctureDefname;
    }


    private DCIteratorBinding resolveTargetIterWithSpel(String spelExpr) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ELContext elctx = fctx.getELContext();
        ExpressionFactory elFactory = fctx.getApplication().getExpressionFactory();
        ValueExpression valueExpr = elFactory.createValueExpression(elctx, spelExpr, Object.class);
        DCIteratorBinding dciter = (DCIteratorBinding) valueExpr.getValue(elctx);
        return dciter;
    }

    /**
     * Custom managed bean method that takes a SelectEvent input argument
     * to generically set the current row corresponding to the selected row
     * in the table.
     * @param selectionEvent
     */
    public static void makeCurrent(SelectionEvent selectionEvent) {
        RichTable _table = (RichTable) selectionEvent.getSource();
        CollectionModel _tableModel = (CollectionModel) _table.getValue();
        JUCtrlHierBinding _adfTableBinding = (JUCtrlHierBinding) _tableModel.getWrappedData();
        DCIteratorBinding _tableIteratorBinding = _adfTableBinding.getDCIteratorBinding();
        Object _selectedRowData = _table.getSelectedRowData();
        JUCtrlHierNodeBinding _nodeBinding = (JUCtrlHierNodeBinding) _selectedRowData;
        Key _rwKey = _nodeBinding.getRowKey();
        _tableIteratorBinding.setCurrentRowWithKey(_rwKey.toStringFormat(true));
    }

    public void markDirty(String s) {
        Map<String, Object> scope = null;
        if ("VIEW".equals(s))
            scope = AdfFacesContext.getCurrentInstance().getViewScope();
        else
            scope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        ControllerContext ctx = ControllerContext.getInstance();
        ctx.markScopeDirty(scope);
    }

    public static boolean contains(List<Object> list, Object iCode) {
        boolean b = list.contains(iCode);
        return b;
    }

    public void clearFilter(RichTable t) {
        if (t != null) {
            FilterableQueryDescriptor queryDescriptor = (FilterableQueryDescriptor) t.getFilterModel();
            if (queryDescriptor != null && queryDescriptor.getFilterCriteria() != null) {
                queryDescriptor.getFilterCriteria().clear();
                t.queueEvent(new QueryEvent(t, queryDescriptor));
            }
        }
    }

    public static int size(List<Object> list) {
        if (list != null)
            return list.size();
        return 0;
    }

    public String getNameFromOneChoice(RichSelectOneChoice rsoc) {
        List childList = rsoc.getChildren();
        for (int i = 0; i < childList.size(); i++) {
            if (childList.get(i) instanceof UISelectItems) {
                UISelectItems csi = (UISelectItems) childList.get(i);
                List l = (List) csi.getValue();
                for (int j = 0; j < l.size(); j++) {
                    SelectItem si = (SelectItem) l.get(j);
                    if ((rsoc.getValue().toString().equals(si.getValue().toString())))
                        return ((si.getLabel()).replace('/', '_').replace(' ', '_'));

                }
            }
        }
        return "";
    }

    /**
     * Locate an UIComponent from its root component.
     * @param base root Component (parent)
     * @param id UIComponent id
     * @return UIComponent object
     */
    public static UIComponent findComponent(UIComponent base, String id) {
        if (id.equals(base.getId()))
            return base;

        UIComponent children = null;
        UIComponent result = null;
        Iterator childrens = base.getFacetsAndChildren();
        while (childrens.hasNext() && (result == null)) {
            children = (UIComponent) childrens.next();
            if (id.equals(children.getId())) {
                result = children;
                break;
            }
            result = findComponent(children, id);
            if (result != null) {
                break;
            }
        }
        return result;
    }

    public static UIComponent findParentComponent(UIComponent base) {
        UIComponent result = null;
        String clientIDStr = base.getClientId();
         int regionStartIndx  = clientIDStr.indexOf("region");
        int regionValidateIndx = clientIDStr.indexOf(":",regionStartIndx);
       
        if (regionValidateIndx == -1) {
            result = base;

        } else {
            if (base.getParent() != null) {
                result = findParentComponent(base.getParent());
            } else {
                result = base;
            }
        }
        //System.out.println("Inside UIComponent: "+base.getClientId() +" returns: "+result.getClientId());
        return result;
    }

}
