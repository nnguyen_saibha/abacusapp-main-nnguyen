package gov.ofda.abacus.listener;


import gov.ofda.abacus.model.module.LOVModuleImpl;


import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;
import oracle.jbo.client.Configuration;


public class AbacusApplicationListener implements ServletContextListener {
    private static ADFLogger logger = ADFLogger.createADFLogger(AbacusApplicationListener.class);

    private ServletContext servletContext;

    public AbacusApplicationListener() {
        super();
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        logger.info("Initializing Application Context");
        String amDef = "gov.ofda.abacus.model.module.LOVModule";
        String config = "LOVModuleLocal";
        Row row = null;
        ViewObject svo = null;
        Map<String, String> instanceMap = null;
        LOVModuleImpl lovModule = null;
        ApplicationModule am = Configuration.createRootApplicationModule(amDef, config);
        try {
            lovModule = (LOVModuleImpl)am;
            instanceMap = lovModule.retrieveInstanceAttributes();
            if (instanceMap != null) {

                for (Map.Entry<String, String> entry : instanceMap.entrySet()) {
                    servletContext.setAttribute((String)entry.getKey(), entry.getValue());
                }
            }

        } finally {
            Configuration.releaseRootApplicationModule(am, true);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        logger.info("Release Application context");
    }
}
