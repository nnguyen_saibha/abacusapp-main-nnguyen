package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.base.JSFUtils;

import java.io.Serializable;

import java.util.List;

import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class LookupManagerBean implements Serializable{
    @SuppressWarnings("compatibility:-601441031968857365")
    private static final long serialVersionUID = 1L;
    private String lookupName="INDICATOR_LOOKUP";
    public LookupManagerBean() {
    }

    public void setUserInfo() {
        String instanceName =
            (String)AdfFacesContext.getCurrentInstance().getPageFlowScope().get("instance");
        if (instanceName != null) {           
           JSFUtils.setManagedBeanValue("sessionScope.dataSource", "jdbc/abacustestDS");
        }
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding operationBinding = bindings.getOperationBinding("setUserInfo");
        //operationBinding.getParamsMap().put("userId",FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userID"));
        operationBinding.getParamsMap().put("userId",ADFContext.getCurrent().getSecurityContext().getUserName().toUpperCase());
        List roles = (List)operationBinding.execute();
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userRoles", roles);
        //Get lookup name
        lookupName=(String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("lookupName");
    }

    public String getLookupName() {
        return lookupName;
    }
}
