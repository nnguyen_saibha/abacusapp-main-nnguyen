package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.base.UIControl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.binding.OperationBinding;

import oracle.jbo.domain.DBSequence;

public class ManageRestrictedDocs  extends UIControl implements Serializable{
    @SuppressWarnings("compatibility:9032188957569164572")
    private static final long serialVersionUID = 1L;
    private List<Long> selectedList=new ArrayList<Long>();

    public List<Long> getSelectedList() {
        return selectedList;
    }

    public ManageRestrictedDocs() {
        super();
    }

    public void selectFileChgLsnr(ValueChangeEvent vce) {
        Map attrs=vce.getComponent().getAttributes();
        Long id=((DBSequence)attrs.get("DocId")).getValue();
       if((Boolean)vce.getNewValue()) {
           selectedList.add(id);
       }
       else {
           selectedList.remove(id);
       }
    }
   public boolean isEnableSelectDownloadFiles() {
       if(selectedList.size()>0)
          return true;
       return false;   
   }

    public void saveChanges(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Commit");
        exec.execute();
        if (exec.getErrors().isEmpty()) {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully",
                                 "Changes saved successfully.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void cancelChanges(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Rollback");
        exec.execute();
        if (exec.getErrors().isEmpty()) {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Changes cancelled",
                                 "Changes cancelled.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
}
