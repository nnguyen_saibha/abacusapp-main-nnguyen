package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.base.UIControl;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.RichPopup;

public class EditSubSectorBean extends UIControl implements Serializable {
    @SuppressWarnings("compatibility:-5301156676861788668")
    private static final long serialVersionUID = 5627578448220504019L;

    public EditSubSectorBean() {
        super();
    }
    public String showAddorRemoveIndicatorPopup() {
        // Add event code here...
        FacesContext fctx = FacesContext.getCurrentInstance();
        UIViewRoot root = fctx.getViewRoot();
        RichPopup popup = (RichPopup) this.findComponent(root, "p6");
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
        return null;
    }
    
    public void showAddorRemoveIndicatorPopup(ActionEvent actionEvent) {
        // Add event code here...
        FacesContext fctx = FacesContext.getCurrentInstance();
        UIComponent base = null;
        base = this.findParentComponent(actionEvent.getComponent());
        
        UIViewRoot root = fctx.getViewRoot();
        String clientIDStr = base.getClientId();
        int regionStartIndx  = clientIDStr.indexOf("region");
        int regionValidateIndx = clientIDStr.indexOf(":",regionStartIndx);
        //System.out.println("Component regionValidateIndx: "+regionValidateIndx);
        if (regionValidateIndx == -1) {
           
           base =  root;
        }
        RichPopup popup = (RichPopup) this.findComponent(base, "p6");
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);       
    }
    
    public void showAddorRemoveOldIndicatorPopup(ActionEvent actionEvent) {
        // Add event code here...
        FacesContext fctx = FacesContext.getCurrentInstance();
        UIComponent base = null;
        base = this.findParentComponent(actionEvent.getComponent());
        
        UIViewRoot root = fctx.getViewRoot();
        String clientIDStr = base.getClientId();
        int regionStartIndx  = clientIDStr.indexOf("region");
        int regionValidateIndx = clientIDStr.indexOf(":",regionStartIndx);
        //System.out.println("Component regionValidateIndx: "+regionValidateIndx);
        if (regionValidateIndx == -1) {
           
           base =  root;
        }
        RichPopup popup = (RichPopup) this.findComponent(base, "p22");
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);       
    }
}
