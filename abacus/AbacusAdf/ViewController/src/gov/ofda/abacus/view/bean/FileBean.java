package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.base.UIControl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import oracle.adf.controller.ControllerContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;

import oracle.adf.view.rich.component.rich.data.RichListView;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.RegionNavigationEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.BlobDomain;

import oracle.jbo.domain.DBSequence;

import org.apache.myfaces.trinidad.model.UploadedFile;

/**
 * FileBean.java
 * Purpose: To handle file uploads and downloads.
 * @author Kartheek Atluri
 * @version 1.0 09/09/2012
 */
public class FileBean extends UIControl {
    private RichTable uploadFileTable;

    public FileBean() {
        super();
    }

    /**
     * Used to download file from a blobdomain.
     * @param facesContext
     * @param outputStream
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    public void DownloadFile(FacesContext facesContext,
                             java.io.OutputStream outputStream) throws UnsupportedEncodingException, IOException {
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        Map requestScope = ADFContext.getCurrent().getRequestScope();
        if (requestScope.get("viewName") != null && requestScope.get("colName") != null) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding(requestScope.get("viewName").toString());
            Row r = iter.getCurrentRow();
            BlobDomain bd = (BlobDomain) r.getAttribute(requestScope.get("colName").toString());
            byte[] content;
            if (bd != null) {
                try {
                    content = bd.toByteArray();
                    ServletOutputStream out;
                    response.setHeader("Content-disposition",
                                       "attachment;filename=\"" +
                                       r.getAttribute(requestScope.get("fNameCol").toString()) + "\"");
                    response.setContentType("application/x-download");
                    out = response.getOutputStream();
                    out.write(content);
                    out.flush();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Converts a UploadedFile to BlobDomain
     * @param file
     * @return
     */
    private BlobDomain bDomain(UploadedFile file) {
        BlobDomain nfile = null;
        try {
            InputStream istream = file.getInputStream();
            long fsize = file.getLength();
            byte[] buffer = new byte[(int) file.getLength()];
            for (int i = 0; i < fsize; i++) {
                istream.read(buffer, i, 1);
            }
            istream.close();
            nfile = new BlobDomain(buffer);
        } catch (Exception e) {
        }
        return nfile;
    }


    /**
     * @param dialogEvent
     */
    public void UploadFilesDialogLnsr(DialogEvent dialogEvent) {
        if (DialogEvent.Outcome.ok == dialogEvent.getOutcome()) {
            Map requestScope = ADFContext.getCurrent().getRequestScope();
            if(requestScope.get("uploadFiles") != null)
            {
            List<UploadedFile> files=new ArrayList<UploadedFile>();
            if("java.util.ArrayList".equals(requestScope.get("uploadFiles").getClass().getName()))
               files = (List<UploadedFile>) requestScope.get("uploadFiles");
            else
               files.add((UploadedFile)requestScope.get("uploadFiles"));
            String id = ((Number) requestScope.get("id")).toString();
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = bindings.getOperationBinding("uploadFiles");
            exec.getParamsMap().put("files", files);
            exec.getParamsMap().put("ActionID", id);
            Boolean res = (Boolean) exec.execute();
            if (exec.getErrors().isEmpty() && res) {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "File(s) uploaded",
                                     "File(s) uploaded successfully and saved.");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
            else {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "File(s) upload failed",
                                     "File(s) upload failed.");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
            exec = bindings.getOperationBinding("ExecuteDocs");
            exec.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(uploadFileTable);
        }
        }
    }

    public void setUploadFileTable(RichTable uploadFileTable) {
        this.uploadFileTable = uploadFileTable;
    }

    public RichTable getUploadFileTable() {
        return uploadFileTable;
    }

    /**
     * @param vce
     */
    public void uploadFileChgLsnr(ValueChangeEvent vce) {
        UIComponent c = vce.getComponent();
        Map requestScope = c.getAttributes();
        if (requestScope.get("viewName") != null && requestScope.get("colName") != null) {
            UploadedFile myfile = (UploadedFile) vce.getNewValue();
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding(requestScope.get("viewName").toString());
            Row r = iter.getCurrentRow();
            if (r != null && myfile != null) {
                String filename = myfile.getFilename();
                String content = myfile.getContentType();
                r.setAttribute(requestScope.get("fNameCol").toString(), filename);
                r.setAttribute(requestScope.get("colContentType").toString(), content);
                r.setAttribute(requestScope.get("colName").toString(), bDomain(myfile));
                AdfFacesContext.getCurrentInstance().addPartialTarget(vce.getComponent().getParent().getParent());
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Uploaded Successfully",
                                     "File uploaded successfully, please save changes.");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        }
    }

    /**
     * Used to download file from a blobdomain.
     * @param facesContext
     * @param outputStream
     * @throws UnsupportedEncodingException
     * @throws IOException
     * http://stackoverflow.com/questions/15962172/using-a-servlet-how-do-you-download-multiple-files-from-a-database-and-zip-them
     */
    public void DownloadAllFiles(FacesContext facesContext,
                                 java.io.OutputStream outputStream) throws UnsupportedEncodingException, IOException {
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        Map requestScope = ADFContext.getCurrent().getRequestScope();
        if (requestScope.get("viewName") != null && requestScope.get("colName") != null) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding(requestScope.get("viewName").toString());
            response.setContentType("application/x-download");
            response.setHeader("Content-Disposition", "attachment; filename=\""+requestScope.get("zipName")+"\"");
            ZipOutputStream output = null;
            RowSet rs = getRowSet(requestScope.get("viewName").toString());
            Row r = rs.first();
            output = new ZipOutputStream(new BufferedOutputStream(response.getOutputStream(), 8192));
            byte[] buffer = new byte[8192];
            List selectedList=(List)requestScope.get("selectedList");
            while (r != null) {
                BlobDomain bd = (BlobDomain) r.getAttribute(requestScope.get("colName").toString());
                Long id=((DBSequence)r.getAttribute("DocId")).getValue();
                if (bd != null && (selectedList==null || selectedList.contains(id))) {
                    InputStream input = null;
                    try {
                        input = new BufferedInputStream(bd.getBinaryStream(), 8192);
                        String fileName = (String) r.getAttribute(requestScope.get("fNameCol").toString());
                        output.putNextEntry(new ZipEntry("AB"+id+"_"+fileName));
                        for (int length = 0; (length = input.read(buffer)) > 0;) {
                            output.write(buffer, 0, length);
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } finally {
                        input.close();
                        output.closeEntry();
                    }
                }
                r = rs.next();
            }

            output.close();
        }
    }

    private RowSet getRowSet(String viewNameIter) {
        BindingContext lBindingContext = BindingContext.getCurrent();
        BindingContainer lBindingContainer = lBindingContext.getCurrentBindingsEntry();
        DCIteratorBinding iterBind = (DCIteratorBinding) lBindingContainer.get(viewNameIter);
        ViewObject vo = null;
        if (iterBind.hasRSI()) //if RowSetITerator is present use it's rowset to get the VO
            vo = iterBind.getRowSetIterator().getRowSet().getViewObject();
        else // use the binding itself
            vo = iterBind.getViewObject();
        // create a secondary rowset to iterate over all rows
        RowSet rs=null;
        if(vo.findRowSet("GetAll")==null)
            rs = vo.createRowSet("GetAll");
        else
            rs=vo.findRowSet("GetAll");
        rs.executeQuery();
        return rs;
    }

}
