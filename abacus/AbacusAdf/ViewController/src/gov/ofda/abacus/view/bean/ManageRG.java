package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.base.JSFUtils;
import gov.ofda.abacus.base.Shuttle;
import gov.ofda.abacus.base.UIControl;

import java.io.Serializable;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichSelectManyShuttle;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaRow;
import oracle.jbo.ViewObject;

import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.util.ComponentReference;

/**
 * ManageRG.java
 * Purpose: Bean for rg-manager-taskflow and manageRG.jsf
 * @author Kartheek Atluri
 * @version 1.0 12/01/2012
 */
public class ManageRG extends UIControl implements Serializable{
    @SuppressWarnings("compatibility:-5362423975367748720")
    private static final long serialVersionUID = 1L;
    private List selectedRG;
    private ComponentReference rgTable; //RichTable
    private ComponentReference rgShuttle; //RichSelectManyShuttle
    public ManageRG() {
        super();
    }

    /**
     * Called from rg-manager-taskflow, it sets the instance, action Id, user Id
     * and calls setUserInfo which sets the user roles.
     */
    public void setActionID() {
        String instanceName =
            (String)AdfFacesContext.getCurrentInstance().getPageFlowScope().get("instance");
        if (instanceName != null) {           
           JSFUtils.setManagedBeanValue("sessionScope.dataSource", "jdbc/abacustestDS");
        }
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding operationBinding = bindings.getOperationBinding("setActionID");
        operationBinding.getParamsMap().put("actionID",
                                            AdfFacesContext.getCurrentInstance().getPageFlowScope().get("actionID"));
        operationBinding.execute();
        operationBinding = bindings.getOperationBinding("setUserInfo");
        //operationBinding.getParamsMap().put("userId",FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userID"));
        operationBinding.getParamsMap().put("userId",ADFContext.getCurrent().getSecurityContext().getUserName().toUpperCase());
        List roles = (List)operationBinding.execute();
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userRoles", roles);
    }

    /**
     * Called by Cancel, which calls Rollback to undo all changes made.
     * It then sets the action ID for PA by calling setActionID in PAModuleImpl
     * @param actionEvent
     */
    public void cancelChanges(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding operationBinding = bindings.getOperationBinding("Rollback");
        operationBinding.execute();
        operationBinding = bindings.getOperationBinding("setActionID");
        operationBinding.getParamsMap().put("actionID",
                                            AdfFacesContext.getCurrentInstance().getPageFlowScope().get("actionID"));
        operationBinding.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getRgTable());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getRgShuttle());
    }

    /**
     * Setter for selectedRG. Any changes made to shuttle values are set here.
     * Here the RG are inserted and/or deleted based on the list selectedRG.
     * @param selectedRG
     */
    public void setSelectedRG(List selectedRG) {
        this.selectedRG = selectedRG;
        Shuttle.setSelected(this.selectedRG, "PaRgView1Iterator", "RgCode",
                                "deleteRG", "createRG");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding operationBinding = bindings.getOperationBinding("ExecuteRG");
        operationBinding.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getRgTable());
    }
    /**
     * Getter for selectedRG. Used in Locations shuttle, this adds all the existing restricted goods
     * into selected list.
     * @return
     */
    public List getSelectedRG() {
        this.selectedRG = Shuttle.getSelected("PaRgView1Iterator", "RgCode");
        return this.selectedRG;
    }
    
    /**
     * Used by shuttle select items, This add all the available restricted goods for the action and already selected 
     * restricted goods incase they are made inactive in which case they will not be in RgLookupView1Iterator.
     * @return
     */  
    public List getAllRG() {
        return Shuttle.getAll("RgLookupView1Iterator", "PaRgView1Iterator", "RgCode",
                             "RgName");

    }

    /**
     * setter for RG table
     * @param rgTable
     */
    public void setRgTable(UIComponent rgTable) {
        this.rgTable = ComponentReference.newUIComponentReference(rgTable);
        this.markDirty("VIEW");
    }

    /**
     * getter for RG table
     * @return
     */
    public UIComponent getRgTable() {
        return rgTable == null? null : rgTable.getComponent();
    }

    /**
     * setter for RG shuttle
     * @param rgShuttle
     */
    public void setRgShuttle(UIComponent rgShuttle) {
        this.rgShuttle = ComponentReference.newUIComponentReference(rgShuttle);
        this.markDirty("VIEW");
    }

    /**
     * getter for RG shuttle
     * @return
     */
    public UIComponent getRgShuttle() {
        return rgShuttle == null? null: rgShuttle.getComponent();
    }
}
