package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.base.JSFUtils;

import gov.ofda.abacus.model.ResultMessage;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.layout.RichPanelTabbed;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichCommandToolbarButton;
import oracle.adf.view.rich.component.rich.output.RichOutputFormatted;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.event.ReturnEvent;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;
/**
 * WelcomeBean.java
 * Purpose: Main bean to handle request for welcome.jspx  and training.jspx page
 * @author Kartheek Atluri
 * @version 1.0 12/01/2012
 */
public class WelcomeBean{
    public WelcomeBean() {
        super();
    }


    /**
     * This is called when the popup return. This will raise an event that applet can capture
     * and enable or refresh its screen
     * @param returnEvent
     */
    public void sectorPopupReturnLsnr(ReturnEvent returnEvent) {
    /*    FacesContext fctx = FacesContext.getCurrentInstance();  
        StringBuilder script = new StringBuilder();
        FacesContext fc=FacesContext.getCurrentInstance();
        fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,"Alert",null));
        script = script.append("document.getElementById(\"forms_applet\").raiseEvent('alert','');");
        Service.getRenderKitService(fctx,ExtendedRenderKitService.class).addScript(fctx,script.toString());*/
    }

    /**
     * Is called when userId is changed. This happens when form call the function using java script.
     * @param valueChangeEvent
     */
    public void userIdChgLsnr(ValueChangeEvent valueChangeEvent) {
        String userId=valueChangeEvent.getNewValue().toString();
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding operationBinding = bindings.getOperationBinding("setUserInfo");
        operationBinding.getParamsMap().put("userId",userId);
        List roles=(List)operationBinding.execute();
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userRoles",roles);
    }

    /**
     * Called by training.jspx page when the test datasource is set to connect to test.
     * @param valueChangeEvent
     */
    public void userIdTrainingChgLsnr(ValueChangeEvent valueChangeEvent) {
        JSFUtils.setManagedBeanValue("sessionScope.dataSource", "jdbc/abacustestDS");
        String userId=valueChangeEvent.getNewValue().toString();
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding operationBinding = bindings.getOperationBinding("setUserInfo");
        operationBinding.getParamsMap().put("userId",userId);
        List roles=(List)operationBinding.execute();
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userRoles",roles);
    }

    public String manageUser() {
        String manageUserID=(String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("manageUserID");
        String manageOperation=(String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("manageOperation");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding operationBinding = bindings.getOperationBinding("manageOIMUser");
        operationBinding.getParamsMap().put("operation",manageOperation);
        operationBinding.getParamsMap().put("userID",manageUserID);
        FacesContext fc=FacesContext.getCurrentInstance();
        String rm=(String)operationBinding.execute();
        if(!"0".equals(rm))
        {
        UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
        UIComponent component=viewRoot.findComponent("pt1:p1");
        RichPopup popup=(RichPopup)component;
        component = viewRoot.findComponent("pt1:ofinfo1");
        RichOutputFormatted rotext=(RichOutputFormatted)component;
        rotext.setValue(rm);
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
        }
       // fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,rm,null));
       /* if(rm.isResult())
            fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,rm.getMessage(),null));
        else
            fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,rm.getMessage(),null));*/
        return null;
    }
}
