package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.base.UIControl;

import java.io.Serializable;

import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGridLayout;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.PopupCanceledEvent;

import oracle.jbo.Key;
import oracle.jbo.NavigatableRowIterator;
import oracle.jbo.Row;
import oracle.jbo.RowIterator;
import oracle.jbo.RowSetIterator;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.component.UIXCollection;
import org.apache.myfaces.trinidad.component.UIXEditableValue;
import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.ComponentReference;
import org.apache.myfaces.trinidad.util.Service;


public class LocationLookupBean extends UIControl implements Serializable{
    @SuppressWarnings("compatibility:1741409787720563201")
    private static final long serialVersionUID = 1L;
    private ComponentReference locationlookupTable; //RichTable
    private ComponentReference LocationTreeTable; //RichTreeTable
    private ComponentReference EditLocationTreeTable; //RichTreeTable
    private ComponentReference locdeleteFailed; //RichPopup
    private ComponentReference locaddPopup; //RichPopup
    private ComponentReference locdeletePopup; //RichPopup
    private ComponentReference locsearchPopup; //RichPopup
    private ComponentReference deleteFailed; //RichPopup
    private String countryCode;
    private String adminLevel1;
    private String adminLevel2;
    private String parentCode;
    private String locationCode;
    private String regionCode;
    private ComponentReference locdisplayForm; //RichPanelFormLayout
    final String OLD_CURR_KEY_VIEWSCOPE_ATTR = "__oldCurrentRowKey__";
    private boolean createFlg;
    private static ADFLogger logger = ADFLogger.createADFLogger(LocationLookupBean.class);
    private ComponentReference locationName; //RichInputText
    private boolean displayCountryTree;
    private boolean addFlag;
    private ComponentReference adminLevel1PLM; //RichPanelLabelAndMessage
    private ComponentReference adminLevel2PLM; //RichPanelLabelAndMessage
    private ComponentReference adminLevel3PLM; //RichPanelLabelAndMessage
    private ComponentReference selectCountry;  //RichPanelLabelAndMessage
    private ComponentReference privilegesPopup; //RichPopup
    List roles = null;
    private ComponentReference adminGridLayout; //RichPanelGridLayout
    private ComponentReference selectAdminLevel; //RichPanelLabelAndMessage

    /**
     * Check if user has role to manage indicator lookup
     * @return
     */
    public boolean getCheckUserRole() {
        SecurityContext context=ADFContext.getCurrent().getSecurityContext();
        if(context.isUserInRole("APP_LOOKUP_OTHER")){
            return true;
        }
        return false;
    }

    /**
     * addLocation is an action Lsnr which creates a new row after the selected row using the row index.
     * @param actionEvent
     */
    public void addLocation(ActionEvent actionEvent) {
        if (getCheckUserRole()) {
            OperationBinding oper1 = null;
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding dciter = (DCIteratorBinding)bindings.get("LocationViewLookupUI1Iterator");
            Row oldCcurrentRow = dciter.getCurrentRow();
            ADFContext adfCtx = ADFContext.getCurrent();
            if (oldCcurrentRow != null)
                adfCtx.getViewScope().put(OLD_CURR_KEY_VIEWSCOPE_ATTR, oldCcurrentRow.getKey().toStringFormat(true));
            NavigatableRowIterator nav = dciter.getNavigatableRowIterator();
            Row newRow = nav.createRow();
            newRow.setNewRowState(Row.STATUS_INITIALIZED);
            Row currentRow = nav.getCurrentRow();
            int currentRowIndex = nav.getRangeIndexOf(currentRow);
            logger.log("currentRowIndex is" + currentRowIndex);
            nav.insertRowAtRangeIndex(currentRowIndex + 1, newRow);
            logger.log("adding row under currentRowIndex " + currentRowIndex);
            dciter.setCurrentRowWithKey(newRow.getKey().toStringFormat(true));
            createFlg = true;
            RichPopup popup = (RichPopup)this.getLocAddPopup();
            displayCountryTree = false;
            addFlag = true;
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            _resetChildren(popup);
            popup.show(ph);
        } else {
            RichPopup popup = (RichPopup)this.getPrivilegesPopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            popup.show(ph);
        }
    }

    /**
     * editLocation  the methods to set the regionCode and countryCode
     * @param actionEvent
     */
    public void editLocation(ActionEvent actionEvent) {
        if (getCheckUserRole()) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding dciter = (DCIteratorBinding)bindings.get("LocationViewLookupUI1Iterator");
            Row oldCcurrentRow = dciter.getCurrentRow();
            ADFContext adfCtx = ADFContext.getCurrent();
            adfCtx.getViewScope().put(OLD_CURR_KEY_VIEWSCOPE_ATTR, oldCcurrentRow.getKey().toStringFormat(true));
            createFlg = false;
            RichPopup popup = (RichPopup)this.getLocAddPopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            OperationBinding exec2 = bindings.getOperationBinding("setRegionCode");
            exec2.getParamsMap().put("regionCode", oldCcurrentRow.getAttribute("RegionCode"));
            exec2.execute();
            logger.log("Region code is set");
            logger.log("region code" + oldCcurrentRow.getAttribute("RegionCode"));
            OperationBinding exec = bindings.getOperationBinding("setLocationCountry");
            exec.getParamsMap().put("countryCode", oldCcurrentRow.getAttribute("CountryCode"));
            parentCode = (String)exec.execute();
            OperationBinding exec1 = bindings.getOperationBinding("setCountryCode");
            exec1.getParamsMap().put("countryCode", oldCcurrentRow.getAttribute("CountryCode"));
            parentCode = (String)exec1.execute();

            displayCountryTree = true;
            addFlag = false;
            popup.show(ph);
        } else {
            RichPopup popup = (RichPopup)this.getPrivilegesPopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            popup.show(ph);
        }
    }

    /**
     *
     * @param actionEvent
     */
    public void deleteLocation(ActionEvent actionEvent) {
        if (getCheckUserRole()) {
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLocDeletePopup());
            RichPopup popup = (RichPopup)this.getLocDeletePopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            popup.show(ph);

        } else {
            RichPopup popup = (RichPopup)this.getPrivilegesPopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            popup.show(ph);
        }
    }

   
    /**
     * addLocationDialogLnsr
     * @param dialogEvent
     */
    public void addLocationDialogLnsr(DialogEvent dialogEvent) {
        logger.log("Inside: " + dialogEvent.getOutcome().name());
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            DCIteratorBinding iter = bindings.findIteratorBinding("LocationViewLookupUI1Iterator");
            Row r = iter.getCurrentRow();
            ADFContext adfCtx = ADFContext.getCurrent();
            logger.log("Location name: " + r.getAttribute("LocationName"));
            logger.log("parentcode " + parentCode);
            if(parentCode != null && r.getAttribute("ParentLocationCode")==null){
                r.setAttribute("ParentLocationCode", parentCode);
            }
          
            OperationBinding exec = null;
            exec = bindings.getOperationBinding("Commit");
            exec.execute();
            exec = bindings.getOperationBinding("Execute");
            exec.execute();
            try {
                iter.setCurrentRowWithKey(r.getKey().toStringFormat(true));
            } catch (oracle.jbo.RowNotFoundException e) {
            }
            logger.log("the location" + r.getKey().toString());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLocationLookupTable());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLocDisplayForm());
        }
    }

    public RowIterator getSelectedNodeRowIterator() {
        if (getLocationTreeTable() != null && ((RichTreeTable)getLocationTreeTable()).getSelectedRowKeys() != null) {
            for (Object opaqueFacesKey : ((RichTreeTable)getLocationTreeTable()).getSelectedRowKeys()) {
                ((RichTreeTable)getLocationTreeTable()).setRowKey(opaqueFacesKey);
                return ((JUCtrlHierNodeBinding)((RichTreeTable)getLocationTreeTable()).getRowData()).getRowIterator();
            }
        }
        return null;
    }

    public Key getSelectedNodeRowKey() {
        if (getLocationTreeTable() != null && ((RichTreeTable)getLocationTreeTable()).getSelectedRowKeys() != null) {
            for (Object opaqueFacesKey : ((RichTreeTable)getLocationTreeTable()).getSelectedRowKeys()) {
                ((RichTreeTable)getLocationTreeTable()).setRowKey(opaqueFacesKey);
                return ((JUCtrlHierNodeBinding)((RichTreeTable)getLocationTreeTable()).getRowData()).getRowKey();
            }
        }
        return null;
    }


 

    /**
     *
     * @param dialogEvent
     */
    public void deleteLocationDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding dciter = (DCIteratorBinding)bindings.get("LocationViewLookupUI1Iterator");
            Row row = dciter.getCurrentRow();
            String keyStr=row.getKey().toStringFormat(true);
            OperationBinding exec = bindings.getOperationBinding("checkLocCodeInPASL");
            exec.getParamsMap().put("locationCode", row.getAttribute("LocationCode"));
            String y = (String)exec.execute();
            logger.log("rows exist?" + y);
            if (y.equalsIgnoreCase("yes")) {
                //Delete failed. So show message that delete has failed.
                RichPopup popup = (RichPopup)this.getDeleteFailed();
                RichPopup.PopupHints ph = new RichPopup.PopupHints();
                popup.show(ph);
            } else {
                OperationBinding exec1 = bindings.getOperationBinding("Delete");
                exec1.execute();
                exec1 = bindings.getOperationBinding("Commit");
                exec1.execute();
                if(!exec1.getErrors().isEmpty()) {
                    /*
                     * Delete failed due to foreign key reference in parent_location_id.
                     * Rollback , show error and set the current row.
                     */
                    Throwable t=(Throwable)exec1.getErrors().get(0);
                    exec1 = bindings.getOperationBinding("Rollback");
                    exec1.execute();
                    dciter.setCurrentRowWithKey(keyStr);
                    FacesContext fc=FacesContext.getCurrentInstance();
                    fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_WARN,t.getMessage(),null));
                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLocationLookupTable());
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLocDisplayForm());
            }
        }
    }

    public void deleteFailedDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding dciter = (DCIteratorBinding)bindings.get("LocationViewLookupUI1Iterator");
            Row row = dciter.getCurrentRow();
            row.setAttribute("Active", "N");
            OperationBinding exec1 = null;
            exec1 = bindings.getOperationBinding("Commit");
            exec1.execute();
            exec1 = bindings.getOperationBinding("Execute");
            exec1.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLocationLookupTable());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLocDisplayForm());
            logger.log("The Location is" + row.getAttribute("LocationCode"));
            logger.log("The active field is" + row.getAttribute("Active"));
        }
    }


    /**
     * regionChgLsnr sets the regionCode when ther is a value change.
     * @param valueChangeEvent
     */
    public void regionChgLsnr(ValueChangeEvent valueChangeEvent) {
        logger.log("In Region change Listener");
        logger.log("In Region chg lsnr: " + valueChangeEvent.getNewValue().toString());
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dciter1 = (DCIteratorBinding)bindings.get("LocationViewLookupUI1Iterator");
        OperationBinding exec = bindings.getOperationBinding("setRegionCode");
        exec.getParamsMap().put("regionCode", valueChangeEvent.getNewValue().toString());
        exec.execute();
        Row row = dciter1.getCurrentRow();
        row.setAttribute("CountryCode", null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSelectCountry());
    }

    /**
     * selectCountryDialogLsnr sets the countryCode when country is selected in the popup and clicked on 'Ok'.
     * @param dialogEvent
     */
    public void selectCountryDialogLsnr(DialogEvent dialogEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dciter = (DCIteratorBinding)bindings.get("LocationCountryLookupView1Iterator");
        DCIteratorBinding dciter1 = (DCIteratorBinding)bindings.get("LocationViewLookupUI1Iterator");
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            Row row = dciter.getCurrentRow();
            Row mainrow = dciter1.getCurrentRow();
            mainrow.setAttribute("CountryCode", row.getAttribute("CountryCode"));
            OperationBinding exec = bindings.getOperationBinding("setLocationCountry");
            OperationBinding exec1 = bindings.getOperationBinding("setCountryCode");
            exec.getParamsMap().put("countryCode", row.getAttribute("CountryCode"));
            exec1.getParamsMap().put("countryCode", row.getAttribute("CountryCode"));

            parentCode = (String)exec.execute();
            logger.log("parent" + parentCode);
            parentCode = (String)exec1.execute();
            displayCountryTree = true;
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSelectCountry());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSelectAdminLevel());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAdminGridLayout());
        }
    }

    /**
     * treeSelLsnr gets the location code of the selected node and sets it to the parent location code for the current location.
     * @param selectionEvent
     */
    public void treeSelLsnr(SelectionEvent selectionEvent) {
        this.onTreeTableSelect(selectionEvent);
        RowKeySet rks = ((RichTreeTable)this.getLocationTreeTable()).getSelectedRowKeys();
        Iterator rksIterator = rks.iterator();
        if (rks.iterator().hasNext()) {
            List key = (List)rksIterator.next();
            JUCtrlHierBinding treeTableBinding = null;
            treeTableBinding = (JUCtrlHierBinding)((CollectionModel)((RichTreeTable)this.getLocationTreeTable()).getValue()).getWrappedData();
            JUCtrlHierNodeBinding nodeBinding = treeTableBinding.findNodeByKeyPath(key);
            parentCode = nodeBinding.getRow().getAttribute(0) + "";
            logger.log("RowKey: " + key);
            logger.log("Parent Code in treeselLsnr: " + parentCode);
        }
    }

    /**
     * treeDialogLsnr checks for the location code in main row matching the parent code and sets the admin levels
     * @param dialogEvent
     */
    public void treeDialogLsnr(DialogEvent dialogEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        logger.log("Parent Code in tree is " + parentCode);
        logger.log("Parent Code in get tree " + (getParentCode().toString()));
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            DCIteratorBinding dciter = (DCIteratorBinding)bindings.get("LocationLookup2Admin1Iterator");
            String locationCode = parentCode;
            Key key = new Key(new Object[] { locationCode });
            RowSetIterator rsi = dciter.getRowSetIterator();
            Row row = rsi.findByKey(key, 1)[0];
            rsi.setCurrentRow(row);
            setAdminLevels(row);
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLocAddPopup());
        }
    }

    /**
     * searchLookupLsnr gets the location code of selected row in search popup
     * and sets it to parent code and calls setAdminLevels().
     * @param dialogEvent
     */
    public void searchLookupLsnr(DialogEvent dialogEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dciter1 = (DCIteratorBinding)bindings.get("LocationViewLookupUI1Iterator");
        Row mainrow = dciter1.getCurrentRow();
        DCIteratorBinding dciter = (DCIteratorBinding)bindings.get("LocationLookup2Admin1Iterator");
        Row row = dciter.getCurrentRow();

        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            parentCode = row.getAttribute("LocationCode").toString();
            setAdminLevels(row);
            logger.log("Parent Code is " + parentCode);
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLocAddPopup());
        }
    }

    /**
     * Sets the Admin levels of the main row with the selected row's or node's admin levels of the search lookup or the tree.
     * @param source
     */
    private void setAdminLevels(Row source) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dciter1 = (DCIteratorBinding)bindings.get("LocationViewLookupUI1Iterator");
        Row destinRow = dciter1.getCurrentRow();
        destinRow.setAttribute("editFlag", Boolean.TRUE);
        destinRow.setAttribute("AdminLevel1", source.getAttribute("AdminLevel1"));
        destinRow.setAttribute("AdminLevel2", source.getAttribute("AdminLevel2"));
        destinRow.setAttribute("AdminLevel3", source.getAttribute("AdminLevel3"));
        destinRow.setAttribute("ParentLocationCode", source.getAttribute("LocationCode").toString());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLocAddPopup());
    }

    public String getParentCode() {
        return parentCode;
    }

    /**
     * addPopupCancelListener sets the focus on to the current selected row when clicked on cancel of the dialog.
     * @param popupCanceledEvent
     */
    public void addPopupCancelListener(PopupCanceledEvent popupCanceledEvent) {

        DCBindingContainer bindingContainer =
            (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter =
            (DCIteratorBinding)bindingContainer.findIteratorBinding("LocationViewLookupUI1Iterator");
        Key key = iter.getCurrentRow().getKey();
        logger.log("Entered with Key: " + key.getKeyValues());

        OperationBinding operationBinding = bindingContainer.getOperationBinding("Rollback");
        operationBinding.execute();
        iter = (DCIteratorBinding)bindingContainer.findIteratorBinding("LocationViewLookupUI1Iterator");
        Row currentRow = iter.getCurrentRow();
        ADFContext adfCtx = ADFContext.getCurrent();
        String oldCurrentRowKey = (String)adfCtx.getViewScope().get(OLD_CURR_KEY_VIEWSCOPE_ATTR);
        iter.setCurrentRowWithKey(oldCurrentRowKey);
        RichPopup popup = (RichPopup)this.getLocAddPopup();
        _resetChildren(popup);
    }

    /**
     *
     * @param comp
     */
    private void _resetChildren(UIComponent comp) {

        Iterator<UIComponent> kids = comp.getFacetsAndChildren();
        //logger.log("_resetChildren: " + comp.getClientId());
        while (kids.hasNext()) {
            UIComponent kid = kids.next();

            if (kid instanceof UIXEditableValue) {
                logger.log("UIXEditableValue: " + kid.getClientId() + " " + ((UIXEditableValue)kid).isLocalValueSet() +
                           " Value: " + ((UIXEditableValue)kid).getValue());
                ((UIXEditableValue)kid).resetValue();
                logger.log("UIXEditableValue: " + kid.getClientId() + " " + ((UIXEditableValue)kid).getValue());
                RequestContext.getCurrentInstance().addPartialTarget(kid);
            } else if (kid instanceof EditableValueHolder) {
                _resetEditableValueHolder((EditableValueHolder)kid);
                RequestContext.getCurrentInstance().addPartialTarget(kid);
            } else if (kid instanceof UIXCollection) {
                ((UIXCollection)kid).resetStampState();
                RequestContext.getCurrentInstance().addPartialTarget(kid);
            }
            _resetChildren(kid);
        }
    }

    private void _resetEditableValueHolder(EditableValueHolder evh) {
        evh.setValue(null);
        evh.setSubmittedValue(null);
        evh.setLocalValueSet(false);
        evh.setValid(true);
    }

    private UIComponent _findPopup(UIComponent component) {
        if (component == null)
            return null;

        if (component instanceof RichPopup)
            return component;

        return _findPopup(component.getParent());
    }

    public void setLocationName(UIComponent locationName) {
        this.locationName = ComponentReference.newUIComponentReference(locationName);
        this.markDirty("VIEW");
    }

    public UIComponent getLocationName() {
        return locationName == null ? null : locationName.getComponent();
    }


    public void setDisplayCountryTree(boolean displayCountryTree) {
        this.displayCountryTree = displayCountryTree;
    }

    public boolean isDisplayCountryTree() {
        return displayCountryTree;
    }

    public void setAddFlag(boolean addFlag) {
        this.addFlag = addFlag;
    }

    public boolean isAddFlag() {
        return addFlag;
    }

    public void setAdminLevel1(String adminLevel1) {
        this.adminLevel1 = adminLevel1;
    }

    public String getAdminLevel1() {
        return adminLevel1;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public void setAdminLevel2(String adminLevel2) {
        this.adminLevel2 = adminLevel2;
    }

    public String getAdminLevel2() {
        return adminLevel2;
    }

    public void setLocsearchPopup(UIComponent locsearchPopup) {
        this.locsearchPopup = ComponentReference.newUIComponentReference(locsearchPopup);
        this.markDirty("VIEW");
    }

    public UIComponent getLocsearchPopup() {
        return locsearchPopup == null ? null : locsearchPopup.getComponent();
    }

    public void setAdminLevel1PLM(UIComponent adminLevel1PLM) {
        this.adminLevel1PLM = ComponentReference.newUIComponentReference(adminLevel1PLM);
        this.markDirty("VIEW");
    }

    public UIComponent getAdminLevel1PLM() {
        return adminLevel1PLM == null ? null : adminLevel1PLM.getComponent();
    }

    public void setAdminLevel2PLM(UIComponent adminLevel2PLM) {
        this.adminLevel2PLM = ComponentReference.newUIComponentReference(adminLevel2PLM);
        this.markDirty("VIEW");
    }

    public UIComponent getAdminLevel2PLM() {
        return adminLevel2PLM == null ? null : adminLevel2PLM.getComponent();
    }

    public void setAdminLevel3PLM(UIComponent adminLevel3PLM) {
        this.adminLevel3PLM = ComponentReference.newUIComponentReference(adminLevel3PLM);
        this.markDirty("VIEW");
    }

    public UIComponent getAdminLevel3PLM() {
        return adminLevel3PLM == null ? null : adminLevel3PLM.getComponent();
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setSelectCountry(UIComponent selectCountry) {
        this.selectCountry = ComponentReference.newUIComponentReference(selectCountry);
        this.markDirty("VIEW");
    }

    public UIComponent getSelectCountry() {
        return selectCountry == null ? null : selectCountry.getComponent();
    }

    public void setDeleteFailed(UIComponent deleteFailed) {
        this.deleteFailed = ComponentReference.newUIComponentReference(deleteFailed);
        this.markDirty("VIEW");
    }

    public UIComponent getDeleteFailed() {
        return deleteFailed == null ? null : deleteFailed.getComponent();
    }

    public void setPrivilegesPopup(UIComponent privilegesPopup) {
        this.privilegesPopup = ComponentReference.newUIComponentReference(privilegesPopup);
        this.markDirty("VIEW");
    }

    public UIComponent getPrivilegesPopup() {
        return privilegesPopup == null ? null : privilegesPopup.getComponent();
    }

    public void setAdminGridLayout(UIComponent adminGridLayout) {
        this.adminGridLayout = ComponentReference.newUIComponentReference(adminGridLayout);
        this.markDirty("VIEW");
    }

    public UIComponent getAdminGridLayout() {
        return adminGridLayout == null ? null : adminGridLayout.getComponent();
    }

    public void setSelectAdminLevel(UIComponent selectAdminLevel) {
        this.selectAdminLevel = ComponentReference.newUIComponentReference(selectAdminLevel);
        this.markDirty("VIEW");
    }

    public UIComponent getSelectAdminLevel() {
        return selectAdminLevel == null ? null : selectAdminLevel.getComponent();
    }
    public void setLocationLookupTable(UIComponent locationlookupTable) {
        this.locationlookupTable = ComponentReference.newUIComponentReference(locationlookupTable);
        this.markDirty("VIEW");
    }

    public UIComponent getLocationLookupTable() {
        return locationlookupTable == null ? null : locationlookupTable.getComponent();
    }

    public void setLocDisplayForm(UIComponent locdisplayForm) {
        this.locdisplayForm = ComponentReference.newUIComponentReference(locdisplayForm);
        this.markDirty("VIEW");
    }

    public UIComponent getLocDisplayForm() {
        return locdisplayForm == null ? null : locdisplayForm.getComponent();
    }

    public void setLocDeleteFailed(UIComponent locdeleteFailed) {
        this.locdeleteFailed = ComponentReference.newUIComponentReference(locdeleteFailed);
        this.markDirty("VIEW");
    }

    public UIComponent getLocDeleteFailed() {
        return locdeleteFailed == null ? null : locdeleteFailed.getComponent();
    }
    public void setLocAddPopup(UIComponent locaddPopup) {
        this.locaddPopup = ComponentReference.newUIComponentReference(locaddPopup);
        this.markDirty("VIEW");
    }

    public UIComponent getLocAddPopup() {
        return locaddPopup == null ? null : locaddPopup.getComponent();
    }


    public void setLocationTreeTable(UIComponent LocationTreeTable) {
        this.LocationTreeTable = ComponentReference.newUIComponentReference(LocationTreeTable);
        this.markDirty("VIEW");
    }

    public UIComponent getLocationTreeTable() {
        return LocationTreeTable == null ? null : LocationTreeTable.getComponent();
    }

    public void setLocDeletePopup(UIComponent locdeletePopup) {
        this.locdeletePopup = ComponentReference.newUIComponentReference(locdeletePopup);
        this.markDirty("VIEW");
    }

    public UIComponent getLocDeletePopup() {
        return locdeletePopup == null ? null : locdeletePopup.getComponent();
    }

    public void setEditLocationTreeTable(UIComponent EditLocationTreeTable) {
        this.EditLocationTreeTable = ComponentReference.newUIComponentReference(EditLocationTreeTable);
        this.markDirty("VIEW");
    }

    public UIComponent getEditLocationTreeTable() {
        return EditLocationTreeTable == null ? null : EditLocationTreeTable.getComponent();
    }

    public void setCreateFlg(boolean createFlg) {
        this.createFlg = createFlg;
    }

    public boolean isCreateFlg() {
        return createFlg;
    }
    
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

}

