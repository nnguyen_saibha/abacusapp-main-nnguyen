package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.base.Shuttle;

import gov.ofda.abacus.base.UIControl;

import gov.ofda.abacus.model.LovList;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.controller.ControllerContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;

import oracle.jbo.Row;

import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.event.DisclosureEvent;
import org.apache.myfaces.trinidad.event.ReturnEvent;
import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.util.ComponentReference;

/**
 * EditSectorBean.java
 * Purpose: Bean for sector-edit-taskflow and editSector.jsff
 * @author Kartheek Atluri
 * @version 1.0 12/01/2012
 */
public class EditSectorBean extends UIControl implements Serializable{
    @SuppressWarnings("compatibility:4327458632583776388")
    private static final long serialVersionUID = 1L;
    private List selectedKeywords;
    private ComponentReference addLocationsPopup;
    private ComponentReference locationsTable;
    private ComponentReference keywordTable;
    
    private ComponentReference locViewLocationTable;
    private ComponentReference locSelectedListTable;
    private ComponentReference locListAddRemCol;
    private List<Long> manageLocList=new ArrayList<Long>();
    private List<LovList> locationLovList=new ArrayList<LovList>();

    public EditSectorBean() {
        super();
    }

    /**
     * Called from sector-edit-taskflow, this method sets
     * the current row for sector and calls setLocationView in PAModuleImpl which sets the LocationLookup view 
     * for Locations table (without setting this, the page is slow as it is loading all locations for LOV values
     *  for Admin levels 1, 2, and 3).
     */
    public void setSectorDetails() {
                   DCBindingContainer bindingContainer =
                       (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
                   OperationBinding ob = null;
                   ob = bindingContainer.getOperationBinding("Execute");
                   ob.execute();
                   ob = bindingContainer.getOperationBinding("setCurrentRowWithKey");
                   ob.execute();
                   ob = bindingContainer.getOperationBinding("setLocationRegionCountry");
                   ob.execute();
                   OperationBinding getLovList = bindingContainer.getOperationBinding("getLocationsLovList");
                   locationLovList=(List<LovList>)getLovList.execute();
                   getLovList = bindingContainer.getOperationBinding("getLocationsList");
                                      manageLocList=(List<Long>)getLovList.execute();
                   
               }
    //Start Keyword functions

    /**
     * Getter for selectedKeywords. Used in Keywords shuttle, this adds all the existing keywords 
     * into selected list.
     * @return
     */
    public List getSelectedKeywords() {
        this.selectedKeywords = Shuttle.getSelected("PasKeywordsView2Iterator", "KeywordCode");
        return this.selectedKeywords;
    }

    /**
     * Setter for selectedKeywords. Any changes made to shuttle values are set here.
     * @param selectedValues
     */
    public void setSelectedKeywords(List selectedValues) {
        this.selectedKeywords = selectedValues;
        /*    Shuttle.setSelected(this.selectedKeywords, "PasKeywordsView2Iterator", "KeywordCode",
                                "deleteKeyword", "createKeyword");*/
    }

    /**
     * Used by shuttle select items, This add all the available keywords for the sector and already selected 
     * keywords incase they are made inactive in which case they will not be in SectorKeywordLinkView2Iterator.
     * @return
     */
    public List getAllKeywords() {
        return Shuttle.getAll("SectorKeywordLinkView2Iterator", "PasKeywordsView2Iterator", "KeywordCode",
                              "KeywordName");
    }

    /**
     * Dialog listener for keywords.
     * Here the keywords are inserted and/or deleted based on the list selectedKeywords.
     * @param dialogEvent
     */
    public void keywordMgrDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            Shuttle.setSelected(this.selectedKeywords, "PasKeywordsView2Iterator", "KeywordCode", "deleteKeyword",
                                "createKeyword");
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getKeywordTable().getParent());
        }
    }
    //End Keyword functions

    /**
     * setter for keyword table
     * @param keywordTable
     */
    public void setKeywordTable(UIComponent keywordTable) {
        this.keywordTable = ComponentReference.newUIComponentReference(keywordTable);
        this.markDirty("PAGEFLOW");
    }

    /**
     * getter for keyword table
     * @return
     */
    public UIComponent getKeywordTable() {
        return keywordTable == null ? null : keywordTable.getComponent();
    }
    /**
     * setter method for add locations popup
     * Table type add location popup binding that can be used as advanced search where users can search by levels, countries and regions.
     * This is not used as of V13.1 release. 
     * @param addLocationsPopup
     */
    public void setAddLocationsPopup(UIComponent addLocationsPopup) {
        this.addLocationsPopup = ComponentReference.newUIComponentReference(addLocationsPopup);
        this.markDirty("PAGEFLOW");
    }

    /**
     * Setter for PAS locations table in sector screen
     * @param locationsTable
     */
    public void setLocationsTable(UIComponent locationsTable) {
        this.locationsTable = ComponentReference.newUIComponentReference(locationsTable);
        this.markDirty("PAGEFLOW");
    }

    /**
     * getter for PAS locations table in sector screen
     * @return
     */
    public UIComponent getLocationsTable() {
        return locationsTable == null ? null : locationsTable.getComponent();
        
    }

    /****************************************************Start Manage Locations******************************
         * @return
         * Called when user clicks on add from Manage Locations popup
         * Will add the location code to selected location list.
         */
        public String addtoLocList() {
            Long locationCode=(Long)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("LocationCode");
            String locationName=(String)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("LocationName");
            Integer level=(Integer)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("Level");
            String regionName=(String)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("RegionName");
            String countryName=(String)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("CountryName");
            String adminLevel1=(String)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("AdminLevel1");
            String adminLevel2=(String)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("AdminLevel2");
            String adminLevel3=(String)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("AdminLevel3");
            if(!manageLocList.contains(locationCode))
                {
                 manageLocList.add(locationCode);
                 locationLovList.add(new LovList(locationCode,locationName,level,regionName,countryName,adminLevel1,adminLevel2,adminLevel3));
                }
            AdfFacesContext.getCurrentInstance().addPartialTarget(locListAddRemCol.getComponent());
            AdfFacesContext.getCurrentInstance().addPartialTarget(locSelectedListTable.getComponent());
            return null;
        }

        /**
         * @return
         * Called when user clicks on remove from Manage Locations popup
         * Will remove the location code from selected location list.
         */
        public String removeFromLocList() {
            Long locationCode=(Long)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("LocationCode");
            if(manageLocList.contains(locationCode))
             manageLocList.remove(locationCode);
            
            AdfFacesContext.getCurrentInstance().addPartialTarget(locListAddRemCol.getComponent());
            AdfFacesContext.getCurrentInstance().addPartialTarget(locSelectedListTable.getComponent());
            return null;
        }

        /**
         * @param popupFetchEvent
         * Called when Manage Location popup is fetched.
         * It will get the existing location list and add the selected location list.
         */
        public void manageLocFetchLsnr(PopupFetchEvent popupFetchEvent) {
            // Add event code here...getLocationsList
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding getList = bindings.getOperationBinding("getLocationsList");
            manageLocList=(List<Long>)getList.execute();
            /*OperationBinding getLovList = bindings.getOperationBinding("getLocationsLovList");
            locationLovList=(List<LovList>)getLovList.execute();*/
            
            AdfFacesContext.getCurrentInstance().addPartialTarget(locViewLocationTable.getComponent());
            AdfFacesContext.getCurrentInstance().addPartialTarget(locSelectedListTable.getComponent());
        }


        /**
         * @param popupCanceledEvent
         * Called when popup is cancelled or closed.
         * It will clear the filter for select location table
         */
        public void manageLocCancelLsnr(PopupCanceledEvent popupCanceledEvent) {
            clearFilter((RichTable)locViewLocationTable.getComponent());
        }

        /**
         * @param dialogEvent
         * Called when user clicks on ok or cancel dialog for manage locations
         * It will send the selected to list to Model operation (updateLocationsInSector) to update the location list.
         * Once updated. It will commit changes and run execute on location iterator and set the row to selected row.
         */
        public void manageLocDialogLnsr(DialogEvent dialogEvent) {
            if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {

                DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
                //Get current row key to keep track of currrent row.
                DCIteratorBinding iter = (DCIteratorBinding)bindings.findIteratorBinding("PasLocationsView1Iterator");
                Key key=null;
                if (iter != null && iter.getCurrentRow() != null)
                    key = iter.getCurrentRow().getKey();
                //call updateLocationsInSector operation with selected location list.
               OperationBinding ctrl = bindings.getOperationBinding("updateLocations");
                ctrl.getParamsMap().put("locList", this.getManageLocList());
                Boolean t=(Boolean)ctrl.execute();
                //If changes are made commit changes.

                    //Call execute on location iterator and set the current row to previous row.
                    if (this.getLocationsTable() != null) {
                         OperationBinding exec = bindings.getOperationBinding("ExecutePasLocations");
                         exec.execute();
                        if(key!=null) {
                            iter = (DCIteratorBinding)bindings.findIteratorBinding("PasLocationsView1Iterator");
                            RowSetIterator rsi = iter.getRowSetIterator();
                            if (rsi.findByKey(key, 1).length > 0)
                                iter.setCurrentRowWithKey(key.toStringFormat(true));
                        }
                        AdfFacesContext.getCurrentInstance().addPartialTarget(locationsTable.getComponent().getParent());
                     }
                
            }
            //Clear select location filter.
            if(locViewLocationTable!=null)
                clearFilter((RichTable)locViewLocationTable.getComponent());
        }

        
        public void setLocViewLocationTable(UIComponent locViewLocationTable) {
            this.locViewLocationTable = ComponentReference.newUIComponentReference(locViewLocationTable);
        }

        public UIComponent getLocViewLocationTable() {
            return locViewLocationTable == null ? null : locViewLocationTable.getComponent();
        }

        public void setManageLocList(List<Long> manageLocList) {
            this.manageLocList = manageLocList;
        }

        public List<Long> getManageLocList() {
            return manageLocList;
        }
        public void setLocListAddRemCol(UIComponent locListAddRemCol) {
            this.locListAddRemCol = ComponentReference.newUIComponentReference(locListAddRemCol);
        }

        public UIComponent getLocListAddRemCol() {
            return locListAddRemCol == null ? null : locListAddRemCol.getComponent();
        }

        public void setLocSelectedListTable(UIComponent locSelectedListTable) {
            this.locSelectedListTable = ComponentReference.newUIComponentReference(locSelectedListTable);
        }

        public UIComponent getLocSelectedListTable() {
            return locSelectedListTable == null ? null : locSelectedListTable.getComponent();
        }

    /******************************************End Manage Location *****************************************************/
    public void setLocationLovList(List<LovList> locationLovList) {
        this.locationLovList = locationLovList;
    }

    public List<LovList> getLocationLovList() {
        return locationLovList;
    }
    
    public String getAddorRemoveSectorPopup(){
        FacesContext fctx = FacesContext.getCurrentInstance();
        UIViewRoot root = fctx.getViewRoot();
        
        return this.findComponent(root, "p3").getContainerClientId(fctx);
    }


    public void showAddorRemovePopupListener(ActionEvent actionEvent) {
        // Add event code here...
        FacesContext fctx = FacesContext.getCurrentInstance();
        UIComponent base = null;
        base = this.findParentComponent(actionEvent.getComponent());
        
        UIViewRoot root = fctx.getViewRoot();
        String clientIDStr = base.getClientId();
        int regionStartIndx  = clientIDStr.indexOf("region");
        int regionValidateIndx = clientIDStr.indexOf(":",regionStartIndx);
        //System.out.println("Component regionValidateIndx: "+regionValidateIndx);
        if (regionValidateIndx == -1) {
           
           base =  root;
        }
        //System.out.println("Component regionValidateIndx: "+base);
        RichPopup popup = (RichPopup) this.findComponent(base, "sdp3");
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);             
    }

    public void addInterventionActionLsnr(ActionEvent actionEvent) {
        // Add event code here...ExecutePasSubsectorInterventionValid AddIntervention
        DCBindingContainer bindingContainer =
            (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ob = null;
        ob = bindingContainer.getOperationBinding("ExecutePasSubsectorInterventionValid");
        ob.execute();
        ob = bindingContainer.getOperationBinding("AddIntervention");
        ob.execute();
    }
}
