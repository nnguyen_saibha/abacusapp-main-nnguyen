package gov.ofda.abacus.view.bean;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;
import oracle.jbo.client.Configuration;

public class ContextDetails {
    public ContextDetails() {
        super();
    }
    private String instanceName;
    public ApplicationModule getViewObject(){
        String        amDef = "gov.ofda.abacus.model.module.ContextModule";
        String        config = "ContextModuleLocal";
        ApplicationModule appModule =Configuration.createRootApplicationModule(amDef,config);
        return appModule;
    }

    public String getInstanceName() {
        ApplicationModule am = getViewObject();
        ViewObject vo = am.findViewObject("InstanceView1");
        vo.executeQuery();
        String tmp="OFDAU11G";
        if (vo.hasNext()) {
           Row currow = vo.next();
           tmp=(String)currow.getAttribute("Instance");
        }
        Configuration.releaseRootApplicationModule(am,true);
        return tmp;
    }
}
