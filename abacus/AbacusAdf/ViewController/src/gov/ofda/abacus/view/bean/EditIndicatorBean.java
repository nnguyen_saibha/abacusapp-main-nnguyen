package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.base.UIControl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.controller.ControllerContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySetImpl;
import org.apache.myfaces.trinidad.util.ComponentReference;

/**
 * EditIndicatorBean.java
 * Purpose: Bean for editIndicator.jsff
 * @author Kartheek Atluri
 * @version 1.0 12/01/2012
 */
public class EditIndicatorBean  extends UIControl implements Serializable{
    @SuppressWarnings("compatibility:7268580661449538127")
    private static final long serialVersionUID = 1L;
    private ComponentReference indForm; //RichPanelFormLayout
    private RowKeySetImpl newDisclosedTreeTableKeys = null;
    private ComponentReference indTargetTree;//RichTreeTable

    public EditIndicatorBean() {
        super();
    }

    /**
     * Calls the addIndicatorType method in PAModuleImpl to add
     * indicator targets for the current indicator.
     * @param actionEvent
     */
    public void addIndActionLsnr(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding operationBinding = bindings.getOperationBinding("addIndicatorType");
        operationBinding.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getIndForm());
    }

   
    

    /**
     * http://www.oracle.com/technetwork/developer-tools/adf/learnmore/78-man-expanding-trees-treetables-354775.pdf
     * setter for disclosed keys. Determines which rows are disclosed in the tree table
     * @param newDisclosedTreeTableKeys
     */
    public void setNewDisclosedTreeTableKeys(RowKeySetImpl newDisclosedTreeTableKeys) {
        this.newDisclosedTreeTableKeys = newDisclosedTreeTableKeys;
    }

    /**
     * http://www.oracle.com/technetwork/developer-tools/adf/learnmore/78-man-expanding-trees-treetables-354775.pdf
     * getter for disclosed keys. Determines which rows are disclosed in the tree table
     * It discloses all rows upto 4 levels.
     * @return
     */
    public RowKeySetImpl getNewDisclosedTreeTableKeys() {
        if (newDisclosedTreeTableKeys == null) {
            newDisclosedTreeTableKeys = new RowKeySetImpl();
            FacesContext fctx = FacesContext.getCurrentInstance();
            UIViewRoot root = fctx.getViewRoot();
            //lookup the tree table component by its component ID
            RichTreeTable treeTable =(RichTreeTable)this.getIndTargetTree();
            
            //if tree table is found
            if (treeTable != null) {
                //get the collection model to access the ADF binding layer for
                ////the tree binding used
                CollectionModel model = (CollectionModel)treeTable.getValue();
                JUCtrlHierBinding treeBinding = (JUCtrlHierBinding)model.getWrappedData();
                JUCtrlHierNodeBinding nodeBinding = treeBinding.getRootNodeBinding();
                expandAllNodes(nodeBinding, newDisclosedTreeTableKeys, 0, 4);
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getIndTargetTree());
            }
        }
        return newDisclosedTreeTableKeys;
    }

    /**
     * Called to add keys of children in a tree to be disclosed.
     * This is a recursive call that uses maxExpandLevel to determine when to stop.
     * @param nodeBinding
     * @param disclosedKeys
     * @param currentExpandLevel
     * @param maxExpandLevel
     */
    private void expandAllNodes(JUCtrlHierNodeBinding nodeBinding, RowKeySetImpl disclosedKeys, int currentExpandLevel,
                                int maxExpandLevel) {
        if (currentExpandLevel <= maxExpandLevel) {
            List<JUCtrlHierNodeBinding> childNodes = (List<JUCtrlHierNodeBinding>)nodeBinding.getChildren();
            ArrayList newKeys = new ArrayList();
            if (childNodes != null) {
                for (JUCtrlHierNodeBinding _node : childNodes) {
                    newKeys.add(_node.getKeyPath());
                    expandAllNodes(_node, disclosedKeys, currentExpandLevel + 1, maxExpandLevel);
                }
            }
            disclosedKeys.addAll(newKeys);
        }
    }

    /**
     * Setter method for Target tree binding
     * @param indTargetTree
     */
    public void setIndTargetTree(UIComponent indTargetTree) {
        this.indTargetTree = ComponentReference.newUIComponentReference(indTargetTree);
        this.markDirty("VIEW");
    }

    /**
     * Getter method for target tree binding
     * @return
     */
    public UIComponent getIndTargetTree() {
        return indTargetTree == null ? null : indTargetTree.getComponent();
    }
    
    /**
     * Setter for Indicator Form binding
     * @param indForm
     */
    public void setIndForm(UIComponent indForm) {
        this.indForm = ComponentReference.newUIComponentReference(indForm);
        this.markDirty("VIEW");
    }

    /**
     * Getter for Indicator Form binding
     * @return
     */
    public UIComponent getIndForm() {
        return indForm == null ? null : indForm.getComponent();
    }
}
