package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.base.Shuttle;
import gov.ofda.abacus.base.UIControl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.PopupCanceledEvent;

import oracle.jbo.Key;
import oracle.jbo.NavigatableRowIterator;
import oracle.jbo.Row;

import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.component.UIXCollection;
import org.apache.myfaces.trinidad.component.UIXEditableValue;
import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySetImpl;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.ComponentReference;
import org.apache.myfaces.trinidad.util.Service;


public class IndicatorLookupBean extends UIControl implements Serializable{
    private static ADFLogger logger = ADFLogger.createADFLogger(IndicatorLookupBean.class);
    @SuppressWarnings("compatibility:-2240372897545715675")
    private static final long serialVersionUID = 1L;
    private ComponentReference lookupTable;//RichTable
    private ComponentReference deleteFailed;//RichPopup
    private ComponentReference editPopup;//RichPopup
    private ComponentReference addPopup;//RichPopup
    private List selectedIndTypes;
    private ComponentReference indTypeTable;//RichTable
    private ComponentReference indTypeValuesTable;//RichTable
    private ComponentReference displayForm;//RichPanelFormLayout
    private String indicatorCode;
    final String OLD_CURR_KEY_VIEWSCOPE_ATTR = "__oldCurrentRowKey__";
    private ComponentReference popupForm;//RichPanelFormLayout
    private boolean createFlg;
    private boolean addFlag;
    private ComponentReference subsectorInput;//RichPanelLabelAndMessage
    private ComponentReference sectorInput;//RichPanelLabelAndMessage
    private ComponentReference privilegesPopup;//RichPopup
    List roles = null;
    private ComponentReference disaggregatedTreeTable; //RichTreeTable
    private RowKeySetImpl newDisclosedTreeTableKeys = null;
    private ComponentReference treeTableGroup; //RichPanelGroupLayout

    public IndicatorLookupBean() {
        super();
    }

    /**
     * Check if user has role to manage indicator lookup
     * @return
     */
    public boolean getCheckUserRole() {
        SecurityContext context=ADFContext.getCurrent().getSecurityContext();
        if(context.isUserInRole("APP_LOOKUP_ADMIN")){
            return true;
        }
        return false;
    }



    /**
     * Checks if indicator is referred in proposal using method action 'checkIndCodeInPASI',
     * if referred then shows delete failed popup, else deletes it.
     * @param dialogEvent
     */
    public void deleteDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding dciter = (DCIteratorBinding)bindings.get("IndicatorLookupView1Iterator");
            Row row = dciter.getCurrentRow();
            OperationBinding exec = bindings.getOperationBinding("checkIndCodeInPASI");
            exec.getParamsMap().put("indicatorCode", row.getAttribute("IndicatorCode"));
            String y = (String)exec.execute();
            logger.log("rows exist?" + y);
            if (y.equalsIgnoreCase("yes")) {
                //Delete failed. So show message that delete has failed.
                RichPopup popup = (RichPopup)this.getDeleteFailed();
                RichPopup.PopupHints ph = new RichPopup.PopupHints();
                popup.show(ph);
            } else {
                OperationBinding exec1 = bindings.getOperationBinding("Delete");
                exec1.execute();
                exec1 = bindings.getOperationBinding("Commit");
                exec1.execute();
                exec1 = bindings.getOperationBinding("Execute");
                exec1.execute();
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLookupTable());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getDisplayForm());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTreeTableGroup());
            
        }
    }

    /**
     * It deactivates the indicator when clicked on 'ok'.
     * @param dialogEvent
     */
    public void deleteFailedDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding dciter = (DCIteratorBinding)bindings.get("IndicatorLookupView1Iterator");
            Row row = dciter.getCurrentRow();
            row.setAttribute("Active", "N");
            OperationBinding exec1 = null;
            exec1 = bindings.getOperationBinding("Commit");
            exec1.execute();
            exec1 = bindings.getOperationBinding("Execute");
            exec1.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLookupTable());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getDisplayForm());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTreeTableGroup());
            logger.log("The Indicator is" + row.getAttribute("IndicatorCode"));
            logger.log("The active field is" + row.getAttribute("Active"));
        }
    }

  

    /**
     * https://blogs.oracle.com/jdevotnharvest/entry/how_to_add_new_adf
     * Creates a new row after the selected row using index.
     * @param actionEvent
     */
    public void addNew(ActionEvent actionEvent) {
        if (getCheckUserRole()) {
            OperationBinding oper1 = null;
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding dciter = (DCIteratorBinding)bindings.get("IndicatorLookupView1Iterator");
            Row oldCcurrentRow = dciter.getCurrentRow();
            //ADFContext is a convenient way to access all kinds of memory
            //scopes. If you like to be persistent in your ADF coding then this
            //is what you want to use
            ADFContext adfCtx = ADFContext.getCurrent();
            if (oldCcurrentRow != null)
                adfCtx.getViewScope().put(OLD_CURR_KEY_VIEWSCOPE_ATTR, oldCcurrentRow.getKey().toStringFormat(true));
            NavigatableRowIterator nav = dciter.getNavigatableRowIterator();
            Row newRow = nav.createRow();
            newRow.setNewRowState(Row.STATUS_INITIALIZED);
            Row currentRow = nav.getCurrentRow();
            int currentRowIndex = nav.getRangeIndexOf(currentRow);
            nav.insertRowAtRangeIndex(currentRowIndex + 1, newRow);
            dciter.setCurrentRowWithKey(newRow.getKey().toStringFormat(true));
            createFlg = true;
            addFlag = true;
            RichPopup popup = (RichPopup)this.getEditPopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            _resetChildren(popup);
            popup.show(ph);
        } else {
            RichPopup popup = (RichPopup)this.getPrivilegesPopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            popup.show(ph);
        }
    }

    /**
     * @param actionEvent
     */
    public void editIndicator(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dciter = (DCIteratorBinding)bindings.get("IndicatorLookupView1Iterator");
        Row oldCcurrentRow = dciter.getCurrentRow();
        ADFContext adfCtx = ADFContext.getCurrent();
        adfCtx.getViewScope().put(OLD_CURR_KEY_VIEWSCOPE_ATTR, oldCcurrentRow.getKey().toStringFormat(true));
        createFlg = false;
        addFlag = false;
        RichPopup popup = (RichPopup)this.getEditPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
    }

    public List getSelectedIndicatorTypes() {
        this.selectedIndTypes = Shuttle.getSelectedLookup("IndIndtypeLinkView1Iterator", "IndTypeId");
        return this.selectedIndTypes;
    }

    public void setSelectedIndicatorTypes(List selectedValues) {
        this.selectedIndTypes = selectedValues;
    }

    /**
     * IndTypeLookupMgrDialogLsnr sets the Shuttle parameters.
     * @param dialogEvent
     */
    public void IndTypeLookupMgrDialogLsnr(DialogEvent dialogEvent) {
        logger.log("Inside: " + dialogEvent.getOutcome().name());

        DCBindingContainer bindingContainer =
            (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            OperationBinding ob1 = null;
            ob1 = bindingContainer.getOperationBinding("Commit");
            ob1.execute();
            Shuttle.setSelectedLookup(this.selectedIndTypes, "IndIndtypeLinkView1Iterator", "IndTypeId",
                                      "DeleteIndtype", "CreateIndtype");
            ob1 = bindingContainer.getOperationBinding("Commit");
            ob1.execute();
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLookupTable());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTreeTableGroup());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getDisplayForm());

    }

    /**
     * addSubsectorDialogListener sets the selected sector subsector values
     * from the search lookup to the main row attribute values on 'Ok'.
     * @param dialogEvent
     */
    public void addSubsectorDialogListener(DialogEvent dialogEvent) {
        DCBindingContainer bindingContainer =
            (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dciter1 = (DCIteratorBinding)bindingContainer.get("IndicatorLookupView1Iterator");
        Row currentIndicatorRow = dciter1.getCurrentRow();
        logger.log("Old Subsector is" + currentIndicatorRow.getAttribute("SubsectorCode"));
        DCIteratorBinding dciter = (DCIteratorBinding)bindingContainer.get("ActiveSubsectorLookupView1Iterator");
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            Row currentRow = dciter.getCurrentRow();
            currentIndicatorRow.setAttribute("SubsectorCode", currentRow.getAttribute("SubsectorCode"));
            currentIndicatorRow.setAttribute("SubsectorName", currentRow.getAttribute("SubsectorName"));
            currentIndicatorRow.setAttribute("SectorName", currentRow.getAttribute("SectorName"));
            currentIndicatorRow.setAttribute("SectorCode", currentRow.getAttribute("SectorCode"));
            logger.log("New subsector code is" + currentIndicatorRow.getAttribute("SubsectorCode"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSubsectorInput());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSectorInput());
        }
    }

    /**
     * editPopupCancelListener sets the focus on to the current selected row when clicked on cancel.
     * @param popupCanceledEvent
     */
    public void editPopupCancelListener(PopupCanceledEvent popupCanceledEvent) {
        DCBindingContainer bindingContainer =
            (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter =
            (DCIteratorBinding)bindingContainer.findIteratorBinding("IndicatorLookupView1Iterator");
        Key key = iter.getCurrentRow().getKey();
        logger.log("Entered with Key: " + key.getKeyValues());

        OperationBinding operationBinding = bindingContainer.getOperationBinding("Rollback");
        operationBinding.execute();
        iter = (DCIteratorBinding)bindingContainer.findIteratorBinding("IndicatorLookupView1Iterator");
        Row currentRow = iter.getCurrentRow();
        ADFContext adfCtx = ADFContext.getCurrent();
        RichPopup popup = (RichPopup)this.getEditPopup();
        _resetChildren(popup);
    }

    private void _resetChildren(UIComponent comp) {
        Iterator<UIComponent> kids = comp.getFacetsAndChildren();

        while (kids.hasNext()) {
            UIComponent kid = kids.next();

            if (kid instanceof UIXEditableValue) {
                ((UIXEditableValue)kid).resetValue();
                RequestContext.getCurrentInstance().addPartialTarget(kid);
            } else if (kid instanceof EditableValueHolder) {
                _resetEditableValueHolder((EditableValueHolder)kid);
                RequestContext.getCurrentInstance().addPartialTarget(kid);
            } else if (kid instanceof UIXCollection) {
                ((UIXCollection)kid).resetStampState();
                RequestContext.getCurrentInstance().addPartialTarget(kid);
            }

            _resetChildren(kid);
        }
    }

    private void _resetEditableValueHolder(EditableValueHolder evh) {
        evh.setValue(null);
        evh.setSubmittedValue(null);
        evh.setLocalValueSet(false);
        evh.setValid(true);
    }

    private UIComponent _findPopup(UIComponent component) {
        if (component == null)
            return null;

        if (component instanceof RichPopup)
            return component;

        return _findPopup(component.getParent());
    }


   

    /**
     * Generates the report for Sector, Subsector and Indicator Link
     * @param actionEvent
     */
    public void getReportURL(ActionEvent actionEvent) {
        String abacusInstance = null;
        String hostURL = null;
        StringBuffer sb = null;
        String redirectURL = null;
        Map<String, Object> applicationScope = ADFContext.getCurrent().getApplicationScope();
        if (applicationScope != null) {
            abacusInstance = (String)applicationScope.get("instance");
            hostURL = (String)applicationScope.get("hostURL");
            if (abacusInstance != null && hostURL != null) {
                sb = new StringBuffer();
                sb.append(hostURL);
                sb.append("/reports/rwservlet?Rep");
                sb.append(abacusInstance);
                sb.append("Pdf+report=SECTOR_SUBSECTORINDICATOR_LINK.RDF+P_FUNDING_ACTION=01,03+DESNAME=SEC.Pdf");
                redirectURL = sb.toString();
            }
        }
        logger.log("Redirect URL: " + redirectURL);
        if (redirectURL != null) {
            FacesContext fctx = FacesContext.getCurrentInstance();
            ExtendedRenderKitService erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
            StringBuilder script = new StringBuilder();
            script.append("window.open('" + redirectURL + "', '_blank');");
            erks.addScript(FacesContext.getCurrentInstance(), script.toString());
        }
    }



    //http://www.oracle.com/technetwork/developer-tools/adf/learnmore/78-man-expanding-trees-treetables-354775.pdf

    public void setNewDisclosedTreeTableKeys(RowKeySetImpl newDisclosedTreeTableKeys) {
        this.newDisclosedTreeTableKeys = newDisclosedTreeTableKeys;
    }
    /**
     * http://www.oracle.com/technetwork/developer-tools/adf/learnmore/78-man-expanding-trees-treetables-354775.pdf
     * getter for disclosed keys. Determines which rows are disclosed in the tree table
     * It discloses all rows upto 4 levels.
     * @return
     */
    public RowKeySetImpl getNewDisclosedTreeTableKeys() {
            newDisclosedTreeTableKeys = new RowKeySetImpl();
            FacesContext fctx = FacesContext.getCurrentInstance();
            UIViewRoot root = fctx.getViewRoot();
            //lookup the tree table component by its component ID
            //r1:0:pt1:tt1
            RichTreeTable treeTable = (RichTreeTable)this.getDisaggregatedTreeTable();
            //if tree table is found
            if (treeTable != null) {
                //get the collection model to access the ADF binding layer for
                ////the tree binding used
                CollectionModel model = (CollectionModel)treeTable.getValue();
                JUCtrlHierBinding treeBinding = (JUCtrlHierBinding)model.getWrappedData();
                JUCtrlHierNodeBinding nodeBinding = treeBinding.getRootNodeBinding();
                expandAllNodes(nodeBinding, newDisclosedTreeTableKeys, 0, 2);
        }
        return newDisclosedTreeTableKeys;
    }
    
    /**
     * Called to add keys of children in a tree to be disclosed.
     * This is a recursive call that uses maxExpandLevel to determine when to stop.
     * @param nodeBinding
     * @param disclosedKeys
     * @param currentExpandLevel
     * @param maxExpandLevel
     */
    private void expandAllNodes(JUCtrlHierNodeBinding nodeBinding, RowKeySetImpl disclosedKeys, int currentExpandLevel,
                                int maxExpandLevel) {
        if (currentExpandLevel <= maxExpandLevel) {
            List<JUCtrlHierNodeBinding> childNodes = (List<JUCtrlHierNodeBinding>)nodeBinding.getChildren();
            ArrayList newKeys = new ArrayList();
            if (childNodes != null) {
                for (JUCtrlHierNodeBinding _node : childNodes) {
                    newKeys.add(_node.getKeyPath());
                    expandAllNodes(_node, disclosedKeys, currentExpandLevel + 1, maxExpandLevel);
                }
            }
            disclosedKeys.addAll(newKeys);
        }
    }
    public void setDisaggregatedTreeTable(UIComponent disaggregatedTreeTable) {
        this.disaggregatedTreeTable = ComponentReference.newUIComponentReference(disaggregatedTreeTable);
        this.markDirty("PAGEFLOW");
    }

    public UIComponent getDisaggregatedTreeTable() {
        return disaggregatedTreeTable == null ? null : disaggregatedTreeTable.getComponent();
    }
    public void setTreeTableGroup(UIComponent treeTableGroup) {
        this.treeTableGroup = ComponentReference.newUIComponentReference(treeTableGroup);
        this.markDirty("PAGEFLOW");
    }

    public UIComponent getTreeTableGroup() {
        return treeTableGroup == null ? null : treeTableGroup.getComponent();
    }
    public void setSubsectorInput(UIComponent subsectorInput) {
        this.subsectorInput = ComponentReference.newUIComponentReference(subsectorInput);
        this.markDirty("PAGEFLOW");
    }

    public UIComponent getSubsectorInput() {
        return subsectorInput == null ? null : subsectorInput.getComponent();
    }
    public void setLookupTable(UIComponent lookupTable) {
        this.lookupTable = ComponentReference.newUIComponentReference(lookupTable);
        this.markDirty("PAGEFLOW");
    }

    public UIComponent getLookupTable() {
        return lookupTable == null ? null : lookupTable.getComponent();
    }

    public void setDeleteFailed(UIComponent deleteFailed) {
        this.deleteFailed = ComponentReference.newUIComponentReference(deleteFailed);
        this.markDirty("PAGEFLOW");
    }

    public UIComponent getDeleteFailed() {
        return deleteFailed == null ? null : deleteFailed.getComponent();
    }

    public void setDisplayForm(UIComponent displayForm) {
        this.displayForm = ComponentReference.newUIComponentReference(displayForm);
        this.markDirty("PAGEFLOW");
    }

    public UIComponent getDisplayForm() {
        return displayForm == null ? null : displayForm.getComponent();
    }

    public void setIndTypeTable(UIComponent indTypeTable) {
        this.indTypeTable = ComponentReference.newUIComponentReference(indTypeTable);
        this.markDirty("PAGEFLOW");
    }

    public UIComponent getIndTypeTable() {
        return indTypeTable == null ? null : indTypeTable.getComponent();
    }

    public void setEditPopup(UIComponent editPopup) {
        this.editPopup = ComponentReference.newUIComponentReference(editPopup);
        this.markDirty("PAGEFLOW");
    }

    public UIComponent getEditPopup() {
        return editPopup == null ? null : editPopup.getComponent();
    }

    public void setAddPopup(UIComponent addPopup) {
        this.addPopup = ComponentReference.newUIComponentReference(addPopup);
        this.markDirty("PAGEFLOW");
    }

    public UIComponent getAddPopup() {
        return addPopup == null ? null : addPopup.getComponent();
    }
    public void setPopupForm(UIComponent popupForm) {
        this.popupForm = ComponentReference.newUIComponentReference(popupForm);
        this.markDirty("PAGEFLOW");
    }

    public UIComponent getPopupForm() {
        return popupForm == null ? null : popupForm.getComponent();
    }


    public void setCreateFlg(boolean createFlg) {
        this.createFlg = createFlg;
    }

    public boolean isCreateFlg() {
        return createFlg;
    }

    public void setAddFlag(boolean addFlag) {
        this.addFlag = addFlag;
    }

    public boolean isAddFlag() {
        return addFlag;
    }

    public void setSectorInput(UIComponent sectorInput) {
        this.sectorInput = ComponentReference.newUIComponentReference(sectorInput);
        this.markDirty("PAGEFLOW");
    }

    public UIComponent getSectorInput() {
        return sectorInput == null ? null : sectorInput.getComponent();
    }

    public void setIndicatorCode(String indicatorCode) {
        this.indicatorCode = indicatorCode;
    }

    public String getIndicatorCode() {
        return indicatorCode;
    }

    public void setPrivilegesPopup(UIComponent privilegesPopup) {
        this.privilegesPopup = ComponentReference.newUIComponentReference(privilegesPopup);
        this.markDirty("PAGEFLOW");
    }

    public UIComponent getPrivilegesPopup() {
        return privilegesPopup == null ? null : privilegesPopup.getComponent();
    }

    public void setIndTypeValuesTable(UIComponent indTypeValuesTable) {
        this.indTypeValuesTable = ComponentReference.newUIComponentReference(indTypeValuesTable);
        this.markDirty("PAGEFLOW");
    }

    public UIComponent getIndTypeValuesTable() {
        return indTypeValuesTable == null ? null : indTypeValuesTable.getComponent();
    }
}

