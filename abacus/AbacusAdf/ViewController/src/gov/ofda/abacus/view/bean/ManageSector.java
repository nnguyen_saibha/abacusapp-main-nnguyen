package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.base.*;

import gov.ofda.abacus.model.NewPAS;

import gov.ofda.abacus.pojo.CustomIndicator;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.fragment.RichRegion;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaRow;
import oracle.jbo.ViewObject;

import oracle.jbo.uicli.binding.JUCtrlHierBinding;

import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.model.RowKeySetImpl;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.ComponentReference;
import org.apache.myfaces.trinidad.util.Service;

/**
 * ManageSector.java
 * Purpose: Bean for sector-manager-taskflow and manageSector.jsf
 * @author Kartheek Atluri
 * @version 1.0 12/01/2012
 */
public class ManageSector extends UIControl implements Serializable {
    private static ADFLogger logger = ADFLogger.createADFLogger(ManageSector.class);
    @SuppressWarnings("compatibility:-2901050793349604601")
    private static final long serialVersionUID = 1L;
    private ComponentReference addSectorPopup; //RichPopup
    private List<NewPAS> sectorList = new ArrayList<NewPAS>();
    private Hashtable<String, NewPAS> sHashtable = new Hashtable<String, NewPAS>();
    private ComponentReference selectedSectorTable; //RichTable
    private ComponentReference sectorTreeTable; //RichTreeTable
    private ComponentReference facetSwitcher; //UIXSwitcher
    private String facetName = "gov.ofda.abacus.model.pa.view.PaSectorView";
    private List selectedSubsectors;
    private Boolean isAddIndicators = false;
    // private List selectedKeywords;
    private List selectedIndicators;
    private String addiionalIndicatorsSSList;
    private ComponentReference additionalIndicatorsPopup; //RichPopup
    private RowKeySetImpl newDisclosedTreeTableKeys = null;
    private Number plannedAmtDiff;
    private Number approvedAmtDiff;
    private List existingSubsectors;

    private int maxExpandLevelVal = 0;
    private boolean displayIndicatorFlg = true;
    private boolean enableShowIndicatorFlg = false;
    private ComponentReference indicatorButton;
    private ComponentReference rollbackChangesButton; //RichButton
    private List<CustomIndicator> customIndicatorList=new ArrayList<CustomIndicator>();
    private List<String> selectedIndicatorsList=null;
    private Boolean indicatorsChanged=false;

    public void setIndicatorsChanged(Boolean indicatorsChanged) {
        this.indicatorsChanged = indicatorsChanged;
    }

    public Boolean getIndicatorsChanged() {
        return indicatorsChanged;
    }

    public void setSelectedIndicatorsList(List<String> selectedIndicatorsList) {
        this.selectedIndicatorsList = selectedIndicatorsList;
    }

    public List<String> getSelectedIndicatorsList() {
        return selectedIndicatorsList;
    }

    public void setCustomIndicatorList(List<CustomIndicator> customIndicatorList) {
        this.customIndicatorList = customIndicatorList;
    }

    public List<CustomIndicator> getCustomIndicatorList() {
        return customIndicatorList;
    }

    public ManageSector() {
        super();
    }

    /**
     * Called from sector-manager-taskflow, it sets the instance, action Id, user Id
     * and calls setUserInfo which sets the user roles.
     * It also sets PaSectorView2Iterator to get around the problem of tree not selecting
     * any row on initial rendering.
     */
    public void setActionID() {

        String instanceName = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("instance");
        if (instanceName != null) {
            JSFUtils.setManagedBeanValue("sessionScope.dataSource", "jdbc/abacustestDS");
        }
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding operationBinding = bindings.getOperationBinding("setActionID");
        operationBinding.getParamsMap().put("actionID",
                                            AdfFacesContext.getCurrentInstance().getPageFlowScope().get("actionID"));
        operationBinding.execute();
        operationBinding = bindings.getOperationBinding("setUserInfo");
        //operationBinding.getParamsMap().put("userId",FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userID"));
        operationBinding.getParamsMap().put("userId",
                                            ADFContext.getCurrent().getSecurityContext().getUserName().toUpperCase());
        List roles = (List) operationBinding.execute();
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userRoles", roles);
        DCIteratorBinding iter = (DCIteratorBinding) bindings.findIteratorBinding("PaSectorView2Iterator");
        ViewObject sectorVO = iter.getViewObject();
        ViewCriteria vc = sectorVO.createViewCriteria();
        ViewCriteriaRow vcr = vc.createViewCriteriaRow();
        vcr.setAttribute("ActionId", AdfFacesContext.getCurrentInstance().getPageFlowScope().get("actionID"));
        vc.addElement(vcr);
        sectorVO.applyViewCriteria(vc, true);
        sectorVO.executeQuery();
    }

    /**
     * Called by Cancel, which calls Rollback to undo all changes made.
     * It then sets the action ID for PA by calling setActionID in PAModuleImpl
     * It also sets the tree table to previously selecte row.
     * @param actionEvent
     */
    
    public void rollbackChanges() {
          cancelChanges(null);
        }
    public void cancelChanges(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding operationBinding = bindings.getOperationBinding("Rollback");
        operationBinding.execute();
        RowKeySet rks = ((RichTreeTable) this.getSectorTreeTable()).getSelectedRowKeys();
        operationBinding = bindings.getOperationBinding("setActionID");
        operationBinding.getParamsMap().put("actionID",
                                            AdfFacesContext.getCurrentInstance().getPageFlowScope().get("actionID"));
        operationBinding.execute();
        operationBinding = bindings.getOperationBinding("executeSector");
        operationBinding.execute();
        if (rks.iterator().hasNext()) {
            List lt = (List) rks.iterator().next();
            Iterator lte = lt.iterator();
            int ct = 0;
            Key k = null;
            while (lte.hasNext()) {
                k = (Key) lte.next();
                ct++;
            }
            if (ct > 0) {
                String iterName[] = {
                    "PaSectorView1Iterator", "PasSubsectorView1Iterator", "PasIndicatorsView1Iterator" };
                DCIteratorBinding iter = (DCIteratorBinding) bindings.findIteratorBinding(iterName[ct - 1]);
                RowSetIterator rsi = iter.getRowSetIterator();
                if (rsi.findByKey(k, 1).length > 0)
                    iter.setCurrentRowWithKey(k.toStringFormat(true));
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSectorTreeTable());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getFacetSwitcher());
        
    }

    /**
     * Not used in V13.1
     * Called when user clicks on Add Sectors.
     * It sets valid sector list by calling setValidAddSectorList in PAModuleImpl
     * and opens addSectorPopup
     * @param actionEvent
     */
    public void AddSectorLsnr(ActionEvent actionEvent) {
        // Add event code here...setValidAddSectorList
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ob = bindingContainer.getOperationBinding("setValidAddSectorList");
        ob.execute();
        AdfFacesContext.getCurrentInstance().getViewScope().put("selectedSectors", null);
        sectorList = new ArrayList<NewPAS>();
        sHashtable = new Hashtable<String, NewPAS>();
        RichPopup popup = (RichPopup) this.getAddSectorPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
    }


    /**
     * Not used in V13.1
     * Called when user closes the addSectorPopup. If user click on ok,
     * it gets the selected sectors list and calls addSector in PAModule to add the selectors
     * in the selectedSectors list.
     * @param dialogEvent
     */
    public void addSectorDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            List str = (ArrayList) AdfFacesContext.getCurrentInstance().getViewScope().get("selectedSectors");
            if (str != null) {
                DCBindingContainer bindingContainer =
                    (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding ob = bindingContainer.getOperationBinding("addSectors");
                ob.getParamsMap().put("sectorList", this.getSectorList());
                ob.execute();
                // ob = bindingContainer.getOperationBinding("Commit");
                // ob.execute();
                ob = bindingContainer.getOperationBinding("executeSector");
                ob.execute();
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSectorTreeTable());
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getFacetSwitcher());
            }

        }
    }

    /**
     * The value change listener for add sector shuttle is used to set the sectorList
     * A new list is created each time value is changed with sector code and  assistance type
     * @param valueChangeEvent
     */
    public void addSectorChgLsnr(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        List str = (ArrayList) AdfFacesContext.getCurrentInstance().getViewScope().get("selectedSectors");
        sectorList = new ArrayList<NewPAS>();
        if (str != null) {
            Object[] o = str.toArray();
            for (Object s : o) {
                //Get the list if it is already present and add to sectorList
                if (sHashtable.containsKey(s))
                    sectorList.add(sHashtable.get(s));
                else {
                    NewPAS newPas = new NewPAS((String) s);
                    sHashtable.put((String) s, newPas);
                    sectorList.add(newPas);
                }
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSelectedSectorTable());
    }

    /**
     * Treetable selection listener for the sector/sub-sector/indicator tree
     * If there are unsaved changes alter the user to save changes, if not call the selectionEvent
     * that sets the facet name to show based on selected row.
     * @param selectionEvent
     */
    public void sectorTreeSelectionListener(SelectionEvent selectionEvent) {
        logger.info("In Selection Event: ");
        if (!this.isDataDirty()) {
            String nodeStrutureDefname = this.onTreeTableSelect(selectionEvent);
            if("gov.ofda.abacus.model.pa.view.PasSubsectorView".equals(nodeStrutureDefname)|| "gov.ofda.abacus.model.pa.view.PaSectorView".equals(nodeStrutureDefname))
                this.setFacetName(nodeStrutureDefname);
            else
               this.setFacetName("empty");
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            adfFacesContext.getCurrentInstance().addPartialTarget(this.getFacetSwitcher());
            this.setSelectedIndicatorsList(null);
        } else {
            FacesContext fctxt = FacesContext.getCurrentInstance();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_WARN, "Unsaved Changes",
                                 "Please save or cancel changes to continue.");
            fctxt.addMessage(null, msg);

            String nodeStrutureDefname = this.onTreeTableSelectDirty(selectionEvent);
            this.setFacetName(nodeStrutureDefname);
            ((RichTreeTable) this.getSectorTreeTable()).setSelectedRowKeys(selectionEvent.getRemovedSet());
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            adfFacesContext.addPartialTarget(this.getSectorTreeTable());
            adfFacesContext.addPartialTarget(this.getFacetSwitcher());
            fctxt.renderResponse();
        }
        logger.info("Out of Selection Event: ");
    }


    /**
     * Not used in V13.1
     * Dialog listener to confirm deletion of sector. If user click on yes then
     * call delete on selected sector.
     * @param dialogEvent
     */
    public void deleteSectorDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) {
            DCBindingContainer bindingContainer =
                (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding ob = bindingContainer.getOperationBinding("deleteSector");
            ob.execute();
            //   ob = bindingContainer.getOperationBinding("Commit");
            //   ob.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSectorTreeTable());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getFacetSwitcher());
        }
    }
    //Start subsector functions

    /**
     * Getter for selectedSubsectors. Used in Subsector shuttle, this adds all the existing subsectors
     * into selected list.
     * @return
     */
    public List getSelectedSubsectors() {
        this.selectedSubsectors = Shuttle.getSelected("PasSubsectorView2Iterator", "SubsectorCode");
        return this.selectedSubsectors;
    }

    /**
     * Setter for selectedSubsectors. Any changes made to shuttle values are set here.
     * @param selectedValues
     */
    public void setSelectedSubsectors(List selectedValues) {
        this.selectedSubsectors = selectedValues;
    }

    /**
     * Used by shuttle select items, This add all the available subsectors for the sector and already selected
     * subsectors incase they are made inactive in which case they will not be in SectorSubsectorLinkView1Iterator.
     * @return
     */
    public List getAllSubsectors() {
        return Shuttle.getAll("SectorSubsectorLinkView1Iterator", "PasSubsectorView2Iterator", "SubsectorCode",
                              "SubsectorName");
    }

    /**
     * Dialog listener for Subsectors.
     * Here the subsectors are inserted and/or deleted based on the list selectedSubsectors.
     * If AddIndicators is checked then call addSubSEctorIndicators in PAModuleImpl to add
     * indicators for all new subsectors added. It will return a list with all subsectors that
     * have additional indicators available.
     * @param dialogEvent
     */
    public void subSectorMgrDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            Shuttle.setSelected(this.selectedSubsectors, "PasSubsectorView2Iterator", "SubsectorCode",
                                "deleteSubsector", "createSubsector");
            DCBindingContainer bindingContainer =
                (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();

            OperationBinding ob = null;
            if (this.isAddIndicators) {
                ob = bindingContainer.getOperationBinding("addSubsectorIndicators");
                ob.getParamsMap().put("existingValues", this.getExistingSubsectors());
                ob.getParamsMap().put("newValues", this.getSelectedSubsectors());
                String ssList = (String) ob.execute();
                if (ssList.length() > 0) {
                    addiionalIndicatorsSSList =
                        "Please add additional indicators manually for the following Subsectors<br><ol>" + ssList +
                        "</ol>";
                    RichPopup popup = (RichPopup) this.getAdditionalIndicatorsPopup();
                    RichPopup.PopupHints ph = new RichPopup.PopupHints();
                    popup.show(ph);
                }
            }
            // ob = bindingContainer.getOperationBinding("executeSector");
            //  ob.execute();
             ob = bindingContainer.getOperationBinding("Commit");
             ob.execute();
                 if (!ob.getErrors().isEmpty()) {
                     /*
                      * Delete failed due to foreign key reference in parent_location_id.
                      * Rollback , show error and set the current row.
                      */
                     Throwable t = (Throwable) ob.getErrors().get(0);
                     ob = bindingContainer.getOperationBinding("Rollback");
                     ob.execute();
                     FacesContext fc = FacesContext.getCurrentInstance();
                     fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error removing Sub Sectors. Please make sure sub sectors being removed are not used in Sector Overview Table.", null));
                 }
            else
                 {
                    ob = bindingContainer.getOperationBinding("publishSubSectorChangesEvent");
                    ob.execute();
                 }
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSectorTreeTable());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getFacetSwitcher());
        }
        isAddIndicators = false;
    }

    //Start Indicator functions

    /**
     * Getter for selectedIndicators. Used in Indicators shuttle, this adds all the existing Indicators
     * @return
     */
    public List getSelectedIndicators() {
        this.selectedIndicators = Shuttle.getSelected("PasIndicatorsView3Iterator", "IndicatorCode");
        return this.selectedIndicators;
    }

    /**
     * Setter for selectedIndicators. Any changes made to shuttle values are set here.
     * @param selectedValues
     */
    public void setSelectedIndicators(List selectedValues) {
        this.selectedIndicators = selectedValues;
    }

    /**
     * Used by shuttle select items, This add all the available for the subsector and already selected
     * indicators incase they are made inactive in which case they will not be in IndicatorLookupView2Iterator.

     * @return
     */
    public List getAllIndicators() {
        return Shuttle.getAll("IndicatorLookupView3Iterator", "PasIndicatorsView3Iterator", "IndicatorCode",
                              "IndicatorName");
    }

    /**
     * Dialog listener for Indicators.
     * Here the Indicators are inserted and/or deleted based on the list selectedIndicators.
     * @param dialogEvent
     */
    public void indicatorMgrDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            Shuttle.setSelected(this.selectedIndicators, "PasIndicatorsView3Iterator", "IndicatorCode",
                                "deleteIndicator", "createIndicator");
            DCBindingContainer bindingContainer =
                (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding ob = null;
              ob = bindingContainer.getOperationBinding("Commit");
              ob.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSectorTreeTable());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getFacetSwitcher());
            this.saveChanges(null);
        }
        else
            rollbackChanges();
        //this.selectedIndicators = Shuttle.getSelected("PasIndicatorsView3Iterator", "IndicatorCode");
        //this.customIndicatorList=new ArrayList<CustomIndicator>();
    }
    //End Indicator functions
    
    
    /**
     * Dialog listener for Indicators.
     * Here the Indicators are inserted and/or deleted based on the list selectedIndicators.
     * @param dialogEvent
     */
    public void indicatorMgr2DialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)) {
            Shuttle.setSelected(this.selectedIndicatorsList, "PasIndicatorsView3Iterator", "IndicatorCode",
                                "deleteIndicator", "createIndicator");
            DCBindingContainer bindingContainer =
                (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding ob = null;
            //  ob = bindingContainer.getOperationBinding("Commit");
            //  ob.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSectorTreeTable());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getFacetSwitcher());
            this.saveChanges(null);
        }
        else
          this.rollbackChanges();
            
        //this.selectedIndicators = Shuttle.getSelected("PasIndicatorsView3Iterator", "IndicatorCode");
        //this.customIndicatorList=new ArrayList<CustomIndicator>();
    }
    //End Indicator functions

    public void setSelectedSectorTable(UIComponent selectedSectorTable) {
        this.selectedSectorTable = ComponentReference.newUIComponentReference(selectedSectorTable);
        this.markDirty("VIEW");
    }

    public UIComponent getSelectedSectorTable() {
        return selectedSectorTable == null ? null : selectedSectorTable.getComponent();
    }

    public void setAddSectorPopup(UIComponent addSectorPopup) {
        this.addSectorPopup = ComponentReference.newUIComponentReference(addSectorPopup);
        this.markDirty("VIEW");
    }

    public UIComponent getAddSectorPopup() {
        return addSectorPopup == null ? null : addSectorPopup.getComponent();
    }

    public void setSectorList(List<NewPAS> sectorList) {
        this.sectorList = sectorList;
    }

    public List<NewPAS> getSectorList() {
        return sectorList;
    }

    public void setSectorTreeTable(UIComponent sectorTreeTable) {
        this.sectorTreeTable = ComponentReference.newUIComponentReference(sectorTreeTable);
        this.markDirty("VIEW");
    }

    public UIComponent getSectorTreeTable() {
        return sectorTreeTable == null ? null : sectorTreeTable.getComponent();
    }

    public void setIndicatorButton(UIComponent indicatorButton) {
        this.indicatorButton = ComponentReference.newUIComponentReference(indicatorButton);
        this.markDirty("VIEW");
    }

    public UIComponent getIndicatorButton() {
        return indicatorButton == null ? null : indicatorButton.getComponent();

    }

    public void setFacetSwitcher(UIComponent facetSwitcher) {
        this.facetSwitcher = ComponentReference.newUIComponentReference(facetSwitcher);
        this.markDirty("VIEW");
    }

    public UIComponent getFacetSwitcher() {
        return facetSwitcher == null ? null : facetSwitcher.getComponent();
    }

    public void setFacetName(String facetName) {
        this.facetName = facetName;
    }

    public String getFacetName() {
        return facetName;
    }

    public void setIsAddIndicators(Boolean isAddIndicators) {
        this.isAddIndicators = isAddIndicators;
    }

    public Boolean getIsAddIndicators() {
        return isAddIndicators;
    }

    public String getAddiionalIndicatorsSSList() {
        return addiionalIndicatorsSSList;
    }

    public void setDisplayIndicatorFlg(boolean displayIndicatorFlg) {
        this.displayIndicatorFlg = displayIndicatorFlg;
    }

    public boolean getDisplayIndicatorFlg() {
        return displayIndicatorFlg;
    }

    public void setEnableShowIndicatorFlg(boolean enableShowIndicatorFlg) {
        this.enableShowIndicatorFlg = enableShowIndicatorFlg;
    }

    public boolean getEnableShowIndicatorFlg() {
       
        FacesContext fctx = FacesContext.getCurrentInstance();
        UIViewRoot root = fctx.getViewRoot();
        RichTreeTable treeTable = (RichTreeTable) this.findComponent(root, "tt1");
        if (treeTable != null) {
            CollectionModel model = (CollectionModel) treeTable.getValue();
            if (model != null && model.getWrappedData() != null) {
                JUCtrlHierBinding treeBinding = (JUCtrlHierBinding) model.getWrappedData();
                JUCtrlHierNodeBinding nodeBinding = treeBinding.getRootNodeBinding();
                List<JUCtrlHierNodeBinding> childNodes = (List<JUCtrlHierNodeBinding>) nodeBinding.getChildren();
                ArrayList newKeys = new ArrayList();
                if (childNodes != null) {
                    for (JUCtrlHierNodeBinding _node : childNodes) {
                        if ( _node.getChildren() != null && !enableShowIndicatorFlg)
                            setEnableShowIndicatorFlg(true);
                    }
                }
            }
        }
        return enableShowIndicatorFlg;
    }

    public void setAdditionalIndicatorsPopup(UIComponent additionalIndicatorsPopup) {
        this.additionalIndicatorsPopup = ComponentReference.newUIComponentReference(additionalIndicatorsPopup);
        this.markDirty("VIEW");
    }

    public UIComponent getAdditionalIndicatorsPopup() {
        return additionalIndicatorsPopup == null ? null : additionalIndicatorsPopup.getComponent();
    }
    //http://www.oracle.com/technetwork/developer-tools/adf/learnmore/78-man-expanding-trees-treetables-354775.pdf

    public void setNewDisclosedTreeTableKeys(RowKeySetImpl newDisclosedTreeTableKeys) {
        this.newDisclosedTreeTableKeys = newDisclosedTreeTableKeys;
    }

    /**
     * http://www.oracle.com/technetwork/developer-tools/adf/learnmore/78-man-expanding-trees-treetables-354775.pdf
     * getter for disclosed keys. Determines which rows are disclosed in the tree table
     * It discloses all rows upto 4 levels.
     * @return
     */
    public RowKeySetImpl getNewDisclosedTreeTableKeys() {
        if (newDisclosedTreeTableKeys == null) {
            newDisclosedTreeTableKeys = new RowKeySetImpl();
            FacesContext fctx = FacesContext.getCurrentInstance();
            UIViewRoot root = fctx.getViewRoot();
            //lookup the tree table component by its component ID
            RichTreeTable treeTable = (RichTreeTable) this.findComponent(root, "tt1");
            //System.out.println("Treetable value: "+treeTable);
            //if tree table is found
            if (treeTable != null) {
                //get the collection model to access the ADF binding layer for
                ////the tree binding used
                CollectionModel model = (CollectionModel) treeTable.getValue();
                if (model != null && model.getWrappedData() != null) {
                    JUCtrlHierBinding treeBinding = (JUCtrlHierBinding) model.getWrappedData();
                    JUCtrlHierNodeBinding nodeBinding = treeBinding.getRootNodeBinding();
                    expandAllNodes(nodeBinding, newDisclosedTreeTableKeys, 0, maxExpandLevelVal);
                }
            }
        } 
        return newDisclosedTreeTableKeys;
    }

    /**
     * Called to add keys of children in a tree to be disclosed.
     * This is a recursive call that uses maxExpandLevel to determine when to stop.
     * @param nodeBinding
     * @param disclosedKeys
     * @param currentExpandLevel
     * @param maxExpandLevel
     */
    private void expandAllNodes(JUCtrlHierNodeBinding nodeBinding, RowKeySetImpl disclosedKeys, int currentExpandLevel,
                                int maxExpandLevel) {
        if (currentExpandLevel <= maxExpandLevel) {
            List<JUCtrlHierNodeBinding> childNodes = (List<JUCtrlHierNodeBinding>) nodeBinding.getChildren();
            ArrayList newKeys = new ArrayList();
            if (childNodes != null) {
                for (JUCtrlHierNodeBinding _node : childNodes) {                   
                    newKeys.add(_node.getKeyPath());
                    expandAllNodes(_node, disclosedKeys, currentExpandLevel + 1, maxExpandLevel);
                }
            }          
            disclosedKeys.addAll(newKeys);
        }
    }


    public void getReportURL(ActionEvent actionEvent) {
        String abacusInstance = null;
        String hostURL = null;
        StringBuffer sb = null;
        String actionID = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("actionID");
        String redirectURL = null;
        ;
        if (actionID != null) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("ProcurementActionView1Iterator");
            Row rw = iter.getCurrentRow();
            Integer budgetFy = (Integer) rw.getAttribute("BudgetFy");
            String ProjectNbr = (String) rw.getAttribute("ProjectNbr");
            Long ActionSeqNbr = (Long) rw.getAttribute("ActionSeqNbr");
            Map<String, Object> applicationScope = ADFContext.getCurrent().getApplicationScope();
            if (applicationScope != null) {
                abacusInstance = (String) applicationScope.get("instance");
                hostURL = (String) applicationScope.get("hostURL");
                if (abacusInstance != null && hostURL != null) {
                    sb = new StringBuffer();
                    sb.append(hostURL);
                    sb.append("/reports/rwservlet?Rep");
                    sb.append(abacusInstance);
                    sb.append("Pdf+report=PAS.RDF+P_FY=");
                    sb.append(budgetFy.toString());
                    sb.append("+P_PROJECT_NBR=");
                    sb.append(ProjectNbr);
                    sb.append("+P_ACTION_SEQ_NBR=");
                    sb.append(ActionSeqNbr.toString());
                    sb.append("+DESNAME=PAS.pdf");
                    redirectURL = sb.toString();
                }
            }
        }
        if (redirectURL != null) {
            FacesContext fctx = FacesContext.getCurrentInstance();
            ExtendedRenderKitService erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
            StringBuilder script = new StringBuilder();
            script.append("window.open('" + redirectURL + "', '_blank');");
            erks.addScript(FacesContext.getCurrentInstance(), script.toString());
        }
    }

    /**
     * Returns the difference between planned amount and total sector amount
     * @return
     */
    public Number getPlannedAmtDiff() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ProcurementActionView1Iterator");
        Row rw = iter.getCurrentRow();
        oracle.jbo.domain.Number pAmt = (oracle.jbo.domain.Number) rw.getAttribute("PlannedAmt");
        oracle.jbo.domain.Number sAmt = (oracle.jbo.domain.Number) rw.getAttribute("TotalSectorAmt");
        if (pAmt != null && sAmt != null)
            return pAmt.longValue() - sAmt.longValue();
        else
            return 0;
    }

    /**
     * Returns the difference between approved amount and total sector amount
     * @return
     */
    public Number getApprovedAmtDiff() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ProcurementActionView1Iterator");
        Row rw = iter.getCurrentRow();
        oracle.jbo.domain.Number pAmt = (oracle.jbo.domain.Number) rw.getAttribute("ApprovedAmt");
        oracle.jbo.domain.Number sAmt = (oracle.jbo.domain.Number) rw.getAttribute("TotalSectorAmt");
        if (pAmt != null && sAmt != null)
            return pAmt.longValue() - sAmt.longValue();
        else
            return 0;
    }

    public void subsectorValueChgLsnr(ValueChangeEvent valueChangeEvent) {
        existingSubsectors = (List) valueChangeEvent.getOldValue();
    }

    public List getExistingSubsectors() {
        return existingSubsectors;
    }

    public void saveChanges(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec1 = bindings.getOperationBinding("Commit");
        exec1.execute();
        if (!exec1.getErrors().isEmpty()) {
            /*
             * Delete failed due to foreign key reference in parent_location_id.
             * Rollback , show error and set the current row.
             */
            Throwable t = (Throwable) exec1.getErrors().get(0);
            exec1 = bindings.getOperationBinding("Rollback");
            exec1.execute();
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, t.getMessage(), null));
        }
        this.setSelectedIndicatorsList(null);
    }

    public void showIndicatorsInTreeTable(ActionEvent actionEvent) {
        // Add event code here...
        newDisclosedTreeTableKeys = null;
        if (displayIndicatorFlg) {

            maxExpandLevelVal = 1;
            displayIndicatorFlg = false;
        } else {

            maxExpandLevelVal = 0;
            displayIndicatorFlg = true;
        }

        getNewDisclosedTreeTableKeys();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getIndicatorButton());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSectorTreeTable());
    }

    public void resetToSectorsSubsectors(ActionEvent actionEvent) {
        // Add event code here...
        newDisclosedTreeTableKeys = null;

        maxExpandLevelVal = 0;
        displayIndicatorFlg = true;
        getNewDisclosedTreeTableKeys();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getIndicatorButton());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSectorTreeTable());
    }


    public void setRollbackChangesButton(UIComponent rollbackChangesButton) {
        this.rollbackChangesButton = ComponentReference.newUIComponentReference(rollbackChangesButton);
    }

    public UIComponent getRollbackChangesButton() {
        return rollbackChangesButton==null?null:rollbackChangesButton.getComponent();
    }

    public void addOrRemoveIndicator(ActionEvent actionEvent) {
        Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String indicatorCode = (String) requestScopeMap.get("selectedIndicatorCode");
       if(this.getSelectedIndicatorsList()!=null)
       {
        if(this.getSelectedIndicatorsList().contains(indicatorCode))
            this.getSelectedIndicatorsList().remove(indicatorCode);
        else
            this.getSelectedIndicatorsList().add(indicatorCode);
        AdfFacesContext.getCurrentInstance().addPartialTarget(actionEvent.getComponent().getParent());
        this.setIndicatorsChanged(true);
       }
    }

    public void addNewCustomIndicator(ActionEvent actionEvent) {
        this.getCustomIndicatorList().add(new CustomIndicator());
    }

    public void deleteCustomIndicator(ActionEvent actionEvent) {
        Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        CustomIndicator customIndicator = (CustomIndicator) requestScopeMap.get("customIndicator");
        if(customIndicator!=null) {
            this.getCustomIndicatorList().remove(customIndicator);
        }
    }

    public void addIndicatorFetchLsnr(PopupFetchEvent popupFetchEvent) {
            this.selectedIndicatorsList=new ArrayList<String>();
            this.selectedIndicatorsList.addAll(this.getSelectedIndicators());
            this.setIndicatorsChanged(false);
    }

    public void saveIndicatorChanges(ActionEvent actionEvent) {
            Shuttle.setSelected(this.selectedIndicatorsList, "PasIndicatorsView3Iterator", "IndicatorCode",
                                "deleteIndicator", "createIndicator");
            DCBindingContainer bindingContainer =
                (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding ob = null;
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSectorTreeTable());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getFacetSwitcher());
            this.saveChanges(null);
            this.selectedIndicatorsList=new ArrayList<String>();
            this.selectedIndicatorsList.addAll(this.getSelectedIndicators());
            this.setIndicatorsChanged(false);
    }

    public void cancelIndicatorChanges(ActionEvent actionEvent) {
        this.rollbackChanges();
        this.selectedIndicatorsList=new ArrayList<String>();
        this.selectedIndicatorsList.addAll(this.getSelectedIndicators());
        this.setIndicatorsChanged(false);
          
    }

    public void closeIndicatorChanges(ActionEvent ae) {
        RichPopup rp=(RichPopup)ae.getComponent().getParent().getParent().getParent();
        rp.hide();
        this.rollbackChanges();
        this.setIndicatorsChanged(false);
    }

    public void manageIndicatorsPopupCancel(PopupCanceledEvent popupCanceledEvent) {
        this.rollbackChanges();
        this.setIndicatorsChanged(false);
    }
}
