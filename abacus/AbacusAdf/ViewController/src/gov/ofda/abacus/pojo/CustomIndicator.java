package gov.ofda.abacus.pojo;

import java.io.Serializable;

public class CustomIndicator implements Serializable {
    @SuppressWarnings("compatibility:-7505305779140671328")
    private static final long serialVersionUID = 1L;
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    private String disaggregatedBy;

    public void setDisaggregatedBy(String disaggregatedBy) {
        this.disaggregatedBy = disaggregatedBy;
    }

    public String getDisaggregatedBy() {
        return disaggregatedBy;
    }

    public CustomIndicator() {
        super();
    }
}
