package gov.ofda.abacus;

import java.sql.SQLIntegrityConstraintViolationException;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCErrorHandlerImpl;
import oracle.jbo.DMLConstraintException;

/**
 * The ADFm error handler class is configuredin the application DataBindings.cpx file to customize the
 * default error handling.
 *
 * @author Frank Nimphius. Oracle Magazine Jan-Feb 2013
 */
public class CustomErrorHandler extends DCErrorHandlerImpl {

    //when extending DCErrorHandlerImpl, ensure you create a no-args constructor in addition to the
    //generated boolean args constructor as otherwise ADF doesn't know how to initialize the custom
    //error handler

    public CustomErrorHandler() {
        super(true);
    }
    public CustomErrorHandler(boolean b) {
     super(b);
     }

    /*
        * The methods below override the functionality in the DCErrorHandlerImpl. To override methods
        * of a super class in JDeveloper, choose "Source > Override Methods" from the JDeveloper menu
        */

       public void reportException(DCBindingContainer dCBindingContainer, Exception exception) {
           //you can use this method to suppress exceptions in that you don't call reportException
           //on the super class
           
           super.reportException(dCBindingContainer, exception);
       }

       /**
        * Method overridden to handle SQL Exceptions such that ORA messages are removed. Also, add
        * prefix to error messages to show custom ADFm error handler is used
        * 
        * Note, if you return null as the display message, then the message will be suppressed in the
        * display. This sample has a button "ADFm Throw Nested Exceptions" that would diplay a message
        * Oracle Magazine Error Handler : null for the wrapping exception. To avoid this we check for 
        * null on the error message and suppress the custom prefix if the error message is null
        */
       public String getDisplayMessage(BindingContext bindingContext, Exception exception) {
           
           String ORAMAG_PREFIX = "Abacus Error : ";
           String errorMessage = super.getDisplayMessage(bindingContext, exception);
           String returnMessage = null;
           
           //remove ORA error code from error message        
           if(errorMessage != null){
            int index = errorMessage.indexOf("ORA");
               
            if (index > 0) {
              errorMessage = errorMessage.substring(0, index + 6);
            }
             //Note that in the sample this leads to the followin message when violating the unique constraint on 
             //the mail field in the employee edit form
            
            //Constraint "EMP_EMAIL_UK" is violated during post operation "Update" using SQL statement "UPDATE EMPLOYEES 
            //Employees SET EMAIL=:1 WHERE EMPLOYEE_ID=:2"
            //
            //This message still is not user friendly. To turn this into a user friendly message, you can change the 
            //default message in ADF BC (which is out of scope of the Oracle Magazine article this sample has been 
            //developed for)
            //
            // See: http://jobinesh.blogspot.com/2011/03/customizing-business-components-error.html
            //
            
            returnMessage = ORAMAG_PREFIX + errorMessage;
           }
           else{            
               returnMessage = null;
           }        
           return returnMessage;
       }

    //Skip SQLIntegrityConstraintViolationException from displaying in the error
    //list displayed to the user

    protected boolean skipException(Exception ex) {
        if (ex instanceof DMLConstraintException) {
            return false;
        } else if (ex instanceof java.sql.SQLIntegrityConstraintViolationException) {
            return true;
        }
        return super.skipException(ex);
    }
}

