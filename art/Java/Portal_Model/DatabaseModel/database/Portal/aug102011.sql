SPOOL aug102011.log

PROMPT alter table 'MBEAN_FP_MAP'
ALTER TABLE ODB.MBEAN_FP_MAP 
DROP CONSTRAINT MBEAN_FP_MAP_FUNCTION_POI_FK1;

PROMPT alter table 'ROLE_FP_MAP'
ALTER TABLE ODB.ROLE_FP_MAP 
DROP CONSTRAINT ROLE_FP_MAP_FUNCTION_POIN_FK1;

PROMPT alter table 'ROLE_FP_MAP'
ALTER TABLE ODB.ROLE_FP_MAP 
DROP CONSTRAINT ROLE_FP_MAP_ROLE_DATA_FK1;

PROMPT alter table 'USER_ROLE_MAP'
ALTER TABLE ODB.USER_ROLE_MAP 
DROP CONSTRAINT USER_ROLE_MAP_ROLE_DATA_FK2;

PROMPT alter table 'USER_ROLE_MAP'
ALTER TABLE ODB.USER_ROLE_MAP 
DROP CONSTRAINT USER_ROLE_MAP_USER_DATA_FK1;

PROMPT drop table 'APP_CODES'
DROP TABLE ODB.APP_CODES CASCADE CONSTRAINTS;

PROMPT drop table 'FUNCTION_POINT'
DROP TABLE ODB.FUNCTION_POINT CASCADE CONSTRAINTS;

PROMPT drop table 'MBEAN_FP_MAP'
DROP TABLE ODB.MBEAN_FP_MAP CASCADE CONSTRAINTS;

PROMPT drop table 'ROLE_DATA'
DROP TABLE ODB.ROLE_DATA CASCADE CONSTRAINTS;

PROMPT drop table 'ROLE_FP_MAP'
DROP TABLE ODB.ROLE_FP_MAP CASCADE CONSTRAINTS;

PROMPT drop sequence 'SEQ_APP_CODE'
DROP SEQUENCE ODB.SEQ_APP_CODE;

PROMPT drop sequence 'SEQ_FUNCTION_POINT'
DROP SEQUENCE ODB.SEQ_FUNCTION_POINT;

PROMPT drop sequence 'SEQ_MBEAN_FP'
DROP SEQUENCE ODB.SEQ_MBEAN_FP;

PROMPT drop sequence 'SEQ_ROLE'
DROP SEQUENCE ODB.SEQ_ROLE;

PROMPT drop sequence 'SEQ_USER'
DROP SEQUENCE ODB.SEQ_USER;

PROMPT drop table 'USER_DATA'
DROP TABLE ODB.USER_DATA CASCADE CONSTRAINTS;

PROMPT drop table 'USER_ROLE_MAP'
DROP TABLE ODB.USER_ROLE_MAP CASCADE CONSTRAINTS;

PROMPT create table 'APP_CODES'
CREATE TABLE ODB.APP_CODES 
(
  APP_CODE_ID NUMBER NOT NULL 
, CODE_VALUE NUMBER NOT NULL 
, CODE_NAME VARCHAR2(20 BYTE) 
, SHORT_DESC VARCHAR2(50 BYTE) 
, LONG_DESC VARCHAR2(1000 BYTE) 
, AUDIT_USERID NUMBER 
, DM_LSTUPDDT TIMESTAMP(6) WITH LOCAL TIME ZONE NOT NULL 
, CONSTRAINT APP_CODES_PK PRIMARY KEY 
  (
    APP_CODE_ID 
  , CODE_VALUE 
  )
  USING INDEX 
  (
      CREATE UNIQUE INDEX ODB.APP_CODES_PK ON ODB.APP_CODES (APP_CODE_ID ASC, CODE_VALUE ASC) 
      LOGGING 
      TABLESPACE "USERS" 
      PCTFREE 10 
      INITRANS 2 
      STORAGE 
      ( 
        INITIAL 65536 
        MINEXTENTS 1 
        MAXEXTENTS UNLIMITED 
        BUFFER_POOL DEFAULT 
      ) 
  )
  ENABLE 
) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 1 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

CREATE TABLE ODB.FUNCTION_POINT 
(
  FUNCTION_POINT_ID NUMBER NOT NULL 
, DESCRIPTION VARCHAR2(1000 BYTE) NOT NULL 
, AUDIT_USERID NUMBER NOT NULL 
, DM_LSTUPDDT TIMESTAMP(6) WITH LOCAL TIME ZONE NOT NULL 
, FUNCTION_POINT_NAME VARCHAR2(100 BYTE) 
) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 1 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

CREATE TABLE ODB.MBEAN_FP_MAP 
(
  MBEAN_FP_ID NUMBER NOT NULL 
, MBEAN_NAME VARCHAR2(100 BYTE) NOT NULL 
, MBEAN_DESC VARCHAR2(1000 BYTE) 
, FUNCTION_POINT_ID NUMBER NOT NULL 
, AUDIT_USERID NUMBER NOT NULL 
, DM_LSTUPDDT TIMESTAMP(6) WITH LOCAL TIME ZONE 
) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 1 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

CREATE TABLE ODB.ROLE_DATA 
(
  ROLE_ID NUMBER NOT NULL 
, ROLE_DESC VARCHAR2(1000 BYTE) 
, AUDIT_USERID NUMBER NOT NULL 
, DMLSTUPDDT TIMESTAMP(6) WITH LOCAL TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL 
, ROLE_NAME VARCHAR2(200 BYTE) NOT NULL 
) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 1 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

CREATE TABLE ODB.ROLE_FP_MAP 
(
  ROLE_ID NUMBER NOT NULL 
, FUNCTION_POINT_ID NUMBER NOT NULL 
, AUDIT_USERID NUMBER NOT NULL 
, DM_LSTUPDDT TIMESTAMP(6) WITH LOCAL TIME ZONE 
, ACCESS_LEVEL_CD NUMBER 
, ACCESS_LEVEL NUMBER 
) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 1 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

CREATE TABLE ODB.USER_DATA 
(
  USER_ID NUMBER NOT NULL 
, USER_NAME VARCHAR2(20 BYTE) NOT NULL 
, PASSWORD VARCHAR2(20 BYTE) NOT NULL 
, AUDIT_USERID NUMBER NOT NULL 
, DMLSTUPDDT TIMESTAMP(6) WITH LOCAL TIME ZONE NOT NULL 
, VERSION VARCHAR2(20 BYTE) DEFAULT 1 NOT NULL 
) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 1 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

CREATE TABLE ODB.USER_ROLE_MAP 
(
  USER_ID NUMBER NOT NULL 
, ROLE_ID NUMBER NOT NULL 
, AUDIT_USERID NUMBER NOT NULL 
, DM_LSTUPDDT TIMESTAMP(6) WITH LOCAL TIME ZONE 
) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 1 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

PROMPT create index 'FUNCTION_POINT_PK'
CREATE UNIQUE INDEX ODB.FUNCTION_POINT_PK ON ODB.FUNCTION_POINT (FUNCTION_POINT_ID ASC) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 2 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

PROMPT create index 'MBEAN_FP_MAP_PK'
CREATE UNIQUE INDEX ODB.MBEAN_FP_MAP_PK ON ODB.MBEAN_FP_MAP (MBEAN_FP_ID ASC) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 2 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

PROMPT create index 'ROLE_DATA_PK'
CREATE UNIQUE INDEX ODB.ROLE_DATA_PK ON ODB.ROLE_DATA (ROLE_ID ASC) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 2 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

PROMPT create index 'ROLE_FP_MAP_PK'
CREATE UNIQUE INDEX ODB.ROLE_FP_MAP_PK ON ODB.ROLE_FP_MAP (ROLE_ID ASC, FUNCTION_POINT_ID ASC) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 2 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

PROMPT create index 'UNIQ_CONSTR_UNAME'
CREATE UNIQUE INDEX ODB.UNIQ_CONSTR_UNAME ON ODB.USER_DATA (USER_NAME ASC) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 2 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

PROMPT create index 'USER_DATA_PK'
CREATE UNIQUE INDEX ODB.USER_DATA_PK ON ODB.USER_DATA (USER_ID ASC) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 2 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

PROMPT create index 'UNIQ_USER_ROLE'
CREATE UNIQUE INDEX ODB.UNIQ_USER_ROLE ON ODB.USER_ROLE_MAP (USER_ID ASC, ROLE_ID ASC) 
LOGGING 
TABLESPACE "USERS" 
PCTFREE 10 
INITRANS 2 
STORAGE 
( 
  INITIAL 65536 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
);

ALTER TABLE ODB.FUNCTION_POINT
ADD CONSTRAINT FUNCTION_POINT_PK PRIMARY KEY 
(
  FUNCTION_POINT_ID 
)
USING INDEX FUNCTION_POINT_PK
ENABLE;

ALTER TABLE ODB.MBEAN_FP_MAP
ADD CONSTRAINT MBEAN_FP_MAP_PK PRIMARY KEY 
(
  MBEAN_FP_ID 
)
USING INDEX MBEAN_FP_MAP_PK
ENABLE;

ALTER TABLE ODB.ROLE_DATA
ADD CONSTRAINT ROLE_DATA_PK PRIMARY KEY 
(
  ROLE_ID 
)
USING INDEX ROLE_DATA_PK
ENABLE;

ALTER TABLE ODB.ROLE_FP_MAP
ADD CONSTRAINT ROLE_FP_MAP_PK PRIMARY KEY 
(
  ROLE_ID 
, FUNCTION_POINT_ID 
)
USING INDEX ROLE_FP_MAP_PK
ENABLE;

ALTER TABLE ODB.USER_DATA
ADD CONSTRAINT USER_DATA_PK PRIMARY KEY 
(
  USER_ID 
)
USING INDEX USER_DATA_PK
ENABLE;

ALTER TABLE ODB.USER_DATA
ADD CONSTRAINT UNIQ_CONSTR_UNAME UNIQUE 
(
  USER_NAME 
)
USING INDEX UNIQ_CONSTR_UNAME
ENABLE;

ALTER TABLE ODB.USER_ROLE_MAP
ADD CONSTRAINT UNIQ_USER_ROLE UNIQUE 
(
  USER_ID 
, ROLE_ID 
)
USING INDEX UNIQ_USER_ROLE
ENABLE;

ALTER TABLE ODB.MBEAN_FP_MAP
ADD CONSTRAINT MBEAN_FP_MAP_FUNCTION_POI_FK1 FOREIGN KEY
(
  FUNCTION_POINT_ID 
)
REFERENCES ODB.FUNCTION_POINT
(
  FUNCTION_POINT_ID 
)
ENABLE;

ALTER TABLE ODB.ROLE_FP_MAP
ADD CONSTRAINT ROLE_FP_MAP_FUNCTION_POIN_FK1 FOREIGN KEY
(
  FUNCTION_POINT_ID 
)
REFERENCES ODB.FUNCTION_POINT
(
  FUNCTION_POINT_ID 
)
ENABLE;

ALTER TABLE ODB.ROLE_FP_MAP
ADD CONSTRAINT ROLE_FP_MAP_ROLE_DATA_FK1 FOREIGN KEY
(
  ROLE_ID 
)
REFERENCES ODB.ROLE_DATA
(
  ROLE_ID 
)
ENABLE;

ALTER TABLE ODB.USER_ROLE_MAP
ADD CONSTRAINT USER_ROLE_MAP_ROLE_DATA_FK2 FOREIGN KEY
(
  ROLE_ID 
)
REFERENCES ODB.ROLE_DATA
(
  ROLE_ID 
)
ENABLE;

ALTER TABLE ODB.USER_ROLE_MAP
ADD CONSTRAINT USER_ROLE_MAP_USER_DATA_FK1 FOREIGN KEY
(
  USER_ID 
)
REFERENCES ODB.USER_DATA
(
  USER_ID 
)
ENABLE;

PROMPT create sequence 'SEQ_APP_CODE'
CREATE SEQUENCE ODB.SEQ_APP_CODE INCREMENT BY 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCACHE;

PROMPT create sequence 'SEQ_FUNCTION_POINT'
CREATE SEQUENCE ODB.SEQ_FUNCTION_POINT INCREMENT BY 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCACHE;

PROMPT create sequence 'SEQ_MBEAN_FP'
CREATE SEQUENCE ODB.SEQ_MBEAN_FP INCREMENT BY 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCACHE;

PROMPT create sequence 'SEQ_ROLE'
CREATE SEQUENCE ODB.SEQ_ROLE INCREMENT BY 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCACHE;

PROMPT create sequence 'SEQ_USER'
CREATE SEQUENCE ODB.SEQ_USER INCREMENT BY 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCACHE;

SPOOL OFF