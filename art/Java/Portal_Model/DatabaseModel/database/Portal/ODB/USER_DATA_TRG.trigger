-- <?xml version = '1.0' encoding = 'UTF-8'?>
-- <trigger xmlns="http://xmlns.oracle.com/jdeveloper/1112/offlinedb">
--   <enabled>true</enabled>
--   <properties>
--     <entry>
--       <key>TemplateObject</key>
--       <value class="oracle.javatools.db.NameBasedID">
--         <name>TEMPLATE_TRIGGER1</name>
--         <schemaName>TEMPLATES</schemaName>
--         <type>TRIGGER</type>
--       </value>
--     </entry>
--   </properties>
-- </trigger>

/*
 * To change this template comment, edit Portal.TEMPLATES.TEMPLATE_TRIGGER1
 */
CREATE OR REPLACE TRIGGER USER_DATA_TRG 
BEFORE INSERT ON USER_DATA 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF :NEW.USER_ID IS NULL THEN
      SELECT SEQ_USER.NEXTVAL INTO :NEW.USER_ID FROM DUAL;
    END IF;
  END COLUMN_SEQUENCES;
  NULL;
END;
/
