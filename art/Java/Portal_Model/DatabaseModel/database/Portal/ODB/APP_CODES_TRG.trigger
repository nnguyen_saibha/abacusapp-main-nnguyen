-- <?xml version = '1.0' encoding = 'UTF-8'?>
-- <trigger xmlns="http://xmlns.oracle.com/jdeveloper/1112/offlinedb">
--   <enabled>true</enabled>
--   <properties>
--     <entry>
--       <key>TemplateObject</key>
--       <value class="oracle.javatools.db.NameBasedID">
--         <name>TEMPLATE_TRIGGER1</name>
--         <schemaName>TEMPLATES</schemaName>
--         <type>TRIGGER</type>
--       </value>
--     </entry>
--   </properties>
-- </trigger>

/*
 * To change this template comment, edit Portal.TEMPLATES.TEMPLATE_TRIGGER1
 */
CREATE OR REPLACE TRIGGER APP_CODES_TRG 
BEFORE INSERT ON APP_CODES 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF :NEW.APP_CODE_ID IS NULL THEN
      SELECT SEQ_APP_CODE.NEXTVAL INTO :NEW.APP_CODE_ID FROM DUAL;
    END IF;
  END COLUMN_SEQUENCES;
  NULL;
END;
/
