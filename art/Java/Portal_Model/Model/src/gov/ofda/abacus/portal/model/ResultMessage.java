package gov.ofda.abacus.portal.model;

import java.io.Serializable;

public class ResultMessage implements Serializable{
    @SuppressWarnings("compatibility:-4272685522281666974")
    private static final long serialVersionUID = 1L;
    private boolean result=false;
    private String message;
    public ResultMessage() {
        super();
    }
    public ResultMessage(boolean res,String mes) {
        this.result=res;
        this.message=mes;
    }
    public void setResult(boolean result) {
        this.result = result;
    }

    public boolean isResult() {
        return result;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
   public String toString() {
       return(this.getMessage());
   }
}
