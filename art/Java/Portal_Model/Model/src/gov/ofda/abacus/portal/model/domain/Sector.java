package gov.ofda.abacus.portal.model.domain;

public class Sector implements Comparable<Sector>{
    private String sectorCode;
    private String sector;

    public void setSectorCode(String sectorCode) {
        this.sectorCode = sectorCode;
    }

    public String getSectorCode() {
        return sectorCode;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getSector() {
        return sector;
    }

    public Sector() {
        super();
    }
    public Sector(String sectorCode,String sector) {
        super();
        this.setSectorCode(sectorCode);;
        this.setSector(sector);
    }
    @Override
    public boolean equals(Object o) {
        if (o == this) {
                    return true;
                }
     if(o instanceof Sector) {
         Sector object=(Sector)o;
         if(object.getSectorCode()!=null && object.getSectorCode().equals(this.getSectorCode()))
            return true;
     }     else if(o instanceof String) {
                 if(o!=null && this.getSectorCode()!=null && this.getSectorCode().equals(o)) ;
                    return true;
              }
     return false;
    }
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + sectorCode.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return this.getSector();
    }

    @Override
    public int compareTo(Sector sector) {
        if(this.sector!=null && sector.sector!=null) {
            return this.sector.compareTo(sector.sector);
        }
        return 0;
    }
}
