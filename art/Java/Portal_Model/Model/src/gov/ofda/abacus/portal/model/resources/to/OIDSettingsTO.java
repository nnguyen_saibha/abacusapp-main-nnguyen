package gov.ofda.abacus.portal.model.resources.to;

public class OIDSettingsTO {

    private String initialContextFactory;
    private String providerURL;
    private String authenticationType;
    private String securityPrincipal;
    private char[] securityCredentials;

    public void setInitialContextFactory(String initialContextFactory) {
        this.initialContextFactory = initialContextFactory;
    }

    public String getInitialContextFactory() {
        return initialContextFactory;
    }


    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    public String getAuthenticationType() {
        return authenticationType;
    }

    public void setSecurityPrincipal(String securityPrincipal) {
        this.securityPrincipal = securityPrincipal;
    }

    public String getSecurityPrincipal() {
        return securityPrincipal;
    }

    public void setSecurityCredentials(char[] securityCredentials) {
        this.securityCredentials = securityCredentials;
    }

    public char[] getSecurityCredentials() {
        return securityCredentials;
    }

    public void setProviderURL(String providerURL) {
        this.providerURL = providerURL;
    }

    public String getProviderURL() {
        return providerURL;
    }
}
