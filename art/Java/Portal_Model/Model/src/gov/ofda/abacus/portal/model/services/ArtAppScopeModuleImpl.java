package gov.ofda.abacus.portal.model.services;

import gov.ofda.abacus.portal.model.framework.PortalApplicationModuleImpl;
import gov.ofda.abacus.portal.model.framework.PortalViewObjectImpl;


import java.util.HashMap;
import java.util.Map;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Jul 26 15:20:06 EDT 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ArtAppScopeModuleImpl extends PortalApplicationModuleImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public ArtAppScopeModuleImpl() {
    }

    /**
     * Container's getter for AbacusConstantsView1.
     * @return AbacusConstantsView1
     */
    public PortalViewObjectImpl getAbacusConstantsView1() {
        return (PortalViewObjectImpl) findViewObject("AbacusConstantsView1");
    }
    public Map<String,String> getAppConstants() {
        Map<String,String> tmp=new HashMap<String,String>();
        Map<String,String> appConstants=new HashMap<String,String>();
        ViewObject vo=this.getAbacusConstantsView1();
        RowSetIterator rsi=vo.createRowSetIterator(null);
        rsi.reset();
        while(rsi.hasNext()) {
                Row r=rsi.next();
                tmp.put((String)r.getAttribute("Code"), (String) r.getAttribute("CodeValue"));
            }
        rsi.closeRowSetIterator();
        appConstants.put("ES_HOST", tmp.get("ES_HOST"));
        appConstants.put("ES_PORT", tmp.get("ES_PORT"));
        appConstants.put("ES_PROTOCOL", tmp.get("ES_PROTOCOL"));
        return appConstants;
    }
    public Map<String,String> getArtConstants() {
        Map<String,String> tmp=new HashMap<String,String>();
        Map<String,String> appConstants=new HashMap<String,String>();
        ViewObject vo=this.getArtConstantsView1();
        RowSetIterator rsi=vo.createRowSetIterator(null);
        rsi.reset();
        while(rsi.hasNext()) {
                Row r=rsi.next();
                tmp.put((String)r.getAttribute("Code"), (String) r.getAttribute("CodeValue"));
            }
        rsi.closeRowSetIterator();
        appConstants.put("INSTANCE",tmp.get("INSTANCE"));
        appConstants.put("EMAIL_SERVER",tmp.get("EMAIL_SERVER"));
        appConstants.put("ABACUS_ADMIN_EMAIL",tmp.get("ABACUS_ADMIN"));
        appConstants.put("AAMP_URL",tmp.get("AAMP_URL"));
        appConstants.put("AAMP_APPLICATION_LINK",tmp.get("AAMP_APPLICATION_LINK"));
        appConstants.put("ABACUS_URL",tmp.get("ABACUS_URL"));
        appConstants.put("ABACUS_APPLICATION_LINK",tmp.get("ABACUS_APPLICATION_LINK"));
        appConstants.put("FROM_EMAIL",tmp.get("FROM_EMAIL"));
        appConstants.put("RESOURCES_URL",tmp.get("RESOURCES_URL"));
        appConstants.put("CHKLST_OF_REQD_ELMTS_URL",tmp.get("CHKLST_OF_REQD_ELMTS_URL"));
        appConstants.put("APPL_GUIDELINES_URL",tmp.get("APPL_GUIDELINES_URL"));
        appConstants.put("CONTRIBUTION_STAFF_EMAIL",tmp.get("CONTRIBUTION_STAFF_EMAIL"));
        appConstants.put("ABACUS_CONTRIBUTION_LINK",tmp.get("ABACUS_CONTRIBUTION_LINK"));
        
        switch(tmp.get("INSTANCE")) {
        case "OFDAP":
            appConstants.put("APP_NAME",  "AAMP");
            break;
        case "OFDAU":
            appConstants.put("APP_NAME",  "AAMP Testing");
            break;
        case "OFDAT":
                appConstants.put("APP_NAME",  "AAMP Training");
                break;
        case "OFDAD":
            appConstants.put("APP_NAME",  "AAMP Development");
            break;
        default:
            appConstants.put("APP_NAME",  "AAMP Unknown");
            appConstants.put("FROM_EMAIL","abacusadmin@ofda.gov");
        }
        return appConstants;
    }

    /**
     * Container's getter for ArtConstantsView1.
     * @return ArtConstantsView1
     */
    public PortalViewObjectImpl getArtConstantsView1() {
        return (PortalViewObjectImpl) findViewObject("ArtConstantsView1");
    }
}

