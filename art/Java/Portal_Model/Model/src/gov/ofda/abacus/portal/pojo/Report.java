package gov.ofda.abacus.portal.pojo;

import gov.ofda.abacus.portal.pojo.comparator.SectorComparator;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Report implements Serializable,Comparable<Report> {
    @SuppressWarnings("compatibility:3782198708482569409")
    private static final long serialVersionUID = 1L;
    private Long AWARD_ID;
    private Long PARSEQ;
    private Integer RPT_FY;
    private String RPT_TYPE_NAME;
    private Integer RPT_TYPE_CODE;
    private String PRD_TYPE_NAME;
    private Integer PRD_TYPE_CODE;
    private Date RPT_DUE_DATE;
    private String STATUS;
    private Date SUBMIT_DATE;
    private String IS_COMPLIANT;
    private Integer PAR_COUNT;
    private Integer RPT_NBR;
    private Integer SORT_ORDER;

    private Long BASELINE_VALUE;
    private Long BASELINE_IDP_VALUE;

    private Long CUM_TOTAL_TARGETED;
    private Long CUM_IDP_TARGETED;

    private Long TOTAL_REACHED;
    private Long IDP_REACHED;

    private Long CUM_TOTAL_REACHED;
    private Long CUM_IDP_REACHED;

    private List<Sector> SECTOR_DETAILS;
    private List<SubSectorIndicator> SUB_SECTOR_INDICATORS;
    private Long DUE_IN_DAYS;
    private Long CAL_CUM_TOTAL_REACHED;
    private Long CAL_CUM_IDP_REACHED;

    private Long PREV_CUM_TOTAL_REACHED;
    private Long PREV_CUM_IDP_REACHED;
    
    private Integer CUM_TOTAL_REACHED_PCT;
    private Integer CUM_IDP_REACHED_PCT;
    private Integer CAL_CUM_TOTAL_REACHED_PCT;
    private Integer CAL_CUM_IDP_REACHED_PCT;
    private Boolean calculated=false;
    public Report() {
        super();
    }

    public void setRPT_NBR(Integer RPT_NBR) {
        this.RPT_NBR = RPT_NBR;
    }

    public Integer getRPT_NBR() {
        return RPT_NBR;
    }

    public void setAWARD_ID(Long AWARD_ID) {
        this.AWARD_ID = AWARD_ID;
    }

    public Long getAWARD_ID() {
        return AWARD_ID;
    }

    public void setPARSEQ(Long PARSEQ) {
        this.PARSEQ = PARSEQ;
    }

    public Long getPARSEQ() {
        return PARSEQ;
    }

    public void setRPT_FY(Integer RPT_FY) {
        this.RPT_FY = RPT_FY;
    }

    public Integer getRPT_FY() {
        return RPT_FY;
    }

    public void setRPT_TYPE_NAME(String RPT_TYPE_NAME) {
        this.RPT_TYPE_NAME = RPT_TYPE_NAME;
    }

    public String getRPT_TYPE_NAME() {
        return RPT_TYPE_NAME;
    }

    public void setRPT_TYPE_CODE(Integer RPT_TYPE_CODE) {
        this.RPT_TYPE_CODE = RPT_TYPE_CODE;
    }

    public Integer getRPT_TYPE_CODE() {
        return RPT_TYPE_CODE;
    }

    public void setPRD_TYPE_NAME(String PRD_TYPE_NAME) {
        this.PRD_TYPE_NAME = PRD_TYPE_NAME;
    }

    public String getPRD_TYPE_NAME() {
        return PRD_TYPE_NAME;
    }

    public void setPRD_TYPE_CODE(Integer PRD_TYPE_CODE) {
        this.PRD_TYPE_CODE = PRD_TYPE_CODE;
    }

    public Integer getPRD_TYPE_CODE() {
        return PRD_TYPE_CODE;
    }

    public void setRPT_DUE_DATE(Date RPT_DUE_DATE) {
        this.RPT_DUE_DATE = RPT_DUE_DATE;
    }

    public Date getRPT_DUE_DATE() {
        return RPT_DUE_DATE;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSUBMIT_DATE(Date SUBMIT_DATE) {
        this.SUBMIT_DATE = SUBMIT_DATE;
    }

    public Date getSUBMIT_DATE() {
        return SUBMIT_DATE;
    }

    public void setIS_COMPLIANT(String IS_COMPLIANT) {
        this.IS_COMPLIANT = IS_COMPLIANT;
    }

    public String getIS_COMPLIANT() {
        return IS_COMPLIANT;
    }

    public void setPAR_COUNT(Integer PAR_COUNT) {
        this.PAR_COUNT = PAR_COUNT;
    }

    public Integer getPAR_COUNT() {
        return PAR_COUNT;
    }

    public void setSORT_ORDER(Integer SORT_ORDER) {
        this.SORT_ORDER = SORT_ORDER;
    }

    public Integer getSORT_ORDER() {
        return SORT_ORDER;
    }

    public void setBASELINE_VALUE(Long BASELINE_VALUE) {
        this.BASELINE_VALUE = BASELINE_VALUE;
    }

    public Long getBASELINE_VALUE() {
        if(this.SUBMIT_DATE==null)
            return null;
        return BASELINE_VALUE;
    }

    public void setBASELINE_IDP_VALUE(Long BASELINE_IDP_VALUE) {
        this.BASELINE_IDP_VALUE = BASELINE_IDP_VALUE;
    }

    public Long getBASELINE_IDP_VALUE() {
        if(this.SUBMIT_DATE==null)
            return null;
        return BASELINE_IDP_VALUE;
    }

    public void setCUM_TOTAL_TARGETED(Long CUM_TOTAL_TARGETED) {
        this.CUM_TOTAL_TARGETED = CUM_TOTAL_TARGETED;
    }

    public Long getCUM_TOTAL_TARGETED() {
        if(this.SUBMIT_DATE==null)
            return null;
        return CUM_TOTAL_TARGETED;
    }

    public void setCUM_IDP_TARGETED(Long CUM_IDP_TARGETED) {
        this.CUM_IDP_TARGETED = CUM_IDP_TARGETED;
    }

    public Long getCUM_IDP_TARGETED() {
        if(this.SUBMIT_DATE==null)
            return null;
        return CUM_IDP_TARGETED;
    }

    public void setTOTAL_REACHED(Long TOTAL_REACHED) {
        this.TOTAL_REACHED = TOTAL_REACHED;
    }

    public Long getTOTAL_REACHED() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        return TOTAL_REACHED;
    }

    public void setIDP_REACHED(Long IDP_REACHED) {
        this.IDP_REACHED = IDP_REACHED;
    }

    public Long getIDP_REACHED() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        return IDP_REACHED;
    }

    public void setCUM_TOTAL_REACHED(Long CUM_TOTAL_REACHED) {
        this.CUM_TOTAL_REACHED = CUM_TOTAL_REACHED;
    }

    public Long getCUM_TOTAL_REACHED() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        return CUM_TOTAL_REACHED;
    }

    public void setCUM_IDP_REACHED(Long CUM_IDP_REACHED) {
        this.CUM_IDP_REACHED = CUM_IDP_REACHED;
    }

    public Long getCUM_IDP_REACHED() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        return CUM_IDP_REACHED;
    }

    @Override
    public int compareTo(Report report) {
        return getRPT_DUE_DATE().compareTo(report.getRPT_DUE_DATE());
    }

    public void setSUB_SECTOR_INDICATORS(List<SubSectorIndicator> SUB_SECTOR_INDICATORS) {
        this.SUB_SECTOR_INDICATORS = SUB_SECTOR_INDICATORS;
    }

    public List<SubSectorIndicator> getSUB_SECTOR_INDICATORS() {
        return SUB_SECTOR_INDICATORS;
    }

    public void setDUE_IN_DAYS(Long DUE_IN_DAYS) {
        this.DUE_IN_DAYS = DUE_IN_DAYS;
    }

    public Long getDUE_IN_DAYS() {
        return DUE_IN_DAYS;
    }

    public void setCAL_CUM_TOTAL_REACHED(Long CAL_CUM_TOTAL_REACHED) {
        this.CAL_CUM_TOTAL_REACHED = CAL_CUM_TOTAL_REACHED;
    }

    public Long getCAL_CUM_TOTAL_REACHED() {
        return CAL_CUM_TOTAL_REACHED;
    }

    public void setCAL_CUM_IDP_REACHED(Long CAL_CUM_IDP_REACHED) {
        this.CAL_CUM_IDP_REACHED = CAL_CUM_IDP_REACHED;
    }

    public Long getCAL_CUM_IDP_REACHED() {
        return CAL_CUM_IDP_REACHED;
    }

    public void setPREV_CUM_TOTAL_REACHED(Long PREV_CUM_TOTAL_REACHED) {
        this.PREV_CUM_TOTAL_REACHED = PREV_CUM_TOTAL_REACHED;
    }

    public Long getPREV_CUM_TOTAL_REACHED() {
        return PREV_CUM_TOTAL_REACHED;
    }

    public void setPREV_CUM_IDP_REACHED(Long PREV_CUM_IDP_REACHED) {
        this.PREV_CUM_IDP_REACHED = PREV_CUM_IDP_REACHED;
    }

    public Long getPREV_CUM_IDP_REACHED() {
        return PREV_CUM_IDP_REACHED;
    }
    public void setSECTOR_DETAILS(List<Sector> SECTOR_DETAILS) {
        this.SECTOR_DETAILS = SECTOR_DETAILS;
    }

    public List<Sector> getSECTOR_DETAILS() {
        if(SECTOR_DETAILS!=null)
        {
            Collections.sort(SECTOR_DETAILS,new SectorComparator());
            for(Sector s:SECTOR_DETAILS) {
                s.setSUBMIT_DATE(SUBMIT_DATE);
                s.setRPT_TYPE_CODE(RPT_TYPE_CODE);
            }
        }
        return SECTOR_DETAILS;
    }
    public String getSECTOR() {
           return null;
       }

    public void setCUM_TOTAL_REACHED_PCT(Integer CUM_TOTAL_REACHED_PCT) {
        this.CUM_TOTAL_REACHED_PCT = CUM_TOTAL_REACHED_PCT;
    }

    public Integer getCUM_TOTAL_REACHED_PCT() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        calculatePercentage();
        return CUM_TOTAL_REACHED_PCT;
    }

    public void setCUM_IDP_REACHED_PCT(Integer CUM_IDP_REACHED_PCT) {
        this.CUM_IDP_REACHED_PCT = CUM_IDP_REACHED_PCT;
    }

    public Integer getCUM_IDP_REACHED_PCT() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        calculatePercentage();
        return CUM_IDP_REACHED_PCT;
    }

    public void setCAL_CUM_TOTAL_REACHED_PCT(Integer CAL_CUM_TOTAL_REACHED_PCT) {
        this.CAL_CUM_TOTAL_REACHED_PCT = CAL_CUM_TOTAL_REACHED_PCT;
    }

    public Integer getCAL_CUM_TOTAL_REACHED_PCT() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        calculatePercentage();
        return CAL_CUM_TOTAL_REACHED_PCT;
    }

    public void setCAL_CUM_IDP_REACHED_PCT(Integer CAL_CUM_IDP_REACHED_PCT) {
        this.CAL_CUM_IDP_REACHED_PCT = CAL_CUM_IDP_REACHED_PCT;
    }

    public Integer getCAL_CUM_IDP_REACHED_PCT() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        calculatePercentage();
        return CAL_CUM_IDP_REACHED_PCT;
    }

    private void calculatePercentage() {
        if(!calculated && this.SUBMIT_DATE!=null)
        {
            calculated=!calculated;
            if(CUM_TOTAL_TARGETED!=null && CUM_TOTAL_TARGETED>0)
            {
                if(CUM_TOTAL_REACHED!=null)
                {
                    Float r1=new Float(CUM_TOTAL_REACHED);
                    r1=(r1/CUM_TOTAL_TARGETED)*100;
                    CUM_TOTAL_REACHED_PCT=Math.round(r1);
                }
                if(CAL_CUM_TOTAL_REACHED!=null)
                {
                    Float r3=new Float(CAL_CUM_TOTAL_REACHED);
                    r3=(r3/CUM_TOTAL_TARGETED)*100;
                    CAL_CUM_TOTAL_REACHED_PCT=Math.round(r3);
                }
            }
            else
            {
                CUM_TOTAL_REACHED_PCT=null;
                CAL_CUM_TOTAL_REACHED_PCT=null;
            }
            if(CUM_IDP_TARGETED!=null && CUM_IDP_TARGETED>0) {
                
                if(CUM_IDP_REACHED!=null)
                {
                 Float r2=new Float(CUM_IDP_REACHED);
                 r2=(r2/CUM_IDP_TARGETED)*100;
                 CUM_IDP_REACHED_PCT=Math.round(r2);
                }
                if(CAL_CUM_IDP_REACHED!=null)
                {
                 Float r3=new Float(CAL_CUM_IDP_REACHED);
                 r3=(r3/CUM_IDP_TARGETED)*100;
                 CAL_CUM_IDP_REACHED_PCT=Math.round(r3);
                }
            }
            else {
                CUM_IDP_REACHED_PCT=null;
                CAL_CUM_IDP_REACHED_PCT=null;
            }
        }
    }
        
}
