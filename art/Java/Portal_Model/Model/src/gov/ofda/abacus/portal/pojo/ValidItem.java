package gov.ofda.abacus.portal.pojo;

import java.io.Serializable;

public class ValidItem implements Serializable {
    @SuppressWarnings("compatibility:-5887487148867991108")
    private static final long serialVersionUID = 1L;

    public ValidItem() {
        super();
    }
    private String fname;
    private String lname;

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getFname() {
        return fname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getLname() {
        return lname;
    }
}
