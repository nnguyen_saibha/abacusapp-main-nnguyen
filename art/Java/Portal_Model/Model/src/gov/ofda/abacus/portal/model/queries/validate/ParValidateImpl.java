package gov.ofda.abacus.portal.model.queries.validate;

import gov.ofda.abacus.portal.model.framework.PortalViewObjectImpl;

import gov.ofda.abacus.portal.model.resources.StoredProcedureUtil;

import java.sql.CallableStatement;
import java.sql.ResultSet;

import java.sql.SQLException;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.JboException;
import oracle.jbo.server.DBTransaction;
import oracle.jbo.server.SQLBuilder;
import oracle.jbo.server.ViewRowImpl;
import oracle.jbo.server.ViewRowSetImpl;

import oracle.jdbc.internal.OracleTypes;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Dec 31 11:24:56 EST 2012
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ParValidateImpl extends PortalViewObjectImpl {
    ADFLogger _LOG = ADFLogger.createADFLogger(ParValidateImpl.class);

    /**
     * This is the default constructor (do not remove).
     */
    public ParValidateImpl() {
    }

    /**
     * executeQueryForCollection - overridden for custom java data source support.
     */
    protected void executeQueryForCollection(Object qc, Object[] params, int noUserParams) {
        _LOG.info("in executeQueryForCollection");
        storeNewResultSet(qc, retrieveRefCursor(qc, params));
        super.executeQueryForCollection(qc, params, noUserParams);
    }

    /**
     * hasNextForCollection - overridden for custom java data source support.
     */
    protected boolean hasNextForCollection(Object qc) {
        ResultSet res = (ResultSet)getUserDataForCollection(qc);

        boolean nextOne = false;
        if (res != null) {
            try {
                nextOne = res.next();
                if (!nextOne) {
                    setFetchCompleteForCollection(qc, true);
                    res.close();
                }
            } catch (SQLException s) {
                throw new JboException(s);
            }
        }
        return nextOne;
    }

    /**
     * createRowFromResultSet - overridden for custom java data source support.
     */
    protected ViewRowImpl createRowFromResultSet(Object qc, ResultSet resultSet) {
        resultSet = (ResultSet)getUserDataForCollection(qc);

        // Create a new row to populate
        ViewRowImpl r = createNewRowForCollection(qc);
        try {
            // Populate new row by attribute slot number for current row in Result Set
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("Parseq"), resultSet.getString("PARSEQ"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("Parseq"),
                                                      StoredProcedureUtil.retrieveDBSequence(resultSet.getLong("PARSEQ")));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("RptNbr"), resultSet.getString("RPT_NBR"));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("Awid"), resultSet.getString("AWID"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("Awid"),
                                                     StoredProcedureUtil.retrieveInteger(resultSet.getInt("AWID")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("RptTypeCode"),resultSet.getString("RPT_TYPE_CODE"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("RptTypeCode"),
                                                     StoredProcedureUtil.retrieveInteger(resultSet.getInt("RPT_TYPE_CODE")));
           this.populateAttributeForRow(r, this.getAttributeIndexOf("PrdTypeCode"),
                                                     StoredProcedureUtil.retrieveInteger(resultSet.getInt("PRD_TYPE_CODE")));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("PrdRptFy"),
                                                     StoredProcedureUtil.retrieveInteger(resultSet.getInt("PRD_RPT_FY")));

            //this.populateAttributeForRow(r, this.getAttributeIndexOf("RptDueDate"),resultSet.getString("RPT_DUE_DATE")); 
            this.populateAttributeForRow(r, this.getAttributeIndexOf("RptDueDate"),
                                                     StoredProcedureUtil.retrieveJBOTimestamp(resultSet.getDate("RPT_DUE_DATE")));
           
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("PrdStartDate"), resultSet.getString("PRD_START_DATE"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("PrdStartDate"),
                                                     StoredProcedureUtil.retrieveJBOTimestamp(resultSet.getDate("PRD_START_DATE")));
            
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("PrdEndDate"), resultSet.getString("PRD_END_DATE"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("PrdEndDate"),
                                                     StoredProcedureUtil.retrieveJBOTimestamp(resultSet.getDate("PRD_END_DATE")));
            
            this.populateAttributeForRow(r, this.getAttributeIndexOf("RptDesc"), resultSet.getString("RPT_DESC"));
            
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("CumTotalTargeted"), resultSet.getString("CUM_TOTAL_TARGETED"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("CumTotalTargeted"),
                                                     StoredProcedureUtil.retrieveLong(resultSet.getLong("CUM_TOTAL_TARGETED")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("isCumTotalTargeted"), resultSet.getString("IS_CUM_TOTAL_TARGETED"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isCumTotalTargeted"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("IS_CUM_TOTAL_TARGETED")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("CumIdpTargeted"), resultSet.getString("CUM_IDP_TARGETED"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("CumIdpTargeted"),
                                                     StoredProcedureUtil.retrieveLong(resultSet.getLong("CUM_IDP_TARGETED")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("isCumIdpTargeted"), resultSet.getString("IS_CUM_IDP_TARGETED"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isCumIdpTargeted"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("IS_CUM_IDP_TARGETED")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("CumTotalReached"), resultSet.getString("CUM_TOTAL_REACHED"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("CumTotalReached"),
                                                     StoredProcedureUtil.retrieveLong(resultSet.getLong("CUM_TOTAL_REACHED")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("isCumTotalReached"), resultSet.getString("IS_CUM_TOTAL_REACHED"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isCumTotalReached"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("IS_CUM_TOTAL_REACHED")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("CumIdpReached"), resultSet.getString("CUM_IDP_REACHED"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("CumIdpReached"),
                                                     StoredProcedureUtil.retrieveLong(resultSet.getLong("CUM_IDP_REACHED")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("isCumIdpReached"), resultSet.getString("IS_CUM_IDP_REACHED"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isCumIdpReached"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("IS_CUM_IDP_REACHED")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("TotalReached"), resultSet.getString("TOTAL_REACHED"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("TotalReached"),
                                                     StoredProcedureUtil.retrieveLong(resultSet.getLong("TOTAL_REACHED")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("isTotalReached"), resultSet.getString("IS_TOTAL_REACHED"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isTotalReached"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("IS_TOTAL_REACHED")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("IdpReached"), resultSet.getString("IDP_REACHED"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("IdpReached"),
                                                     StoredProcedureUtil.retrieveLong(resultSet.getLong("IDP_REACHED")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("isIdpReached"), resultSet.getString("IS_IDP_REACHED"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isIdpReached"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("IS_IDP_REACHED")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("PrevRptAmt"), resultSet.getString("PREV_RPT_AMT"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("PrevRptAmt"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("PREV_RPT_AMT")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("isPrevRptAmt"), resultSet.getString("IS_PREV_RPT_AMT"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isPrevRptAmt"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("IS_PREV_RPT_AMT")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("FedPrevRptAmt"), resultSet.getString("FED_PREV_RPT_AMT"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("FedPrevRptAmt"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("FED_PREV_RPT_AMT")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("isFedPrevRptAmt"), resultSet.getString("IS_FED_PREV_RPT_AMT"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isFedPrevRptAmt"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("IS_FED_PREV_RPT_AMT")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("PrdAmt"), resultSet.getString("PRD_AMT"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("PrdAmt"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("PRD_AMT")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("isPrdAmt"), resultSet.getString("IS_PRD_AMT"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isPrdAmt"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("IS_PRD_AMT")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("FedPrdAmt"), resultSet.getString("FED_PRD_AMT"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("FedPrdAmt"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("FED_PRD_AMT")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("isFedPrdAmt"), resultSet.getString("IS_FED_PRD_AMT"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isFedPrdAmt"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("IS_FED_PRD_AMT")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("CumAmt"), resultSet.getString("CUM_AMT"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("CumAmt"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getLong("CUM_AMT")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("isCumAmt"), resultSet.getString("IS_CUM_AMT"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isCumAmt"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_CUM_AMT")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("FedCumAmt"), resultSet.getString("FED_CUM_AMT"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("FedCumAmt"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("FED_CUM_AMT")));
            //this.populateAttributeForRow(r, this.getAttributeIndexOf("isFedCumAmt"), resultSet.getString("IS_FED_CUM_AMT"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isFedCumAmt"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_FED_CUM_AMT")));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isPrdTypeCode"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_PRD_TYPE_CODE")));
            
            this.populateAttributeForRow(r, this.getAttributeIndexOf("BaselineValue"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("BASELINE_VALUE")));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isBaselineValue"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_BASELINE_VALUE")));            
            this.populateAttributeForRow(r, this.getAttributeIndexOf("Limitations"), resultSet.getString("LIMITATIONS"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isLimitations"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_LIMITATIONS")));
            
            this.populateAttributeForRow(r, this.getAttributeIndexOf("Analysis"), resultSet.getString("ANALYSIS"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isAnalysis"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_ANALYSIS")));
            
            this.populateAttributeForRow(r, this.getAttributeIndexOf("LocationTiming"), resultSet.getString("LOCATION_TIMING"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isLocationTiming"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_LOCATION_TIMING")));
            
            this.populateAttributeForRow(r, this.getAttributeIndexOf("MethodsSources"), resultSet.getString("METHODS_SOURCES"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isMethodsSources"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_METHODS_SOURCES")));
            
            this.populateAttributeForRow(r, this.getAttributeIndexOf("EvalReq"), resultSet.getString("EVAL_REQ"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isEvalReq"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_EVAL_REQ")));
            
            this.populateAttributeForRow(r, this.getAttributeIndexOf("EvalAuthor"), resultSet.getString("EVAL_AUTHOR"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isEvalAuthor"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_EVAL_AUTHOR")));
            
            this.populateAttributeForRow(r, this.getAttributeIndexOf("EvalTypeId"), resultSet.getString("EVAL_TYPE_ID"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isEvalTypeId"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_EVAL_TYPE_ID")));
            
            this.populateAttributeForRow(r, this.getAttributeIndexOf("EvalPurposeId"), resultSet.getString("EVAL_PURPOSE_ID"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isEvalPurposeId"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_EVAL_PURPOSE_ID")));
            
            this.populateAttributeForRow(r, this.getAttributeIndexOf("EvalPurposeOther"), resultSet.getString("EVAL_PURPOSE_OTHER"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isEvalPurposeOther"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_EVAL_PURPOSE_OTHER")));
            
            this.populateAttributeForRow(r, this.getAttributeIndexOf("EvalCompltDate"), resultSet.getString("EVAL_COMPLT_DATE"));
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isEvalCompltDate"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_EVAL_COMPLT_DATE")));
            
            this.populateAttributeForRow(r, this.getAttributeIndexOf("isEvalSectors"),
                                                     StoredProcedureUtil.retrieveJBONumber(resultSet.getInt("IS_EVAL_SECTORS")));
        } catch (SQLException s) {
            throw new JboException(s);
        }
        return r;

    }

    /**
     * getQueryHitCount - overridden for custom java data source support.
     */
    public long getQueryHitCount(ViewRowSetImpl viewRowSet) {
        long value = super.getQueryHitCount(viewRowSet);
        return value;
    }

    @Override
    protected void create() {
        this.getViewDef().setQuery(null);
        this.getViewDef().setSelectClause(null);
        this.setQuery(null);
    }

    @Override
    protected void releaseUserDataForCollection(Object qc, Object rs) {

        ResultSet userDataRS = (ResultSet)getUserDataForCollection(qc);
        if (userDataRS != null) {
            try {
                userDataRS.close();
            } catch (SQLException s) {
                throw new JboException(s);
            }
        }
        super.releaseUserDataForCollection(qc, rs);
    }

    private void storeNewResultSet(Object qc, ResultSet rs) {
        ResultSet existingRs = (ResultSet)getUserDataForCollection(qc);

        // If this query collection is getting reused, close out any previous rowset
        if (existingRs != null) {
            try {
                existingRs.close();
            } catch (SQLException e) {
                throw new JboException(e);
            }
        }
        setUserDataForCollection(qc, rs);
        hasNextForCollection(qc); // Prime the pump with the first row.
    }

    private ResultSet retrieveRefCursor(Object qc, Object[] params) {
        ResultSet resultSet = null;
        String methodCallString = "begin ? := appx_pkg.is_report_complete_award(?); end;";

        CallableStatement st = null;
        DBTransaction transaction = this.getDBTransaction();
        try {
            Long parSeq = (Long)getNamedBindParamValue("parSeq", params);
            st = transaction.createCallableStatement(methodCallString, 0);
            st.setLong(2, parSeq);
            st.registerOutParameter(1, OracleTypes.CURSOR);

            //st.executeUpdate();
            st.execute();

            resultSet = (ResultSet)st.getObject(1);
        } catch (SQLException e) {
            _LOG.warning("Error in fetching: ", e);
        }

        return resultSet;
    }

    private Object getNamedBindParamValue(String varName, Object[] params) {
        Object result = null;
        if (getBindingStyle() == SQLBuilder.BINDING_STYLE_ORACLE_NAME) {
            if (params != null) {
                for (Object param : params) {
                    Object[] nameValue = (Object[])param;
                    String name = (String)nameValue[0];
                    if (name.equals(varName)) {
                        return (Long)nameValue[1];
                    }
                }
            }
        }
        throw new JboException("No bind variable named '" + varName + "'");
    }

    /**
     * Returns the bind variable value for parSeq.
     * @return bind variable value for parSeq
     */
    public Long getparSeq() {
        return (Long)getNamedWhereClauseParam("parSeq");
    }

    /**
     * Sets <code>value</code> for bind variable parSeq.
     * @param value value to bind as parSeq
     */
    public void setparSeq(Long value) {
        setNamedWhereClauseParam("parSeq", value);
    }
}
