package gov.ofda.abacus.portal.model.queries.appl;

import gov.ofda.abacus.portal.model.framework.PortalViewRowImpl;

import java.math.BigDecimal;

import oracle.adf.share.ADFContext;

import oracle.adf.share.security.SecurityContext;

import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Jul 08 14:40:42 EDT 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ApplValidStatusLOVRowImpl extends PortalViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        UserType {
            public Object get(ApplValidStatusLOVRowImpl obj) {
                return obj.getUserType();
            }

            public void put(ApplValidStatusLOVRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        ApplType {
            public Object get(ApplValidStatusLOVRowImpl obj) {
                return obj.getApplType();
            }

            public void put(ApplValidStatusLOVRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        ButtonLabel {
            public Object get(ApplValidStatusLOVRowImpl obj) {
                return obj.getButtonLabel();
            }

            public void put(ApplValidStatusLOVRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        StatusCode {
            public Object get(ApplValidStatusLOVRowImpl obj) {
                return obj.getStatusCode();
            }

            public void put(ApplValidStatusLOVRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        StatusLabel {
            public Object get(ApplValidStatusLOVRowImpl obj) {
                return obj.getStatusLabel();
            }

            public void put(ApplValidStatusLOVRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        StatusConfirmMsg {
            public Object get(ApplValidStatusLOVRowImpl obj) {
                return obj.getStatusConfirmMsg();
            }

            public void put(ApplValidStatusLOVRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        CommentsMsg {
            public Object get(ApplValidStatusLOVRowImpl obj) {
                return obj.getCommentsMsg();
            }

            public void put(ApplValidStatusLOVRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        StatusErrorMsg {
            public Object get(ApplValidStatusLOVRowImpl obj) {
                return obj.getStatusErrorMsg();
            }

            public void put(ApplValidStatusLOVRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        UserRolesList {
            public Object get(ApplValidStatusLOVRowImpl obj) {
                return obj.getUserRolesList();
            }

            public void put(ApplValidStatusLOVRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        IsEnabled {
            public Object get(ApplValidStatusLOVRowImpl obj) {
                return obj.getIsEnabled();
            }

            public void put(ApplValidStatusLOVRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        PopupMsg {
            public Object get(ApplValidStatusLOVRowImpl obj) {
                return obj.getPopupMsg();
            }

            public void put(ApplValidStatusLOVRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        SortOrder {
            public Object get(ApplValidStatusLOVRowImpl obj) {
                return obj.getSortOrder();
            }

            public void put(ApplValidStatusLOVRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        IsVisible {
            public Object get(ApplValidStatusLOVRowImpl obj) {
                return obj.getIsVisible();
            }

            public void put(ApplValidStatusLOVRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(ApplValidStatusLOVRowImpl object);

        public abstract void put(ApplValidStatusLOVRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int USERTYPE = AttributesEnum.UserType.index();
    public static final int APPLTYPE = AttributesEnum.ApplType.index();
    public static final int BUTTONLABEL = AttributesEnum.ButtonLabel.index();
    public static final int STATUSCODE = AttributesEnum.StatusCode.index();
    public static final int STATUSLABEL = AttributesEnum.StatusLabel.index();
    public static final int STATUSCONFIRMMSG = AttributesEnum.StatusConfirmMsg.index();
    public static final int COMMENTSMSG = AttributesEnum.CommentsMsg.index();
    public static final int STATUSERRORMSG = AttributesEnum.StatusErrorMsg.index();
    public static final int USERROLESLIST = AttributesEnum.UserRolesList.index();
    public static final int ISENABLED = AttributesEnum.IsEnabled.index();
    public static final int POPUPMSG = AttributesEnum.PopupMsg.index();
    public static final int SORTORDER = AttributesEnum.SortOrder.index();
    public static final int ISVISIBLE = AttributesEnum.IsVisible.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ApplValidStatusLOVRowImpl() {
    }


    /**
     * Gets the attribute value for the calculated attribute UserType.
     * @return the UserType
     */
    public String getUserType() {
        return (String) getAttributeInternal(USERTYPE);
    }

    /**
     * Gets the attribute value for the calculated attribute ApplType.
     * @return the ApplType
     */
    public String getApplType() {
        return (String) getAttributeInternal(APPLTYPE);
    }

    /**
     * Gets the attribute value for the calculated attribute ButtonLabel.
     * @return the ButtonLabel
     */
    public String getButtonLabel() {
        return (String) getAttributeInternal(BUTTONLABEL);
    }

    /**
     * Gets the attribute value for the calculated attribute StatusCode.
     * @return the StatusCode
     */
    public String getStatusCode() {
        return (String) getAttributeInternal(STATUSCODE);
    }

    /**
     * Gets the attribute value for the calculated attribute StatusLabel.
     * @return the StatusLabel
     */
    public String getStatusLabel() {
        return (String) getAttributeInternal(STATUSLABEL);
    }

    /**
     * Gets the attribute value for the calculated attribute StatusConfirmMsg.
     * @return the StatusConfirmMsg
     */
    public String getStatusConfirmMsg() {
        return (String) getAttributeInternal(STATUSCONFIRMMSG);
    }

    /**
     * Gets the attribute value for the calculated attribute CommentsMsg.
     * @return the CommentsMsg
     */
    public String getCommentsMsg() {
        return (String) getAttributeInternal(COMMENTSMSG);
    }

    /**
     * Gets the attribute value for the calculated attribute StatusErrorMsg.
     * @return the StatusErrorMsg
     */
    public String getStatusErrorMsg() {
        return (String) getAttributeInternal(STATUSERRORMSG);
    }

    /**
     * Gets the attribute value for the calculated attribute UserRolesList.
     * @return the UserRolesList
     */
    public String getUserRolesList() {
        return (String) getAttributeInternal(USERROLESLIST);
    }

    /**
     * Gets the attribute value for the calculated attribute IsEnabled.
     * @return the IsEnabled
     */
    public String getIsEnabled() {
        return (String) getAttributeInternal(ISENABLED);
    }

    /**
     * Gets the attribute value for the calculated attribute SortOrder.
     * @return the SortOrder
     */
    public BigDecimal getSortOrder() {
        return (BigDecimal) getAttributeInternal(SORTORDER);
    }

    /**
     * Gets the attribute value for the calculated attribute PopupMsg.
     * @return the PopupMsg
     */
    public String getPopupMsg() {
        return (String) getAttributeInternal(POPUPMSG);
    }

    /**
     * Gets the attribute value for the calculated attribute IsVisible.
     * @return the IsVisible
     */
    public Boolean getIsVisible() {
        if(this.getUserRolesList()==null)
            return false;
        String[] t=this.getUserRolesList().split(",");
        SecurityContext ctx=ADFContext.getCurrent().getSecurityContext();
        for(String i:t) {
            if(ctx.isUserInRole(i))
                return true;
        }
        //return (Boolean) getAttributeInternal(ISVISIBLE);
        return false;
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

