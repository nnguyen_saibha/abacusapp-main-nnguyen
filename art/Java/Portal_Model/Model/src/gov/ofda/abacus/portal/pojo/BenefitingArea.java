package gov.ofda.abacus.portal.pojo;

import java.io.Serializable;

public class BenefitingArea implements Serializable{
    @SuppressWarnings("compatibility:-7245603246596702191")
    private static final long serialVersionUID = 1L;
    private String code;
    private String regionName;
    private String countryName;
    private String regionCode;
    private String countryCode;
    
    public BenefitingArea() {
        super();
    }
    public BenefitingArea(String code,String regionName,String countryName,String regionCode,String countryCode) {
        super();
        this.code=code;
        this.regionName=regionName;
        this.countryName=countryName;
        this.regionCode=regionCode;
        this.countryCode=countryCode;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
                    return true;
                }
     if(o instanceof BenefitingArea) {
         BenefitingArea object=(BenefitingArea)o;
         if(object.getCode()!=null && object.getCode().equals(this.getCode()))
            return true;
     }
     return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + code.hashCode();
        return result;
    }

    @Override
    public String toString() {
        
        return code;
    }
}
