package gov.ofda.abacus.portal.model.framework;

import oracle.jbo.NoDefException;
import oracle.jbo.Row;
import oracle.jbo.Variable;
import oracle.jbo.VariableManager;
import oracle.jbo.ViewCriteriaItem;
import oracle.jbo.server.Entity;
import oracle.jbo.server.ViewDefImpl;
import oracle.jbo.server.ViewObjectImpl;

public class PortalViewObjectImpl extends ViewObjectImpl {
    /**
     * Retruns the row status.
     * Initialized, Modified, Unmodified or New
     * @param row
     * @return status
     */
    public String getRowStatus(Row row) {
        PortalViewRowImpl rwImpl=(PortalViewRowImpl)row;
        String rwStatus=translateStatusToString(rwImpl.getEntity(0).getEntityState());
    return rwStatus;
    }
//Code taken from http://www.oracle.com/technetwork/developer-tools/adf/learnmore/06-cancelform-java-169125.pdf
     private String translateStatusToString(byte b) {
         String ret = null; 
         switch (b) 
         { case Entity.STATUS_INITIALIZED:  { ret = "Initialized"; break; } 
           case Entity.STATUS_MODIFIED:     { ret = "Modified"; break; } 
           case Entity.STATUS_UNMODIFIED:   { ret = "Unmodified"; break; } 
           case Entity.STATUS_NEW:          { ret = "New"; break; } 
         } 
      return ret; 
    }
    private boolean hasViewDefVariableNamed(String name) {
           boolean ret = false;
           VariableManager viewDefVarMgr = getViewDef().ensureVariableManager();
           if (viewDefVarMgr != null) {
               try {
                   ret = viewDefVarMgr.findVariable(name) != null;
               }
               catch (NoDefException ex) {
                   // ignore
               }
           }
           return ret;
       }

       public void clearWhereState() {
           ViewDefImpl viewDef = getViewDef();
           Variable[] viewInstanceVars = null;
           VariableManager viewInstanceVarMgr = ensureVariableManager();
           if (viewInstanceVarMgr != null) {
               viewInstanceVars = viewInstanceVarMgr.getVariablesOfKind(Variable.VAR_KIND_WHERE_CLAUSE_PARAM);
               if (viewInstanceVars != null) {
                   for (Variable v: viewInstanceVars) {
                       // only remove the variable if its not on the view def.
                       if (!hasViewDefVariableNamed(v.getName())) {
                         removeNamedWhereClauseParam(v.getName());
                       }
                   }
               }
           }
           getDefaultRowSet().setExecuteParameters(null, null, true);
           setWhereClause(null);
           getDefaultRowSet().setWhereClauseParams(null);
       }
}
