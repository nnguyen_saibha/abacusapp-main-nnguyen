package gov.ofda.abacus.portal.model.entities.appl;

import gov.ofda.abacus.portal.model.framework.PortalEntityImpl;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;

import oracle.jbo.Key;
import oracle.jbo.domain.BlobDomain;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.TransactionEvent;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Jul 31 13:56:21 EDT 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PacpDocsImpl extends PortalEntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        PacpDocsId {
            public Object get(PacpDocsImpl obj) {
                return obj.getPacpDocsId();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setPacpDocsId((DBSequence) value);
            }
        }
        ,
        PaaId {
            public Object get(PacpDocsImpl obj) {
                return obj.getPaaId();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setPaaId((BigDecimal) value);
            }
        }
        ,
        DocTypeCode {
            public Object get(PacpDocsImpl obj) {
                return obj.getDocTypeCode();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setDocTypeCode((String) value);
            }
        }
        ,
        FileFormat {
            public Object get(PacpDocsImpl obj) {
                return obj.getFileFormat();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setFileFormat((String) value);
            }
        }
        ,
        FileName {
            public Object get(PacpDocsImpl obj) {
                return obj.getFileName();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setFileName((String) value);
            }
        }
        ,
        FileDesc {
            public Object get(PacpDocsImpl obj) {
                return obj.getFileDesc();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setFileDesc((String) value);
            }
        }
        ,
        FileContentType {
            public Object get(PacpDocsImpl obj) {
                return obj.getFileContentType();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setFileContentType((String) value);
            }
        }
        ,
        FileSize {
            public Object get(PacpDocsImpl obj) {
                return obj.getFileSize();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setFileSize((String) value);
            }
        }
        ,
        FileContents {
            public Object get(PacpDocsImpl obj) {
                return obj.getFileContents();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setFileContents((BlobDomain) value);
            }
        }
        ,
        FileLocation {
            public Object get(PacpDocsImpl obj) {
                return obj.getFileLocation();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setFileLocation((String) value);
            }
        }
        ,
        Comments {
            public Object get(PacpDocsImpl obj) {
                return obj.getComments();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setComments((String) value);
            }
        }
        ,
        Description {
            public Object get(PacpDocsImpl obj) {
                return obj.getDescription();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setDescription((String) value);
            }
        }
        ,
        UpdDate {
            public Object get(PacpDocsImpl obj) {
                return obj.getUpdDate();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        UpdId {
            public Object get(PacpDocsImpl obj) {
                return obj.getUpdId();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        Paa {
            public Object get(PacpDocsImpl obj) {
                return obj.getPaa();
            }

            public void put(PacpDocsImpl obj, Object value) {
                obj.setPaa((PaaImpl) value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(PacpDocsImpl object);

        public abstract void put(PacpDocsImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int PACPDOCSID = AttributesEnum.PacpDocsId.index();
    public static final int PAAID = AttributesEnum.PaaId.index();
    public static final int DOCTYPECODE = AttributesEnum.DocTypeCode.index();
    public static final int FILEFORMAT = AttributesEnum.FileFormat.index();
    public static final int FILENAME = AttributesEnum.FileName.index();
    public static final int FILEDESC = AttributesEnum.FileDesc.index();
    public static final int FILECONTENTTYPE = AttributesEnum.FileContentType.index();
    public static final int FILESIZE = AttributesEnum.FileSize.index();
    public static final int FILECONTENTS = AttributesEnum.FileContents.index();
    public static final int FILELOCATION = AttributesEnum.FileLocation.index();
    public static final int COMMENTS = AttributesEnum.Comments.index();
    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int PAA = AttributesEnum.Paa.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PacpDocsImpl() {
    }

    /**
     * Gets the attribute value for PacpDocsId, using the alias name PacpDocsId.
     * @return the value of PacpDocsId
     */
    public DBSequence getPacpDocsId() {
        return (DBSequence) getAttributeInternal(PACPDOCSID);
    }

    /**
     * Sets <code>value</code> as the attribute value for PacpDocsId.
     * @param value value to set the PacpDocsId
     */
    public void setPacpDocsId(DBSequence value) {
        setAttributeInternal(PACPDOCSID, value);
    }

    /**
     * Gets the attribute value for PaaId, using the alias name PaaId.
     * @return the value of PaaId
     */
    public BigDecimal getPaaId() {
        return (BigDecimal) getAttributeInternal(PAAID);
    }

    /**
     * Sets <code>value</code> as the attribute value for PaaId.
     * @param value value to set the PaaId
     */
    public void setPaaId(BigDecimal value) {
        setAttributeInternal(PAAID, value);
    }

    /**
     * Gets the attribute value for DocTypeCode, using the alias name DocTypeCode.
     * @return the value of DocTypeCode
     */
    public String getDocTypeCode() {
        return (String) getAttributeInternal(DOCTYPECODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for DocTypeCode.
     * @param value value to set the DocTypeCode
     */
    public void setDocTypeCode(String value) {
        setAttributeInternal(DOCTYPECODE, value);
    }

    /**
     * Gets the attribute value for FileFormat, using the alias name FileFormat.
     * @return the value of FileFormat
     */
    public String getFileFormat() {
        return (String) getAttributeInternal(FILEFORMAT);
    }

    /**
     * Sets <code>value</code> as the attribute value for FileFormat.
     * @param value value to set the FileFormat
     */
    public void setFileFormat(String value) {
        setAttributeInternal(FILEFORMAT, value);
    }

    /**
     * Gets the attribute value for FileName, using the alias name FileName.
     * @return the value of FileName
     */
    public String getFileName() {
        return (String) getAttributeInternal(FILENAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for FileName.
     * @param value value to set the FileName
     */
    public void setFileName(String value) {
        setAttributeInternal(FILENAME, value);
    }

    /**
     * Gets the attribute value for FileDesc, using the alias name FileDesc.
     * @return the value of FileDesc
     */
    public String getFileDesc() {
        return (String) getAttributeInternal(FILEDESC);
    }

    /**
     * Sets <code>value</code> as the attribute value for FileDesc.
     * @param value value to set the FileDesc
     */
    public void setFileDesc(String value) {
        setAttributeInternal(FILEDESC, value);
    }

    /**
     * Gets the attribute value for FileContentType, using the alias name FileContentType.
     * @return the value of FileContentType
     */
    public String getFileContentType() {
        return (String) getAttributeInternal(FILECONTENTTYPE);
    }

    /**
     * Sets <code>value</code> as the attribute value for FileContentType.
     * @param value value to set the FileContentType
     */
    public void setFileContentType(String value) {
        setAttributeInternal(FILECONTENTTYPE, value);
    }

    /**
     * Gets the attribute value for FileSize, using the alias name FileSize.
     * @return the value of FileSize
     */
    public String getFileSize() {
        return (String) getAttributeInternal(FILESIZE);
    }

    /**
     * Sets <code>value</code> as the attribute value for FileSize.
     * @param value value to set the FileSize
     */
    public void setFileSize(String value) {
        setAttributeInternal(FILESIZE, value);
    }

    /**
     * Gets the attribute value for FileContents, using the alias name FileContents.
     * @return the value of FileContents
     */
    public BlobDomain getFileContents() {
        return (BlobDomain) getAttributeInternal(FILECONTENTS);
    }

    /**
     * Sets <code>value</code> as the attribute value for FileContents.
     * @param value value to set the FileContents
     */
    public void setFileContents(BlobDomain value) {
        setAttributeInternal(FILECONTENTS, value);
    }

    /**
     * Gets the attribute value for FileLocation, using the alias name FileLocation.
     * @return the value of FileLocation
     */
    public String getFileLocation() {
        return (String) getAttributeInternal(FILELOCATION);
    }

    /**
     * Sets <code>value</code> as the attribute value for FileLocation.
     * @param value value to set the FileLocation
     */
    public void setFileLocation(String value) {
        setAttributeInternal(FILELOCATION, value);
    }

    /**
     * Gets the attribute value for Comments, using the alias name Comments.
     * @return the value of Comments
     */
    public String getComments() {
        return (String) getAttributeInternal(COMMENTS);
    }

    /**
     * Sets <code>value</code> as the attribute value for Comments.
     * @param value value to set the Comments
     */
    public void setComments(String value) {
        setAttributeInternal(COMMENTS, value);
    }

    /**
     * Gets the attribute value for Description, using the alias name Description.
     * @return the value of Description
     */
    public String getDescription() {
        return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as the attribute value for Description.
     * @param value value to set the Description
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for UpdDate, using the alias name UpdDate.
     * @return the value of UpdDate
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }

    /**
     * Gets the attribute value for UpdId, using the alias name UpdId.
     * @return the value of UpdId
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }

    /**
     * @return the associated entity PaaImpl.
     */
    public PaaImpl getPaa() {
        return (PaaImpl) getAttributeInternal(PAA);
    }

    /**
     * Sets <code>value</code> as the associated entity PaaImpl.
     */
    public void setPaa(PaaImpl value) {
        setAttributeInternal(PAA, value);
    }

    /**
     * @param pacpDocsId key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(DBSequence pacpDocsId) {
        return new Key(new Object[] { pacpDocsId });
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("gov.ofda.abacus.portal.model.entities.appl.PacpDocs");
    }
    
    @Override
    protected void prepareForDML(int i, TransactionEvent transactionEvent) {
        if(i==DML_UPDATE || i==DML_INSERT)
        {
        ADFContext adfCtx = ADFContext.getCurrent();  
        SecurityContext secCntx = adfCtx.getSecurityContext();  
        String user = secCntx.getUserName(); 
        if(user!=null)
            setAttributeInternal(UPDID,user.toUpperCase());
        }
        super.prepareForDML(i, transactionEvent);
    }
}

