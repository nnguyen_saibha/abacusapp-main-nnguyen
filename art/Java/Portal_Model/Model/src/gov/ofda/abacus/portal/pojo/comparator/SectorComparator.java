package gov.ofda.abacus.portal.pojo.comparator;

import gov.ofda.abacus.portal.pojo.Sector;

import java.util.Comparator;

public class SectorComparator implements Comparator<Sector>{
    public SectorComparator() {
        super();
    }

    @Override
    public int compare(Sector sector1, Sector sector2) {
           if(sector1!=null)
            return (sector1.getSECTOR().compareTo(sector2.getSECTOR()));
           return 0;
        
    }
}