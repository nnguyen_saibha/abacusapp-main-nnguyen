package gov.ofda.abacus.portal.pojo;

import java.io.Serializable;


public class SubSectorIndicator implements Serializable{
    @SuppressWarnings("compatibility:-6663482680643501649")
    private static final long serialVersionUID = 1L;
    private Long    Parseq;
    private Long    Parsiseq;
    private Long    Parsectorseq;
    private String  SectorCode;
    private String  Sector;
    private String  SubsectorCode;
    private String  Subsector;
    private String  IndicatorCode;
    private String  IndicatorName;
    private String  IndTypePath;
    private String  Level1;
    private String  Level2;
    private String  Level3;
    private String  Level4;
    private String  Level5;
    private String  Isnew;
    private String  Isofda;
    private String  IndicatorType;
    private String  Uom1;
    private Double  Value1Baseline;
    private Double  Value1Targeted;
    private Double  Value1Reached;
    private String  Uom2;
    private Double  Value2Baseline;
    private Double  Value2Targeted;
    private Double  Value2Reached;
    private Double  CumValue1Reached;
    private Double  CumValue1Targeted;
    private Double  CumValue2Reached;
    private Double  CumValue2Targeted;
    private Integer IndFlg;
    private Integer IndLevel;
    private String  Isapplicable;

    public SubSectorIndicator() {
        super();
    }

    public void setParseq(Long Parseq) {
        this.Parseq = Parseq;
    }

    public Long getParseq() {
        return Parseq;
    }

    public void setParsiseq(Long Parsiseq) {
        this.Parsiseq = Parsiseq;
    }

    public Long getParsiseq() {
        return Parsiseq;
    }

    public void setParsectorseq(Long Parsectorseq) {
        this.Parsectorseq = Parsectorseq;
    }

    public Long getParsectorseq() {
        return Parsectorseq;
    }

    public void setSectorCode(String SectorCode) {
        this.SectorCode = SectorCode;
    }

    public String getSectorCode() {
        return SectorCode;
    }

    public void setSector(String Sector) {
        this.Sector = Sector;
    }

    public String getSector() {
        return Sector;
    }

    public void setSubsectorCode(String SubsectorCode) {
        this.SubsectorCode = SubsectorCode;
    }

    public String getSubsectorCode() {
        return SubsectorCode;
    }

    public void setSubsector(String Subsector) {
        this.Subsector = Subsector;
    }

    public String getSubsector() {
        return Subsector;
    }

    public void setIndicatorCode(String IndicatorCode) {
        this.IndicatorCode = IndicatorCode;
    }

    public String getIndicatorCode() {
        return IndicatorCode;
    }

    public void setIndicatorName(String IndicatorName) {
        this.IndicatorName = IndicatorName;
    }

    public String getIndicatorName() {
        return IndicatorName;
    }

    public void setIndTypePath(String IndTypePath) {
        this.IndTypePath = IndTypePath;
    }

    public String getIndTypePath() {
        return IndTypePath;
    }

    public void setLevel1(String Level1) {
        this.Level1 = Level1;
    }

    public String getLevel1() {
        return Level1;
    }

    public void setLevel2(String Level2) {
        this.Level2 = Level2;
    }

    public String getLevel2() {
        return Level2;
    }

    public void setLevel3(String Level3) {
        this.Level3 = Level3;
    }

    public String getLevel3() {
        return Level3;
    }

    public void setLevel4(String Level4) {
        this.Level4 = Level4;
    }

    public String getLevel4() {
        return Level4;
    }

    public void setLevel5(String Level5) {
        this.Level5 = Level5;
    }

    public String getLevel5() {
        return Level5;
    }

    public void setIsnew(String Isnew) {
        this.Isnew = Isnew;
    }

    public String getIsnew() {
        return Isnew;
    }

    public void setIsofda(String Isofda) {
        this.Isofda = Isofda;
    }

    public String getIsofda() {
        return Isofda;
    }

    public void setIndicatorType(String IndicatorType) {
        this.IndicatorType = IndicatorType;
    }

    public String getIndicatorType() {
        return IndicatorType;
    }

    public void setUom1(String Uom1) {
        this.Uom1 = Uom1;
    }

    public String getUom1() {
        return Uom1;
    }

    public void setValue1Baseline(Double Value1Baseline) {
        this.Value1Baseline = Value1Baseline;
    }

    public Double getValue1Baseline() {
        return Value1Baseline;
    }

    public void setValue1Targeted(Double Value1Targeted) {
        this.Value1Targeted = Value1Targeted;
    }

    public Double getValue1Targeted() {
        return Value1Targeted;
    }

    public void setValue1Reached(Double Value1Reached) {
        this.Value1Reached = Value1Reached;
    }

    public Double getValue1Reached() {
        return Value1Reached;
    }

    public void setUom2(String Uom2) {
        this.Uom2 = Uom2;
    }

    public String getUom2() {
        return Uom2;
    }

    public void setValue2Baseline(Double Value2Baseline) {
        this.Value2Baseline = Value2Baseline;
    }

    public Double getValue2Baseline() {
        return Value2Baseline;
    }

    public void setValue2Targeted(Double Value2Targeted) {
        this.Value2Targeted = Value2Targeted;
    }

    public Double getValue2Targeted() {
        return Value2Targeted;
    }

    public void setValue2Reached(Double Value2Reached) {
        this.Value2Reached = Value2Reached;
    }

    public Double getValue2Reached() {
        return Value2Reached;
    }

    public void setCumValue1Reached(Double CumValue1Reached) {
        this.CumValue1Reached = CumValue1Reached;
    }

    public Double getCumValue1Reached() {
        return CumValue1Reached;
    }

    public void setCumValue1Targeted(Double CumValue1Targeted) {
        this.CumValue1Targeted = CumValue1Targeted;
    }

    public Double getCumValue1Targeted() {
        return CumValue1Targeted;
    }

    public void setCumValue2Reached(Double CumValue2Reached) {
        this.CumValue2Reached = CumValue2Reached;
    }

    public Double getCumValue2Reached() {
        return CumValue2Reached;
    }

    public void setCumValue2Targeted(Double CumValue2Targeted) {
        this.CumValue2Targeted = CumValue2Targeted;
    }

    public Double getCumValue2Targeted() {
        return CumValue2Targeted;
    }

    public void setIndFlg(Integer IndFlg) {
        this.IndFlg = IndFlg;
    }

    public Integer getIndFlg() {
        return IndFlg;
    }

    public void setIndLevel(Integer IndLevel) {
        this.IndLevel = IndLevel;
    }

    public Integer getIndLevel() {
        return IndLevel;
    }

    public void setIsapplicable(String Isapplicable) {
        this.Isapplicable = Isapplicable;
    }

    public String getIsapplicable() {
        return Isapplicable;
    }
}
