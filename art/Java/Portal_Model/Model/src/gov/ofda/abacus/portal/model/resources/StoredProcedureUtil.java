package gov.ofda.abacus.portal.model.resources;

import oracle.jbo.domain.DBSequence;

public class StoredProcedureUtil {
    public StoredProcedureUtil() {
        super();
    }

    public static oracle.jbo.domain.Timestamp retrieveJBOTimestamp(java.sql.Date date) {
        oracle.jbo.domain.Timestamp ts = null;
        ts = new oracle.jbo.domain.Timestamp(date);
        return ts;
    }

    public static Integer retrieveInteger(int value) {
        Integer intObj = null;
        if(value>0){
            intObj = new Integer(value);
        }        
        return intObj;
    }
    
    public static Long retrieveLong(long value) {
        Long longObj = null;
        if(value>0l){
            longObj = new Long(value);
        }
       
        return longObj;
    }
    
    public static oracle.jbo.domain.DBSequence retrieveDBSequence(long value) {
        Long longObj = null;
        oracle.jbo.domain.DBSequence sequence = null;
        if(value>0l){
            longObj = new Long(value);
            sequence = new DBSequence(longObj);
        }
        
        return sequence;
    }
    
    public static oracle.jbo.domain.Number retrieveJBONumber(long value){
        Long longObj = null;
        oracle.jbo.domain.Number jboNumber = null;
        if(value>0l){
            longObj = new Long(value);
            jboNumber = new oracle.jbo.domain.Number(longObj);
        }
        
        return jboNumber;
    }

}
