package gov.ofda.abacus.portal.pojo;

import java.io.Serializable;

import java.util.List;

public class ApplValidItem implements Serializable{
    @SuppressWarnings("compatibility:1388811096688442705")
    private static final long serialVersionUID = 1L;

    public ApplValidItem() {
        super();
    }
    private Long type;
    private List<ValidItem> General;
    private List<ValidItem> Budget;
    private List<ValidItem> Beneficiaries;
    private List<ValidItem> Documents;
    private Boolean isValid;
    private Boolean isGeneralValid;
    private Boolean isBudgetValid;
    private Boolean isBeneficiariesValid;
    private Boolean isDocumentsValid;

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    public Boolean getIsValid() {
        if(isValid==null) {
            if(General!=null && Budget!=null && Beneficiaries!=null && Documents!=null && General.size()==0 && Budget.size()==0&&Beneficiaries.size()==0&&Documents.size()==0)
                isValid=true;
            else
                isValid=false;
        }
        return isValid;
    }

    public void setIsGeneralValid(Boolean isGeneralValid) {
        this.isGeneralValid = isGeneralValid;
    }

    public Boolean getIsGeneralValid() {
        if(isGeneralValid==null) {
            if(General!=null && General.size()==0)
                isGeneralValid=true;
            else
                isGeneralValid=false;
        }
        return isGeneralValid;
    }

    public void setIsBudgetValid(Boolean isBudgetValid) {
        this.isBudgetValid = isBudgetValid;
    }

    public Boolean getIsBudgetValid() {
        if(isBudgetValid==null) {
            if(Budget!=null && Budget.size()==0)
                isBudgetValid=true;
            else
                isBudgetValid=false;
        }
        return isBudgetValid;
    }

    public void setIsBeneficiariesValid(Boolean isBeneficiariesValid) {
        this.isBeneficiariesValid = isBeneficiariesValid;
    }

    public Boolean getIsBeneficiariesValid() {
        if(isBeneficiariesValid==null) {
            if(Beneficiaries!=null && Beneficiaries.size()==0)
                isBeneficiariesValid=true;
            else
                isBeneficiariesValid=false;
        }
        return isBeneficiariesValid;
    }

    public void setIsDocumentsValid(Boolean isDocumentsValid) {
        this.isDocumentsValid = isDocumentsValid;
    }

    public Boolean getIsDocumentsValid() {
        if(isDocumentsValid==null) {
            if(Documents!=null && Documents.size()==0)
                isDocumentsValid=true;
            else
                isDocumentsValid=false;
        }
        return isDocumentsValid;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getType() {
        return type;
    }

    public void setGeneral(List<ValidItem> General) {
        this.General = General;
    }

    public List<ValidItem> getGeneral() {
        return General;
    }

    public void setBudget(List<ValidItem> Budget) {
        this.Budget = Budget;
    }

    public List<ValidItem> getBudget() {
        return Budget;
    }

    public void setBeneficiaries(List<ValidItem> Beneficiaries) {
        this.Beneficiaries = Beneficiaries;
    }

    public List<ValidItem> getBeneficiaries() {
        return Beneficiaries;
    }

    public void setDocuments(List<ValidItem> Documents) {
        this.Documents = Documents;
    }

    public List<ValidItem> getDocuments() {
        return Documents;
    }
}
