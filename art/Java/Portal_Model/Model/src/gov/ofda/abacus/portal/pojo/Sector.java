package gov.ofda.abacus.portal.pojo;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

import oracle.security.xmlsec.saml2.ac.Protection;

public class Sector implements Serializable {
    @SuppressWarnings("compatibility:2481752859776008525")
      private static final long serialVersionUID = 1L;
      private Long BASELINE_VALUE_IDP;
      private Long CUM_BENE_IDP_REACHED;
      private Long BASELINE_VALUE;
      private String SECTOR_CODE;
      private String ISAPPLICABLE;
      private String SECTOR;
      private Long CUM_BENE_TOT_TARGET;
      private Long  BENE_IDP_REACHED;
      private Long CUM_BENE_TOT_REACHED;
      private Long PARSECTORSEQ;
      private Long CUM_BENE_IDP_TARGET;
      private Long BENE_TOT_REACHED;
      private Integer RPT_TYPE_CODE;
      private Date SUBMIT_DATE;
      private Integer CUM_TOTAL_REACHED_PCT;
      private Integer CUM_IDP_REACHED_PCT;
      private Boolean calculated=false;
    public Sector() {
        super();
    }

    public void setBASELINE_VALUE_IDP(Long BASELINE_VALUE_IDP) {
        this.BASELINE_VALUE_IDP = BASELINE_VALUE_IDP;
    }

    public Long getBASELINE_VALUE_IDP() {
        if(this.SUBMIT_DATE==null)
            return null;
        return BASELINE_VALUE_IDP;
    }

    public void setCUM_BENE_IDP_REACHED(Long CUM_BENE_IDP_REACHED) {
        this.CUM_BENE_IDP_REACHED = CUM_BENE_IDP_REACHED;
    }

    public Long getCUM_BENE_IDP_REACHED() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        return CUM_BENE_IDP_REACHED;
    }

    public void setBASELINE_VALUE(Long BASELINE_VALUE) {
        this.BASELINE_VALUE = BASELINE_VALUE;
    }

    public Long getBASELINE_VALUE() {
        if(this.SUBMIT_DATE==null )
            return null;
        return BASELINE_VALUE;
    }

    public void setSECTOR_CODE(String SECTOR_CODE) {
        this.SECTOR_CODE = SECTOR_CODE;
    }

    public String getSECTOR_CODE() {
        return SECTOR_CODE;
    }

    public void setISAPPLICABLE(String ISAPPLICABLE) {
        this.ISAPPLICABLE = ISAPPLICABLE;
    }

    public String getISAPPLICABLE() {
        return ISAPPLICABLE;
    }

    public void setSECTOR(String SECTOR) {
        this.SECTOR = SECTOR;
    }

    public String getSECTOR() {
        return SECTOR;
    }

    public void setCUM_BENE_TOT_TARGET(Long CUM_BENE_TOT_TARGET) {
        this.CUM_BENE_TOT_TARGET = CUM_BENE_TOT_TARGET;
    }

    public Long getCUM_BENE_TOT_TARGET() {
        if(this.SUBMIT_DATE==null)
            return null;
        return CUM_BENE_TOT_TARGET;
    }

    public void setBENE_IDP_REACHED(Long BENE_IDP_REACHED) {
        this.BENE_IDP_REACHED = BENE_IDP_REACHED;
    }

    public Long getBENE_IDP_REACHED() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        return BENE_IDP_REACHED;
    }

    public void setCUM_BENE_TOT_REACHED(Long CUM_BENE_TOT_REACHED) {
        this.CUM_BENE_TOT_REACHED = CUM_BENE_TOT_REACHED;
    }

    public Long getCUM_BENE_TOT_REACHED() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        return CUM_BENE_TOT_REACHED;
    }

    public void setPARSECTORSEQ(Long PARSECTORSEQ) {
        this.PARSECTORSEQ = PARSECTORSEQ;
    }

    public Long getPARSECTORSEQ() {
        return PARSECTORSEQ;
    }

    public void setCUM_BENE_IDP_TARGET(Long CUM_BENE_IDP_TARGET) {
        this.CUM_BENE_IDP_TARGET = CUM_BENE_IDP_TARGET;
    }

    public Long getCUM_BENE_IDP_TARGET() {
        if(this.SUBMIT_DATE==null)
            return null;
        return CUM_BENE_IDP_TARGET;
    }

    public void setBENE_TOT_REACHED(Long BENE_TOT_REACHED) {
        this.BENE_TOT_REACHED = BENE_TOT_REACHED;
    }

    public Long getBENE_TOT_REACHED() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        return BENE_TOT_REACHED;
    }
    public Long getAWARD_ID() {
        return null;
    }


    public Long getPARSEQ() {
        return null;
    }


    public Integer getRPT_FY() {
        return null;
    }
    public Integer getRPT_NBR() {
        return null;
    }

    public String getRPT_TYPE_NAME() {
        return null;
    }

    public Integer getRPT_TYPE_CODE() {
        return null;
    }


    public String getPRD_TYPE_NAME() {
        return null;
    }


    public Integer getPRD_TYPE_CODE() {
        return null;
    }


    public Date getRPT_DUE_DATE() {
        return null;
    }


    public String getSTATUS() {
        return null;
    }


    public Date getSUBMIT_DATE() {
        return null;
    }


    public String getIS_COMPLIANT() {
        return null;
    }


    public Integer getPAR_COUNT() {
        return null;
    }


    public Integer getSORT_ORDER() {
        return null;
    }


    public Long getBASELINE_IDP_VALUE() {
        if(this.SUBMIT_DATE==null)
            return null;
        return BASELINE_VALUE_IDP;
    }


    public Long getCUM_TOTAL_TARGETED() {
        if(this.SUBMIT_DATE==null)
            return null;
        return CUM_BENE_TOT_TARGET;
    }

    public Long getCUM_IDP_TARGETED() {
        if(this.SUBMIT_DATE==null)
            return null;
        return CUM_BENE_IDP_TARGET;
    }


    public Long getTOTAL_REACHED() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        return BENE_TOT_REACHED;
    }


    public Long getIDP_REACHED() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        return BENE_IDP_REACHED;
    }


    public Long getCUM_TOTAL_REACHED() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        return CUM_BENE_TOT_REACHED;
    }


    public Long getCUM_IDP_REACHED() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        return CUM_BENE_IDP_REACHED;
    }

    public void setDUE_IN_DAYS(Long DUE_IN_DAYS) {
        this.DUE_IN_DAYS = DUE_IN_DAYS;
    }

    public Long getDUE_IN_DAYS() {
        return DUE_IN_DAYS;
    }
    private Long DUE_IN_DAYS;
    
    private List<Sector> SECTOR_DETAILS;


    public void setSECTOR_DETAILS(List<Sector> SECTOR_DETAILS) {
        this.SECTOR_DETAILS = SECTOR_DETAILS;
    }

    public List<Sector> getSECTOR_DETAILS() {
        return SECTOR_DETAILS;
    }
    public Long getCAL_CUM_TOTAL_REACHED() {
        return null;
    }

    public Long getCAL_CUM_IDP_REACHED() {
        return null;
    }

    public void setRPT_TYPE_CODE(Integer RPT_TYPE_CODE) {
        this.RPT_TYPE_CODE = RPT_TYPE_CODE;
    }

    public Integer getRPT_TYPE_CODE1() {
        return RPT_TYPE_CODE;
    }

    public void setSUBMIT_DATE(Date SUBMIT_DATE) {
        this.SUBMIT_DATE = SUBMIT_DATE;
    }

    public Date getSUBMIT_DATE1() {
        return SUBMIT_DATE;
    }
    
    public Integer getCAL_CUM_TOTAL_REACHED_PCT() {
            return null;
    }

    public Integer getCAL_CUM_IDP_REACHED_PCT() {
            return null;

    }

    public void setCUM_TOTAL_REACHED_PCT(Integer CUM_TOTAL_REACHED_PCT) {
        this.CUM_TOTAL_REACHED_PCT = CUM_TOTAL_REACHED_PCT;
    }

    public Integer getCUM_TOTAL_REACHED_PCT() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        calculatePercentage();
        return CUM_TOTAL_REACHED_PCT;
    }

    public void setCUM_IDP_REACHED_PCT(Integer CUM_IDP_REACHED_PCT) {
        this.CUM_IDP_REACHED_PCT = CUM_IDP_REACHED_PCT;
    }

    public Integer getCUM_IDP_REACHED_PCT() {
        if(this.SUBMIT_DATE==null || this.RPT_TYPE_CODE!=1)
            return null;
        calculatePercentage();
        return CUM_IDP_REACHED_PCT;
    }
    
    private void calculatePercentage() {
        if(!calculated)
        {
            calculated=!calculated;
            if(CUM_BENE_TOT_TARGET!=null && CUM_BENE_TOT_TARGET>0)
            {
                Float r1=new Float(CUM_BENE_TOT_REACHED);
                r1=(r1/CUM_BENE_TOT_TARGET)*100;
                CUM_TOTAL_REACHED_PCT=Math.round(r1);
                           

            }
            else
            {
                CUM_TOTAL_REACHED_PCT=null;
            }
            if(CUM_BENE_IDP_TARGET!=null && CUM_BENE_IDP_TARGET>0) {
                Float r2=new Float(CUM_BENE_IDP_REACHED);
                r2=(r2/CUM_BENE_IDP_TARGET)*100;
                CUM_IDP_REACHED_PCT=Math.round(r2);
                
            }
            else {
                CUM_IDP_REACHED_PCT=null;
            }
        }
    }

}
