package gov.ofda.abacus.portal.model.entityViews;

import gov.ofda.abacus.portal.model.framework.PortalEntityImpl;
import gov.ofda.abacus.portal.model.framework.PortalViewRowImpl;

import oracle.jbo.Row;
import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.domain.BlobDomain;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.domain.Date;
import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Nov 10 11:57:20 EST 2011
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class RptDocsViewRowImpl extends PortalViewRowImpl {


    public static final int ENTITY_ABACUSXDOCS = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. Do not modify.
     */
    public enum AttributesEnum {
        DocTypeCode {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getDocTypeCode();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setDocTypeCode((String)value);
            }
        }
        ,
        Docseq {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getDocseq();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setDocseq((DBSequence)value);
            }
        }
        ,
        FileContentType {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getFileContentType();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setFileContentType((String)value);
            }
        }
        ,
        FileContents {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getFileContents();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setFileContents((BlobDomain)value);
            }
        }
        ,
        FileDesc {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getFileDesc();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setFileDesc((String)value);
            }
        }
        ,
        FileFormat {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getFileFormat();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setFileFormat((String)value);
            }
        }
        ,
        FileName {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getFileName();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setFileName((String)value);
            }
        }
        ,
        Parseq {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getParseq();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setParseq((Integer)value);
            }
        }
        ,
        UpdDate {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getUpdDate();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setUpdDate((Date)value);
            }
        }
        ,
        UpdId {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getUpdId();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setUpdId((String)value);
            }
        }
        ,
        FileSize {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getFileSize();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setFileSize((String)value);
            }
        }
        ,
        isEditable {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getisEditable();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setisEditable((String)value);
            }
        }
        ,
        ParDocsView {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getParDocsView();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        ParView {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getParView();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        ParView1 {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getParView1();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        DocTypeLookupLOV1 {
            public Object get(RptDocsViewRowImpl obj) {
                return obj.getDocTypeLookupLOV1();
            }

            public void put(RptDocsViewRowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static int firstIndex = 0;

        public abstract Object get(RptDocsViewRowImpl object);

        public abstract void put(RptDocsViewRowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int DOCTYPECODE = AttributesEnum.DocTypeCode.index();
    public static final int DOCSEQ = AttributesEnum.Docseq.index();
    public static final int FILECONTENTTYPE = AttributesEnum.FileContentType.index();
    public static final int FILECONTENTS = AttributesEnum.FileContents.index();
    public static final int FILEDESC = AttributesEnum.FileDesc.index();
    public static final int FILEFORMAT = AttributesEnum.FileFormat.index();
    public static final int FILENAME = AttributesEnum.FileName.index();
    public static final int PARSEQ = AttributesEnum.Parseq.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int FILESIZE = AttributesEnum.FileSize.index();
    public static final int ISEDITABLE = AttributesEnum.isEditable.index();
    public static final int PARDOCSVIEW = AttributesEnum.ParDocsView.index();
    public static final int PARVIEW = AttributesEnum.ParView.index();
    public static final int PARVIEW1 = AttributesEnum.ParView1.index();
    public static final int DOCTYPELOOKUPLOV1 = AttributesEnum.DocTypeLookupLOV1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public RptDocsViewRowImpl() {
    }
    /**
     * Overrides default method to check if attribute is updatable depending
     * on the status of report. uses getisEditable() to check if it is updatable.
     * @param i
     * @return
     */
    public boolean isAttributeUpdateable(int i) {
    if(this.getisEditable().toString().equals("N"))
            return false;
        else
         return super.isAttributeUpdateable(i);
     
    }
    /**
     * Gets AbacusxDocs entity object.
     * @return the AbacusxDocs
     */
    public PortalEntityImpl getAbacusxDocs() {
        return (PortalEntityImpl)getEntity(ENTITY_ABACUSXDOCS);
    }

    /**
     * Gets the attribute value for DOC_TYPE_CODE using the alias name DocTypeCode.
     * @return the DOC_TYPE_CODE
     */
    public String getDocTypeCode() {
        return (String) getAttributeInternal(DOCTYPECODE);
    }

    /**
     * Sets <code>value</code> as attribute value for DOC_TYPE_CODE using the alias name DocTypeCode.
     * @param value value to set the DOC_TYPE_CODE
     */
    public void setDocTypeCode(String value) {
        setAttributeInternal(DOCTYPECODE, value);
    }

    /**
     * Gets the attribute value for DOCSEQ using the alias name Docseq.
     * @return the DOCSEQ
     */
    public DBSequence getDocseq() {
        return (DBSequence)getAttributeInternal(DOCSEQ);
    }

    /**
     * Sets <code>value</code> as attribute value for DOCSEQ using the alias name Docseq.
     * @param value value to set the DOCSEQ
     */
    public void setDocseq(DBSequence value) {
        setAttributeInternal(DOCSEQ, value);
    }

    /**
     * Gets the attribute value for FILE_CONTENT_TYPE using the alias name FileContentType.
     * @return the FILE_CONTENT_TYPE
     */
    public String getFileContentType() {
        return (String) getAttributeInternal(FILECONTENTTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for FILE_CONTENT_TYPE using the alias name FileContentType.
     * @param value value to set the FILE_CONTENT_TYPE
     */
    public void setFileContentType(String value) {
        setAttributeInternal(FILECONTENTTYPE, value);
    }

    /**
     * Gets the attribute value for FILE_CONTENTS using the alias name FileContents.
     * @return the FILE_CONTENTS
     */
    public BlobDomain getFileContents() {
        return (BlobDomain) getAttributeInternal(FILECONTENTS);
    }

    /**
     * Sets <code>value</code> as attribute value for FILE_CONTENTS using the alias name FileContents.
     * @param value value to set the FILE_CONTENTS
     */
    public void setFileContents(BlobDomain value) {
        setAttributeInternal(FILECONTENTS, value);
    }

    /**
     * Gets the attribute value for FILE_DESC using the alias name FileDesc.
     * @return the FILE_DESC
     */
    public String getFileDesc() {
        return (String) getAttributeInternal(FILEDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for FILE_DESC using the alias name FileDesc.
     * @param value value to set the FILE_DESC
     */
    public void setFileDesc(String value) {
        setAttributeInternal(FILEDESC, value);
    }

    /**
     * Gets the attribute value for FILE_FORMAT using the alias name FileFormat.
     * @return the FILE_FORMAT
     */
    public String getFileFormat() {
        return (String) getAttributeInternal(FILEFORMAT);
    }

    /**
     * Sets <code>value</code> as attribute value for FILE_FORMAT using the alias name FileFormat.
     * @param value value to set the FILE_FORMAT
     */
    public void setFileFormat(String value) {
        setAttributeInternal(FILEFORMAT, value);
    }

    /**
     * Gets the attribute value for FILE_NAME using the alias name FileName.
     * @return the FILE_NAME
     */
    public String getFileName() {
        return (String) getAttributeInternal(FILENAME);
    }

    /**
     * Sets <code>value</code> as attribute value for FILE_NAME using the alias name FileName.
     * @param value value to set the FILE_NAME
     */
    public void setFileName(String value) {
        setAttributeInternal(FILENAME, value);
    }

    /**
     * Gets the attribute value for PARSEQ using the alias name Parseq.
     * @return the PARSEQ
     */
    public Integer getParseq() {
        return (Integer) getAttributeInternal(PARSEQ);
    }

    /**
     * Sets <code>value</code> as attribute value for PARSEQ using the alias name Parseq.
     * @param value value to set the PARSEQ
     */
    public void setParseq(Integer value) {
        setAttributeInternal(PARSEQ, value);
    }

    /**
     * Gets the attribute value for UPD_DATE using the alias name UpdDate.
     * @return the UPD_DATE
     */
    public Date getUpdDate() {
        return (Date) getAttributeInternal(UPDDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for UPD_DATE using the alias name UpdDate.
     * @param value value to set the UPD_DATE
     */
    public void setUpdDate(Date value) {
        setAttributeInternal(UPDDATE, value);
    }

    /**
     * Gets the attribute value for UPD_ID using the alias name UpdId.
     * @return the UPD_ID
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Sets <code>value</code> as attribute value for UPD_ID using the alias name UpdId.
     * @param value value to set the UPD_ID
     */
    public void setUpdId(String value) {
        setAttributeInternal(UPDID, value.toUpperCase());
    }

    /**
     * Gets the attribute value for FILE_SIZE using the alias name FileSize.
     * @return the FILE_SIZE
     */
    public String getFileSize() {
        return (String) getAttributeInternal(FILESIZE);
    }

    /**
     * Sets <code>value</code> as attribute value for FILE_SIZE using the alias name FileSize.
     * @param value value to set the FILE_SIZE
     */
    public void setFileSize(String value) {
        setAttributeInternal(FILESIZE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute isEditable.
     * The attribute is calculated using Par table attribute "isEditable"
     * @return the isEditable
     */
    public String getisEditable() {
        Row r=this.getParView1();
        return r.getAttribute("isEditable").toString();
        //return (String) getAttributeInternal(SUBMIT);
    }


    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute isEditable.
     * @param value value to set the  isEditable
     */
    public void setisEditable(String value) {
        setAttributeInternal(ISEDITABLE, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link ParDocsView.
     */
    public RowIterator getParDocsView() {
        return (RowIterator)getAttributeInternal(PARDOCSVIEW);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link ParView.
     */
    public RowIterator getParView() {
        return (RowIterator)getAttributeInternal(PARVIEW);
    }

    /**
     * Gets the associated <code>Row</code> using master-detail link ParView1.
     */
    public Row getParView1() {
        return (Row)getAttributeInternal(PARVIEW1);
    }

    /**
     * Sets the master-detail link ParView1 between this object and <code>value</code>.
     */
    public void setParView1(Row value) {
        setAttributeInternal(PARVIEW1, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> DocTypeLookupLOV1.
     */
    public RowSet getDocTypeLookupLOV1() {
        return (RowSet)getAttributeInternal(DOCTYPELOOKUPLOV1);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}
