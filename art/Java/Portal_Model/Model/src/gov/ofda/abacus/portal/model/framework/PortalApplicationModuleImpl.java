package gov.ofda.abacus.portal.model.framework;

import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;

import oracle.jbo.server.ApplicationModuleImpl;

public class PortalApplicationModuleImpl extends ApplicationModuleImpl {
    public String getUserName() {
        ADFContext adfCtx = ADFContext.getCurrent();
        SecurityContext secCntx = adfCtx.getSecurityContext();
        return (secCntx.getUserName().toUpperCase());
    }
}
