package gov.ofda.abacus.portal.pojo;


import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.myfaces.trinidad.model.ChildPropertyTreeModel;

public class Award implements Serializable {
    @SuppressWarnings("compatibility:-3324334197175727635")
    private static final long serialVersionUID = 1L;
    private Long awid;

    private String bureau_code;
    private String office_code;
    private String ofda_division_code;
    private String ofda_team_code;
    private String aor;
    private String aor_name;

    private String pname;
    private String pnbr;

    private String region_name;
    private String region_code;

    private String cname;
    private String ccode;

    private String awardee;
    private String awardee_acronym;
    private String awardee_code;
    private String awardee_type_code;

    private String award_nbr;
    private Double award_amt;
    private String award_status;

    private Date award_start_date;
    private Date award_end_date;
    private Integer start_fy;
    private Integer end_fy;
    private Integer award_duration;

    private String iso_code;
    private Double longitude;
    private Double latitude;

    private List<Report> reports;
    private List<Report> beneReports = null;
    private boolean reportsSorted = false;
    private Long CUM_TOTAL_TARGETED;
    private Long CUM_IDP_TARGETED;

    private Long TOTAL_REACHED;
    private Long IDP_REACHED;
    private Integer TOTAL_REACHED_PCT;
    private Integer IDP_REACHED_PCT;

    private Long CUM_TOTAL_REACHED;
    private Long CUM_IDP_REACHED;
    private Integer CUM_TOTAL_REACHED_PCT;
    private Integer CUM_IDP_REACHED_PCT;
    private Integer RPT_FY;
    private String RPT_TYPE_NAME;
    private String PRD_TYPE_NAME;
    private transient ChildPropertyTreeModel reportBeneTree = null;

    public Award() {
        super();
    }

    public void setAwid(Long awid) {
        this.awid = awid;
    }

    public Long getAwid() {
        return awid;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPname() {
        return pname;
    }

    public void setPnbr(String pnbr) {
        this.pnbr = pnbr;
    }

    public String getPnbr() {
        return pnbr;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_code(String region_code) {
        this.region_code = region_code;
    }

    public String getRegion_code() {
        return region_code;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCname() {
        return cname;
    }

    public void setCcode(String ccode) {
        this.ccode = ccode;
    }

    public String getCcode() {
        return ccode;
    }

    public void setAwardee(String awardee) {
        this.awardee = awardee;
    }

    public String getAwardee() {
        return awardee;
    }

    public void setAwardee_code(String awardee_code) {
        this.awardee_code = awardee_code;
    }

    public String getAwardee_code() {
        return awardee_code;
    }

    public void setAwardee_type_code(String awardee_type_code) {
        this.awardee_type_code = awardee_type_code;
    }

    public String getAwardee_type_code() {
        return awardee_type_code;
    }

    public void setAward_nbr(String award_nbr) {
        this.award_nbr = award_nbr;
    }

    public String getAward_nbr() {
        return award_nbr;
    }

    public void setAward_amt(Double award_amt) {
        this.award_amt = award_amt;
    }

    public Double getAward_amt() {
        return award_amt;
    }

    public void setAward_status(String award_status) {
        this.award_status = award_status;
    }

    public String getAward_status() {
        return award_status;
    }

    public void setAward_start_date(Date award_start_date) {
        this.award_start_date = award_start_date;
    }

    public Date getAward_start_date() {
        return award_start_date;
    }

    public void setAward_end_date(Date award_end_date) {
        this.award_end_date = award_end_date;
    }

    public Date getAward_end_date() {
        return award_end_date;
    }

    public void setStart_fy(Integer start_fy) {
        this.start_fy = start_fy;
    }

    public Integer getStart_fy() {
        return start_fy;
    }

    public void setEnd_fy(Integer end_fy) {
        this.end_fy = end_fy;
    }

    public Integer getEnd_fy() {
        return end_fy;
    }

    public void setAward_duration(Integer award_duration) {
        this.award_duration = award_duration;
    }

    public Integer getAward_duration() {
        return award_duration;
    }

    public void setIso_code(String iso_code) {
        this.iso_code = iso_code;
    }

    public String getIso_code() {
        return iso_code;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setReports(List<Report> reports) {
        this.reports = reports;
    }

    public List<Report> getReports() {
        /*    if(!reportsSorted) {
            Collections.sort(this.reports,new ReportDueDateComparator());
        }*/
        return reports;
    }

    public void setOfda_division_code(String ofda_division_code) {
        this.ofda_division_code = ofda_division_code;
    }

    public String getOfda_division_code() {
        return ofda_division_code;
    }

    public void setOfda_team_code(String ofda_team_code) {
        this.ofda_team_code = ofda_team_code;
    }

    public String getOfda_team_code() {
        return ofda_team_code;
    }

    public void setAor(String aor) {
        this.aor = aor;
    }

    public String getAor() {
        return aor;
    }

    public void setAor_name(String aor_name) {
        this.aor_name = aor_name;
    }

    public String getAor_name() {
        return aor_name;
    }

    public void setReportsSorted(boolean reportsSorted) {
        this.reportsSorted = reportsSorted;
    }

    public boolean isReportsSorted() {
        return reportsSorted;
    }

    public void setBeneReports(List<Report> beneReports) {
        this.beneReports = beneReports;
    }

    public void setAwardee_acronym(String awardee_acronym) {
        this.awardee_acronym = awardee_acronym;
    }

    public String getAwardee_acronym() {
        return awardee_acronym;
    }

    public List<Report> getBeneReports() {
        if (beneReports == null) {
            beneReports = new ArrayList<Report>();
            if(reports!=null)
            {
                for (Report r : reports) {
                    if (r.getRPT_TYPE_CODE() == 4)
                        beneReports.add(r);
                }
                for (Report r : reports) {
                    if (r.getRPT_TYPE_CODE() == 1)
                        beneReports.add(r);
                }
            }
        }
        return beneReports;
    }
    public void setReportBeneTree(ChildPropertyTreeModel reportTree) {
        this.reportBeneTree = reportTree;
    }

    public ChildPropertyTreeModel getReportBeneTree() {
        if(reportBeneTree==null) {
            reportBeneTree=new ChildPropertyTreeModel(this.getBeneReports(),"SECTOR_DETAILS");
        }
        return reportBeneTree;
    }
    public Long getCUM_TOTAL_TARGETED() {
        if(CUM_TOTAL_TARGETED==null)
            getAwardLevelBeneficiaries();
        
        return CUM_TOTAL_TARGETED;
    }

    public Long getCUM_IDP_TARGETED() {
        if(CUM_IDP_TARGETED==null)
            getAwardLevelBeneficiaries();
        return CUM_IDP_TARGETED;
    }

    public Long getTOTAL_REACHED() {
        if(TOTAL_REACHED==null)
            getAwardLevelBeneficiaries();
        return TOTAL_REACHED;
    }

    public Long getIDP_REACHED() {
        if(IDP_REACHED==null)
            getAwardLevelBeneficiaries();
        return IDP_REACHED;
    }

    public Long getCUM_TOTAL_REACHED() {
        if(CUM_TOTAL_REACHED==null)
            getAwardLevelBeneficiaries();
        return CUM_TOTAL_REACHED;
    }

    public Long getCUM_IDP_REACHED() {
        if(CUM_IDP_REACHED==null)
            getAwardLevelBeneficiaries();
        return CUM_IDP_REACHED;
    }
    private void getAwardLevelBeneficiaries(){
        List<Report> t=this.getBeneReports();
        Collections.reverse(t);
        Boolean b=false;
        Long reached=new Long(0);
        Long idp=new Long(0);
        CUM_TOTAL_TARGETED=new Long(0);
        CUM_IDP_TARGETED=new Long(0);
        RPT_FY=0;
        for(Report r:t) {
            if(r.getSUBMIT_DATE()!=null && r.getRPT_TYPE_CODE()==1) {
                if(!b)
                {
                this.CUM_TOTAL_TARGETED=r.getCUM_TOTAL_TARGETED();
                this.CUM_IDP_TARGETED=r.getCUM_IDP_TARGETED();
                this.CUM_TOTAL_REACHED=r.getCUM_TOTAL_REACHED();
                this.CUM_IDP_REACHED=r.getCUM_IDP_REACHED();
                this.RPT_FY=r.getRPT_FY();
                this.RPT_TYPE_NAME=r.getRPT_TYPE_NAME();
                this.PRD_TYPE_NAME=r.getPRD_TYPE_NAME();
                b=true;
                }
                if(r.getTOTAL_REACHED()!=null)
                    reached=reached+r.getTOTAL_REACHED();
                if(r.getIDP_REACHED()!=null)
                    idp=idp+r.getIDP_REACHED();
            }
        }
       this.TOTAL_REACHED=reached;
       this.IDP_REACHED=idp;
       
        
        if(CUM_TOTAL_TARGETED!=null && CUM_TOTAL_TARGETED>0)
        {
            if(TOTAL_REACHED!=null)
            {
             Float r1=new Float(TOTAL_REACHED);
             r1=(r1/CUM_TOTAL_TARGETED)*100;
             TOTAL_REACHED_PCT=Math.round(r1);
            }
            if(CUM_TOTAL_REACHED!=null)
            {
             Float r3=new Float(CUM_TOTAL_REACHED);
             r3=(r3/CUM_TOTAL_TARGETED)*100;
             CUM_TOTAL_REACHED_PCT=Math.round(r3);
            }
        }
        else
        {
            TOTAL_REACHED_PCT=0;
            CUM_TOTAL_REACHED_PCT=0;
        }
        if(CUM_IDP_TARGETED!=null && CUM_IDP_TARGETED>0) {
            if(IDP_REACHED!=null)
            {
            Float r2=new Float(IDP_REACHED);
            r2=(r2/CUM_IDP_TARGETED)*100;
            IDP_REACHED_PCT=Math.round(r2);
            }
            if(CUM_IDP_REACHED!=null)
            {
             Float r3=new Float(CUM_IDP_REACHED);
             r3=(r3/CUM_IDP_TARGETED)*100;
             CUM_IDP_REACHED_PCT=Math.round(r3);
            }
        }
        else {
            IDP_REACHED_PCT=0;
            CUM_IDP_REACHED_PCT=0;
        }
        Collections.reverse(t);
        reached=new Long(0);
        idp=new Long(0);
        for(Report r:t) {
            if(r.getSUBMIT_DATE()!=null && r.getRPT_TYPE_CODE()==1) {
                r.setPREV_CUM_TOTAL_REACHED(reached);
                r.setPREV_CUM_IDP_REACHED(idp);
                if(r.getTOTAL_REACHED()!=null)
                    reached=reached+r.getTOTAL_REACHED();
                if(r.getIDP_REACHED()!=null)
                    idp=idp+r.getIDP_REACHED();
                r.setCAL_CUM_TOTAL_REACHED(reached);
                r.setCAL_CUM_IDP_REACHED(idp);
            }
        }
    }

    public Integer getTOTAL_REACHED_PCT() {
        return TOTAL_REACHED_PCT;
    }

    public Integer getIDP_REACHED_PCT() {
        return IDP_REACHED_PCT;
    }

    public Integer getCUM_TOTAL_REACHED_PCT() {
        return CUM_TOTAL_REACHED_PCT;
    }

    public Integer getCUM_IDP_REACHED_PCT() {
        return CUM_IDP_REACHED_PCT;
    }

    public Integer getRPT_FY() {
        if(RPT_FY==null)
            getAwardLevelBeneficiaries();
        return RPT_FY;
    }

    public String getRPT_TYPE_NAME() {
        return RPT_TYPE_NAME;
    }

    public String getPRD_TYPE_NAME() {
        return PRD_TYPE_NAME;
    }

    public void setBureau_code(String bureau_code) {
        this.bureau_code = bureau_code;
    }

    public String getBureau_code() {
        return bureau_code;
    }

    public void setOffice_code(String office_code) {
        this.office_code = office_code;
    }

    public String getOffice_code() {
        return office_code;
    }
}
