package gov.ofda.abacus.portal.controller.elasticsearch;

import gov.ofda.abacus.portal.pojo.Award;

import gov.ofda.abacus.portal.pojo.Comparator.AggregationResultItemComparator;
import gov.ofda.abacus.portal.pojo.Comparator.ReportFrequencyComparator;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;

public class ResultItem implements Serializable {
    @SuppressWarnings("compatibility:-4773637714756127468")
    private static final long serialVersionUID = 1L;
    private Map<String,AggregationResultItem> aggregateList=new LinkedHashMap<String,AggregationResultItem>();
    private List<Award> awardList=new ArrayList<Award>();
    private Long total;
    private Long compliantReports;
    private Long requiredReports;
    private Integer compliantReportPercent;
    private Integer compliantAwards;
    private Integer compliantAwardPercent;
    private List<AggregationResultItem> aggList=null;
    public ResultItem() {
        super();
    }

    public void setTotal(Long total) {
        
        //this.total = Math.round(Math.ceil(total/10));
        this.total=total;
        
    }

    public Long getTotal() {
        return this.total;
    }

    public void setAggregateList(Map<String, AggregationResultItem> aggregateList) {
        this.aggregateList = aggregateList;
    }

    public Map<String, AggregationResultItem> getAggregateList() {
        return aggregateList;
    }

    public List<AggregationResultItem> getAggList() {
            if(aggList==null) {
                aggList=new ArrayList<AggregationResultItem>(this.getAggregateList().values());
                Collections.sort(aggList,new AggregationResultItemComparator());
            }
            return aggList;
    }

    public void setAwardList(List<Award> awardList) {
        this.awardList = awardList;
    }

    public List<Award> getAwardList() {
        return awardList;
    }

    public void setCompliantReports(Long compliantReports) {
        this.compliantReports = compliantReports;
    }

    public Long getCompliantReports() {
        return compliantReports;
    }

    public void setRequiredReports(Long requiredReports) {
        this.requiredReports = requiredReports;
    }

    public Long getRequiredReports() {
        return requiredReports;
    }

    public void setCompliantReportPercent(Integer compliantReportPercent) {
        this.compliantReportPercent = compliantReportPercent;
    }

    public Integer getCompliantReportPercent() {
        return compliantReportPercent;
    }

    public void setCompliantAwards(Integer compliantAwards) {
        this.compliantAwards = compliantAwards;
    }

    public Integer getCompliantAwards() {
        return compliantAwards;
    }

    public void setCompliantAwardPercent(Integer compliantAwardPercent) {
        this.compliantAwardPercent = compliantAwardPercent;
    }

    public Integer getCompliantAwardPercent() {
        return compliantAwardPercent;
    }
}
