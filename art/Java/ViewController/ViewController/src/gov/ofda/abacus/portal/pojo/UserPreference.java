package gov.ofda.abacus.portal.pojo;

import gov.ofda.abacus.portal.pojo.Comparator.UserDashboardPreferenceComparator;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class UserPreference implements Serializable{
    @SuppressWarnings("compatibility:-3722194852215450379")
    private static final long serialVersionUID = 1L;

    public UserPreference() {
        super();
    }
    private String username;
    private String userType;
    private Boolean showTour=true;
    private List<UserDashboardPreference> dashboardPreferenceList=new ArrayList<UserDashboardPreference>();
    private Date createdDate=new Date();
    private Date updatedDate=new Date();
    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserType() {
        return userType;
    }

    public void setDashboardPreferenceList(List<UserDashboardPreference> dashboardPreferenceList) {
        this.dashboardPreferenceList = dashboardPreferenceList;
    }

    public List<UserDashboardPreference> getDashboardPreferenceList() {
        Collections.sort(dashboardPreferenceList,new UserDashboardPreferenceComparator());
        return dashboardPreferenceList;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
                    return true;
                }
     if(o instanceof UserPreference) {
         UserPreference object=(UserPreference)o;
         if(object.getUsername()!=null && object.getUsername().equals(this.getUsername())&& object.getUsername()!=null && object.getUsername().equals(this.getUsername()))
            return true;
     }
     return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + username.hashCode();
        return result;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setShowTour(Boolean showTour) {
        this.showTour = showTour;
    }

    public Boolean getShowTour() {
        return showTour;
    }
}
