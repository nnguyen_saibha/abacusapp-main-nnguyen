package gov.ofda.abacus.portal.pojo.Comparator;

import gov.ofda.abacus.portal.controller.elasticsearch.BucketItem;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class ReportTypeComparator implements Comparator<BucketItem> {
    public ReportTypeComparator() {
        super();
    }
    private static final Map<String,Integer> l;
    static{
    Map<String,Integer> tmp=new HashMap<String,Integer>();
        tmp.put("Baseline",1);
        tmp.put("Program",2);
        tmp.put("Financial",3);
        tmp.put("Evaluation",4);
        tmp.put("M and E Plan",5);
        tmp.put("Milestones",6);
        tmp.put("Needs Assessment",7);
        tmp.put("Property",8);
        tmp.put("Work Plan",9);
        tmp.put("Other",10);
         l=Collections.unmodifiableMap(tmp);
    }
    @Override
    public int compare(BucketItem bucketItem, BucketItem bucketItem2) {
        Integer item1=l.get(bucketItem.getKey());
        Integer item2=l.get(bucketItem2.getKey());
        if(item1!=null && item2!=null)
            return item1.compareTo(item2);
        return (int) (bucketItem.getKey().compareTo(bucketItem2.getKey()));
    }
}
