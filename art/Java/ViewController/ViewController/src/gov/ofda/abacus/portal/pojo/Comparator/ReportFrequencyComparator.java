package gov.ofda.abacus.portal.pojo.Comparator;

import gov.ofda.abacus.portal.controller.elasticsearch.BucketItem;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class ReportFrequencyComparator implements Comparator<BucketItem>{
    public ReportFrequencyComparator() {
        super();
    }
    
    private static final Map<String,Integer> l;
    static{
    Map<String,Integer> tmp=new HashMap<String,Integer>();
             tmp.put("Q1",1);
             tmp.put("Q2",2);
             tmp.put("Q3",3);
             tmp.put("Q4",4);
             tmp.put("Q1 (Oct - Dec)",1);
             tmp.put("Q2 (Jan - Mar)",2);
             tmp.put("Q3 (Apr - Jun)",3);
             tmp.put("Q4 (Jul - Sep)",4);
             tmp.put("Semi-Annual 1",5);
             tmp.put("SA1 (Oct - Mar)",5);
             tmp.put("SA1",5);
             tmp.put("Semi-Annual 2",6);
             tmp.put("SA2 (Apr - Sep)",6);
             tmp.put("SA2",6);
             tmp.put("Annual (Oct - Sep)",7);
             tmp.put("Annual",7);
             tmp.put("Final",8);
             tmp.put("Missing",11);
             tmp.put("Not Applicable",10);
             tmp.put("Custom",9);
             l=Collections.unmodifiableMap(tmp);
    }
        @Override
        public int compare(BucketItem bucketItem, BucketItem bucketItem2) {
            Integer item1=l.get(bucketItem.getKey());
            Integer item2=l.get(bucketItem2.getKey());
            if(item1!=null && item2!=null)
                return item1.compareTo(item2);
            return (int) (bucketItem.getKey().compareTo(bucketItem2.getKey()));
        }
}
