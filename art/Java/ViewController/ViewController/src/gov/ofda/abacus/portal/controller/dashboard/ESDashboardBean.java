package gov.ofda.abacus.portal.controller.dashboard;

import gov.ofda.abacus.portal.controller.UIControl;

import gov.ofda.abacus.portal.controller.elasticsearch.AggregationResultItem;
import gov.ofda.abacus.portal.controller.elasticsearch.BucketItem;
import gov.ofda.abacus.portal.controller.elasticsearch.ElasticSearchRestBean;
import gov.ofda.abacus.portal.controller.elasticsearch.SearchItem;
import gov.ofda.abacus.portal.controller.elasticsearch.TermItem;
import gov.ofda.abacus.portal.pojo.Award;

import gov.ofda.abacus.portal.pojo.FilterItem;

import gov.ofda.abacus.portal.pojo.SortItem;

import gov.ofda.abacus.portal.pojo.UserDashboardPreference;

import gov.ofda.abacus.portal.pojo.UserPreference;

import java.io.Serializable;

import java.util.ArrayList;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.share.ADFContext;

import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.faces.bi.component.chart.UIChartBase;
import oracle.adf.view.faces.bi.component.sunburst.UISunburst;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;
import oracle.adf.view.rich.model.NumberRange;

import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;


import oracle.jbo.Row;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.myfaces.trinidad.event.DisclosureEvent;
import org.apache.myfaces.trinidad.event.ReturnEvent;
import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.ComponentReference;
import org.apache.myfaces.trinidad.util.Service;

public class ESDashboardBean implements Serializable {
    @SuppressWarnings("compatibility:-2534915421150331730")
    private static final long serialVersionUID = 1L;
    private ComponentReference toolbar;
    private Integer startFY;
    private NumberRange fYRange;
    private Integer currentFY;
    private Boolean showFilter=true;
    private Boolean showCharts=true;
    private SearchItem sitem=new SearchItem();
    List<Award> awardList=new ArrayList<Award>();
    List<Integer> fiscalYearList=new ArrayList<Integer>();
    private ComponentReference searchHeader;
    private Boolean dataTabSelection[]= {true,false,false};
    private Boolean chartTabSelection[][]= {{true,true,false,false,false,false,false,false},{false,true,false,false}};
    private Boolean filterTabSelection[]= {true,false};
    public Integer getCurrentFY() {
        return currentFY;
    }

    public void setFYRange(NumberRange fYRange) {
        
        startFY=fYRange.getMinimum().intValue();
        endFY=fYRange.getMaximum().intValue();
        if(this.fYRange!=null)
        {
            sitem.setStartFY(startFY);
            sitem.setEndFY(endFY);
            sitem.performSearch();
        }
        this.fYRange = fYRange;
    }

    public NumberRange getFYRange() {
        return fYRange;
    }

    public void setShowFilter(Boolean showFilter) {
        this.showFilter = showFilter;
    }

    public Boolean getShowFilter() {
        return showFilter;
    }

    public void setShowCharts(Boolean showCharts) {
        this.showCharts = showCharts;
    }

    public Boolean getShowCharts() {
        return showCharts;
    }

    public void setStartFY(Integer startFY) {
        this.startFY = startFY;
        sitem.setStartFY(startFY);
        sitem.performSearch();
    }

    public Integer getStartFY() {
        return startFY;
    }

    public void setEndFY(Integer endFY) {
        this.endFY = endFY;
        sitem.setEndFY(endFY);
        sitem.performSearch();
    }

    public Integer getEndFY() {
        return endFY;
    }
    private Integer endFY;
    public ESDashboardBean() {
    }
    private String[] checkedValues = { "In-Progress","Expired", "Closed" };

    public void setCheckedValues(String[] checkedValues) {
        this.checkedValues = checkedValues;
    }

    public String[] getCheckedValues() {
        return checkedValues;
    }

    public void setAwardStatusForDashBoard() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl=null;
        SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
          ctrl= bindings.getOperationBinding("getCurrentFy");
          Integer year = (Integer) ctrl.execute();
          this.currentFY=year;
          startFY=year-1;
          endFY=year;
          NumberRange nr=new NumberRange(startFY,endFY);
          this.setFYRange(nr);
        sitem=new SearchItem();
        //sitem.setValuesWithoutSearch("", 0,"relevance");
        sitem.setStartFY(startFY);
        sitem.setEndFY(endFY);
        this.getFiscalYearList().clear();
        for(int i=currentFY-6;i<currentFY+2;i++)
            this.getFiscalYearList().add(i);
        String userType="partner";
        showFilter=true;
        showCharts=true;
        if(sc.isUserInRole("admin")||sc.isUserInRole("ofda")||sc.isUserInRole("APP_CONNECT")) {
            sitem.setRestrictAwardee(false);
            if(sc.isUserInRole("admin"))
                userType="admin";
            else
            userType="ofda";
            dataTabSelection= new Boolean[]{true,false,false};
            chartTabSelection= new Boolean[][] {{true,true,false,false,false,false,false,false},{false,true,false,false}};
            filterTabSelection= new Boolean[] {true,false};
            //getCurrentAOR
           /* ctrl= bindings.getOperationBinding("getCurrentAOR");
            String aorName = (String) ctrl.execute();
            if(aorName!=null) {

                 TermItem tt=new TermItem("AOR","aor_name",aorName,aorName);
                 sitem.addTermFilter(tt);
            }*/
        }
        else if(sc.isUserInRole("partner"))
        {
            dataTabSelection= new Boolean[]{true,false,false};
            chartTabSelection= new Boolean[][] {{false,false,true,false,false,false,false},{true,false,true,false}};
            filterTabSelection= new Boolean[] {false,true};
            ctrl= bindings.getOperationBinding("getCurrentUserAwardee");
            String awardee = (String) ctrl.execute();
            sitem.setAwardeeCode(awardee);
        }
        else
            sitem.setAwardeeCode("0");
      /*  ctrl= bindings.getOperationBinding("setAwardStatusForDashBoard");
        ctrl.getParamsMap().put("statusList", this.getCheckedValues());
        ctrl.getParamsMap().put("startFY", this.getStartFY());
        ctrl.getParamsMap().put("endFY", this.getEndFY());
        ctrl.execute();*/
      UserPreference udpl=sitem.getUserPreference();
      Map rMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
      String reset = (String) rMap.get("reset");
      if(reset!=null ||(udpl==null || udpl.getShowTour())) {
          sitem.performSearch();
          if("true".equals(rMap.get("isTour"))||(udpl==null || udpl.getShowTour()))
          {
              dataTabSelection= new Boolean[]{true,false,false};
              chartTabSelection= new Boolean[][] {{true,true,false,false,false,false,false,false},{false,true,false,false}};
              filterTabSelection= new Boolean[] {true,false};
              if(udpl==null) {
                  UserPreference userPref=sitem.getUserPreference();
                      userPref=new UserPreference();
                      userPref.setUsername(sc.getUserName().toUpperCase());
                      if(sc.isUserInRole("admin"))
                          userPref.setUserType("admin");
                      else if(sc.isUserInRole("ofda")||sc.isUserInRole("APP_CONNECT"))
                              userPref.setUserType("ofda");
                           else
                               userPref.setUserType("partner");
                  userPref.setShowTour(false);
                  this.sitem.setUserPreference(userPref);
              }
                  else
              {
                    udpl.setShowTour(false);
                  sitem.setUserPreference(udpl);
              }
              ExtendedRenderKitService erks =
                  Service.getRenderKitService(FacesContext.getCurrentInstance(), ExtendedRenderKitService.class);
              StringBuilder strb = new StringBuilder(
              "          setTimeout(function(){ " +
              "startTour();}, 500);");
              erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
          }
          return;
      }
        
        if(udpl!=null && userType.equals(udpl.getUserType())) {
            UserDashboardPreference defaultPref=null;
            if(udpl.getDashboardPreferenceList()!=null)
            {
                for(UserDashboardPreference i:udpl.getDashboardPreferenceList()) {
                    if(i.getIsDefault())
                    {
                        defaultPref=i;
                        break;
                    }
                }
            }
            if(defaultPref!=null)
            {
             Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
             pfm.put("defaultUserPreferenceName", defaultPref.getPreferenceName());
             if(defaultPref.getDataTabSelection()!=null)
                this.dataTabSelection=SerializationUtils.clone(defaultPref.getDataTabSelection());
             if(defaultPref.getChartTabSelection()!=null)
                this.chartTabSelection=SerializationUtils.clone(defaultPref.getChartTabSelection());
             if(defaultPref.getFilterTabSelection()!=null)
                this.filterTabSelection=SerializationUtils.clone(defaultPref.getFilterTabSelection());
             this.showCharts=defaultPref.getShowCharts();
             this.showFilter=defaultPref.getShowFilter();
             this.sitem.setUserPeferenceDefault(defaultPref);
             this.startFY=defaultPref.getStartFY();
             this.endFY=defaultPref.getEndFY();
             nr=new NumberRange(startFY,endFY);
             this.fYRange = nr;
            }
            else
                sitem.performSearch();
            
        }
        else
            sitem.performSearch();
        }

    public void checboxChangeListener(ValueChangeEvent valueChangeEvent) {


        checkedValues = (String[]) valueChangeEvent.getNewValue();
        setAwardStatusForDashBoard();
        AdfFacesContext.getCurrentInstance().addPartialTarget(valueChangeEvent.getComponent().getParent().getParent());

    }

    public void fiscalYearSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("RPT_FY").getList(),"RPT_FY");
    }
    public void reportDueDateSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("RPT_DUE_DATE").getList(),"RPT_DUE_DATE");
    }
    public void frequencySelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("PRD_TYPE_NAME").getList(),"PRD_TYPE_NAME");
    }

    public void projectSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("pname").getList(),"pname");
    }

    public void awardStatusSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("award_status").getList(),"award_status");
    }

    public void awardRegionSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("region_name").getList(),"region_name");
    }

    public void awardCountrySelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("cname").getList(),"cname");
    }

    public void awardReportStatusSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("STATUS").getList(),"STATUS");
    }

    public void awardeeTableSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("awardee").getList(),"awardee");
    }

    public void awardReportTypeSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("RPT_TYPE_NAME").getList(),"RPT_TYPE_NAME");
    }
    public void sectorSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("SECTOR").getList(),"SECTOR");
    }
    public void awardBureauSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("bureau_code").getList(),"bureau_code");
    }
    public void awardOfficeSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("office_code").getList(),"office_code");
    }
    public void awardDivisionSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("ofda_division_code").getList(),"ofda_division_code");
    }
    public void awardTeamSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("ofda_team_code").getList(),"ofda_team_code");
    }
    public void aorSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("aor_name").getList(),"aor_name");
    }
    
    public void fiscalYearReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("RPT_FY").getListSortedByReached(),"RPT_FY");
    }
    public void reportDueDateReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("RPT_DUE_DATE").getListSortedByReached(),"RPT_DUE_DATE");
    }
    public void frequencyReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("PRD_TYPE_NAME").getListSortedByReached(),"PRD_TYPE_NAME");
    }

    public void projectReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("pname").getListSortedByReached(),"pname");
    }

    public void awardStatusReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("award_status").getListSortedByReached(),"award_status");
    }

    public void awardRegionReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("region_name").getListSortedByReached(),"region_name");
    }

    public void awardCountryReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("cname").getListSortedByReached(),"cname");
    }

    public void awardReportStatusReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("STATUS").getListSortedByReached(),"STATUS");
    }

    public void awardeeTableReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("awardee").getListSortedByReached(),"awardee");
    }

    public void awardReportTypeReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("RPT_TYPE_NAME").getListSortedByReached(),"RPT_TYPE_NAME");
    }
    public void awardDivisionReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("ofda_division_code").getListSortedByReached(),"ofda_division_code");
    }
    public void awardTeamReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("ofda_team_code").getListSortedByReached(),"ofda_team_code");
    }
    public void aorReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("aor_name").getListSortedByReached(),"aor_name");
    }
    public void sectorReachedSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("SECTOR").getListSortedByReached(),"SECTOR");
    }
    public void awardeeTypeSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("awardee_type_code").getList(),"awardee_type_code");
    }
    public void awardDurationSelectionLsnr(SelectionEvent selectionEvent) {
        setFilter(selectionEvent, this.getSitem().getRitem().getAggregateList().get("award_duration").getList(),"award_duration");
    }


    private void setFilter(SelectionEvent selectionEvent, List<BucketItem> list,String termName) {
        AggregationResultItem asi=this.getSitem().getRitem().getAggregateList().get(termName);
        if(list!=null) {
            RowKeySet rks=selectionEvent.getAddedSet();
            for(Object o:rks) {
                BucketItem bi=null;
                if(o instanceof Integer)
                   bi=list.get((Integer)o);
                else if(o instanceof ArrayList){
                    List<Integer> l = (List<Integer>) o;
                    bi=list.get(l.get(0));
                }
                if(bi!=null)
                {
                  TermItem t=new TermItem(asi.getName(),asi.getColumnName(),bi.getKey_as_string(),bi.getKey());
                  boolean r=sitem.addTermFilter(t);
                }
                    
            }
        }
        String className=selectionEvent.getComponent().getClass().getSimpleName();
        if("UIHorizontalBarChart".equals(className)||"UIBarChart".equals(className))
            ((UIChartBase)selectionEvent.getComponent()).getSelectedRowKeys().clear();
        else if("UISunburst".equals(className))
                ((UISunburst)selectionEvent.getComponent()).getSelectedRowKeys().clear();
        AdfFacesContext.getCurrentInstance().addPartialTarget(selectionEvent.getComponent());
    }


    public void setToolbar(UIComponent toolbar) {
        this.toolbar = ComponentReference.newUIComponentReference(toolbar);
    }

    public UIComponent getToolbar() {
        return toolbar == null ? null : toolbar.getComponent();
    }

    public void newPar(ActionEvent actionEvent) {

    }

    public String newReport() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("createParAndPopulate");
        exec.execute();
        Integer parseq = (Integer) exec.getResult();
        if (parseq > 0) {
            Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            pfm.put("parseq", parseq);
            exec = bindings.getOperationBinding("getESAwardDetails");
            Award a = (Award) exec.execute();
            ElasticSearchRestBean esrb=new ElasticSearchRestBean();
            esrb.updateArticle(a);
            return "next";

        }
        return "back";
    }
    public String setFilter(ActionEvent actionEvent) {
        Map rMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String term = (String) rMap.get("term");
        String value = (String) rMap.get("value");
        String termLabel = (String) rMap.get("termLabel");
        String valueLabel = (String) rMap.get("valueLabel");
        if(term!=null && value!=null)
        {
         TermItem tt=new TermItem(termLabel,term,value,valueLabel);
         boolean r=sitem.addTermFilter(tt);
        }
        return null;
    }
    public String removeFilter(ActionEvent actionEvent) {
        Map rMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String term = (String) rMap.get("term");
        String value = (String) rMap.get("value");
        if(term!=null && value!=null)
        {
         TermItem tt=new TermItem(term,term,value,value);
         boolean r=sitem.removeTermFilter(tt);
        }
        return null;
    }
    public void removeAllFilters(ActionEvent actionEvent)
    {
        sitem.removeAllTermFilters();
    }
    public void filterValueChangeLsnr(ValueChangeEvent vce) {
        if(vce.getNewValue()!=null)
        {
        UIComponent c=vce.getComponent();
        Map pfp=c.getAttributes();
        String term = (String) pfp.get("filterTerm");
        String value = (String) pfp.get("filterValue");
        String termLabel = (String) pfp.get("filterTermLabel");
        String valueLabel = (String) pfp.get("filterValueLabel");
        pfp.put("filterTerm",null);
        pfp.put("filterValue",null);
        pfp.put("filterTermLabel",null);
        pfp.put("filterValueLabel",null);
        if(term!=null && value!=null)
        {
         TermItem tt=new TermItem(termLabel,term,valueLabel,value);
         if((Boolean)vce.getNewValue())
            sitem.addTermFilter(tt);
         else
            sitem.removeTermFilter(tt);
        }
        }
    }
    public void performSearch() {
        sitem.refreshSearch();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(pfm.get("scrollToID")!=null)
        {
          StringBuilder builder = new StringBuilder();
          builder.append("var c = AdfPage.PAGE.findComponent('");
          builder.append(pfm.get("scrollToID"));
          builder.append("'); c.scrollIntoView(true,'{behavior: \"smooth\", block: \"nearest\", inline: \"start\"}');");
          FacesContext fctx = FacesContext.getCurrentInstance();
          ExtendedRenderKitService erks =
          Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
          erks.addScript(fctx, builder.toString());
        }

    }

    public void setSitem(SearchItem sitem) {
        this.sitem = sitem;
    }

    public SearchItem getSitem() {
        return sitem;
    }

    public void setAwardList(List<Award> awardList) {
        this.awardList = awardList;
    }

    public List<Award> getAwardList() {
        return awardList;
    }

    public void refreshES(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding  exec = bindings.getOperationBinding("getAwardListForES");
        List<Award> list = (List<Award>) exec.execute();
        ElasticSearchRestBean esrb=new ElasticSearchRestBean();
        esrb.refreshES(list);
    }

    public void sortOrderFetchLsnr(PopupFetchEvent popupFetchEvent) {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        List<SortItem> selectedSortList=new ArrayList<SortItem>();
        selectedSortList.addAll(this.getSitem().getSelectedSortList());
        pfm.put("selectedSortList", selectedSortList);
    }

    public void sortOrderDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            List<SortItem> selectedSortList = (List<SortItem>) pfm.get("selectedSortList");
            if(selectedSortList!=null) {
              this.getSitem().getSelectedSortList().clear();
              this.getSitem().getSelectedSortList().addAll(selectedSortList);
             sitem.refreshSearch();
            }
        }
    }

    public String syncAward() {
        Map rMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        if(rMap.get("awardId")!=null)
        {
         DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
         OperationBinding  exec = bindings.getOperationBinding("getESAwardDetails");
         exec.getParamsMap().put("awid", rMap.get("awardId"));
         Award a = (Award) exec.execute();
         ElasticSearchRestBean esrb=new ElasticSearchRestBean();
         Boolean t=esrb.updateArticle(a);
            FacesMessage message =null;
         if(t)
             message =
                 new FacesMessage(FacesMessage.SEVERITY_INFO, "Award "+rMap.get("awardId")+" Synced", "This Award is synced. Click on search to see updated information.");
         else
             message =
                 new FacesMessage(FacesMessage.SEVERITY_ERROR, "Award "+rMap.get("awardId")+" not synced", "Award "+rMap.get("awardId")+" not synced in Elasticsearch");
        FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return null;
    
    }

    public void setFiscalYearList(List<Integer> fiscalYearList) {
        this.fiscalYearList = fiscalYearList;
    }

    public List<Integer> getFiscalYearList() {
        return fiscalYearList;
    }

    public void saveMyPreferenceDialogLsnr(DialogEvent event) {
        if(event.getOutcome()==DialogEvent.Outcome.ok)
        {
            Map rMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
            String name = (String) rMap.get("preferenceName");
            Boolean isDefault = (Boolean) rMap.get("defaultPreference");
            UserDashboardPreference udp=new UserDashboardPreference();
            udp.setPreferenceName(name);
            udp.setIsDefault(isDefault);
            udp.setShowCharts(this.showCharts);
            udp.setShowFilter(this.showFilter);
            udp.setDataTabSelection(SerializationUtils.clone(this.dataTabSelection));
            udp.setChartTabSelection(SerializationUtils.clone(this.chartTabSelection));
            udp.setFilterTabSelection(SerializationUtils.clone(this.filterTabSelection));
            if(this.sitem.getTermFilters()!=null) {
                //Term filters cannot be changed so no need for clone
                udp.getTermFilters().addAll(this.sitem.getTermFilters());
            }
            udp.setSearchQuery(this.sitem.getSearchQuery());
            if(this.sitem.getSelectedSortList()!=null) {
                for(SortItem s:this.sitem.getSelectedSortList()) {
                    SortItem j=SerializationUtils.clone(s);
                    udp.getSelectedSortList().add(j);
                }
            }
            udp.setStartFY(this.sitem.getStartFY());
            udp.setEndFY(this.sitem.getEndFY());
            UserPreference userPref=sitem.getUserPreference();
            if(userPref==null) {
                userPref=new UserPreference();
                SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
                userPref.setUsername(sc.getUserName().toUpperCase());
                if(sc.isUserInRole("admin"))
                    userPref.setUserType("admin");
                else if(sc.isUserInRole("ofda")||sc.isUserInRole("APP_CONNECT"))
                        userPref.setUserType("ofda");
                     else
                         userPref.setUserType("partner");
                userPref.setDashboardPreferenceList(new ArrayList<UserDashboardPreference>());
            }
            if(isDefault && userPref.getDashboardPreferenceList()!=null) {
                for(UserDashboardPreference i: userPref.getDashboardPreferenceList())
                    i.setIsDefault(false);
            }
            List<UserDashboardPreference> udpl=userPref.getDashboardPreferenceList();
            if(udpl!=null)
                udpl.add(udp);
            else {
                udpl=new ArrayList<UserDashboardPreference>();
                udpl.add(udp);
                userPref.setDashboardPreferenceList(udpl);
                
            }
            Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            pfm.put("defaultUserPreferenceName", udp.getPreferenceName());
            sitem.setUserPreference(userPref);
            AdfFacesContext.getCurrentInstance().addPartialTarget(event.getComponent().getParent().getParent());
        }
    }

    public void setDashboardUserPreference(ActionEvent actionEvent) {
        Map rMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        UserDashboardPreference defaultPref = (UserDashboardPreference) rMap.get("userPreference");
            if(defaultPref!=null)
            {
                Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
                pfm.put("defaultUserPreferenceName", defaultPref.getPreferenceName());
                if(defaultPref.getDataTabSelection()!=null)
                   this.dataTabSelection=SerializationUtils.clone(defaultPref.getDataTabSelection());
                if(defaultPref.getChartTabSelection()!=null)
                   this.chartTabSelection=SerializationUtils.clone(defaultPref.getChartTabSelection());
                if(defaultPref.getFilterTabSelection()!=null)
                   this.filterTabSelection=SerializationUtils.clone(defaultPref.getFilterTabSelection());
                this.showCharts=defaultPref.getShowCharts();
                this.showFilter=defaultPref.getShowFilter();
                this.sitem.setUserPeferenceDefault(defaultPref);
                this.startFY=defaultPref.getStartFY();
                this.endFY=defaultPref.getEndFY();
                NumberRange nr=new NumberRange(startFY,endFY);
                this.fYRange = nr;
                AdfFacesContext.getCurrentInstance().addPartialTarget(actionEvent.getComponent().getParent().getParent().getParent());
            }
        }

    public void manageUserPreferencesDialogLnsr(DialogEvent event) {
        if(event.getOutcome()==DialogEvent.Outcome.ok)
        {
            List<UserDashboardPreference> l=this.sitem.getUserPreference().getDashboardPreferenceList();
            List<UserDashboardPreference> deleteList=new ArrayList<UserDashboardPreference>();
            for(UserDashboardPreference i:l) {
                if(i.isIsDelete()) {
                    deleteList.add(i);
                }
            }
            l.removeAll(deleteList);
            this.sitem.setUserPreference(this.sitem.getUserPreference());
            AdfFacesContext.getCurrentInstance().addPartialTarget(event.getComponent().getParent().getParent());
        }
    }

    public void setDataTabSelection(Boolean[] dataTabSelection) {
        this.dataTabSelection = dataTabSelection;
    }

    public Boolean[] getDataTabSelection() {
        return dataTabSelection;
    }

    public void setChartTabSelection(Boolean[][] chartTabSelection) {
        this.chartTabSelection = chartTabSelection;
    }

    public Boolean[][] getChartTabSelection() {
        return chartTabSelection;
    }

    public void setFilterTabSelection(Boolean[] filterTabSelection) {
        this.filterTabSelection = filterTabSelection;
    }

    public Boolean[] getFilterTabSelection() {
        return filterTabSelection;
    }

    public void editReportActionLsnr(ActionEvent actionEvent) {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfm.put("scrollToID", actionEvent.getComponent().getParent().getParent().getClientId());
        NavigationHandler  nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        if(nvHndlr!=null)
            nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "edit");
    }
    public void newReportActionLsnr(ActionEvent actionEvent) {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfm.put("scrollToID", actionEvent.getComponent().getParent().getParent().getClientId());
        NavigationHandler  nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        if(nvHndlr!=null)
            nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "new");
    }

    public void startBeneTour(ActionEvent actionEvent) {
        ExtendedRenderKitService erks =
            Service.getRenderKitService(FacesContext.getCurrentInstance(), ExtendedRenderKitService.class);
        StringBuilder strb = new StringBuilder(
        "          setTimeout(function(){ " +
        "          startBeneTour();}, 500);");
        erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
    }

    public void endTour(ActionEvent actionEvent) {
        Map rMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        rMap.put("reset","reset");
        NavigationHandler  nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        if(nvHndlr!=null)
            nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "reset");
    }
}
