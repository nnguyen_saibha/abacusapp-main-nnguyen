package gov.ofda.abacus.portal.controller;

import java.util.Date;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.TaskFlowContext;
import oracle.adf.controller.TaskFlowTrainModel;
import oracle.adf.controller.TaskFlowTrainStopModel;
import oracle.adf.controller.ViewPortContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.output.RichImage;
import oracle.adf.view.rich.component.rich.output.RichOutputFormatted;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class TrainTemplateBean extends UIControl {
    private static ADFLogger logger = ADFLogger.createADFLogger(TrainTemplateBean.class);
    private RichOutputText trainTemplateReportStatus;
    private RichImage editMsgImage;
    private RichOutputFormatted editMsgText;

    public TrainTemplateBean() {
    }

    public void setKey() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        //Set Award View
        DCIteratorBinding iter = (DCIteratorBinding)bindings.findIteratorBinding("AwardView1Iterator");
        Key key = iter.getCurrentRow().getKey();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("awardRowKey", key);
        //Set Par View
        iter = (DCIteratorBinding)bindings.findIteratorBinding("ParView1Iterator");
        key = iter.getCurrentRow().getKey();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("parRowKey", key);

        ControllerContext controllerContext = ControllerContext.getInstance();
         ViewPortContext currentViewPortCtx =   controllerContext.getCurrentViewPort();
         TaskFlowContext taskFlowCtx =  currentViewPortCtx.getTaskFlowContext();
         TaskFlowTrainModel taskFlowTrainModel = taskFlowCtx.getTaskFlowTrainModel();
         TaskFlowTrainStopModel currentStop =taskFlowTrainModel.getCurrentStop(); 
        if("sector".equals(currentStop.getLocalActivityId()))
        {
        //Set ParSector View
        iter = (DCIteratorBinding)bindings.findIteratorBinding("ParsectorView1Iterator");
        if (iter != null && iter.getCurrentRow() != null) {
            key = iter.getCurrentRow().getKey();
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put("parSectorRowKey", key);
        }
        }
        if("uploadDocument".equals(currentStop.getLocalActivityId()))
        {
        //Set ParDocs View
        iter = (DCIteratorBinding)bindings.findIteratorBinding("AbacusxDocsView1Iterator");
        if (iter != null && iter.getCurrentRow() != null) {
            key = iter.getCurrentRow().getKey();
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put("parDocsRowKey", key);
        }
        }
//        if("manageIndicators".equals(currentStop.getLocalActivityId()))
//        {
//        //Set ParSectorIndicator View
//        iter = (DCIteratorBinding)bindings.findIteratorBinding("ParsectorindicatorView3Iterator");
//        if (iter != null && iter.getCurrentRow() != null) {
//            key = iter.getCurrentRow().getKey();
//            AdfFacesContext.getCurrentInstance().getPageFlowScope().put("parSectorIndicatorRowKey", key);
//        }
//        }
        }

    /**
     * Called when user clicks on cancel.
     * @return
     */
    public String cancelChanges() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        this.setKey();
        OperationBinding exec1 = bindings.getOperationBinding("Rollback");
        exec1.execute();
        DCIteratorBinding iter = null;

        //Set Award Row
        Key key = (Key)AdfFacesContext.getCurrentInstance().getPageFlowScope().get("awardRowKey");
        iter = (DCIteratorBinding)bindings.findIteratorBinding("AwardView1Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        if (rsi.findByKey(key, 1).length > 0)
            iter.setCurrentRowWithKey(key.toStringFormat(true));

        //Set Par Row
        key = (Key)AdfFacesContext.getCurrentInstance().getPageFlowScope().get("parRowKey");
        OperationBinding exec = bindings.getOperationBinding("ExecutePar");
        exec.execute();
        iter = (DCIteratorBinding)bindings.findIteratorBinding("ParView1Iterator");
        rsi = iter.getRowSetIterator();
        if (rsi.findByKey(key, 1).length > 0)
            iter.setCurrentRowWithKey(key.toStringFormat(true));
        
        ControllerContext controllerContext = ControllerContext.getInstance();
         ViewPortContext currentViewPortCtx =   controllerContext.getCurrentViewPort();
         TaskFlowContext taskFlowCtx =  currentViewPortCtx.getTaskFlowContext();
         TaskFlowTrainModel taskFlowTrainModel = taskFlowCtx.getTaskFlowTrainModel();
         TaskFlowTrainStopModel currentStop =taskFlowTrainModel.getCurrentStop(); 

    if("sector".equals(currentStop.getLocalActivityId()))
    {
        //Set ParSector Row
        key = (Key)AdfFacesContext.getCurrentInstance().getPageFlowScope().get("parSectorRowKey");
        if (key != null) {
            exec = bindings.getOperationBinding("ExecuteParSector");
            exec.execute();
            iter = (DCIteratorBinding)bindings.findIteratorBinding("ParsectorView1Iterator");
            rsi = iter.getRowSetIterator();
            if (rsi.findByKey(key, 1).length > 0)
                iter.setCurrentRowWithKey(key.toStringFormat(true));
        }
    }
    if("uploadDocument".equals(currentStop.getLocalActivityId()))
    {
        //Set ParDocs Row
        key = (Key)AdfFacesContext.getCurrentInstance().getPageFlowScope().get("parDocsRowKey");
        if (key != null) {
            exec = bindings.getOperationBinding("ExecuteDocs");
            exec.execute();
            iter = (DCIteratorBinding)bindings.findIteratorBinding("AbacusxDocsView1Iterator");
            rsi = iter.getRowSetIterator();
            if (rsi.findByKey(key, 1).length > 0)
                iter.setCurrentRowWithKey(key.toStringFormat(true));
        }
    }
   if("manageIndicators".equals(currentStop.getLocalActivityId()))
   {
        //Set ParSectorIndicator Row
        key = (Key)AdfFacesContext.getCurrentInstance().getPageFlowScope().get("parSectorIndicatorRowKey");
        if (key != null) {
            exec = bindings.getOperationBinding("ExecuteParSectorIndicator");
            exec.getParamsMap().put("p_parsiseq", key.getAttribute(0));
            exec.execute();
//            iter = (DCIteratorBinding)bindings.findIteratorBinding("ParsectorindicatorView3Iterator");
//            rsi = iter.getRowSetIterator();
//            if (rsi.findByKey(key, 1).length > 0)
//                iter.setCurrentRowWithKey(key.toStringFormat(true));
        }
   }

        exec = bindings.getOperationBinding("restrictDocTypes");
        if (exec != null)
            exec.execute();
        if (exec1.getErrors().isEmpty()) {
            if (FacesContext.getCurrentInstance().getMessageList().size() == 0) {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancel Successful", "Changes cancelled successfully");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }

        }
        return null;
    }

    public String saveChanges() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec_commit = bindings.getOperationBinding("Commit");
        exec_commit.execute();
        if (exec_commit.getErrors().isEmpty()) {
            OperationBinding populateStagectrl = bindings.getOperationBinding("populateStageTable");
            populateStagectrl.execute();
            exec_commit.execute();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Successful", "Changes saved successfully");
            FacesContext.getCurrentInstance().addMessage(null, msg);

        }
        return null;
    }

    public void refresh() {
        if (trainTemplateReportStatus != null)
            AdfFacesContext.getCurrentInstance().addPartialTarget(trainTemplateReportStatus);
        if(editMsgImage!=null)
            AdfFacesContext.getCurrentInstance().addPartialTarget(editMsgImage);
        if(editMsgText!=null)
            AdfFacesContext.getCurrentInstance().addPartialTarget(editMsgText);
    }

    public String saveAndValidateChanges() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec_commit = bindings.getOperationBinding("Commit");
        exec_commit.execute();
        if (exec_commit.getErrors().isEmpty()) {
            OperationBinding exec = bindings.getOperationBinding("validatePar");
            exec.execute();
            OperationBinding populateStagectrl = bindings.getOperationBinding("populateStageTable");
            populateStagectrl.execute();
            exec_commit.execute();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Successful", "Changes saved successfully and Validated Changes");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

        return null;
    }


    public void setTrainTemplateReportStatus(RichOutputText trainTemplateReportStatus) {
        this.trainTemplateReportStatus = trainTemplateReportStatus;
    }

    public RichOutputText getTrainTemplateReportStatus() {
        return trainTemplateReportStatus;
    }


    public void setEditMsgImage(RichImage editMsgImage) {
        this.editMsgImage = editMsgImage;
    }

    public RichImage getEditMsgImage() {
        return editMsgImage;
    }

    public void setEditMsgText(RichOutputFormatted editMsgText) {
        this.editMsgText = editMsgText;
    }

    public RichOutputFormatted getEditMsgText() {
        return editMsgText;
    }
}
