package gov.ofda.abacus.portal.controller.w4;

import gov.ofda.abacus.portal.controller.UIControl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.util.ComponentReference;

/**
 * Par4wDetailsBean.java
 * Purpose: Bean to manage Par 4w tracker details.
 *
 * @author Kartheek Atluri
 * @version 1.0 11/20/2013
 */
public class Par4wDetailsBean extends UIControl implements Serializable{

    @SuppressWarnings("compatibility:6423886883910572974")
    private static final long serialVersionUID = 1L;
    private ComponentReference locViewLocationTable;
    private ComponentReference locSelectedListTable;
    private ComponentReference par4wDetailsLocTable;
    private ComponentReference locListAddRemCol;
    private List<Long> manageLocList=new ArrayList<Long>();
    
    
    private ComponentReference locViewIndicatorTable;
    private ComponentReference indListAddRemCol;
    private List<Integer> manageIndicatorList=new ArrayList<Integer>();
    private ComponentReference indicatorSelectedListTable;
    private ComponentReference par4wDetailsIndTable;


    public Par4wDetailsBean() {
    }
/**Start Manage Locations******************************
     * @return
     * Called when user clicks on add from Manage Locations popup
     * Will add the location code to selected location list.
     */
    public String addtoLocList() {
        Long location4wCode=(Long)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("Location4wCode");
        if(!manageLocList.contains(location4wCode))
            manageLocList.add(location4wCode);
        AdfFacesContext.getCurrentInstance().addPartialTarget(locListAddRemCol.getComponent());
        AdfFacesContext.getCurrentInstance().addPartialTarget(locSelectedListTable.getComponent());
        return null;
    }

    /**
     * @return
     * Called when user clicks on remove from Manage Locations popup
     * Will remove the location code from selected location list.
     */
    public String removeFromLocList() {
        Long location4wCode=(Long)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("Location4wCode");
        if(manageLocList.contains(location4wCode))
         manageLocList.remove(location4wCode);
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(locListAddRemCol.getComponent());
        AdfFacesContext.getCurrentInstance().addPartialTarget(locSelectedListTable.getComponent());
        return null;
    }

    /**
     * @param popupFetchEvent
     * Called when Manage Location popup is fetched.
     * It will get the existing location list and add the selected location list.
     */
    public void manageLocFetchLsnr(PopupFetchEvent popupFetchEvent) {
        // Add event code here...getLocationsList
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding getList = bindings.getOperationBinding("getLocationsList");
        manageLocList=(List<Long>)getList.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(locViewLocationTable.getComponent());
        AdfFacesContext.getCurrentInstance().addPartialTarget(locSelectedListTable.getComponent());
    }


    /**
     * @param popupCanceledEvent
     * Called when popup is cancelled or closed.
     * It will clear the filter for select location table
     */
    public void manageLocCancelLsnr(PopupCanceledEvent popupCanceledEvent) {
        clearFilter((RichTable)locViewLocationTable.getComponent());
    }

    /**
     * @param dialogEvent
     * Called when user clicks on ok or cancel dialog for manage locations
     * It will send the selected to list to Model operation (updateLocationsInSector) to update the location list.
     * Once updated. It will commit changes and run execute on location iterator and set the row to selected row.
     */
    public void manageLocDailogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {

            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            //Get current row key to keep track of currrent row.
            DCIteratorBinding iter = (DCIteratorBinding)bindings.findIteratorBinding("Par4wisLocationView1Iterator");
            Key key=null;
            if (iter != null && iter.getCurrentRow() != null)
                key = iter.getCurrentRow().getKey();
            //call updateLocationsInSector operation with selected location list.
           OperationBinding ctrl = bindings.getOperationBinding("updateLocations");
            ctrl.getParamsMap().put("locList", this.getManageLocList());
            Boolean t=(Boolean)ctrl.execute();
            //If changes are made commit changes.
            if (t) {
                
                ctrl = bindings.getOperationBinding("Commit");
                ctrl.execute();
                if (ctrl.getErrors().isEmpty()) {
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Locations Modified", "Locations modified and saved successfully");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
            }
                //Call execute on location iterator and set the current row to previous row.
                if (this.getPar4wDetailsLocTable() != null) {
                     OperationBinding exec = bindings.getOperationBinding("ExecuteLocation");
                     exec.execute();
                    if(key!=null) {
                        iter = (DCIteratorBinding)bindings.findIteratorBinding("Par4wisLocationView1Iterator");
                        RowSetIterator rsi = iter.getRowSetIterator();
                        if (rsi.findByKey(key, 1).length > 0)
                            iter.setCurrentRowWithKey(key.toStringFormat(true));
                    }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(par4wDetailsLocTable.getComponent().getParent());
                 }
            
        }
        //Clear select location filter.
        if(locViewLocationTable!=null)
            clearFilter((RichTable)locViewLocationTable.getComponent());
    }

    /**
     * @return
     * Return the count of all locations.
     */
    public int getLocationCount()
{
   DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
    DCIteratorBinding iter=(DCIteratorBinding)bindings.findIteratorBinding("Location4wLookupView1Iterator");
    return(iter.getRowSetIterator().getRowCount());
}
    
    public void setLocViewIndicatorTable(UIComponent locViewIndicatorTable) {
        this.locViewIndicatorTable = ComponentReference.newUIComponentReference(locViewIndicatorTable);
    }

    public UIComponent getLocViewIndicatorTable() {
        return locViewIndicatorTable == null ? null : locViewIndicatorTable.getComponent();
    }
    public void setLocViewLocationTable(UIComponent locViewLocationTable) {
        this.locViewLocationTable = ComponentReference.newUIComponentReference(locViewLocationTable);
    }

    public UIComponent getLocViewLocationTable() {
        return locViewLocationTable == null ? null : locViewLocationTable.getComponent();
    }

    public void setManageLocList(List<Long> manageLocList) {
        this.manageLocList = manageLocList;
    }

    public List<Long> getManageLocList() {
        return manageLocList;
    }
    public void setLocListAddRemCol(UIComponent locListAddRemCol) {
        this.locListAddRemCol = ComponentReference.newUIComponentReference(locListAddRemCol);
    }

    public UIComponent getLocListAddRemCol() {
        return locListAddRemCol == null ? null : locListAddRemCol.getComponent();
    }
    public void setPar4wDetailsLocTable(UIComponent par4wDetailsLocTable) {
        this.par4wDetailsLocTable = ComponentReference.newUIComponentReference(par4wDetailsLocTable);
    }

    public UIComponent getPar4wDetailsLocTable() {
        return par4wDetailsLocTable == null ? null : par4wDetailsLocTable.getComponent();
    }

    public void setLocSelectedListTable(UIComponent locSelectedListTable) {
        this.locSelectedListTable = ComponentReference.newUIComponentReference(locSelectedListTable);
    }

    public UIComponent getLocSelectedListTable() {
        return locSelectedListTable == null ? null : locSelectedListTable.getComponent();
    }
/******************************************End Manage Location *****************************************************/
/******************************************Start Manage Indicator **************************************************/
 /**
  * @param popupFetchEvent
  * Called when Manage Indicator popup is fetched.
  * It will get the existing indicator list and adds to the selected list.
  */
 public void manageIndicatorFetchLsnr(PopupFetchEvent popupFetchEvent) {
     // Add event code here...getLocationsList
     DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
     OperationBinding getList = bindings.getOperationBinding("getIndicatorsList");
     manageIndicatorList=(List<Integer>)getList.execute();
     AdfFacesContext.getCurrentInstance().addPartialTarget(indListAddRemCol.getComponent());
     AdfFacesContext.getCurrentInstance().addPartialTarget(indicatorSelectedListTable.getComponent());
 }
    /**
     * @return
     * Called when user clicks on add from Manage indicator popup
     * Will add the indicator seq to selected list.
     */
    public String addtoIndicatorList() {
        Integer ind4wSeq=(Integer)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("Ind4wSeq");
        if(!manageIndicatorList.contains(ind4wSeq))
            manageIndicatorList.add(ind4wSeq);
        AdfFacesContext.getCurrentInstance().addPartialTarget(indListAddRemCol.getComponent());
        AdfFacesContext.getCurrentInstance().addPartialTarget(indicatorSelectedListTable.getComponent());
        return null;
    }
    /**
     * @return
     * Called when user clicks on remove from Manage indicator popup
     * Will remove the indicator from selected list.
     */
    public String removeFromIndicatorList() {
        Integer ind4wSeq=(Integer)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("Ind4wSeq");
        if(manageIndicatorList.contains(ind4wSeq))
         manageIndicatorList.remove(ind4wSeq);
        AdfFacesContext.getCurrentInstance().addPartialTarget(indListAddRemCol.getComponent());
        AdfFacesContext.getCurrentInstance().addPartialTarget(indicatorSelectedListTable.getComponent());
        return null;
    }
    /**
     * @param dialogEvent
     * Called when user clicks on ok or cancel dialog for manage indicators
     * It will send the selected to list to Model operation (updateIndicators) to update the indicator list.
     * Once updated,it will commit changes and run execute on indicator iterator and set the row to selected row.
     */
    public void manageIndicatorDialogLnsr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {

            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = (DCIteratorBinding)bindings.findIteratorBinding("Par4wIndicatorView1Iterator");
            Key key=null;
            if (iter != null && iter.getCurrentRow() != null)
                key = iter.getCurrentRow().getKey();
           OperationBinding ctrl = bindings.getOperationBinding("updateIndicators");
            ctrl.getParamsMap().put("indList", this.getManageIndicatorList());
            Boolean t=(Boolean)ctrl.execute();
            if (t) {
                ctrl = bindings.getOperationBinding("Commit");
                ctrl.execute();
                if (ctrl.getErrors().isEmpty()) {
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Indicators Modified", "Indicators modified and saved successfully");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
            }
                     OperationBinding exec = bindings.getOperationBinding("ExecuteIndicator");
                     exec.execute();
                    if(key!=null) {

                        iter = (DCIteratorBinding)bindings.findIteratorBinding("Par4wIndicatorView1Iterator");
                        RowSetIterator rsi = iter.getRowSetIterator();
                        if (rsi.findByKey(key, 1).length > 0)
                            iter.setCurrentRowWithKey(key.toStringFormat(true));
                    }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(par4wDetailsIndTable.getComponent().getParent());
                    AdfFacesContext.getCurrentInstance().addPartialTarget(par4wDetailsLocTable.getComponent().getParent().getParent());            
        }
        //Clear select location filter.
        if(locViewIndicatorTable!=null)
            clearFilter((RichTable)locViewIndicatorTable.getComponent());
    }
    /**
     * @return
     * Return the count of all indicators.
     */
    public int getIndicatorCount()
    {
       DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter=(DCIteratorBinding)bindings.findIteratorBinding("Indicator4wLookupView1Iterator");
        return(iter.getRowSetIterator().getRowCount());
    }
    /**
     * @param popupCanceledEvent
     * Called when popup is cancelled or closed.
     * It will clear the filter for select indicator table
     */
    public void manageIndicatorCancelLsnr(PopupCanceledEvent popupCanceledEvent) {
        clearFilter((RichTable)locViewIndicatorTable.getComponent());
    }
    public void setManageIndicatorList(List<Integer> manageIndicatorList) {
        this.manageIndicatorList = manageIndicatorList;
    }

    public List<Integer> getManageIndicatorList() {
        return manageIndicatorList;
    }
    public void setIndListAddRemCol(UIComponent indListAddRemCol) {
        this.indListAddRemCol = ComponentReference.newUIComponentReference(indListAddRemCol);
        
    }

    public UIComponent getIndListAddRemCol() {
        return indListAddRemCol == null ? null : indListAddRemCol.getComponent();
    }
    public void setIndicatorSelectedListTable(UIComponent indicatorSelectedListTable) {
        this.indicatorSelectedListTable = ComponentReference.newUIComponentReference(indicatorSelectedListTable);
    }

    public UIComponent getIndicatorSelectedListTable() {
        return indicatorSelectedListTable == null ? null : indicatorSelectedListTable.getComponent();
    }

    /******************************************End Manage Indicator **************************************************/
 /*    public void uom1ChgLsnr(ValueChangeEvent vce) {
         DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
         DCIteratorBinding iter = (DCIteratorBinding)bindings.findIteratorBinding("Par4wisLocationView1Iterator");
         Row r=iter.getCurrentRow();
         BigDecimal uom2=(BigDecimal)r.getAttribute("Uom2Value");
         BigDecimal uom3=new BigDecimal(0);
         //String tmp=(String)vce.getNewValue();
         //BigDecimal uom1=new BigDecimal(tmp);
         BigDecimal uom1=(BigDecimal)vce.getNewValue();
         if(uom1!=null)
            uom3=uom3.add(uom1);
         if(uom2!=null)
             uom3=uom3.add(uom2);
         uom3Value.setValue(uom3);
         AdfFacesContext.getCurrentInstance().addPartialTarget(uom3Value);
     }

     public void uom2ChgLsnr(ValueChangeEvent vce) {
         DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
         DCIteratorBinding iter = (DCIteratorBinding)bindings.findIteratorBinding("Par4wisLocationView1Iterator");
         Row r=iter.getCurrentRow();
         BigDecimal uom1=(BigDecimal)r.getAttribute("Uom1Value");
         BigDecimal uom3=new BigDecimal(0);
        // String tmp=(String)vce.getNewValue();
        // BigDecimal uom2=new BigDecimal(tmp);
        BigDecimal uom2=(BigDecimal)vce.getNewValue();
         if(uom2!=null)
             uom3=uom3.add(uom2);
         if(uom1!=null)
             uom3=uom3.add(uom1);
         uom3Value.setValue(uom3);
         AdfFacesContext.getCurrentInstance().addPartialTarget(uom3Value);
     }
     */

    public void setPar4wDetailsIndTable(UIComponent par4wDetailsIndTable) {
        this.par4wDetailsIndTable = ComponentReference.newUIComponentReference(par4wDetailsIndTable);
    }

    public UIComponent getPar4wDetailsIndTable() {
        return par4wDetailsIndTable == null ? null : par4wDetailsIndTable.getComponent();
    }
}
