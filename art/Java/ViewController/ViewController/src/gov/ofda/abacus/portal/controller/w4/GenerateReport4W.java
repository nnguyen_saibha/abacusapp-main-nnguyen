package gov.ofda.abacus.portal.controller.w4;

import gov.ofda.abacus.portal.controller.UIControl;

import java.io.Serializable;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.model.SelectItem;

import javax.swing.JPanel;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import org.apache.myfaces.trinidad.util.ComponentReference;

public class GenerateReport4W extends UIControl implements Serializable{
    @SuppressWarnings("compatibility:-4441445981418756909")
    private static final long serialVersionUID = 1L;
    private String projectName;
    private String awardeeName;
    private ComponentReference projectOneChoice;
    private ComponentReference awardeeOneChoice;

    public GenerateReport4W() {
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName() {
          return getNameFromOneChoice((RichSelectOneChoice)this.getProjectOneChoice());
      }
    public String getAwardeeName() {
          return getNameFromOneChoice((RichSelectOneChoice)this.getAwardeeOneChoice());
      }
    public void setAwardeeName(String awardeeName) {
        this.awardeeName = awardeeName;
    }

    public void setProjectOneChoice(UIComponent projectOneChoice) {
        this.projectOneChoice = ComponentReference.newUIComponentReference(projectOneChoice);
    }

    public UIComponent getProjectOneChoice() {
        return projectOneChoice == null ? null : projectOneChoice.getComponent();
    }

    public void setAwardeeOneChoice(UIComponent awardeeOneChoice) {
        this.awardeeOneChoice = ComponentReference.newUIComponentReference(awardeeOneChoice);
    }

    public UIComponent getAwardeeOneChoice() {
        return awardeeOneChoice == null ? null : awardeeOneChoice.getComponent();
    }
}
