package gov.ofda.abacus.portal.controller.dashboard;

import java.io.Serializable;

import java.util.Map;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.binding.OperationBinding;

public class ARTAppScopeBean implements Serializable {
    @SuppressWarnings("compatibility:-4320850330337432714")
    private static final long serialVersionUID = 1L;
    private Map<String,String> constants;
    private Map<String,String> artConstants;

    public ARTAppScopeBean() {
        super();
        
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =bindingContext.findBindingContainer("gov_ofda_abacus_art_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getAppConstants");
        exec.execute();
        constants = (Map<String, String>) exec.getResult();
        exec = dcBindingContainer.getOperationBinding("getArtConstants");
        exec.execute();
        artConstants = (Map<String, String>) exec.getResult();  
    }
    public void setConstants(Map<String, String> constants) {
        this.constants = constants;
    }

    public Map<String, String> getConstants() {
        return constants;
    }
    public void setArtConstants(Map<String, String> artConstants) {
        this.artConstants = artConstants;
    }

    public Map<String, String> getArtConstants() {
        return artConstants;
    }
}