package gov.ofda.abacus.exception;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.context.ExceptionHandler;

public class AbacusExceptionHandler extends ExceptionHandler {
    private static ADFLogger logger =ADFLogger.createADFLogger(gov.ofda.abacus.exception.AbacusExceptionHandler.class);
    public AbacusExceptionHandler() {
        super();
    }

    @Override
    public void handleException(FacesContext facesContext, Throwable throwable, PhaseId phaseId) throws Throwable {
        String error_msg;
        error_msg=throwable.getMessage();
        if(error_msg != null &&
            error_msg.indexOf("ADF_FACES-60003") > -1) {
            logger.log(logger.WARNING,"Handled: ADF_FACES-60003");
        }
        else
        {
            //Default ADFc exception handler
            throw throwable;
        }
    }
}
