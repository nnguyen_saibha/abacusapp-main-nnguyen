package gov.ofda.abacus.portal.pojo;

import java.io.Serializable;

public class SortItem implements Serializable {
    @SuppressWarnings("compatibility:-4583310033302534118")
    private static final long serialVersionUID = 1L;
    private String label;
    private String value;
    private String sortAscLabel="A to Z";
    private String sortDescLabel="Z to A";
    private String sortValue="asc";
    public SortItem(String label,String value) {
        super();
        this.label=label;
        this.value=value;
    }
    public SortItem(String label,String value,String sortAscLabel,String sortDescLabel) {
        super();
        this.label=label;
        this.value=value;
        this.sortAscLabel=sortAscLabel;
        this.sortDescLabel=sortDescLabel;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setSortAscLabel(String sortAscLabel) {
        this.sortAscLabel = sortAscLabel;
    }

    public String getSortAscLabel() {
        return sortAscLabel;
    }

    public void setSortDescLabel(String sortDescLabel) {
        this.sortDescLabel = sortDescLabel;
    }

    public String getSortDescLabel() {
        return sortDescLabel;
    }

    public void setSortValue(String sortValue) {
        this.sortValue = sortValue;
    }

    public String getSortValue() {
        return sortValue;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
                    return true;
                }
     if(o instanceof SortItem) {
         SortItem object=(SortItem)o;
         if(object.getValue()!=null && object.getValue().equals(this.getValue()))
            return true;
     }
     return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + value.hashCode();
        return result;
    }
}
