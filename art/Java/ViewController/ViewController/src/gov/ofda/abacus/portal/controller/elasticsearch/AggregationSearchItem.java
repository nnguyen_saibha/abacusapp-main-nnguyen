package gov.ofda.abacus.portal.controller.elasticsearch;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

public class AggregationSearchItem implements Serializable{
    @SuppressWarnings("compatibility:-7058281498354259940")
    private static final long serialVersionUID = 1L;
    private String term;
    private String termLabel;
    private Integer level;
    private String path;
    private Integer size;
    private String orderBy="_key";
    private String sortOrder="asc";
    private String totalBeneficiaries="TOTAL_REACHED";
    private String totalIDPBeneficiaries="IDP_REACHED";
    private String name;
    private Boolean isTermKeyword=false;
    private Boolean isLabelKeyword=false;
    private Boolean isDate=false;
    private String interval="1M";
    private String otherRestriction="";
    private Integer resultDisplayOrder=1;
    private Boolean hidden=false;
    private Boolean disclosed=true;
    private List<String> ignoreList=new ArrayList<String>();
    public AggregationSearchItem() {
        super();
    }
    public AggregationSearchItem(String name,String termLabel,String term, Integer level,String path,Integer size,String orderBy,String sortOrder,String totalBeneficiaries,String totalIDPBeneficiaries,Boolean isTermKeyword,Boolean isLabelKeyword,Integer resultDisplayOrder,Boolean hidden) {
        super();
        this.name=name;
        this.term=term;
        this.termLabel=termLabel;
        this.level=level;
        this.path=path;
        this.size=size;
        this.orderBy=orderBy;
        this.sortOrder=sortOrder;
        this.totalBeneficiaries=totalBeneficiaries;
        this.totalIDPBeneficiaries=totalIDPBeneficiaries;
        this.isTermKeyword=isTermKeyword;
        this.isLabelKeyword=isLabelKeyword;
        this.resultDisplayOrder=resultDisplayOrder;
        this.hidden=hidden;

    }
    public AggregationSearchItem(String name,String termLabel,String term, Integer level,String path,Integer size,String orderBy,String sortOrder,String totalBeneficiaries,String totalIDPBeneficiaries,Boolean isTermKeyword,Boolean isLabelKeyword,Integer resultDisplayOrder,Boolean hidden, Boolean disclosed) {
        super();
        this.name=name;
        this.term=term;
        this.termLabel=termLabel;
        this.level=level;
        this.path=path;
        this.size=size;
        this.orderBy=orderBy;
        this.sortOrder=sortOrder;
        this.totalBeneficiaries=totalBeneficiaries;
        this.totalIDPBeneficiaries=totalIDPBeneficiaries;
        this.isTermKeyword=isTermKeyword;
        this.isLabelKeyword=isLabelKeyword;
        this.resultDisplayOrder=resultDisplayOrder;
        this.hidden=hidden;
        this.disclosed=disclosed;

    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getLevel() {
        return level;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getSize() {
        return size;
    }

    public void setIgnoreList(List<String> ignoreList) {
        this.ignoreList = ignoreList;
    }

    public List<String> getIgnoreList() {
        return ignoreList;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setResultDisplayOrder(Integer resultDisplayOrder) {
        this.resultDisplayOrder = resultDisplayOrder;
    }

    public Integer getResultDisplayOrder() {
        return resultDisplayOrder;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getTerm() {
        return term;
    }

    public void setTermLabel(String termLabel) {
        this.termLabel = termLabel;
    }

    public String getTermLabel() {
        return termLabel;
    }
    @Override
    public boolean equals(Object o) {
        if (o == this) {
                    return true;
                }
     if(o instanceof AggregationSearchItem) {
         AggregationSearchItem object=(AggregationSearchItem)o;
         if(object.getTerm()!=null && object.getTerm().equals(this.getTerm()))
            return true;
     }
     return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + term.hashCode();
        return result;
    }

    public void setTotalBeneficiaries(String totalBeneficiaries) {
        this.totalBeneficiaries = totalBeneficiaries;
    }

    public String getTotalBeneficiaries() {
        return totalBeneficiaries;
    }

    public void setTotalIDPBeneficiaries(String totalIDPBeneficiaries) {
        this.totalIDPBeneficiaries = totalIDPBeneficiaries;
    }

    public String getTotalIDPBeneficiaries() {
        return totalIDPBeneficiaries;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setIsTermKeyword(Boolean isTermKeyword) {
        this.isTermKeyword = isTermKeyword;
    }

    public Boolean getIsTermKeyword() {
        return isTermKeyword;
    }

    public void setIsLabelKeyword(Boolean isLabelKeyword) {
        this.isLabelKeyword = isLabelKeyword;
    }

    public Boolean getIsLabelKeyword() {
        return isLabelKeyword;
    }

    public void setIsDate(Boolean isDate) {
        this.isDate = isDate;
    }

    public Boolean getIsDate() {
        return isDate;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public String getInterval() {
        return interval;
    }

    public void setOtherRestriction(String otherRestriction) {
        this.otherRestriction = otherRestriction;
    }

    public String getOtherRestriction() {
        return otherRestriction;
    }

    public void setDisclosed(Boolean disclosed) {
        this.disclosed = disclosed;
    }

    public Boolean getDisclosed() {
        return disclosed;
    }
}

