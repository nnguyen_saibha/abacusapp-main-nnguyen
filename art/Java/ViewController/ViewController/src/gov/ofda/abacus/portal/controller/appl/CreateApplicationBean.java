package gov.ofda.abacus.portal.controller.appl;

import java.math.BigDecimal;

import java.util.HashMap;
import java.util.Map;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class CreateApplicationBean {
    boolean conceptPaperRequired=false;
    boolean modApplPaperRequired=false;
    public CreateApplicationBean() {
    }

    public void setInitialValues() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl=ctrl= bindings.getOperationBinding("getCurrentUserAwardee");
        String awardee = (String) ctrl.execute();
        ctrl= bindings.getOperationBinding("getCurrentFy");
        Integer year = (Integer) ctrl.execute();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(!"0".equals(awardee))
        {
            pfm.put("awardee",awardee);
            setDependentLOVs();
        }
        pfm.put("fiscalYear",year);
        pfm.put("bureau" ,"BHA");
        pfm.put("office" ,null); 
        pfm.put("fundingOppty" , new BigDecimal(20)); // Non-Competitive
        
    }

    public void saveChanges() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl= bindings.getOperationBinding("createAppl");
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("applicationType" ,pfm.get("applicationType"));
        paramMap.put("programName" ,pfm.get("programName"));
        paramMap.put("programType" ,pfm.get("programType"));
        paramMap.put("fiscalYear" ,pfm.get("fiscalYear"));
        paramMap.put("awardee" ,pfm.get("awardee"));
        paramMap.put("cpPaaId" ,pfm.get("cpPaaId"));
        paramMap.put("modPaaId" ,pfm.get("modPaaId"));
        paramMap.put("bureau" ,pfm.get("bureau"));
        paramMap.put("office" ,pfm.get("office"));        
        paramMap.put("requestType", pfm.get("requestType"));
        paramMap.put("sensitive", pfm.get("sensitive")); 
        paramMap.put("fundingOppty", pfm.get("fundingOppty"));   
    
        
        ctrl.getParamsMap().put("paramMap", paramMap);
        Long id = (Long) ctrl.execute();
        pfm.put("paaId", id);
    }
    public void awardeeReturnPopupLsnr(ReturnPopupEvent returnPopupEvent) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding activityIter = bindings.findIteratorBinding("AwardeeLookupLOV1Iterator");
        RowSetIterator activityRSIter = activityIter.getRowSetIterator();
        Row r = activityRSIter.getCurrentRow();
        if (r != null) {
            String awardee = (String) r.getAttribute("AwardeeName");
            if (r.getAttribute("AwardeeAcronym") != null)
                awardee += " (" + r.getAttribute("AwardeeAcronym") + ")";
            pageFlowScope.put("awardeeName", awardee);
            pageFlowScope.put("awardee", r.getAttribute("AwardeeCode"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent().getParent());
            setDependentLOVs();
        }
    }

    public void conceptPaperReturnLsnr(ReturnPopupEvent returnPopupEvent) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding activityIter = bindings.findIteratorBinding("ApplConceptPaperLOV1Iterator");
        RowSetIterator activityRSIter = activityIter.getRowSetIterator();
        Row r = activityRSIter.getCurrentRow();
        if (r != null) {
            pageFlowScope.put("cpPaaId", r.getAttribute("PaaId"));
            pageFlowScope.put("cpApplNbr", r.getAttribute("ApplNbr"));
            pageFlowScope.put("cpProgramName", r.getAttribute("ApplProgramName"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent().getParent());
        }
    }

    public void modAwardReturnLsnr(ReturnPopupEvent returnPopupEvent) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding activityIter = bindings.findIteratorBinding("ApplAwardLOV1Iterator");
        RowSetIterator activityRSIter = activityIter.getRowSetIterator();
        Row r = activityRSIter.getCurrentRow();
        if (r != null) {
            pageFlowScope.put("modPaaId", r.getAttribute("Awid"));
            pageFlowScope.put("modApplNbr", r.getAttribute("AwardNbr"));
            pageFlowScope.put("modAwardNbr", r.getAttribute("AwardNbr"));
            pageFlowScope.put("modProgramName", r.getAttribute("ProposalName"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent().getParent());
        }
    }
    
    private void setDependentLOVs(){
        setModApplLOV();
        setConceptPaperLOV();
        
    }
        
        
    private void setModApplLOV() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl= bindings.getOperationBinding("setModApplLOV");
        ctrl.getParamsMap().put("p_awardee_code", pfm.get("awardee"));
        ctrl.execute();
        
      
        pfm.put("modPaaId", null);
        pfm.put("modApplNbr", null);
        pfm.put("modAwardNbr", null);
        pfm.put("modProgramName", null);
     }
    
    private void setConceptPaperLOV() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding  ctrl= bindings.getOperationBinding("setConceptPaperLOV");
        ctrl.getParamsMap().put("p_awardee_code", pfm.get("awardee"));
        ctrl.execute();
    }


    public boolean isConceptPaperRequired() {
        return conceptPaperRequired;
    }

    public boolean isModApplPaperRequired() {
        return modApplPaperRequired;
    }

    public void applicationTypeValueChgLnsr(ValueChangeEvent vce) {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfm.put("applicationType" ,vce.getNewValue());
       
        if("11".equals(vce.getNewValue().toString()))  
        {
            conceptPaperRequired=true;
             this.setConceptPaperLOV();            
        }
        else  //Application or  Concept Paper
            conceptPaperRequired=false;   
        if(pfm.get("requestType")!=null && "9".equals(vce.getNewValue().toString()) && "18".equals(pfm.get("requestType").toString()))
                pfm.put("requestType",null);
    }
    
    public void requestTypeValueChgLnsr(ValueChangeEvent vce) {
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("requestType" ,vce.getNewValue());
       
        if("13".equals(vce.getNewValue().toString()))  //Request Type -    New 
            modApplPaperRequired=false;         
        else 
        {  //Request Type -     Funded Mod / Unfunded Mod
            modApplPaperRequired=true;        
            this.setModApplLOV();
        }
    }
    }
