package gov.ofda.abacus.portal.controller.elasticsearch;


import java.io.Serializable;

import java.text.NumberFormat;

public class BucketItem implements Serializable {
    @SuppressWarnings("compatibility:2686591481416080545")
    private static final long serialVersionUID = 1L;
    private String key;
    private String key_as_string;
    private Long doc_count;
    private Long total_reached;
    private Long idp_reached;
    private Boolean selected=false;
        
    public BucketItem() {
        super();
    }
    public BucketItem(String key,String key_as_string, Long doc_count) {
        super();
        this.key=key;
        this.key_as_string=key_as_string;
        this.doc_count=doc_count;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
    @Override
    public boolean equals(Object o) {
        if (o == this) {
                    return true;
                }
     if(o instanceof BucketItem) {
         BucketItem object=(BucketItem)o;
         if(object.getKey()!=null && object.getKey().equals(this.getKey()))
            return true;
     }
     return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + key.hashCode();
        return result;
    }

    public void setKey_as_string(String key_as_string) {
        this.key_as_string = key_as_string;
    }

    public String getKey_as_string() {
        if(key_as_string==null)
            return key;
        return key_as_string;
    }

    public void setDoc_count(Long doc_count) {
        this.doc_count = doc_count;
    }

    public Long getDoc_count() {
        return doc_count;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Boolean getSelected() {
        return selected;
    }
    public void setTotal_reached(Long total_reached) {
        this.total_reached = total_reached;
    }

    public Long getTotal_reached() {
        return total_reached;
    }

    public void setIdp_reached(Long idp_reached) {
        this.idp_reached = idp_reached;
    }

    public Long getIdp_reached() {
        return idp_reached;
    }
    public String getDoc_countFmt() {
        return format(doc_count);
    }
    public String getTotal_reachedFmt() {
        return format(total_reached);
    }
    public String getIdp_reachedFmt() {
        return format(idp_reached);
    }
                  
            
   private String format(Long value) {
       NumberFormat formatter;
       formatter = NumberFormat.getInstance();
       return formatter.format(value);
   }
}

