package gov.ofda.abacus.portal.pojo.Comparator;

import gov.ofda.abacus.portal.controller.elasticsearch.BucketItem;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class AwardStatusComparator implements Comparator<BucketItem> {
    public AwardStatusComparator() {
        super();
    }
    
    private static final Map<String,Integer> l;
    static{
    Map<String,Integer> tmp=new HashMap<String,Integer>();
         tmp.put("In Progress",1);
         tmp.put("In-Progress",1);
         tmp.put("Expired",2);
         tmp.put("Closed",3);
         l=Collections.unmodifiableMap(tmp);
    }

    @Override
    public int compare(BucketItem bucketItem, BucketItem bucketItem2) {
        Integer item1=l.get(bucketItem.getKey());
        Integer item2=l.get(bucketItem2.getKey());
        if(item1!=null && item2!=null)
            return item1.compareTo(item2);
        return (int) (bucketItem.getKey().compareTo(bucketItem2.getKey()));
    }
}
