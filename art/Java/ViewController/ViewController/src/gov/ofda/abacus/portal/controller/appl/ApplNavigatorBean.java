package gov.ofda.abacus.portal.controller.appl;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.event.ReturnEvent;

public class ApplNavigatorBean {
    public ApplNavigatorBean() {
    }

    public void setInitialValues() {
        Integer p_ofda = null;
        Integer p_admin = null;
        String p_awardee_code =  null;
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl=null;
        //Setting User Role for Navigator
        SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
        String userType="partner";
        
        if(sc.isUserInRole("APP_CONNECT")) {
        userType="ofdauser"  ;
        p_ofda = 1;
        }
        if(sc.isUserInRole("admin")){
            userType="admin";
            p_admin = 1;
            p_ofda =2;
        }


    //Setting Awardee code for Partner
    if(userType.equals("partner")){ 
    
        ctrl= bindings.getOperationBinding("getCurrentUserAwardee");
        String awardee = (String) ctrl.execute();
        p_awardee_code = awardee;
    }
        ctrl= bindings.getOperationBinding("getCurrentFy");
        Integer year = (Integer) ctrl.execute();
        OperationBinding exec = bindings.getOperationBinding("ExecutePAATable");
        exec.getParamsMap().put("p_fy", year);
        exec.getParamsMap().put("p_awardee_code", p_awardee_code);
        exec.getParamsMap().put("p_ofda", p_ofda);
        exec.getParamsMap().put("p_admin", p_admin);
        exec.execute();
    
    }

    public void createReturnLnsr(ReturnEvent returnEvent) {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(pfm.get("paaId")!=null)
        {
         FacesContext context = FacesContext.getCurrentInstance();
         context.getApplication().getNavigationHandler().handleNavigation(context,null,"details");
        }
    }
    
    
    
        
    public void deleteApplDialogLsnr(DialogEvent de) {
            if (DialogEvent.Outcome.yes == de.getOutcome()) {
                DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding exec = bindings.getOperationBinding("deleteAppl");
                exec.execute();
                FacesContext fc = FacesContext.getCurrentInstance();
                String errorMsg = (String) exec.getResult();
                if(errorMsg==null)
                    fc.addMessage(null,
                                  new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                   "Application deleted successfully.", null));
                else
                {
                  /*if(errorMsg.contains("Unexpected Error")) {
                      fc.addMessage(null,
                                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                     "Unexpected Error. Please contact Abacus Team (abacus@ofda.gov) with action information.", null));
                      MessageBean mb=new MessageBean();
                      mb.sendAdminEmail("Error Delete Action", errorMsg);
                  }
                  else*/
                    fc.addMessage(null,
                                  new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                   errorMsg, null));
             }
            }
    }
    
    
    public String getDeleteApplValid() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("deleteApplCheck");
        exec.execute();
        String errorMsg = (String) exec.getResult();
        Map viewScope=AdfFacesContext.getCurrentInstance().getViewScope();
        viewScope.put("deleteApplErrorMsg", errorMsg);
        return errorMsg;
    }

}
