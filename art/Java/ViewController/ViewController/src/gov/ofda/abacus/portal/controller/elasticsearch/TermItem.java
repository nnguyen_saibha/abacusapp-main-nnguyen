package gov.ofda.abacus.portal.controller.elasticsearch;

import java.io.Serializable;

public class TermItem implements Serializable{
    @SuppressWarnings("compatibility:-3111792366852535834")
    private static final long serialVersionUID = 1L;
    private String term;
    private String value;
    private String termLabel;
    private String valueLabel;
    
    public TermItem() {
        super();
    }
    public TermItem(String termLabel,String term,String valueLabel,String value) {
        super();
        this.term=term;
        this.value=value;
        this.termLabel=termLabel;
        this.valueLabel=valueLabel;
    }
    public void setTerm(String term) {
        this.term = term;
    }

    public String getTerm() {
        return term;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setTermLabel(String termLabel) {
        this.termLabel = termLabel;
    }

    public String getTermLabel() {
        return termLabel;
    }
    @Override
    public boolean equals(Object o) {
        if (o == this) {
                    return true;
                }
     if(o instanceof TermItem) {
         TermItem object=(TermItem)o;
         if(object.getTerm()!=null && object.getTerm().equals(this.getTerm())&& object.getValue()!=null && object.getValue().equals(this.getValue()))
            return true;
     }
     return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + value.hashCode();
        return result;
    }

    public void setValueLabel(String valueLabel) {
        this.valueLabel = valueLabel;
    }

    public String getValueLabel() {
        return valueLabel;
    }
}
