package gov.ofda.abacus.portal.controller.elasticsearch;


import gov.ofda.abacus.portal.pojo.Award;
import gov.ofda.abacus.portal.pojo.Report;
import gov.ofda.abacus.portal.pojo.Sector;
import gov.ofda.abacus.portal.pojo.SortItem;

import gov.ofda.abacus.portal.pojo.UserDashboardPreference;
import gov.ofda.abacus.portal.pojo.UserPreference;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;

import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

import javax.faces.context.FacesContext;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFCellUtil;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.NumberToTextConverter;

public class SearchItem implements Serializable {
    @SuppressWarnings("compatibility:-1077025609819587548")
    private static final long serialVersionUID = 1L;
    private Integer startFY=2017;
    private Integer endFY=2018;
    private List<TermItem> termFilters=new ArrayList<TermItem>();
    private Map<String,List<TermItem>> tfMap=new HashMap<String,List<TermItem>>();
    private String status[]={"In-Progress","Expired","Closed"};
    private ElasticSearchRestBean esrb=new ElasticSearchRestBean();
    private ResultItem ritem=new ResultItem();
    private Map<String,AggregationSearchItem> aggSearchList=new HashMap<String,AggregationSearchItem>();
    private Boolean restrictAwardee=true;
    private String awardeeCode;

    private String searchQuery="";
    private Boolean favoritesOnly=false;
    private String sortOrder="relevance";
    private Integer size=10;
    private Integer from=0;
    private List<Integer> nextList=new ArrayList<Integer>();
    private List<SortItem> sortList=new ArrayList<SortItem>();
    private List<SortItem> selectedSortList=new ArrayList<SortItem>();
    private UserPreference userPreference;
    private boolean hasUserPreference=true;
    private boolean showInnerHits=true;
    private boolean showAggregations=true;
    public SearchItem() {
        super();
        SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
        if(sc.isUserInRole("admin")||sc.isUserInRole("ofda")||sc.isUserInRole("APP_CONNECT")) {
            AggregationSearchItem asiBureau=new AggregationSearchItem("Bureau","bureau_code","bureau_code", 0,null,20,"_key","asc",null,null,true,true,2,false);
            asiBureau.getIgnoreList().add("office_code");
            asiBureau.getIgnoreList().add("ofda_division_code");
            asiBureau.getIgnoreList().add("ofda_team_code");
            aggSearchList.put("bureau_code",asiBureau);
            
            AggregationSearchItem asiOffice=new AggregationSearchItem("Office","office_code","office_code", 0,null,20,"_key","asc",null,null,true,true,3,false);
            asiOffice.getIgnoreList().add("ofda_division_code");
            asiOffice.getIgnoreList().add("ofda_team_code");
            aggSearchList.put("office_code",asiOffice);
            
            AggregationSearchItem asiDiv=new AggregationSearchItem("Division","ofda_division_code","ofda_division_code", 0,null,20,"_key","asc",null,null,true,true,4,false);
            asiDiv.getIgnoreList().add("ofda_team_code");
            aggSearchList.put("ofda_division_code",asiDiv);
            aggSearchList.put("ofda_team_code",new AggregationSearchItem("Team","ofda_team_code","ofda_team_code", 0,null,50,"_key","asc",null,null,true,true,5,false));
            aggSearchList.put("aor_name",new AggregationSearchItem("AOR","aor_name","aor_name", 0,null,2000,"_key","asc",null,null,true,true,11,false));
            sortList.add(new SortItem("Bureau","bureau_code.keyword"));
            sortList.add(new SortItem("office","office_code.keyword"));
            sortList.add(new SortItem("Division","ofda_division_code.keyword"));
            sortList.add(new SortItem("Team","ofda_team_code.keyword"));
            sortList.add(new SortItem("Region","region_name.keyword"));
            sortList.add(new SortItem("Country","cname.keyword"));
            sortList.add(new SortItem("Project","pname.keyword"));
            sortList.add(new SortItem("Awardee","awardee.keyword"));
            sortList.add(new SortItem("Award Nbr","award_nbr.keyword"));
            sortList.add(new SortItem("Award Amount","award_amt","Low to High","High to Low"));
            sortList.add(new SortItem("Award Start Date","award_start_date","Old to New","New to Old"));
            sortList.add(new SortItem("Award End Date","award_end_date","Old to New","New to Old"));
            sortList.add(new SortItem("Award Duration","award_duration","Low to High","High to Low"));
            sortList.add(new SortItem("AOR","aor_name.keyword"));
            selectedSortList.add(new SortItem("Project","pname.keyword"));
            selectedSortList.add(new SortItem("Awardee","awardee.keyword"));
            selectedSortList.add(new SortItem("Award Nbr","award_nbr.keyword"));
        }
        else {
            sortList.add(new SortItem("Region","region_name.keyword"));
            sortList.add(new SortItem("Country","cname.keyword"));
            sortList.add(new SortItem("Project","pname.keyword"));
            sortList.add(new SortItem("Award Nbr","award_nbr.keyword"));
            sortList.add(new SortItem("Award Amount","award_amt","Low to High","High to Low"));
            sortList.add(new SortItem("Award Start Date","award_start_date","Old to New","New to Old"));
            sortList.add(new SortItem("Award End Date","award_end_date","Old to New","New to Old"));
            sortList.add(new SortItem("Award Duration","award_duration","Low to High","High to Low"));
            selectedSortList.add(new SortItem("Project","pname.keyword"));
            selectedSortList.add(new SortItem("Award Nbr","award_nbr.keyword"));
        }
        AggregationSearchItem asAwardeeType=new AggregationSearchItem("Awardee Type","awardee_type_code","awardee_type_code", 0,null,50,"_key","asc",null,null,true,true,9,false);
        AggregationSearchItem asAwardee=new AggregationSearchItem("Awardee","awardee","awardee", 0,null,1000,"_key","asc","TOTAL_REACHED","IDP_REACHED",true,true,10,false);
        if(sc.isUserInRole("partner"))
        {
            asAwardee.setHidden(true);
            asAwardeeType.setHidden(true);
        }
        aggSearchList.put("awardee_type_code",asAwardeeType);
        aggSearchList.put("awardee",asAwardee);
        aggSearchList.put("IS_COMPLIANT",new AggregationSearchItem("Report Compliance","IS_COMPLIANT","IS_COMPLIANT", 1,"reports",5,"_key","desc",null,null,true,true,16,true));
        aggSearchList.put("award_status",new AggregationSearchItem("Award Status","award_status","award_status", 0,null,10,"_key","asc",null,null,true,true,1,false));
        AggregationSearchItem asRegion=new AggregationSearchItem("Region","region_name","region_name", 0,null,25,"_key","asc","TOTAL_REACHED","IDP_REACHED",true,true,6,false);
        asRegion.getIgnoreList().add("cname");
        aggSearchList.put("region_name",asRegion);
        aggSearchList.put("cname",new AggregationSearchItem("Country","cname","cname", 0,null,250,"_key","asc","TOTAL_REACHED","IDP_REACHED",true,true,7,false));
        aggSearchList.put("pname",new AggregationSearchItem("Project","pname","pname", 0,null,1000,"_key","asc","TOTAL_REACHED","IDP_REACHED",true,true,8,false));
        aggSearchList.put("award_duration",new AggregationSearchItem("Award Duration (Months)","award_duration","award_duration", 0,null,100,"_key","asc",null,null,false,false,12,false));
        AggregationSearchItem fy=new AggregationSearchItem("Report Year","RPT_FY","RPT_FY", 1,"reports",25,"_key","asc","TOTAL_REACHED","IDP_REACHED",false,false,23,false);
        aggSearchList.put("PRD_TYPE_NAME",new AggregationSearchItem("Report Frequency","PRD_TYPE_NAME","PRD_TYPE_NAME", 1,"reports",50,"_key","asc","TOTAL_REACHED","IDP_REACHED",true,true,22,false));
        aggSearchList.put("RPT_TYPE_NAME",new AggregationSearchItem("Report Type","RPT_TYPE_NAME","RPT_TYPE_NAME", 1,"reports",50,"_key","asc","TOTAL_REACHED","IDP_REACHED",true,true,21,false));
        aggSearchList.put("STATUS",new AggregationSearchItem("Report Status","STATUS","STATUS", 1,"reports",10,"_key","asc","TOTAL_REACHED","IDP_REACHED",true,true,20,false));
        AggregationSearchItem sector=new AggregationSearchItem("Sector","SECTOR","SECTOR", 2,"reports.SECTOR_DETAILS",50,"_key","asc","BENE_TOT_REACHED","BENE_IDP_REACHED",true,true,25,false);
        aggSearchList.put("iso_code",new AggregationSearchItem("Country ISO","iso_code","iso_code", 0,null,250,"_key","asc","TOTAL_REACHED","IDP_REACHED",true,true,14,true));
        AggregationSearchItem asiDate=new AggregationSearchItem("Report Due by Month","RPT_DUE_DATE","RPT_DUE_DATE", 1,"reports",100,"_key","asc",null,null,false,false,24,false);
        asiDate.setIsDate(true);
        asiDate.setInterval("1M");
        asiDate.setOtherRestriction(
        "              {\"range\": {\n" + 
        "                \"reports.RPT_DUE_DATE\": {\n" + 
        "                  \"lte\": \"now+3M\",\n" + 
        "                  \"gte\":\"now-2y\"\n" + 
        "                }}\n" + 
        "              }");
        if(sc.isUserInRole("partner"))
        {
            fy.setDisclosed(false);
            asiDate.setDisclosed(false);
            sector.setDisclosed(false);
        }
        aggSearchList.put("RPT_FY",fy);
        aggSearchList.put("RPT_DUE_DATE",asiDate);
        aggSearchList.put("SECTOR",sector);
    
        
     /*   TermItem t1=new TermItem("Region","region_name","East Asia and Pacific","East Asia and Pacific");
        TermItem t5=new TermItem("Region","region_name","Middle East","Middle East");
        TermItem t2=new TermItem("Report Frequency","PRD_TYPE_NAME","Semi-Annual 1","Semi-Annual 1");
       TermItem t3=new TermItem("Report Year","RPT_FY","2018","2018");
        TermItem t4=new TermItem("Report Type","RPT_TYPE_NAME","Program","Program");
        
        TermItem t6=new TermItem("Country","cname","Syria","Syria");
        TermItem t7=new TermItem("Country","cname","Solomon Islands","Solomon Islands");
        
        TermItem t8=new TermItem("Report Due On","RPT_DUE_DATE","1522540800000","1522540800000");
        TermItem t9=new TermItem("Report Due On","RPT_DUE_DATE","1514764800000","1514764800000");
        termFilters.add(t1);
        termFilters.add(t5);
        termFilters.add(t2);
        termFilters.add(t3);
        termFilters.add(t4);
        termFilters.add(t6);
        termFilters.add(t7);
        termFilters.add(t8);
        termFilters.add(t9);
        
        List<TermItem> t=new ArrayList<TermItem>();
        t.add(t8);
        t.add(t9);
        tfMap.put(t8.getTerm(), t);
        
        t=new ArrayList<TermItem>();
        t.add(t1);
        t.add(t5);
        tfMap.put(t1.getTerm(), t);
        
        t=new ArrayList<TermItem>();
        t.add(t6);
        t.add(t7);
        tfMap.put(t6.getTerm(), t);
        
        t=new ArrayList<TermItem>();
        t.add(t2);

        tfMap.put(t2.getTerm(), t);
        
        t=new ArrayList<TermItem>();
        t.add(t3);

        tfMap.put(t3.getTerm(), t);
        
        t=new ArrayList<TermItem>();
        t.add(t4);
        tfMap.put(t4.getTerm(), t);*/
    }

    public void setStartFY(Integer startFY) {
        this.startFY = startFY;
    }

    public Integer getStartFY() {
        return startFY;
    }

    public void setEndFY(Integer endFY) {
        this.endFY = endFY;
    }

    public Integer getEndFY() {
        return endFY;
    }
    
    private void resetFrom(){
        from=0;
        nextList.clear();
        for(int i=0;i<5;i++)
          nextList.add(i);
    }
    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
        resetFrom();
        ritem=esrb.performARTRequest(this);
        
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setTermFilters(List<TermItem> termFilters) {
        this.termFilters = termFilters;
        resetFrom();
        ritem=esrb.performARTRequest(this);
        
    }

    public List<TermItem> getTermFilters() {
        return termFilters;
    }

    public void setStatus(String[] status) {
        this.status = status;
        resetFrom();
        ritem=esrb.performARTRequest(this);
        
    }

    public String[] getStatus() {
        return status;
    }

    public void setFavoritesOnly(Boolean favoritesOnly) {
        this.favoritesOnly = favoritesOnly;
        resetFrom();
        ritem=esrb.performARTRequest(this);
        
    }

    public Boolean getFavoritesOnly() {
        return favoritesOnly;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
        resetFrom();
        ritem=esrb.performARTRequest(this);
        
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSize(Integer size) {
        this.size = size;
        ritem=esrb.performARTRequest(this);
    }

    public Integer getSize() {
        return size;
    }
    public boolean addTermFilter(TermItem ti) {
        if(!this.getTermFilters().contains(ti)) {
            this.getTermFilters().add(ti);
            if(!tfMap.containsKey(ti.getTerm())) {
                List<TermItem> t=new ArrayList<TermItem>();
                t.add(ti);
                tfMap.put(ti.getTerm(), t);
            }
            else {
                List<TermItem> t=tfMap.get(ti.getTerm());
                t.add(ti);
            }
            resetFrom();
            ritem=esrb.performARTRequest(this);
            
            return true;
        }
        else removeTermFilter(ti);
     return false;
    }
    public boolean removeTermFilter(TermItem ti){
        if(this.getTermFilters().contains(ti)) {
            this.getTermFilters().remove(ti);
            if(tfMap.containsKey(ti.getTerm())) {
                List<TermItem> t=tfMap.get(ti.getTerm());
                t.remove(ti);
                if(t.size()==0)
                  tfMap.remove(ti.getTerm());
            }
            resetFrom();
            ritem=esrb.performARTRequest(this);
            
            return true;
        }
        return false;
    }
    public void removeAllTermFilters(){
        this.getTermFilters().clear();
        this.getTfMap().clear();
        resetFrom();
        ritem=esrb.performARTRequest(this);
        
    }

    public ResultItem getRitem() {
        return ritem;
    }
    public void performSearch() {
        resetFrom();
        ritem=esrb.performARTRequest(this);
    }
    
    public void refreshSearch() {
        ritem=esrb.performARTRequest(this);
    }
    public void setTfMap(Map<String, List<TermItem>> tfMap) {
        this.tfMap = tfMap;
    }

    public Map<String, List<TermItem>> getTfMap() {
        return tfMap;
    }

    public void setFrom(Integer from) {
        this.from = from;
        ritem=esrb.performARTRequest(this);
        int tmp=from/size;
        Double results=new Double(ritem.getTotal());
        int total = (int) Math.round(Math.ceil( results/ size));
        if(tmp+2>(total))
            tmp = total - 2;
         if(tmp<2)
             tmp=2;
        nextList.clear();

         for(int i=tmp-2;i<tmp+3;i++)
         {
          nextList.add(i);
         }
    }

    public Integer getFrom() {
        return from;
    }

    public void setNextList(List<Integer> nextList) {
        this.nextList = nextList;
    }

    public List<Integer> getNextList() {
        return nextList;
    }
    public void setValuesWithoutSearch(String query,Integer size, String sortOrder) {
        this.searchQuery=query;
        this.size=size;
        this.sortOrder=sortOrder;
    }

    public Map<String, AggregationSearchItem> getAggSearchList() {
        return aggSearchList;
    }

    public void setRestrictAwardee(Boolean restrictAwardee) {
        this.restrictAwardee = restrictAwardee;
    }

    public Boolean getRestrictAwardee() {
        return restrictAwardee;
    }

    public void setAwardeeCode(String awardeeCode) {
        this.awardeeCode = awardeeCode;
    }

    public String getAwardeeCode() {
        return awardeeCode;
    }

    public void setSortList(List<SortItem> sortList) {
        this.sortList = sortList;
    }

    public List<SortItem> getSortList() {
        return sortList;
    }

    public boolean setUserPeferenceDefault(UserDashboardPreference udp){
        this.searchQuery=udp.getSearchQuery();
        this.startFY=udp.getStartFY();
        this.endFY=udp.getEndFY();
        this.selectedSortList.clear();
        for(SortItem s:udp.getSelectedSortList()) {
            SortItem j=SerializationUtils.clone(s);
            this.getSelectedSortList().add(j);
        }
        this.getTermFilters().clear();
        this.getTfMap().clear();
        for(TermItem ti:udp.getTermFilters())
        {
        if(!this.getTermFilters().contains(ti)) {
            this.getTermFilters().add(ti);
            if(!tfMap.containsKey(ti.getTerm())) {
                List<TermItem> t=new ArrayList<TermItem>();
                t.add(ti);
                tfMap.put(ti.getTerm(), t);
            }
            else {
                List<TermItem> t=tfMap.get(ti.getTerm());
                t.add(ti);
            }
        }
        }
        resetFrom();
        ritem=esrb.performARTRequest(this);
        return true;
    }
    public static void main(String arg[]) {
        SearchItem si=new SearchItem();
        ElasticSearchRestBean  esrb=new ElasticSearchRestBean();
        esrb.performARTRequest(si);
}

    public void setSelectedSortList(List<SortItem> selectedSortList) {
        this.selectedSortList = selectedSortList;
    }

    public List<SortItem> getSelectedSortList() {
        return selectedSortList;
    }

    public void exportARTDataToExcel(FacesContext facesContext, OutputStream outputStream){
        Map rMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String level = (String) rMap.get("exportLevel");
        Integer from=this.getFrom();
        this.showAggregations=false;
        if("0".equals(level))
            this.showInnerHits=false;
        if(level==null)
                level="1";
        try {
            HSSFWorkbook workbook;
            HSSFSheet sheet;
            HSSFCellStyle headerStyle;
            HSSFCellStyle bodyStyle;
            
            HSSFCellStyle numberStyle;
            HSSFCellStyle dateStyle;
            HSSFCellStyle amountStyle;
            
            workbook = new HSSFWorkbook();
            sheet = workbook.createSheet("Data");
            HSSFCellStyle style= workbook.createCellStyle();
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            HSSFFont boldFont = workbook.createFont();
            HSSFDataFormat format = workbook.createDataFormat();
            SimpleDateFormat df = new SimpleDateFormat("MMM-dd-yyyy");
            NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
            
            
            boldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

            headerStyle = workbook.createCellStyle();
            bodyStyle = workbook.createCellStyle();

            headerStyle.setFont(boldFont);
            headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
            headerStyle.setBorderRight(CellStyle.BORDER_THIN);
            headerStyle.setBorderTop(CellStyle.BORDER_THIN);
            headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
            headerStyle.setWrapText(true);
            headerStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            headerStyle.setFont(boldFont);
            headerStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);
            
            bodyStyle.setWrapText(false);
            bodyStyle.setBorderLeft(CellStyle.BORDER_THIN);
            bodyStyle.setBorderRight(CellStyle.BORDER_THIN);
            bodyStyle.setBorderTop(CellStyle.BORDER_THIN);
            bodyStyle.setBorderBottom(CellStyle.BORDER_THIN);
            bodyStyle.setAlignment(CellStyle.VERTICAL_TOP);
            bodyStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);
            
            
            
            numberStyle = workbook.createCellStyle();
            numberStyle.setWrapText(true);
            numberStyle.setBorderLeft(CellStyle.BORDER_THIN);
            numberStyle.setBorderRight(CellStyle.BORDER_THIN);
            numberStyle.setBorderTop(CellStyle.BORDER_THIN);
            numberStyle.setBorderBottom(CellStyle.BORDER_THIN);
            numberStyle.setDataFormat((short)3);
            numberStyle.setAlignment(CellStyle.ALIGN_RIGHT);
            
            dateStyle = workbook.createCellStyle();
            dateStyle.setWrapText(false);
            dateStyle.setBorderLeft(CellStyle.BORDER_THIN);
            dateStyle.setBorderRight(CellStyle.BORDER_THIN);
            dateStyle.setBorderTop(CellStyle.BORDER_THIN);
            dateStyle.setBorderBottom(CellStyle.BORDER_THIN);
            dateStyle.setDataFormat((short)15);
            
            
            amountStyle = workbook.createCellStyle();
            amountStyle.setWrapText(false);
            amountStyle.setBorderLeft(CellStyle.BORDER_THIN);
            amountStyle.setBorderRight(CellStyle.BORDER_THIN);
            amountStyle.setBorderTop(CellStyle.BORDER_THIN);
            amountStyle.setBorderBottom(CellStyle.BORDER_THIN);
            amountStyle.setDataFormat((short)8);
            amountStyle.setAlignment(CellStyle.ALIGN_RIGHT);
            amountStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);

            HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
            response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
            ServletOutputStream out;
            int rowPos=1;
            HSSFRow headerRow=sheet.createRow(0);
            int tcolPos=0;
            HSSFCellUtil.createCell(headerRow, tcolPos++, "Project",headerStyle);
            if(sc.isUserInRole("admin")||sc.isUserInRole("ofda")||sc.isUserInRole("APP_CONNECT")) {
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Awardee Type",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Awardee",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Division",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Team",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "AOR",headerStyle);
            }
            HSSFCellUtil.createCell(headerRow, tcolPos++, "Award Nbr",headerStyle);
            HSSFCellUtil.createCell(headerRow, tcolPos++, "Amount",headerStyle);
            HSSFCellUtil.createCell(headerRow, tcolPos++, "Start Date",headerStyle);
            HSSFCellUtil.createCell(headerRow, tcolPos++, "End Date",headerStyle);
            HSSFCellUtil.createCell(headerRow, tcolPos++, "Award Duration (Months)",headerStyle);
            HSSFCellUtil.createCell(headerRow, tcolPos++, "Status",headerStyle);
            HSSFCellUtil.createCell(headerRow, tcolPos++, "Region",headerStyle);
            HSSFCellUtil.createCell(headerRow, tcolPos++, "Country",headerStyle);
            if(!"0".equals(level))
            {
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Report Nbr",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Report Type",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Fiscal Year",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Frequency",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Due Date",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "# of Days Due/Late",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Submitted Date",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Submit Status",headerStyle);
            }
            if("3".equals(level))
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Sector",headerStyle);
            if("2".equals(level)|| "3".equals(level))
            {
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Basline Total",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Baseline IDP",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Cumulative Targeted Total",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Cumulative Targeted IDP",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Reporting Period Reached Total",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Reporting Period Reached IDP",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Cumulative Reached Total",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Cumulative Reached Total %",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Cumulative Reached IDP",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "Cumulative Reached IDP %",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "System Calculated Cumulative Reached Total",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "System Calculated Cumulative Reached Total %",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "System Calculated Cumulative Reached IDP",headerStyle);
                HSSFCellUtil.createCell(headerRow, tcolPos++, "System Calculated Cumulative Reached IDP %",headerStyle);
            }
            HSSFCell c=null;
            int maxValue=ritem.getTotal().intValue();;
            int size=100;
            if("0".equals(level))
                size=maxValue;
            
            for(int i=0;i<maxValue;i=i+size)
            {
                this.from=i;
                this.size=size;
                ResultItem excelResultItem=esrb.performARTRequest(this);
            try {
                for(Award a:excelResultItem.getAwardList())
                {
                    if(!"0".equals(level))
                    {
                        a.getCUM_IDP_REACHED();
                        if(a.getReports()!=null) {
                            for(Report r:a.getReports()) {
                                if(!"1".equals(level) && r.getRPT_TYPE_CODE()!=1 && r.getRPT_TYPE_CODE()!=4)
                                    continue;
                                int colPos=0;
                                HSSFRow row = sheet.createRow(rowPos++);
                                HSSFCellUtil.createCell(row, colPos++, a.getPname(),bodyStyle);
                                
                                if(sc.isUserInRole("admin")||sc.isUserInRole("ofda")||sc.isUserInRole("APP_CONNECT")) {
                                    HSSFCellUtil.createCell(row, colPos++, a.getAwardee_type_code(),bodyStyle);
                                    HSSFCellUtil.createCell(row, colPos++, a.getAwardee(),bodyStyle);
                                    HSSFCellUtil.createCell(row, colPos++, a.getOfda_division_code(),bodyStyle);
                                    HSSFCellUtil.createCell(row, colPos++, a.getOfda_team_code(),bodyStyle);
                                    HSSFCellUtil.createCell(row, colPos++, a.getAor_name(),bodyStyle);
                                }
                                HSSFCellUtil.createCell(row, colPos++, a.getAward_nbr(),bodyStyle);
                                c=HSSFCellUtil.createCell(row, colPos++, null,amountStyle);
                                if((a.getAward_amt()!=null))
                                    c.setCellValue(a.getAward_amt());
                                HSSFCellUtil.createCell(row, colPos++, (a.getAward_start_date() !=null?df.format(a.getAward_start_date()):""),bodyStyle);
                                HSSFCellUtil.createCell(row, colPos++, (a.getAward_end_date()!=null ?df.format(a.getAward_end_date()):""),bodyStyle);
                                c=HSSFCellUtil.createCell(row, colPos++, null,bodyStyle);
                                if(a.getAward_duration()!=null)
                                    c.setCellValue(a.getAward_duration());  
                                HSSFCellUtil.createCell(row, colPos++, a.getAward_status(),bodyStyle);
    
                                HSSFCellUtil.createCell(row, colPos++, a.getRegion_name(),bodyStyle);
                                HSSFCellUtil.createCell(row, colPos++, a.getCname(),bodyStyle);
                                c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                if(r.getRPT_NBR()!=null)
                                    c.setCellValue(r.getRPT_NBR());
                                HSSFCellUtil.createCell(row, colPos++, ""+r.getRPT_TYPE_NAME(),bodyStyle);
                                c=HSSFCellUtil.createCell(row, colPos++, ""+r.getRPT_FY(),bodyStyle);
                                if(r.getRPT_FY()!=null)
                                    c.setCellValue(r.getRPT_FY());
                                HSSFCellUtil.createCell(row, colPos++, ""+r.getPRD_TYPE_NAME(),bodyStyle);
                                HSSFCellUtil.createCell(row, colPos++, ""+(r.getRPT_DUE_DATE()!=null?df.format(r.getRPT_DUE_DATE()):""),bodyStyle);
                                c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                if(r.getDUE_IN_DAYS()!=null)
                                    c.setCellValue(r.getDUE_IN_DAYS());
                                HSSFCellUtil.createCell(row, colPos++, ""+(r.getSUBMIT_DATE() != null?df.format(r.getSUBMIT_DATE()):""),bodyStyle);
                                HSSFCellUtil.createCell(row, colPos++, ""+r.getSTATUS(),bodyStyle);
                                if("3".equals(level))
                                    HSSFCellUtil.createCell(row, colPos++, "",bodyStyle);
                                if("2".equals(level)|| "3".equals(level))
                                {
                                    if(r.getSUBMIT_DATE()!=null && (r.getRPT_TYPE_CODE()==1 || r.getRPT_TYPE_CODE()==4))
                                    {
                                    c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                    if(r.getBASELINE_VALUE()!=null)
                                        c.setCellValue(r.getBASELINE_VALUE());
                                    c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                    if(r.getBASELINE_IDP_VALUE()!=null)
                                        c.setCellValue(r.getBASELINE_IDP_VALUE());
                                    c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                    if(r.getCUM_TOTAL_TARGETED()!=null)
                                        c.setCellValue(r.getCUM_TOTAL_TARGETED());
                                    c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                    if(r.getCUM_IDP_TARGETED()!=null)
                                        c.setCellValue(r.getCUM_IDP_TARGETED());
                                    if(r.getRPT_TYPE_CODE()==1)
                                    {
        
                                        c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                        if(r.getTOTAL_REACHED()!=null)
                                            c.setCellValue(r.getTOTAL_REACHED());
                                        c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                        if(r.getIDP_REACHED()!=null)
                                            c.setCellValue(r.getIDP_REACHED());
                                        c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                        if(r.getCUM_TOTAL_REACHED()!=null)
                                            c.setCellValue(r.getCUM_TOTAL_REACHED());
                                        c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                        if(r.getCUM_TOTAL_REACHED_PCT()!=null)
                                            c.setCellValue(r.getCUM_TOTAL_REACHED_PCT());
                                        c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                        if(r.getCUM_IDP_REACHED()!=null)
                                            c.setCellValue(r.getCUM_IDP_REACHED());
                                        c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                        if(r.getCUM_IDP_REACHED_PCT()!=null)
                                            c.setCellValue(r.getCUM_IDP_REACHED_PCT());
                                        c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                        if(r.getCAL_CUM_TOTAL_REACHED()!=null)
                                            c.setCellValue(r.getCAL_CUM_TOTAL_REACHED());
                                        c=HSSFCellUtil.createCell(row, colPos++,null,numberStyle);
                                        if(r.getCAL_CUM_TOTAL_REACHED_PCT()!=null)
                                            c.setCellValue(r.getCAL_CUM_TOTAL_REACHED_PCT());
                                        c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                        if(r.getCAL_CUM_IDP_REACHED()!=null)
                                            c.setCellValue(r.getCAL_CUM_IDP_REACHED());
                                        c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                        if(r.getCAL_CUM_IDP_REACHED_PCT()!=null)
                                            c.setCellValue(r.getCAL_CUM_IDP_REACHED_PCT());
                                    }
                                    if("3".equals(level))
                                    for(Sector s:r.getSECTOR_DETAILS()) {
                                        colPos=0;
                                        row = sheet.createRow(rowPos++);
                                        HSSFCellUtil.createCell(row, colPos++, a.getPname(),bodyStyle);
                                        
                                        if(sc.isUserInRole("admin")||sc.isUserInRole("ofda")||sc.isUserInRole("APP_CONNECT")) {
                                            HSSFCellUtil.createCell(row, colPos++, a.getAwardee_type_code(),bodyStyle);
                                            HSSFCellUtil.createCell(row, colPos++, a.getAwardee(),bodyStyle);
                                            HSSFCellUtil.createCell(row, colPos++, a.getOfda_division_code(),bodyStyle);
                                            HSSFCellUtil.createCell(row, colPos++, a.getOfda_team_code(),bodyStyle);
                                            HSSFCellUtil.createCell(row, colPos++, a.getAor_name(),bodyStyle);
                                        }
                                        HSSFCellUtil.createCell(row, colPos++, a.getAward_nbr(),bodyStyle);
                                        c=HSSFCellUtil.createCell(row, colPos++, null,amountStyle);
                                        if((a.getAward_amt()!=null))
                                            c.setCellValue(a.getAward_amt());
                                        HSSFCellUtil.createCell(row, colPos++, (a.getAward_start_date() !=null?df.format(a.getAward_start_date()):""),bodyStyle);
                                        HSSFCellUtil.createCell(row, colPos++, (a.getAward_end_date()!=null ?df.format(a.getAward_end_date()):""),bodyStyle);
                                        c=HSSFCellUtil.createCell(row, colPos++, null,bodyStyle);
                                        if(a.getAward_duration()!=null)
                                            c.setCellValue(a.getAward_duration()); 
                                        HSSFCellUtil.createCell(row, colPos++, a.getAward_status(),bodyStyle);
    
                                        HSSFCellUtil.createCell(row, colPos++, a.getRegion_name(),bodyStyle);
                                        HSSFCellUtil.createCell(row, colPos++, a.getCname(),bodyStyle);
                                        c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                        if(r.getRPT_NBR()!=null)
                                            c.setCellValue(r.getRPT_NBR());
                                        HSSFCellUtil.createCell(row, colPos++, ""+r.getRPT_TYPE_NAME(),bodyStyle);
                                        c=HSSFCellUtil.createCell(row, colPos++, ""+r.getRPT_FY(),bodyStyle);
                                        if(r.getRPT_FY()!=null)
                                            c.setCellValue(r.getRPT_FY());
                                        HSSFCellUtil.createCell(row, colPos++, ""+r.getPRD_TYPE_NAME(),bodyStyle);
                                        HSSFCellUtil.createCell(row, colPos++, ""+(r.getRPT_DUE_DATE()!=null?df.format(r.getRPT_DUE_DATE()):""),bodyStyle);
                                        c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                        if(r.getDUE_IN_DAYS()!=null)
                                            c.setCellValue(r.getDUE_IN_DAYS());
                                        HSSFCellUtil.createCell(row, colPos++, ""+(r.getSUBMIT_DATE() != null?df.format(r.getSUBMIT_DATE()):""),bodyStyle);
                                        HSSFCellUtil.createCell(row, colPos++, ""+r.getSTATUS(),bodyStyle);
                                        HSSFCellUtil.createCell(row, colPos++, ""+s.getSECTOR(),bodyStyle);
                                        if(r.getSUBMIT_DATE()!=null && (r.getRPT_TYPE_CODE()==1 || r.getRPT_TYPE_CODE()==4))
                                        {
                                            c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                            if(s.getBASELINE_VALUE()!=null)
                                                c.setCellValue(s.getBASELINE_VALUE());
                                            c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                            if(s.getBASELINE_IDP_VALUE()!=null)
                                                c.setCellValue(s.getBASELINE_IDP_VALUE());
                                            c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                            if(s.getCUM_TOTAL_TARGETED()!=null)
                                                c.setCellValue(s.getCUM_TOTAL_TARGETED());
                                            c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                            if(s.getCUM_IDP_TARGETED()!=null)
                                                c.setCellValue(s.getCUM_IDP_TARGETED());
                                            if(r.getRPT_TYPE_CODE()==1)
                                        if(r.getRPT_TYPE_CODE()==1)
                                        {
        
                                            c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                            if(s.getTOTAL_REACHED()!=null)
                                                c.setCellValue(s.getTOTAL_REACHED());
                                            c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                            if(s.getIDP_REACHED()!=null)
                                                c.setCellValue(s.getIDP_REACHED());
                                            c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                            if(s.getCUM_TOTAL_REACHED()!=null)
                                                c.setCellValue(s.getCUM_TOTAL_REACHED());
                                            c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                            if(s.getCUM_TOTAL_REACHED_PCT()!=null)
                                                c.setCellValue(s.getCUM_TOTAL_REACHED_PCT());
                                            c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                            if(s.getCUM_IDP_REACHED()!=null)
                                                c.setCellValue(s.getCUM_IDP_REACHED());
                                            c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                            if(s.getCUM_IDP_REACHED_PCT()!=null)
                                                c.setCellValue(s.getCUM_IDP_REACHED_PCT());
                                            c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                            if(s.getCAL_CUM_TOTAL_REACHED()!=null)
                                                c.setCellValue(s.getCAL_CUM_TOTAL_REACHED());
                                            c=HSSFCellUtil.createCell(row, colPos++,null,numberStyle);
                                            if(s.getCAL_CUM_TOTAL_REACHED_PCT()!=null)
                                                c.setCellValue(s.getCAL_CUM_TOTAL_REACHED_PCT());
                                            c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                            if(s.getCAL_CUM_IDP_REACHED()!=null)
                                                c.setCellValue(s.getCAL_CUM_IDP_REACHED());
                                            c=HSSFCellUtil.createCell(row, colPos++, null,numberStyle);
                                            if(s.getCAL_CUM_IDP_REACHED_PCT()!=null)
                                                c.setCellValue(s.getCAL_CUM_IDP_REACHED_PCT());
                                        }
                                        }
                                    }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        int colPos=0;
                        HSSFRow row = sheet.createRow(rowPos++);
                        HSSFCellUtil.createCell(row, colPos++, a.getPname(),bodyStyle);
                        
                        if(sc.isUserInRole("admin")||sc.isUserInRole("ofda")||sc.isUserInRole("APP_CONNECT")) {
                            HSSFCellUtil.createCell(row, colPos++, a.getAwardee_type_code(),bodyStyle);
                            HSSFCellUtil.createCell(row, colPos++, a.getAwardee(),bodyStyle);
                            HSSFCellUtil.createCell(row, colPos++, a.getOfda_division_code(),bodyStyle);
                            HSSFCellUtil.createCell(row, colPos++, a.getOfda_team_code(),bodyStyle);
                            HSSFCellUtil.createCell(row, colPos++, a.getAor_name(),bodyStyle);
                        }
                        HSSFCellUtil.createCell(row, colPos++, a.getAward_nbr(),bodyStyle);
                        c=HSSFCellUtil.createCell(row, colPos++, null,amountStyle);
                        if((a.getAward_amt()!=null))
                            c.setCellValue(a.getAward_amt());
                        HSSFCellUtil.createCell(row, colPos++, (a.getAward_start_date() !=null?df.format(a.getAward_start_date()):""),bodyStyle);
                        HSSFCellUtil.createCell(row, colPos++, (a.getAward_end_date()!=null ?df.format(a.getAward_end_date()):""),bodyStyle);
                        c=HSSFCellUtil.createCell(row, colPos++, null,bodyStyle);
                        if(a.getAward_duration()!=null)
                            c.setCellValue(a.getAward_duration()); 
                        HSSFCellUtil.createCell(row, colPos++, a.getAward_status(),bodyStyle);
                        
                        HSSFCellUtil.createCell(row, colPos++, a.getRegion_name(),bodyStyle);
                        HSSFCellUtil.createCell(row, colPos++, a.getCname(),bodyStyle);
                    }
                }
                int autosize[];
                if(sc.isUserInRole("admin")||sc.isUserInRole("ofda")||sc.isUserInRole("APP_CONNECT"))
                {
                    autosize=new int[]{7,8,9,10,12,18,19,21,22};
                    for(int j=21;j<36;j++)
                        sheet.setColumnWidth(j, 2820);
                }
                else
                {
                    autosize=new int[]{1,2,3,4,5,7,13,14,16,17};
                    for(int j=17;j<31;j++)
                        sheet.setColumnWidth(j, 2820);
                }
                for(int j:autosize)
                {
                    //zero based so i-1
                    sheet.autoSizeColumn(j-1);
                }
                sheet.getRow(0).setHeightInPoints(64);
                } 
            catch (Exception e) {
                    e.printStackTrace();
                }
            }
            String filename="ART_Dashboard_";
            if("0".equals(level))
                filename="ART_Dashboard_Awards";
            else if("2".equals(level))
                filename+="Award_Level_Beneficiaries";
            else if("3".equals(level))
                filename+="Award_And_Sector_Level_Beneficiaries";
            else
                filename+="Reports";
            response.setHeader("Content-disposition", "attachment;filename=\""+filename+".xls\"");
            out = response.getOutputStream();
            HSSFRow row = sheet.createRow(rowPos + 5);
            HSSFCell cell = row.createCell(0);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Exported on");
            cell = row.createCell(1);
            cell.setCellStyle(bodyStyle);
            Date d = new Date();
            cell.setCellValue(df.format(d));
            
            HSSFSheet sheet2 = workbook.createSheet("Filters");
            int sheet2Row=0;
            row = sheet2.createRow(sheet2Row++);
            cell = row.createCell(0);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Filter Name");
            cell = row.createCell(1);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Filter Value");
            for(TermItem ti: getTermFilters()) {
                row = sheet2.createRow(sheet2Row++);
                cell = row.createCell(0);
                cell.setCellStyle(headerStyle);
                cell.setCellValue(ti.getTermLabel());
                cell = row.createCell(1);
                cell.setCellStyle(bodyStyle);
                cell.setCellValue(ti.getValueLabel());
            }
                row = sheet2.createRow(sheet2Row+2);
                cell = row.createCell(0);
                cell.setCellStyle(headerStyle);
                cell.setCellValue("Search Keyword");
                cell = row.createCell(1);
                cell.setCellStyle(bodyStyle);
                cell.setCellValue(this.getSearchQuery());
                sheet2.autoSizeColumn(0);
                sheet2.autoSizeColumn(1);

            workbook.write(out);
            out.flush();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        finally{
            this.from=from;
            this.size=10;
            this.showInnerHits=true;
            this.showAggregations=true;
        }
    }

    public void setUserPreference(UserPreference userPreference) {
        this.userPreference = userPreference;
        this.userPreference.setUpdatedDate(new Date());
        esrb.updateUserDashboardPreference(userPreference);
        hasUserPreference=true;
    }

    public UserPreference getUserPreference() {
        if(userPreference==null && hasUserPreference) {
            userPreference=esrb.getUserDashboardPreference();
            if(userPreference==null)
                hasUserPreference=false;
        }
        return userPreference;
    }

    public void setShowInnerHits(boolean showInnerHits) {
        this.showInnerHits = showInnerHits;
    }

    public boolean isShowInnerHits() {
        return showInnerHits;
    }

    public void setShowAggregations(boolean showAggregations) {
        this.showAggregations = showAggregations;
    }

    public boolean isShowAggregations() {
        return showAggregations;
    }
}
