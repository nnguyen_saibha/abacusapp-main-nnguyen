package gov.ofda.abacus.portal.controller;

public class RegionCountry {
    public RegionCountry() {
        super();
    }
    
    private String code; 
    private String regionCode;
    private String regionName;
    private String countryCode;
    private String countryName;
    
    public RegionCountry(String code,String regionCode, String regionName, String countryCode,String countryName){
        
        this.code = code;
        
        this.regionCode = regionCode;
        this.regionName = regionName; 
        
        this.countryCode = countryCode;
        this.countryName = countryName;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryName() {
        return countryName;
    }

}
