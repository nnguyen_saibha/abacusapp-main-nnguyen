package gov.ofda.abacus.portal.controller;

import java.io.IOException;

import java.util.HashSet;
import java.util.ResourceBundle;

import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ComponentSystemEvent;

import javax.security.auth.Subject;

import javax.security.auth.login.FailedLoginException;

import javax.security.auth.login.LoginException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;

import org.apache.myfaces.trinidad.event.PollEvent;

import weblogic.security.URLCallbackHandler;
import weblogic.security.services.Authentication;

import weblogic.servlet.security.ServletAuthentication;


import oracle.adf.model.BindingContainer;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCControlBinding;

import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;

import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichPoll;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

/**
 * LoginHandler.java
 * Purpose: To handle login for the application.
 *
 * @author Kartheek Atluri
 * @version 1.0 09/09/2011
 */
public class LoginHandler {
    private String _username;
    private String _password;
    private String userType;
    private HashSet pcFeaturesOff;
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.portal.controller.LoginHandler.class);
    private boolean popupLaunched = false;

    public void setPopupLaunched(boolean popupLaunched) {
        this.popupLaunched = popupLaunched;
    }

    public boolean isPopupLaunched() {
        return popupLaunched;
    }
    private RichPopup notePopup;
    
    /**
     * Class Constructor
     */
    public LoginHandler() {
    }

    /**
     * @param _username setter method for _username
     */
    public void setUsername(String _username) {
        this._username = _username;
    }

    /**
     * @return getter method for _username
     */
    public String getUsername() {
        return _username;
    }

    /**
     * @param _password setter method for _password
     */
    public void setPassword(String _password) {
        this._password = _password;
    }

    /**
     * @return getter method for _password
     */
    public String getPassword() {
        return _password;
    }

    /**
     * Will authenticate the user and redirect to default page upon successful redirection.
     * Will display a message if authentication is not successful or user is Inactive.
     * @return will return null in all cases.
     */
    public String performLogin() {
        byte[] pw = _password.getBytes();
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
        try {
            Subject mySubject = Authentication.login(new URLCallbackHandler(_username, pw));
            ServletAuthentication.runAs(mySubject, request);
            String loginUrl = "/adfAuthentication?success_url=/faces/dashboard"; // + ctx.getViewRoot().getViewId();
            HttpServletResponse response = (HttpServletResponse)ctx.getExternalContext().getResponse();
            sendForward(request, response, loginUrl, true);
        } catch (FailedLoginException fle) {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Inactive or Incorrect Username or Password",
                                 "An Inactive or Incorrect Username or Password was specified");
            FacesContext.getCurrentInstance().addMessage("pt_it1", msg);
            logger.log(logger.ERROR, "Inactive or Incorrect Username or Password " + this.getUsername());
        } catch (LoginException le) {
            reportUnexpectedLoginError("loginException", le);
            logger.log(logger.ERROR, le.toString());
        }
        _password = null;
        return null;
    }

    /**
     * To forward and complete the response.
     *
     * @param request
     * @param response
     * @param loginUrl
     * @param islogin If True will check if the current user is active or not in appx_user database table .
     */
    private void sendForward(HttpServletRequest request, HttpServletResponse response, String loginUrl,
                             boolean islogin) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        RequestDispatcher dispatcher = request.getRequestDispatcher(loginUrl);
        try {

            if (islogin) {
                DCBindingContainer bindings =
                    (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
                DCControlBinding userCtrl = bindings.findCtrlBinding("AppxUserView1");
                Row uRow = userCtrl.getCurrentRow();
                if (uRow == null || !uRow.getAttribute("Active").toString().equals(("Y"))) {
                    logger.log(logger.ERROR, "User Inactive" + this.getUsername());
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Inactive or Incorrect Username or Password",
                                         "An Inactive or Incorrect Username or Password was specified");
                    FacesContext.getCurrentInstance().addMessage("pt_it1", msg);
                    dispatcher =
                            request.getRequestDispatcher("/adfAuthentication?logout=true");

                } else {
                    userType = (String)uRow.getAttribute("UserType");
                    logger.log(logger.NOTIFICATION, this.getUsername() + " logged in");
                }

            }
            dispatcher.forward(request, response);

        } catch (ServletException se) {
            logger.log(logger.ERROR,"The user is unable to login due to the exception" ,se);
            reportUnexpectedLoginError("ServletException", se);
        } catch (IOException ie) {
            logger.log(logger.ERROR,"The user is unable to login due to the exception" ,ie);
            reportUnexpectedLoginError("IOException", ie);
        }
        ctx.responseComplete();
    }

    /**
     *To report Unexpected Login Errors other than invalid username and password.
     *
     * @param errType
     * @param e
     */
    private void reportUnexpectedLoginError(String errType, Exception e) {
        FacesMessage msg =
            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected error During Login", "Unexpected Error during Login (" +
                             errType + "), please consult logs for detail");
        FacesContext.getCurrentInstance().addMessage("pt_it1", msg);
        e.printStackTrace();
    }

    /**
     * To log the user out of the application and redirect to searchAward.
     * @return
     */
    public String performLogout() {
        boolean oamConfigured = false;
        String oamConfiguredStr = null;
        StringBuffer sb = new StringBuffer();
        FacesContext ctx = FacesContext.getCurrentInstance();
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("gov.ofda.abacus.portal.model.resources.settings");
            if (bundle != null) {
                oamConfiguredStr = bundle.getString("oamConfigured");
                if (oamConfiguredStr != null) {
                    oamConfigured = Boolean.valueOf(oamConfiguredStr);
                }
            }
        } catch (Exception e) {
            logger.log(logger.ERROR, "In Exception " + e.getMessage());
        }
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse)ctx.getExternalContext().getResponse();
        if (oamConfigured) {
            sb.append("/adfAuthentication?logout=true");    
        } else {
            sb.append("/adfAuthentication?logout=true");
        }
        logger.log(logger.NOTIFICATION, "logout URL: " + sb.toString());
        logger.log(logger.NOTIFICATION, this.getUsername() + " logged out");
        sendForward(request, response, sb.toString(), false);        
        return null;
    }

    public String getUserType() {
        if (userType == null) {
            SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
            if(sc.isUserInRole("admin"))
                userType= "A";
            else if(sc.isUserInRole("ofda")|| sc.isUserInRole("APP_CONNECT"))
                userType="O";
            else if(sc.isUserInRole("partner"))
                userType="P";
          //  userType="N";
        }
        return userType;
    }

    public HashSet getPcFeaturesOff() {
        HashSet foff=new HashSet();
        foff.add("statusBar");
            if(!"A".equalsIgnoreCase(this.getUserType())) 
            {
                foff.add("detachToolbarItem");
                foff.add("viewMenu");
                //foff.add("detach");
                foff.add("columnsMenuItem");
            }
        return foff;
    }
   public Boolean getIsAdmin() {
       if("A".equalsIgnoreCase(this.getUserType())) 
           return true;
       return false;
   }
   
    public void setNotePopup(RichPopup notePopup) {
        this.notePopup = notePopup;
    }

    public RichPopup getNotePopup() {
        return notePopup;
    }

    public void preRender(ComponentSystemEvent componentSystemEvent) {
        if (!popupLaunched) {
 
        RichPopup popup = this.getNotePopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
       // ph.add(RichPopup.PopupHints.HintTypes.HINT_ALIGN_ID, "pt1:pt_db1::tl");
        popup.show(ph);
 
        }
        popupLaunched = true;
    }

    public void pollLsnr(PollEvent pollEvent) {
        RichPopup popup = this.getNotePopup();
        popup.hide();
        ((RichPoll)pollEvent.getComponent()).setInterval(0);
        
    }
}
