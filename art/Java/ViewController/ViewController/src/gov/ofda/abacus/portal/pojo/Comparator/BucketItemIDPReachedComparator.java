package gov.ofda.abacus.portal.pojo.Comparator;

import gov.ofda.abacus.portal.controller.elasticsearch.BucketItem;

import java.util.Comparator;

public class BucketItemIDPReachedComparator implements Comparator<BucketItem>{
    public BucketItemIDPReachedComparator() {
        super();
    }

    @Override
    public int compare(BucketItem bucketItem, BucketItem bucketItem2) {
        return (int) (bucketItem2.getIdp_reached() - bucketItem.getIdp_reached());
    }
}
