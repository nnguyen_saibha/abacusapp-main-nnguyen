package gov.ofda.abacus.portal.controller;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.model.binding.DCControlBinding;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
/**
 * UserDetails.java
 * Purpose: Used by template to show user details
 *
 * @author Kartheek Atluri
 */
public class UserDetails {
 private String awardee;
 private String fname;
 private String lname;
 private String usertype;
 private String awardee_code;
 private String active;
  public UserDetails() {
    
  }

    /**
     * Return the Awardee Name by calling getAwardeeName in PortalServiceImpl 
     * @return Awardee Name
     */
    public String getAwardee() {
        DCBindingContainer bindings=(DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding adocs_create=bindings.getOperationBinding("getAwardeeName");
        Object aname=adocs_create.execute();
        if(aname!=null)
        {
            awardee=aname.toString();
        }
        return awardee;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    /**
     * Return the UserType by calling getUsertype in PortalServiceImpl 
     * @return userType
     */
    public String getUsertype() {
        if(usertype==null)
        {
        DCBindingContainer bindings=(DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding adocs_create=bindings.getOperationBinding("getUsertype");
        Object aname=adocs_create.execute();
        usertype=aname.toString();
        }
        return usertype;
    }

    public String getAwardee_code() {
        return awardee_code;
    }

    public String getActive() {
        return active;
    }
}
