package gov.ofda.abacus.portal.controller;

import gov.ofda.abacus.portal.controller.elasticsearch.ElasticSearchRestBean;
import gov.ofda.abacus.portal.model.domain.Sector;
import gov.ofda.abacus.portal.model.domain.common.ReportValidSections;

import gov.ofda.abacus.portal.pojo.Award;

import java.io.IOException;

import java.sql.SQLException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.*;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.faces.model.SelectItem;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCControlBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputNumberSpinbox;
import oracle.adf.view.rich.component.rich.input.RichSelectManyCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelSplitter;
import oracle.adf.view.rich.component.rich.layout.RichToolbar;
import oracle.adf.view.rich.component.rich.nav.RichCommandToolbarButton;
import oracle.adf.view.rich.component.rich.output.RichOutputFormatted;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.adf.view.rich.render.ClientEvent;

import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;

import oracle.jbo.RowSetIterator;

import oracle.jbo.domain.DBSequence;

import org.apache.myfaces.trinidad.event.DisclosureEvent;
import org.apache.myfaces.trinidad.event.ReturnEvent;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


/**
 * ReportsBean.java
 * Purpose: Used by Used by manageReportFlow to add, edit or view Reports
 *
 * @author Kartheek Atluri
 * @version 1.0 09/09/2011
 */
public class ReportsBean extends UIControl {
    private String parseq;
    private RichTable parSeqTable;
    private RichTable parSector;
    private RichPanelFormLayout parForm;
    private RichTable awardTable;
    private RichPopup indicatorPopup;
    private RichTable parIndicatorTable;
    private RichSelectOneRadio isComplete;
    private RichSelectOneRadio submit;
    private RichToolbar mainToolbar;
    private String newReportType;
    private RichToolbar parToolbar;
    private String rowColor;
    private HtmlInputTextarea newIndicatorJustification;
    private String newReportPeriod;
    private RichInputDate newReportDueDate;
    private RichInputDate newReportStartDate;
    private RichInputDate newReportEndDate;
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.portal.controller.ReportsBean.class);
    private RichOutputText requiredValues;
    private RichPanelSplitter mainSplitter;
    private RichSelectManyCheckbox statusCheckBox;
    private String[] checkedValues = { "In-Progress", "Expired"};
    private String awardViewWhereClause;
    private RichPanelSplitter searchMyAwardsSplitter;
    private RichPopup submitPopup;
    private RichPopup missingValuesPopup;
    private RichTable parROTable;
    private Boolean enableComplete = Boolean.FALSE;
    private RichInputNumberSpinbox newReportFY;
    private Integer getFY;
    private RichTable docTabTable;
    private RichPopup newReportPopup;
    private RichTable addIndicatorTable;
    private RichPopup frequencyChgPopup;
    private Boolean latestSubmittedReport;
    private RichPopup documentsPopup;
    private RichToolbar reportsTabToolbar;
    private RichToolbar indicatorToolbar;
    private RichCommandToolbarButton newReportRedirect;

    private int selectedPar;
    private int rptTypeCode;
    private RichTreeTable indicatorTree;
    private RichPanelFormLayout parIndicatorForm;
    private RichPanelFormLayout reportsForm;
    private RichDialog reportInstructionInformationDialog;
    private RichPopup reportCreationInformationPopup;
    private RichPanelGroupLayout validatePagePanelGroup;
    private RichSelectManyCheckbox filter4wCheckBox;
    private String whereClauseAwardStatus;
    private String whereClause4W;
    private RichTable report4wTable;
    private ReportValidSections reportValidSections;
    private List<Sector> sectorList=new ArrayList<Sector>();
    private List<Sector> awardSectorList=new ArrayList<Sector>();
    private RichPopup keywordIndicatorCommentsPopup;
    private RichPopup commentsPopup;

    public void setAwardSectorList(List<Sector> awardSectorList) {
        this.awardSectorList = awardSectorList;
    }

    public List<Sector> getAwardSectorList() {
        return awardSectorList;
    }

    public void setSectorList(List<Sector> sectorList) {
        this.sectorList = sectorList;
    }

    public List<Sector> getSectorList() {
        return sectorList;
    }

    public void setReportValidSections(ReportValidSections reportValidSections) {
        this.reportValidSections = reportValidSections;
    }

    public ReportValidSections getReportValidSections() {
        return reportValidSections;
    }

    public ReportsBean() {
        super();
    }
    public void reportInitializer() {
        Map pfm =  AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("getReportValidSections");
        exec.getParamsMap().put("parseq", pfm.get("selectedPar"));
        ReportValidSections rvs = (ReportValidSections) exec.execute();
        if(rvs!=null) {
            this.reportValidSections=rvs;
        }
        else {
            try {
                ReportValidSections t = new ReportValidSections();
                t.setDefaultValues();
                this.reportValidSections=t;
            } catch (SQLException e) {
            }
        }
    }
    /**
     * Commits all changes.
     * Runs execute on Par, parsector,parsectorindicator views
     * and reset the selected row to ones before execute.
     * @return
     */
    public String saveChanges() {
        logger.log(logger.TRACE, "In save changes ");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCControlBinding reports_ctrl = bindings.findCtrlBinding("ParView1");
        Row parrow = reports_ctrl.getCurrentRow();
        parseq = parrow.getAttribute("Parseq").toString();

        OperationBinding ctrl_commit = bindings.getOperationBinding("Commit");
        ctrl_commit.execute();
        OperationBinding populateStagectrl = bindings.getOperationBinding("populateStageTable");
        populateStagectrl.execute();
        //commit the data
        ctrl_commit.execute();
        Key parkey = this.getRowKey(parROTable);
        // Key parsectorkey = this.getRowKey(parSector);
        OperationBinding reports_execute = bindings.getOperationBinding("Execute");
        reports_execute.execute();
        logger.log(logger.TRACE, "Commit reports data ");
        this.setCurrentRow(parROTable, parkey);
        // this.setCurrentRow(parSector, parsectorkey);

        logger.log(logger.TRACE, "Report Saved for changes");
        return null;
    }

    public void setParSeqTable(RichTable parSeqTable) {
        this.parSeqTable = parSeqTable;
    }

    public RichTable getParSeqTable() {
        return parSeqTable;
    }

    public void setParSector(RichTable parSector) {
        this.parSector = parSector;
    }

    public RichTable getParSector() {
        return parSector;
    }

    /**
     * It creates a new row and then calls newPar and attachSectorsIndicators in AwardModuleImpl
     * to add details for new reports and add sectors and indicators if the report type is program.
     * @return
     */
    public String newReport() {
        logger.log(logger.TRACE, "In New Report");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl = bindings.getOperationBinding("CreateWithParams");
        ctrl.execute();
        OperationBinding ctrl1 = bindings.getOperationBinding("newPar");
        ctrl1.execute();
        OperationBinding ctrl_addsec = bindings.getOperationBinding("attachSectorsIndicators");
        ctrl_addsec.execute();
        logger.log(logger.TRACE, "New Report Row created");

        return null;
    }


    public void setParForm(RichPanelFormLayout parForm) {
        this.parForm = parForm;
    }

    public RichPanelFormLayout getParForm() {
        return parForm;
    }


    public void setAwardTable(RichTable awardTable) {
        this.awardTable = awardTable;
    }

    public RichTable getAwardTable() {
        return awardTable;
    }

    public void setIndicatorPopup(RichPopup indicatorPopup) {
        this.indicatorPopup = indicatorPopup;
    }

    public RichPopup getIndicatorPopup() {
        return indicatorPopup;
    }

    /**
     * Called when user exists out of AddIndicator Dialog.
     * If user click on ok, this method checks if an indicator is selected. If so
     * then will call addNewIndicator in AwardModuleImpl
     * It then displays a popup the new indicator has been added.
     * @param dialogEvent
     */
    public void indicatorDialogListener(DialogEvent dialogEvent) {
        logger.log(logger.TRACE, "In indicator Dialog Listener method");
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {

            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCControlBinding parindicatorLOV = bindings.findCtrlBinding("AwardIndicatorLookup2");
            if (parindicatorLOV.getCurrentRow() != null) {
                OperationBinding ctrl_addsec = bindings.getOperationBinding("addNewIndicator");
                ctrl_addsec.execute();
                AdfFacesContext.getCurrentInstance().addPartialTarget(mainToolbar);
                /* FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Indicator Added", "New Indicator is added. Please save changes");
                FacesContext.getCurrentInstance().addMessage(null, message);*/
                AdfFacesContext.getCurrentInstance().addPartialTarget(parIndicatorTable);
            }
        }
        logger.log(logger.TRACE, "completed indicator Dialog Listener method");
    }

    public void setParIndicatorTable(RichTable parIndicatorTable) {
        this.parIndicatorTable = parIndicatorTable;
    }

    public RichTable getParIndicatorTable() {
        return parIndicatorTable;
    }


    /**
     * Called when AddIndicator popup is called.
     * It calls removeExistingIndicators in AwardModuleImpl which removes the existing
     * indicators from the poup table.
     * @param popupFetchEvent
     */
    public void addIndicatorPopup(PopupFetchEvent popupFetchEvent) {
        logger.log(logger.TRACE, "In addIndicatorPopup method");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl_addsec = bindings.getOperationBinding("removeExistingIndicators");
        ctrl_addsec.execute();
        logger.log(logger.TRACE, "Completed addIndicatorPopup method");
    }

    /**
     * Called when value for isComplete or isSubmit is changed.
     * If complete='Y' then it will show Do you want to submit popup.
     * If called from submit then it checks if complete is 'Y' if so then will call submit popup
     * If not then it will popup a message saying you can't submit an incomplete report.
     * @param valueChangeEvent
     */
    public void isCompleteChangeListener(ValueChangeEvent valueChangeEvent) {
        logger.log(logger.TRACE, "In isCompleteChangeListener method");
        logger.finest("is complete" + isComplete.getValue());
        logger.finest("is submit" + submit.getValue());
        if (isComplete.getValue().toString() == "Y") {
            /* logger.finest("in complete if");
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(), ExtendedRenderKitService.class);
            StringBuilder strb = new StringBuilder("AdfPage.PAGE.findComponent(\"pt1:r1:1:p3\").show();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());*/
            RichPopup popup = this.getSubmitPopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            popup.show(ph);
        } else if (submit.getValue().toString() == "Y") {
            submit.setValue("N");
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Report Not Complete", "You cannot submit a report that is not complete.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        logger.log(logger.TRACE, "completed isCompleteChangeListener method");
    }

    public void setIsComplete(RichSelectOneRadio isComplete) {
        this.isComplete = isComplete;
    }

    public RichSelectOneRadio getIsComplete() {
        return isComplete;
    }

    /**
     * Called from Submit popup.
     * If user clicks on yes then it will set issubmit,submitby and submitdate for the report
     * and will refresh all the tables to make them uneditable.
     * If user clicks no then it will set issubmit='N'
     * @param dialogEvent
     */
    public void submitPopup(DialogEvent dialogEvent) {
        logger.log(logger.TRACE, "In submit popup");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            FacesContext fctx = FacesContext.getCurrentInstance();
            DCIteratorBinding iter = bindings.findIteratorBinding("PARView1Iterator");
            Row rw = iter.getCurrentRow();
            rw.setAttribute("Issubmit", "Y");
            //submit.setValue("Y");
            SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
            String username = sc.getUserName().toUpperCase();
            rw.setAttribute("SubmitBy", username);
            rw.setAttribute("SubmitDate", new Date());
            //AdfFacesContext.getCurrentInstance().addPartialTarget(parSeqTable);
            //AdfFacesContext.getCurrentInstance().addPartialTarget(parIndicatorTable);
            Key parkey = this.getRowKey(parROTable);
            Key parsectorkey = this.getRowKey(parSector);
            OperationBinding reports_execute = bindings.getOperationBinding("Execute");
            reports_execute.execute();
            this.setCurrentRow(parROTable, parkey);
            this.setCurrentRow(parSector, parsectorkey);

        } else {
            // submit.setValue("N");
            DCIteratorBinding iter = bindings.findIteratorBinding("PARView1Iterator");
            Row rw = iter.getCurrentRow();
            rw.setAttribute("Issubmit", "N");
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(submit);
        AdfFacesContext.getCurrentInstance().addPartialTarget(parSeqTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(parROTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(parForm);
        AdfFacesContext.getCurrentInstance().addPartialTarget(parSector);
        AdfFacesContext.getCurrentInstance().addPartialTarget(parIndicatorTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(mainToolbar);
        AdfFacesContext.getCurrentInstance().addPartialTarget(parToolbar);
        AdfFacesContext.getCurrentInstance().addPartialTarget(indicatorToolbar);
        this.saveChanges();
        logger.log(logger.TRACE, "completed submit popup");
    }


    /**
     * This function return true if the latest submitted report is equal to the current report.
     * It internally uses appx_pkg.latest_Sumbitted_Report function in AwardModule.
     * @return
     */
    public Boolean getLatestSubmittedReport() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl = bindings.getOperationBinding("get_latestSubmittedReport");
        Integer tmp = (Integer)ctrl.execute();
        DCIteratorBinding iter = bindings.findIteratorBinding("PARView1Iterator");
        Row rw = iter.getCurrentRow();
        if (rw != null && rw.getAttribute("Parseq").toString().equals("" + tmp))
            return true;
        return false;
    }

    /**
     *Called by the Recall popup. If yes then it sets the iscomplete and issubmit to N
     * and saves changes.
     * @param dialogEvent
     */
    public void recallReportDialogLnsr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            logger.log(logger.TRACE, "In recall function");
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("PARView1Iterator");
            Row rw = iter.getCurrentRow();
            rw.setAttribute("Iscomplete", "N");
            rw.setAttribute("SubmitBy", null);
            rw.setAttribute("SubmitDate", null);
            rw.setAttribute("Issubmit", "N");


            AdfFacesContext.getCurrentInstance().addPartialTarget(parROTable);
            try {
                AdfFacesContext.getCurrentInstance().addPartialTarget(submit);
                AdfFacesContext.getCurrentInstance().addPartialTarget(parSeqTable);
                AdfFacesContext.getCurrentInstance().addPartialTarget(parForm);
                AdfFacesContext.getCurrentInstance().addPartialTarget(parSector);
                AdfFacesContext.getCurrentInstance().addPartialTarget(parIndicatorTable);
                AdfFacesContext.getCurrentInstance().addPartialTarget(mainToolbar);
                AdfFacesContext.getCurrentInstance().addPartialTarget(parToolbar);
                AdfFacesContext.getCurrentInstance().addPartialTarget(isComplete);
                AdfFacesContext.getCurrentInstance().addPartialTarget(indicatorToolbar);
            } catch (NullPointerException e) {
                logger.log(logger.TRACE,
                           "completed " + rw.getAttribute("Parseq") + " report recalled from Search Awards screen");
            }
            this.saveChanges();
            logger.log(logger.TRACE, "completed " + rw.getAttribute("Parseq") + " report recall");

        }
    }

    public void setSubmit(RichSelectOneRadio submit) {
        this.submit = submit;
    }

    public RichSelectOneRadio getSubmit() {
        return submit;
    }

    public void setMainToolbar(RichToolbar mainToolbar) {
        this.mainToolbar = mainToolbar;
    }

    public RichToolbar getMainToolbar() {
        return mainToolbar;
    }


    public String getNewReportType() {
        return newReportType;
    }

    /**
     * Called when user clicks on New Report.
     * Creates new row in Par and will call newPar in AwardModuleImpl to add par details
     * and if report type is Program then will call attachSectorsIndicators in AwardModuleImpl
     * @param dialogEvent
     */
    public void newReportDL(DialogEvent dialogEvent) {
        logger.log(logger.TRACE, "In new Report dialog");
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            this.newReportOK();
        }
    }

    public void setParToolbar(RichToolbar parToolbar) {
        this.parToolbar = parToolbar;
    }

    public RichToolbar getParToolbar() {
        return parToolbar;
    }

    /**
     * Called when user clicks on Complete or Submit button.
     * If all the required values are present then isComplete is set to Y and then submit popup is show to user.
     * @return
     */
    public String reportCompleteAction() {
        logger.log(logger.TRACE, "In Report complete action");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();

        DCIteratorBinding iter = bindings.findIteratorBinding("PARView1Iterator");
        Row rw = iter.getCurrentRow();
        rw.setAttribute("Iscomplete", "Y");
        //   isComplete.setValue("Y");
        if (isComplete.getValue().toString() == "Y") {
            RichPopup popup = this.getSubmitPopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            popup.show(ph);
        } else if (submit.getValue().toString() == "Y") {
            submit.setValue("N");
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Report Not Complete", "You cannot submit a report that is not complete.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(isComplete);
        AdfFacesContext.getCurrentInstance().addPartialTarget(parSeqTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(parROTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(parForm);
        AdfFacesContext.getCurrentInstance().addPartialTarget(mainToolbar);
        AdfFacesContext.getCurrentInstance().addPartialTarget(parToolbar);
        AdfFacesContext.getCurrentInstance().addPartialTarget(indicatorToolbar);
        saveChanges();
        logger.log(logger.TRACE, "Report Changed to complete");
        return null;
    }

    /**
     * Called by each row in the tables par,parsector and parsectorindicator
     * Return color depending on rowStatus
     * Green    - if new
     * Yellow   - if modified
     * Grey     - if Submitted
     * @return
     */
    public String getRowColor() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExpressionFactory ef = ctx.getApplication().getExpressionFactory();
        ValueExpression ve = ef.createValueExpression(ctx.getELContext(), "#{row}", JUCtrlHierNodeBinding.class);
        JUCtrlHierNodeBinding node = (JUCtrlHierNodeBinding)ve.getValue(ctx.getELContext());
        Row row = node.getRow();

        OperationBinding getRowStatusBinding = bindings.getOperationBinding("getRowStatus");
        getRowStatusBinding.getParamsMap().put("row", row);
        String isEditable = (String)row.getAttribute("isEditable");
        if (isEditable.equals("N"))
            return "background-color:#C0C0C0";
        String rwStatus = (String)getRowStatusBinding.execute();
        if (rwStatus.equals("New"))
            return "background-color:#99FF99";
        else if (rwStatus.equals("Modified"))
            return "background-color:#FFFF99";
        else
            return "";

    }

    public void setRowColor(String rowColor) {
        this.rowColor = rowColor;
    }

    public void setNewIndicatorJustification(HtmlInputTextarea newIndicatorJustification) {
        this.newIndicatorJustification = newIndicatorJustification;
    }

    public HtmlInputTextarea getNewIndicatorJustification() {
        return newIndicatorJustification;
    }


    public String getNewReportPeriod() {

        return newReportPeriod;
    }

    public void setNewReportDueDate(RichInputDate newReportDueDate) {
        this.newReportDueDate = newReportDueDate;
    }

    public RichInputDate getNewReportDueDate() {
        return newReportDueDate;
    }

    public void setNewReportStartDate(RichInputDate newReportStartDate) {
        this.newReportStartDate = newReportStartDate;
    }

    public RichInputDate getNewReportStartDate() {
        return newReportStartDate;
    }

    public void setNewReportEndDate(RichInputDate newReportEndDate) {
        this.newReportEndDate = newReportEndDate;
    }

    public RichInputDate getNewReportEndDate() {
        return newReportEndDate;
    }

    /**
     * called when newReportPeriod is changed to update the due date, start date and end date
     * depending on the period type selected.
     * @param valueChangeEvent
     */
    public void newReportPeriodListener(ValueChangeEvent valueChangeEvent) {
        logger.log(logger.TRACE, "In report period listener");
        String newPeriod = valueChangeEvent.getNewValue().toString();
        newReportPeriod = newPeriod;
        Date dt[] = this.getPeriodDates(newPeriod, (Integer)this.getNewReportFY().getValue());
        newReportStartDate.setValue(dt[0]);
        newReportEndDate.setValue(dt[1]);
        newReportDueDate.setValue(dt[2]);
        AdfFacesContext.getCurrentInstance().addPartialTarget(newReportEndDate);
        AdfFacesContext.getCurrentInstance().addPartialTarget(newReportStartDate);
        AdfFacesContext.getCurrentInstance().addPartialTarget(newReportDueDate);
        logger.log(logger.TRACE, "Completed report period listener");
    }

    /**
     * This method return a Date array with Dates corresponding to the Period sent
     * to this method. The Dates in array are in the following order:- Start Date, End Date, Due Date
     * @param newPeriod
     * @return
     */
    private Date[] getPeriodDates(String newPeriod, Integer fy) {
        Date dt[] = new Date[3];
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding operationBinding = bindings.getOperationBinding("getPeriodDates");
        //arguments
        operationBinding.getParamsMap().put("pcode", newPeriod);
        operationBinding.getParamsMap().put("fy", fy);
        //invoke method
        String t[] = (String[])operationBinding.execute();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            for (int i = 0; i < 3; i++)
                if (t[i] != null)
                    dt[i] = dateFormat.parse(t[i]);
        } catch (ParseException e) {
            return null;
        }

        return dt;
    }

    /**
     * Called when Period (frequency) is changed in existing report.
     * It brings a popup asking for confirmation if user wants to populate dates
     * according to the period selected.
     * @param valueChangeEvent
     */
    public void reportPeriodChgLsnr(ValueChangeEvent valueChangeEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding operationBinding = bindings.getOperationBinding("restrictDocTypes");
        //arguments
        Integer newPeriod = (Integer)valueChangeEvent.getNewValue();
        operationBinding.getParamsMap().put("prdTypeCode", newPeriod);
        //invoke method
        operationBinding.execute();
        if (valueChangeEvent.getOldValue() != null) {
            RichPopup popup = this.getFrequencyChgPopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            popup.show(ph);
        } else {
            DCIteratorBinding iter = bindings.findIteratorBinding("ParView1Iterator");
            Row r = iter.getCurrentRow();
            Date dt[] = this.getPeriodDates("" + newPeriod, (Integer)r.getAttribute("PrdRptFy"));
            if (dt[0] != null)
                r.setAttribute("PrdStartDate", dt[0]);
            if (dt[1] != null)
                r.setAttribute("PrdEndDate", dt[1]);
            if (dt[2] != null)
                r.setAttribute("RptDueDate", dt[2]);
            if (reportsForm != null) {
                AdfFacesContext.getCurrentInstance().addPartialTarget(reportsForm);
            }

        }
    }

    public void setFrequencyChgPopup(RichPopup frequencyChgPopup) {
        this.frequencyChgPopup = frequencyChgPopup;
    }

    public RichPopup getFrequencyChgPopup() {
        return frequencyChgPopup;
    }

    /**
     * This is called from Period (Frequency) change popup. If the user selected "Yes"
     * then the dates are repopulated again from the lookup using getPeriodDates function.
     * @param dialogEvent
     */
    public void frequencyChgPopupDialog(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("PARView1Iterator");
            Row r = iter.getCurrentRow();
            String ftype = r.getAttribute("PrdTypeCode").toString();
            Date dt[] = this.getPeriodDates(ftype, (Integer)r.getAttribute("PrdRptFy"));
            if (dt[0] != null)
                r.setAttribute("PrdStartDate", dt[0]);
            if (dt[1] != null)
                r.setAttribute("PrdEndDate", dt[1]);
            if (dt[2] != null)
                r.setAttribute("RptDueDate", dt[2]);
            if (reportsForm != null) {
                AdfFacesContext.getCurrentInstance().addPartialTarget(reportsForm);
            }

            if (parSeqTable != null) {
                AdfFacesContext.getCurrentInstance().addPartialTarget(parSeqTable);
            }
        }
    }

    /**
     * Called when user clicks on Manage Reports/View Reports button.
     * If the award is closed it will display popup that award is closed and you can only view reports.
     * @param actionEvent
     */
    public void manageReportsButton(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("AwardView1Iterator");
        Row rw = iter.getCurrentRow();
        String status = rw.getAttribute("AwardStatus").toString();
        if ("Closed".equals(status)) {
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Award is Closed", "This Award is closed. You can only view existing reports");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }


    }

    public void setRequiredValues(RichOutputText requiredValues) {
        this.requiredValues = requiredValues;
    }

    public RichOutputText getRequiredValues() {
        return requiredValues;
    }

    public void setMainSplitter(RichPanelSplitter mainSplitter) {
        this.mainSplitter = mainSplitter;
    }

    public RichPanelSplitter getMainSplitter() {
        return mainSplitter;
    }

    public void setStatusCheckBox(RichSelectManyCheckbox statusCheckBox) {
        this.statusCheckBox = statusCheckBox;

    }

    public RichSelectManyCheckbox getStatusCheckBox() {
        return statusCheckBox;
    }

    public void runAwardWhereClause() {
        if (checkedValues!=null && checkedValues.length > 0) {

            whereClauseAwardStatus = "award_status in (";
            for (int i = 0; i < checkedValues.length; i++) {
                whereClauseAwardStatus += "'" + checkedValues[i] + "'";
                if (i + 1 != checkedValues.length)
                    whereClauseAwardStatus += ",";
            }
            whereClauseAwardStatus += ")";

        } else
            whereClauseAwardStatus = "1=1";
        awardViewWhereClause = concatenateWhereClause();
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl = bindings.getOperationBinding("addAwardWhereClause");
        ctrl.execute();
    }

    public void checboxChangeListener(ValueChangeEvent valueChangeEvent) {

 //       if (valueChangeEvent.getNewValue() != null) {
            checkedValues = (String[])valueChangeEvent.getNewValue();
            runAwardWhereClause();
   //     }
        AdfFacesContext.getCurrentInstance().addPartialTarget(awardTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(searchMyAwardsSplitter);
    }


    public void setCheckedValues(String[] checkedValues) {
        this.checkedValues = checkedValues;
    }

    public String[] getCheckedValues() {
        return checkedValues;
    }

    public String getAwardViewWhereClause() {
        return awardViewWhereClause;
    }

    public void setSearchMyAwardsSplitter(RichPanelSplitter searchMyAwardsSplitter) {
        this.searchMyAwardsSplitter = searchMyAwardsSplitter;
    }

    public RichPanelSplitter getSearchMyAwardsSplitter() {
        return searchMyAwardsSplitter;
    }

    public void rollbackChanges(ActionEvent actionEvent) {
        logger.log(logger.TRACE, "In rollback changes");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        Key awardKey = this.getRowKey(awardTable);
        Key parKey = this.getRowKey(parROTable);
        OperationBinding reports_execute = bindings.getOperationBinding("Rollback");
        reports_execute.execute();
        this.setCurrentRow(awardTable, awardKey);
        AdfFacesContext.getCurrentInstance().addPartialTarget(mainSplitter);
        AdfFacesContext.getCurrentInstance().addPartialTarget(awardTable);
        try {
            this.setCurrentRow(parROTable, parKey);
        } catch (Exception e) {
            logger.finest("New report cancel");
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(parROTable);

        AdfFacesContext.getCurrentInstance().addPartialTarget(parSeqTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(parForm);
        AdfFacesContext.getCurrentInstance().addPartialTarget(parSector);
        AdfFacesContext.getCurrentInstance().addPartialTarget(parIndicatorTable);

        logger.log(logger.TRACE, "Completed Rollback changes");
    }

    public void setSubmitPopup(RichPopup submitPopup) {
        this.submitPopup = submitPopup;
    }

    public RichPopup getSubmitPopup() {
        return submitPopup;
    }

    public void setMissingValuesPopup(RichPopup missingValuesPopup) {
        this.missingValuesPopup = missingValuesPopup;
    }

    public RichPopup getMissingValuesPopup() {
        return missingValuesPopup;
    }

    public void setParROTable(RichTable parROTable) {
        this.parROTable = parROTable;
    }

    public RichTable getParROTable() {
        return parROTable;
    }

    public String validateReport() {
        // Add event code here...
        logger.log(logger.TRACE, "In Validate Report");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl = bindings.getOperationBinding("isReportComplete");
        Object t = ctrl.execute();
        if (t == null) {
            if (requiredValues != null) {
                requiredValues.setValue("All the values required in the report are present. Click on \"Complete\" button to complete and submit the report.");
                AdfFacesContext.getCurrentInstance().addPartialTarget(requiredValues);
            }
        } else {
            if (requiredValues != null) {
                requiredValues.setValue(t);
                AdfFacesContext.getCurrentInstance().addPartialTarget(requiredValues);
            }
        }
        RichPopup popup = this.getMissingValuesPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
        logger.log(logger.TRACE, "Completed Validate Report");
        return null;
    }

    public void setEnableComplete(Boolean enableComplete) {
        this.enableComplete = enableComplete;
    }

    public Boolean getEnableComplete() {
        logger.log(logger.TRACE, "In Enable Complete");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl = bindings.getOperationBinding("isReportComplete");
        DCControlBinding reports_ctrl = bindings.findCtrlBinding("ParView1");
        Row parrow = reports_ctrl.getCurrentRow();
        if (parrow != null) {
            Object t = ctrl.execute();
            if (t == null) {
                enableComplete = Boolean.TRUE;
            } else {
                enableComplete = Boolean.FALSE;
            }
        } else
            enableComplete = Boolean.FALSE;
        logger.log(logger.TRACE, "Completed Validate Report with value: " + enableComplete);
        return enableComplete;
    }

    public void setNewReportFY(RichInputNumberSpinbox newReportFY) {
        this.newReportFY = newReportFY;
    }

    public RichInputNumberSpinbox getNewReportFY() {

        return newReportFY;
    }

    public Integer getGetFY() {
       /* if (getFY == null) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iterb = bindings.findIteratorBinding("CurrentFY1Iterator");
            RowSetIterator iter = iterb.getRowSetIterator();
            Row rw = iter.first();

            if (rw != null)
                return ((Integer)rw.getAttribute("Fy"));
        }*/
        return getFY;
    }

    public void setGetFY(Integer getFY) {
        this.getFY = getFY;
    }

    public void docTabLsnr(DisclosureEvent disclosureEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl = bindings.getOperationBinding("Execute");
        ctrl.execute();
    }

    public void setDocTabTable(RichTable docTabTable) {
        this.docTabTable = docTabTable;
    }

    public RichTable getDocTabTable() {
        return docTabTable;
    }

    public String getHelpTopicID() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCControlBinding reports_ctrl = bindings.findCtrlBinding("ParView1");
        Row parrow = reports_ctrl.getCurrentRow();
        if (parrow != null) {
            Integer t = (Integer)parrow.getAttribute("RptTypeCode");
            if (t == 1 || t == 3)
                return "topic6a" + t;
            else
                return "topic6a4";
        } else
            return "topic6a0";
    }

    public void setNewReportPopup(RichPopup newReportPopup) {
        this.newReportPopup = newReportPopup;
    }

    public RichPopup getNewReportPopup() {
        return newReportPopup;
    }

    public String newReportOK() {
        logger.log(logger.TRACE, "In new Report dialog");
        if (newReportType != null) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            // OperationBinding ctrl = bindings.getOperationBinding("CreateWithParams");
            // ctrl.execute();
            OperationBinding ctrl1 = bindings.getOperationBinding("createParAndPopulate");
            ctrl1.execute();
            DCControlBinding reports_ctrl = bindings.findCtrlBinding("ParView1");
            Row parrow = reports_ctrl.getCurrentRow();
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put("selectedPar", parrow.getAttribute("Parseq"));
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put("rptTypeCode",
                                                                        parrow.getAttribute("RptTypeCode"));
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put("rptNbr", parrow.getAttribute("RptNbr"));
            if(parrow!=null) {
                //getESAwardDetails
                ctrl1 = bindings.getOperationBinding("getESAwardDetails");
                ctrl1.getParamsMap().put("awid", parrow.getAttribute("Awid"));
                Award a = (Award) ctrl1.execute();
                ElasticSearchRestBean esrb=new ElasticSearchRestBean();
                esrb.updateArticle(a);
            }
            OperationBinding exec = bindings.getOperationBinding("getReportValidSections");
            exec.getParamsMap().put("parseq", parrow.getAttribute("Parseq"));
            ReportValidSections rvs = (ReportValidSections) exec.execute();
            if(rvs!=null) {
                this.reportValidSections=rvs;
            }
            else {
                try {
                    ReportValidSections t = new ReportValidSections();
                    t.setDefaultValues();
                    this.reportValidSections=t;
                } catch (SQLException e) {
                }
            }
            logger.log(logger.TRACE, "Created new report");
        }
        newReportPopup.hide();
        RichPopup popup = this.getReportCreationInformationPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
        return null;

    }

    public String newReportCancel() {
        newReportPopup.hide();
        return null;
    }

    public void executeAddIndicator(ReturnEvent returnEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl = bindings.getOperationBinding("Execute6");
        ctrl.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(addIndicatorTable);
    }

    public void setAddIndicatorTable(RichTable addIndicatorTable) {
        this.addIndicatorTable = addIndicatorTable;
    }

    public RichTable getAddIndicatorTable() {
        return addIndicatorTable;
    }

    public void newReportTypeLsnr(ValueChangeEvent valueChangeEvent) {
        newReportType = valueChangeEvent.getNewValue().toString();

    }

    /**
     * Recalculate This period amount when cumulative and previously reported are changed.
     * @param valueChangeEvent
     */
    public void cumTotalChgLsnr(ValueChangeEvent vce) {
        this.updateThisPeriodAmt("PrevRptAmt", (String)vce.getNewValue(), "PrdAmt", 1);
    }

    public void prevTotalChgLsnr(ValueChangeEvent vce) {
        this.updateThisPeriodAmt("CumAmt", (String)vce.getNewValue(), "PrdAmt", 2);

    }

    public void fedPrevTotalChgLsnr(ValueChangeEvent vce) {
        this.updateThisPeriodAmt("FedCumAmt", (String)vce.getNewValue(), "FedPrdAmt", 2);

    }

    public void fedCumTotalChgLsnr(ValueChangeEvent vce) {
        this.updateThisPeriodAmt("FedPrevRptAmt", (String)vce.getNewValue(), "FedPrdAmt", 1);

    }

    /**
     *
     * @param unchangedName
     * @param changedValue
     * @param toAttributeName Name of the view object attribute that needs to be updated.
     * @param flag if 1 then subtracts changed value - unchanged values. If others then unchanged value-changed value
     *
     */
    private void updateThisPeriodAmt(String unchangedName, String changedValue, String toAttributeName, int flag) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ParView1Iterator");
        Row r = iter.getCurrentRow();
        oracle.jbo.domain.Number unchanged = (oracle.jbo.domain.Number)r.getAttribute(unchangedName);
        if (unchanged == null)
            unchanged = new oracle.jbo.domain.Number(0);

        Number changed = 0;
        if (changedValue != null)
            changed = new Double(changedValue);
        if (flag == 1)
            r.setAttribute(toAttributeName, (changed.doubleValue() - unchanged.doubleValue()));
        else
            r.setAttribute(toAttributeName, (unchanged.doubleValue() - changed.doubleValue()));
        //  AdfFacesContext.getCurrentInstance().addPartialTarget(parSeqTable);
    }


    public void setDocumentsPopup(RichPopup documentsPopup) {
        this.documentsPopup = documentsPopup;
    }

    public RichPopup getDocumentsPopup() {
        return documentsPopup;
    }

    /**
     * Restricts the doc type depending on Type and Period of the Report.
     * @return
     */
    public String showDocumentsPopup() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl = bindings.getOperationBinding("restrictDocTypes");
        ctrl.execute();
        RichPopup popup = this.getDocumentsPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
        return null;
    }

    /**
     * Delete selected report
     * @param dialogEvent
     */
    public void deleteReportDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            //Get ParSeq to be used by populateStageTable
            DCControlBinding reports_ctrl = bindings.findCtrlBinding("ParView1");
            Row parrow = reports_ctrl.getCurrentRow();
            Object awid=parrow.getAttribute("Awid");
            parseq = parrow.getAttribute("Parseq").toString();
            OperationBinding commit = bindings.getOperationBinding("Commit");
            //parrow.setAttribute("RptDesc","Deleted by "+ADFContext.getCurrent().getSecurityContext().getUserName().toUpperCase());
            //commit.execute();
            OperationBinding delete = bindings.getOperationBinding("Delete");
            delete.execute();
            commit.execute();
            OperationBinding auditUpdate = bindings.getOperationBinding("updateAuditRecordsonDelete");
            auditUpdate.execute();
            OperationBinding populateStagectrl = bindings.getOperationBinding("populateStageTable");
            populateStagectrl.execute();
            commit.execute();
            if(awid!=null) {
                //getESAwardDetails
                OperationBinding ctrl1 = bindings.getOperationBinding("getESAwardDetails");
                ctrl1.getParamsMap().put("awid",awid );
                Award a = (Award) ctrl1.execute();
                ElasticSearchRestBean esrb=new ElasticSearchRestBean();
                esrb.updateArticle(a);
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(parROTable);
            AdfFacesContext.getCurrentInstance().addPartialTarget(reportsTabToolbar);
        }
    }

    public void setReportsTabToolbar(RichToolbar reportsTabToolbar) {
        this.reportsTabToolbar = reportsTabToolbar;
    }

    public RichToolbar getReportsTabToolbar() {
        return reportsTabToolbar;
    }

    public String getParseq() {
        return parseq;
    }

    /**
     *
     * @param dialogEvent
     */
    public void deleteIndicator(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding delete = bindings.getOperationBinding("DeleteIndicator");
            delete.execute();
            /*delete = bindings.getOperationBinding("Commit");
                delete.execute();*/
            AdfFacesContext.getCurrentInstance().addPartialTarget(indicatorTree);
            AdfFacesContext.getCurrentInstance().addPartialTarget(parIndicatorForm);
        }

    }

    public void setIndicatorToolbar(RichToolbar indicatorToolbar) {
        this.indicatorToolbar = indicatorToolbar;
    }

    public RichToolbar getIndicatorToolbar() {
        return indicatorToolbar;
    }

    public void newFYChgLsnr(ValueChangeEvent valueChangeEvent) {
        Integer fy = (Integer)valueChangeEvent.getNewValue();
        Date dt[] = this.getPeriodDates(newReportPeriod, fy);
        newReportStartDate.setValue(dt[0]);
        newReportEndDate.setValue(dt[1]);
        newReportDueDate.setValue(dt[2]);
        AdfFacesContext.getCurrentInstance().addPartialTarget(newReportEndDate);
        AdfFacesContext.getCurrentInstance().addPartialTarget(newReportStartDate);
        AdfFacesContext.getCurrentInstance().addPartialTarget(newReportDueDate);
    }

    public void setNewReportRedirect(RichCommandToolbarButton newReportRedirect) {
        this.newReportRedirect = newReportRedirect;
    }

    public RichCommandToolbarButton getNewReportRedirect() {
        return newReportRedirect;
    }


    public void setSelectedPar(int selectedPar) {
        this.selectedPar = selectedPar;
    }

    public int getSelectedPar() {
        return selectedPar;
    }


    public void setRptTypeCode(int rptTypeCode) {
        this.rptTypeCode = rptTypeCode;
    }

    public int getRptTypeCode() {
        return rptTypeCode;
    }

    public void setIndicatorTree(RichTreeTable indicatorTree) {
        this.indicatorTree = indicatorTree;
    }

    public RichTreeTable getIndicatorTree() {
        return indicatorTree;
    }

    public void setParIndicatorForm(RichPanelFormLayout parIndicatorForm) {
        this.parIndicatorForm = parIndicatorForm;
    }

    public RichPanelFormLayout getParIndicatorForm() {
        return parIndicatorForm;
    }

    public void setReportsForm(RichPanelFormLayout reportsForm) {
        this.reportsForm = reportsForm;
    }

    public RichPanelFormLayout getReportsForm() {
        return reportsForm;
    }

    /**
     * This is called from Period (Frequency) change popup. If the user selected "Yes"
     * then the dates are repopulated again from the lookup using getPeriodDates function.
     * @param dialogEvent
     */
    public void frequencyChangePopupDialog(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("ParView1Iterator");
            Row r = iter.getCurrentRow();
            String ftype = r.getAttribute("PrdTypeCode").toString();
            Date dt[] = this.getPeriodDates(ftype, (Integer)r.getAttribute("PrdRptFy"));
            if (dt[0] != null)
                r.setAttribute("PrdStartDate", dt[0]);
            if (dt[1] != null)
                r.setAttribute("PrdEndDate", dt[1]);
            if (dt[2] != null)
                r.setAttribute("RptDueDate", dt[2]);
            if (reportsForm != null) {
                AdfFacesContext.getCurrentInstance().addPartialTarget(reportsForm);
            }

        }
    }

    public void setKey() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = (DCIteratorBinding)bindings.findIteratorBinding("AwardView1Iterator");
        Key key = iter.getCurrentRow().getKey();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("awardRowKey", key);
        iter = (DCIteratorBinding)bindings.findIteratorBinding("ParView1Iterator");
        key = iter.getCurrentRow().getKey();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("parRowKey", key);
    }

    public void setCurrentAwardParRows() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = null;
        Key key = (Key)AdfFacesContext.getCurrentInstance().getPageFlowScope().get("awardRowKey");
        if (key == null) {
            iter = (DCIteratorBinding)bindings.findIteratorBinding("AwardView1Iterator");
            key = iter.getCurrentRow().getKey();
        }
        OperationBinding exec = bindings.getOperationBinding("ExecuteAward");
        exec.execute();
        iter = (DCIteratorBinding)bindings.findIteratorBinding("AwardView1Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        if (rsi.findByKey(key, 1).length > 0)
            iter.setCurrentRowWithKey(key.toStringFormat(true));

        key = (Key)AdfFacesContext.getCurrentInstance().getPageFlowScope().get("parRowKey");
        if (key == null) {
            iter = (DCIteratorBinding)bindings.findIteratorBinding("ParView1Iterator");
            key = iter.getCurrentRow().getKey();
        }
        exec = bindings.getOperationBinding("ExecutePar");
        exec.execute();
        iter = (DCIteratorBinding)bindings.findIteratorBinding("ParView1Iterator");
        rsi = iter.getRowSetIterator();
        if (rsi.findByKey(key, 1).length > 0)
            iter.setCurrentRowWithKey(key.toStringFormat(true));
    }

    /**
     * Called when user clicks on cancel.
     * @return
     */
    public String cancelChanges() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        this.setKey();
        OperationBinding exec = bindings.getOperationBinding("Rollback");
        exec.execute();
        exec = bindings.getOperationBinding("setAwardCurrentRowWithKey");
        exec.execute();
        exec = bindings.getOperationBinding("ExecutePar");
        exec.execute();
        exec = bindings.getOperationBinding("setParCurrentRowWithKey");
        exec.execute();
        return null;
    }

    public String reportComplete() {
        RichPopup popup = this.getSubmitPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
        return null;
    }


    public String changeReportToDraft() {
        TrainTemplateBean pageFlowScopeInstance = null;
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ParView1Iterator");
        Row rw = iter.getCurrentRow();
        rw.setAttribute("Iscomplete", "N");
        rw.setAttribute("SubmitBy", null);
        rw.setAttribute("SubmitDate", null);
        rw.setAttribute("Issubmit", "N");
        pageFlowScopeInstance = retrievePageFlowScopeTrainTemplateBean();
        pageFlowScopeInstance.saveChanges();
        pageFlowScopeInstance.cancelChanges();
        if(rw!=null)
        {
            
            OperationBinding ctrl1 = bindings.getOperationBinding("getESAwardDetails");
            ctrl1.getParamsMap().put("awid", rw.getAttribute("Awid"));
            Award a = (Award) ctrl1.execute();
            ElasticSearchRestBean esrb=new ElasticSearchRestBean();
            esrb.updateArticle(a);
        }
        if (validatePagePanelGroup != null) {
            AdfFacesContext.getCurrentInstance().addPartialTarget(validatePagePanelGroup);
        }
        pageFlowScopeInstance.refresh();
        return null;
    }

    public TrainTemplateBean retrievePageFlowScopeTrainTemplateBean() {
        FacesContext fctx = FacesContext.getCurrentInstance();
        Application application = fctx.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext context = fctx.getELContext();
        ValueExpression createValueExpression =
            expressionFactory.createValueExpression(context, "#{pageFlowScope.TrainTemplateBean}",
                                                    TrainTemplateBean.class);
        TrainTemplateBean pageFlowScopeInstance = (TrainTemplateBean)createValueExpression.getValue(context);
        return pageFlowScopeInstance;
    }

    /**
     * This function return true if the latest submitted report is equal to the current report.
     * It internally uses appx_pkg.latest_Sumbitted_Report function in AwardModule.
     * a) All completed reports(but not submitted) can be recalled.
     * b) For submitted report, last submitted report should match the parseq
     * @return
     */
    public Boolean getRecallApplicable() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ParView1Iterator");
        Row rw = iter.getCurrentRow();
        OperationBinding ctrl = bindings.getOperationBinding("get_latestSubmittedReport");
        ctrl.getParamsMap().put("awid", rw.getAttribute("Awid").toString());
        ctrl.getParamsMap().put("rptType", rw.getAttribute("RptTypeCode").toString());
        ctrl.getParamsMap().put("parseq", rw.getAttribute("Parseq").toString());
        Integer tmp = (Integer)ctrl.execute();
        if (rw != null &&
            (rw.getAttribute("Iscomplete").toString().equals("Y") && rw.getAttribute("Issubmit").toString().equals("N")))
            return false;
        if (rw != null && rw.getAttribute("Parseq").toString().equals("" + tmp))
            return false;
        return true;
    }

    public void processRecallReport(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            logger.log(logger.TRACE, "In recall function");
            this.changeReportToDraft();
        }
    }

    public void completePopupListener(DialogEvent dialogEvent) {
        TrainTemplateBean pageFlowScopeInstance = null;
        logger.log(logger.TRACE, "In Report complete action: " + dialogEvent.getOutcome());
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ParView1Iterator");
        Row rw = iter.getCurrentRow();
        rw.setAttribute("Iscomplete", "Y");
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            rw.setAttribute("Issubmit", "Y");
            SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
            String username = sc.getUserName().toUpperCase();
            rw.setAttribute("SubmitBy", username);
            rw.setAttribute("SubmitDate", new Date());
        }
        RichPopup popup = this.getSubmitPopup();
        popup.hide();
        pageFlowScopeInstance = retrievePageFlowScopeTrainTemplateBean();
        pageFlowScopeInstance.saveChanges();
        pageFlowScopeInstance.cancelChanges();
        if(rw!=null && "Y".equals(rw.getAttribute("Issubmit"))) {
            OperationBinding ctrl1 = bindings.getOperationBinding("getESAwardDetails");
            ctrl1.getParamsMap().put("awid", rw.getAttribute("Awid"));
            Award a = (Award) ctrl1.execute();
            ElasticSearchRestBean esrb=new ElasticSearchRestBean();
            esrb.updateArticle(a);
        }
        if (validatePagePanelGroup != null) {
            AdfFacesContext.getCurrentInstance().addPartialTarget(validatePagePanelGroup);
        }
        pageFlowScopeInstance.refresh();
        logger.log(logger.TRACE, "Report Changed to complete");
    }

    public void ReportCreationInstructionDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            ActionEvent e = new ActionEvent(newReportRedirect);
            e.queue();
            reportCreationInformationPopup.hide();
        }
    }


    public void setReportCreationInformationPopup(RichPopup reportCreationInformationPopup) {
        this.reportCreationInformationPopup = reportCreationInformationPopup;
    }

    public RichPopup getReportCreationInformationPopup() {
        return reportCreationInformationPopup;
    }

    public void setValidatePagePanelGroup(RichPanelGroupLayout validatePagePanelGroup) {
        this.validatePagePanelGroup = validatePagePanelGroup;
    }

    public RichPanelGroupLayout getValidatePagePanelGroup() {
        return validatePagePanelGroup;
    }

    public void reportFYChgLsnr(ValueChangeEvent valueChangeEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ParView1Iterator");
        Row r = iter.getCurrentRow();
        if (r.getAttribute("PrdTypeCode") != null) {
            RichPopup popup = this.getFrequencyChgPopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            popup.show(ph);
        }
    }

    public boolean isFourwValid() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding check = bindings.getOperationBinding("is4wValid");
        Boolean valid = (Boolean)check.execute();
        if (valid != null)
            return valid;
        return false;
    }

    public void delPar4wDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding commit = bindings.getOperationBinding("Commit");
            OperationBinding delete = bindings.getOperationBinding("DeletePar4wReport");
            delete.execute();
            commit.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(report4wTable.getParent());
        }
    }

    public void updatePar4wView() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = (DCIteratorBinding)bindings.findIteratorBinding("Par4wView1Iterator");
        Key key = null;
        if (iter != null && iter.getCurrentRow() != null)
            key = iter.getCurrentRow().getKey();
        OperationBinding ctrl = bindings.getOperationBinding("Execute");
        ctrl.execute();
        if (key != null) {
            iter = (DCIteratorBinding)bindings.findIteratorBinding("Par4wView1Iterator");
            RowSetIterator rsi = iter.getRowSetIterator();
            if (rsi.findByKey(key, 1).length > 0)
                iter.setCurrentRowWithKey(key.toStringFormat(true));
        }
    }
    public List<SelectItem> getApplicableValuesfor4W() {
        List<SelectItem> locationsSelectItems = new ArrayList<SelectItem>();
        SelectItem item = new SelectItem("true", "4W Trackers", null);
        locationsSelectItems.add(item);
        return locationsSelectItems;
    }

    public void setFilter4wCheckBox(RichSelectManyCheckbox filter4wCheckBox) {
        this.filter4wCheckBox = filter4wCheckBox;
    }

    public RichSelectManyCheckbox getFilter4wCheckBox() {
        return filter4wCheckBox;
    }

    public void checbox4wChangeListener(ValueChangeEvent valueChangeEvent) {
        List values4w = null;

        if (valueChangeEvent.getNewValue() != null) {
            values4w = (List)valueChangeEvent.getNewValue();
            if (values4w != null && values4w.size() > 0) {
                whereClause4W = "is_4wtracker in ('True')";
            }
        } else {
            whereClause4W = "1=1";
        }
        awardViewWhereClause = concatenateWhereClause();
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl = bindings.getOperationBinding("addAwardWhereClause");
        ctrl.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(awardTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(searchMyAwardsSplitter);
    }

    private String concatenateWhereClause() {
        StringBuffer sb = new StringBuffer();
        if (whereClause4W != null) {
            sb.append(whereClause4W);
            sb.append(" and ");
        }
        if (whereClauseAwardStatus != null) {
            sb.append(whereClauseAwardStatus);
        }
        if (whereClause4W == null && whereClauseAwardStatus == null) {
            sb.append("1=1");
        }
        return sb.toString();
    }

    public void setReport4wTable(RichTable report4wTable) {
        this.report4wTable = report4wTable;
    }

    public RichTable getReport4wTable() {
        return report4wTable;
    }

    public void addEvalSectorFetchLsnr(PopupFetchEvent popupFetchEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("getAwardSectorList");
        Set<Sector> awardSectorSet=(Set<Sector>)exec.execute();
        
        exec = bindings.getOperationBinding("getSectorList");
        List<Sector> t = (List<Sector>) exec.execute();
        this.setSectorList(t);
        
        awardSectorSet.addAll(t);
        Set<Sector> ts=new TreeSet<Sector>(awardSectorSet);
        this.setAwardSectorList(new ArrayList<Sector>(ts));
    }

    public void addEvalSectorsDialogLsnr(DialogEvent dialogEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("updateSectors");
        Boolean t = (Boolean) exec.execute();
    }
    
    public void addSectorToList(ActionEvent actionEvent) {
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String sectorCode  = (String) requestMap.get("sectorCode");
        String sector=(String)requestMap.get("sector");
        Sector t=new Sector(sectorCode,sector);
        if(!this.getSectorList().contains(t)) {
            this.getSectorList().add(t);
            AdfFacesContext.getCurrentInstance().addPartialTarget(actionEvent.getComponent().getParent().getParent());
        }
        
    }

    public void removeSectorFromList(ActionEvent actionEvent) {
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String sectorCode  = (String) requestMap.get("sectorCode");
        String sector=(String)requestMap.get("sector");
        Sector t=new Sector(sectorCode,sector);
        if(this.getSectorList().contains(t)) {
            this.getSectorList().remove(t);
            AdfFacesContext.getCurrentInstance().addPartialTarget(actionEvent.getComponent().getParent().getParent());
        }
    }

    public void isApplicableChgLsnr(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Map map = ((UIComponent)vce.getSource()).getAttributes();
        String value = (String)map.get("rowIndexVal");
        if("N".equals(value)) {
            RichPopup popup = this.getCommentsPopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            popup.show(ph);
        }
    }
    
    public void setCommentsPopup(RichPopup commentsPopup) {
        this.commentsPopup = commentsPopup;
    }

    public RichPopup getCommentsPopup() {
        return commentsPopup;
    }
    public void keywordIndIsapplicableChgLsnr(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Map map = ((UIComponent)vce.getSource()).getAttributes();
        String value = (String)map.get("rowIndexVal");
        if("N".equals(value)) {
            RichPopup popup = this.getKeywordIndicatorCommentsPopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            popup.show(ph);
        }
    }

    public void setKeywordIndicatorCommentsPopup(RichPopup keywordIndicatorCommentsPopup) {
        this.keywordIndicatorCommentsPopup = keywordIndicatorCommentsPopup;
    }

    public RichPopup getKeywordIndicatorCommentsPopup() {
        return keywordIndicatorCommentsPopup;
    }
}
