package gov.ofda.abacus.portal.controller.elasticsearch;

import com.google.gson.Gson;

import com.google.gson.GsonBuilder;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.share.ADFContext;

import gov.ofda.abacus.portal.pojo.Award;
import gov.ofda.abacus.portal.pojo.Comparator.ReportDueDateComparator;
import gov.ofda.abacus.portal.pojo.Report;

import gov.ofda.abacus.portal.pojo.SortItem;

import gov.ofda.abacus.portal.pojo.UserDashboardPreference;
import gov.ofda.abacus.portal.pojo.UserPreference;

import java.io.IOException;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import oracle.adf.share.logging.ADFLogger;

import oracle.adf.share.security.SecurityContext;

import oracle.binding.OperationBinding;

import oracle.security.jps.JpsException;
import oracle.security.jps.service.JpsServiceLocator;
import oracle.security.jps.service.ServiceLocator;
import oracle.security.jps.service.credstore.CredStoreException;
import oracle.security.jps.service.credstore.CredentialExpiredException;
import oracle.security.jps.service.credstore.CredentialStore;
import oracle.security.jps.service.credstore.GenericCredential;
import oracle.security.jps.service.credstore.PasswordCredential;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;

import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;

import org.elasticsearch.client.RestClientBuilder;

import org.json.JSONArray;
import org.json.JSONObject;
public class ElasticSearchRestBean implements Serializable{
    @SuppressWarnings("compatibility:-2555319143612783658")
    private static final long serialVersionUID = 1L;
    private String ES_PROTOCOL;
    private String ES_IP;
    private Integer ES_PORT;
    private Map appScope;
    final transient CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
    private static ADFLogger logger = ADFLogger.createADFLogger(ElasticSearchRestBean.class);
    public ElasticSearchRestBean() {
        super();
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =bindingContext.findBindingContainer("gov_ofda_abacus_art_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getAppConstants");
        exec.execute();
        appScope = (Map<String, String>) exec.getResult();       
        ES_IP = (String) appScope.get("ES_HOST");
        ES_PROTOCOL=(String)appScope.get("ES_PROTOCOL");
        try{
            ES_PORT=Integer.parseInt((String) appScope.get("ES_PORT"));
           }
        catch(NumberFormatException e){
            ES_PORT=9200;
            }
        ServiceLocator locator = JpsServiceLocator.getServiceLocator();
        CredentialStore store = null;
        PasswordCredential pcred = null;
        try {
            store = locator.lookup(CredentialStore.class);
            pcred = (PasswordCredential)store.getCredential("ES", "Password");
            
        } catch (CredentialExpiredException e) {
            logger.log(e.toString());
        } catch (CredStoreException e) {
            logger.log(e.toString());
        } catch (JpsException e) {
            logger.log(e.toString());
    }
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(pcred.getName(), String.valueOf(pcred.getPassword())));
    }
    public ResultItem performARTRequest(SearchItem sitem) {
        ResultItem ritem=new ResultItem();
        Long total=new Long(0);
        RestClient restClient=null;;
        try {
            StringBuilder finalPostFilter=new StringBuilder();
            StringBuilder postFilter[]=new StringBuilder[3];
            postFilter[0]=new StringBuilder();
            postFilter[1]=new StringBuilder();
            postFilter[2]=new StringBuilder();
            StringBuilder postFilterLevel1=new StringBuilder();
            if(sitem!=null && sitem.getTermFilters()!=null)
            {
                Map<String,List<TermItem>>tfMap=sitem.getTfMap();
                for(Map.Entry<String,List<TermItem>> entry: tfMap.entrySet()) {
                    AggregationSearchItem asi=sitem.getAggSearchList().get(entry.getKey());
                    Integer level=asi.getLevel();

                    if(postFilter[level].length()>0) {
                        postFilter[level].append(",\n"); 
                    }
                    if(asi.getIsDate()) {
                        postFilter[level].append(
                        "                      {\n" + 
                        "                        \"bool\":{\"should\":[\n"); 
                        List<TermItem> termlist=entry.getValue();
                        for(int i=0;i<termlist.size();i++) {
                            postFilter[level].append(
                            "                          {\n" + 
                            "                            \"range\":\n" + 
                            "                            {\"");
                            if(level==1)
                                postFilter[level].append("reports.");
                            else if(level==2)
                                postFilter[level].append("reports.SECTOR_DETAILS");
                            postFilter[level].append(entry.getKey()+"\":{");
                            postFilter[level].append(
                            "                              \"gte\":\""+termlist.get(i).getValue()+"\",\n" + 
                            "                              \"lt\":\""+termlist.get(i).getValue()+"||+"+asi.getInterval()+"\"\n" + 
                            "                            }}\n" + 
                            "                          }");
                        if(i!=termlist.size()-1)
                             postFilter[level].append(","); 
                        }
                        postFilter[level].append(
                        "                          ]}\n" + 
                        "                      }");
                    }
                    else{
                        postFilter[level].append("{\"terms\":{\""+(asi.getPath() ==null?"":asi.getPath()+".")+entry.getKey()+(asi.getIsTermKeyword()?".keyword":"")+"\":[");
                        List<TermItem> termlist=entry.getValue();
                        for(int i=0;i<termlist.size();i++) {
                          postFilter[level].append("\""+termlist.get(i).getValue()+"\"");
                          if(i!=termlist.size()-1)
                              postFilter[level].append(","); 
                        }
                        postFilter[level].append("]}}");
                    }
                }
                finalPostFilter.append(postFilter[0]);
                if( postFilter[1].length()>0) {
                    if(finalPostFilter.length()>0)
                       finalPostFilter.append(",");
                    finalPostFilter.append(
                    "     { \"nested\": {\n" + 
                    "        \"path\": \"reports\",\n" + 
                    "        \"query\": {\n" + 
                    "            \"bool\": {\n" + 
                    "              \"must\":[\n" + postFilter[1]+
                    (postFilter[2].length()>0?
                        ",     { \"nested\": {\n" + 
                        "        \"path\": \"reports.SECTOR_DETAILS\",\n" + 
                        "        \"query\": {\n" + 
                        "            \"bool\": {\n" + 
                        "              \"must\":[\n" + postFilter[2]+
                        "                ]\n" + 
                        "            } \n" + 
                        "        }\n" + 
                        "      }}":"")+
                    "                ]\n" + 
                    "            } \n" + 
                    "        }\n" + 
                    (sitem.isShowInnerHits()?",        \"inner_hits\":{" +
                    "        \"size\":75" +
                    ",\"sort\": [\n" + 
                    "                  { \"reports.RPT_DUE_DATE\":{\n" + 
                    "                     \"order\": \"asc\"\n" + 
                    "                   }}\n" + 
                    "                 ]"+
                                             "}\n":"") + 
                    "      }}");
                    
                }
             else if(postFilter[2].length()>0) {
                    if(finalPostFilter.length()>0)
                       finalPostFilter.append(",");
                    finalPostFilter.append(
                    "     { \"nested\": {\n" + 
                    "        \"path\": \"reports\",\n" + 
                    "        \"query\": {\n" + 
                    "            \"bool\": {\n" + 
                    "              \"must\":[\n"+
                    "     { \"nested\": {\n" + 
                    "        \"path\": \"reports.SECTOR_DETAILS\",\n" + 
                    "        \"query\": {\n" + 
                    "            \"bool\": {\n" + 
                    "              \"must\":[\n" + postFilter[2]+
                    "                ]\n" + 
                    "            } \n" + 
                    "        }\n" + 
                    "      }}"+
                    "                ]\n" + 
                    "            } \n" + 
                    "        }\n" + 
                    (sitem.isShowInnerHits()?",        \"inner_hits\":{" +
                    "        \"size\":75" +
                    ",\"sort\": [\n" + 
                    "                  { \"reports.RPT_DUE_DATE\":{\n" + 
                    "                     \"order\": \"asc\"\n" + 
                    "                   }}\n" + 
                    "                 ]"+
                    "}\n":"") + 
                    "      }}");
                }
            }
            StringBuilder sb=new StringBuilder();
            /***********************************Award Active Years***************************************/
            Integer startFY=sitem.getStartFY();
            Integer endFY=sitem.getEndFY();
            if(startFY==null)
                startFY=2017;
            if(endFY==null)
                endFY=2018;
            sb.append("{\"bool\": {\n" + 
            "        \"should\": [\n" + 
            "        {\n" + 
            "          \"bool\": {\n" + 
            "            \"must\": [\n" + 
            "              {\"range\": {\n" + 
            "                \"start_fy\": {\n" + 
            "                  \"lte\": "+startFY+"\n" + 
            "                }\n" + 
            "              }},\n" + 
            "              {\"range\": {\n" + 
            "                \"end_fy\": {\n" + 
            "                  \"gte\": "+startFY+"\n" + 
            "                }\n" + 
            "              }}\n" + 
            "            ]\n" + 
            "          }},\n" + 
            "          {\"bool\": {\n" + 
            "            \"must\": [\n" + 
            "              {\"range\": {\n" + 
            "                \"start_fy\": {\n" + 
            "                  \"lte\": "+endFY+"\n" + 
            "                }\n" + 
            "              }},\n" + 
            "              {\"range\": {\n" + 
            "                \"end_fy\": {\n" + 
            "                  \"gte\": "+endFY+"\n" + 
            "                }\n" + 
            "              }}\n" + 
            "            ]\n" + 
            "          }},\n" + 
            "          {\"bool\": {\n" + 
            "            \"must\": [\n" + 
            "              {\"range\": {\n" + 
            "                \"start_fy\": {\n" + 
            "                  \"gte\": "+startFY+",\n" + 
            "                  \"lte\":"+endFY+"\n" + 
            "                }\n" + 
            "              }}\n" + 
            "            ]\n" + 
            "          }\n" + 
            "        }\n" + 
            "      ],\n" + 
            "      \"must_not\": [\n" + 
            "        {\"match\": {\n" + 
            "          \"awardee_type_code.keyword\": \"UNN\"\n" + 
            "        }},\n" + 
            "        {\"match\": {\n" + 
            "          \"awardee_type_code.keyword\": \"IIO\"\n" + 
            "        }}\n" + 
            "        \n" + 
            "      ]\n" + 
            "        }}, \n");
            if(sitem.getRestrictAwardee()) {
                sb.append("          {\n" + 
                "            \"bool\":{\n" + 
                "              \"must\":[\n" + 
                "                  {\"term\":{\n" + 
                "                    \"awardee_code.keyword\":\""+sitem.getAwardeeCode()+"\"\n" + 
                "                  }}\n" + 
                "                ]\n" + 
                "            }\n" + 
                "          },");
            }
            /***********************************Award Active Years***************************************/
            String status[]=sitem.getStatus();
            sb.append("{\"bool\": {\n" + 
            "\"should\": [\n");
            if(status!=null && status.length>0) {
                for(int s=0;s<status.length;s++)
                {
                    sb.append("{ \"term\": { \"award_status.keyword\": \""+status[s]+"\" }}");
                    if(s!=status.length-1)
                        sb.append(", \n");
                    else
                        sb.append("\n");
                }
            }
            else
                sb.append("{ \"term\": { \"award_status.keyword\": \"Active\" }}\n"); 
            sb.append("        ]\n" + "}}");

            String query=sitem.getSearchQuery();
            query=query.trim();
            StringBuilder queryString=new StringBuilder();
            if(query!=null){
                if(query.contains("\"")) {
                query=query.replaceAll("\"", "");
                    queryString.append(
                    "        {\"multi_match\": {\n" + 
                    "          \"query\": \""+query+"\",\n" + 
                    "          \"fields\": [\"award_status\",\"award_nbr\",\"awardee_acronym\",\"awardee\",\"pname\",\"region_name\",\"cname\",\"ofda_division_code\",\"ofda_team_code\",\"aor_name\",\"award_status\",\"region_code\",\"ccode\",\"pnbr\"],\n" + 
                    "          \"type\": \"phrase\"\n" + 
                    "        }}"+
                    ",        {\"nested\": {\n" + 
                    "              \"path\": \"reports\",\n" + 
                    "              \"query\": {\"multi_match\": {\n" + 
                    "          \"query\": \""+query+"\",\n" + 
                    "          \"fields\": [\"reports.RPT_TYPE_NAME\",\"reports.PRD_TYPE_NAME\",\"reports.STATUS\"],\n" + 
                    "          \"type\": \"phrase\"\n" + 
                    "        }}}}"+
                    ",        {\"nested\": {\n" + 
                    "              \"path\": \"reports.SECTOR_DETAILS\",\n" + 
                    "              \"query\": {\"multi_match\": {\n" + 
                    "          \"query\": \""+query+"\",\n" + 
                    "          \"fields\": [\"reports.SECTOR_DETAILS.SECTOR\"],\n" + 
                    "          \"type\": \"phrase\"\n" + 
                    "        }}}}");
                    }
                else {
                    queryString.append(
                    "{\n" + 
                    "    \"simple_query_string\": {\n" + 
                    "      \"query\": \""+query+"\",\n" + 
                    "      \"fields\": [\n" + 
                    "        \"award_nbr^3\"\n" + 
                    "        ,\"awardee_acronym^2\"\n" + 
                    "        ,\"awardee^2\"\n" + 
                    "        ,\"pname^2\"\n" + 
                    "        ,\"region_name^2\"\n" + 
                    "        ,\"cname^2\"\n" + 
                    "        \n" + 
                    "        ,\"ofda_division_code^2\"\n" + 
                    "        ,\"ofda_team_code^2\"\n" + 
                    "        ,\"aor_name^2\"\n" + 
                    "        \n" + 
                    "        ,\"award_status^2\"\n" + 
                    "        ,\"region_code\"\n" + 
                    "        ,\"ccode\"\n" + 
                    "        ,\"pnbr\"\n" + 
                    "        ]\n" + 
                    "    }\n" + 
                    "  }"+
                    ",          {\n" + 
                    "            \"nested\": {\n" + 
                    "              \"path\": \"reports\",\n" + 
                    "              \"query\": {\n" + 
                    "              \"simple_query_string\": {\n" + 
                    "              \"query\": \""+query+"\",\n" + 
                    "          \"fields\": [\"reports.RPT_TYPE_NAME\",\"reports.PRD_TYPE_NAME\",\"reports.STATUS\"]\n" +  
                    "            }\n" + 
                    "              }\n" + 
                    "            }\n" + 
                    "\n" + 
                    "        }"+
                    ",          {\n" + 
                    "            \"nested\": {\n" + 
                    "              \"path\": \"reports.SECTOR_DETAILS\",\n" + 
                    "              \"query\": {\n" + 
                    "              \"simple_query_string\": {\n" + 
                    "              \"query\": \""+query+"\",\n" + 
                    "              \"fields\": [\"reports.SECTOR_DETAILS.SECTOR\"]\n" + 
                    "            }\n" + 
                    "              }\n" + 
                    "            }\n" + 
                    "\n" + 
                    "        }");
                    }
                }
            String sortOrder=sitem.getSortOrder();
            StringBuilder sortList=new StringBuilder();
            for(SortItem s:sitem.getSelectedSortList()) {
                if(sortList.length()>0)
                    sortList.append(",");
                    sortList.append("{\n" + 
                     "                     \""+s.getValue()+"\": {\n" + 
                     "                       \"order\": \""+s.getSortValue()+"\"\n" + 
                     "                     }\n" + 
                     "                   }\n" );
            }
            restClient=RestClient.builder(
            new HttpHost(ES_IP, ES_PORT, ES_PROTOCOL))
            .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
                        @Override
                        public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                            return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                        }
                    })
                .build();
            HttpEntity entity = new NStringEntity(
             "{\n" +
             "\"from\": "+(sitem.getFrom()>=0?sitem.getFrom():0)+", \n"+
             "  \"size\": "+sitem.getSize()+", \n  " +
             "  \"query\": { \n" + 
             "    \"bool\": { \n" + 
            (query!=null && query.length()>0?"\"minimum_should_match\": 1,":"\n") + 
             "      \"should\": [\n" + 
             (query!=null && query.length()>0?queryString.toString():"\n") + 
             "      ],\n" + 
             "      \"filter\": [ \n" + 
             sb.toString()+
             "]\n" + 
             "    }\n" + 
             "  }"+
             ",\n" +
             (sitem.isShowAggregations()?"  \"aggs\": {"+
             getAggregateQuery(sitem)+
             ",\"NON_COMPLIANT_COUNT\":{\n" + 
             "      \"filter\": {\n" + 
             "        \"bool\": {\n" + 
             "          \"must\": [\n" + 
             postFilter[0].toString()+
             "          ]\n" + 
             "        }\n" + 
             "      },\n" + 
             "    \"aggs\":{\n" + 
             "    \"NON_COMPLIANT_COUNT\": {\n" + 
             "      \"nested\": {\n" + 
             "        \"path\": \"reports\"\n" + 
             "      }, \n" + 
             "      \"aggs\": {\n" + 
             "        \"NON_COMPLIANT_COUNT\":{\n" + 
             "          \"filter\": {\n" + 
             "                \"bool\": {\n" + 
             "                  \"must\": [\n" + 
             "                    {\n" + 
             "                      \"terms\": {\n" + 
             "                        \"reports.IS_COMPLIANT.keyword\": [\"No\"]\n" + 
             "                      }\n" + 
             "                    }\n" + 
            (postFilter[1].length()>0?","+postFilter[1].toString():"")+
            (postFilter[2].length()>0?
                ",     { \"nested\": {\n" + 
                "        \"path\": \"reports.SECTOR_DETAILS\",\n" + 
                "        \"query\": {\n" + 
                "            \"bool\": {\n" + 
                "              \"must\":[\n" + postFilter[2]+
                "                ]\n" + 
                "            } \n" + 
                "        }\n" + 
                "      }}":"")+
             "                  ]\n" + 
             "                }\n" + 
             "              },\n" + 
             "        \"aggs\":{\n" + 
             "        \"NON_COMPLIANT_COUNT\": {\n" + 
             "                \"cardinality\": {\n" + 
             "        \"field\": \"reports.AWARD_ID\",\n" + 
             "        \"precision_threshold\": 40000\n" + 
             "      }\n" + 
             "        }}}\n" + 
             "      }\n" + 
             "    }}}"+
                                         "},":"")+
             "  \"post_filter\": {\n" + 
             "    \"bool\": {\n" + 
             "              \"must\":\n" + 
             "              [\n" + 
             finalPostFilter.toString()+
             "              ] \n" + 
             "            }\n" + 
             "                 }"+
             (!sitem.isShowInnerHits() ||postFilter[1].length()>0?",\"_source\": {\n" + 
             "    \"excludes\": [\"reports\"]\n" + 
             "    }":"")+
             (query!=null && query.length()>0 ?"":("                 ,\n" + 
             "                 \"sort\": \n" + 
             "                 [\n" + 
             sortList.toString()+
             "                 ]"))+
             "}", ContentType.APPLICATION_JSON);
            Response response=restClient.performRequest("GET", "/awardreport/_search",Collections.singletonMap("pretty", "true"), entity);
            String jsonString=EntityUtils.toString(response.getEntity());
            JSONObject json = null;
            json=new JSONObject(jsonString);
            List<Award> awardList=new ArrayList<Award>();
            JSONObject hits=json.getJSONObject("hits");
            total=hits.getLong("total");
            if(sitem.isShowAggregations())
            {
            JSONObject aggs = json.getJSONObject("aggregations");
                Map<String,AggregationSearchItem> aggSearchList=sitem.getAggSearchList();
                for(Map.Entry<String,AggregationSearchItem> aggEntry: aggSearchList.entrySet()) {
                    AggregationSearchItem asi=aggEntry.getValue();
                    JSONObject buckets=null;
                    Integer count=0;
                    if(asi.getLevel()==0)
                    {
                        count=aggs.getJSONObject(asi.getTerm()).getInt("doc_count");
                        buckets=aggs.getJSONObject(asi.getTerm()).getJSONObject(asi.getTerm());

                    }
                    else if(asi.getLevel()==1)
                    {
                        count=aggs.getJSONObject(asi.getTerm()).getJSONObject(asi.getTerm()).getJSONObject(asi.getTerm()).getInt("doc_count");
                        buckets=aggs.getJSONObject(asi.getTerm()).getJSONObject(asi.getTerm()).getJSONObject(asi.getTerm()).getJSONObject(asi.getTerm());

                    }
                    else if(asi.getLevel()==2)
                    {
                        count=aggs.getJSONObject(asi.getTerm()).getJSONObject(asi.getTerm()).getJSONObject(asi.getTerm()).getJSONObject(asi.getTerm()).getInt("doc_count");
                        buckets=aggs.getJSONObject(asi.getTerm()).getJSONObject(asi.getTerm()).getJSONObject(asi.getTerm()).getJSONObject(asi.getTerm()).getJSONObject(asi.getTerm());

                    }
                    JSONArray aggsArray = null;
                    List<BucketItem> blist=new ArrayList<BucketItem>();
                    aggsArray=buckets.getJSONArray("buckets");
                    Map<String,List<TermItem>>tfMap=sitem.getTfMap();
                    List<TermItem> l=tfMap.get(aggEntry.getKey());
                    for (int j=0; j<aggsArray.length(); j++) {
                      JSONObject h = aggsArray.getJSONObject(j);
                      BucketItem bitem=new BucketItem();
                     if(asi.getIsLabelKeyword())
                      bitem.setKey(h.getString("key"));
                     else
                      bitem.setKey(""+h.getLong("key"));  
                     if(h.has("key_as_string"))
                         bitem.setKey_as_string(""+h.getString("key_as_string"));
                      bitem.setDoc_count(h.getLong("doc_count"));  
                      if(asi.getTotalBeneficiaries()!=null || asi.getTotalIDPBeneficiaries()!=null) {
                          JSONObject bene=null;
                          if(asi.getLevel()==0)
                            bene= aggsArray.getJSONObject(j).getJSONObject("Beneficiaries").getJSONObject("Beneficiaries");
                          else
                            bene=aggsArray.getJSONObject(j);
                          if(asi.getTotalBeneficiaries()!=null)
                            bitem.setTotal_reached(bene.getJSONObject("TOTAL_REACHED").getLong("value"));
                          if(asi.getTotalIDPBeneficiaries()!=null)
                            bitem.setIdp_reached(bene.getJSONObject("IDP_REACHED").getLong("value"));
                      }
                       
                      if(l!=null && l.contains(new TermItem(aggEntry.getKey(),aggEntry.getKey(),bitem.getKey(),bitem.getKey())))
                          bitem.setSelected(true);
                      blist.add(bitem);
                     }
                    AggregationResultItem ari=new AggregationResultItem(asi.getName(),asi.getTerm());
                    ari.setHidden(asi.getHidden());
                    ari.setDisclosed(asi.getDisclosed());
                    ari.setResultDisplayOrder(asi.getResultDisplayOrder());
                    ari.setLevel(asi.getLevel());
                    ari.setCount(count);
                    ari.setList(blist);
                AggregationResultItem tmp=sitem.getRitem().getAggregateList().get(asi.getTerm());
                if(tmp!=null) {
                    if(tmp.getDisplayCount()!=5)
                        ari.setDisplayCount(ari.getList().size());
                    ari.setDisclosed(tmp.getDisclosed());
                }
                ritem.getAggregateList().put(asi.getTerm(), ari);
            }

                    AggregationResultItem cari=ritem.getAggregateList().get("IS_COMPLIANT");
                    if(cari!=null) {
                        Long reportsRequired=new Long(0);
                        Long reportsCompliant=new Long(0);
                        List<BucketItem> bList=cari.getList();
                        for(BucketItem b:bList) {
                            if("Yes".equals(b.getKey()))
                               reportsCompliant=b.getDoc_count();
                            if(!"NA".equals(b.getKey()))
                               reportsRequired+=b.getDoc_count();
                                
                        }
                        ritem.setCompliantReports(reportsCompliant);
                        ritem.setRequiredReports(reportsRequired);
                        Float per=new Float(reportsCompliant);
                        per=(per/reportsRequired)*100;
                        if(reportsRequired>0)
                            ritem.setCompliantReportPercent(Math.round(per));
                        else
                            ritem.setCompliantReportPercent(0);
                        
                        }
                /*****************************Award Compliance*******************************/
                Integer nonCompliantAwards=aggs.getJSONObject("NON_COMPLIANT_COUNT").getJSONObject("NON_COMPLIANT_COUNT").getJSONObject("NON_COMPLIANT_COUNT").getJSONObject("NON_COMPLIANT_COUNT").getInt("value");
                Integer awardsCompliant=total.intValue() - nonCompliantAwards;
                if(awardsCompliant<0)
                    awardsCompliant=0;
                ritem.setCompliantAwards(awardsCompliant);
                Float per=new Float(awardsCompliant);
                per=(per/total)*100;
                if(total>0)
                    ritem.setCompliantAwardPercent(Math.round(per));
                else
                    ritem.setCompliantAwardPercent(0);
                /*****************************Award Compliance*******************************/
            }
                    /**********************************Get Results************************/
                    JSONArray hitsArray=hits.getJSONArray("hits");
                    for(int i=0;i<hitsArray.length();i++) {
                        JSONObject h = hitsArray.getJSONObject(i);
                        JSONObject tt=h.getJSONObject("_source");
                        Award a=new Gson().fromJson(tt.toString(), Award.class);
                        if(h.has("inner_hits")) {
                            List<Report> reportList=new ArrayList<Report>();
                            JSONArray innerHitsArray=h.getJSONObject("inner_hits").getJSONObject("reports").getJSONObject("hits").getJSONArray("hits");
                            for(int j=0;j<innerHitsArray.length();j++) {
                                JSONObject itt=innerHitsArray.getJSONObject(j).getJSONObject("_source");
                                Report r=new Gson().fromJson(itt.toString(), Report.class);
                                reportList.add(r);
                            }
                          a.setReports(reportList);
                        }
                        else if(a.getReports()!=null){
                            Collections.sort(a.getReports(),new ReportDueDateComparator());
                        }
                        awardList.add(a);                        
                }
            ritem.setAwardList(awardList);
            ritem.setTotal(total);
            restClient.close();
        }
        catch(Exception e) {
            logger.log(ADFLogger.ERROR,"ES"+e.toString()+e.getCause());
        }
        finally{
            if(restClient!=null)
                try{
              restClient.close();
                }
                catch(Exception e){}
        }
        return ritem;
    }
    public ResultItem getNextARTResultSet(SearchItem sitem) {
        return new ResultItem();
    }
    private String getAggregateQuery(SearchItem sitem) {
        StringBuilder sb=new StringBuilder();
        StringBuilder fsb[]=new StringBuilder[3];
        
        Map<String,AggregationSearchItem> aggSearchList=sitem.getAggSearchList();
        for(Map.Entry<String,AggregationSearchItem> aggEntry: aggSearchList.entrySet()) {
            fsb[0]=new StringBuilder();
            fsb[1]=new StringBuilder();
            fsb[2]=new StringBuilder();
            String name=aggEntry.getKey();
            AggregationSearchItem asi=aggEntry.getValue();
            if(sb.length()>0)
              sb.append(",\n");
            sb.append("\""+name+"\": {");
            if(sitem.getTermFilters()!=null)
            {
                Map<String,List<TermItem>>tfMap=sitem.getTfMap();
                for(Map.Entry<String,List<TermItem>> entry: tfMap.entrySet()) {
                    if(!name.equals(entry.getKey()) && !asi.getIgnoreList().contains(entry.getKey()))
                    {
                     int level=sitem.getAggSearchList().get(entry.getKey()).getLevel();
                     Boolean isDate=sitem.getAggSearchList().get(entry.getKey()).getIsDate();
                     String interval=sitem.getAggSearchList().get(entry.getKey()).getInterval();
                     StringBuilder postFilter=fsb[level];
                     if(postFilter.length()>0) {
                        postFilter.append(",\n"); 
                     }
                     if(isDate) {
                         postFilter.append(
                         "                      {\n" + 
                         "                        \"bool\":{\"should\":[\n"); 
                         List<TermItem> termlist=entry.getValue();
                         for(int i=0;i<termlist.size();i++) {
                             postFilter.append(
                             "                          {\n" + 
                             "                            \"range\":\n" + 
                             "                            {\"");
                             if(level==1)
                                 postFilter.append("reports.");
                             else if(level==2)
                                 postFilter.append("reports.SECTOR_DETAILS");
                             postFilter.append(entry.getKey()+"\":{");
                             postFilter.append(
                             "                              \"gte\":\""+termlist.get(i).getValue()+"\",\n" + 
                             "                              \"lt\":\""+termlist.get(i).getValue()+"||+"+interval+"\"\n" + 
                             "                            }}\n" + 
                             "                          }");
                         if(i!=termlist.size()-1)
                              postFilter.append(","); 
                         }
                         postFilter.append(
                         "                          ]}\n" + 
                         "                      }");
                     }
                     else
                     {
                         postFilter.append("{\"terms\":{\"");
                         if(level==1)
                             postFilter.append("reports.");
                         else if(level==2)
                             postFilter.append("reports.SECTOR_DETAILS.");
                         postFilter.append(entry.getKey());
                         if(sitem.getAggSearchList().get(entry.getKey()).getIsTermKeyword())
                            postFilter.append(".keyword");
                         postFilter.append("\":[");
                         List<TermItem> termlist=entry.getValue();
                         for(int i=0;i<termlist.size();i++) {
                           postFilter.append("\""+termlist.get(i).getValue()+"\"");
                         if(i!=termlist.size()-1)
                              postFilter.append(","); 
                        }
                        postFilter.append("]}}");
                     }
                    }
                }
            }
            if(asi.getOtherRestriction()!=null && asi.getOtherRestriction().length()>0) {
                if(fsb[asi.getLevel()].length()>0) {
                    fsb[asi.getLevel()].append(",");
                }
                fsb[asi.getLevel()].append(asi.getOtherRestriction());
            }
                sb.append("\"filter\": {\n" + 
                "        \"bool\": {\n" + 
                "          \"must\":[\n" + 
                fsb[0].toString());
                if(asi.getLevel()==0 &&fsb[1].length()>0) {
                    if(fsb[0].length()>0)
                    sb.append(",");
                    sb.append("{ \"nested\": {\n" + 
                    "        \"path\": \"reports\",\n" + 
                    "        \"query\": {\n" + 
                    "            \"bool\": {\n" + 
                    "              \"must\":[");
                    sb.append(fsb[1]);
                    if(fsb[2].length()>0) {
                        sb.append(",{ \"nested\": {\n" + 
                        "        \"path\": \"reports.SECTOR_DETAILS\",\n" + 
                        "        \"query\": {\n" + 
                        "            \"bool\": {\n" + 
                        "              \"must\":[");
                        sb.append(fsb[2]);
                        sb.append("              ]\n" + 
                        "            } \n" + 
                        "        }\n" + 
                        "      }}");
                    }
                    sb.append("              ]\n" + 
                    "            } \n" + 
                    "        }\n" + 
                    "      }}");
                }
               if(asi.getLevel()<2 &&fsb[1].length()==0 && fsb[2].length()>0) {
                   if(fsb[0].length()>0)
                    sb.append(",");
                       sb.append("{ \"nested\": {\n" + 
                       "        \"path\": \"reports\",\n" + 
                       "        \"query\": {\n" + 
                       "            \"bool\": {\n" + 
                       "              \"must\":[");
                   sb.append("{ \"nested\": {\n" + 
                    "        \"path\": \"reports.SECTOR_DETAILS\",\n" + 
                    "        \"query\": {\n" + 
                    "            \"bool\": {\n" + 
                    "              \"must\":[");
                   sb.append(fsb[2]);
                   sb.append("              ]\n" + 
                    "            } \n" + 
                    "        }\n" + 
                    "      }}");
                       sb.append("              ]\n" + 
                        "            } \n" + 
                        "        }\n" + 
                        "      }}");
               }
                sb.append("                 ]\n" + 
                "        }\n" + 
                "      },");
            sb.append("\"aggs\":{");
            sb.append("\""+name+"\": {");
            if(asi.getLevel()>0) {
                String path=asi.getPath();
                if(asi.getLevel()>1) {
                    path=asi.getPath().split("\\.")[0];
                }
                sb.append("" +
                "\"nested\": {\n" + 
                "        \"path\": \""+path+"\"\n" + 
                "      },");
                sb.append("\"aggs\":{");
                sb.append("\""+name+"\": {"); 
                   sb.append(
                   "          \"filter\": {\n" + 
                   "              \"bool\": {\n" + 
                   "              \"must\":["); 
                   sb.append(fsb[1].toString());
                   if(fsb[2].length()>0) {
                       if(fsb[1].length()>0)
                           sb.append(",");
                       sb.append("{ \"nested\": {\n" + 
                        "        \"path\": \"reports.SECTOR_DETAILS\",\n" + 
                        "        \"query\": {\n" + 
                        "            \"bool\": {\n" + 
                        "              \"must\":[");
                       sb.append(fsb[2]);
                       sb.append("              ]\n" + 
                        "            } \n" + 
                        "        }\n" + 
                        "      }}");
                   }
                   sb.append(
                   "                ]\n" + 
                   "            } \n" + 
                   "          },");
                if(asi.getLevel()==2)
                { 
                    sb.append(
                    "        \"aggs\": {\n" + 
                    "          \""+name+"\": {\n" + 
                    "            \"nested\": {\n" + 
                    "              \"path\": \""+asi.getPath()+"\"\n" + 
                    "            }, ");
                }
                sb.append("\"aggs\":{");
                sb.append("\""+name+"\": {"); 
                if(asi.getIsDate()) {
                    sb.append("      \"date_histogram\": {\n");
                    sb.append(
                    "        \"interval\": \""+asi.getInterval()+"\",\n" + 
                    "        \"format\":\"MMM-yyyy\",\n" + 
                    "        \"min_doc_count\": 1,");
                }
                else 
                {
                 sb.append("      \"terms\": {\n"); 
                }
                sb.append("        \"field\": \""+asi.getPath()+"."+asi.getTermLabel());
                if(asi.getIsLabelKeyword())
                    sb.append(".keyword");
                sb.append("\",\n" + 
                (asi.getIsDate()?"":"        \"size\": "+asi.getSize()+",\n") + 
                "        \"order\": {\n" + 
                "          \""+asi.getOrderBy()+"\": \""+asi.getSortOrder()+"\"\n" + 
                "        }}");
                if(asi.getTotalBeneficiaries()!=null|| asi.getTotalIDPBeneficiaries()!=null) {
                  sb.append(",\n" + 
                  "            \"aggs\": {\n" + 
                  "                  \"TOTAL_REACHED\": {\n" + 
                  "                    \"sum\": {\n" + 
                  "                      \"field\": \""+asi.getPath()+"."+asi.getTotalBeneficiaries()+"\"\n" + 
                  "                    }\n" + 
                  "                  },\n" + 
                  "                 \"IDP_REACHED\": {\n" + 
                  "                    \"sum\": {\n" + 
                  "                      \"field\": \""+asi.getPath()+"."+asi.getTotalIDPBeneficiaries()+"\"\n" + 
                  "                    }\n" + 
                  "          }\n" + 
                  "        }");
                }
                sb.append("\n}");
                sb.append("\n}");  
                if(asi.getLevel()==2) {
                    sb.append("\n}");
                    sb.append("\n}"); 
                }
                sb.append("\n}");
                sb.append("\n}");    
               
            }
            else {
                sb.append(
                "      \"terms\": {\n" + 
                "        \"field\": \""+asi.getTermLabel());
                if(asi.getIsLabelKeyword())
                    sb.append(".keyword");
                sb.append("\",\n" + 
                "        \"size\": "+asi.getSize()+",\n" + 
                "        \"order\": {\n" + 
                "          \""+asi.getOrderBy()+"\": \""+asi.getSortOrder()+"\"\n" + 
                "        }}");
                if(asi.getTotalBeneficiaries()!=null|| asi.getTotalIDPBeneficiaries()!=null) {
                  sb.append(",\n" + 
                  "        \"aggs\": {\n" + 
                  "          \"Beneficiaries\": {\n" + 
                  "            \"nested\": {\n" + 
                  "              \"path\": \"reports\"\n" + 
                  "            }, \n" +
                  "            \"aggs\": {\n" + 
                  "              \"Beneficiaries\":{\n" + 
                  "                         \"filter\": {\n" + 
                  "                          \"bool\": {\n" + 
                  "                            \"must\":[\n" + fsb[1].toString()+
                  "                                    ]\n" + 
                  "                                  } \n" + 
                  "                          } ,"+
                  "            \"aggs\": {\n" + 
                  "                  \"TOTAL_REACHED\": {\n" + 
                  "                    \"sum\": {\n" + 
                  "                      \"field\": \"reports."+asi.getTotalBeneficiaries()+"\"\n" + 
                  "                    }\n" + 
                  "                  },\n" + 
                  "                 \"IDP_REACHED\": {\n" + 
                  "                    \"sum\": {\n" + 
                  "                      \"field\": \"reports."+asi.getTotalIDPBeneficiaries()+"\"\n" + 
                  "                    }\n" + 
                  "                  }\n" + 
                  "                }\n" + 
                  "          }\n" + 
                  "         } }\n" + 
                  "        }");  
                }
            }
            
            sb.append("\n}");
            sb.append("\n}");
            sb.append("\n}");
            
        }
        
        return sb.toString();
    }
    public Boolean updateArticle(Award a) {
        if(a!=null)
        {
            try(RestClient restClient=RestClient.builder(
                    new HttpHost(ES_IP, ES_PORT, ES_PROTOCOL))
                .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
                            @Override
                            public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                                return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                            }
                        })
                .build();) {
                
                Gson gson = new GsonBuilder()
                                   .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
                Response indexResponse = restClient.performRequest(
                        "PUT",
                        "/awardreport/awardreport/"+a.getAwid(),
                        Collections.<String, String>emptyMap(),
                        new NStringEntity(gson.toJson(a),ContentType.APPLICATION_JSON));
            }
            catch(IOException e){
                logger.log(ADFLogger.ERROR,"ES"+e.toString()+e.getCause());
                return false;
            }
        }
        return true;
    }
  public Boolean updateUserDashboardPreference(UserPreference udpl) {
      try (          RestClient restClient=RestClient.builder(
              new HttpHost(ES_IP, ES_PORT, ES_PROTOCOL))
          .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
                      @Override
                      public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                          return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                      }
                  })
          .build();){
          
          Gson gson = new GsonBuilder()
                             .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
          Response indexResponse = restClient.performRequest(
                  "PUT",
                  "/art-dashboard-user-pref/art-dashboard-user-pref/"+udpl.getUsername(),
                  Collections.<String, String>emptyMap(),
                  new NStringEntity(gson.toJson(udpl),ContentType.APPLICATION_JSON));
      }
      catch(IOException e){
          logger.log(ADFLogger.ERROR,"ES"+e.toString()+e.getCause());
          return false;
      }
      return true;
  }
public UserPreference getUserDashboardPreference() {
               RestClient restClient=null;
               try {
                   String username = ADFContext.getCurrent().getSecurityContext().getUserName().toUpperCase();
                   if(username!=null)
                   {
                   restClient=RestClient.builder(
                       new HttpHost(ES_IP, ES_PORT, ES_PROTOCOL)).setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
                        @Override
                        public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                            return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                        }
                    })
                .build();
                   Response response=restClient.performRequest("GET", "/art-dashboard-user-pref/art-dashboard-user-pref/"+username,Collections.singletonMap("pretty", "true"));
                   String jsonString=EntityUtils.toString(response.getEntity());
                   JSONObject json=new JSONObject(jsonString);
                   JSONObject source=json.getJSONObject("_source");
                   UserPreference udpl=new Gson().fromJson(source.toString(), UserPreference.class);
                   restClient.close();
                       SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
                       String userType="partner";
                       if(sc.isUserInRole("admin"))
                           userType="admin";
                       else if(sc.isUserInRole("ofda")||sc.isUserInRole("APP_CONNECT"))
                               userType="ofda";
                    if(userType.equals(udpl.getUserType()))
                        return udpl;
                    else
                        return null;
                   
                   }
               }
               catch(IOException e){
                   logger.log(ADFLogger.WARNING,"ES"+e.toString()+e.getCause());
                   return null;
               }
               finally{
                   if(restClient!=null)
                       try{
                     restClient.close();
                       }
                       catch(Exception e){
                               logger.log(ADFLogger.ERROR,"ES"+e.toString()+e.getCause());
                               return null;
                           }
               }
        return null;
    }
    public Boolean refreshES(List<Award> list) {
        if(list!=null)
        {
            try (RestClient restClient=RestClient.builder(
                    new HttpHost(ES_IP, ES_PORT, ES_PROTOCOL))
                .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
                            @Override
                            public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                                return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                            }
                        })
                .build();) {
                
                Gson gson = new GsonBuilder()
                                   .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
                for(Award a:list)
                {
                restClient.performRequest(
                        "PUT",
                        "/awardreport/awardreport/"+a.getAwid(),
                        Collections.<String, String>emptyMap(),
                        new NStringEntity(gson.toJson(a),ContentType.APPLICATION_JSON));
                }
            }
            catch(IOException e){
                logger.log(ADFLogger.ERROR,"ES"+e.toString()+e.getCause());
            }
        }
        return true;
    }
}
