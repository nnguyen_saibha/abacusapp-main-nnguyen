package gov.ofda.abacus.portal.controller;

import gov.ofda.abacus.portal.model.services.AwardModuleImpl;

import java.io.IOException;
import java.io.InputStream;

import java.io.UnsupportedEncodingException;

import java.sql.Blob;

import java.sql.SQLException;

import java.text.DecimalFormat;

import java.util.Iterator;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.model.binding.DCControlBinding;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.input.RichTextEditor;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGridLayout;
import oracle.adf.view.rich.component.rich.layout.RichToolbar;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;

import oracle.jbo.client.Configuration;
import oracle.jbo.domain.BlobDomain;

import org.apache.myfaces.trinidad.model.UploadedFile;

/**
 * UploadFile.java
 * Purpose: Used by manageReportFlow to add, edit or view documents in database.
 *
 * @author Kartheek Atluri
 */
public class UploadFile extends UIControl {
    private UploadedFile file;
    private UploadedFile newFile;
    private String filelength = "";
    private Row docRow;
    private RichSelectOneChoice docTypeCode;
    private RichTable docsTable;
    private RichPanelFormLayout docsForm;
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.portal.controller.UploadFile.class);
    private RichToolbar docsToolbar;
    private RichTextEditor docsDesc;
    private RichPanelGridLayout updatedGrid;


    public UploadFile() {
    }


    /**
     * Called when user updates existing document.
     * This function will update the row with new document details and content.
     * @param file
     */
    public void setFile(UploadedFile file) {
        this.file = file;
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCControlBinding adocs_ctrl = bindings.findCtrlBinding("AbacusxDocsView1");
        Row r = adocs_ctrl.getCurrentRow();
        if (r != null && this.getFile() != null) {
            UploadedFile myfile = this.getFile();
            String filename = myfile.getFilename();
            String content = myfile.getContentType();
            long fsize = myfile.getLength();
            logger.log(logger.NOTIFICATION, "file details: " + filename + content + fsize);
            BlobDomain nfile = null;
            try {
                InputStream istream = myfile.getInputStream();
                byte[] buffer = new byte[(int)myfile.getLength()];
                for (int i = 0; i < fsize; i++) {
                    istream.read(buffer, i, 1);
                }
                nfile = new BlobDomain(buffer);
                r.setAttribute("FileContents", nfile);
                r.setAttribute("FileName", filename);
                r.setAttribute("FileContentType", content);
                r.setAttribute("FileFormat", content);
                r.setAttribute("FileSize", fsize);
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "File Uploaded", "File uploaded successfully. Please save changes");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            } catch (Exception e) {
                logger.log(logger.ERROR, e.toString());
            }

            AdfFacesContext.getCurrentInstance().addPartialTarget(docsTable);
            AdfFacesContext.getCurrentInstance().addPartialTarget(docsForm);
        }
        this.file=null;
    }

    public UploadedFile getFile() {
        return file;
    }


    /**
     * Called from af:fileDownloadActionListener.
     * af:setActionListener sets the current row to docRow.
     * This method then uses information in this row and sends the document
     * @param facesContext
     * @param outputStream
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    public void fileToDownload(FacesContext facesContext,
                               java.io.OutputStream outputStream) throws UnsupportedEncodingException, IOException {
        HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
        response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        if (docRow != null)
            try {
                byte[] content;
                content = null;
                BlobDomain bd = (BlobDomain)docRow.getAttribute("FileContents");
                content = bd.toByteArray();
                ServletOutputStream out;
                response.setHeader("Content-disposition",
                                   "attachment;filename=\"" + docRow.getAttribute("FileName") + "\"");
                out = response.getOutputStream();
                out.write(content);
                out.flush();

            } catch (IOException ex) {
                ex.printStackTrace();
            }
    }

    public void setFilelength(String filelength) {
        this.filelength = filelength;
    }

    /**
     * Calculates teh File Length in KB using FileSize attribute in AbacusxDocsView table.
     * @return File length in KB
     */
    public String getFilelength() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCControlBinding adocs_ctrl = bindings.findCtrlBinding("AbacusxDocsView1");
        Row r = adocs_ctrl.getCurrentRow();
        if (r != null && r.getAttribute("FileSize") != null) {
            long fsize = Long.parseLong(r.getAttribute("FileSize").toString());
            double tmp = ((double)fsize) / 1024;
            if(tmp<1)
                filelength="Less than 1 KB";
            else if(tmp<1024)
            {
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            filelength = formatter.format(tmp) + " KB";
            }
            else {
                tmp=tmp/1024;
                DecimalFormat formatter = new DecimalFormat("#,###,###.##");
                filelength = formatter.format(tmp) + " MB";
            }
        }
        return filelength;
    }

    public void setDocRow(Row docRow) {
        this.docRow = docRow;
    }

    public Row getDocRow() {
        return docRow;
    }

    public void setDocTypeCode(RichSelectOneChoice docTypeCode) {
        this.docTypeCode = docTypeCode;
    }

    public RichSelectOneChoice getDocTypeCode() {
        return docTypeCode;
    }

    /**
     * Called when user click on OK in new document popup dialog.
     * Will check if doc type code and file is not null and if so then
     * calls createDoc method in AwardModuleImpl which will insert new record and details into the table
     * @param dialogEvent
     */
    public void newDocument(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            if (docTypeCode.getValue() != null && newFile != null) {
                DCBindingContainer bindings =
                    (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding adocs_create = bindings.getOperationBinding("createDoc");
                adocs_create.execute();
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "File Uploaded", "File uploaded successfully. Please save changes");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                logger.log(logger.NOTIFICATION,
                           "file details: " + newFile.getFilename() + " " + newFile.getContentType() + " " +
                           newFile.getLength());
               // OperationBinding exec = bindings.getOperationBinding("Commit");
               // exec.execute();
                AdfFacesContext.getCurrentInstance().addPartialTarget(docsTable);
                AdfFacesContext.getCurrentInstance().addPartialTarget(docsForm);
                AdfFacesContext.getCurrentInstance().addPartialTarget(updatedGrid);
                //AdfFacesContext.getCurrentInstance().addPartialTarget(docsForm);
                //saveEditReportDoc();
                newFile=null;
            }
        }
    }

    public void setNewFile(UploadedFile newFile) {
        this.newFile = newFile;
    }

    public UploadedFile getNewFile() {
        return newFile;
    }

    public void setDocsTable(RichTable docsTable) {
        this.docsTable = docsTable;
    }

    public RichTable getDocsTable() {
        return docsTable;
    }

    public void setDocsForm(RichPanelFormLayout docsForm) {
        this.docsForm = docsForm;
    }

    public RichPanelFormLayout getDocsForm() {
        return docsForm;
    }


    public void deleteDocument(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = bindings.getOperationBinding("DeleteDocument");
            exec.execute();
           // exec = bindings.getOperationBinding("Commit");
           // exec.execute();
            if (exec.getErrors().isEmpty()) {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Document Deleted", "Document deleted successfully. Please save changes");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
            
            AdfFacesContext.getCurrentInstance().addPartialTarget(docsTable);
            AdfFacesContext.getCurrentInstance().addPartialTarget(docsForm);
            AdfFacesContext.getCurrentInstance().addPartialTarget(docsToolbar);
            AdfFacesContext.getCurrentInstance().addPartialTarget(docsDesc);
            AdfFacesContext.getCurrentInstance().addPartialTarget(updatedGrid);
            
            
        }

    }


    /**
     * This method is used by the awardDocuments UI to download the file using the af:fileDownloadActionListener.
     * af:setActionListener sets the current row to docRow.
     * This method then uses information in this row and sends the document
     * @param facesContext
     * @param outputStream
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    public void awardfileToDownload(FacesContext facesContext,
                                    java.io.OutputStream outputStream) throws UnsupportedEncodingException,
                                                                              IOException {
        HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
        response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        if (selectedDocument != null && fileName != null)
            try {
                byte[] content;
                content = null;
                
                int length = (int)selectedDocument.length();
                int buffersize = 1024;
                byte[] buffer = new byte[buffersize];
                InputStream is = selectedDocument.getBinaryStream();
                ServletOutputStream out;
                response.setHeader("Content-disposition",
                                   "attachment;filename=\"" + fileName + "\"");
                out = response.getOutputStream();
                while ((length = is.read(buffer)) != -1) {
                    out.write(buffer, 0, length);
                }
                is.close();
                out.flush();

            } catch (IOException ex) {
                logger.log(logger.ERROR, "IO Exception in Download File  ", ex.getCause());
                ex.printStackTrace();
            } catch (SQLException e) {
                logger.log(logger.ERROR, "SQL Exception in Download File  ", e.getCause());
            }
    }

    private Blob selectedDocument;
    
    private String fileName;

    public void setSelectedDocument(Blob selectedDocument) {
        this.selectedDocument = selectedDocument;
    }

    public Blob getSelectedDocument() {
        return selectedDocument;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setDocsToolbar(RichToolbar docsToolbar) {
        this.docsToolbar = docsToolbar;
    }

    public RichToolbar getDocsToolbar() {
        return docsToolbar;
    }

    public void setDocsDesc(RichTextEditor docsDesc) {
        this.docsDesc = docsDesc;
    }

    public RichTextEditor getDocsDesc() {
        return docsDesc;
    }

    public void setUpdatedGrid(RichPanelGridLayout updatedGrid) {
        this.updatedGrid = updatedGrid;
    }

    public RichPanelGridLayout getUpdatedGrid() {
        return updatedGrid;
    }
}
