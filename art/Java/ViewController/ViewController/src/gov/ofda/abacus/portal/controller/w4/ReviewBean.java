package gov.ofda.abacus.portal.controller.w4;


import gov.ofda.abacus.portal.controller.TrainTemplateBean;
import gov.ofda.abacus.portal.controller.UIControl;

import java.io.Serializable;

import java.util.Date;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.util.ComponentReference;

public class ReviewBean extends UIControl implements Serializable{
    private static ADFLogger logger = ADFLogger.createADFLogger(ReviewBean.class);
    @SuppressWarnings("compatibility:6163820036948723427")
    private static final long serialVersionUID = 1L;
    private ComponentReference submitPopup;
    private ComponentReference validatePanelGroup;
    private ComponentReference recallPopup;

    public ReviewBean() {
        super();
    }

    public void setSubmitPopup(UIComponent submitPopup) {
        this.submitPopup = ComponentReference.newUIComponentReference(submitPopup);
    }

    public UIComponent getSubmitPopup() {
        return submitPopup == null ? null : submitPopup.getComponent();
    }

    public void completePopupListener(DialogEvent dialogEvent) {
        W4TemplateBean pageFlowScopeInstance = null;
        logger.log(logger.TRACE, "In Report complete action: " + dialogEvent.getOutcome());
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("Par4wView2Iterator");
        iter.executeQuery();
        Row rw = iter.getCurrentRow();
        rw.setAttribute("IsComplete", "Y");
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            rw.setAttribute("IsSubmit", "Y");
            SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
            String username = sc.getUserName().toUpperCase();
            rw.setAttribute("SubmitBy", username);
            rw.setAttribute("SubmitDate", new Date());
        }
        RichPopup popup = (RichPopup)this.getSubmitPopup();
        popup.hide();
        pageFlowScopeInstance = retrievePageFlowScopeTrainTemplateBean();
        pageFlowScopeInstance.saveChanges();
        if (validatePanelGroup != null) {
            AdfFacesContext.getCurrentInstance().addPartialTarget(validatePanelGroup.getComponent());
        }
        pageFlowScopeInstance.refresh();
        logger.log(logger.TRACE, "Report Changed to complete");
    }

    public W4TemplateBean retrievePageFlowScopeTrainTemplateBean() {
        FacesContext fctx = FacesContext.getCurrentInstance();
        Application application = fctx.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext context = fctx.getELContext();
        ValueExpression createValueExpression =
            expressionFactory.createValueExpression(context, "#{pageFlowScope.W4TemplateBean}",
                                                    W4TemplateBean.class);
        W4TemplateBean pageFlowScopeInstance = (W4TemplateBean)createValueExpression.getValue(context);
        return pageFlowScopeInstance;
    }

    public void setValidatePanelGroup(UIComponent validatePanelGroup) {
        this.validatePanelGroup = ComponentReference.newUIComponentReference(validatePanelGroup);
    }

    public UIComponent getValidatePanelGroup() {
        return validatePanelGroup == null ? null : validatePanelGroup.getComponent();
    }

    public String reportComplete() {
        RichPopup popup = (RichPopup)this.getSubmitPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
        return null;
    }

    public String changeReportToDraft() {
        W4TemplateBean pageFlowScopeInstance = null;
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("Par4wView2Iterator");
        iter.executeQuery();
        Row rw = iter.getCurrentRow();
        rw.setAttribute("IsComplete", "N");
        rw.setAttribute("SubmitBy", null);
        rw.setAttribute("SubmitDate", null);
        rw.setAttribute("IsSubmit", "N");
        pageFlowScopeInstance = retrievePageFlowScopeTrainTemplateBean();
        pageFlowScopeInstance.saveChanges();

        if (validatePanelGroup != null) {
            AdfFacesContext.getCurrentInstance().addPartialTarget(validatePanelGroup.getComponent());
        }
        pageFlowScopeInstance.refresh();
        return null;
    }
    
    public void processRecallReport(DialogEvent dialogEvent) {
        RichPopup popup = (RichPopup)this.getRecallPopup();
        popup.hide();
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            logger.log(logger.TRACE, "In recall function");
            this.changeReportToDraft();
        }
        
        
    }
    
    /**
     * This function return true if the latest submitted report is equal to the current report.
     * It internally uses appx_pkg.latest_Sumbitted_Report function in AwardModule.
     * a) All completed reports(but not submitted) can be recalled.
     * b) For submitted report, last submitted report should match the parseq 
     * @return
     */
    public Boolean getRecallApplicable() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("Par4wView2Iterator");
        Row rw = iter.getCurrentRow();
        OperationBinding ctrl = bindings.getOperationBinding("getLatestSubmittedReport");
        ctrl.getParamsMap().put("pnbr", rw.getAttribute("Pnbr").toString());
        ctrl.getParamsMap().put("awardeeCode", rw.getAttribute("AwardeeCode").toString()); 
        ctrl.getParamsMap().put("parseq", rw.getAttribute("Par4wSeq").toString());
        Integer tmp = (Integer)ctrl.execute();       
        if (rw != null && (rw.getAttribute("IsComplete").toString().equals("Y") && rw.getAttribute("IsSubmit").toString().equals("N")  ))
            return false;
        if (rw != null && rw.getAttribute("Par4wSeq").toString().equals("" + tmp))
            return false;        
        return true;
    }

    public void setRecallPopup(UIComponent recallPopup) {
        this.recallPopup = ComponentReference.newUIComponentReference(recallPopup);
    }

    public UIComponent getRecallPopup() {
        return recallPopup == null ? null : recallPopup.getComponent();
    }
}
