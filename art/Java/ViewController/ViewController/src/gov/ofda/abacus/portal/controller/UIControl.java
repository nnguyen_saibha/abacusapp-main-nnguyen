package gov.ofda.abacus.portal.controller;

import gov.ofda.abacus.portal.model.domain.Sector;

import gov.ofda.abacus.portal.pojo.BenefitingArea;

import java.util.Iterator;
import java.util.List;

import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;

import javax.faces.model.SelectItem;

import javax.servlet.http.HttpServletRequest;

import oracle.adf.controller.ControllerContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.component.rich.data.RichTreeTable;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.event.QueryEvent;
import oracle.adf.view.rich.model.FilterableQueryDescriptor;

import oracle.binding.OperationBinding;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Key;
import oracle.jbo.RowNotFoundException;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import oracle.jbo.uicli.binding.JUCtrlHierTypeBinding;
import oracle.jbo.uicli.binding.JUIteratorBinding;

import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.ComponentReference;
import org.apache.myfaces.trinidad.util.Service;

/**
 * UIControl.java
 * Purpose: Used as to provide common methods that all backing beans use by extending this class.
 *
 * @author Kartheek Atluri
 */

public class UIControl {
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.portal.controller.UIControl.class);
    public enum GrowlType {
        error, 
        notice, 
        warning,
        mailok,
        mailerror
    }
    public UIControl() {
        super();
    }

    /**
     * Returns Row's Key of the selected row in table t
     * @param t
     * @return Key of selected row
     */
    public Key getRowKey(RichTable t) {
      Object _selectedRowData1 = t.getSelectedRowData();
              JUCtrlHierNodeBinding _nodeBinding1 = (JUCtrlHierNodeBinding)_selectedRowData1;
              if(_nodeBinding1!=null)
              return(_nodeBinding1.getRowKey());
   return null;           
  }

    /**
     * Sets the selected row in table t to the row with key k
     * @param t
     * @param k
     */
    public void setCurrentRow(RichTable t, Key k) {
          CollectionModel _tableModle1 = (CollectionModel)t.getValue();
          JUCtrlHierBinding _tableBinding1 = (JUCtrlHierBinding)_tableModle1.getWrappedData();
          DCIteratorBinding _tableIteratorBinding1 = _tableBinding1.getDCIteratorBinding();
          
      try
      {
        _tableIteratorBinding1.setCurrentRowWithKey(k.toStringFormat(true));
      }
      catch(RowNotFoundException e) {
          logger.log(logger.TRACE," Record deleted");
      }
      catch(Exception e) {
          logger.log(logger.TRACE," Record deleted");
      }
   return;
  }

    /**
     * Return if the current task flow is dirty (unsaved changes).
     * @return
     */
    public boolean isDataDirty() {
      ControllerContext ctx=ControllerContext.getInstance();
      return ctx.getCurrentViewPort().getTaskFlowContext().isDataDirty();
  }
    /**
     * Custom managed bean method that takes a SelectEvent input argument
     * to generically set the current row corresponding to the selected row
     * in the tree. Note that this method is a way to replace "makeCurrent"
     * EL expression (#{bindings.<tree binding>. treeModel.makeCurrent}that
     * Oracle JDeveloper adds to the tree component SelectionListener
     * property when dragging a collection from the Data Controls panel.
     * Using this custom selection listener allows developers to add pre-
     * and post processing instructions. For example, you may enforce PPR
     * on a specific item after a new tree node has been  selected. This
     * methods performs the following steps
     *
     * i.   get access to the tree component
     * ii. get access to the ADF tree binding
     * iii. set the current row on the ADF binding
     * iv. get the information about target iterators to synchronize
     * v.   synchronize target iterator
     *
     * @param selectionEvent object passed in by ADF Faces when configuring
     *  this method to become the selection listener
     *
     * @author Frank Nimphius
     */
    public String onTreeTableSelect(SelectionEvent selectionEvent) {

        RichTreeTable tree1 = (RichTreeTable)selectionEvent.getSource();
        RowKeySet rks2 = selectionEvent.getAddedSet();
        String nodeStuctureDefname = null;
        //iterate over the contained keys. Though for a single selection use
        //case we only expect one entry in here
        Iterator rksIterator = rks2.iterator();
        //support single row selection case
        if (rksIterator.hasNext()) {
            //get the tree node key, which is a List of path entries describing
            //the location of the node in the tree including its parents nodes
            List key = (List)rksIterator.next();
            //get the ADF tree binding to work with
            JUCtrlHierBinding treeBinding = null;
            //The Trinidad CollectionModel is used to provide data to trees and
            //tables. In the ADF binding case, it contains the tree binding as
            //wrapped data
            treeBinding = (JUCtrlHierBinding)((CollectionModel)tree1.getValue()).getWrappedData();
            //find the node identified by the node path from the ADF binding
            //layer. Note that we don't need to know about the name of the tree
            //binding in the PageDef file because
            //all information is provided
            JUCtrlHierNodeBinding nodeBinding = nodeBinding = treeBinding.findNodeByKeyPath(key);
            //the current row is set on the iterator binding. Because all
            //bindings have an internal reference to their iterator usage,
            //the iterator can be queried from the ADF binding object
            DCIteratorBinding _treeIteratorBinding = null;
            _treeIteratorBinding = treeBinding.getDCIteratorBinding();
            JUIteratorBinding iterator = nodeBinding.getIteratorBinding();
            String keyStr = nodeBinding.getRowKey().toStringFormat(true);
            iterator.setCurrentRowWithKey(keyStr);
            //get selected node type information
            JUCtrlHierTypeBinding typeBinding = nodeBinding.getHierTypeBinding();
            //The tree node rule may have a target iterator defined. Target
            //iterators are configured using the Target Data Source entry in
            //the tree node edit dialog
            //and allow developers to declaratively synchronize an independent
            //iterator binding with the node selection in the tree.
            //
            nodeStuctureDefname = nodeBinding.getHierTypeBinding().getStructureDefName();
            String targetIteratorSpelString = typeBinding.getTargetIterator();

            //chances are that the target iterator option is not configured. We
            //avoid NPE by checking this condition

            if (targetIteratorSpelString != null && !targetIteratorSpelString.isEmpty()) {

                //resolve SPEL string for target iterator
                DCIteratorBinding targetIterator = resolveTargetIterWithSpel(targetIteratorSpelString);
                //synchronize the row in the target iterator
                Key rowKey = nodeBinding.getCurrentRow().getKey();
                if (rowKey != null && targetIterator != null) {
                    targetIterator.setCurrentRowWithKey(rowKey.toStringFormat(true));
                }

            }
        }
        return nodeStuctureDefname;
    }

    private DCIteratorBinding resolveTargetIterWithSpel(String spelExpr) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ELContext elctx = fctx.getELContext();
        ExpressionFactory elFactory = fctx.getApplication().getExpressionFactory();
        ValueExpression valueExpr = elFactory.createValueExpression(elctx, spelExpr, Object.class);
        DCIteratorBinding dciter = (DCIteratorBinding)valueExpr.getValue(elctx);
        return dciter;
    }

    /**
     * Custom managed bean method that takes a SelectEvent input argument
     * to generically set the current row corresponding to the selected row
     * in the table.
     * @param selectionEvent
     */
    public static void makeCurrent(SelectionEvent selectionEvent) {
        RichTable _table = (RichTable)selectionEvent.getSource();
        CollectionModel _tableModel = (CollectionModel)_table.getValue();
        JUCtrlHierBinding _adfTableBinding = (JUCtrlHierBinding)_tableModel.getWrappedData();
        DCIteratorBinding _tableIteratorBinding = _adfTableBinding.getDCIteratorBinding();
        Object _selectedRowData = _table.getSelectedRowData();
        JUCtrlHierNodeBinding _nodeBinding = (JUCtrlHierNodeBinding)_selectedRowData;
        Key _rwKey = _nodeBinding.getRowKey();
        _tableIteratorBinding.setCurrentRowWithKey(_rwKey.toStringFormat(true));
    }

    /**
     * @param expr
     * @param rtnType
     * @param argTypes
     * @param argValues
     * @return
     * http://fusionsuperhero.blogspot.com/
     */
    public static Object resolveMethodExpression(String expr, Class rtnType,Class[] argTypes,Object[]argValues) {
       FacesContext fc = FacesContext.getCurrentInstance();
       Application app = fc.getApplication();
       ExpressionFactory elFactory = app.getExpressionFactory();
       ELContext elContext = fc.getELContext();
       MethodExpression methodExpression =
           elFactory.createMethodExpression(elContext, expr, rtnType,argTypes);
     return(methodExpression.invoke(elContext, argValues));
   }
    
    /**
     * @param phaseEvent
     */
    public void beforePhaseMethod(PhaseEvent phaseEvent) {
        //only perform action if RENDER_RESPONSE phase is reached
        try {
            if (phaseEvent.getPhaseId() == PhaseId.RENDER_RESPONSE) {
                FacesContext fctx = FacesContext.getCurrentInstance();
                //check internal request parameter
                Map requestMap = fctx.getExternalContext().getRequestMap();
                Object showPrintableBehavior = requestMap.get("oracle.adfinternal.view.faces.el.PrintablePage");
                if (showPrintableBehavior != null) {
                    if (Boolean.TRUE == showPrintableBehavior) {
                        ExtendedRenderKitService erks = null;
                        erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
                        //invoke JavaScript from the server
                        erks.addScript(fctx, "window.print();");
                    }
                }
            }
        } catch (Exception e) {
            logger.warning("In Before phase Exception: ", e);
        }

    }    
    public static boolean contains(List<Object> list,Object iCode) {
         boolean b = list.contains(iCode);
         return b;
     }
    public void clearFilter(RichTable t) {
        if(t!=null)
        {
        FilterableQueryDescriptor queryDescriptor = (FilterableQueryDescriptor)  t.getFilterModel();
        if (queryDescriptor != null && queryDescriptor.getFilterCriteria() != null){
            queryDescriptor.getFilterCriteria().clear();
        t.queueEvent(new QueryEvent(t, queryDescriptor));
        }
        }
    }
    public static int size(List<Object> list) {
        if(list!=null)
         return list.size();
        return 0;
     }
    public String getNameFromOneChoice(RichSelectOneChoice rsoc) {
        List childList = rsoc.getChildren();
               for (int i = 0; i < childList.size(); i++) {
                   if (childList.get(i) instanceof UISelectItems) {
                       UISelectItems csi = (UISelectItems)childList.get(i);
                       List l = (List)csi.getValue();
                for(int j=0;j<l.size();j++)
                       {
                           SelectItem si = (SelectItem)l.get(j);
                           if((rsoc.getValue().toString().equals(si.getValue().toString())))
                            return ((si.getLabel()).replace('/', '_').replace(' ','_'));
                                
                       }
                   }
               }
        return "";
    }
    
    public static boolean containsSector(List<Sector> list,String code) {
        Sector t=new Sector(code,null);
         boolean b = list.contains(t);
         return b;
     }
    
    public static boolean containsBenefitingArea(List<BenefitingArea> list,String code) {
        BenefitingArea t=new BenefitingArea(code,null,null,null,null);
         boolean b = list.contains(t);
         return b;
     }
    
    /**
    * Programmatic evaluation of EL.
    *
    * @param el EL to evaluate
    * @return Result of the evaluation
    */
    public static Object evaluateEL(String el) {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    ELContext elContext = facesContext.getELContext();
    ExpressionFactory expressionFactory =
    facesContext.getApplication().getExpressionFactory();
    ValueExpression exp =
    expressionFactory.createValueExpression(elContext, el,
    Object.class);

    return exp.getValue(elContext);
    }
    
    
    /**
    * Sets a value into an EL object. Provides similar functionality to
    * the <af:setActionListener> tag, except the from is
    * not an EL. You can get similar behavior by using the following...

    * setEL(to, evaluateEL(from))
    *
    * @param el EL object to assign a value
    * @param val Value to assign
    */
    public static void setEL(String el, Object val) {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    ELContext elContext = facesContext.getELContext();
    ExpressionFactory expressionFactory =
    facesContext.getApplication().getExpressionFactory();
    ValueExpression exp =
    expressionFactory.createValueExpression(elContext, el,
    Object.class);

    exp.setValue(elContext, val);
    }
    
    
    public  Object invokeMethod(String methodName) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding method = bindings.getOperationBinding(methodName);
        return  method.execute(); 
    }
    public void addToGrowl(GrowlType type, String message, int delay,int duration) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
        writeJavaScriptToClient("setTimeout(function(){$.growl."+type+"({ message: \""+message+"\", duration: "+duration+",context: \""+request.getContextPath()+"\"});},"+delay+");");
    }
    public void writeJavaScriptToClient(String script) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExtendedRenderKitService erks = null;
        erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
        erks.addScript(fctx, script);
    }
    public void autoCloseMessage(String clientID) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExtendedRenderKitService erks = Service.getRenderKitService(ctx, ExtendedRenderKitService.class);
         StringBuilder builder= new StringBuilder();
         builder.append("setTimeout(function(){AdfPage.PAGE.clearMessages('"+clientID+"');},1234)");
         erks.addScript(ctx, builder.toString());
    }
}
