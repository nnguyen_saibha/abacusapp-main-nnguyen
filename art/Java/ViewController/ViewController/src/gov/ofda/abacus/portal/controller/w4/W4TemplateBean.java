package gov.ofda.abacus.portal.controller.w4;

import gov.ofda.abacus.portal.controller.UIControl;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class W4TemplateBean extends UIControl implements Serializable{
    private static ADFLogger logger =
        ADFLogger.createADFLogger(W4TemplateBean.class);
    @SuppressWarnings("compatibility:-6974495347950674160")
    private static final long serialVersionUID = 1L;

    public W4TemplateBean() {
        super();
    }
    
    
    public String saveChanges() {
        logger.log("Save Changes");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec_commit = bindings.getOperationBinding("Commit");
        exec_commit.execute();
        if (exec_commit.getErrors().isEmpty()) {            
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Successful", "Changes saved successfully");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        refresh();

        return null;
    }

    public void refresh() {
        logger.log("Refresh Changes");
        Key kPar4w=getKey("Par4wView2Iterator");
        Key kPar4wi=getKey("Par4wIndicatorView1Iterator");
        Key kPar4wisl=getKey("Par4wiLocationView1Iterator");
        Key kPar4wdocs=getKey("Par4wDocsView1Iterator");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec=bindings.getOperationBinding("ExecutePar4w");
        exec.execute();
        setKey("Par4wView2Iterator",kPar4w);
        
        exec=bindings.getOperationBinding("ExecutePar4wi");
        exec.execute();
        setKey("Par4wIndicatorView1Iterator",kPar4wi);
        
        exec=bindings.getOperationBinding("ExecutePar4wil");
        exec.execute();
        setKey("Par4wiLocationView1Iterator",kPar4wisl);
        
        exec=bindings.getOperationBinding("ExecutePar4widocs");
        exec.execute();
        setKey("Par4wDocsView1Iterator",kPar4wdocs);
        
        logger.log("Before retrieve Changes");
        DCIteratorBinding iter = bindings.findIteratorBinding("Par4wView2Iterator");
        Row rw = iter.getCurrentRow();
        OperationBinding ctrl = bindings.getOperationBinding("retriveIncompletePAR4W");
        ctrl.getParamsMap().put("par4wSeq", rw.getAttribute("Par4wSeq").toString());
        ctrl.execute();
        
    }
 private Key getKey(String iteratorName) {
     DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
     DCIteratorBinding iter = (DCIteratorBinding)bindings.findIteratorBinding(iteratorName);
     Key key=null;
     if (iter != null && iter.getCurrentRow() != null)
         key = iter.getCurrentRow().getKey();
     return key;
 }
private void setKey(String iteratorName,Key key) {
    DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
    DCIteratorBinding iter = (DCIteratorBinding)bindings.findIteratorBinding(iteratorName);
    if(key!=null && iter!=null) {
        
        iter = (DCIteratorBinding)bindings.findIteratorBinding(iteratorName);
        RowSetIterator rsi = iter.getRowSetIterator();
        if (rsi.findByKey(key, 1).length > 0)
            iter.setCurrentRowWithKey(key.toStringFormat(true));
    }
}
    
    public String saveAndValidateChanges() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec_commit = bindings.getOperationBinding("Commit");
        exec_commit.execute();
        if (exec_commit.getErrors().isEmpty()) {
            OperationBinding exec = bindings.getOperationBinding("retriveIncompletePAR4W");
            exec.execute();            
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Successful", "Changes saved successfully and Validated Changes");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

        return null;
    }

    public String cancelChanges() {
        Key kPar4w=getKey("Par4wView2Iterator");
        Key kPar4wi=getKey("Par4wIndicatorView1Iterator");
        Key kPar4wisl=getKey("Par4wiLocationView1Iterator");
        Key kPar4wdocs=getKey("Par4wDocsView1Iterator");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec1 = bindings.getOperationBinding("Rollback");
        exec1.execute();
        if (exec1.getErrors().isEmpty()) {
            if (FacesContext.getCurrentInstance().getMessageList().size() == 0) {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancel Successful", "Changes cancelled successfully");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        OperationBinding exec=bindings.getOperationBinding("ExecutePar4w");
        exec.execute();
        setKey("Par4wView2Iterator",kPar4w);
        
        exec=bindings.getOperationBinding("ExecutePar4wi");
        exec.execute();
        setKey("Par4wIndicatorView1Iterator",kPar4wi);
        
        
        exec=bindings.getOperationBinding("ExecutePar4wil");
        exec.execute();
        setKey("Par4wiLocationView1Iterator",kPar4wisl);
        
        exec=bindings.getOperationBinding("ExecutePar4widocs");
        exec.execute();
        setKey("Par4wDocsView1Iterator",kPar4wdocs);
        }
        return null;
    }
}
