package gov.ofda.abacus.portal.pojo.Comparator;

import gov.ofda.abacus.portal.pojo.UserDashboardPreference;

import java.util.Comparator;

public class UserDashboardPreferenceComparator implements Comparator<UserDashboardPreference>{
    public UserDashboardPreferenceComparator() {
        super();
    }
    @Override
    public int compare(UserDashboardPreference one, UserDashboardPreference two) {
           if(one!=null)
            return (one.getPreferenceName().toLowerCase().compareTo(two.getPreferenceName().toLowerCase()));
           return 0;
        
    }
}
