package gov.ofda.abacus.portal.pojo.Comparator;

import gov.ofda.abacus.portal.controller.elasticsearch.BucketItem;

import java.util.Comparator;

public class BucketItemTotalReachedComparator implements Comparator<BucketItem>{
    public BucketItemTotalReachedComparator() {
        super();
    }

    @Override
    public int compare(BucketItem bucketItem, BucketItem bucketItem2) {
        return (int) (bucketItem2.getTotal_reached() - bucketItem.getTotal_reached());
    }
}
