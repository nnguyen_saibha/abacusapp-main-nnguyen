package gov.ofda.abacus.portal.controller;

import gov.ofda.abacus.portal.model.ResultMessage;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import java.util.Hashtable;

import javax.faces.application.FacesMessage;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.*;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.render.ClientEvent;

import oracle.jbo.Key;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

/**
 * ManageUsers.java
 * Purpose: Used by manageUsers.jsff as a backing bean to manage users data.
 *
 * @author Kartheek Atluri
 *
 */
public class ManageUsers extends UIControl {
    private RichInputText newPwd;
    private RichInputText confirmNewPwd;
    private RichOutputText username;
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.portal.controller.ManageUsers.class);
    private RichPopup editUserPopup;
    private RichTable userTable;
    private RichSelectOneChoice awardee;

    public ManageUsers() {
    }

    /**
     * Calls saveUserChanges function in PortalServiceImpl to create any new partner users in OID
     * and then will call Commit to save all changes to database.
     * @return
     */
    public String saveChanges() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();

        OperationBinding oper = bindings.getOperationBinding("saveUserChanges");
        ResultMessage msg = (ResultMessage)oper.execute();
        if(msg.isResult())
        {
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg.getMessage(), null));
        OperationBinding exec = bindings.getOperationBinding("Commit");
        exec.execute();
        Key key = this.getRowKey(userTable);
        exec = bindings.getOperationBinding("Execute");
        exec.execute();
        this.setCurrentRow(userTable, key);
        RichPopup popup = this.getEditUserPopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(userTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(editUserPopup);
        }
        else
        {
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg.getMessage(), null));
            OperationBinding exec = bindings.getOperationBinding("Rollback");
            exec.execute();
            RichPopup popup = this.getEditUserPopup();
            popup.hide();
            AdfFacesContext.getCurrentInstance().addPartialTarget(userTable);
            AdfFacesContext.getCurrentInstance().addPartialTarget(editUserPopup);
        }
        return null;
    }


    public String manageUser() {
        String manageUserID =
            (String)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("manageUserID");
        String manageOperation =
            (String)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("manageOperation");
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding operationBinding = bindings.getOperationBinding("manageOIMUser");
        operationBinding.getParamsMap().put("operation", manageOperation);
        operationBinding.getParamsMap().put("userID", manageUserID);
        FacesContext fc = FacesContext.getCurrentInstance();
        String rm = (String)operationBinding.execute();
        /*UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
         UIComponent component=viewRoot.findComponent("pt1:r1:0:pc1:p3");
         RichPopup popup=(RichPopup)component;
         component = viewRoot.findComponent("pt1:r1:0:pc1:p3:ofinfo1");
         RichOutputFormatted rotext=(RichOutputFormatted)component;
         rotext.setValue(rm);
         RichPopup.PopupHints ph = new RichPopup.PopupHints();
         popup.show(ph);*/
        fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, rm, null));
        return null;
    }

    public void setNewPwd(RichInputText newPwd) {
        this.newPwd = newPwd;
    }

    public RichInputText getNewPwd() {
        return newPwd;
    }

    public void setConfirmNewPwd(RichInputText confirmNewPwd) {
        this.confirmNewPwd = confirmNewPwd;
    }

    public RichInputText getConfirmNewPwd() {
        return confirmNewPwd;
    }

    public void setUsername(RichOutputText username) {
        this.username = username;
    }

    public RichOutputText getUsername() {
        return username;
    }


    public void setEditUserPopup(RichPopup editUserPopup) {
        this.editUserPopup = editUserPopup;
    }

    public RichPopup getEditUserPopup() {
        return editUserPopup;
    }

    public void setUserTable(RichTable userTable) {
        this.userTable = userTable;
    }

    public RichTable getUserTable() {
        return userTable;
    }

    public void handleTableDoubleClick(ClientEvent clientEvent) {
        RichPopup popup = this.getEditUserPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
    }

    public String cancelChanges() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        Key key = this.getRowKey(userTable);
        //  Key formKey = this.getRowKey(userFormTable);
        OperationBinding exec = bindings.getOperationBinding("Rollback");
        exec.execute();
        this.setCurrentRow(userTable, key);
        //   this.setCurrentRow(userFormTable, formKey);
        RichPopup popup = this.getEditUserPopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(userTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(editUserPopup);

        return null;
    }

    public String newUser() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding oper1 = bindings.getOperationBinding("CreateWithParams");
        oper1.execute();
        RichPopup popup = this.getEditUserPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
        return null;
    }

    public void setAwardee(RichSelectOneChoice awardee) {
        this.awardee = awardee;
    }

    public RichSelectOneChoice getAwardee() {
        return awardee;
    }

    public void userTypeChgLsnr(ValueChangeEvent valueChangeEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ManageAppxUserView1Iterator");
        Row rw = iter.getCurrentRow();
        rw.setAttribute("AwardeeCode", null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(awardee);
        AdfFacesContext.getCurrentInstance().addPartialTarget(userTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(editUserPopup);

    }

    public void deleteUserDialog(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            String manageUserID =
                (String)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("manageUserID");
            System.out.println(":::" + manageUserID);
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding operationBinding = bindings.getOperationBinding("manageOIMUser");
            operationBinding.getParamsMap().put("operation", "DELETE_USER");
            operationBinding.getParamsMap().put("userID", manageUserID);
            FacesContext fc = FacesContext.getCurrentInstance();
            String rm = (String)operationBinding.execute();
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, rm, null));
            OperationBinding exec = bindings.getOperationBinding("Commit");
            exec.execute();
            Key key = this.getRowKey(userTable);
            exec = bindings.getOperationBinding("Execute");
            exec.execute();
            this.setCurrentRow(userTable, key);
            AdfFacesContext.getCurrentInstance().addPartialTarget(userTable);
            AdfFacesContext.getCurrentInstance().addPartialTarget(editUserPopup);
        }
    }
}
