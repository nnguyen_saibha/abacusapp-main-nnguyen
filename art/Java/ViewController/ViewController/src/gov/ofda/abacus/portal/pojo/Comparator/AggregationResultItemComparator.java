package gov.ofda.abacus.portal.pojo.Comparator;

import gov.ofda.abacus.portal.controller.elasticsearch.AggregationResultItem;

import java.util.Comparator;

public class AggregationResultItemComparator  implements Comparator<AggregationResultItem>{
    public AggregationResultItemComparator() {
        super();
    }
    @Override
    public int compare(AggregationResultItem aggregationResultItem, AggregationResultItem aggregationResultItem2) {
        return aggregationResultItem.getResultDisplayOrder().compareTo(aggregationResultItem2.getResultDisplayOrder());
    }
}
