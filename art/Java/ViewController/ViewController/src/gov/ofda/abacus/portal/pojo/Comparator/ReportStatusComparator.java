package gov.ofda.abacus.portal.pojo.Comparator;

import gov.ofda.abacus.portal.controller.elasticsearch.BucketItem;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ReportStatusComparator implements Comparator<BucketItem>{
    public ReportStatusComparator() {
        super();
    }
    private static final Map<String,Integer> l;
    static{
    Map<String,Integer> tmp=new HashMap<String,Integer>();
         tmp.put("Submitted On Time",1);
         tmp.put("Submitted Late",2);
         tmp.put("Due within 30 Days",3);
         tmp.put("Over Due",4);
         tmp.put("Not Due",5);
        tmp.put("Not Tracked",6);
         l=Collections.unmodifiableMap(tmp);
    }
    @Override
    public int compare(BucketItem bucketItem, BucketItem bucketItem2) {
        Integer item1=l.get(bucketItem.getKey());
        Integer item2=l.get(bucketItem2.getKey());
        if(item1!=null && item2!=null)
            return item1.compareTo(item2);
        return (int) (bucketItem.getKey().compareTo(bucketItem2.getKey()));
    }
}
