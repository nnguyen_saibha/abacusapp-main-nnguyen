package gov.ofda.abacus.portal.controller.elasticsearch;


import gov.ofda.abacus.portal.pojo.Comparator.AwardStatusComparator;
import gov.ofda.abacus.portal.pojo.Comparator.BucketItemIDPReachedComparator;
import gov.ofda.abacus.portal.pojo.Comparator.BucketItemTotalReachedComparator;

import gov.ofda.abacus.portal.pojo.Comparator.ReportFrequencyComparator;
import gov.ofda.abacus.portal.pojo.Comparator.ReportStatusComparator;

import gov.ofda.abacus.portal.pojo.Comparator.ReportTypeComparator;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class AggregationResultItem implements Serializable {
    @SuppressWarnings("compatibility:-2422779580327636489")
    private static final long serialVersionUID = 1L;
    private String name;
    private String columnName;
    private Integer displayCount=5;
    private Integer sortOrder=1;
    private Boolean disclosed=true;
    private Integer count;
    private List<BucketItem> list=new ArrayList<BucketItem>();
    private List<BucketItem> sortedList=null;
    private Integer resultDisplayOrder=1;
    private Boolean hidden=false;
    private Integer level=0;
    public AggregationResultItem(String name,String columnName) {
        super();
        this.name=name;
        this.columnName=columnName;
    }
    public AggregationResultItem() {
        super();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getLevel() {
        return level;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setList(List<BucketItem> list) {
        this.list = list;
        switch(columnName) {
        case "RPT_TYPE_NAME":
            Collections.sort(this.list,new ReportTypeComparator());
            break;
        case "PRD_TYPE_NAME":
            Collections.sort(this.list,new ReportFrequencyComparator());
            break;
        case "award_status":
            Collections.sort(this.list,new AwardStatusComparator());
            break;
        case "STATUS":
            Collections.sort(this.list,new ReportStatusComparator());
            break;
        default:
        }         
        if(sortedList!=null) {
            sortedList.clear();
            sortedList.addAll(list);
            Collections.sort(sortedList,new BucketItemTotalReachedComparator());
        }
    }

    public List<BucketItem> getList() {
        return list;
    }
    

    public void setDisplayCount(Integer displayCount) {
        if(displayCount>this.getList().size()) {
            this.displayCount = this.getList().size();
        }
        else if(displayCount<5)
                this.displayCount=5;
        else
            this.displayCount = displayCount;
    }

    public Integer getDisplayCount() {
        return displayCount;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }
    @Override
    public boolean equals(Object o) {
        if (o == this) {
                    return true;
                }
     if(o instanceof AggregationResultItem) {
         AggregationResultItem object=(AggregationResultItem)o;
         if(object.getColumnName()!=null && object.getColumnName().equals(this.getColumnName()))
            return true;
     }
     return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + columnName.hashCode();
        return result;
    }

    public void setDisclosed(Boolean disclosed) {
        this.disclosed = disclosed;
    }

    public Boolean getDisclosed() {
        return disclosed;
    }

    public void setDisplayCount1(Integer displayCount) {
        this.displayCount = displayCount;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }
    public List<BucketItem> getListSortedByReached() {
        if(sortedList==null)
        {
         sortedList=new ArrayList<BucketItem>();
         sortedList.addAll(list);
         Collections.sort(sortedList,new BucketItemTotalReachedComparator());
        }
        return sortedList;
    }
    public List<BucketItem> getListSortedByIDP() {
        List<BucketItem> sortedList=new ArrayList<BucketItem>();
        sortedList.addAll(list);
        Collections.sort(sortedList,new BucketItemIDPReachedComparator());
        return sortedList;
    }

    public void setResultDisplayOrder(Integer resultDisplayOrder) {
        this.resultDisplayOrder = resultDisplayOrder;
    }

    public Integer getResultDisplayOrder() {
        return resultDisplayOrder;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Boolean getHidden() {
        return hidden;
    }
}
