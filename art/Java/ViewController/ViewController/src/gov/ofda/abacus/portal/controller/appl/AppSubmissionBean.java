package gov.ofda.abacus.portal.controller.appl;

import gov.ofda.abacus.portal.controller.JSFUtils;
import gov.ofda.abacus.portal.controller.Shuttle;
import gov.ofda.abacus.portal.controller.UIControl;
import gov.ofda.abacus.portal.controller.UIControl.GrowlType;
import gov.ofda.abacus.portal.controller.email.EmailMessageBean;
import gov.ofda.abacus.portal.pojo.ApplValidItem;
import gov.ofda.abacus.portal.pojo.BenefitingArea;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import java.math.BigDecimal;

import java.net.MalformedURLException;
import java.net.URL;

import java.sql.Blob;
import java.sql.SQLException;

import java.sql.Timestamp;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import java.util.Map;

import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
 

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.adf.view.rich.event.QueryEvent;
import oracle.adf.view.rich.model.FilterableQueryDescriptor;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.BlobDomain;
import oracle.jbo.domain.Number;

import org.apache.myfaces.trinidad.model.UploadedFile;
import org.apache.myfaces.trinidad.util.ComponentReference;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class AppSubmissionBean extends UIControl implements  Serializable{
    @SuppressWarnings("compatibility:6205369709484444452")
    private static final long serialVersionUID = 1L;
    private static ADFLogger logger = ADFLogger.createADFLogger(AppSubmissionBean.class);
    public static final String PARTNER_USER = "PARTNER";
    public static final String OFDA_USER = "OFDA";
    public static final String ADMIN_USER = "ADMIN";        
    private transient List<UploadedFile> uploadedFile;
    private transient Row docRow;
    
    private List<BenefitingArea> manageRCList = new ArrayList<BenefitingArea>();
    private ComponentReference rcListAddRemoveCol;
    private ComponentReference rcListSelectedListTable;
    private ComponentReference rcListTable;
    
    private List selectedDisaster;
    private List selectedHqTeam = new ArrayList();
    private BigDecimal oneHqTeam=null;
    private transient RichPopup recallPopup;
    private ApplValidItem applValidItem=new ApplValidItem();
    private Map appScope;
    private String isCp = "N";
    private String isAppl  = "N";

    public void setIsCp(String isCp) {
        this.isCp = isCp;
    }

    public String getIsCp() {
        return isCp;
    }

    public void setIsAppl(String isAppl) {
        this.isAppl = isAppl;
    }

    public String getIsAppl() {
        return isAppl;
    }
    
    
    public void setSelectedDisaster(List selectedDisaster) {
        this.selectedDisaster = selectedDisaster;
    }

    public List getSelectedDisaster() {
        this.selectedDisaster = Shuttle.getSelected("PaaDisasterView1Iterator", "DisasterCode");
        return this.selectedDisaster;
    }
    
    public List getAllDisaster() {
        return Shuttle.getAll("DisasterLookupLOV1Iterator",null , "DisasterCode",   "DisasterDescription");
    }
    
    
    public void disasterDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            Shuttle.setSelected(this.selectedDisaster, "PaaDisasterView1Iterator", "DisasterCode", "DeleteDisaster", "CreateInsertDisaster");
            AdfFacesContext.getCurrentInstance().addPartialTarget(dialogEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
        }
    }
    
    public void setSelectedHqTeam(List selectedHqTeam) {
         this.selectedHqTeam = selectedHqTeam;
    }

    public List getSelectedHqTeam() {
        this.selectedHqTeam = Shuttle.getSelected("PaaHqTeamView1Iterator", "HqTeamId");
        return this.selectedHqTeam;
    }
    
    public void applHqListValChgLsnr(ValueChangeEvent vce) {
        Shuttle.setSelected((List)vce.getNewValue(), "PaaHqTeamView1Iterator", "HqTeamId", "DeleteHqTeam", "CreateInsertHqTeam");
    }
    
    public List getAllHqList() {
        return Shuttle.getAll("HqTeamLookupLOV1Iterator",null , "HqTeamId",   "HqTeamName");
    }

    public void setManageRCList(List<BenefitingArea> manageRCList) {
        this.manageRCList = manageRCList;
    }

    public List<BenefitingArea> getManageRCList() {
        return manageRCList;
    }


    public AppSubmissionBean() {
        super();
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =bindingContext.findBindingContainer("gov_ofda_abacus_art_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getAppConstants");
        exec.execute();
        appScope = (Map<String, String>) exec.getResult();
    }
    public void setUploadedFile(List<UploadedFile> uploadedFile) {
        this.uploadedFile = uploadedFile;
    }
    public List<UploadedFile> getUploadedFile() {
        return uploadedFile;
    }
    
    public void setDocRow(Row docRow) {
        this.docRow = docRow;
    }

    public Row getDocRow() {
        return docRow;
    }
    
    public String initialize() throws SQLException {
        //Check Roles
        //Check access wrt awardee
        //Set View Permission
        //applType = "NEW" "CP" "ACP" "MOD" "EDIT"
        
        String p_awardee_code =  null;
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl=null;
        //Setting User Role for Navigator
        SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
        String userType=null;
        if(sc.isUserInRole("OFDAPP_APPL")) {
        userType=PARTNER_USER  ;
        }
        if(sc.isUserInRole("APP_CONNECT")) {
        userType=OFDA_USER  ;
        }
        if(sc.isUserInRole("admin")){
            userType=ADMIN_USER;
        }
        if(userType==null)
            return "back";
        OperationBinding execcp =bindings.getOperationBinding("ExecuteCPDetail") ;
        OperationBinding execappl =bindings.getOperationBinding("ExecuteAppDetail") ;
        //Restrict Appl if partner user
        String awardeeCode=null;
        if(userType.equals(PARTNER_USER))
            {
                ctrl= bindings.getOperationBinding("getCurrentUserAwardee");
                awardeeCode = (String) ctrl.execute();
                execcp.getParamsMap().put("p_awardee_code", awardeeCode);
                execappl.getParamsMap().put("p_awardee_code", awardeeCode);
            }
        if(userType.equals(OFDA_USER)) {
           // execcp.getParamsMap().put("p_is_submitted", "Y");
          //  execappl.getParamsMap().put("p_is_submitted", "Y");
        }
        execcp.execute();
        execappl.execute();

        DCIteratorBinding citer = bindings.findIteratorBinding("PaaConceptDetailView1Iterator");
        Row cpr = citer.getCurrentRow();
        DCIteratorBinding aiter = bindings.findIteratorBinding("PaaAppDetailView1Iterator");
        Row applr = aiter.getCurrentRow();
        if(cpr==null && applr==null)
            return "back";
        OperationBinding execstatus=bindings.getOperationBinding("ExecuteApplValidStatus") ;
        execstatus.execute();
        execstatus=bindings.getOperationBinding("getMissingDetails") ;
        applValidItem=(ApplValidItem)execstatus.execute();
        execstatus=bindings.getOperationBinding("setPaaCpDocTypeList") ;
        execstatus.execute();
        execstatus=bindings.getOperationBinding("setPaaDocTypeList") ;
        execstatus.execute();
        
        execstatus=bindings.getOperationBinding("ExecuteHqTeam") ;
        execstatus.execute();
        execstatus=bindings.getOperationBinding("ExecuteHQTeamLookup") ;
        execstatus.execute();
        
     return "next";
    } 
    
    public void createNewApplication(DialogEvent dialogEvent) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getApplication().getNavigationHandler().handleNavigation(context,null,"details");
    } 
    
    /**
     * This method will retrieve document(s) from Input file component add to database when user click on 'upload' button.
     * @param actionEvent
     */
    public void uploadDocuments(ActionEvent actionEvent) {
       
        List<UploadedFile> fileList = this.getUploadedFile();
        List<UploadedFile> removedDuplicateFilesLst = new ArrayList<UploadedFile>();
        if (fileList != null && fileList.size() > 0) {
            for (UploadedFile file : fileList) {
                boolean addFile = false;
                for (UploadedFile uploadedFile : removedDuplicateFilesLst) {
                    if (file.getFilename().equalsIgnoreCase(uploadedFile.getFilename())) {
                        addFile = true;
                    }
                }
                if (!addFile) {
                    removedDuplicateFilesLst.add(file);
                }
            }
        }
       
        if (fileList != null) {        
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding fileAttachExecute = bindings.getOperationBinding("uploadDocuments");
            fileAttachExecute.getParamsMap().put("files", removedDuplicateFilesLst);
            fileAttachExecute.execute();
           
            setUploadedFile(null);
         }
    
    }
   
    /**
     * Called from af:fileDownloadActionListener.
     * af:setActionListener sets the current row to docRow.
     * This method then uses information in this row and sends the document
     * @param facesContext
     * @param outputStreamfileToDownload
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    public void fileToDownload(FacesContext facesContext,
                               java.io.OutputStream outputStream) throws UnsupportedEncodingException, IOException {
        HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
        response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        if (docRow != null)
            try {
                byte[] content;
                content = null;
                if (null != docRow.getAttribute("FileContents")){
                BlobDomain bd = (BlobDomain)docRow.getAttribute("FileContents");
                content = bd.toByteArray();
                ServletOutputStream out;
                response.setHeader("Content-disposition",
                                   "attachment;filename=\"" + docRow.getAttribute("FileName") + "\"");
                out = response.getOutputStream();
                out.write(content);
                out.flush();
                }
                else {
                    ServletOutputStream out;
                    response.setHeader("Content-disposition",
                                       "attachment;filename=\"missing.txt\"");
                    out = response.getOutputStream();
                    out.write((""+docRow.getAttribute("FileName")+" has not been uploaded properly.").getBytes());
                    out.flush();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
    }

/**
 * ********************************Start Manage Region/Country******************************
 */  
    public void addRCToList(ActionEvent ae) {
        String code = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("code");
        String regionName = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("regionName");
        String countryName = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("countryName");
        String regionCode = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("regionCode");
        String countryCode = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("countryCode");
        BenefitingArea ba=new BenefitingArea(code,regionName,countryName,regionCode,countryCode);
        if (!manageRCList.contains(ba)) {
            manageRCList.add(ba);
        } else {
            manageRCList.remove(ba);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(rcListAddRemoveCol.getComponent());
        AdfFacesContext.getCurrentInstance().addPartialTarget(rcListSelectedListTable.getComponent());
        
    }
 
    public void manageRCDialogLsnr(DialogEvent dialogEvent) {
        
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
         
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding ctrl = bindings.getOperationBinding("updateRCList");
            ctrl.getParamsMap().put("rcList", this.getManageRCList());
            Boolean t = (Boolean) ctrl.execute();
            OperationBinding exec = bindings.getOperationBinding("ExecuteAppRC");
            exec.execute();
        }
        if(rcListTable!= null)
            clearFilter((RichTable)rcListTable.getComponent());
    }

    /**
     * @param popupFetchEvent
     * Called when Manage Location popup is fetched.
     * It will get the existing location list and add the selected location list.
     */
    public void manageRCFetchLsnr(PopupFetchEvent popupFetchEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding getList = bindings.getOperationBinding("getRCList");
        manageRCList = (List<BenefitingArea>) getList.execute(); 
    }
    
   
    /**
     * @param popupCanceledEvent
     * Called when popup is cancelled or closed.
     * It will clear the filter for select location table
     */
    public void manageRCCancelLsnr(PopupCanceledEvent popupCanceledEvent) {
        if(rcListTable!= null)
        clearFilter((RichTable)rcListTable.getComponent());
    }

    public void setRcListAddRemoveCol(UIComponent rcListAddRemoveCol) {
        this.rcListAddRemoveCol = ComponentReference.newUIComponentReference(rcListAddRemoveCol);
    }

    public UIComponent getRcListAddRemoveCol() {
        return rcListAddRemoveCol == null ? null : rcListAddRemoveCol.getComponent();        
    }

    public void setRcListSelectedListTable(UIComponent rcListSelectedListTable) {
        this.rcListSelectedListTable = ComponentReference.newUIComponentReference(rcListSelectedListTable);
    }

    public UIComponent getRcListSelectedListTable() {
        return rcListSelectedListTable == null ? null :rcListSelectedListTable.getComponent();
    }
    
    public void setRcListTable(UIComponent rcListTable) {
        this.rcListTable = ComponentReference.newUIComponentReference(rcListTable);;
    }

    public UIComponent getRcListTable() {
        return rcListTable == null ? null : rcListTable.getComponent();
    }
    
    public void showPopup( RichPopup popup){
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            popup.show(ph);   
    }
    public void showInfoMsg( String msg,int delay,int duration){
        
        addToGrowl(GrowlType.notice,msg,delay,duration);
    }
    
    public void showWarningMsg( String msg,int delay,int duration){
        
        addToGrowl(GrowlType.warning,msg,delay,duration);
    }
    public void showErrorMsg( String msg,int delay,int duration){
        
        addToGrowl(GrowlType.warning,msg,delay,duration);
    }

    public void setRecallPopup(RichPopup recallPopup) {
        this.recallPopup = recallPopup;
    }

    public RichPopup getRecallPopup() {
        return recallPopup;
    }

    public void clearFilter(RichTable t) {
        if (t != null) {
            FilterableQueryDescriptor queryDescriptor = (FilterableQueryDescriptor) t.getFilterModel();
            if (queryDescriptor != null && queryDescriptor.getFilterCriteria() != null) {
                queryDescriptor.getFilterCriteria().clear();
                t.queueEvent(new QueryEvent(t, queryDescriptor));
            }
        }
    }


    public void cancelChanges(ActionEvent ae) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Rollback");
        exec.execute();
        OperationBinding execstatus=bindings.getOperationBinding("ExecuteApplValidStatus") ;
        execstatus.execute();
        execstatus=bindings.getOperationBinding("getMissingDetails") ;
        applValidItem=(ApplValidItem)execstatus.execute();
        FacesContext fc = FacesContext.getCurrentInstance();
        if (exec.getErrors().isEmpty() )
        {
            addToGrowl(GrowlType.notice,"Cancelled changes",0,5000);
        }
        else
        addToGrowl(GrowlType.error,"Error cancelling changes",0,5000);
        autoCloseMessage(ae.getComponent().getClientId());
    }

    public void saveChanges(ActionEvent ae) {

        Boolean isCommitEnabled=(Boolean)JSFUtils.resolveExpression("#{bindings.Commit.enabled}");
        if(!(isCommitEnabled)) {
           addToGrowl(GrowlType.notice,"No changes to save",0,5000);
            return;
        }
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Commit");
        exec.execute();
        OperationBinding execstatus=bindings.getOperationBinding("ExecuteApplValidStatus") ;
        execstatus.execute();
        execstatus=bindings.getOperationBinding("getMissingDetails") ;
        applValidItem=(ApplValidItem)execstatus.execute();
        FacesContext fc = FacesContext.getCurrentInstance();
        if (exec.getErrors().isEmpty() )
        {
            addToGrowl(GrowlType.notice,"Saved successfully",0,5000);
        }
        else
            addToGrowl(GrowlType.error,"Unable to save changes. Cancel your changes and try again",0,5000);
        autoCloseMessage(ae.getComponent().getClientId());
    }

    public void closeView(ActionEvent ae) {
        Boolean isCommitEnabled=(Boolean)JSFUtils.resolveExpression("#{bindings.Commit.enabled}");
        if(!(isCommitEnabled)) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.getApplication().getNavigationHandler().handleNavigation(context,null,"back");
            return;
        }
        addToGrowl(GrowlType.warning,"Unsaved changes. Please save or cancel changes to proceed",0,5000);
        autoCloseMessage(ae.getComponent().getClientId());
        
        
    }

    public void changeApplicationStatus(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding aiterOld = bindings.findIteratorBinding("PaaAppDetailView1Iterator");
        Row applrOld = aiterOld.getCurrentRow();
        String oldApplStatusName=null;
        long oldApplStatus=0;
        if(applrOld!=null && applrOld.getAttribute("ApplStatus")!=null) {
            oldApplStatusName=(String)applrOld.getAttribute("ApplStatusName");
            oldApplStatus=((BigDecimal)applrOld.getAttribute("ApplStatus")).longValue();
        }
        OperationBinding execstatus=bindings.getOperationBinding("changeApplStatus") ;
        Boolean result=(Boolean)execstatus.execute();
         Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(result) {
            execstatus=bindings.getOperationBinding("ExecuteCp") ;
            execstatus.execute();
            execstatus=bindings.getOperationBinding("ExecuteAppl") ;
            execstatus.execute();
            execstatus=bindings.getOperationBinding("UpdateStatusHistory") ;
            execstatus.execute();
            execstatus=bindings.getOperationBinding("ExecuteApplValidStatus") ;
            execstatus.execute();
            execstatus=bindings.getOperationBinding("getMissingDetails") ;
            applValidItem=(ApplValidItem)execstatus.execute();
            addToGrowl(GrowlType.notice,(String)(pfm.get("popupMsg") != null?pfm.get("popupMsg"):"Status successfully changed"),0,5000);
            DCIteratorBinding citer = bindings.findIteratorBinding("PaaConceptDetailView1Iterator");
            Row cpr = citer.getCurrentRow();
            DCIteratorBinding aiter = bindings.findIteratorBinding("PaaAppDetailView1Iterator");
            Row applr = aiter.getCurrentRow();
            SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
            Boolean sendEmail = (Boolean) pfm.get("sendEmail");
            if(sendEmail==null)
                    sendEmail=false;
            if(applr!=null &&(!sc.isUserInRole("admin")||sendEmail)) {
                EmailMessageBean emb=new EmailMessageBean();
                long paaId=((Number)applr.getAttribute("PaaId")).longValue();
                long applNbr=((BigDecimal)applr.getAttribute("ApplNbr")).longValue();
                String awardee=(String)applr.getAttribute("AwardeeName");
                String applStatusName=(String)applr.getAttribute("ApplStatusName");
                long applStatus=((BigDecimal)applr.getAttribute("ApplStatus")).longValue();
                String applProgramName=(String)applr.getAttribute("ApplProgramName");
                String partnerSubject="USAID/BHA submission #"+applNbr+" has changed";
                String partnerMessage="USAID/BHA submission #"+applNbr+": "+applProgramName+" has changed from "+oldApplStatusName+" to "+applStatusName+".";
                //Do not send partner notifications when status changes from Draft/Requires Revision to Complete or Complete to Draft.
                if(!((oldApplStatus==1 && applStatus==2)||(oldApplStatus==14 && applStatus==2)||(oldApplStatus==2 && applStatus==1)))
                    emb.sendPartnerNotification(partnerSubject, partnerMessage, ""+paaId);
                
                //Notify USAID/BHA Staff for Recalled and Submitted Application
                if((oldApplStatus==3 && applStatus==1)) {
                    String ofdaSubject=""+awardee+" has recalled submission #"+applNbr+": "+applProgramName;
                    String ofdaMessage=""+awardee+" has recalled submission #"+applNbr+": "+applProgramName+". The applicaton details will no longer be visible to USAID/BHA staff until the applicant re-submits the application.";
                    emb.sendStaffNotification(ofdaSubject, ofdaMessage, ""+paaId);
                }
                else if(applStatus==3) {
                    String ofdaSubject=""+awardee+" has submitted submission #"+applNbr+": "+applProgramName;
                    String ofdaMessage=""+awardee+" has submitted submission #"+applNbr+": "+applProgramName+".";
                    emb.sendStaffNotification(ofdaSubject, ofdaMessage, ""+paaId);
                }
                
            }
            if(cpr==null && applr==null)
            {
                FacesContext context = FacesContext.getCurrentInstance();
                context.getApplication().getNavigationHandler().handleNavigation(context,null,"reset");
                addToGrowl(GrowlType.warning,"Application is no longer visible",500,5000);
            }
        }
        else
        {
            addToGrowl(GrowlType.error,"Unable to change status. Please try again or contact support team.",0,5000);
            FacesContext context = FacesContext.getCurrentInstance();
            context.getApplication().getNavigationHandler().handleNavigation(context,null,"reset");

        }
    }

    public void changeStatusConfirmDialongLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            changeApplicationStatus(null);
        }
        Map pageFlowScopeMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pageFlowScopeMap.put("statusComments" , null);
        pageFlowScopeMap.put("sendEmail" , null);
        
    }
    
    
    public void changeCsStatusDialongLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes || dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding execstatus=bindings.getOperationBinding("changeCostSheetStatus") ;
            Boolean result=(Boolean)execstatus.execute();
            if(result) {
                    execstatus=bindings.getOperationBinding("ExecuteAppl") ;
                    execstatus.execute();               
                    addToGrowl(GrowlType.notice,"Contribution Status successfully changed",0,5000);
                    DCIteratorBinding aiter = bindings.findIteratorBinding("PaaAppDetailView1Iterator");
                    Row applr = aiter.getCurrentRow();
                    Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
                    if(applr!=null && applr.getAttribute("PaxActivitiesId")!=null)
                    {
                        BigDecimal contributionNbr = (BigDecimal) applr.getAttribute("PaxActivitiesId");
                        String awardee=(String)applr.getAttribute("AwardeeName");
                        EmailMessageBean emb=new EmailMessageBean();
                        String contributionStatus = (String) pfm.get("newCsStatusCode");
                        String statusCsComment=(String)pfm.get("statusCsComments");
                        Date d = new Date();
                        SimpleDateFormat df = new SimpleDateFormat("MMM-dd-yyyy");
                        String currentDate=df.format(d);
                        if("83".equals(contributionStatus)) {
                            //Requires Adjustment
                            String subject="Applicant "+awardee+" requests adjustment for Contribution # "+contributionNbr.longValue();
                            String message="Contribution Adjustment requested by Applicant "+awardee+" for Contribution # "+contributionNbr.longValue();
                            message+="<br><br><b>Date:</b> "+currentDate+"<br><br><b>Comments:</b> "+statusCsComment;
                            emb.sendStaffContributionNotification(subject, message, ""+contributionNbr.longValue());
                        }
                        else if ("63".equals(contributionStatus)) {
                            //Validated
                            String subject="Contribution # "+contributionNbr.longValue()+" validated by Applicant "+awardee;
                            String message="Contribution # "+contributionNbr.longValue()+" validated by Applicant "+awardee+"<br><br><b>Date:</b> "+currentDate;
                            emb.sendStaffContributionNotification(subject, message, ""+contributionNbr.longValue());
                        }
                            
                        
                        
                    }
                }
                else
            {
                    addToGrowl(GrowlType.error,"Unable to change status. Please try again or contact support team.",0,5000);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.getApplication().getNavigationHandler().handleNavigation(context,null,"reset");
            }
            
        }
        Map pageFlowScopeMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pageFlowScopeMap.put("statusCsComments" , null);
    }

    public void setApplValidItem(ApplValidItem applValidItem) {
        this.applValidItem = applValidItem;
    }

    public ApplValidItem getApplValidItem() {
        return applValidItem;
    }
    public boolean getModAwardNbrRequired() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding aiter = bindings.findIteratorBinding("PaaAppDetailView1Iterator");
        Row applr = aiter.getCurrentRow();
        BigDecimal type = (BigDecimal) applr.getAttribute("RequestType");
        if(applr.getAttribute("RequestType")!=null && (type.intValue()==12|| type.intValue()==18))
            return true;
        return false;
    }
  public void downloadCostsheet() {
      Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
      
  }
  
    public void downloadCostsheet(FacesContext facesContext,
                               java.io.OutputStream outputStream) throws UnsupportedEncodingException, IOException {
        HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
        response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String reportType = requestMap.get("reportType").toString();
        if(requestMap.get("paxActivitiesId")!=null)
        {
                StringBuilder baseURL = new StringBuilder();                
                try {                    
                    File pFile ;                           
                    if("Rtf".equals(reportType)){
                        baseURL.append(appScope.get("REPORT_SERVER_DIRECT_URL") + "?Rep" + appScope.get("INSTANCE") +
                                       "Rtf+report=AAMP_COST_SHEET.RDF+P_PAX_ACTIVITIES_ID=" + requestMap.get("paxActivitiesId") + "+DESNAME=Individual%20Costsheets.Rtf");
                        pFile = new File( "Individual_Costsheets.Rtf");                       
                        response.setHeader("Content-disposition","attachment;filename=\"Individual_Costsheets.Rtf\"");
                    
                    }else{
                        baseURL.append(appScope.get("REPORT_SERVER_DIRECT_URL") + "?Rep" + appScope.get("INSTANCE") +
                                       "Excel+report=AAMP_COST_SHEET.RDF+P_PAX_ACTIVITIES_ID=" + requestMap.get("paxActivitiesId") + "+DESNAME=Individual%20Costsheets.xls");
                        pFile = new File( "Individual_Costsheets.xls");                       
                        response.setHeader("Content-disposition","attachment;filename=\"Individual_Costsheets.xls\"");
                    }
                    
                    URL costsheetLink = new URL(baseURL.toString());                     
                    FileUtils.copyURLToFile(costsheetLink, pFile);
                    ServletOutputStream out;                    
                    out = response.getOutputStream();
                    FileInputStream in = new FileInputStream(pFile);
                    byte[] buffer = new byte[4096];
                    int length;
                    while ((length = in.read(buffer)) > 0){
                        out.write(buffer, 0, length);
                    }
                    in.close();
                    out.flush();
                } catch (IOException e) {
                    addToGrowl(GrowlType.error,"Unable to download file. Please try again or contact support team.",0,5000);
                } 
        }
    }
    
    public void downloadCostsheetSummary(FacesContext facesContext,
                               java.io.OutputStream outputStream) throws UnsupportedEncodingException, IOException {
        HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
        response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        
        String reportType = requestMap.get("reportType").toString();
        if(requestMap.get("paxActivitiesId")!=null)
        {
                StringBuilder baseURL = new StringBuilder();
                try {
                          
                    File pFile ; 
                          
                    if("Rtf".equals(reportType)){
                        baseURL.append(appScope.get("REPORT_SERVER_DIRECT_URL") + "?Rep" + appScope.get("INSTANCE") +
                                   "Rtf+report=AAMP_CONTRIBUTION_SUMMARY.RDF+P_PAX_ACTIVITIES_ID=" + requestMap.get("paxActivitiesId") + "+DESNAME=Contribution%20Summary.Rtf");
                        pFile = new File( "Contribution_Summary.Rtf");                       
                        response.setHeader("Content-disposition","attachment;filename=\"Contribution_Summary.Rtf\"");
                    }else{
                        
                        baseURL.append(appScope.get("REPORT_SERVER_DIRECT_URL") + "?Rep" + appScope.get("INSTANCE") +
                                       "Excel+report=AAMP_CONTRIBUTION_SUMMARY.RDF+P_PAX_ACTIVITIES_ID=" + requestMap.get("paxActivitiesId") + "+DESNAME=Contribution%20Summary.xls");
                        pFile = new File( "Contribution_Summary.xls");                       
                        response.setHeader("Content-disposition","attachment;filename=\"Contribution_Summary.xls\"");
                    }
                    
                    URL costsheetLink = new URL(baseURL.toString());
                    FileUtils.copyURLToFile(costsheetLink, pFile);
                    ServletOutputStream out;
                    out = response.getOutputStream();
                    FileInputStream in = new FileInputStream(pFile);
                    byte[] buffer = new byte[4096];
                    int length;
                    while ((length = in.read(buffer)) > 0){
                        out.write(buffer, 0, length);
                    }
                    in.close();
                    out.flush();
                } catch (IOException e) {
                    addToGrowl(GrowlType.error,"Unable to download file. Please try again or contact support team.",0,5000);
                } 
        }
    }

    public void CpProgramTypeChgLnsr(ValueChangeEvent vce) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding execstatus=bindings.getOperationBinding("setPaaCpDocTypeList") ;
        execstatus.getParamsMap().put("p_program_type_id", vce.getNewValue());
        execstatus.execute();
    }

    public void applProgramTypeChgLnsr(ValueChangeEvent vce) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding execstatus=bindings.getOperationBinding("setPaaDocTypeList") ;
        execstatus.getParamsMap().put("p_program_type_id", vce.getNewValue());
        execstatus.execute();
    }
    
    @SuppressWarnings({ "unchecked", "oracle.jdeveloper.java.nested-assignment",
                      "oracle.jdeveloper.java.semantic-warning" })
    public void downloadAllFiles(FacesContext facesContext,
                                 java.io.OutputStream outputStream) throws UnsupportedEncodingException, IOException,
                                                                           SQLException {

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding docIter = null;
        String fileName = null;
        if (isAppl != null  && isAppl.equals("Y")){
            docIter = bindings.findIteratorBinding("PaaDocsView1Iterator");
            isAppl = "N";
        }
        if (isCp != null  && isCp.equals("Y")){
            docIter = bindings.findIteratorBinding("PacpDocsView1Iterator");
            isCp ="N";
        }

        Map requestScope = ADFContext.getCurrent().getRequestScope();


        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setContentType("application/x-download");
        String zipName = (String) requestScope.get("zipName");
        
        response.setHeader("Content-Disposition", "attachment; filename=\"" + requestScope.get("zipName") + ".zip\"");
        ZipOutputStream output = null;
        output = new ZipOutputStream(new BufferedOutputStream(response.getOutputStream(), 8192));
        byte[] buffer = new byte[8192];
Row[] rowArray= docIter.getAllRowsInRange();
int i=1;
for(Row r:rowArray){

            InputStream is = null;

            try {
                if (null != r.getAttribute("FileContents")){
                BlobDomain bd = (BlobDomain) r.getAttribute("FileContents");
                is = new BufferedInputStream(bd.getBinaryStream(), 8192);
                if (null != r.getAttribute("RevisionNbr")){
                fileName = ("Rev"+r.getAttribute("RevisionNbr")+"_"+i+"_"+(String)r.getAttribute("FileName"));
                }else{
                    fileName = ("Orig_"+i+"_"+(String)r.getAttribute("FileName"));
                }
                    
                output.putNextEntry(new ZipEntry(fileName));
                
                for (int length = 0; (length = is.read(buffer)) > 0;) {
                    output.write(buffer, 0, length);
                }
                }

            } catch (IOException ex) {
                logger.log(logger.ERROR, "IO Exception in Download File  ", ex.getCause());
                ex.printStackTrace();
            } finally {
                if(is!=null)
                    is.close();
                if(output != null)
                output.closeEntry();
            }
i++;
        }

        output.close();
    }
    public Date getOfdaMinStartDate() {
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            // ART-168
            cal.add(Calendar.DATE, 0); 
            java.util.Date date = cal.getTime();
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String currentDate = formatter.format(date);
            return formatter.parse(currentDate);
                    } catch (Exception e) {
            return null;
                    }
    }

    public void setOneHqTeam(BigDecimal oneHqTeam) {
        this.oneHqTeam = oneHqTeam;
}

    public BigDecimal getOneHqTeam() {
        if(this.getSelectedHqTeam().size()>0)
            oneHqTeam = (BigDecimal) this.getSelectedHqTeam().get(0);
        return oneHqTeam;
    }
    public void applOneHqTeamValChgLsnr(ValueChangeEvent vce) {
        List<BigDecimal> t=new ArrayList<BigDecimal>();
        if(vce.getNewValue()!=null) {
            t.add((BigDecimal)vce.getNewValue());
        }
        Shuttle.setSelected(t, "PaaHqTeamView1Iterator", "HqTeamId", "DeleteHqTeam", "CreateInsertHqTeam");
    }

    public void applTypeChgLnsr(ValueChangeEvent vce) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding execstatus=bindings.getOperationBinding("setPaaDocTypeList") ;
        execstatus.getParamsMap().put("p_appl_type", ""+vce.getNewValue());
        execstatus.execute();
    }

    public void requestTypeChgLnsr(ValueChangeEvent vce) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding execstatus=bindings.getOperationBinding("setPaaDocTypeList") ;
        execstatus.getParamsMap().put("p_request_type", vce.getNewValue());
        execstatus.execute();
    }
}
