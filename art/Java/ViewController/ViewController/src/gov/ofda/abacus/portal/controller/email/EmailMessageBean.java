package gov.ofda.abacus.portal.controller.email;

import gov.ofda.abacus.portal.controller.UIControl;
import gov.ofda.abacus.portal.controller.UIControl.GrowlType;
import gov.ofda.abacus.portal.controller.dashboard.ARTAppScopeBean;

import java.io.Serializable;

import java.util.Map;

import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class EmailMessageBean extends UIControl implements Serializable{
    @SuppressWarnings("compatibility:-8526697436393122144")
    private static final long serialVersionUID = 1L;
    private Map appScope;
    ArtEmail email = new ArtEmail();
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.portal.controller.email.EmailMessageBean.class);
    public EmailMessageBean() {
        super();
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =bindingContext.findBindingContainer("gov_ofda_abacus_art_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getArtConstants");
        exec.execute();
        appScope = (Map<String, String>) exec.getResult();    
    }
    
    
    public boolean sendPartnerNotification(String subject, String message, String paaId) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_art_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getApplicantEmailList");
        exec.getParamsMap().put("paaId", paaId);
        exec.execute();
        String[] list = (String[]) exec.getResult();
        if(list[0]!=null)
        {
        boolean result= email.sendHtmlEmail(list[0], list[1], subject, message + "<br><br>" + getPartnerApplicationDetails(paaId),
                            "Application status changed", null);
        if(result) {
            this.addToGrowl(GrowlType.notice, "Application status update email sent to Applicant Headquarters/Field Email list", 50, 5000);
        }
        else {
            this.addToGrowl(GrowlType.error, "Application status update email failed.", 50, 5000);
        }
        return result;
        }
        return false;
    }
    
    public boolean sendStaffNotification(String subject, String message, String paaId) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_art_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getStaffApplicationEmailList");
        exec.getParamsMap().put("paaId", paaId);
        exec.execute();
        String[] list = (String[]) exec.getResult();
        if(list[0]!=null)
        {
        boolean result= email.sendHtmlEmail(list[0], list[1], subject, message + "<br><br>" + getOFDAStaffApplicationDetails(paaId),
                            "Application status changed", null);
            if(result) {
                this.addToGrowl(GrowlType.notice, "Email notification sent to USAID/BHA Headquarters teams selected.", 50, 5000);
            }
            else {
                this.addToGrowl(GrowlType.error, "Email notification to USAID/BHA Headquarters teams selected failed.", 50, 5000);
            }
            return result;
        }
        return false;
    }
    
    public void sendAdminEmail() {
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String servername = FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
        int serverport = FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort();
        String username = ADFContext.getCurrent().getSecurityContext().getUserName();
        String message = (String) pfp.get("message");
        String module = (String) pfp.get("module");
        if (message != null) {
            String tmp =
                "<table><tr><th>Error Message</th><td>" + message + "</td></tr><tr><th>Username</th><td>" + username +
                "</td></tr><tr><th>Server</th><td>" + servername + "</td></tr><tr><th>port</th><td>" + serverport +
                "</td></tr></table>";
            Thread thread = new Thread(new MySendMailRunnable(email, "Error in " + module, tmp));
            thread.start();
        }
    }
    
    public String getPartnerApplicationDetails(String paaId) {
        String actionURL =
            "<br><a href=\"" + appScope.get("AAMP_URL") +appScope.get("AAMP_APPLICATION_LINK") +
            "?paaId=" + paaId + "\">Click here to view application.</a><br>";
        return actionURL;
    }
    
    public String getOFDAStaffApplicationDetails(String paaId) {
        String actionURL =
            "<br><a href=\"" + appScope.get("ABACUS_URL") +appScope.get("ABACUS_APPLICATION_LINK") +
            "?paaId=" + paaId + "\">Click here to view application.</a><br>";
        return actionURL;
    }

    public void sendAdminEmail(String subject, String message) {
        String servername = FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
        int serverport = FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort();
        String username = ADFContext.getCurrent().getSecurityContext().getUserName();
        if (message != null) {
            String tmp =
                "<table><tr><th>Error Message</th><td>" + message + "</td></tr><tr><th>Username</th><td>" + username +
                "</td></tr><tr><th>Server</th><td>" + servername + "</td></tr><tr><th>port</th><td>" + serverport +
                "</td></tr></table>";
            Thread thread = new Thread(new MySendMailRunnable(email, subject, tmp));
            thread.start();
        }
    }
    
    public boolean sendStaffContributionNotification(String subject, String message, String contributionNbr) {
        String staffContributionEmail = (String) appScope.get("CONTRIBUTION_STAFF_EMAIL");
        if(staffContributionEmail!=null)
        {
            String contributionURL =
                "<br><a href=\"" + appScope.get("ABACUS_URL") +appScope.get("ABACUS_CONTRIBUTION_LINK") +
                "?contributionNbr=" + contributionNbr + "\">Click here to view contribution in Abacus.</a><br>";
        boolean result= email.sendHtmlEmail(staffContributionEmail, null, subject, message + "<br><br>" + contributionURL,
                            "Contribution status changed", null);
            if(result) {
                this.addToGrowl(GrowlType.notice, "Email notification sent to USAID/BHA Staff.", 50, 5000);
            }
            else {
                this.addToGrowl(GrowlType.error, "Email notification to USAID/BHA Staff failed.", 50, 5000);
            }
            return result;
        }
        return false;
    }
    private class MySendMailRunnable implements Runnable {
        private ArtEmail email;
        private String subject;
        private String message;

        public String getSubject() {
            return subject;
        }

        public String getMessage() {
            return message;
        }


        public MySendMailRunnable(ArtEmail email, String subject, String message) {
            super();
            this.subject = subject;
            this.message = message;
            this.email = email;
        }

        @Override
        public void run() {
            email.sendHtmlEmail("abacusadmin@ofda.gov", null, this.getSubject(), this.getMessage(), null, null);
        }
    }
}
