package gov.ofda.abacus.portal.controller.w4;

import gov.ofda.abacus.portal.controller.UIControl;

import java.io.IOException;
import java.io.InputStream;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import java.text.DecimalFormat;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCControlBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichTextEditor;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGridLayout;
import oracle.adf.view.rich.component.rich.layout.RichToolbar;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.domain.BlobDomain;

import org.apache.myfaces.trinidad.model.UploadedFile;
import org.apache.myfaces.trinidad.util.ComponentReference;


public class UploadFile4W extends UIControl implements Serializable{
    @SuppressWarnings("compatibility:-8794093666986360055")
    private static final long serialVersionUID = 1L;
    private transient UploadedFile file;
    private transient UploadedFile newFile;
    private String filelength = "";
    private transient Row docRow;
    private ComponentReference docsTable;
    private ComponentReference docsForm;
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.portal.controller.UploadFile.class);
    private ComponentReference docsToolbar;
    private ComponentReference updatedGrid;
    public UploadFile4W() {
    }
    
    /**
     * Called when user updates existing document.
     * This function will update the row with new document details and content.
     * @param file
     */
    public void setFile(UploadedFile file) {
        this.file = file;
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCControlBinding adocs_ctrl = bindings.findCtrlBinding("Par4wDocsView1");
        Row r = adocs_ctrl.getCurrentRow();
        if (r != null && this.getFile() != null) {
            UploadedFile myfile = this.getFile();
            String filename = myfile.getFilename();
            String content = myfile.getContentType();
            long fsize = myfile.getLength();
            logger.log(logger.NOTIFICATION, "file details: " + filename + content + fsize);
            BlobDomain nfile = null;
            try {
                InputStream istream = myfile.getInputStream();
                byte[] buffer = new byte[(int)myfile.getLength()];
                for (int i = 0; i < fsize; i++) {
                    istream.read(buffer, i, 1);
                }
                nfile = new BlobDomain(buffer);
                r.setAttribute("FileContents", nfile);
                r.setAttribute("FileName", filename);
                r.setAttribute("FileContentType", content);
                r.setAttribute("FileSize", fsize);
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "File Uploaded", "File uploaded successfully. Please save changes");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            } catch (Exception e) {
                logger.log(logger.ERROR, e.toString());
            }

            AdfFacesContext.getCurrentInstance().addPartialTarget(docsTable.getComponent());
            AdfFacesContext.getCurrentInstance().addPartialTarget(docsForm.getComponent());
        }
        this.file=null;
    }
    public UploadedFile getFile() {
        return file;
    }
    
    /**
     * Called from af:fileDownloadActionListener.
     * af:setActionListener sets the current row to docRow.
     * This method then uses information in this row and sends the document
     * @param facesContext
     * @param outputStream
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    public void fileToDownload(FacesContext facesContext,
                               java.io.OutputStream outputStream) throws UnsupportedEncodingException, IOException {
        HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
        response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        if (docRow != null)
            try {
                byte[] content;
                content = null;
                BlobDomain bd = (BlobDomain)docRow.getAttribute("FileContents");
                content = bd.toByteArray();
                ServletOutputStream out;
                response.setHeader("Content-disposition",
                                   "attachment;filename=\"" + docRow.getAttribute("FileName") + "\"");
                out = response.getOutputStream();
                out.write(content);
                out.flush();

            } catch (IOException ex) {
                ex.printStackTrace();
            }
    }

    public void setFilelength(String filelength) {
        this.filelength = filelength;
    }
    /**
     * Calculates teh File Length in KB using FileSize attribute in AbacusxDocsView table.
     * @return File length in KB
     */
    public String getFilelength() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCControlBinding adocs_ctrl = bindings.findCtrlBinding("Par4wDocsView1");
        Row r = adocs_ctrl.getCurrentRow();
        if (r != null && r.getAttribute("FileSize") != null) {
            long fsize = Long.parseLong(r.getAttribute("FileSize").toString());
            double tmp = ((double)fsize) / 1024;
            if(tmp<1)
                filelength="Less than 1 KB";
            else if(tmp<1024)
            {
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            filelength = formatter.format(tmp) + " KB";
            }
            else {
                tmp=tmp/1024;
                DecimalFormat formatter = new DecimalFormat("#,###,###.##");
                filelength = formatter.format(tmp) + " MB";
            }
        }
        return filelength;
    }
    
    public void setDocRow(Row docRow) {
        this.docRow = docRow;
    }

    public Row getDocRow() {
        return docRow;
    }
    
    /**
     * Called when user click on OK in new document popup dialog.
     * Will check if doc type code and file is not null and if so then
     * calls createDoc method in AwardModuleImpl which will insert new record and details into the table
     * @param dialogEvent
     */
    public void newDocument(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            if (newFile != null) {
                DCBindingContainer bindings =
                    (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding adocs_create = bindings.getOperationBinding("createDoc");
                adocs_create.execute();
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "File Uploaded", "File uploaded successfully. Please save changes");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                logger.log(logger.NOTIFICATION,
                           "file details: " + newFile.getFilename() + " " + newFile.getContentType() + " " +
                           newFile.getLength());
               // OperationBinding exec = bindings.getOperationBinding("Commit");
               // exec.execute();
                AdfFacesContext.getCurrentInstance().addPartialTarget(docsTable.getComponent());
                AdfFacesContext.getCurrentInstance().addPartialTarget(docsForm.getComponent());
                AdfFacesContext.getCurrentInstance().addPartialTarget(updatedGrid.getComponent());
                //AdfFacesContext.getCurrentInstance().addPartialTarget(docsForm);
                //saveEditReportDoc();
                newFile=null;
            }
        }
    }
    public void setNewFile(UploadedFile newFile) {
        this.newFile = newFile;
    }

    public UploadedFile getNewFile() {
        return newFile;
    }

    public void setDocsTable(UIComponent docsTable) {
        this.docsTable = ComponentReference.newUIComponentReference(docsTable);
    }

    public UIComponent getDocsTable() {
        return docsTable == null ? null : docsTable.getComponent();
    }

    public void setDocsForm(UIComponent docsForm) {
        this.docsForm = ComponentReference.newUIComponentReference(docsForm);
    }

    public UIComponent getDocsForm() {
        return docsForm == null ? null : docsForm .getComponent();
    }


    public void deleteDocument(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = bindings.getOperationBinding("DeleteDocument");
            exec.execute();
           // exec = bindings.getOperationBinding("Commit");
           // exec.execute();
            if (exec.getErrors().isEmpty()) {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Document Deleted", "Document deleted successfully. Please save changes");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
            
            AdfFacesContext.getCurrentInstance().addPartialTarget(docsTable.getComponent());
            AdfFacesContext.getCurrentInstance().addPartialTarget(docsForm.getComponent());
            AdfFacesContext.getCurrentInstance().addPartialTarget(docsToolbar.getComponent());
            AdfFacesContext.getCurrentInstance().addPartialTarget(updatedGrid.getComponent());        
        }
    }  
    
    public void setDocsToolbar(UIComponent docsToolbar) {
        this.docsToolbar = ComponentReference.newUIComponentReference(docsToolbar);
    }

    public UIComponent getDocsToolbar() {
        return docsToolbar == null ? null : docsToolbar.getComponent();
    }
    public void setUpdatedGrid(UIComponent updatedGrid) {
        this.updatedGrid = ComponentReference.newUIComponentReference(updatedGrid);
    }

    public UIComponent getUpdatedGrid() {
        return updatedGrid == null ? null : updatedGrid.getComponent();
    }
}
