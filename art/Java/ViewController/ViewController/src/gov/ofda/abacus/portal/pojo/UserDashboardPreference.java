package gov.ofda.abacus.portal.pojo;

import gov.ofda.abacus.portal.controller.elasticsearch.TermItem;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

public class UserDashboardPreference implements Serializable {
    @SuppressWarnings("compatibility:621605602421689014")
    private static final long serialVersionUID = 1L;

    public UserDashboardPreference() {
        super();
    }
    private String preferenceName;
    private Integer startFY=2017;
    private Integer endFY=2018;
    private Boolean isDefault=false;
    private Boolean showFilter=true;
    private Boolean showCharts=true;
    private List<TermItem> termFilters=new ArrayList<TermItem>();
    private String searchQuery="";
    private Boolean favoritesOnly=false;
    private String sortOrder="relevance";
    private Integer size=10;
    private Integer from=0;
    private List<SortItem> selectedSortList=new ArrayList<SortItem>();
    private Boolean dataTabSelection[]= {true,false,false};
    private Boolean chartTabSelection[][]= {{true,true,false,false,false,false,false,false},{false,true,false,false}};
    private Boolean filterTabSelection[]= {true,false};
    private transient boolean isDelete=false;

    public void setShowFilter(Boolean showFilter) {
        this.showFilter = showFilter;
    }

    public Boolean getShowFilter() {
        return showFilter;
    }

    public void setShowCharts(Boolean showCharts) {
        this.showCharts = showCharts;
    }

    public Boolean getShowCharts() {
        return showCharts;
    }

    public void setTermFilters(List<TermItem> termFilters) {
        this.termFilters = termFilters;
    }

    public List<TermItem> getTermFilters() {
        return termFilters;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setFavoritesOnly(Boolean favoritesOnly) {
        this.favoritesOnly = favoritesOnly;
    }

    public Boolean getFavoritesOnly() {
        return favoritesOnly;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getSize() {
        return size;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getFrom() {
        return from;
    }

    public void setSelectedSortList(List<SortItem> selectedSortList) {
        this.selectedSortList = selectedSortList;
    }

    public List<SortItem> getSelectedSortList() {
        return selectedSortList;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }


    public void setPreferenceName(String preferenceName) {
        this.preferenceName = preferenceName;
    }

    public String getPreferenceName() {
        return preferenceName;
    }

    public void setStartFY(Integer startFY) {
        this.startFY = startFY;
    }

    public Integer getStartFY() {
        return startFY;
    }

    public void setEndFY(Integer endFY) {
        this.endFY = endFY;
    }

    public Integer getEndFY() {
        return endFY;
    }
    @Override
    public boolean equals(Object o) {
        if (o == this) {
                    return true;
                }
     if(o instanceof UserDashboardPreference) {
         UserDashboardPreference object=(UserDashboardPreference)o;
         if(object.getPreferenceName()!=null && object.getPreferenceName().equals(this.getPreferenceName())&& object.getPreferenceName()!=null && object.getPreferenceName().equals(this.getPreferenceName()))
            return true;
     }
     return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + preferenceName.hashCode();
        return result;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setDataTabSelection(Boolean[] dataTabSelection) {
        this.dataTabSelection = dataTabSelection;
    }

    public Boolean[] getDataTabSelection() {
        return dataTabSelection;
    }

    public void setChartTabSelection(Boolean[][] chartTabSelection) {
        if(chartTabSelection!=null && chartTabSelection.length>0 && chartTabSelection[0].length==7) {
            Boolean temp []=new Boolean[8];
            for(int i=0;i<chartTabSelection[0].length;i++)
            {
                temp[i]=chartTabSelection[0][i];
            }
            temp[7]=false;
        }
        this.chartTabSelection = chartTabSelection;
    }

    public Boolean[][] getChartTabSelection() {
        return chartTabSelection;
    }

    public void setFilterTabSelection(Boolean[] filterTabSelection) {
        this.filterTabSelection = filterTabSelection;
    }

    public Boolean[] getFilterTabSelection() {
        return filterTabSelection;
    }
}
