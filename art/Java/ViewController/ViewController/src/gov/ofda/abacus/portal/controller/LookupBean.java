package gov.ofda.abacus.portal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichToolbar;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySetImpl;

/**
 * LookupBean.java
 * Purpose: Used by indicatorLookup.jsff as a backing bean to manage Indicator Lookup data.
 *
 * @author Kartheek Atluri
 *
 */
public class LookupBean extends UIControl {
    private RichTable indicatorXTable;
    private static ADFLogger logger =
        ADFLogger.createADFLogger(gov.ofda.abacus.portal.controller.LookupBean.class);
    private RichPopup editIndicatorX;
    private RichSelectOneChoice subSector;
    private RichToolbar lookuptoobar;
    private List selectedTypes;
    private RichPanelGroupLayout treeTableGroup;
    private RichTreeTable disaggregatedTreeTable;
    private RowKeySetImpl newDisclosedTreeTableKeys = null;
    public LookupBean() {
    }

    /**
     * Called when user click on save.
     * Saves the rowkey of the currently selected row, runs execute on IndicatorxLookupView 
     * and sets the selected row to the row before execute is run.
     * @return
     */
    public String saveChanges() {
        Key parkey=this.getRowKey(indicatorXTable);
        DCBindingContainer bindings=(DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding reports_execute=bindings.getOperationBinding("Execute");
        reports_execute.execute();
        this.setCurrentRow(indicatorXTable,parkey);   
        return null;
    }

    /**
     * Is called when user click on New. Will execute Createwithparameters action.
     * @return
     */
    public String createNew() {
        DCBindingContainer bindings=(DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding oper1=bindings.getOperationBinding("Createwithparameters");
        oper1.execute();
        RichPopup popup = this.getEditIndicatorX();
        //no hints means that popup is launched in the
        //center of the page
         RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
        return null;
    }

    public void setIndicatorXTable(RichTable indicatorXTable) {
        this.indicatorXTable = indicatorXTable;
    }

    public RichTable getIndicatorXTable() {
        return indicatorXTable;
    }

    public void handleTableDoubleClick(ClientEvent clientEvent) {
        RichPopup popup = this.getEditIndicatorX();
        //no hints means that popup is launched in the
        //center of the page
         RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
    }

    public void setEditIndicatorX(RichPopup editIndicatorX) {
        this.editIndicatorX = editIndicatorX;
    }

    public RichPopup getEditIndicatorX() {
        return editIndicatorX;
    }

    public String cancelEditIndicatorX() {
        DCBindingContainer bindings=(DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = (DCIteratorBinding)bindings.findIteratorBinding("IndicatorxLookupView1Iterator");
        Key key = iter.getCurrentRow().getKey();
        OperationBinding exec=bindings.getOperationBinding("Rollback");
        exec.execute();
        RowSetIterator rsi = iter.getRowSetIterator();
        if (rsi.findByKey(key, 1).length > 0)
            iter.setCurrentRowWithKey(key.toStringFormat(true));
        RichPopup popup = this.getEditIndicatorX();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(indicatorXTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(editIndicatorX);
        
        return null;
    }
    public String saveAndCloseEditIndicatorX() {
        DCBindingContainer bindings=(DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec=bindings.getOperationBinding("Commit");
        exec.execute();
        Key key=this.getRowKey(indicatorXTable);
        exec=bindings.getOperationBinding("Execute");
        exec.execute();
        this.setCurrentRow(indicatorXTable,key);  
        RichPopup popup = this.getEditIndicatorX();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(indicatorXTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(editIndicatorX);
        AdfFacesContext.getCurrentInstance().addPartialTarget(disaggregatedTreeTable);
        
        return null;
    }

    public String saveEditIndicatorX() {
        DCBindingContainer bindings=(DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec=bindings.getOperationBinding("Commit");
        exec.execute();
        Key key=this.getRowKey(indicatorXTable);
        exec=bindings.getOperationBinding("Execute");
        exec.execute();
        this.setCurrentRow(indicatorXTable,key);  
        AdfFacesContext.getCurrentInstance().addPartialTarget(indicatorXTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(editIndicatorX);
        AdfFacesContext.getCurrentInstance().addPartialTarget(disaggregatedTreeTable);
        
        return null;
    }
    public void setSubSector(RichSelectOneChoice subSector) {
        this.subSector = subSector;
    }

    public RichSelectOneChoice getSubSector() {
        return subSector;
    }

    public void sectorChgLsnr(ValueChangeEvent valueChangeEvent) {
        AdfFacesContext.getCurrentInstance().addPartialTarget(subSector);
    }

    public void indicatorXDelete(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings=(DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("IndicatorxLookupView1Iterator");
            Row rw = iter.getCurrentRow();
            rw.setAttribute("Active", "N");
            OperationBinding exec=bindings.getOperationBinding("Commit");
            exec.execute();
            exec=bindings.getOperationBinding("Execute");
            exec.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(indicatorXTable);
            AdfFacesContext.getCurrentInstance().addPartialTarget(editIndicatorX);
            AdfFacesContext.getCurrentInstance().addPartialTarget(disaggregatedTreeTable);
            AdfFacesContext.getCurrentInstance().addPartialTarget(lookuptoobar);

        }
        
    }

    public void setLookuptoobar(RichToolbar lookuptoobar) {
        this.lookuptoobar = lookuptoobar;
    }

    public RichToolbar getLookuptoobar() {
        return lookuptoobar;
    }
    public List getSelectedTypes() {
        this.selectedTypes = Shuttle.getSelected("IndxIndxtypeLinkView1Iterator", "IndTypeId");
        return this.selectedTypes;
    }


    public void setSelectedTypes(List selectedValues) {
        this.selectedTypes = selectedValues;
            Shuttle.setOrderedSelected(this.selectedTypes, "IndxIndxtypeLinkView1Iterator", "IndTypeId",
                                "DeleteTypes", "CreateTypes","IndTypeLevel","ParentIndTypeId");
    }

    public List getAllTypes() {
        return Shuttle.getAll("MvIndTypeLookupView1Iterator", "IndxIndxtypeLinkView1Iterator", "IndTypeId",
                              "IndTypeName");
    }
    public void setDisaggregatedTreeTable(RichTreeTable disaggregatedTreeTable) {
        this.disaggregatedTreeTable = disaggregatedTreeTable;
    }

    public RichTreeTable getDisaggregatedTreeTable() {
        return disaggregatedTreeTable;
    }
    //http://www.oracle.com/technetwork/developer-tools/adf/learnmore/78-man-expanding-trees-treetables-354775.pdf

    public void setNewDisclosedTreeTableKeys(RowKeySetImpl newDisclosedTreeTableKeys) {
        this.newDisclosedTreeTableKeys = newDisclosedTreeTableKeys;
    }
    /**
     * http://www.oracle.com/technetwork/developer-tools/adf/learnmore/78-man-expanding-trees-treetables-354775.pdf
     * getter for disclosed keys. Determines which rows are disclosed in the tree table
     * It discloses all rows upto 4 levels.
     * @return
     */
    public RowKeySetImpl getNewDisclosedTreeTableKeys() {
            newDisclosedTreeTableKeys = new RowKeySetImpl();
            FacesContext fctx = FacesContext.getCurrentInstance();
            String ispopup=(String)AdfFacesContext.getCurrentInstance().getPageFlowScope().get("isPopup");
            UIViewRoot root = fctx.getViewRoot();
            //lookup the tree table component by its component ID
            //r1:0:pt1:tt1
            RichTreeTable treeTable = (RichTreeTable)disaggregatedTreeTable;
            //if tree table is found
            if (treeTable != null && ispopup==null ) {
                //get the collection model to access the ADF binding layer for
                ////the tree binding used
                CollectionModel model = (CollectionModel)treeTable.getValue();
                JUCtrlHierBinding treeBinding = (JUCtrlHierBinding)model.getWrappedData();
                JUCtrlHierNodeBinding nodeBinding = treeBinding.getRootNodeBinding();
                expandAllNodes(nodeBinding, newDisclosedTreeTableKeys, 0, 2);
        }
        return newDisclosedTreeTableKeys;
    }
    
    /**
     * Called to add keys of children in a tree to be disclosed.
     * This is a recursive call that uses maxExpandLevel to determine when to stop.
     * @param nodeBinding
     * @param disclosedKeys
     * @param currentExpandLevel
     * @param maxExpandLevel
     */
    private void expandAllNodes(JUCtrlHierNodeBinding nodeBinding, RowKeySetImpl disclosedKeys, int currentExpandLevel,
                                int maxExpandLevel) {
        if (currentExpandLevel <= maxExpandLevel) {
            List<JUCtrlHierNodeBinding> childNodes = (List<JUCtrlHierNodeBinding>)nodeBinding.getChildren();
            ArrayList newKeys = new ArrayList();
            if (childNodes != null) {
                for (JUCtrlHierNodeBinding _node : childNodes) {
                    newKeys.add(_node.getKeyPath());
                    expandAllNodes(_node, disclosedKeys, currentExpandLevel + 1, maxExpandLevel);
                }
            }
            disclosedKeys.addAll(newKeys);
        }
    }

    public void setTreeTableGroup(RichPanelGroupLayout treeTableGroup) {
        this.treeTableGroup = treeTableGroup;
    }

    public RichPanelGroupLayout getTreeTableGroup() {
        return treeTableGroup;
    }

    public void indicatorTypeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null && ((List)object).size()>3) {
            throw new ValidatorException(new FacesMessage("Indicator disaggregated by values cannot be more than 3 levels"));
        }

    }
}
