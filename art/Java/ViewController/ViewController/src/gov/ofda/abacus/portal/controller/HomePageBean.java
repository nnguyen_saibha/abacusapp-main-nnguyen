package gov.ofda.abacus.portal.controller;

import gov.ofda.abacus.portal.model.domain.common.ReportValidSections;

import java.io.IOException;
import java.io.OutputStream;

import java.util.Map;

import java.io.Serializable;

import java.sql.ResultSet;

import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

import javax.faces.component.UISelectItems;
import javax.faces.context.ResponseWriter;
import javax.faces.model.SelectItem;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.component.rich.input.RichInputNumberSpinbox;
import oracle.adf.view.rich.component.rich.input.RichSelectItem;
import oracle.adf.view.rich.component.rich.input.RichTextEditor;
import oracle.adf.view.rich.component.rich.output.RichOutputFormatted;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.component.UIXCollection;
import org.apache.myfaces.trinidad.component.UIXComponent;
import org.apache.myfaces.trinidad.component.UIXComponentBase;
import org.apache.myfaces.trinidad.component.UIXGroup;
import org.apache.myfaces.trinidad.event.DisclosureEvent;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFCellUtil;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;

public class HomePageBean implements Serializable {
    @SuppressWarnings("compatibility:3159410098294716557")
    private static final long serialVersionUID = 1L;
    private RichPopup reportDetailsPopup;

    private String selectedPar;

    private String awid;
    private RichPopup reportSummaryPopup;

    private RichPopup parDetailPopup;
    private RichPopup regionReportSummary;
    private String priorQtr3;
    private String priorQtr2;
    private String priorQtr1;
    private String currentQtr;

    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.portal.controller.HomePageBean.class);
    private RichTable reportTable;
    private RichTable sectorTable;
    private RichTable indicatorTable;
    private HSSFWorkbook workbook;
    private HSSFSheet sheet;
    HSSFCellStyle headerStyle;
    HSSFCellStyle bodyStyle;
    private ResultSet t[] = new ResultSet[3];
    private String filename = "";
    private Integer currentFy;
    private RichTable reportDetailsTable;
    private ReportValidSections reportValidSections;
    private RichTable narrativeTable;
    private RichTable evaluationDetailsTable;
    private RichTable evaluationSectorsTable;
    private RichTable keywordIndicatorsTable;

    public void setReportValidSections(ReportValidSections reportValidSections) {
        this.reportValidSections = reportValidSections;
    }

    public ReportValidSections getReportValidSections() {
        return reportValidSections;
    }

    public HomePageBean() {
        super();

    }


    public void setReportDetailsPopup(RichPopup reportDetailsPopup) {
        this.reportDetailsPopup = reportDetailsPopup;
    }

    public RichPopup getReportDetailsPopup() {
        return reportDetailsPopup;
    }

    public void setSelectedPar(String selectedPar) {
        this.selectedPar = selectedPar;
    }

    public String getSelectedPar() {
        return selectedPar;
    }

    public void setAwid(String awid) {

        this.awid = awid;
    }

    public String getAwid() {
        return awid;
    }

    public void setReportSummaryPopup(RichPopup reportSummaryPopup) {
        this.reportSummaryPopup = reportSummaryPopup;
    }

    public RichPopup getReportSummaryPopup() {
        return reportSummaryPopup;
    }

    public void setParDetailPopup(RichPopup parDetailPopup) {
        this.parDetailPopup = parDetailPopup;
    }

    public RichPopup getParDetailPopup() {
        return parDetailPopup;
    }

    public void setRegionReportSummary(RichPopup regionReportSummary) {
        this.regionReportSummary = regionReportSummary;
    }

    public RichPopup getRegionReportSummary() {
        return regionReportSummary;
    }

    public void setPriorQtr3(String priorQtr3) {
        this.priorQtr3 = priorQtr3;
    }

    public String getPriorQtr3() {
        //  populateQtrs();
        return priorQtr3;
    }

    public void setPriorQtr2(String priorQtr2) {
        this.priorQtr2 = priorQtr2;
    }

    public String getPriorQtr2() {
        return priorQtr2;
    }

    public void setPriorQtr1(String priorQtr1) {
        this.priorQtr1 = priorQtr1;
    }

    public String getPriorQtr1() {
        return priorQtr1;
    }

    public void setCurrentQtr(String currentQtr) {
        this.currentQtr = currentQtr;
    }

    public String getCurrentQtr() {
        return currentQtr;
    }

    /**
     * @param phaseEvent
     */
    public void populateQtrs(PhaseEvent phaseEvent) {
        /*logger.log(logger.TRACE, "In Phase Event " + phaseEvent.getPhaseId());
        if (phaseEvent.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            logger.log(logger.TRACE, "Inside Populate Qtrs");
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding ctrl_addsec = bindings.getOperationBinding("getCurrentDataConfig");
            Map qtrMap = (Map)ctrl_addsec.execute();
            if (qtrMap != null) {
                priorQtr3 = (String)qtrMap.get("PQ3");
                priorQtr2 = (String)qtrMap.get("PQ2");
                priorQtr1 = (String)qtrMap.get("PQ1");
                currentQtr = (String)qtrMap.get("CQ");
                logger.log(logger.TRACE, "Populated Values for Quarters");
            }
        }
        // Add event code here...
*/
    }


    /**
     * @param phaseEvent
     */
    public void beforePhaseMethod(PhaseEvent phaseEvent) {
        //only perform action if RENDER_RESPONSE phase is reached
        try {
            if (phaseEvent.getPhaseId() == PhaseId.RENDER_RESPONSE) {
                FacesContext fctx = FacesContext.getCurrentInstance();
                //check internal request parameter
                Map requestMap = fctx.getExternalContext().getRequestMap();
                Object showPrintableBehavior = requestMap.get("oracle.adfinternal.view.faces.el.PrintablePage");
                if (showPrintableBehavior != null) {
                    if (Boolean.TRUE == showPrintableBehavior) {
                        ExtendedRenderKitService erks = null;
                        erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
                        //invoke JavaScript from the server
                        erks.addScript(fctx, "window.print();");
                    }
                }
            }
        } catch (Exception e) {
            logger.warning("In Before phase Exception: ", e);
        }

    }

    public void exportToExcel(FacesContext facesContext, OutputStream outputStream) {
        try {
            workbook = new HSSFWorkbook();
            AdfFacesContext fctx = AdfFacesContext.getCurrentInstance();
            String awardNbr = fctx.getPageFlowScope().get("AwardNbr").toString();
            String rptDetails = fctx.getPageFlowScope().get("RptDetails").toString();
            sheet = workbook.createSheet("" + rptDetails);
            filename = awardNbr + "_" + rptDetails;
            if (filename.length() < 1)
                filename = "OFDA_ART";
            HSSFFont boldFont = workbook.createFont();
            HSSFDataFormat format = workbook.createDataFormat();
            boldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

            headerStyle = workbook.createCellStyle();
            bodyStyle = workbook.createCellStyle();

            headerStyle.setFont(boldFont);
            headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
            headerStyle.setBorderRight(CellStyle.BORDER_THIN);
            headerStyle.setBorderTop(CellStyle.BORDER_THIN);
            headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
            headerStyle.setWrapText(true);
            headerStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            headerStyle.setFont(boldFont);

            bodyStyle.setWrapText(true);
            bodyStyle.setBorderLeft(CellStyle.BORDER_THIN);
            bodyStyle.setBorderRight(CellStyle.BORDER_THIN);
            bodyStyle.setBorderTop(CellStyle.BORDER_THIN);
            bodyStyle.setBorderBottom(CellStyle.BORDER_THIN);

            HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
            response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");

            ServletOutputStream out;
            int ct = exportRichTableToExcel(reportTable, sheet, 0);
            if("1".equals(reportValidSections.getAwardBeneficiaries()) || "1".equals(reportValidSections.getFundingDetails()))
                ct = exportRichTableToExcel(reportDetailsTable, sheet, ct);
            if("1".equals(reportValidSections.getSectorBeneficiaries()))
                ct = exportRichTableToExcel(sectorTable, sheet, ct);
            if("1".equals(reportValidSections.getKeywordIndicators()))
                ct = exportRichTableToExcel(keywordIndicatorsTable, sheet, ct);
            if("1".equals(reportValidSections.getSubsectorIndicators()))
                ct = exportRichTableToExcel(indicatorTable, sheet, ct);
            if("1".equals(reportValidSections.getNarrative()))
                ct = exportRichTableToExcel(narrativeTable, sheet, ct);
            if("1".equals(reportValidSections.getEvaluationDetails()))
            {
                ct = exportRichTableToExcel(evaluationDetailsTable, sheet, ct);
                ct = exportRichTableToExcel(evaluationSectorsTable, sheet, ct);
            }
            response.setHeader("Content-disposition", "attachment;filename=\"" + filename + ".xls\"");
            out = response.getOutputStream();
            HSSFRow row = sheet.createRow(ct + 5);
            HSSFCell cell = row.createCell(0);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Exported on");
            cell = row.createCell(1);
            cell.setCellStyle(bodyStyle);
            Date d = new Date();
            SimpleDateFormat df = new SimpleDateFormat("MMM-dd-yyyy");
            cell.setCellValue(df.format(d));


            workbook.write(out);
            out.flush();

        } catch (IOException ex) {
            ex.printStackTrace();
        }


    }

    private int exportRichTableToExcel(RichTable richTable, HSSFSheet sheet, int rowCT) {
        if (richTable instanceof UIXCollection) {
            UIXCollection collection = (UIXCollection)richTable;
            Object oldRowKey = collection.getRowKey();
            try {
                int rowCount = collection.getRowCount();
                /**
                 * Column Headers
                 */
                if (rowCount > 0) {
                    int rowPos = rowCT;
                    HSSFRow row = sheet.createRow(rowPos);
                    int colPos = 0;
                    List<UIComponent> tblcolumn = collection.getChildren();
                    for (int i = 0; i < tblcolumn.size(); i++) {
                        if (tblcolumn.get(i) instanceof RichColumn) {
                            RichColumn cInstance = (RichColumn)tblcolumn.get(i);
                            try {
                                int t[] = printColumnHeaderFromRichColumn(cInstance, sheet, row, rowPos, colPos);
                                //To keep track of the row pointer. The next row should be inserted at rowCT+1
                                if (rowCT < t[0])
                                    rowCT = t[0];
                                //The next position to insert the column is colPos
                                colPos = t[1] + 1;
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    //Start Column merging for headers
                    for (int k = rowCT - 1; k >= rowPos; k--) {
                        HSSFRow hrow = sheet.getRow(k);
                        for (int i = 0; i < colPos; i++) {
                            if (hrow.getCell(i) != null) {
                                boolean flag = true;
                                for (int j = k + 1; j <= rowCT; j++) {
                                    HSSFRow trow = sheet.getRow(j);
                                    if (trow.getCell(i) != null)
                                        flag = false;
                                    else {
                                        HSSFCell cell = HSSFCellUtil.createCell(trow, i, null);
                                        cell.setCellStyle(headerStyle);
                                    }
                                }
                                if (flag) {
                                    sheet.addMergedRegion(new CellRangeAddress(k, rowCT, i, i));
                                }
                            }
                        }

                    }
                    //End Column merging for headers

                    //Insert data.
                    for (int ii = 0; ii < rowCount; ii++) {
                        row = sheet.createRow(++rowCT);
                        colPos = 0;
                        collection.setRowIndex(ii);
                        if (collection.getChildren() != null) {
                            if (collection.getChildren() instanceof List) {

                                tblcolumn = collection.getChildren();
                                for (int i = 0; i < tblcolumn.size(); i++) {
                                    RichColumn rChildColumn = (RichColumn)tblcolumn.get(i);
                                    try {
                                        if (rChildColumn.isRendered() && rChildColumn.isVisible()) {
                                            colPos = printColumnValues(rChildColumn, row, colPos);
                                        }

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }


                    }

                    //Set column width
                    for (int i = 0; i < colPos; i++) {
                        if (i < 3)
                            sheet.setColumnWidth(i, 20 * 256);
                        //sheet.autoSizeColumn((short)i);
                        else
                            sheet.setColumnWidth(i, 11 * 256);
                    }
                    row = sheet.createRow(++rowCT);
                    row = sheet.createRow(++rowCT);
                    rowCT++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            finally {
                collection.setRowKey(oldRowKey);
            }

        }
        return rowCT;
    }


    /**
     * Iterates through columns and prints the column header to the text file
     * @param writer
     * @param rColumn
     * @throws IOException
     */
    private int[] printColumnHeaderFromRichColumn(RichColumn rColumn, HSSFSheet sheet, HSSFRow rowP, int rowPos,
                                                  int colPos) throws IOException {
        int rtn[] = { rowPos, colPos };
        if (rColumn != null && rColumn.isRendered() && rColumn.isVisible()) {
            //  HSSFRow row=sheet.getRow(rowPos);
            if ((rColumn.getChildCount() == 1)) {
                if (rColumn.isRendered() && rColumn.isVisible()) {
                    HSSFCell cell = HSSFCellUtil.createCell(rowP, colPos, null);
                    if (rColumn.getHeaderText() != null)
                        cell.setCellValue(rColumn.getHeaderText());
                    cell.setCellStyle(headerStyle);
                    rtn[0] = rowPos;
                    rtn[1] = colPos;
                }
            } else {
                HSSFCell cell = HSSFCellUtil.createCell(rowP, colPos, null);
                if (rColumn.getHeaderText() != null)
                    cell.setCellValue(rColumn.getHeaderText());
                cell.setCellStyle(headerStyle);
                int headerXPos = rowPos;
                int headerYPos = colPos;
                int depth = rowPos;
                rowPos++;
                HSSFRow row = sheet.getRow(rowPos);
                if (row == null)
                    row = sheet.createRow(rowPos);
                for (Object o1 : rColumn.getChildren()) {
                    if (o1 instanceof RichColumn) {
                        RichColumn rChildColumn = (RichColumn)o1;
                        rtn = printColumnHeaderFromRichColumn(rChildColumn, sheet, row, rowPos, colPos);
                        colPos = rtn[1] + 1;
                        if (rtn[0] > depth)
                            depth = rtn[0];
                    }
                }
                rtn[0] = depth;

                for (int j = headerYPos + 1; j < colPos; j++) {
                    cell = HSSFCellUtil.createCell(rowP, j, null);
                    cell.setCellStyle(headerStyle);
                }
                sheet.addMergedRegion(new CellRangeAddress(headerXPos, headerXPos, headerYPos, colPos - 1));
            }
        } else
            rtn[1] = rtn[1] - 1;

        return (rtn);
    }

    private int printColumnValues(RichColumn rColumn, HSSFRow row, int colPos) throws IOException {
        if (rColumn != null) {
            if (rColumn.getChildCount() == 1) {
                if (rColumn.isRendered() && rColumn.isVisible()) {
                    int flag = 0;
                    List<UIComponent> cList = rColumn.getChildren();
                    if (cList.size() == 1 && cList.get(0) instanceof UIXGroup)
                        cList = ((UIXGroup)cList.get(0)).getChildren();
                    for (Object c1 : cList) {
                        UIComponent ui1 = (UIComponent)c1;
                        if (!ui1.isRendered())
                            continue;
                        else
                            flag = 1;
                        if (c1 instanceof RichOutputText) {
                            RichOutputText cROT = (RichOutputText)c1;
                            HSSFCell cell = HSSFCellUtil.createCell(row, colPos++, null);
                            if (cROT.getValue() != null) {
                                if ("java.lang.Integer".equals(cROT.getValue().getClass().getName()))
                                    cell.setCellValue(((Integer)cROT.getValue()));
                                else if ("java.lang.Long".equals(cROT.getValue().getClass().getName()))
                                    cell.setCellValue(((Long)cROT.getValue()).intValue());
                                    else if(cROT.getValue() instanceof oracle.jbo.domain.Number)
                                    cell.setCellValue(((oracle.jbo.domain.Number)cROT.getValue()).doubleValue());
                                else
                                    cell.setCellValue(cROT.getValue().toString());
                            }
                            cell.setCellStyle(bodyStyle);
                        }
                        if (c1 instanceof RichInputNumberSpinbox) {
                            RichInputNumberSpinbox cRINSB = (RichInputNumberSpinbox)c1;
                            HSSFCell cell = HSSFCellUtil.createCell(row, colPos++, null);
                            if (cRINSB.getValue() != null) {
                                if ("java.lang.Integer".equals(cRINSB.getValue().getClass().getName()))
                                    cell.setCellValue(((Integer)cRINSB.getValue()));
                                else if ("java.lang.Long".equals(cRINSB.getValue().getClass().getName()))
                                    cell.setCellValue(((Long)cRINSB.getValue()).intValue());
                                else if(cRINSB.getValue() instanceof oracle.jbo.domain.Number)
                                    cell.setCellValue(((oracle.jbo.domain.Number)cRINSB.getValue()).doubleValue());
                                else
                                    cell.setCellValue(cRINSB.getValue().toString());
                            }
                            cell.setCellStyle(bodyStyle);
                        }
                        if (c1 instanceof RichTextEditor) {
                            RichTextEditor cRTE = (RichTextEditor)c1;
                            HSSFCell cell = HSSFCellUtil.createCell(row, colPos++, null);
                            if (cRTE.getValue() != null) {
                                cell.setCellValue(cRTE.getValue().toString());
                            }
                            cell.setCellStyle(bodyStyle);
                        }
                        if (c1 instanceof RichOutputFormatted) {
                            RichOutputFormatted cRTE = (RichOutputFormatted)c1;
                            HSSFCell cell = HSSFCellUtil.createCell(row, colPos++, null);
                            if (cRTE.getValue() != null) {
                                cell.setCellValue(cRTE.getValue().toString());
                            }
                            cell.setCellStyle(bodyStyle);
                        }
                        if (c1 instanceof RichSelectOneChoice) {
                            RichSelectOneChoice cRSOC = (RichSelectOneChoice)c1;
                            for (Object c2 : cRSOC.getChildren()) {
                                if (c2 instanceof UISelectItems) {
                                    UISelectItems c2UISI = (UISelectItems)c2;
                                    if (c2UISI.getValue() instanceof List) {
                                        List selectItemArrayList = (List)c2UISI.getValue();
                                        int soc_data=0;
                                        for (Object o3 : selectItemArrayList) {
                                            if (o3 instanceof SelectItem) {
                                                SelectItem o3SI = (SelectItem)o3;
                                                if (o3SI.getValue() instanceof Integer) {
                                                    Integer x1 = (Integer)cRSOC.getValue();
                                                    Integer x2 = (Integer)o3SI.getValue();
                                                    if (x1!=null && x1.equals(x2)) {
                                                        HSSFCell cell = HSSFCellUtil.createCell(row, colPos++, null);
                                                        if (o3SI.getLabel() != null)
                                                            cell.setCellValue(o3SI.getLabel());
                                                        cell.setCellStyle(bodyStyle);
                                                        soc_data=1;
                                                    }
                                                }
                                            }
                                        }
                                        if(soc_data==0) {
                                            HSSFCell cell = HSSFCellUtil.createCell(row, colPos++, null);
                                            cell.setCellValue("");
                                            cell.setCellStyle(bodyStyle);
                                        }
                                    }
                                }
                                else if (c2 instanceof RichSelectItem) {
                                    RichSelectItem o3SI = (RichSelectItem)c2;
                                    if(cRSOC.getValue()!=null && o3SI.getValue()!=null)
                                    {
                                    String x1 = cRSOC.getValue().toString();
                                    String x2 = (String)o3SI.getValue().toString();
                                    if (x1.equals(x2)) {

                                        HSSFCell cell = HSSFCellUtil.createCell(row, colPos++, null);
                                        if (o3SI.getLabel() != null)
                                            cell.setCellValue(o3SI.getLabel());
                                        cell.setCellStyle(bodyStyle);
                                    }
                                    }
                                    else
                                    {
                                    HSSFCell cell = HSSFCellUtil.createCell(row, colPos++, null);
                                    cell.setCellValue("");
                                    cell.setCellStyle(bodyStyle);
                                    break;
                                    }
                                }
                                else
                                {
                                HSSFCell cell = HSSFCellUtil.createCell(row, colPos++, null);
                                cell.setCellValue("");
                                cell.setCellStyle(bodyStyle);
                                }
                            }
                        }
                    }
                    if (flag == 0) {
                        HSSFCell cell = HSSFCellUtil.createCell(row, colPos++, null);
                        cell.setCellValue("");
                        cell.setCellStyle(bodyStyle);
                    }
                }
            } else {
                for (Object o1 : rColumn.getChildren()) {
                    if (o1 instanceof RichColumn) {
                        RichColumn rChildColumn = (RichColumn)o1;
                        colPos = printColumnValues(rChildColumn, row, colPos);
                    }
                }
            }
        }
        return colPos;
    }

    public void setReportTable(RichTable reportTable) {
        this.reportTable = reportTable;
    }

    public RichTable getReportTable() {
        return reportTable;
    }

    public void setSectorTable(RichTable sectorTable) {
        this.sectorTable = sectorTable;
    }

    public RichTable getSectorTable() {
        return sectorTable;
    }

    public void setIndicatorTable(RichTable indicatorTable) {
        this.indicatorTable = indicatorTable;
    }

    public RichTable getIndicatorTable() {
        return indicatorTable;
    }

    public Integer getCurrentFy() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl = bindings.getOperationBinding("getCurrentFy");
        Integer tmp = (Integer)ctrl.execute();
        this.currentFy = tmp;
        return currentFy;
    }

    public void setReportDetailsTable(RichTable reportDetailsTable) {
        this.reportDetailsTable = reportDetailsTable;
    }

    public RichTable getReportDetailsTable() {
        return reportDetailsTable;
    }

    public void execReportValidSections() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("getReportValidSections");
        ReportValidSections rvs = (ReportValidSections) exec.execute();
        if(rvs!=null) {
            this.reportValidSections=rvs;
        }
        else {
            try {
                ReportValidSections t = new ReportValidSections();
                t.setDefaultValues();
                this.reportValidSections=t;
            } catch (SQLException e) {
            }
        }
    }

    public void setNarrativeTable(RichTable narrativeTable) {
        this.narrativeTable = narrativeTable;
    }

    public RichTable getNarrativeTable() {
        return narrativeTable;
    }

    public void setEvaluationDetailsTable(RichTable evaluationDetailsTable) {
        this.evaluationDetailsTable = evaluationDetailsTable;
    }

    public RichTable getEvaluationDetailsTable() {
        return evaluationDetailsTable;
    }

    public void setEvaluationSectorsTable(RichTable evaluationSectorsTable) {
        this.evaluationSectorsTable = evaluationSectorsTable;
    }

    public RichTable getEvaluationSectorsTable() {
        return evaluationSectorsTable;
    }

    public void setKeywordIndicatorsTable(RichTable keywordIndicatorsTable) {
        this.keywordIndicatorsTable = keywordIndicatorsTable;
    }

    public RichTable getKeywordIndicatorsTable() {
        return keywordIndicatorsTable;
    }

    private static class EncodingState {
        private ResponseWriter _rw;

        private EncodingState(ResponseWriter rw) {
            this._rw = rw;
        }
    }


}
