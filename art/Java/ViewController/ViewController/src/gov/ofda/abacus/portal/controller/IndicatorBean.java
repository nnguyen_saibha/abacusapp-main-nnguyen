package gov.ofda.abacus.portal.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputTextarea;

import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichToolbar;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import oracle.jbo.uicli.binding.JUCtrlHierTypeBinding;
import oracle.jbo.uicli.binding.JUIteratorBinding;

import org.apache.myfaces.trinidad.component.UIXSwitcher;
import org.apache.myfaces.trinidad.event.ReturnEvent;
import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.model.RowKeySetImpl;
import org.apache.myfaces.trinidad.model.TreeModel;

public class IndicatorBean extends UIControl {
    private RichTreeTable indicatorTree;
    private RichPanelFormLayout parIndicatorForm;
    private RichTable addIndicatorTable;
    private HtmlInputTextarea newIndicatorJustification;
    private UIXSwitcher facetSwitcher;
    private String facetName = "other";
    private RowKeySetImpl newDisclosedTreeTableKeys = null;
    private RowKeySetImpl newDisclosedIndicatorTableKeys = null;

    public void setNewDisclosedIndicatorTableKeys(RowKeySetImpl newDisclosedIndicatorTableKeys) {
        this.newDisclosedIndicatorTableKeys = newDisclosedIndicatorTableKeys;
    }

    public RowKeySetImpl getNewDisclosedIndicatorTableKeys() {
        if(newDisclosedIndicatorTableKeys==null) {
            CollectionModel model = (CollectionModel) JSFUtils.resolveExpression("#{bindings.ParsitypeView1.treeModel}");
            newDisclosedIndicatorTableKeys = new RowKeySetImpl();
            if (model != null) {

                JUCtrlHierBinding treeBinding = (JUCtrlHierBinding)model.getWrappedData();
                JUCtrlHierNodeBinding nodeBinding = treeBinding.getRootNodeBinding();
                expandAllNodes(nodeBinding, newDisclosedIndicatorTableKeys, 0, 3);
        }
        }
        return newDisclosedIndicatorTableKeys;
    }
    private RichToolbar treeToolbar;

    public IndicatorBean() {
        super();
    }

    public void setIndicatorTree(RichTreeTable indicatorTree) {
        this.indicatorTree = indicatorTree;
    }

    public RichTreeTable getIndicatorTree() {
        return indicatorTree;
    }

    public void setParIndicatorForm(RichPanelFormLayout parIndicatorForm) {
        this.parIndicatorForm = parIndicatorForm;
    }

    public RichPanelFormLayout getParIndicatorForm() {
        return parIndicatorForm;
    }

    /**
     * Called when AddIndicator popup is called.
     * It calls removeExistingIndicators in AwardModuleImpl which removes the existing
     * indicators from the poup table.
     * @param popupFetchEvent
     */
    public void addIndicatorPopup(PopupFetchEvent popupFetchEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl_addsec = bindings.getOperationBinding("removeExistingIndicators");
        ctrl_addsec.execute();
    }

    /**
     * Called when user exists out of AddIndicator Dialog.
     * If user click on ok, this method checks if an indicator is selected. If so
     * then will call addNewIndicator in AwardModuleImpl
     * It then displays a popup the new indicator has been added.
     * @param dialogEvent
     */
    public void indicatorDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {

            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = (DCIteratorBinding)bindings.findIteratorBinding("ParsectorindicatorView1Iterator");
            Key key=null;
            if (iter != null && iter.getCurrentRow() != null)
                key = iter.getCurrentRow().getKey();
            OperationBinding ctrl = bindings.getOperationBinding("addNewIndicator");
            Boolean isAdded = (Boolean)ctrl.execute();
            if (isAdded) {
                ctrl = bindings.getOperationBinding("Commit");
                ctrl.execute();
                if (ctrl.getErrors().isEmpty()) {
                  //  ctrl = bindings.getOperationBinding("populateStageTable");
                  //  ctrl.execute();
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Indicators Added", "Indicators added and saved successfully");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
                ctrl = bindings.getOperationBinding("ExecuteSubsectorList");
                ctrl.execute();
                ctrl = bindings.getOperationBinding("ExecuteIndicator");
                ctrl.execute();
                if (this.getIndicatorTree() != null) {
                    if(key!=null) {
                        OperationBinding exec = bindings.getOperationBinding("ExecuteIndicator");
                        exec.execute();
                        iter = (DCIteratorBinding)bindings.findIteratorBinding("ParsectorindicatorView1Iterator");
                        RowSetIterator rsi = iter.getRowSetIterator();
                        if (rsi.findByKey(key, 1).length > 0)
                            iter.setCurrentRowWithKey(key.toStringFormat(true));
                        
                    }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(indicatorTree);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(parIndicatorForm);
                }
            }

        }
    }

    public void executeAddIndicator(ReturnEvent returnEvent) {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding ctrl = bindings.getOperationBinding("ExecuteAwardIndicatorLookup");
        ctrl.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(addIndicatorTable);

    }

    public void setAddIndicatorTable(RichTable addIndicatorTable) {
        this.addIndicatorTable = addIndicatorTable;
    }

    public RichTable getAddIndicatorTable() {
        return addIndicatorTable;
    }

    public void setNewIndicatorJustification(HtmlInputTextarea newIndicatorJustification) {
        this.newIndicatorJustification = newIndicatorJustification;
    }

    public HtmlInputTextarea getNewIndicatorJustification() {
        return newIndicatorJustification;
    }

    public void deleteIndicator(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding delete = bindings.getOperationBinding("DeleteIndicator");
            delete.execute();
            OperationBinding commit = bindings.getOperationBinding("Commit");
            commit.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(indicatorTree);
            AdfFacesContext.getCurrentInstance().addPartialTarget(parIndicatorForm);
            facetName = "other";
            AdfFacesContext.getCurrentInstance().addPartialTarget(facetSwitcher);
            if (commit.getErrors().isEmpty()) {
             //   OperationBinding ctrl = bindings.getOperationBinding("populateStageTable");
             //   ctrl.execute();
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Indicator Deleted", "Indicator deleted successfully");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        }

    }

    /**
     * Treetable selection listener for the sector/sub-sector/indicator tree
     * If there are unsaved changes alter the user to save changes, if not call the selectionEvent
     * that sets the facet name to show based on selected row.
     * @param selectionEvent
     */
    public void sectorTreeSelectionListener(SelectionEvent selectionEvent) {
        String nodeStrutureDefname = this.onTreeTableSelect(selectionEvent);
        this.setFacetName(nodeStrutureDefname);
        AdfFacesContext.getCurrentInstance().addPartialTarget(facetSwitcher);
        AdfFacesContext.getCurrentInstance().addPartialTarget(treeToolbar);

    }

    public void treeSelectionLsnr(SelectionEvent e) {
        UIControl.resolveMethodExpression("#{bindings.ParsectorView1.treeModel.makeCurrent}", null,
                                             new Class[] { SelectionEvent.class },new Object[] { e });
       
             String nodeStrutureDefname = this.onTreeTableSelect(e);
            this.setFacetName(nodeStrutureDefname);
            AdfFacesContext.getCurrentInstance().addPartialTarget(facetSwitcher);
            AdfFacesContext.getCurrentInstance().addPartialTarget(treeToolbar);
            
            if("gov.ofda.abacus.portal.model.entityViews.ParsectorindicatorView".equals(nodeStrutureDefname)) {
                RowKeySet rowKeySet = e.getAddedSet(); 
                Iterator rksIterator = rowKeySet.iterator();
                if(rksIterator.hasNext()) {
                    List key = (List) rksIterator.next();
                    if(key!=null && key.size()==3)
                    {
                     DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
                        OperationBinding exec = bindings.getOperationBinding("ExecuteIndicator");
                        exec.getParamsMap().put("p_parsiseq", ((Key)key.get(2)).getAttribute(0));
                        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("parSectorIndicatorRowKey",key.get(2));
                        exec.execute();
                        this.newDisclosedIndicatorTableKeys=null;
                    }
                    
                }
            }

    }

    public void setFacetName(String facetName) {
        this.facetName = facetName;
    }

    public String getFacetName() {
        return facetName;
    }

    public void setFacetSwitcher(UIXSwitcher facetSwitcher) {
        this.facetSwitcher = facetSwitcher;
    }

    public UIXSwitcher getFacetSwitcher() {
        return facetSwitcher;
    }
    //http://www.oracle.com/technetwork/developer-tools/adf/learnmore/78-man-expanding-trees-treetables-354775.pdf

    public void setNewDisclosedTreeTableKeys(RowKeySetImpl newDisclosedTreeTableKeys) {
        this.newDisclosedTreeTableKeys = newDisclosedTreeTableKeys;
    }

    /**
     * http://www.oracle.com/technetwork/developer-tools/adf/learnmore/78-man-expanding-trees-treetables-354775.pdf
     * getter for disclosed keys. Determines which rows are disclosed in the tree table
     * It discloses all rows upto 4 levels.
     * @return
     */
    public RowKeySetImpl getNewDisclosedTreeTableKeys() {
        if (newDisclosedTreeTableKeys == null) {
            newDisclosedTreeTableKeys = new RowKeySetImpl();
            FacesContext fctx = FacesContext.getCurrentInstance();
            UIViewRoot root = fctx.getViewRoot();
            //lookup the tree table component by its component ID
            RichTreeTable treeTable = this.getIndicatorTree();
            //if tree table is found
            if (treeTable != null) {
                //get the collection model to access the ADF binding layer for
                ////the tree binding used
                CollectionModel model = (CollectionModel)treeTable.getValue();
                JUCtrlHierBinding treeBinding = (JUCtrlHierBinding)model.getWrappedData();
                JUCtrlHierNodeBinding nodeBinding = treeBinding.getRootNodeBinding();
                expandAllNodes(nodeBinding, newDisclosedTreeTableKeys, 0, 4);
            }
        }
        return newDisclosedTreeTableKeys;
    }

    /**
     * Called to add keys of children in a tree to be disclosed.
     * This is a recursive call that uses maxExpandLevel to determine when to stop.
     * @param nodeBinding
     * @param disclosedKeys
     * @param currentExpandLevel
     * @param maxExpandLevel
     */
    private void expandAllNodes(JUCtrlHierNodeBinding nodeBinding, RowKeySetImpl disclosedKeys, int currentExpandLevel,
                                int maxExpandLevel) {
        if (currentExpandLevel <= maxExpandLevel) {
            List<JUCtrlHierNodeBinding> childNodes = (List<JUCtrlHierNodeBinding>)nodeBinding.getChildren();
            ArrayList newKeys = new ArrayList();
            if (childNodes != null) {
                for (JUCtrlHierNodeBinding _node : childNodes) {
                    newKeys.add(_node.getKeyPath());
                    expandAllNodes(_node, disclosedKeys, currentExpandLevel + 1, maxExpandLevel);
                }
            }
            disclosedKeys.addAll(newKeys);
        }
    }

    public void setTreeToolbar(RichToolbar treeToolbar) {
        this.treeToolbar = treeToolbar;
    }

    public RichToolbar getTreeToolbar() {
        return treeToolbar;
    }
}
