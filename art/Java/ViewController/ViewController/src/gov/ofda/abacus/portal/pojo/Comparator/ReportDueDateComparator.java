package gov.ofda.abacus.portal.pojo.Comparator;

import gov.ofda.abacus.portal.pojo.Report;

import java.util.Comparator;

public class ReportDueDateComparator implements Comparator<Report>{
    public ReportDueDateComparator() {
        super();
    }

    @Override
    public int compare(Report report, Report report2) {
        if(report.getRPT_DUE_DATE()==null)
            return 1;
        if(report2.getRPT_DUE_DATE()==null)
            return -1;
        if(report.getRPT_DUE_DATE().compareTo(report2.getRPT_DUE_DATE())==0)
            return (report.getRPT_TYPE_CODE().compareTo(report2.getRPT_TYPE_CODE()));
        else
            return (report.getRPT_DUE_DATE().compareTo(report2.getRPT_DUE_DATE()));
    }
}
