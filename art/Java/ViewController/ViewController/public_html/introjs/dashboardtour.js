    function startTour() {
          var intro = introJs();
          setIntroOptions(intro)
         intro.onbeforechange(function(targetElement){
              if(intro._currentStep >=10 && intro._currentStep <=13 &&  document.getElementById('pt1:r1:0:pgl65')==null)
              {
                   var bene = document.getElementById('pt1:r1:0:tab9::disAcr');
                   var currentStep=intro._currentStep;
                   bene.click(setTimeout(function(){intro.exit();setIntroOptions(intro);intro.start().goToStepNumber(currentStep+1);},1000));
              }
              if(intro._currentStep>=14 && intro._currentStep <=18 && document.getElementById('pt1:r1:0:pgl66')==null)
              {
                   bene = document.getElementById('pt1:r1:0:tab10::disAcr');
                   currentStep=intro._currentStep;
                   bene.click(setTimeout(function(){intro.exit();setIntroOptions(intro);intro.start().goToStepNumber(currentStep+1);},1000));
              }
          });
          intro.start();
    }
function setIntroOptions(intro) {
    intro.setOption('showProgress', true);
    intro.setOption('showStepNumbers', true);
    intro.setOption('tooltipPosition', 'auto');
    intro.setOption('disableInteraction', true);
    intro.setOption('positionPrecedence', ['bottom','left', 'right', 'top']);
    intro.setOptions({
            steps: [
              { //Release notes
                
                intro: "<p class=\"introp\"><b>Release V19.3 Award Level Beneficiaries and Report Improvements</b></p><ul>" +
                "<li>Dashboard now shows beneficiaries at award level.</li>" +
                "<li>Dashboard will now show all reports created by partners, including custom reports.</li>" +
                "<li>All Reports prior to Fiscal Year 2018 are no longer tracked.</li>"+
                "<li>Any reports submitted prior to Fiscal Year 2018 will have new Submit Status: Not Tracked.</li>"+
                "<li>New features <ul>" +
                    "<li>My Favorites, enabling users to save Dashboard preferences.</li>"+
                    "<li>Support for multiple favorites, with option to display a default favorite on next login</li>"+
                    "<li>Guided Dashboard Tour</li>"+
                    "<li>Export data to Excel at award, report, sector and beneficiary level</li>"+
                    "<li>View Award Documents shows all documents in one place</li>"+
                    "<li>Ability to sort data</li>"+
                    "<li>Show/Hide Charts & Filters</li>"+
                "</ul></li>"+
                "<li>Users can utilize new filters on the left side of the page to filter data by award and report.</li>"+
                "<li>New filter options are added at award and report level</li>"+
                "</ul><i> To view this again click on <b>Dashboard Tour</b></i>"
              },
              { //Search field
                element: document.getElementById('pt1:r1:0:it1'),
                intro: "In the full-text search field, users can enter Region, Country, Project, Award Number, Award Status, Report Status, Report Type, or Report Frequency and see matching results in the Reports section below. The filterable data charts also update to match the search text results. Click on <b>Search</b> to view results."
              },
              { //Dashboard menu
                element: document.getElementById('pt1:r1:0:t8'),
                intro: "The Dashboard menu provides various options and features to control Dashboard view and behavior."
              },
              { //Reset
                element: document.getElementById('pt1:r1:0:b11'),
                intro: "Users can reset all selections, including search fields, filters, sort criteria, tab selections, and results by clicking the <b>Reset</b> button."
              },
              { //Sort
                element: document.getElementById('pt1:r1:0:b5'),
                intro: "Users can sort search results by clicking on the <b>Sort</b> button. Users can sort by Region, Country, Project, Award Number, Award Amount, Award Start Date, Award End Date, and Award Duration."
              },
                {//Show/Hide
                element: document.getElementById('pt1:r1:0:b6'),
                intro: "Show/Hide Filters and Charts enables users to customize their view. By hiding filters and charts, users can focus on results. This is useful when working with small resolution screens."
              },
                {//Export
                element: document.getElementById('pt1:r1:0:m1'),
                intro: "<p>Export to Excel provides users with the option to export all reports to Excel. Users can export award details, report details, award level beneficiaries, or award and sector level beneficiaries.</p>"
              },
                { //Favorites
                element: document.getElementById('pt1:r1:0:m2'),
                intro: '<p class=\"introp300\">Using My Favorites, users can customize the Home/Dashboard screen with search text, filters, sort options, and visible tabs. Users can create multiple favorites and can define one as their Default Favorite. <br><br><b>Next time the user logs in, the Home/Dashboard screen will show the default favorite preference stored by the user.</b> <br><br>Use Manage favorites to change the default or change or delete your favorites.</p><br> <img src=images/favorites_demo.png alt="Favorites screenshot">'
              },
              { //Filters
                element: document.getElementById('pt1:r1:0:ps1::f'),
                intro: 'Filters are organized into two tabs Award and Report. Filters are grouped by type, Users can select more than one filter for each group.'
              },
              { //Fiscal Year
                element: document.getElementById('pt1:r1:0:tab9::ti::_afrTabCnt'),
                intro: "Filter by Award shows submitted reports percentage, along with visual interactive charts. Clicking on a specific data point in the chart will apply a filter, similar to selecting filters on the left."
              },
              { //Fiscal Year
                element: document.getElementById('pt1:r1:0:pgl65'),
                intro: "Users can define the Fiscal Year parameters by using From and To drop down options to select desired years. The default view shows active awards during current and last fiscal year."
              },
              {
                element: document.getElementById('pt1:r1:0:pgl10'),
                intro: "The Report Submitted by Award graph represents the percentage of awards that have required reports submitted(excludes Due within 30 Days, Not Due and Not Tracked reports when calculating percentage). It shows percentage as well as the associated count of all awards for the search parameters selected."
              },
              { // Award Required Percent
                element: document.getElementById('pt1:r1:0:pgl30'),
                intro: 'Shows number of awards by Award Status. Award Status categories include <ol><li><b>In Progress:</b> Today\'s date is less than Award End Date (Previously identified as Active).</li><li><b>Expired:</b> Award End Date has passed.</li><li><b>Closed:</b> Official Agency Award Closeout.</li></ol>'
              },
              { //Award tabs
                element: document.getElementById('pt1:r1:0:pt1'),
                intro: 'These tabs include interactive bar charts that show the number of awards by each category. Users can click on a bar to filter the results. The bar charts are alphabetically ordered by name and show the first 5-6 bars by default.  An overview slider is shown below some charts (Region/Country/Project...) that enables users to click and resize or drag to navigate to a specific location within the chart. Users can also zoom in and out of the chart by using the scroll wheel on the mouse.'
              },
              
              { //Results (Reports)
                element: document.getElementById('pt1:r1:0:tab10::ti::_afrTabCnt'),
                intro: 'Users can also use Filter by Report to filter reports by report type, reporting year, frequency and report status.'
              },{ //Fiscal Year
                element: document.getElementById('pt1:r1:0:pgl66'),
                intro: "Users can define the Fiscal Year parameters by using From and To drop down options to select desired years. The default view shows active awards during current and last fiscal year."
              },
              {
                element: document.getElementById('pt1:r1:0:pgl34'),
                intro: "The Reports Submitted graph represents the percentage of required reports that have been submitted (Submitted On Time and Submitted Late). Reports with status Due within 30 days, Not Due, and Not Tracked reports are not taken into account when calculating percentage."
              },
              { // Award Required Percent
                element: document.getElementById('pt1:r1:0:pgl33'),
                intro: '<p class=\"introp\">Shows number of reports by Report Status.</p> <b>Report Submit Status info</b><ul><li><b>Submitted On Time</b> - Report submitted on/before report due date.</li><li><b>Submitted Late</b> - Report submitted after report due date.</li><li><b>Over Due</b> - Reports not yet submitted but past report due date.</li><li><b>Due within 30 Days</b> - Report due soon within 30 days or less.</li><li><b>Not Due</b>- Report is required but is not yet due.</li><li><b>Not Tracked</b> - Not Tracked includes any reports prior to Fiscal Year 2018 and any reports that are not program, financial, baseline, and evaluation reports.</li></ul>'
              },
              { //Award tabs
                element: document.getElementById('pt1:r1:0:pt5'),
                intro: 'These tabs include interactive bar charts that show the number of reports by each category. Users can click on a bar to filter the results. The bar charts are alphabetically ordered by name and show the first 5-6 bars by default.  An overview slider is shown below some charts (Reports Due by Year/Month, Report Type/Frequency, Sector) that enables users to click and resize or drag to navigate to a specific location within the chart. Users can also zoom in and out of the chart by using the scroll wheel on the mouse.'
              },{ //Results (Reports)
                element: document.getElementById('pt1:r1:0:tab2'),
                intro: '<ul>' +
                '<li>In this section, users can see the results of the full-text search and any filters that are applied to the awards and its reports.</li>' +
                '<li>All reports for an award are grouped and shown as a table list under each award.</li>' +
                '<li>Awards are shown in pages, with each page showing 10 awards at a time. Click on page numbers to navigate.</li>' +
                '</ul>'
              },
               { //Sample Table
                element: document.getElementById('pt1:r1:0:tab7::ti'),
                intro: 'In Beneficiary tab, beneficiary information at report and sector level are shown for each award. Table shows baseline, cumulative targeted, reporting period reached, cumulative reached, and system calculated cumulative reached (calculated by adding current and all previous reporting period reached values with reports ordered by due date). Cumulative reached and system calculated cumulative reached should always match, including numbers from the Final Report.<br>Below is sample table to see how beneficiary information must be entered.<br><img src=images/beneficiary_sample_demo.png alt="Beneficiaries" width="900" height="300">',
                postion: 'bottom-left-aligned'
              },
               { //Sample chart
                element: document.getElementById('pt1:r1:0:tab7::ti'),
                intro: 'Below is sample chart to see how the line chart will look if all information is correctly entered. Note that cumulative reached and system calculated cumulative reached are aligned. The system does not display a chart if no reports are submitted.<br><img src=images/bene_line_chart_demo.png alt="Beneficiaries line chart">',
                position: 'bottom-left-aligned'
              },{ //Fiscal Year
                    intro: "<p style=\"text-align:center;font-weight:bold;font-size:16px;\">End of Tour.<br><span style=\"font-size:50px;\">&#x1F44F&#x1F44F&#x263A&#x263A&#x1F44B&#x1F44B</span><br>You can always return by clicking on <b>Dashboard Tour</b>.</p>"
                  }
            ]
          });
}