CREATE OR REPLACE 
PACKAGE appx_pkg
IS
TYPE pars_type is REF CURSOR Return parsector%rowtype;
TYPE parsi_type is REF CURSOR Return parsectorindicator%rowtype;
FUNCTION proposal_order(action_type varchar2) RETURN number;
FUNCTION par_status(parseq number) RETURN VARCHAR2;
Function new_report(p_awid varchar2,nbr_or_date varchar2) return varchar2;
FUNCTION is_report_complete(p_par integer) return clob;
END;
/
