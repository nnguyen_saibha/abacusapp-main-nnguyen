drop table parsector
/
-- Start of DDL Script for Table PARSECTOR

CREATE TABLE parsector
    (parsectorseq                  NUMBER ,
    parseq                         NUMBER,
    sector_code                    VARCHAR2(5) NOT NULL,
    sector                         VARCHAR2(60),
    bene_tot_target                NUMBER(10),
    bene_idp_target                NUMBER(10),
    bene_tot_reached               NUMBER(10),
    bene_idp_reached               NUMBER(10),
    cum_bene_tot_target            NUMBER,
    cum_bene_idp_target            NUMBER,
    cum_bene_tot_reached           NUMBER,
    cum_bene_idp_reached           NUMBER,
    comments                       VARCHAR2(2000),
    upd_id                     	   VARCHAR2(20) default user,
    upd_date                       DATE DEFAULT sysdate)
  PCTFREE     10
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  APPXDBT_TBS
  STORAGE   (INITIAL    1M    NEXT     5M)
  NOCACHE
  MONITORING
  NOPARALLEL
  LOGGING
/


-- Constraints for PARSECTOR

ALTER TABLE parsector
ADD CONSTRAINT pk_parsector PRIMARY KEY (parsectorseq)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  APPXDBT_INDX_TBS
  STORAGE   (INITIAL    1M    NEXT   1M)
/

-- Foreign Key
ALTER TABLE parsector
ADD CONSTRAINT fk_par_parsector FOREIGN KEY (parseq)
REFERENCES par (parseq)
/


-- Start of DDL Script for Sequence PARSECTORSEQ
-- Drop the old instance of PARSECTORSEQ
DROP SEQUENCE parsectorseq
/

CREATE SEQUENCE parsectorseq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 99999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

-- End of DDL Script for Sequence PARSECTORSEQ

-- Triggers for PARSECTOR

CREATE OR REPLACE TRIGGER PARSECTOR_SEQ_TRIGG     
before insert on "PARSECTOR"    for each row
begin     
   if inserting then       
     if :NEW."PARSECTORSEQ" is null then          
        select PARSECTORSEQ.nextval into :NEW."PARSECTORSEQ" from dual; 
     end if;    
   end if; 
end;
/


-- End of DDL Script for Table PARSECTOR

