-- Start of DDL Script for Table RPT_DOCS

drop table rpt_docs
/

CREATE TABLE RPT_DOCS
    (docseq                        NUMBER NOT NULL,
    parseq                         NUMBER,
    doc_type_code                  VARCHAR2(10),
    file_format                    VARCHAR2(100),
    file_name                      VARCHAR2(500),
    file_desc                      VARCHAR2(1000),
    file_content_type              VARCHAR2(500),
    file_size                      VARCHAR2(100),
    file_contents                  BLOB,
    upd_id                         VARCHAR2(20) default user,
    upd_date                       DATE DEFAULT sysdate)
  PCTFREE     10
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  APPXDBT_TBS
  STORAGE   (INITIAL  2M    NEXT  5M)
  NOCACHE
  MONITORING
  LOB ("FILE_CONTENTS") STORE AS RPT_DOCS_FCONTENT
  (
  TABLESPACE  APPXDBT_DOCS_TBS
  STORAGE   (INITIAL    5M   NEXT        5M)
   NOCACHE LOGGING
   CHUNK 8192
   PCTVERSION 10
  )
  NOPARALLEL
  LOGGING
/

-- Constraints for RPT_DOCS

ALTER TABLE RPT_docs
ADD CONSTRAINT pk_RPT_docs PRIMARY KEY (docseq)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  APPXDBT_INDX_TBS
  STORAGE   (INITIAL 1M    NEXT    1M)
/

-- Foreign Key
ALTER TABLE rpt_docs
ADD CONSTRAINT fk_par_rptdocs FOREIGN KEY (parseq)
REFERENCES par (parseq)
/

-- Start of DDL Script for Sequence DOCSEQ
-- Drop the old instance of DOCSEQ

DROP SEQUENCE docseq
/

CREATE SEQUENCE docseq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 99999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

-- End of DDL Script for Sequence DOCSEQ

-- Triggers for RPT_DOCS

CREATE OR REPLACE TRIGGER DOCSSEQ_TRIGG     
before insert on "RPT_DOCS"    for each row
begin     
   if inserting then       
     if :NEW."DOCSEQ" is null then
        select DOCSEQ.nextval into :NEW."DOCSEQ" from dual; 
     end if;    
   end if; 
end;
/

-- End of DDL Script for Table RPT_DOCS
