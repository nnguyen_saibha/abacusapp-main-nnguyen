-- Start of DDL Script for Table PARSECTOR_AUDIT

DROP TABLE PARSECTOR_AUDIT
/
CREATE TABLE PARSECTOR_AUDIT
    (audit_nbr                     NUMBER ,
    audit_type                     VARCHAR2(5),
    audit_comments                 VARCHAR2(2000),
    PARSECTORSEQ                   NUMBER,
    PARSEQ                         NUMBER,
    SECTOR_CODE                    VARCHAR2(5),
    SECTOR                         VARCHAR2(60),
    BENE_TOT_TARGET                NUMBER(10),
    BENE_IDP_TARGET                NUMBER(10),
    BENE_TOT_REACHED               NUMBER(10),
    BENE_IDP_REACHED               NUMBER(10),
    cum_bene_tot_target            NUMBER,
    cum_bene_idp_target            NUMBER,
    cum_bene_tot_reached           NUMBER,
    cum_bene_idp_reached           NUMBER,
    COMMENTS                       VARCHAR2(2000),
    upd_id                         VARCHAR2(20),
    upd_date                       DATE)
  PCTFREE     10
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  APPXDBT_TBS
  STORAGE   (
    INITIAL     2M
    NEXT        5M
  )
  NOCACHE
  MONITORING
  NOPARALLEL
  LOGGING
/
-- Constraints for PTAUDIT

ALTER TABLE PARSECTOR_AUDIT
ADD CONSTRAINT pk_parsectoraudit PRIMARY KEY (audit_nbr)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  APPXDBT_INDX_TBS
  STORAGE   (
    INITIAL     1M
    NEXT        1M
  )
/
-- Sequence for psaudittrigg

CREATE SEQUENCE psauditseq
  INCREMENT BY 1
  START WITH 10
  MINVALUE 1
  MAXVALUE 99999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

-- End of DDL Script for Table PARSECTOR_AUDIT

---Trigg for PARSECTOR
CREATE OR REPLACE TRIGGER psaudittrigg
 AFTER
  DELETE OR UPDATE
 ON PARSECTOR
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Declare
TransType varchar2(10);
comment varchar2(2000);
Begin
IF UPDATING THEN
TransType:=' M ';
comment:= ' MODIFIED BY '||:new.upd_id ||' on '|| sysdate;
ELSIF DELETING THEN
TransType:=' D ';
comment:= ' DELETED BY '||user ||' on '|| sysdate;
END IF;
INSERT INTO PARSECTOR_AUDIT
 values
(psauditseq.NEXTVAL,TransType,comment,
:old.PARSECTORSEQ ,
:old.PARSEQ,
:old.SECTOR_CODE,
:old.SECTOR,
:old.BENE_TOT_TARGET,
:old.BENE_IDP_TARGET,
:old.BENE_TOT_REACHED,
:old.BENE_IDP_REACHED,
:old.cum_bene_tot_target,
:old.cum_bene_idp_target,
:old.cum_bene_tot_reached,
:old.cum_bene_idp_reached,
:old.comments,
:old.upd_id,
:old.upd_date) ;
END;
/
