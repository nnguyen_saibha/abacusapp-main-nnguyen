
-- Start of DDL Script for Table APPX.APPX_USER

-- Drop the old instance of APPX_USER
DROP TABLE appx_user
/

CREATE TABLE appx_user
    (userid                        VARCHAR2(15) NOT NULL,
    last_name                      VARCHAR2(30),
    first_name                     VARCHAR2(30),
    user_type                      CHAR(1) DEFAULT 'P' NOT NULL,
    awardee_code                   VARCHAR2(30),
    country                        VARCHAR2(30),
    email                          VARCHAR2(30),
    phone_nbr                      VARCHAR2(20),
    active                         CHAR(1) DEFAULT 'Y' NOT NULL,
    upd_id                     	   VARCHAR2(20) default user,
    upd_date                       DATE DEFAULT sysdate)
  PCTFREE     10
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  APPXDBT_TBS
  STORAGE   (INITIAL     2M    NEXT        1M)
  NOCACHE
  MONITORING
  NOPARALLEL
  LOGGING
/

-- Constraints for APPX_USER

ALTER TABLE APPX_USER 
ADD CONSTRAINT PK_APPX_USER PRIMARY KEY (userid)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  APPXDBT_INDX_TBS
  STORAGE   (INITIAL  1M    NEXT  1M)
/

ALTER TABLE appx_user
ADD CONSTRAINT ck_appx_user_type CHECK (user_type IN ('P','O','A'))
/

-- End of DDL Script for Table APPX_USER
