-- Start of DDL Script for Table PAR

drop table par
/

CREATE TABLE par     (
    parseq                     NUMBER ,   --pk
    awid                       NUMBER NOT NULL,
    rpt_type_code              NUMBER(5),    --FK for report type lookup
    prd_type_code              NUMBER(1), --FK for Period type lookup
    rpt_due_date               DATE,
    prd_start_date	       DATE,
    prd_end_date	       DATE,
    rpt_nbr                    VARCHAR2(20),
    rpt_desc                   VARCHAR2(3000),
    submit_by                  VARCHAR2(20),
    submit_date                DATE, 
    total_targeted             NUMBER(10,0),
    idp_targeted               NUMBER(10,0),
    total_reached              NUMBER(10,0),
    idp_reached                NUMBER(10,0),
    cum_total_targeted         NUMBER,
    cum_idp_targeted           NUMBER,
    cum_total_reached          NUMBER,
    cum_idp_reached            NUMBER,
    prev_rpt_amt               NUMBER,
    prd_amt                    NUMBER,
    fed_prev_rpt_amt           NUMBER,
    fed_prd_amt                NUMBER,
    iscomplete                 CHAR(1) DEFAULT 'N',
    issubmit                   CHAR(1) DEFAULT 'N',
    isvalid                    CHAR(1) DEFAULT 'N',
    upd_id                     VARCHAR2(20) default user,
    upd_date                   DATE DEFAULT sysdate
)
  PCTFREE     10
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  APPXDBT_TBS
  STORAGE   (INITIAL     2M     NEXT        5M)
  NOCACHE
  MONITORING
  NOPARALLEL
  LOGGING
/

-- Constraints for PAR

ALTER TABLE par 
ADD CONSTRAINT pk_par PRIMARY KEY (parseq)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  APPXDBT_INDX_TBS
  STORAGE   (INITIAL  1M    NEXT  1M)
/

--check constraints

ALTER TABLE par
ADD CONSTRAINT ck_par_iscomplete CHECK (iscomplete IN ('Y','N'))
/
ALTER TABLE par
ADD CONSTRAINT ck_par_issubmit CHECK (issubmit IN ('Y','N'))
/
ALTER TABLE par
ADD CONSTRAINT ck_par_isvalid CHECK (isvalid IN ('Y','N'))
/

-- Foreign Key

ALTER TABLE par
ADD CONSTRAINT fk_par_rpt_type 
FOREIGN KEY (rpt_type_code)
REFERENCES rpt_type_lookup (rpt_type_code)
/

ALTER TABLE par
ADD CONSTRAINT fk_par_prd_type FOREIGN KEY (prd_type_code)
REFERENCES prd_type_lookup (prd_type_code)
/

-- Start of DDL Script for Sequence PARSEQ
-- Drop the old instance of PARSEQ

DROP SEQUENCE parseq
/

CREATE SEQUENCE parseq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 99999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

-- End of DDL Script for Sequence PARSEQ

-- Triggers for PAR

CREATE OR REPLACE TRIGGER PAR_SEQ_TRIGG     
before insert on "PAR"    for each row
begin     
  if inserting then       
   if :NEW."PARSEQ" is null then          
          select PARSEQ.nextval into :NEW."PARSEQ" from dual;       
   end if;    
 end if; 
end;
/


-- End of DDL Script for Table PAR
