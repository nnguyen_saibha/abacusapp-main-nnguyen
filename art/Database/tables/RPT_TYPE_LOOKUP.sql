drop table rpt_type_lookup
/
-- Start of DDL Script for Table APPX.rpt_TYPE_LOOKUP

CREATE TABLE rpt_type_lookup
    (rpt_type_code                  NUMBER NOT NULL,
     rpt_type_name                  VARCHAR2(60 BYTE),
     active                         VARCHAR2(1 BYTE) DEFAULT 'Y' NOT NULL,
     upd_id                         VARCHAR2(20 BYTE) default user,
     upd_date                       DATE DEFAULT sysdate
    )
  PCTFREE     10
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  APPXDBT_TBS
  STORAGE   (INITIAL     1M    NEXT    5M)
  NOCACHE
  MONITORING
  NOPARALLEL
  LOGGING
/

-- Constraints for rpt_TYPE_LOOKUP

ALTER TABLE rpt_type_lookup
ADD CONSTRAINT pk_rpt_type_lookup PRIMARY KEY (rpt_type_code)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  APPXDBT_INDX_TBS
  STORAGE   (INITIAL     1M    NEXT     1M)
/


-- End of DDL Script for Table rpt_TYPE_LOOKUP
