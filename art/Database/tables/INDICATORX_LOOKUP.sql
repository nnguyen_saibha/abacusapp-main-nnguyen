-- Start of DDL Script for Table INDICATORX_LOOKUP
drop table indicatorx_lookup
/
CREATE TABLE indicatorx_lookup
    (indseq                         NUMBER ,                --pk
     indicator_code                 VARCHAR2(5) NOT NULL,
     indicator_name                 VARCHAR2(600) NOT NULL,
     sector_code                    VARCHAR2(5),
     subsector_code                 VARCHAR2(5),
     uom1                           VARCHAR2(15) NOT NULL,
     uom2                           VARCHAR2(15),
     active                         CHAR(1) DEFAULT 'Y',
     upd_id                         VARCHAR2(20) DEFAULT user,
     upd_date                       DATE DEFAULT sysdate)
  PCTFREE     10
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  APPXDBT_TBS
  STORAGE   (INITIAL   1M   NEXT     5M)
  NOCACHE
  MONITORING
  NOPARALLEL
  LOGGING
/

-- Constraints for INDICATORX_LOOKUP

ALTER TABLE indicatorx_lookup
ADD CONSTRAINT pk_indicatorx_lookup PRIMARY KEY (indseq)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  APPXDBT_INDX_TBS
  STORAGE   (INITIAL  1M   NEXT    1M)
/

ALTER TABLE indicatorx_lookup
ADD CONSTRAINT uk_indicatorx_lookup UNIQUE (indicator_code, subsector_code, sector_code)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  APPXDBT_INDX_TBS
  STORAGE   (INITIAL     1M    NEXT    1M)
/

-- Start of DDL Script for Sequence INDICATORXCODESEQ
-- Drop the old instance of INDICATORXCODESEQ
DROP SEQUENCE indicatorxcodeseq
/

CREATE SEQUENCE indicatorxcodeseq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 99999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/
-- End of DDL Script for Sequence INDICATORXCODESEQ

--Construct Indicator Code
CREATE OR REPLACE TRIGGER INDICATORX_LOOKUPCODE_TRIGG
BEFORE  INSERT ON INDICATORX_LOOKUP
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
begin
    if inserting then
       if :NEW."INDICATOR_CODE" is null then
          select 'IX'  ||  INDICATORXCODESEQ.nextval into :NEW."INDICATOR_CODE" from dual;
       end if;
    end if;
end;
/

-- Start of DDL Script for Sequence INDSEQ
-- Drop the old instance of INDSEQ
DROP SEQUENCE indseq
/

CREATE SEQUENCE indseq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 99999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

-- Triggers for INDICATORX_LOOKUP 
-- generate indseq
CREATE OR REPLACE TRIGGER INDICATORX_LOOKUPSEQ_TRIGG     
before insert on "INDICATORX_LOOKUP"    for each row
begin
     if inserting then
       if :NEW."INDSEQ" is null then
          select INDSEQ.nextval into :NEW."INDSEQ" from dual;
       end if;
    end if;
end;
/
-- End of DDL Script for Sequence INDSEQ

-- End of DDL Script for Table INDICATORX_LOOKUP
