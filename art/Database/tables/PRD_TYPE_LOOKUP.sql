-- Start of DDL Script for Table PRD_TYPE_LOOKUP
-- Generated 10/26/2011 2:40:29 PM from APPX@OFDAXU

-- Drop the old instance of PRD_TYPE_LOOKUP
DROP TABLE prd_type_lookup
/

CREATE TABLE prd_type_lookup
    (prd_type_code                  NUMBER NOT NULL,
    prd_type_name                  VARCHAR2(60 BYTE),
    PRD_START_DATE		     DATE,
    PRD_END_DATE			     DATE,
    PRD_DUE_DATE			     DATE,
    active                         VARCHAR2(1 BYTE) DEFAULT 'Y' NOT NULL,
    upd_id                         VARCHAR2(20 BYTE) DEFAULT user,
    upd_date                       DATE DEFAULT sysdate)
  PCTFREE     10
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  APPXDBT_TBS
  STORAGE   (
    INITIAL     1048576
    NEXT        5242880
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
  NOCACHE
  MONITORING
  NOPARALLEL
  LOGGING
/





-- Constraints for PRD_TYPE_LOOKUP

ALTER TABLE prd_type_lookup
ADD CONSTRAINT pk_prd_type_lookup PRIMARY KEY (prd_type_code)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  APPXDBT_INDX_TBS
  STORAGE   (
    INITIAL     1048576
    NEXT        1048576
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/


-- End of DDL Script for Table PRD_TYPE_LOOKUP
