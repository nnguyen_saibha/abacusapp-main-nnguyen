drop table parsectorindicator
/
-- Start of DDL Script for Table PARSECTORINDICATOR

CREATE TABLE parsectorindicator
    (parsiseq                      NUMBER ,
    parsectorseq                   NUMBER,
    sector_code                    VARCHAR2(5) NOT NULL,
    sector                         VARCHAR2(60),
    subsector_code                 VARCHAR2(5) NOT NULL,
    subsector                      VARCHAR2(60),
    indicator_code                 VARCHAR2(5) NOT NULL,
    indicator_name                 VARCHAR2(600),
    isnew                          CHAR(1) DEFAULT 'N' NOT NULL,
    isofda                         CHAR(1) DEFAULT 'N' NOT NULL,
    uom1                           VARCHAR2(15),
    value1_targeted                NUMBER(15),
    value1_reached                 NUMBER(15),
    cum_value1_targeted            NUMBER,
    cum_value1_reached             NUMBER,
    uom2                           VARCHAR2(15),
    value2_targeted                NUMBER(15),
    value2_reached                 NUMBER(15),
    cum_value2_targeted            NUMBER,
    cum_value2_reached             NUMBER,
    comments                       VARCHAR2(2000),
    upd_id                         VARCHAR2(20) default user,
    upd_date                       DATE DEFAULT sysdate)
  PCTFREE     10
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  APPXDBT_TBS
  STORAGE   (INITIAL   1M   NEXT   5M)
  NOCACHE
  MONITORING
  NOPARALLEL
  LOGGING
/

-- Constraints for PARSECTORINDICATOR

ALTER TABLE parsectorindicator
ADD CONSTRAINT pk_parsectorindicator PRIMARY KEY (parsiseq)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  APPXDBT_INDX_TBS
  STORAGE   (INITIAL    1M    NEXT    1M)
/

--check constraints

ALTER TABLE parsectorindicator
ADD CONSTRAINT ck_parsi_isnew CHECK (isnew IN ('Y','N'))
/
ALTER TABLE parsectorindicator
ADD CONSTRAINT ck_parsi_isofda CHECK (isofda IN ('Y','N'))
/

-- Foreign Key

ALTER TABLE parsectorindicator
ADD CONSTRAINT  fk_parsector_parsecind FOREIGN KEY (parsectorseq)
REFERENCES parsector (parsectorseq)
/

-- Start of DDL Script for Sequence PARSECTORINDICATORSEQ

-- Drop the old instance of PARSISEQ
DROP SEQUENCE parsiseq
/

CREATE SEQUENCE parsiseq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 99999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

-- End of DDL Script for Sequence PARSISEQ

-- Triggers for PARSECTORINDICATOR

CREATE OR REPLACE TRIGGER PARSECTORINDICATOR_TRIGG     
before insert on "PARSECTORINDICATOR"    for each row
begin     
    if inserting then       
      if :NEW."PARSISEQ" is null then          
         select PARSISEQ.nextval into :NEW."PARSISEQ" from dual;
      end if;
    end if;
end;
/


-- End of DDL Script for Table PARSECTORINDICATOR

