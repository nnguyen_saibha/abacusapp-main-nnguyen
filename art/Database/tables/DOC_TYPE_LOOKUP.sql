-- Start of DDL Script for Table APP.DOC_TYPE_LOOKUP
-- Generated 10/26/2011 2:45:40 PM from APP@OFDADNEW

-- Drop the old instance of DOC_TYPE_LOOKUP
DROP TABLE doc_type_lookup
/

CREATE TABLE doc_type_lookup
    (doc_type_code                  VARCHAR2(3) NOT NULL,
     doc_type_name                  VARCHAR2(60),
     active                         VARCHAR2(1 BYTE) DEFAULT 'Y' NOT NULL,
     upd_id                         VARCHAR2(20 BYTE) default user,
     upd_date                       DATE DEFAULT sysdate
   )
    
  PCTFREE     10
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  APPXDBT_TBS
  STORAGE   (INITIAL     1M    NEXT    5M)
  NOCACHE
  MONITORING
  NOPARALLEL
  LOGGING
/



-- Constraints for DOC_TYPE_LOOKUP

ALTER TABLE doc_type_lookup
ADD CONSTRAINT pk_doc_type PRIMARY KEY (doc_type_code)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  APPXDBT_INDX_TBS
  STORAGE   (
    INITIAL     516096
    NEXT        1048576
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/


-- End of DDL Script for Table DOC_TYPE_LOOKUP
