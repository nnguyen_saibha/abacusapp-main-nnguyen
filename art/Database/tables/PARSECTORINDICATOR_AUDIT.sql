-- Start of DDL Script for Table PARSECTORINDICATOR_AUDIT
DROP TABLE PARSECTORINDICATOR_AUDIT
/
CREATE TABLE PARSECTORINDICATOR_AUDIT
 (  AUDIT_SEQ_NBR                  NUMBER ,
    AUDIT_TYPE                     VARCHAR2(5),
    AUDIT_COMMENTS                 VARCHAR2(2000), 
    parsiseq                       NUMBER ,
    parsectorseq                   NUMBER,
    sector_code                    VARCHAR2(5) ,
    sector                         VARCHAR2(60),
    subsector_code                 VARCHAR2(5) ,
    subsector                      VARCHAR2(60),
    indicator_code                 VARCHAR2(5),
    indicator_name                 VARCHAR2(600),
    isnew                          CHAR(1) ,
    isofda                         CHAR(1) ,
    uom1                           VARCHAR2(15),
    value1_targeted                NUMBER(15),
    value1_reached                 NUMBER(15),
    cum_value1_targeted            NUMBER,
    cum_value1_reached             NUMBER,
    uom2                           VARCHAR2(15),
    value2_targeted                NUMBER(15),
    value2_reached                 NUMBER(15),
    cum_value2_targeted            NUMBER,
    cum_value2_reached             NUMBER,
    comments                       VARCHAR2(2000),
    upd_id                         VARCHAR2(20),
    upd_date                       DATE
 )
  PCTFREE     10
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  APPXDBT_TBS
  STORAGE   (INITIAL   1M   NEXT   5M)
  NOCACHE
  MONITORING
  NOPARALLEL
  LOGGING
/

DROP SEQUENCE PSI_AUDIT_SEQ
/
CREATE SEQUENCE PSI_AUDIT_SEQ
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 99999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/
CREATE OR REPLACE TRIGGER PARSECTORINDICATOR_AUDIT_TRIG
 AFTER
  DELETE OR UPDATE
 ON PARSECTORINDICATOR
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW

Declare
TransType varchar2(10);
comment varchar2(2050);
Begin
IF UPDATING THEN
TransType:=' M ';
comment:= ' MODIFIED BY '||:NEW.UPD_ID ||' on '|| sysdate;
ELSIF DELETING THEN
TransType:=' D ';
comment:= ' DELETED BY '||user ||' on '|| sysdate;
END IF;
INSERT INTO PARSECTORINDICATOR_AUDIT
(AUDIT_SEQ_NBR,AUDIT_TYPE,AUDIT_COMMENTS,
PARSISEQ,  PARSECTORSEQ,  SECTOR_CODE,  SECTOR,
        SUBSECTOR_CODE,  SUBSECTOR,  INDICATOR_CODE,
        INDICATOR_NAME,  ISNEW,  ISOFDA,  UOM1,  VALUE1_TARGETED,
        VALUE1_REACHED, cum_value1_targeted, cum_value1_reached,
        UOM2,  VALUE2_TARGETED,  VALUE2_REACHED,cum_value2_targeted, cum_value2_reached,
        COMMENTS,  UPD_ID,  UPD_DATE )  
values
(PSI_AUDIT_SEQ.NEXTVAL,TransType,comment,
:OLD.PARSISEQ, 
:OLD.PARSECTORSEQ, 
:OLD.SECTOR_CODE, 
:OLD.SECTOR,
:OLD.SUBSECTOR_CODE, 
:OLD.SUBSECTOR, 
:OLD.INDICATOR_CODE,
:OLD.INDICATOR_NAME,
:OLD.ISNEW,
:OLD.ISOFDA, 
:OLD.UOM1,
:OLD.VALUE1_TARGETED,
:OLD.VALUE1_REACHED,
:OLD.cum_value1_targeted, 
:OLD.cum_value1_reached, 
:OLD.UOM2, 
:OLD.VALUE2_TARGETED, 
:OLD.VALUE2_REACHED,
:OLD.cum_value1_targeted, 
:OLD.cum_value1_reached,
:OLD.COMMENTS, 
:OLD.UPD_ID, 
:OLD.UPD_DATE ) ;
END; 
/

