drop table par_docs
/
-- Start of DDL Script for Table PAR_DOCS

CREATE TABLE par_docs
    (parseq                        NUMBER(10) NOT NULL,
    docseq                         NUMBER NOT NULL,
    comments                       VARCHAR2(1000),
    upd_id                         VARCHAR2(20) DEFAULT user,
    upd_date                       DATE DEFAULT sysdate)
  PCTFREE     20
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  APPXDBT_TBS
  STORAGE   (INITIAL   2M   NEXT   2M)
  NOCACHE
  MONITORING
  NOPARALLEL
  LOGGING
/

-- Constraints for PAR_DOCS

ALTER TABLE par_docs
ADD CONSTRAINT pk_par_docs PRIMARY KEY (parseq, docseq)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  APPXDBT_INDX_TBS
  STORAGE   (INITIAL    1M    NEXT    1M)
/

-- End of DDL Script for Table PAR_DOCS

-- Foreign Key

ALTER TABLE par_docs
ADD CONSTRAINT fk_par_docs_parseq FOREIGN KEY (parseq)
REFERENCES par (parseq)
/
ALTER TABLE par_docs
ADD CONSTRAINT fk_par_docs_docseq FOREIGN KEY (docseq)
REFERENCES Rpt_docs (docseq)
/

-- End of DDL script for Foreign Key(s)
