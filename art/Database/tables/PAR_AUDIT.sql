DROP TABLE PAR_AUDIT
/
CREATE TABLE PAR_AUDIT
(   AUDIT_SEQ_NBR              NUMBER ,
    AUDIT_TYPE                 VARCHAR2(5),
    AUDIT_COMMENTS             VARCHAR2(2000),
    parseq                     NUMBER ,   
    awid                       NUMBER,
    rpt_type_code              NUMBER(5),
    prd_type_code              NUMBER(1),    
    rpt_due_date               DATE,
    prd_start_date             DATE,
    prd_end_date               DATE,
    rpt_nbr                    VARCHAR2(20),
    rpt_desc                   VARCHAR2(3000),
    submit_by                  VARCHAR2(20),
    submit_date                DATE, 
    total_targeted             NUMBER(10,0),
    idp_targeted               NUMBER(10,0),
    total_reached              NUMBER(10,0),
    idp_reached                NUMBER(10,0),
    cum_total_targeted         NUMBER,
    cum_idp_targeted           NUMBER,
    cum_total_reached          NUMBER,
    cum_idp_reached            NUMBER,
    prev_rpt_amt               NUMBER,
    prd_amt                    NUMBER,
    fed_prev_rpt_amt           NUMBER,
    fed_prd_amt                NUMBER,
    iscomplete                 CHAR(1) ,
    issubmit                   CHAR(1) ,
    isvalid                    CHAR(1) ,
    upd_id                     VARCHAR2(20) ,
    upd_date                   DATE
)
  PCTFREE     10
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  APPXDBT_TBS
  STORAGE   (INITIAL     2M     NEXT        5M)
  NOCACHE
  MONITORING
  NOPARALLEL
  LOGGING
/

DROP SEQUENCE PAR_AUDIT_SEQ
/
CREATE SEQUENCE PAR_AUDIT_SEQ
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 99999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/
CREATE OR REPLACE TRIGGER PAR_AUDIT_TRIG
 AFTER
  DELETE OR UPDATE
 ON PAR
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Declare
TransType varchar2(10);
comment varchar2(2050);
Begin
IF UPDATING THEN
TransType:=' M ';
comment:= ' MODIFIED BY '||:new.upd_id ||' on '|| sysdate;
ELSIF DELETING THEN
TransType:=' D ';
comment:= ' DELETED BY '||user ||' on '|| sysdate;
END IF;
INSERT INTO PAR_AUDIT
(AUDIT_SEQ_NBR,AUDIT_TYPE,AUDIT_COMMENTS,
PARSEQ, AWID, RPT_TYPE_CODE,prd_type_code, RPT_DUE_DATE,
       PRD_START_DATE, PRD_END_DATE, RPT_NBR, RPT_DESC,
       SUBMIT_BY, SUBMIT_DATE, TOTAL_TARGETED, IDP_TARGETED,
       TOTAL_REACHED, IDP_REACHED,cum_total_targeted,  cum_idp_targeted,  cum_total_reached,
        cum_idp_reached,  prev_rpt_amt,  prd_amt,  fed_prev_rpt_amt,
        fed_prd_amt, ISCOMPLETE, ISSUBMIT,
       ISVALID, UPD_ID, UPD_DATE)
VALUES
(PAR_AUDIT_SEQ.NEXTVAL,TransType,comment,
:OLD.PARSEQ, 
:OLD.AWID, 
:OLD.RPT_TYPE_CODE,
:OLD.PRD_TYPE_CODE,
:OLD.RPT_DUE_DATE, 
:OLD.PRD_START_DATE, 
:OLD.PRD_END_DATE, 
:OLD.RPT_NBR, 
:OLD.RPT_DESC, 
:OLD.SUBMIT_BY, 
:OLD.SUBMIT_DATE, 
:OLD.TOTAL_TARGETED, 
:OLD.IDP_TARGETED, 
:OLD.TOTAL_REACHED, 
:OLD.IDP_REACHED,
:OLD.cum_total_targeted,
:OLD.cum_idp_targeted,
:OLD.cum_total_reached,
:OLD.cum_idp_reached,
:OLD.prev_rpt_amt,
:OLD.prd_amt,
:OLD.fed_prev_rpt_amt,
:OLD.fed_prd_amt,
:OLD.ISCOMPLETE, 
:OLD.ISSUBMIT, 
:OLD.ISVALID, 
:OLD.UPD_ID, 
:OLD.UPD_DATE) ;
END;
/
