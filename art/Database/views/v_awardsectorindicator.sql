DROP VIEW v_awardsectorindicator
/

CREATE OR REPLACE VIEW v_awardsectorindicator (
   awid,
   sector_code,
   sector,
   subsector_code,
   subsector,
   indicator_code,
   indicator_name,
   isnew,
   isofda,
   uom1,
   uom2,
   Value1_Targeted,
   Value2_Targeted,
   Cum_Value1_Targeted,
   Cum_Value2_Targeted )
AS
select nvl(vsi.awid,vsix.awid) awid,
       nvl(vsi.sector_code,vsix.sector_code) sector_code,
       nvl(vsi.sector,vsix.sector) sector,
       nvl(vsi.subsector_code,vsix.subsector_code) subsector_code,
       nvl(vsi.subsector,vsix.subsector) subsector,
       nvl(vsi.indicator_code,vsix.indicator_code) indicator_code,
       nvl(vsi.indicator_name,vsix.indicator_name) indicator_name,
       nvl(vsix.isnew,'N') isnew,
       nvl(vsix.isofda,'Y')isofda,
       nvl(vsi.uom1,vsix.uom1) uom1,
       nvl(vsi.uom2,vsix.uom2) uom2,
       vsix.value1_targeted Value1_Targeted,
       vsix.value2_targeted Value2_Targeted,
       vsix.cum_value1_targeted Cum_Value1_Targeted,
       vsix.cum_value2_targeted Cum_Value2_Targeted
from (
(select latest_report.awid awid,pas.sector_code,pas.sector,parsi.subsector_code,parsi.subsector,parsi.indicator_code,parsi.indicator_name,parsi.uom1,parsi.uom2,isnew,isofda,value1_targeted,value2_targeted,cum_value1_targeted,cum_value2_targeted from 
((select max(rpt_nbr) rpt_nbr,awid from par p where p.issubmit='Y' and p.rpt_type_code='1' group by awid) latest_report
join
(select parseq,awid,rpt_nbr from par) par
on (latest_report.awid=par.awid and latest_report.rpt_nbr=par.rpt_nbr))
join
parsector pas
on (par.parseq=pas.parseq)
join
parsectorindicator parsi
on(pas.parsectorseq=parsi.parsectorseq))vsix
full outer join
(select distinct
     awid,sector,sector_code,subsector,subsector_code,indicator_name, indicator_code,uom1,uom2
 from mv_sectorindicator) vsi
on (vsi.awid=vsix.awid and vsi.sector_code=vsix.sector_code and vsi.subsector_code=vsix.subsector_code and vsi.indicator_code=vsix.indicator_code))
/
