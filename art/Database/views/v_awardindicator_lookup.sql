DROP VIEW v_awardindicator_lookup
/

CREATE OR REPLACE VIEW v_awardindicator_lookup (
   sector_code,
   sector,
   subsector_code,
   subsector,
   indicator_code,
   indicator_name,
   uom1,
   uom2,
   active,
   isofda )
AS
SELECT  sector_code, 
        sector, 
        subsector_code, 
        subsector,
        indicator_code, 
        indicator_name, 
        uom1, 
        uom2, 
        active,
        'Y' isofda
FROM mv_indicator_lookup
union
SELECT ixl.sector_code,
           sector_name, 
           ixl.subsector_code, 
           subsector_name,
           indicator_code, 
           indicator_name, 
           uom1, 
           uom2, 
           ixl.active,
           'N' isofda
FROM indicatorx_lookup ixl,mv_sector_lookup sl,mv_subsector_lookup ssl
where ixl.sector_code=sl.sector_code 
and ixl.subsector_code=ssl.subsector_code
/