DROP VIEW v_awardsector
/

CREATE OR REPLACE VIEW v_awardsector(
awid,sector_code,sector,bene_tot_target,bene_idp_target,cum_bene_tot_target,cum_bene_idp_target)
AS
select mv_sector.awid awid,mv_sector.sector_code sector_code,mv_sector.sector sector,bene_tot_target,bene_idp_target,cum_bene_tot_target,cum_bene_idp_target from ((select max(rpt_nbr) rpt_nbr,awid from par p where p.issubmit='Y' and p.rpt_type_code='1' group by awid) latest_report
join
(select parseq,awid,rpt_nbr from par) par
on (latest_report.awid=par.awid and latest_report.rpt_nbr=par.rpt_nbr))
join
parsector pas
on (par.parseq=pas.parseq)
right outer join
(select distinct   awid,
                   sector_code,
                   sector
 from mv_sector
) mv_sector
on (mv_sector.awid=par.awid and mv_sector.sector_code=pas.sector_code)
/
