DROP VIEW v_award
/
CREATE OR REPLACE VIEW v_award(
   awid,
   awardee_code,
   awardee,
   pnbr,
   pname,
   ccode,
   cname,
   fatcode,
   fatname,
   award_nbr,
   award_start_date,
   award_end_date,
   award_amt,
   award_status )
AS
select awid,awardee_code,awardee,pnbr,pname,
ccode,cname,fatcode,fatname,
award_nbr,
award_start_date,award_end_date,
award_amt,award_status
from mv_action action
where awid=aid
and (budget_fy >=2010 or (awid in (select distinct awid from mv_action b where 1=1 and budget_fy >=2010) ))
/