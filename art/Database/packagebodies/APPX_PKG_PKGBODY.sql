CREATE OR REPLACE 
PACKAGE BODY appx_pkg
IS
FUNCTION proposal_order(action_type varchar2) RETURN number IS
type_new  varchar2(10):='New';
type_newd varchar2(10):='New/D';
type_mod  varchar2(10):='Mod';
type_modd varchar2(10):='Mod/D';
type_unfunded varchar2(20):='Unfunded Mod';
atypeorder number:=0;
BEGIN
select DECODE(action_type,type_new,'1',type_newd,'2',type_mod,'3',type_modd,'4',type_unfunded,'5') into atypeorder from dual;
return atypeorder;
EXCEPTION
  WHEN OTHERS THEN
   RETURN 0;
END;

FUNCTION par_status(parseq number) RETURN VARCHAR2 is
begin
   return 'N';
END;


Function new_report(p_awid varchar2,nbr_or_date varchar2) return varchar2 is
nbr varchar2(10):='NBR';
dt varchar2(10):='DATE';
l_reportnbr number:=0;
l_enddate date:=sysdate;

begin
    select nvl(max(to_number(rpt_nbr))+1,1),nvl(max(PRD_END_DATE)+1,sysdate)
    into l_reportnbr,l_enddate
    from par where awid=p_awid;
    case nbr_or_date
       when nbr then return l_reportnbr;
       when dt  then return l_enddate;
       else return null;
    end case;
       return null;
end;
FUNCTION is_report_complete(p_par INTEGER) return clob is
  par_row par%ROWTYPE;
  pars_row parsector%ROWTYPE;
  parsi_row parsectorindicator%ROWTYPE;
  rtn clob:='';
  temp1 clob:='';
  temp2 clob:='';
  temp3 clob:='';
  sectorheader number:=0;
  parsc pars_type;
  parsic parsi_type;
begin
  begin
    select * into par_row from par where parseq=p_par;
    case par_row.rpt_type_code
        when 1 then --Program
           rtn:='';
          if(par_row.total_targeted is null)then        rtn:=rtn||'Total Targeted,'; end if;
          if(par_row.total_reached is null)then         rtn:=rtn||' Total Reached,'; end if;
          if(par_row.cum_total_targeted is null)then    rtn:=rtn||' Cumulative Total Targeted,';end if;
          if(par_row.cum_total_reached is null)then     rtn:=rtn||' Cumulative Total Reached'; end if;
          if(length(rtn)>0) then
            rtn:='For the current report '||rtn||' must be specified';
          end if;
          open parsc FOR
          select * from parsector where parseq=p_par order by sector;
          LOOP
           fetch parsc into pars_row;
           exit when parsc%NOTFOUND;
             temp1:='';
             sectorheader:=0;
             if(pars_row.bene_tot_target is null)then        temp1:=temp1||'Total Targeted,'; end if;
             if(pars_row.bene_tot_reached is null)then       temp1:=temp1||' Total Reached,'; end if;
             if(pars_row.cum_bene_tot_target is null)then    temp1:=temp1||' Cumulative Total Targeted,';end if;
             if(pars_row.cum_bene_tot_reached is null)then   temp1:=temp1||' Cumulative Total Reached'; end if;
             if(length(temp1)>0) then
                sectorheader:=1;
                rtn:=rtn||'<tr><th colspan=2>'||pars_row.sector ||'</th><th colspan=2>'||trim(',' from trim(temp1))||'</th></tr>';
             end if;
             open parsic FOR
             select * from parsectorindicator where parsectorseq=pars_row.parsectorseq order by subsector,indicator_name;
             LOOP
              fetch parsic into parsi_row;
              exit when parsic%NOTFOUND;
              temp2:='';
              if(parsi_row.uom1 is not null)then
               if(parsi_row.value1_targeted is null)then        temp2:=temp2||'Total Targeted<br>'; end if;
               if(parsi_row.value1_reached is  null) then        temp2:=temp2||'Total Reached<br>'; end if;
               if(parsi_row.cum_value1_targeted is null)then    temp2:=temp2||'Cumulative Total Targeted<br>';end if;
               if(parsi_row.cum_value1_reached is null) then    temp2:=temp2||'Cumulative Total Reached<br>'; end if;
              end if;
              temp3:='';
              if(parsi_row.uom2 is not null)then
               if(parsi_row.value2_targeted is null)then        temp3:=temp3||'Total Targeted<br>'; end if;
               if(parsi_row.value2_reached is null) then        temp3:=temp3||'Total Reached<br>'; end if;
               if(parsi_row.cum_value2_targeted is null)then    temp3:=temp3||'Cumulative Total Targeted<br>';end if;
               if(parsi_row.cum_value2_reached is null) then    temp3:=temp3||'Cumulative Total Reached<br>'; end if;
              end if;
              if(length(temp2)>0 or length(temp3)>0)then
               if(sectorheader=0)then
                     rtn:=rtn||'<tr><th colspan=2>'||pars_row.sector ||'</th><th colspan=2> </th></tr>';
                     sectorheader:=1;
               end if;
               rtn:=rtn||'<tr><td>'||parsi_row.subsector||'</td><td>'||parsi_row.indicator_name||'</td><td>'||temp2||'</td><td>'||temp3||'</td></tr>';

             end if;
             end loop;
             close parsic;
          end loop;
          close parsc;



         if(length(rtn)>0)then
            rtn:='<table border=1><tr><th colspan=2>Sector</th><th colspan=2>Required Values</th></tr><tr><th>Subsector</th><th>Indicator</th><th style="width:18%">Value 1</th><th style="width:18%">Value 2</th></tr>'||rtn||'</table>';
            return '<b>Total targeted and Total Reached are required at each level (Report,Sector and Indicator)</b><br>'||rtn;
         end if;

        when 3 then --Financial
          rtn:='';
          if(par_row.prev_rpt_amt is null)then      rtn:=rtn||'Previous Reported Amount,'; end if;
          if(par_row.prd_amt is null)then           rtn:=rtn||' This Period Amount,'; end if;
          if(par_row.fed_prev_rpt_amt is null)then  rtn:=rtn||' Federal Previous Reported Amount,'; end if;
          if(par_row.fed_prd_amt is null)then       rtn:=rtn||' Federal Period Amount'; end if;

          if(length(rtn)>0)then return rtn||' are not specified';
          else return null;
          end if;
       else return null;
    end case;

    exception
    when others then
     return null;
  end;
return null;
end;
end;
/
