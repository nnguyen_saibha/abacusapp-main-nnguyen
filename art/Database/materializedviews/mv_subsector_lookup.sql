DROP SNAPSHOT mv_subsector_lookup
/

CREATE MATERIALIZED VIEW mv_subsector_lookup
  PCTFREE     5
  MAXTRANS    255
  STORAGE   (
     INITIAL     57344
    NEXT        57344 )
  NOCACHE
NOLOGGING
PARALLEL (DEGREE 5)
BUILD IMMEDIATE 
--REFRESH NEXT SYSDATE + 1
REFRESH START WITH TRUNC(SYSDATE) + 1 + (4.5/24) -- refresh at 4:30am
        NEXT SYSDATE + 1
AS
select sector_subsector_link.sector_code,
       subsector_lookup.subsector_code,
       subsector_lookup.subsector_name,
       subsector_lookup.active
from subsector_lookup@OFDA,sector_subsector_link@OFDA
where subsector_lookup.subsector_code=sector_subsector_link.subsector_code
and subsector_lookup.active='Y'
/