DROP SNAPSHOT mv_sectorindicator
/

CREATE MATERIALIZED VIEW mv_sectorindicator
  PCTFREE     5
  MAXTRANS    255
  STORAGE   (
     INITIAL     57344
    NEXT        57344 )
  NOCACHE
NOLOGGING
PARALLEL (DEGREE 5)
BUILD IMMEDIATE 
--REFRESH NEXT SYSDATE + 1
REFRESH START WITH TRUNC(SYSDATE) + 1 + (3.5/24)   -- 3:30 am
        NEXT  SYSDATE + 1
AS
select
 nvl(PA.mod_action_id, PA.action_id) AWID
,PA.Action_id AID
,PAI.sector_code sector_code
,app_pkg.get_name@OFDA('sector_code',pai.sector_code) Sector
,PAI.subsector_code subsector_code
,app_pkg.get_name@OFDA('subsector_code',pai.subsector_code) subsector
,PAI.indicator_code  indicator_code
,app_pkg.get_name@OFDA('indicator_code',pai.indicator_code) indicator_name
,PAI.indicator_value value1
,IL.uom_code UOM1
,PAI.indicator_value2 value2
,IL.uom_code2 UOM2
from pas_indicators@OFDA PAI,indicator_lookup@OFDA IL,procurement_action@OFDA PA
where
funding_action_type in (01,03)
and Commit_flag = 'Y'
and action_type_code <> 'L'
and Is_checkbook <> 'N'
and PA.ofda_control_nbr is not null
and (obligated_amt is not null
  or pa.obligated_date is not null or pa.pkg_received_from_op is not null)
and pa_pkg.get_award_status@OFDA(pa.action_id) in ('Active','Grace','Expired','Closed')
and PAI.action_id = PA.action_id
and IL.indicator_code=PAI.indicator_code
/