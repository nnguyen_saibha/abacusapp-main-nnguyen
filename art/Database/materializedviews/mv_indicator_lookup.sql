DROP SNAPSHOT mv_indicator_lookup
/

CREATE MATERIALIZED VIEW mv_indicator_lookup
  PCTFREE     5
  MAXTRANS    255
  STORAGE   (
      INITIAL   57344
    NEXT        57344  )
  NOCACHE
NOLOGGING
PARALLEL (DEGREE 5)
BUILD IMMEDIATE 
--REFRESH NEXT SYSDATE + 1
REFRESH START WITH TRUNC(SYSDATE) + 1 + (4.84/24)  -- refresh at 4:50 am
        NEXT SYSDATE + 1
AS
SELECT sslink.sector_code sector_code,
       app_pkg.get_name@OFDA('sector_code',sector_code) sector,
       indicator_lookup.subsector_code subsector_code,
       app_pkg.get_name@OFDA('subsector_code',indicator_lookup.subsector_code) subsector,
       indicator_lookup.indicator_code indicator_code,
       indicator_lookup.indicator_name indicator_name,
       indicator_lookup.uom_code uom1,
       indicator_lookup.uom_code2 uom2,
       indicator_lookup.active active
  FROM indicator_lookup@OFDA ,sector_subsector_link@OFDA sslink
  where indicator_lookup.active='Y' and indicator_lookup.subsector_code=sslink.subsector_code
/
