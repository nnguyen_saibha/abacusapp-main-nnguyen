DROP SNAPSHOT mv_sector_lookup
/

CREATE MATERIALIZED VIEW mv_sector_lookup
  PCTFREE     5
  MAXTRANS    255
  STORAGE   (
       INITIAL     57344
    NEXT        57344 )
  NOCACHE
NOLOGGING
PARALLEL (DEGREE 5)
BUILD IMMEDIATE 
--REFRESH NEXT SYSDATE + 1
REFRESH START  WITH TRUNC(SYSDATE) + 1 + (4.18/24) -- refresh at 4:10am        
        NEXT SYSDATE + 1
AS
SELECT distinct sector_lookup.sector_code,
                sector_lookup.sector_name,
                sector_lookup.active
from sector_lookup@OFDA,sector_fat_link@OFDA
where sector_lookup.active      ='Y'
and sector_lookup.sector_code   =sector_fat_link.sector_code
and sector_fat_link.funding_action_type in ('01','03')
/