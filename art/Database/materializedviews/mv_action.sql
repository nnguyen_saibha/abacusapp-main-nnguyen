DROP SNAPSHOT mv_action
/
CREATE MATERIALIZED VIEW mv_action
  PCTFREE     5
  MAXTRANS    255
  STORAGE   (
    INITIAL     57344
    NEXT        57344
     )
  NOCACHE
NOLOGGING
PARALLEL (DEGREE 5)
BUILD IMMEDIATE 
--REFRESH NEXT SYSDATE + 1
REFRESH START WITH TRUNC(SYSDATE) + 1 + (2/24)  -- 2:00 am
        NEXT  SYSDATE + 1
AS
select
 nvl(PA.mod_action_id, PA.action_id) AWID
,PA.Action_id AID
,PA.mod_action_id MID
,PA.budget_fy budget_fy
,PA.project_nbr Pnbr
,app_pkg.get_name@OFDA('project_nbr',project_nbr) Pname
,pa.country_code ccode
,app_pkg.get_name@OFDA('country_code',country_code) cname
,PA.action_seq_nbr As_nbr
,PA.awardee_code  Awardee_code
,app_pkg.get_name@OFDA('awardee_code',awardee_code) Awardee
,PA.funding_action_type FATCode
,app_pkg.get_name@OFDA('funding_action_type',funding_action_type) FATname
,pa.action_type_code atypecode
,app_pkg.get_name@OFDA('action_type_code',action_type_code) atypename
,PA.proposal_name Proposal_name
,PA.award_start_date Aid_start_date
,pa_pkg.new_end_date@OFDA(action_id) Aid_end_date
,PA.obligated_amt Aid_amt
,PA.ofda_control_nbr control_nbr
,PA.award_nbr award_nbr
,PA.mod_id
,pa_pkg.AWARD_START_DATE@OFDA(action_id) award_start_date
,pa_pkg.AWARD_END_DATE@OFDA(action_id) award_end_date
,pa_pkg.get_award_amt@OFDA(action_id) award_amt
,pa_pkg.get_award_status@OFDA(action_id) award_status
,PA.planned_targeted Bene_tot_target
,PA.idp_beneficiaries_planned Bene_Idp_target
from procurement_action@OFDA PA
where
funding_action_type in (01,03)
and Commit_flag = 'Y'
and action_type_code <> 'L'
and Is_checkbook <> 'N'
and PA.ofda_control_nbr is not null
and (obligated_amt is not null
  or pa.obligated_date is not null or pa.pkg_received_from_op is not null)
and pa_pkg.get_award_status@OFDA(action_id) in ('Active','Grace','Expired','Closed') 
/