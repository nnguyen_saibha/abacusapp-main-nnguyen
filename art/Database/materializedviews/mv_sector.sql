DROP SNAPSHOT mv_sector
/
CREATE MATERIALIZED VIEW mv_sector
  PCTFREE     5
  MAXTRANS    255
  STORAGE   (
      INITIAL     57344
    NEXT        57344)
  NOCACHE
NOLOGGING
PARALLEL (DEGREE 5)
BUILD IMMEDIATE 
--REFRESH NEXT SYSDATE + 1
REFRESH START WITH TRUNC(SYSDATE) + 1 + (3/24)  -- 3:00 am
        NEXT  SYSDATE + 1
AS
select
 nvl(PA.mod_action_id, PA.action_id) awid
,PA.Action_id Aid
,PAS.Sector_code Sector_code
,app_pkg.get_name@OFDA('sector_code',pas.sector_code) Sector
,PAS.sector_amt Amount
,PAS.objective Objective
,PAS.Planned_targeted Bene_tot_target
,PAS.IDP_Beneficiaries_planned Bene_Idp_target
,PAS.activity_desc
from Pa_sector@OFDA PAS,procurement_action@OFDA pa
where PAS.action_id = PA.Action_id
and pa.funding_action_type in (01,03)
and pa.Commit_flag = 'Y'
and pa.action_type_code <> 'L'
and pa.Is_checkbook <> 'N'
and PA.ofda_control_nbr is not null
and (pa.obligated_amt is not null
  or pa.obligated_date is not null or pa.pkg_received_from_op is not null)
and pa_pkg.get_award_status@OFDA(pa.action_id) in ('Active','Grace','Expired','Closed')
/