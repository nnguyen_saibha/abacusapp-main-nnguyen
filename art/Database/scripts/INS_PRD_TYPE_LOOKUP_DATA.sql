INSERT INTO PRD_type_lookup (prd_type_code, prd_type_name) VALUES(1,'Q1');
INSERT INTO PRD_type_lookup (prd_type_code, prd_type_name) VALUES(2,'Q2');
INSERT INTO PRD_type_lookup (prd_type_code, prd_type_name) VALUES(3,'Q3');
INSERT INTO PRD_type_lookup (prd_type_code, prd_type_name) VALUES(4,'Q4');
INSERT INTO PRD_type_lookup (prd_type_code, prd_type_name) VALUES(5,'Annual');
INSERT INTO PRD_type_lookup (prd_type_code, prd_type_name) VALUES(6,'Final');
INSERT INTO PRD_type_lookup (prd_type_code, prd_type_name) VALUES(7,'Custom');
