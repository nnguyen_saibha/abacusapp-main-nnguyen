set echo on
set serveroutput on
set time on
set timing on
spool mv.log
@C:\JDeveloper\Development\Database\materializedviews\mv_action.sql;
@C:\JDeveloper\Development\Database\materializedviews\mv_indicator_lookup.sql;
@C:\JDeveloper\Development\Database\materializedviews\mv_sector.sql;
@C:\JDeveloper\Development\Database\materializedviews\mv_sectorindicator.sql;
@C:\JDeveloper\Development\Database\materializedviews\mv_sector_lookup.sql;
@C:\JDeveloper\Development\Database\materializedviews\mv_subsector_lookup.sql;
spool off
