CREATE USER APPXT IDENTIFIED BY p0o9i8u7
DEFAULT TABLESPACE APPXDBT_TBS
TEMPORARY TABLESPACE temp
/
  
grant  CONNECT, CREATE SYNONYM TO APPXT;

--Syngrant scripts  
  select 'GRANT SELECT, INSERT, UPDATE, DELETE ON '||owner||'.'||object_name||
' TO APPXT ;'
from all_objects where owner='APPXDBT' and object_type in ('TABLE','VIEW');
select 'GRANT EXECUTE ON '||owner||'.'||object_name||
' TO APPXT ;' from all_objects where owner='APPXDBT' and object_type in ('PACKAGE');

select 'DROP SYNONYM APPXT.'||object_name||' ;'
from all_objects where owner='APPXDBT' and object_type in ('TABLE','VIEW','PACKAGE');

select 'CREATE SYNONYM APPXT.'||object_name||
' for APPXDBT.'||object_name||' ;'
from all_objects where owner='APPXDBT' and object_type in ('TABLE','VIEW');

select 'CREATE SYNONYM APPXT.'||object_name||
' for APPXDBT.'||object_name||' ;'
from all_objects where owner='APPXDBT' and object_type in ('PACKAGE');