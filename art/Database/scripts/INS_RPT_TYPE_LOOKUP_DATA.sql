INSERT INTO rpt_type_lookup (rpt_type_code, rpt_type_name) VALUES(1,'Program');
INSERT INTO rpt_type_lookup (rpt_type_code, rpt_type_name) VALUES(2,'Other');
INSERT INTO rpt_type_lookup (rpt_type_code, rpt_type_name) VALUES(3,'Financial');
INSERT INTO rpt_type_lookup (rpt_type_code, rpt_type_name) VALUES(4,'Baseline');
INSERT INTO rpt_type_lookup (rpt_type_code, rpt_type_name) VALUES(5,'Property');
