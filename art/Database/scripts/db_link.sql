drop public database link OFDA
/

CREATE PUBLIC DATABASE LINK OFDA CONNECT TO appx_link IDENTIFIED BY &&Abacusappxpassword USING '&&OFDADBSID';
