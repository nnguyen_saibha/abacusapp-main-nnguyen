insert into doc_type_lookup (doc_type_code,doc_type_name) values(1,'Quarter');
insert into doc_type_lookup (doc_type_code,doc_type_name) values(2,'Annual');
insert into doc_type_lookup (doc_type_code,doc_type_name) values(3,'Final');
insert into doc_type_lookup (doc_type_code,doc_type_name) values(4,'Baseline');
insert into doc_type_lookup (doc_type_code,doc_type_name) values(5,'Property');
