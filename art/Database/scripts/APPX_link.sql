-- Start of DDL Script for User appx_link
-- Generated 8-Sep-2011 18:30:22 from APP@OFDAU11g

-- Drop the old instance of appx_link
DROP USER appx_link
  CASCADE
/

CREATE USER appx_link
IDENTIFIED BY Appx$ofdau11g2107
DEFAULT TABLESPACE USERS
TEMPORARY TABLESPACE TEMP
/
GRANT CREATE MATERIALIZED VIEW TO appx_link
/
GRANT UNLIMITED TABLESPACE TO appx_link
/
GRANT CONNECT TO appx_link
/
GRANT RESOURCE TO appx_link
/
ALTER USER appx_link DEFAULT ROLE ALL
/
GRANT EXECUTE ON app.app_pkg TO appx_link
/
GRANT EXECUTE ON app.app_pkg TO appx_link
/
GRANT SELECT ON app.awardee_lookup TO appx_link
/
GRANT SELECT ON app.doc_type_lookup TO appx_link
/
GRANT EXECUTE ON app.get_description TO appx_link
/
GRANT SELECT ON app.indicator_lookup TO appx_link
/
GRANT SELECT ON app.pas_indicators TO appx_link
/
GRANT SELECT ON app.pa_nocost TO appx_link
/
GRANT EXECUTE ON app.pa_pkg TO appx_link
/
GRANT EXECUTE ON app.pa_pkg TO appx_link
/
GRANT SELECT ON app.pa_sector TO appx_link
/
GRANT ON COMMIT REFRESH ON app.procurement_action TO appx_link
/
GRANT SELECT ON app.procurement_action TO appx_link
/
GRANT SELECT ON app.sector_fat_link TO appx_link
/
GRANT SELECT ON app.sector_lookup TO appx_link
/
GRANT SELECT ON app.sector_subsector_link TO appx_link
/
GRANT SELECT ON app.subsector_lookup TO appx_link
/


-- End of DDL Script for User appx_link


