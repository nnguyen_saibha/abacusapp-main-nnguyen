/**
 * This class provides the Coherence Cache mechanism for the Navgation menu.
 */

package gov.ofda.menu.cache;

import com.tangosol.net.CacheFactory;
import com.tangosol.net.NamedCache;

import gov.ofda.Constants;
import gov.ofda.menu.facade.MenuDetailFacade;
import gov.ofda.ui.bean.MenuBean;

import java.io.Serializable;

import java.util.Map;

import oracle.adf.share.logging.ADFLogger;


public class CacheManager implements Serializable {
    @SuppressWarnings("compatibility:5652973022763515507")
    private static final long serialVersionUID = -1102612480319964562L;

    private MenuDetailFacade facade;
    
    private static final String MENU_DETAIL_CACHE = "menuDetailCache";
    
    private static final String CONSTANTS_CACHE = "constantsCache";
    
    private static CacheManager instance = null;
    
    // create logger
    private static ADFLogger logger = ADFLogger.createADFLogger(CacheManager.class);
    
    // 
    protected CacheManager(){
       // now allowing default instantiation. 
    }

    /**
     * @return cacheManager Instance
     */
    public static CacheManager getInstance(){
        if(instance == null){
            instance = new CacheManager();
        }
        return instance;
    }

    /**
     * This method caches the Navigation Menu on coherence cache and return it.
     * @return a tree map which contains the Navigation Menu.
     */
    public Map<String, MenuBean> getMenuFromCache(){
        NamedCache cache = CacheFactory.getCache(Constants.ABACUS_CACHE);
        Map<String, MenuBean> navMenuBeanList;
        if(null != cache && cache.size() > 0 ){
            logger.info("Retrieve Menu Details From Cache");
            navMenuBeanList = (Map<String, MenuBean>)cache.get(MENU_DETAIL_CACHE);
            if(null == navMenuBeanList){
                logger.info("Cache is null... so retrieveing Menu Details  from Database");
                navMenuBeanList = retrieveMenuFromDB();
                cache.put(MENU_DETAIL_CACHE, navMenuBeanList);
            }
        } else{
            logger.info("No Cache available at present, so retriving it from Database");
            navMenuBeanList = retrieveMenuFromDB();
            logger.info("Put Menu Details into Cache");
            cache.put(MENU_DETAIL_CACHE, navMenuBeanList);
        }
        return navMenuBeanList;
     }

    private Map<String, MenuBean> retrieveMenuFromDB() {
        Map<String, MenuBean> navMenuBeanList;
        facade = new MenuDetailFacade();
        navMenuBeanList = facade.retrieveMenuDataFromDatabase();
        return navMenuBeanList;
    }

    /**
     *This method re writes the Menu Details Cache.
     * @param map
     */
    public void reWriteMenuDetailCache(Map<String, MenuBean> map) {
        logger.info("Start : Re-write Menu Details Cache");
        NamedCache cache = CacheFactory.getCache(Constants.ABACUS_CACHE);
        cache.put(MENU_DETAIL_CACHE, map);
        logger.info("End : Re-write Menu Details Cache");
    }

    public Map<String, String> getConstantsFromCache() {
        NamedCache cache = CacheFactory.getCache(Constants.ABACUS_CACHE);
        Map<String, String> constants;
        if(null != cache && cache.size() > 0){
            logger.info("Retrieve Constants From Cache");
            constants = (Map<String, String>)cache.get(CONSTANTS_CACHE);
            if(null == constants || constants.size()== 0 ){
                logger.info("Cache is null... so retrieveing constants from Database");
                constants = retrieveConstantsFromDB();
                cache.put(CONSTANTS_CACHE, constants);
            }
        } else{
            logger.info("No Cache available at present, so retriving it from Database");
            constants = retrieveConstantsFromDB();
            logger.info("Put constants into Cache");
            cache.put(CONSTANTS_CACHE, constants);
        }
        
        return constants;
    }

    private Map<String, String> retrieveConstantsFromDB() {
        facade = new MenuDetailFacade();
        return facade.getConstantsFromDatabase();
    }

    public void reWriteConstantsCache(Map<String, String> map) {
        
        logger.info("Start : Re-write Constants Cache");
        NamedCache cache = CacheFactory.getCache(Constants.ABACUS_CACHE);
        cache.put(CONSTANTS_CACHE, map);
        logger.info("End : Re-write Constants Cache");
        
    }
}
