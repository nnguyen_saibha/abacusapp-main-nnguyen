package gov.ofda.menu.facade;

import gov.ofda.Constants;
import gov.ofda.menu.cache.CacheManager;
import gov.ofda.ui.bean.MenuBean;
import gov.ofda.ui.bean.UIMenuBean;

import java.io.Serializable;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichMenu;
import oracle.adf.view.rich.component.rich.nav.RichCommandMenuItem;
import oracle.adf.view.rich.component.rich.nav.RichGoMenuItem;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.client.Configuration;


public class MenuDetailFacade implements Serializable {
    @SuppressWarnings("compatibility:738790428899301485")
    private static final long serialVersionUID = 1049689466905828690L;

    private static final String MENU_DETAIL_VIEW = "MenuDetail1";

    private static final String ABACUS_CONSTANTS_VIEW = "AbacusConstantsView1";

    private static final String CONFIG_MENU_MODULE = "MenuModuleLocal";

    private static final String AM_DEFINITION = "gov.ofda.uishell.model.service.MenuModule";

    private static final String TARGET_PAGE_BLANK = "_blank";

    private static final String PATH_DELIMETER = "/";

    private static final int WINDOW_WIDTH = 1100;

    private static final int WINDOW_HEIGHT = 768;

    private static final String PUBLIC_URL = "PUBLICURL";

    private static final String PROTOCOL = "PROTOCOL";

    private String publicUrl = "";

    // private static final String[] PARENT_MENU_IDS_ARRAY={"25","26","27","28","29"};

    // get the protocol from database, it defaults to https
    private String protocol = "https";

    // get CacheManager Instance
    private CacheManager cacheManager = CacheManager.getInstance();

    // create logger.info

    private static ADFLogger logger = ADFLogger.createADFLogger(MenuDetailFacade.class);

//        public static void main(String args[]){
//            MenuDetailFacade facade = new MenuDetailFacade();
//            Map<String, MenuBean> menuMap = facade.retrieveMenuDataFromDatabase();
//            LinkedList<UIMenuBean> list = facade.getNavigationMenu();
//            System.out.println(list);
//        }
//        public MenuDetailFacade() {
//            super();
//        }


    public Map<String, MenuBean> retrieveMenuDataFromDatabase() {

        Row row = null;
        ViewObject svo = null;
        MenuBean menuBean = null;

        List<MenuBean> menuList = new LinkedList<MenuBean>();

        ApplicationModule applicationModule =
            Configuration.createRootApplicationModule(AM_DEFINITION, CONFIG_MENU_MODULE);
        try {
            svo = applicationModule.findViewObject(MENU_DETAIL_VIEW);
            RowSetIterator rsi = svo.createRowSetIterator(null);
            //logger.info("rsi : " + rsi.toString());
            while (rsi.hasNext()) {
                row = rsi.next();
                menuBean = populateMenuBean(row);
                //map.put(menuBean.getAppMenuId().toString(), menuBean);
                menuList.add(menuBean);
            }

        } finally {
            Configuration.releaseRootApplicationModule(applicationModule, true);
        }

        return populateTopNavigationalMenu(menuList);
    }


    private MenuBean populateMenuBean(Row row) {
        MenuBean menuBean = new MenuBean();
        menuBean.setAbacusRole((String) row.getAttribute("AbacusRole"));
        menuBean.setAccessKey((String) row.getAttribute("Accesskey"));
        menuBean.setAction((String) row.getAttribute("Action"));
        menuBean.setAppMenuId((Long) row.getAttribute("AppMenuId"));
        // menuBean.setAppName((String) row.getAttribute("AppName"));
        menuBean.setIsDisabled(retriveBoolean((String) row.getAttribute("IsDisabled")));
        menuBean.setIsNew(retriveBoolean((String) row.getAttribute("IsNew")));
        menuBean.setIsRendered(retriveBoolean((String) row.getAttribute("IsRendered")));
        menuBean.setLinkType((String) row.getAttribute("LinkType"));
        menuBean.setName((String) row.getAttribute("Name"));
        menuBean.setPath((String) row.getAttribute("Path"));
        menuBean.setPathLen((Long) row.getAttribute("Pathlen"));
        menuBean.setParentAppMenuId((Long) row.getAttribute("ParentAppMenuId"));
        menuBean.setRootAppMenuId((Long) row.getAttribute("RootMenuId"));
        menuBean.setSortOrder((Long) row.getAttribute("SortOrder"));
        menuBean.setTarget((String) row.getAttribute("Target"));
        menuBean.setMenuType((Long) row.getAttribute("MenuType"));
        return menuBean;
    }


    private boolean retriveBoolean(String booleanStringValue) {
        boolean retValue = false;
        if (booleanStringValue != null && "Y".equalsIgnoreCase(booleanStringValue)) {
            retValue = true;
        }
        return retValue;
    }


    private MethodExpression getMethodExpression(String name) {
        Class[] argtypes = new Class[1];
        argtypes[0] = ActionEvent.class;
        FacesContext facesCtx = FacesContext.getCurrentInstance();
        Application app = facesCtx.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesCtx.getELContext();
        return elFactory.createMethodExpression(elContext, name, null, argtypes);
    }

    private Map<String, MenuBean> populateTopNavigationalMenu(List<MenuBean> menuList) {
    
        List<MenuBean> noParentFoundList = new LinkedList<MenuBean>();
        Map<String, MenuBean> map = new LinkedHashMap<String, MenuBean>();
        for (MenuBean bean : menuList) {
            if (bean.getPathLen() == Constants.ZERO) {
                map.put(bean.getAppMenuId().toString(), bean);

            } else if (bean.getPathLen() == Constants.ONE) {
                if (map.containsKey(bean.getParentAppMenuId().toString())) {
                    MenuBean parentBean = map.get(bean.getParentAppMenuId().toString());
                    if(parentBean == null){
                        noParentFoundList.add(bean);
                    }
                    parentBean.getChildMenu().put(bean.getAppMenuId().toString(), bean);
                }
            } else if (bean.getPathLen() == Constants.TWO) {
                String[] pathArray = bean.getPath().split(PATH_DELIMETER);
                if (pathArray.length == Constants.FOUR) {
                    MenuBean parentBean = map.get(pathArray[Constants.ONE]);
                    MenuBean firstChildBean = parentBean.getChildMenu().get(pathArray[Constants.TWO]);
                    if(firstChildBean == null){
                        noParentFoundList.add(bean);
                    }
                    firstChildBean.getChildMenu().put(bean.getAppMenuId().toString(), bean);
                }
            } else if(bean.getPathLen() == Constants.THREE){
                String[] pathArray = bean.getPath().split(PATH_DELIMETER);
                if(pathArray.length == Constants.FIVE){
                    MenuBean parentBean = map.get(pathArray[Constants.ONE]);
                    MenuBean firstChildBean = parentBean.getChildMenu().get(pathArray[Constants.TWO]);
                    MenuBean secondChildBean = firstChildBean.getChildMenu().get(pathArray[Constants.THREE]);
                    if(secondChildBean == null){
                        noParentFoundList.add(bean);
                    }
                    secondChildBean.getChildMenu().put(bean.getAppMenuId().toString(), bean);
                }
            }
        }
        if(noParentFoundList.isEmpty()){
            return map;
        }else{
            for (MenuBean bean : noParentFoundList) {
               if (bean.getPathLen() == Constants.ONE) {
                    if (map.containsKey(bean.getParentAppMenuId().toString())) {
                        MenuBean parentBean = map.get(bean.getParentAppMenuId().toString());
                        parentBean.getChildMenu().put(bean.getAppMenuId().toString(), bean);
                    }
                } else if (bean.getPathLen() == Constants.TWO) {
                    String[] pathArray = bean.getPath().split(PATH_DELIMETER);
                    if (pathArray.length == Constants.FOUR) {
                        MenuBean parentBean = map.get(pathArray[Constants.ONE]);
                        MenuBean firstChildBean = parentBean.getChildMenu().get(pathArray[Constants.TWO]);
                        firstChildBean.getChildMenu().put(bean.getAppMenuId().toString(), bean);
                    }
                } else if(bean.getPathLen() == Constants.THREE){

                    String[] pathArray = bean.getPath().split(PATH_DELIMETER);
                    if(pathArray.length == Constants.FIVE){
                        MenuBean parentBean = map.get(pathArray[Constants.ONE]);
                        MenuBean firstChildBean = parentBean.getChildMenu().get(pathArray[Constants.TWO]);
                        MenuBean secondChildBean = firstChildBean.getChildMenu().get(pathArray[Constants.THREE]);
                        secondChildBean.getChildMenu().put(bean.getAppMenuId().toString(), bean);
                    }
                }
            }
            return map;
        }
        
    }

    public LinkedList<UIMenuBean> getNavigationMenu() {

        LinkedList<UIMenuBean> menuBeanList = new LinkedList<UIMenuBean>();

        // retrieve the menu data from cache
        Map<String, MenuBean> map = cacheManager.getMenuFromCache();

        // get the constants from Cache
        Map<String, String> constantsMap = cacheManager.getConstantsFromCache();

        // set the public url & protocol
        setPublicUrl(constantsMap.get(PUBLIC_URL));
        setProtocol(constantsMap.get(PROTOCOL));

        // iterate through the menu data map to construct a menu.
        for (Map.Entry<String, MenuBean> entry : map.entrySet()) {
            MenuBean value = entry.getValue();

            UIMenuBean bean = constructMenu(value);
            menuBeanList.add(bean);
        }
        return menuBeanList;
    }

    private UIMenuBean constructMenu(MenuBean value) {
        Boolean b = null;
        UIMenuBean bean = new UIMenuBean();
        logger.log("Username: " + ADFContext.getCurrent().getSecurityContext().getUserName());
        if (Constants.ONE == value.getMenuType()) {
            bean.setRichMenuflg(true);
            RichMenu richMenu = new RichMenu();
            richMenu.setText(value.getName());
            b = (Boolean) resolveSecurityStringExpression(value.getSecurityString());
            richMenu.setRendered(b.booleanValue());
            logger.log(value.getName() + " For Expression: " + value.getSecurityString() + " with rendered value:" + b);
            // check does the menu has child menu data mappings
            if (value.getChildMenu().size() > Constants.ZERO) {
                // retrieve the child menu data
                Map<String, MenuBean> childMap = value.getChildMenu();

                // Iterate though the child menu and construct a menu.
                for (Map.Entry<String, MenuBean> entry : childMap.entrySet()) {
                    MenuBean childValue = entry.getValue();

                    if (Constants.ONE == childValue.getMenuType()) {
                        RichMenu childRichMenu = constructRichMenu(childValue);
                        b = (Boolean) resolveSecurityStringExpression(childValue.getSecurityString());
                        childRichMenu.setRendered(b.booleanValue());
                        richMenu.getChildren().add(childRichMenu);
                    } else if (Constants.TWO == childValue.getMenuType()) {
                        RichCommandMenuItem rcmItem = new RichCommandMenuItem();
                        if (null != childValue.getAction()) {
                            rcmItem.setActionExpression(getMethodExpression(childValue.getAction()));
                        }
                        b = (Boolean) resolveSecurityStringExpression(childValue.getSecurityString());
                        rcmItem.setRendered(b.booleanValue());
                        rcmItem.setUseWindow(true);
                        rcmItem.setWindowWidth(WINDOW_WIDTH);
                        rcmItem.setWindowHeight(WINDOW_HEIGHT);
                        rcmItem.setText(childValue.getName());
                        richMenu.getChildren().add(rcmItem);

                    } else if (Constants.THREE == childValue.getMenuType()) {
                        RichGoMenuItem rgmItem = new RichGoMenuItem();
                        b = (Boolean) resolveSecurityStringExpression(childValue.getSecurityString());
                        rgmItem.setRendered(b.booleanValue());
                        rgmItem.setText(childValue.getName());
                        rgmItem.setTargetFrame(TARGET_PAGE_BLANK);
                        rgmItem.setDestination(constructUrlString(childValue.getAction()));
                        richMenu.getChildren().add(rgmItem);
                    }
                }
            }

            bean.setRichMenu(richMenu);

        }

        else if (Constants.TWO == value.getMenuType()) {
            RichCommandMenuItem rcmItem = new RichCommandMenuItem();
            rcmItem.setText(value.getName());
            b = (Boolean) resolveSecurityStringExpression(value.getSecurityString());
            rcmItem.setRendered(b.booleanValue());
            if (null != value.getAction()) {
                rcmItem.setActionExpression(getMethodExpression(value.getAction()));
            }
            rcmItem.setUseWindow(true);
            rcmItem.setWindowWidth(WINDOW_WIDTH);
            rcmItem.setWindowHeight(WINDOW_HEIGHT);
            bean.setRichMenuflg(false);
            bean.setRichCmdMenuItem(rcmItem);

        } else if (Constants.THREE == value.getMenuType()) {
            RichGoMenuItem rgmItem = new RichGoMenuItem();
            b = (Boolean) resolveSecurityStringExpression(value.getSecurityString());
            rgmItem.setRendered(b.booleanValue());
            rgmItem.setText(value.getName());
            rgmItem.setTargetFrame(TARGET_PAGE_BLANK);
            rgmItem.setDestination(constructUrlString(value.getAction()));
            bean.setRichGoMenuItem(rgmItem);
        }
        return bean;


    }

    private RichMenu constructRichMenu(MenuBean childValue) {
        RichMenu richMenu = new RichMenu();
        richMenu.setText(childValue.getName());

        // check if child data has another child
        if (childValue.getChildMenu().size() > 0) {

            // retrieve child child menu data
            Map<String, MenuBean> childMap = childValue.getChildMenu();
            // Iterate through the child menu
            for (Map.Entry<String, MenuBean> entry : childMap.entrySet()) {
                MenuBean val = entry.getValue();
                if (Constants.ONE == val.getMenuType()) {
                    RichMenu childRichMenu = constructRichMenu(val);
                    richMenu.getChildren().add(childRichMenu);
                } else if (Constants.TWO == val.getMenuType()) {
                    RichCommandMenuItem rcmItem = new RichCommandMenuItem();
                    if (null != val.getAction()) {
                        rcmItem.setActionExpression(getMethodExpression(val.getAction()));
                    }
                    rcmItem.setUseWindow(true);
                    rcmItem.setWindowWidth(WINDOW_WIDTH);
                    rcmItem.setWindowHeight(WINDOW_HEIGHT);
                    rcmItem.setText(val.getName());

                    richMenu.getChildren().add(rcmItem);
                } else if (Constants.THREE == val.getMenuType()) {
                    RichGoMenuItem rgmItem = new RichGoMenuItem();
                    rgmItem.setText(val.getName());
                    rgmItem.setTargetFrame(TARGET_PAGE_BLANK);
                    rgmItem.setDestination(constructUrlString(val.getAction()));
                    richMenu.getChildren().add(rgmItem);
                }
            }
        }
        return richMenu;
    }

    /**
     * This method evict the Menu Details Cache.
     */
    public void clearMenuCache() {
        logger.info("Start : Clearing the Menu Details & Constants Cache");
        Map<String, MenuBean> map = retrieveMenuDataFromDatabase();
        cacheManager.reWriteMenuDetailCache(map);
        Map<String, String> constantsMap = getConstantsFromDatabase();
        cacheManager.reWriteConstantsCache(constantsMap);
        logger.info("End : Clearing the Menu Details & Constants Cache");
    }

    public Map<String, String> getConstantsFromDatabase() {

        Row row = null;
        ViewObject svo = null;

        Map<String, String> map = new HashMap<String, String>();
        ApplicationModule applicationModule =
            Configuration.createRootApplicationModule(AM_DEFINITION, CONFIG_MENU_MODULE);
        try {
            svo = applicationModule.findViewObject(ABACUS_CONSTANTS_VIEW);
            RowSetIterator rsi = svo.createRowSetIterator(null);

            while (rsi.hasNext()) {
                row = rsi.next();
                map.put(String.valueOf(row.getAttribute("Code")), String.valueOf(row.getAttribute("CodeValue")));
            }

        } finally {
            Configuration.releaseRootApplicationModule(applicationModule, true);
        }

        return map;
    }

    private String constructUrlString(String url) {
        return this.protocol + "://" + this.publicUrl + url;
    }

    public void setPublicUrl(String publicUrl) {
        this.publicUrl = publicUrl;
    }

    public String getPublicUrl() {
        return publicUrl;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProtocol() {
        return protocol;
    }

    public static Object resolveSecurityStringExpression(String expression) {
        Boolean b = Boolean.FALSE;
        if (expression != null) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            Application app = facesContext.getApplication();
            ExpressionFactory elFactory = app.getExpressionFactory();
            ELContext elContext = facesContext.getELContext();
            ValueExpression valueExp = elFactory.createValueExpression(elContext, expression, Object.class);
            return valueExp.getValue(elContext);
        }
        return b;
    }


}

