package gov.ofda;


public class Constants {
    public Constants() {
        super();
    }
    
    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;
    public static final int FOUR = 4;
    public static final int FIVE = 5;
    // Cache key for Abacus application
    public static final String ABACUS_CACHE = "abacus-cache";
}
