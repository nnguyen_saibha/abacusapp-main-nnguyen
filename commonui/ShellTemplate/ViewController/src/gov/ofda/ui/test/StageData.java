package gov.ofda.ui.test;

import gov.ofda.ui.bean.UIMenuBean;

import java.util.LinkedList;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.RichMenu;
import oracle.adf.view.rich.component.rich.nav.RichCommandMenuItem;


public class StageData {
    public StageData() {
        super();
    }
    int tmp =0;
    
    public LinkedList<UIMenuBean> getRichMenu(){
        UIMenuBean uiMenu = null;
        LinkedList<UIMenuBean> richMenuList = new LinkedList<UIMenuBean>();
        uiMenu = retrievePopulateHome();
        richMenuList.add(uiMenu);
        uiMenu = retrieveNavigator();
        richMenuList.add(uiMenu);
        System.out.println("Populated richMenu");
        return richMenuList;
    }
    private UIMenuBean retrievePopulateHome(){
        UIMenuBean uiMenu = new UIMenuBean();
        uiMenu.setRichMenuflg(false);
        /*RichGoMenuItem goItem = new RichGoMenuItem();
        goItem.setId("go_item_t" + tmp++);
        goItem.setText("Home");
        goItem.setTargetFrame("_blank");
        goItem.setDestination("home");*/
        RichCommandMenuItem goItem = new RichCommandMenuItem();
        goItem.setId("go_item_t" + tmp++);
        goItem.setText("Action");
        uiMenu.setRichCmdMenuItem(goItem);
        return uiMenu;
    }
    
    private UIMenuBean retrieveNavigator(){
        UIMenuBean uiMenu = new UIMenuBean();
        uiMenu.setRichMenuflg(true);
        RichMenu menuMain = new RichMenu();
        menuMain.setId("na1");
        menuMain.setText("Navigator");
       
        //test
        RichCommandMenuItem goItem = new RichCommandMenuItem();
        goItem.setId("go_item_t" + tmp++);
        goItem.setText("Action");
       
        goItem.setActionExpression(getActionExpression("test"));
       
        menuMain.getChildren().add(goItem);
        
        uiMenu.setRichMenu(menuMain);
        return uiMenu;
    }
    
  
    
    private MethodExpression getMethodExpression(String name) {
           Class[] argtypes = new Class[1];
           argtypes[0] = ActionEvent.class;
           FacesContext facesCtx = FacesContext.getCurrentInstance();
           Application app = facesCtx.getApplication();
           ExpressionFactory elFactory = app.getExpressionFactory();
           ELContext elContext = facesCtx.getELContext();
           
           return elFactory.createMethodExpression(elContext, name, null, argtypes);
       }
    
    private MethodExpression getActionExpression(String expr) {
         FacesContext context = FacesContext.getCurrentInstance();
         Application app = context.getApplication();                                                
         ExpressionFactory elFactory = app.getExpressionFactory();  
         ELContext elContext = context.getELContext();                        
         MethodExpression mexpr = elFactory.createMethodExpression(elContext, 
                                                     expr, 
                                                     String.class,  
                                                     new Class[] { Void.TYPE });        
         return mexpr;
     }


}
