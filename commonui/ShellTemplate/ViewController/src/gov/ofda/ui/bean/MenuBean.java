package gov.ofda.ui.bean;

import java.io.Serializable;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MenuBean implements Serializable{
    @SuppressWarnings("compatibility:201778278798607940")
    private static final long serialVersionUID = 6521780277727319021L;

    public MenuBean() {
        super();
    }
    
    Long appMenuId;
    Long rootAppMenuId;
    Long pathLen;
    Long parentAppMenuId;
    String path;
    String name;
    String abacusRole;
    String appName;
    String action;
    String accessKey;
    String linkType;
    String target;
    boolean isNew;
    Long menuType;
    Long sortOrder;
    boolean isRendered;
    boolean isDisabled;
    
   // List<Map<String, MenuBean>> list = new ArrayList<>();
    
    Map<String, MenuBean> childMenu = new LinkedHashMap<String, MenuBean>();
    
    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAbacusRole(String abacusRole) {
        this.abacusRole = abacusRole;
    }

    public String getAbacusRole() {
        return abacusRole;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTarget() {
        return target;
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }

    public boolean isIsNew() {
        return isNew;
    }

    public void setIsRendered(boolean isRendered) {
        this.isRendered = isRendered;
    }

    public boolean isIsRendered() {
        return isRendered;
    }

    public void setIsDisabled(boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    public boolean isIsDisabled() {
        return isDisabled;
    }


    public void setAppMenuId(Long appMenuId) {
        this.appMenuId = appMenuId;
    }

    public Long getAppMenuId() {
        return appMenuId;
    }

    public void setRootAppMenuId(Long rootAppMenuId) {
        this.rootAppMenuId = rootAppMenuId;
    }

    public Long getRootAppMenuId() {
        return rootAppMenuId;
    }

    public void setPathLen(Long pathLen) {
        this.pathLen = pathLen;
    }

    public Long getPathLen() {
        return pathLen;
    }

    public void setParentAppMenuId(Long parentAppMenuId) {
        this.parentAppMenuId = parentAppMenuId;
    }

    public Long getParentAppMenuId() {
        return parentAppMenuId;
    }

    public void setMenuType(Long menuType) {
        this.menuType = menuType;
    }

    public Long getMenuType() {
        return menuType;
    }

    public void setSortOrder(Long sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Long getSortOrder() {
        return sortOrder;
    }


    public void setChildMenu(Map<String, MenuBean> childMenu) {
        this.childMenu = childMenu;
    }

    public Map<String, MenuBean> getChildMenu() {
        return childMenu;
    }
    
    public String getSecurityString() {
        String securityString = null;       
        if (abacusRole != null) {
            StringBuffer sb = new StringBuffer();
            List<String> items = Arrays.asList(abacusRole.split(","));
            int itemLength = items.size();
            int counter = 0;
            sb.append("#{");
            for (String s : items) {
                sb.append("securityContext.userInRole[");
                sb.append(s);
                sb.append("]");
                counter++;
                if (counter < itemLength) {
                    sb.append("||");
                }
            }
            sb.append("}");
            return sb.toString();
        }
       return null;
    }
    
    
}
