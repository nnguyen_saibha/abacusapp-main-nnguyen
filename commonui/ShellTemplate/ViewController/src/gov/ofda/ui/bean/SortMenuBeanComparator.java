package gov.ofda.ui.bean;

import java.util.Comparator;

public class SortMenuBeanComparator  implements Comparator<MenuBean>{
    public SortMenuBeanComparator() {
        super();
    }

    @Override
    public int compare(MenuBean m1, MenuBean m2) {
        // TODO Implement this method
        int retVal =0;
        if(m1!=null && m2!=null){
            if(m1.sortOrder < m2.sortOrder){
                retVal = -1;
            }else if (m1.sortOrder > m2.sortOrder){
                retVal = 1;
            }                   
        }
        return retVal;
    }
}
