package gov.ofda.ui.bean;

import java.io.Serializable;

import oracle.adf.view.rich.component.rich.RichMenu;
import oracle.adf.view.rich.component.rich.nav.RichCommandMenuItem;
import oracle.adf.view.rich.component.rich.nav.RichGoMenuItem;

public class UIMenuBean implements Serializable{
    @SuppressWarnings("compatibility:1585226470527339745")
    private static final long serialVersionUID = -7985513036255159534L;

    public UIMenuBean() {
        super();
    }
    
    private RichGoMenuItem richGoMenuItem;
    
    private boolean richMenuflg;
    
    private RichMenu richMenu;
    
    private RichCommandMenuItem richCmdMenuItem;


    public void setRichMenuflg(boolean richMenuflg) {
        this.richMenuflg = richMenuflg;
    }

    public boolean isRichMenuflg() {
        return richMenuflg;
    }

    public void setRichMenu(RichMenu richMenu) {
        this.richMenu = richMenu;
    }

    public RichMenu getRichMenu() {
        return richMenu;
    }

    public void setRichCmdMenuItem(RichCommandMenuItem richCmdMenuItem) {
        this.richCmdMenuItem = richCmdMenuItem;
    }

    public RichCommandMenuItem getRichCmdMenuItem() {
        return richCmdMenuItem;
    }

    public void setRichGoMenuItem(RichGoMenuItem richGoMenuItem) {
        this.richGoMenuItem = richGoMenuItem;
    }

    public RichGoMenuItem getRichGoMenuItem() {
        return richGoMenuItem;
    }

}
