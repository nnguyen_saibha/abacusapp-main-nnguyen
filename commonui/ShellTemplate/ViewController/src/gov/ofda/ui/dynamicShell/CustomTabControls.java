package gov.ofda.ui.dynamicShell;

import gov.ofda.uishell.Tab;
import gov.ofda.uishell.TabContext;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import oracle.adf.controller.ControllerContext;

import oracle.adf.view.rich.component.rich.nav.RichCommandNavigationItem;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.ItemEvent;


import org.apache.myfaces.trinidad.model.MenuModel;

public class CustomTabControls implements Serializable {
    @SuppressWarnings("compatibility:6206164467819260628")
    private static final long serialVersionUID = -8816012271068318423L;


    public void closeMultipleTabsDialogLsnr(DialogEvent de) {
        if (de.getOutcome() == DialogEvent.Outcome.ok && closeMultipleTabsList != null) {
            for (Object j : closeMultipleTabsList) {
                TabContext.getCurrentInstance().removeTab(Integer.parseInt("" + j));
            }
        }
        closeMultipleTabsList.clear();
        this.markDirty("VIEW");
        AdfFacesContext.getCurrentInstance().addPartialTarget(de.getComponent());
    }

    public void selectAllTabsToClose(ActionEvent ae) {
        closeMultipleTabsList = new ArrayList<Integer>();
        this.markDirty("VIEW");
        for (SelectItem i : this.getTabList()) {
            closeMultipleTabsList.add(i.getValue());
        }
    }
    
    public void tabActivatedEvent(ActionEvent action) {
        RichCommandNavigationItem tab = (RichCommandNavigationItem) action.getComponent();
        TabContext tabContext = TabContext.getCurrentInstance();
        // get tab index from id
        Object tabIndex = tab.getAttributes().get("tabIndex"); // NOTRANS
        tabContext.setSelectedTabIndex((Integer) tabIndex);
        this.markDirty("VIEW");
    }
    
    public List<SelectItem> getTabList() {   
        TabContext tabContext = TabContext.getCurrentInstance();
        MenuModel mm =tabContext.getTabMenuModel();
        Iterator i = mm.iterator();
        tabList = new ArrayList<SelectItem>();
        while (i.hasNext()) {
            Tab  j = (Tab) i.next();
            if (j.isActive()) {
                tabList.add(new SelectItem(j.getIndex(), j.getTitle()));
            }
        }
        return tabList;
    }

    public void unselectAllTabsToClose(ActionEvent actionEvent) {
        this.markDirty("VIEW");
        closeMultipleTabsList.clear();
    }

    public void setCloseMultipleTabsList(List closeMultipleTabsList) {
        this.markDirty("VIEW");
        this.closeMultipleTabsList = closeMultipleTabsList;
    }

    public List getCloseMultipleTabsList() {
        return closeMultipleTabsList;
    }
    
    public void tabRemoveListener(ItemEvent itemEvent) {
        if ("remove".equals(itemEvent.getType().toString())) {
            UIComponent ui = (UIComponent) itemEvent.getSource();
            Map<String, Object> mapattrs = ui.getAttributes();
            Integer i = (Integer) mapattrs.get("tabIndex");
            this.markDirty("VIEW");
            TabContext.getCurrentInstance().removeTab(i.intValue());
        }
    }
    
    private List<SelectItem> tabList = null;
    private List closeMultipleTabsList;
    public void markDirty(String s) {
        Map<String, Object> scope = null;
        if ("VIEW".equals(s))
            scope = AdfFacesContext.getCurrentInstance().getViewScope();
        else
            scope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        ControllerContext ctx = ControllerContext.getInstance();
        ctx.markScopeDirty(scope);
    }
}
