package gov.ofda.ui.listener;

import gov.ofda.menu.facade.MenuDetailFacade;

import gov.ofda.ui.bean.UIMenuBean;

import java.util.LinkedList;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class UISessionListener implements HttpSessionListener {

    private MenuDetailFacade facade;

    public UISessionListener() {
        super();
    }

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        facade = new MenuDetailFacade();
        HttpSession session = httpSessionEvent.getSession();
        MenuDetailFacade facade = new MenuDetailFacade();
        LinkedList<UIMenuBean> menuList = facade.getNavigationMenu();
        // CacheManager cacheManager = CacheManager.getInstance();
        session.setAttribute("menu", menuList);
        //  Map<String, MenuBean> list = cacheManager.getMenuFromCache();
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        // TODO Implement this method
    }
}
