package gov.ofda.ui.managedbean;

import gov.ofda.menu.facade.MenuDetailFacade;
import gov.ofda.ui.bean.UIMenuBean;

import java.io.IOException;
import java.io.Serializable;

import java.util.LinkedList;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.security.auth.Subject;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.adf.controller.ControllerContext;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.context.AdfFacesContext;

import weblogic.security.URLCallbackHandler;
import weblogic.security.services.Authentication;

import weblogic.servlet.security.ServletAuthentication;

public class DynamicTemplateMenu implements Serializable {
    @SuppressWarnings("compatibility:-4729626139818660954")
    private static final long serialVersionUID = -5384453812372133740L;
    private String _username;
    private String _password;
    private static String initialPage = "/faces/welcome";


    private static ADFLogger logger = ADFLogger.createADFLogger(DynamicTemplateMenu.class);

    private MenuDetailFacade facade;

    private LinkedList<UIMenuBean> menuList;

    public void setUsername(String _username) {
        this._username = _username;
    }

    public String getUsername() {
        return _username;
    }

    public void setPassword(String _password) {
        this._password = _password;
    }

    public String getPassword() {
        return _password;
    }

    public void setMenuList(LinkedList<UIMenuBean> menuList) {
        this.menuList = menuList;
    }

    public void markDirty(String s) {
        Map<String, Object> scope = null;
        if ("VIEW".equals(s))
            scope = AdfFacesContext.getCurrentInstance().getViewScope();
        else
            scope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        ControllerContext ctx = ControllerContext.getInstance();
        ctx.markScopeDirty(scope);
    }

    public LinkedList<UIMenuBean> getMenuList() {

        if (this.menuList == null) {
            facade = new MenuDetailFacade();
            this.menuList = facade.getNavigationMenu();
            //           this.markDirty("VIEW");
        }

        return this.menuList;
    }

    /**
     * getDisplayName fetches the username from the user store.
     * @return displayName
     */
    public String getDisplayName() {
        return ADFContext.getCurrent().getSecurityContext().getUserName();
    }

    /**
     * Will authenticate the user and redirect to default page upon successful redirection.
     * Will display a message if authentication is not successful or user is Inactive.
     * @return will return null in all cases.
     */
    public String performLogin() {
        byte[] pw = _password.getBytes();
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) ctx.getExternalContext().getRequest();
        try {
            Subject mySubject = Authentication.login(new URLCallbackHandler(_username, pw));
            ServletAuthentication.runAs(mySubject, request);
            String loginUrl = "/adfAuthentication?success_url="; // + ctx.getViewRoot().getViewId();
            loginUrl = loginUrl.concat(initialPage);
            logger.log(ADFLogger.NOTIFICATION, "login URL: " + loginUrl.toString());
            HttpServletResponse response = (HttpServletResponse) ctx.getExternalContext().getResponse();
            sendForward(request, response, loginUrl, true);
        } catch (FailedLoginException fle) {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Inactive or Incorrect Username or Password",
                                 "An Inactive or Incorrect Username or Password was specified");
            FacesContext.getCurrentInstance().addMessage("pt_it1", msg);
            logger.log(ADFLogger.ERROR, "Inactive or Incorrect Username or Password " + this.getUsername());
        } catch (LoginException le) {
            reportUnexpectedLoginError("loginException", le);
            logger.log(ADFLogger.ERROR, le.toString());
        }
        _password = null;
        return null;
    }

    /**
     * To forward and complete the response.
     *
     * @param request
     * @param response
     * @param loginUrl
     * @param islogin If True will check if the current user is active or not in appx_user database table .
     */
    private void sendForward(HttpServletRequest request, HttpServletResponse response, String loginUrl,
                             boolean islogin) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        RequestDispatcher dispatcher = request.getRequestDispatcher(loginUrl);
        try {
            dispatcher.forward(request, response);

        } catch (ServletException se) {
            logger.log(ADFLogger.ERROR, "The user is unable to login due to the exception", se);
            reportUnexpectedLoginError("ServletException", se);
        } catch (IOException ie) {
            logger.log(ADFLogger.ERROR, "The user is unable to login due to the exception", ie);
            reportUnexpectedLoginError("IOException", ie);
        }
        ctx.responseComplete();
    }

    /**
     *To report Unexpected Login Errors other than invalid username and password.
     *
     * @param errType
     * @param e
     */
    private void reportUnexpectedLoginError(String errType, Exception e) {
        FacesMessage msg =
            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected error During Login",
                             "Unexpected Error during Login (" + errType + "), please consult logs for detail");
        FacesContext.getCurrentInstance().addMessage("pt_it1", msg);
        e.printStackTrace();
    }

    /**
     * To log the user out of the application and redirect to searchAward.
     * @return
     */
    public String performLogout() {
        boolean oamConfigured = true;
        String oamConfiguredStr = null;
        StringBuffer sb = new StringBuffer();
        FacesContext ctx = FacesContext.getCurrentInstance();
        logger.log(ADFLogger.NOTIFICATION, this.getUsername() + " logged out");
        HttpServletRequest request = (HttpServletRequest) ctx.getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) ctx.getExternalContext().getResponse();
        // Get the current URL
        String requestUrl = request.getRequestURL().toString();

        // Get the context root
        String contextRoot = ctx.getExternalContext().getRequestContextPath();

        // Get host and port number
        requestUrl = requestUrl.substring(0, requestUrl.indexOf(contextRoot));
        System.out.println("RequestURL: "+requestUrl);
        sb.append("/adfAuthentication?logout=true");
        if(requestUrl != null){
            sb.append("&end_url=");
            sb.append(requestUrl);
            sb.append("/ofdalogout.html");
        }
//        if (oamConfigured) {
//            sb.append("/adfAuthentication?logout=true&end_url=/logout.html");
//        } else {
//            sb.append("/adfAuthentication?logout=true&en_url=/faces/welcome");
//        }
        logger.log(ADFLogger.NOTIFICATION, "logout URL: " + sb.toString());

        sendForward(request, response, sb.toString(), false);
        return null;
    }

    /**
     * This method triggers the clear cache method in Menu Details Facade.
     */
    public String clearMenuCache() {
        facade = new MenuDetailFacade();
        facade.clearMenuCache();
        return initialPage;
    }
}
