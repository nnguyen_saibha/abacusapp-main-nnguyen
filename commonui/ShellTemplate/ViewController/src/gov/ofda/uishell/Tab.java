package gov.ofda.uishell;

import java.io.Serializable;

import java.util.Collections;

import java.util.List;

import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.faces.context.FacesContext;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCDataControl;
import oracle.adf.model.binding.DCExecutableBindingDef;
import oracle.adf.view.rich.model.RegionModel;


/**
 * This class is not meant for public API.
 *
 * Copyright 2010, Oracle USA, Inc.
 */
public class Tab implements Serializable {
    @SuppressWarnings("compatibility:-2383389567196367932")
    private static final long serialVersionUID = 1L;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setTitle(String title) {
        _localizedName = title;
    }

    public String getTitle() {
        return _localizedName;
    }

    public int getIndex() {
        return _index;
    }

    /**
     * This will return the binding container associated with the current tab.And this should return a non-null value
     * unless user invokes it from a execution context whose bindingContainer does't have a reference to dynamic tab shell page template.
     * @return binding container associated with current tab
     */
    public DCBindingContainer getBinding() {
        DCBindingContainer bindingContainer = null;
        if (BindingContext.getCurrent() != null && BindingContext.getCurrent().getCurrentBindingsEntry() != null) {
            bindingContainer =
                (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry().get("r" + _index); // NOTRANS
            //If tab.getBinding() invoked from actual template then BindingContext.getCurrent().getCurrentBindingsEntry() will return template's pagedefinition file
            //but if it is invoked from a consuming page then BindingContext.getCurrent().getCurrentBindingsEntry() will refer to the consuming page's pagedef file
            //Then we need to look at all executable bindings which are refering to "oracle.ui.pattern.dynamicShell.model.dynamicTabShellDefinition"
            //and use that to retrieve the tab binding.
            if (bindingContainer == null) {
                List execBindings =
                    ((DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry()).getExecutableBindings();
                if (execBindings != null) {
                    for (Object binding : execBindings) {
                        DCExecutableBindingDef execDef =
                            ((DCExecutableBindingDef) ((DCBindingContainer) binding).getExecutableDef());
                        if (execDef != null &&
                            "oracle.ui.pattern.dynamicShell.model.dynamicTabShellDefinition".equals(execDef.getFullName())) // NOTRANS
                        {
                            return (DCBindingContainer) ((DCBindingContainer) binding).get("r" + _index); // NOTRANS
                        }
                    }
                }
            }

        }
        return bindingContainer;
    }

    public void setActive(boolean rendered) {
        _isActive = rendered;

        /* if (!_isActive)
            setDirty(false);*/
    }

    public boolean isActive() {
        return _isActive;
    }

    public void setDirty(boolean isDirty) {
        _isDirty = isDirty;
    }

    public boolean isDirty() {
        // System.out.println("Is Transaction dirty "+getBinding().getDataControl().getApplicationModule().getTransaction().isDirty());
        //System.out.println("Tab name: "+_localizedName+"Dirty Flag: "+_isDirty);
        //System.out.println("Tab name: "+_localizedName+"isActive Flag: "+_isActive);
        //if( (!_isDirty) && _isActive){
        if (checkForDirty()) {
            _isDirty = true;
        } else {
            _isDirty = false;
        }
        //}

        //System.out.println("Tab name: "+_localizedName+" Dirty Flag:"+_isDirty);
        return _isDirty;
    }

    public void setTaskflowId(TaskFlowId id) {
        _taskflowId = id;
    }

    public TaskFlowId getTaskflowId() {
        return _taskflowId;
    }

    public void setParameters(Map<String, Object> parameters) {
        _parameters = parameters;
    }

    public Map<String, Object> getParameters() {
        return _parameters;
    }

    public List<Tab> getChildren() {
        return Collections.emptyList();
    }

    Tab(int index, TaskFlowId id) {
        _index = index;
        _taskflowId = id;
    }

    private boolean checkForDirty() {
        DCBindingContainer bindingContainer = null;
        boolean dirtyFlg = false;
        bindingContainer = getBinding();
        if (bindingContainer != null) {
            //System.out.println("Tab Binding " + bindingContainer.toString());
            List ebList = bindingContainer.getExecutableBindings();
            if (ebList != null) {
                for (Object ebObject : ebList) {
                    if (ebObject instanceof DCBindingContainer) {
                        DCBindingContainer taskFlowBc = (DCBindingContainer) ebObject;
                        if (taskFlowBc != null && !(taskFlowBc.toString().contains("search"))) {
                            //  System.out.println(" taskFlowBc Tab Binding " + taskFlowBc.toString());
                            List taskFlowBcList = taskFlowBc.getExecutableBindings();
                            if (taskFlowBcList != null) {
                                for (Object taskFlowBc1 : taskFlowBcList) {
                                    //   System.out.println("taskFlowBc1 Tab Binding " + taskFlowBc1.toString());
                                    if (taskFlowBc1 instanceof DCBindingContainer) {
                                        //    System.out.println("DCBindingContainerTab taskFlowBc1 Binding " + taskFlowBc1.toString());
                                        if (taskFlowBc1 != null) {
                                            DCBindingContainer taskFlowBcontain1 = (DCBindingContainer) taskFlowBc1;
                                            List taskFlowBc1List = taskFlowBcontain1.getExecutableBindings();
                                            if (taskFlowBc1List != null) {
                                                for (Object taskFlowBc2 : taskFlowBc1List) {
                                                    if (taskFlowBc2 instanceof DCBindingContainer) {
                                                        DCBindingContainer taskFlowBcontain2 =
                                                            (DCBindingContainer) taskFlowBc2;
                                                        //  System.out.println("Tab Binding taskFlowBc2 " + taskFlowBc2.toString());
                                                        List taskFlowBc2List =
                                                            taskFlowBcontain2.getExecutableBindings();
                                                        DCDataControl dc = taskFlowBcontain2.getDataControl();
                                                        if (dc != null) {

                                                            if (dc.isTransactionDirty() || dc.isTransactionModified())
                                                                dirtyFlg = true;
                                                            //    System.out.println("Dirty Flg:"+dirtyFlg);
                                                        }

                                                    }
                                                }
                                            }

                                        }
                                    }
                                }

                            }

                        }
                    }

                }
            }

        }
        Boolean isDirty=false;
        String elExpression="#{viewScope.tabContext.tabs["+_index+"].binding.regionModel}";
        Object t=getExpressionObjectReference(elExpression);
        if(t!=null)
            isDirty=((RegionModel)t).isDataDirty();
        return dirtyFlg||isDirty;

    }
    /**
     * Create value binding for EL exression
     * @param expression
     * @return Object
     */
    public static Object getExpressionObjectReference(String expression) {
        FacesContext fc = FacesContext.getCurrentInstance();
        ELContext elctx = fc.getELContext();
        ExpressionFactory elFactory = 
            fc.getApplication().getExpressionFactory();
        return elFactory.createValueExpression(elctx, expression, 
                                               Object.class).getValue(elctx);
    }
    private final int _index;

    private boolean _isActive = false;
    private boolean _isDirty = false;
    private String _localizedName;
    private TaskFlowId _taskflowId;
    private String id;
    private Map<String, Object> _parameters;
}
