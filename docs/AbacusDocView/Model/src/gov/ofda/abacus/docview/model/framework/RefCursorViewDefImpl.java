package gov.ofda.abacus.docview.model.framework;

import java.util.Arrays;
import java.util.List;

import oracle.jbo.AttributeDef;
import oracle.jbo.server.ViewDefImpl;

public class RefCursorViewDefImpl extends ViewDefImpl {

   

    @Override
    protected void finishedLoading() {
        super.finishedLoading();
        retrieveParams = getAttrs("RetrieveIndex", true);
        countParams = getAttrs("CountIndex", true);
        targetAttrs = getAttrs("TargetIndex", false);
        retrieveFnCall = createFnCall("RetrieveFn", retrieveParams.size());
        countFnCall = createFnCall("CountFn", countParams.size());
    }
    
    private List<AttributeDef> getAttrs(String indexPropertyName, boolean bindVariables) {
        AttributeDef[] attrs = bindVariables ?
            getVariableManager().getDeclaredVariables() :
            getAttributeDefs();
        AttributeDef[] workspace = new AttributeDef[attrs.length];
        int numAttrs = 0;
        for (AttributeDef attr : attrs) {
            String indexStr = (String) attr.getProperty(indexPropertyName);
            if (indexStr != null && indexStr.length() != 0) {
                int index = Integer.valueOf(indexStr);
                workspace[index - 1] = attr;
                if (index > numAttrs) numAttrs = index;
            }
        }
        return Arrays.asList(workspace).subList(0, numAttrs);
    }
    private String createFnCall(String fnNameProp, int numParams) {
        String fnName = (String) getProperty(fnNameProp);
        StringBuffer buffer = new StringBuffer(fnName.length() + numParams * 3 + 17);
        buffer.append("begin ? := ");
        buffer.append(fnName);
        buffer.append('(');
        for (int i=0; i < numParams; i++) {
            if (i>0) {
                buffer.append(", ");
            }
            buffer.append('?');
        }
        buffer.append("); end;");
        return buffer.toString();
    }

    public void setRetrieveParams(List<AttributeDef> retrieveParams) {
        this.retrieveParams = retrieveParams;
    }

    public List<AttributeDef> getRetrieveParams() {
        return retrieveParams;
    }

    public void setCountParams(List<AttributeDef> countParams) {
        this.countParams = countParams;
    }

    public List<AttributeDef> getCountParams() {
        return countParams;
    }

    public void setTargetAttrs(List<AttributeDef> targetAttrs) {
        this.targetAttrs = targetAttrs;
    }

    public List<AttributeDef> getTargetAttrs() {
        return targetAttrs;
    }

    public void setRetrieveFnCall(String retrieveFnCall) {
        this.retrieveFnCall = retrieveFnCall;
    }

    public String getRetrieveFnCall() {
        return retrieveFnCall;
    }

    public void setCountFnCall(String countFnCall) {
        this.countFnCall = countFnCall;
    }

    public String getCountFnCall() {
        return countFnCall;
    }
    private List<AttributeDef> retrieveParams;
    private List<AttributeDef> countParams;
    private List<AttributeDef> targetAttrs;
    private String retrieveFnCall;
    private String countFnCall;
}
