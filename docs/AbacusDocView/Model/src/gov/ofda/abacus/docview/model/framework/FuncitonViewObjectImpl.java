package gov.ofda.abacus.docview.model.framework;

import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.ResultSet;

import java.sql.SQLException;

import java.sql.Types;

import oracle.jbo.JboException;
import oracle.jbo.server.SQLBuilder;
import oracle.jbo.server.ViewObjectImpl;

import oracle.jbo.server.ViewRowImpl;
import oracle.jbo.server.ViewRowSetImpl;

import oracle.jdbc.OracleTypes;

public class FuncitonViewObjectImpl extends ViewObjectImpl {
    
    protected void create() {
            getViewDef().setQuery(null);
            getViewDef().setSelectClause(null);
            setQuery(null);
        }
    /**
     * executeQueryForCollection - overridden for custom java data source support.
     */
    protected void executeQueryForCollection(Object qc, Object[] params, int noUserParams) {
        storeNewResultSet(qc, retrieveRefCursor(qc, params));
        super.executeQueryForCollection(qc, params, noUserParams);
    }

    private ResultSet retrieveRefCursor(Object qc, Object[] params) {
        ResultSet rs =
            (ResultSet)callStoredFunction(OracleTypes.CURSOR, "art_docs_pkg.retrieve_docs(?,?)", new Object[] { getNamedBindParamValue("awardID",
                                                                                                                                       params),
                                                                                                                getNamedBindParamValue("isOfdaFlg",
                                                                                                                                       params) });
        return rs;
    }

    /**
     * Overridden framework method.
     *
     * Return true if the datasource has at least one more record to fetch.
     */
    protected boolean hasNextForCollection(Object qc) {
        ResultSet rs = getResultSet(qc);
        boolean nextOne = false;
        try {
            nextOne = rs.next();
            /*
            * When were at the end of the result set, mark the query collection
            * as "FetchComplete".
            */
            if (!nextOne) {
                setFetchCompleteForCollection(qc, true);
                /*
              * Close the result set, we're done with it
              */
                rs.close();
            }
        } catch (SQLException s) {
            throw new JboException(s);
        }
        return nextOne;
    }
    
    /**
         * Overridden framework method.
         *
         * The framework gives us a chance to clean up any resources related
         * to the datasource when a query collection is done being used.
         */
        protected void releaseUserDataForCollection(Object qc, Object rs) {
            /*
               * Ignore the ResultSet passed in since we've created our own.
               * Fetch the ResultSet from the User-Data context instead
               */
            ResultSet userDataRS = getResultSet(qc);
            if (userDataRS != null) {
                try {
                    userDataRS.close();
                } catch (SQLException s) {
                    /* Ignore */
                }
            }
            super.releaseUserDataForCollection(qc, rs);
        }


        /**
         * Simplifies calling a stored function with bind variables
         *
         * You can use the NUMBER, DATE, and VARCHAR2 constants in this
         * class to indicate the function return type for these three common types,
         * otherwise use one of the JDBC types in the java.sql.Types class.
         *
         * NOTE: If you want to invoke a stored procedure without any bind variables
         * ====  then you can just use the basic getDBTransaction().executeCommand()
         *
         * @param sqlReturnType JDBC datatype constant of function return value
         * @param stmt stored function statement
         * @param bindVars Object array of parameters
         * @return function return value as an Object
         */
        protected Object callStoredFunction(int sqlReturnType, String stmt, Object[] bindVars) {
            CallableStatement st = null;
            try {
                st = getDBTransaction().createCallableStatement("begin ? := " + stmt + "; end;", 0);
                st.registerOutParameter(1, sqlReturnType);
                if (bindVars != null) {
                    for (int z = 0; z < bindVars.length; z++) {
                        st.setObject(z + 2, bindVars[z]);
                    }
                }
                st.executeUpdate();
                return st.getObject(1);
            } catch (SQLException e) {
                throw new JboException(e);
            }
        }

        /**
         * Store a new result set in the query-collection-private user-data context
         */
        private void storeNewResultSet(Object qc, ResultSet rs) {
            ResultSet existingRs = getResultSet(qc);
            // If this query collection is getting reused, close out any previous rowset
            if (existingRs != null) {
                try {
                    existingRs.close();
                } catch (SQLException s) {
                }
            }
            setUserDataForCollection(qc, rs);
            hasNextForCollection(qc); // Prime the pump with the first row.
        }

        /**
         * Retrieve the result set wrapper from the query-collection user-data
         */
        private ResultSet getResultSet(Object qc) {
            return (ResultSet)getUserDataForCollection(qc);
        }


        /**
         * Overridden framework method.
         *
         * The role of this method is to "fetch", populate, and return a single row
         * from the datasource by calling createNewRowForCollection() and populating
         * its attributes using populateAttributeForRow().
         */
        protected ViewRowImpl createRowFromResultSet(Object qc, ResultSet rs) {
            /*
               * We ignore the JDBC ResultSet passed by the framework (null anyway) and
               * use the resultset that we've stored in the query-collection-private
               * user data storage
               */
            rs = getResultSet(qc);

            /*
               * Create a new row to populate
               */
            ViewRowImpl r = createNewRowForCollection(qc);

            try {
                /*
                 * Populate new row by attribute slot number for current row in Result Set
                 */
                //TODO
                populateAttributeForRow(r, 0, rs.getLong(1));
                populateAttributeForRow(r, 1, rs.getLong(2));
                populateAttributeForRow(r, 2, rs.getString(3));
                populateAttributeForRow(r, 3, rs.getString(4));
                populateAttributeForRow(r, 4, rs.getString(5));
                populateAttributeForRow(r, 5, rs.getString(6));
                populateAttributeForRow(r, 6, rs.getBlob(7));
                populateAttributeForRow(r, 7, rs.getLong(8));
                populateAttributeForRow(r, 8, rs.getLong(9));
                populateAttributeForRow(r, 9, rs.getLong(10));
                populateAttributeForRow(r, 10, rs.getDate(11));
                populateAttributeForRow(r, 11, rs.getDate(12));
                populateAttributeForRow(r, 12, rs.getDate(13));
                populateAttributeForRow(r, 13, rs.getString(14));
                populateAttributeForRow(r, 14, rs.getString(15));
                populateAttributeForRow(r, 15, rs.getLong(16));
                populateAttributeForRow(r, 16, rs.getLong(17));
                populateAttributeForRow(r, 17, rs.getLong(18));

            } catch (SQLException s) {
                throw new JboException(s);
            }
            return r;
        }

        /**
         * Overridden framework method
         *
         * Return the number of rows that would be returned by executing
         * the query implied by the datasource. This gives the developer a
         * chance to perform a fast count of the rows that would be retrieved
         * if all rows were fetched from the database. In the default implementation
         * the framework will perform a SELECT COUNT(*) FROM (...) wrapper query
         * to let the database return the count. This count might only be an estimate
         * depending on how resource-intensive it would be to actually count the rows.
         */
        public long getQueryHitCount(ViewRowSetImpl viewRowSet) {
            Object[] params = viewRowSet.getParameters(true);
            BigDecimal result =
                (BigDecimal)callStoredFunction(Types.NUMERIC, "art_docs_pkg.retrieve_docs_count(?,?)", new Object[] { getNamedBindParamValue("awardID",
                                                                                                                                             params),
                                                                                                                      getNamedBindParamValue("isOfdaFlg",
                                                                                                                                             params) });
            return result.longValue();
        }

        private Object getNamedBindParamValue(String varName, Object[] params) {
            Object result = null;
            if (getBindingStyle() == SQLBuilder.BINDING_STYLE_ORACLE_NAME) {
                if (params != null) {
                    for (Object param : params) {
                        Object[] nameValue = (Object[])param;
                        String name = (String)nameValue[0];
                        if ("awardID".equals(varName)) {
                            return (Number)nameValue[1];
                        }
                        if (name.equals(varName)) {
                            return (String)nameValue[1];
                        }
                    }
                }
            }
            throw new JboException("No bind variable named '" + varName + "'");
        }


}
