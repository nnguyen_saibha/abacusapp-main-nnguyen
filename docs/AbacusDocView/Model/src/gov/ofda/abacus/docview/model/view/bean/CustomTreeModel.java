package gov.ofda.abacus.docview.model.view.bean;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;


public class CustomTreeModel {
    public CustomTreeModel() {
        super();
    }
    
    public CustomTreeModel(String name) {
        this.name = name;
    }
    public CustomTreeModel(String name,Long docId, String sourceId, Long suppSourceId, Long sourceType) {
        this.name = name;
        this.docId = docId;
        this.sourceId = sourceId;
        this.suppSourceId = suppSourceId;
        this.sourceType = sourceType;
    }
    public String name;
    
    public Long docId;
    
    public String sourceId;
    
    public Long suppSourceId;
    
    public Long sourceType;
    
    public TreeMap  childMap;
    
    public   LinkedList childNodes ;
   
    public   List childNodesList ;

    public void setChildNodesList(List childNodesList) {
        this.childNodesList = childNodesList;
    }

    public List getChildNodesList() {
        return childNodesList;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDocId(Long docId) {
        this.docId = docId;
    }

    public Long getDocId() {
        return docId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSuppSourceId(Long suppSourceId) {
        this.suppSourceId = suppSourceId;
    }

    public Long getSuppSourceId() {
        return suppSourceId;
    }

    public void setChildNodes(LinkedList<CustomTreeModel> childNodes) {
        this.childNodes = childNodes;
    }

    public LinkedList<CustomTreeModel> getChildNodes() {       
        if(childMap != null){
            childNodes = new LinkedList(childMap.values());
        }
        return childNodes;
    }

    public void setChildMap(TreeMap childMap) {
        this.childMap = childMap;
    }

    public TreeMap getChildMap() {
        return childMap;
    }
    
    public void addChildNode(CustomTreeModel childNode){
        if(childMap == null){
           childMap = new TreeMap();
        }
        childMap.put(childNode.getName(),childNode);
    }

    public void setSourceType(Long sourceType) {
        this.sourceType = sourceType;
    }

    public Long getSourceType() {
        return sourceType;
    }

    public void populateChildNodes(){
        if(childMap != null){
            childNodes = new LinkedList(childMap.values());
            childNodesList = new ArrayList(childMap.values());
        }
    }

}
