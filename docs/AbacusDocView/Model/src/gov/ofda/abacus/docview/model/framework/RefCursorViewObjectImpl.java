package gov.ofda.abacus.docview.model.framework;

import java.sql.CallableStatement;

import java.sql.ResultSet;

import java.sql.SQLException;

import java.util.List;

import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.AttributeDef;
import oracle.jbo.server.DBTransaction;
import oracle.jbo.server.ViewDefImpl;
import oracle.jbo.server.ViewObjectImpl;

import oracle.jdbc.OracleTypes;

public class RefCursorViewObjectImpl extends ViewObjectImpl {
    ADFLogger logger = ADFLogger.createADFLogger(RefCursorViewObjectImpl.class);
    public RefCursorViewObjectImpl(String string, ViewDefImpl viewDefImpl) {
        super(string, viewDefImpl);
    }

    public RefCursorViewObjectImpl() {
        super();
    }
    @Override
    protected void executeQueryForCollection(Object qc, Object[] paramValues,
                                             int numParams) {
        RefCursorViewDefImpl def = (RefCursorViewDefImpl) getViewDef();
        CallableStatement stmt = getDBTransaction().createCallableStatement(def.getRetrieveFnCall(), DBTransaction.DEFAULT);
        try {
            registerParameters(stmt, def.getRetrieveParams(), paramValues);
            setResultSetForCollection(qc, (ResultSet) getResult(stmt));
        } catch (SQLException e) {
            logger.warning(e);
        }       
        super.executeQueryForCollection(qc, paramValues, numParams);
    }
    
    private void registerParameters(CallableStatement stmt,
                                    List<AttributeDef> paramDefs,
                                    Object[] paramValues) throws SQLException {
        stmt.registerOutParameter(1, OracleTypes.CURSOR);
        int i=2;
        for (AttributeDef paramDef : paramDefs) {
            Object paramValue = getNamedWhereClauseParam(paramDef.getName());
            stmt.setObject(i++, paramValue);
        }
    }
    private Object getResult(CallableStatement stmt) throws SQLException {
        stmt.execute();
        return (ResultSet) stmt.getObject(1);
    }
}
