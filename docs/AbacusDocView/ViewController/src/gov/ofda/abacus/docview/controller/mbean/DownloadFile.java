package gov.ofda.abacus.docview.controller.mbean;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import java.sql.Blob;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.faces.context.FacesContext;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.domain.BlobDomain;
import oracle.jbo.domain.ClobDomain;


public class DownloadFile implements Serializable {
    @SuppressWarnings("compatibility:-7061358945305415462")
    private static final long serialVersionUID = 1L;
    private static ADFLogger logger = ADFLogger.createADFLogger(DownloadFile.class);
    private Long iSourceType;
    private Long iDocsId;
    private String fileName;
    private Long iSuppSourceId;
    private String isAsist;

    public void setIsAsist(String isAsist) {
        this.isAsist = isAsist;
    }

    public String getIsAsist() {
        return isAsist;
    }
    private Map<String, Map<String, String>> selectedList = new HashMap<String, Map<String, String>>();


    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public DownloadFile() {
        super();
    }


    public void setISuppSourceId(Long iSuppSourceId) {
        this.iSuppSourceId = iSuppSourceId;
    }

    public Long getISuppSourceId() {
        return iSuppSourceId;
    }

    public void fileToDownload(FacesContext facesContext,
                               java.io.OutputStream outputStream) throws UnsupportedEncodingException, IOException {
        if (iDocsId != null && iSuppSourceId == 2) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding adocs_ctrl = bindings.getOperationBinding("fetchCable");


            try {
                HashMap docsMap = (HashMap) adocs_ctrl.execute();
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
                response.setHeader("Pragma", "public");
                byte[] content = null;
                ClobDomain bd = (ClobDomain) docsMap.get(iDocsId);
                int length = (int) bd.getLength();
                int buffersize = 1024;
                byte[] buffer = new byte[buffersize];
                InputStream is = bd.getAsciiStream();
                ServletOutputStream out;
                response.setCharacterEncoding("UTF-8");
                response.setHeader("Content-disposition",
                                   "attachment;filename=\"" + "abacus_cable_" + iDocsId + ".rtf\"");
                out = response.getOutputStream();
                while ((length = is.read(buffer)) != -1) {
                    out.write(buffer, 0, length);
                }
                is.close();
                out.flush();

            } catch (IOException ex) {
                logger.log(logger.ERROR, "IO Exception in Download File  ", ex.getCause());
                ex.printStackTrace();
            }
        } else {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding adocs_ctrl = bindings.getOperationBinding("fetchDocument");


            try {
                //adocs_ctrl.getParamsMap().put(arg0, arg1)
                HashMap docsMap = (HashMap) adocs_ctrl.execute();
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
                response.setHeader("Pragma", "public");
                byte[] content = null;
                Blob bd = (Blob) docsMap.get(fileName);


                int length = (int) bd.length();
                int buffersize = 1024;
                byte[] buffer = new byte[buffersize];
                InputStream is = bd.getBinaryStream();
                ServletOutputStream out;
                if (isAsist != null && isAsist.equals("Y")){
                    String origFileName = fileName.substring(0, fileName.lastIndexOf('.'));
                    String extnFileName = fileName.substring(fileName.lastIndexOf('.'));
                    String newFileName  = (origFileName+iSourceType+iDocsId).replaceAll("[^a-zA-Z0-9]", "_");
                    fileName = newFileName+extnFileName;  
                    response.setHeader("Content-disposition", "attachment;filename=\"" + fileName + "\"");
                }else{
                response.setHeader("Content-disposition", "attachment;filename=\"" + fileName + "\"");
                }
                out = response.getOutputStream();
                while ((length = is.read(buffer)) != -1) {
                    out.write(buffer, 0, length);
                }
                is.close();
                out.flush();

            } catch (IOException ex) {
                logger.log(logger.ERROR, "IO Exception in Download File  ", ex.getCause());
                ex.printStackTrace();
            } catch (SQLException e) {
                logger.log(logger.ERROR, "SQL Exception in Download File  ", e.getCause());
            }

        }
    }

    @SuppressWarnings({ "unchecked", "oracle.jdeveloper.java.nested-assignment" })
    public void downloadAllFiles(FacesContext facesContext,
                                 java.io.OutputStream outputStream) throws UnsupportedEncodingException, IOException,
                                                                           SQLException {

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding adocs_ctrl = bindings.getOperationBinding("fetchDocument2");
        Map requestScope = ADFContext.getCurrent().getRequestScope();
        Iterator<String> it = selectedList.keySet().iterator();

        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setContentType("application/x-download");
        String zipName = (String) requestScope.get("zipName");
        
        response.setHeader("Content-Disposition", "attachment; filename=\"" + requestScope.get("zipName") + ".zip\"");
        ZipOutputStream output = null;
        output = new ZipOutputStream(new BufferedOutputStream(response.getOutputStream(), 8192));
        byte[] buffer = new byte[8192];

        while (it.hasNext()) {
            String t = it.next();
            Map<String, String> tt = selectedList.get(t);
            
            adocs_ctrl.getParamsMap().put("iDocId", tt.get("docId"));
            adocs_ctrl.getParamsMap().put("inSourceType", tt.get("sourceType"));
            InputStream is = null;

            try {
                
                
                HashMap docsMap = (HashMap) adocs_ctrl.execute();
                Blob bd = (Blob) docsMap.get(tt.get("fileName"));
                is = new BufferedInputStream(bd.getBinaryStream(), 8192);
                fileName = ((tt.get("sec")!=null?"sec"+tt.get("sec")+"_":"")+(tt.get("subsec") !=null?"subsec"+tt.get("subsec"):"")+"_"+tt.get("fileName"));
                String origFileName = fileName.substring(0, fileName.lastIndexOf('.'));
                String extnFileName = fileName.substring(fileName.lastIndexOf('.'));
                String newFileName  = (origFileName+tt.get("sourceType")+tt.get("docId")).replaceAll("[^a-zA-Z0-9]", "_");
                fileName = newFileName+extnFileName;
                output.putNextEntry(new ZipEntry(fileName));
                
                for (int length = 0; (length = is.read(buffer)) > 0;) {
                    output.write(buffer, 0, length);
                }

            } catch (IOException ex) {
                logger.log(logger.ERROR, "IO Exception in Download File  ", ex.getCause());
                ex.printStackTrace();
            } finally {
                if(is!=null)
                    is.close();
                if(output != null)
                output.closeEntry();
            }
        }
        selectedList.clear();
        output.close();
    }

    public void setISourceType(Long iSourceType) {
        this.iSourceType = iSourceType;
    }

    public Long getISourceType() {
        return iSourceType;
    }

    public void setIDocsId(Long iDocsId) {
        this.iDocsId = iDocsId;
    }

    public Long getIDocsId() {
        return iDocsId;
    }

    public void initializeValidDocTypes() {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        List<String> list = (List<String>) pageFlowMap.get("validDocTypeList");
        // process docTypeCodeList as comma seperated values
        String docTypeCode = "";
        if (list != null && list.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String s : list) {
                sb.append(s);
                sb.append(",");
            }
            docTypeCode = sb.toString().substring(0, sb.toString().length() - 1);
        }
        pageFlowMap.put("validDocTypeStr", docTypeCode);
    }

    public String getDocTypeNames() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("getDocumentTypeNames");
        exec.execute();
        return ": " + (String) exec.getResult();
    }

    public void setSelectedList(Map<String, Map<String, String>> selectedList) {
        this.selectedList = selectedList;
    }

    public Map<String, Map<String, String>> getSelectedList() {
        return selectedList;
    }
}
