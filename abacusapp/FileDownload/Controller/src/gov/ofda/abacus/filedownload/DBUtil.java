package gov.ofda.abacus.filedownload;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.Hashtable;

import javax.naming.Context;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;

public final class DBUtil {
    private static DataSource datasource;

    public DBUtil() {
        super();
    }
    static {
        Context initContext;
        try {

            Context ctx = new InitialContext();
            datasource= (DataSource)ctx.lookup("jdbc/filedownload");
           // datasource = (DataSource)initContext.lookup("jdbc/filedownload");
            System.out.println("Intialized properly");
        } catch (NamingException e) {
            // TODO Auto-generated catch block
            System.out.println("Error in initialization of filedownload");
            e.printStackTrace();
        }
    }

    public static Connection getConnection() throws SQLException {
        
        Connection connection = null;
        if (datasource != null && datasource.getConnection() != null) {
            connection = datasource.getConnection();
        } 
        return connection;
    }
}
