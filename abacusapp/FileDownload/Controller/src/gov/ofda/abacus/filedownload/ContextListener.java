package gov.ofda.abacus.filedownload;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ContextListener implements ServletContextListener {
    private ServletContext context = null;

    public void contextInitialized(ServletContextEvent event) {
        context = event.getServletContext();
        DBUtil dbClass = new DBUtil();  
    }

    public void contextDestroyed(ServletContextEvent event) {
        context = event.getServletContext();
    }
}
