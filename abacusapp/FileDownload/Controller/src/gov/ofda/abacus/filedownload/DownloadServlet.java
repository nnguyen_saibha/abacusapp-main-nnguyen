package gov.ofda.abacus.filedownload;

import java.io.File;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;

public class DownloadServlet extends HttpServlet implements Servlet {
    @SuppressWarnings("compatibility:-2592921232344748221")
    private static final long serialVersionUID = 1L;
    private static final int BUFSIZE = 4096;

    public DownloadServlet() {
        super();
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response) {
        Connection connection = null;
        int length = 0;
        String sql = null;
        Long docKey = null;
        StringBuffer sb = new StringBuffer();
        PreparedStatement stmt = null;
        try {
            connection = DBUtil.getConnection();
            if(connection!= null){
                ServletOutputStream outStream = response.getOutputStream();
                String docSeqStr = request.getParameter("artDocID");
                //System.out.println("Doc Seq:" + docSeqStr);
                String abacusdocSeqStr = request.getParameter("abacusDocID");
                //System.out.println("Doc Seq:" + abacusdocSeqStr);
                if (docSeqStr != null || abacusdocSeqStr != null) {
                    if (docSeqStr != null) {
                        sb.append(
                "select file_name,file_content_type,file_contents,dbms_lob.getlength(file_contents) from ART.rpt_docs where docseq=?");
                        
                       // sb.append(docSeqStr);
                        sql= sb.toString();   
                        docKey = new Long(docSeqStr);
                         stmt = connection.prepareStatement(sql);
                        stmt.setLong(1,docKey.longValue() );
                    }
                    if (abacusdocSeqStr != null) {
                        sb.append("select org_file_name,null,file_contents,dbms_lob.getlength(file_contents) from abacus_docs where doc_id=?");
                        sql= sb.toString();   
                        docKey = new Long(abacusdocSeqStr);
                        stmt = connection.prepareStatement(sql);
                        stmt.setLong(1,docKey.longValue() );
                        //System.out.println("Executed SQL: " + sql);
                    }

                    if(stmt!= null){
                        ResultSet resultSet = stmt.executeQuery();
                        //System.out.println("Fetched Data: " + resultSet.getFetchSize() + " "+resultSet.toString() );
                        while (resultSet.next()) {
                        
                            String fileName = resultSet.getString(1);
                            //inserting the code for asist document name convention//
                                            String origFileName = fileName.substring(0, fileName.lastIndexOf('.'));
                                            String extnFileName = fileName.substring(fileName.lastIndexOf('.'));
                                            String newFileName  = origFileName.replaceAll("[^a-zA-Z0-9]", "_");
                                            fileName = newFileName+extnFileName;
                                            //end//

                            String fileContentType = resultSet.getString(2);
                            InputStream iStream = resultSet.getBinaryStream(3);
                            int blobLength = resultSet.getInt(4);
                           // System.out.println("Fetched File name: " + fileName);
                            if (fileContentType == null) {
                                fileContentType = "application/octet-stream";
                            }
                            response.setContentType(fileContentType);
                            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                            response.setContentLength(blobLength);
                            byte[] byteBuffer = new byte[BUFSIZE];
                            while ((iStream != null) && ((length = iStream.read(byteBuffer)) != -1)) {
                                outStream.write(byteBuffer, 0, length);
                            }
                            iStream.close();
                            outStream.close();
                           // System.out.println("Completed File Download " + fileName);
                        }    
                    }
                    
                }                
            }else{
                //System.out.println("JDBC connection missing");
            }
            
        } catch (IOException ie) {
            ie.printStackTrace();
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            try {
                if (null != connection) {
                    connection.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

    }

}
