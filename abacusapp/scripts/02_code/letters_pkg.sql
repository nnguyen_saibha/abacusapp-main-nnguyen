--------------------------------------------------------
--  File created - Tuesday-August-04-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package LETTERS_PKG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "APP"."LETTERS_PKG" 
IS                                                                     -- spec
    pa_rec               procurement_action%ROWTYPE := NULL;
    sga_rec              pa_subgrant_letter%ROWTYPE := NULL;
    ta_rec               pa_ta_approval%ROWTYPE := NULL;
    awardee_rec          awardee_lookup%ROWTYPE := NULL;
    awardee_add_rec      awardee_address%ROWTYPE := NULL;
    contact_rec          awardee_contacts%ROWTYPE := NULL;
    funding_action_rec   funding_action_lookup%ROWTYPE := NULL;
    abacus_user_rec      abacus_user%ROWTYPE := NULL;
    award_rec            award%ROWTYPE := NULL;
    error_msg            VARCHAR2 (1000) := ''; --All error messages will be here
    flag                 NUMBER (2) := 0;
    letter_type          VARCHAR2 (20) := '';

    generate_flag        NUMBER (2) := 0;
    generate_err_msg     VARCHAR2 (1000) := '';
    generate_pa_rec      procurement_action%ROWTYPE := NULL;
    pal                  VARCHAR2 (20) := 'PAL';
    pml                  VARCHAR2 (20) := 'PML';
    nce_ack              VARCHAR2 (20) := 'NCK';
    nce_approval         VARCHAR2 (20) := 'NCA';
    nce                  VARCHAR2 (20) := 'NCE';
    sga                  VARCHAR2 (20) := 'SGA';
    ta                   VARCHAR2 (20) := 'TA';
    date_format          VARCHAR2 (20) := 'FMMonth DD, YYYY';
    action_obligated     VARCHAR2 (30) := 'Obligated';

    TYPE procurement_action_x IS RECORD (
        --PAL
        proposal_name         VARCHAR2 (255),                           -- L11
        country_region        VARCHAR2 (60),     -- L11 country or region name
        proposal_rec_date     DATE,                                     -- L12
        pal_start_date        DATE,                                     -- L22
        funding_action_name   VARCHAR2 (60), -- L17,25use lookup to get funding name from code
        pal_amt               NUMBER (15, 2),                -- L23 pal amount
        agreement_officer     VARCHAR2 (60),                            -- L36
        --PML
        award_start_date      DATE,                     --L25 award_start_date
        pml_end_date          DATE,                         --L27 pml_end_date
        pml_req_date          DATE,              --L27 pml_req_date --Apr 2013
        current_award_nbr     VARCHAR2 (30),
        aotr_name             VARCHAR2 (60), --CTO get full name from abacus_manager
        aotr_phone            VARCHAR2 (20),
        award_nbr             VARCHAR2 (30),                       --award_nbr
        --NCE ACK and APPROVAL
        award_end_date        DATE,                           --Award_end_date
        nce_date              DATE,                                 --NCE_DATE
        --SGA Sub Award Approval
        approved_amt          NUMBER (15, 2),     --L20 Detail Approved Amount
        parent_award_nbr      VARCHAR2 (30),                --parent award_nbr
        aotr_email            VARCHAR2 (30)
    );

    TYPE award_x IS RECORD (
        aotr_name        VARCHAR2 (60), --CTO get full name from abacus_manager
        aotr_phone       VARCHAR2 (20),
        award_nbr        VARCHAR2 (30),                            --award_nbr
        --NCE ACK and APPROVAL
        award_end_date   DATE,                                --Award_end_date
        aotr_email       VARCHAR2 (30)
    );

    TYPE awardee_lookup_x IS RECORD (
        organization_name   VARCHAR2 (60),                     -- awardee name
        organization_add    VARCHAR2 (500)                  -- awardee address
    );

    TYPE awardee_contacts_x IS RECORD (
        contact_name   VARCHAR2 (60),                           -- Line 4 Name
        salutation     VARCHAR2 (60)
    );

    TYPE subgrant_app_x IS RECORD (
        letter_date      DATE,
        refa             VARCHAR2 (4000),
        refb             VARCHAR2 (4000),
        purpose          VARCHAR2 (4000),
        details          VARCHAR2 (4000),
        effective_date   DATE
    );

    TYPE ta_app_x IS RECORD (
        letter_date   DATE,
        ref_subject   VARCHAR2 (4000),
        purpose       VARCHAR2 (4000),
        ref_date      DATE
    );

    pa_rec_x             procurement_action_x;
    award_rec_x          award_x;
    awardee_rec_x        awardee_lookup_x;
    awardee_p_rec_x      awardee_lookup_x;
    contact_rec_x        awardee_contacts_x;
    contact_p_rec_x      awardee_contacts_x;
    subgrant_app_rec_x   subgrant_app_x;
    ta_rec_x             ta_app_x;

    PROCEDURE get_pa_rec (p_action_id NUMBER, p_aotr_id VARCHAR2);

    PROCEDURE get_award_rec (p_award_seq NUMBER, p_aotr_id VARCHAR2);

    PROCEDURE get_awardee_rec (awardeecode          NUMBER,
                               p_awardee_add_seq    NUMBER,
                               flagp                NUMBER);

    PROCEDURE get_contact_rec (p_awardee_contact_seq    NUMBER,
                               flagp                    NUMBER,
                               p_awardee_code           NUMBER);

    PROCEDURE get_funding_action_rec (funding_action_type VARCHAR2);

    PROCEDURE get_abacus_user_rec (p_aotr_id VARCHAR2);

    PROCEDURE get_awardee_add_rec (p_awardee_code       NUMBER,
                                   p_awardee_add_seq    NUMBER);

    PROCEDURE get_subgrant_app_rec (p_action_id NUMBER);

    --  procedure get_ta_app_rec          (p_action_id            number);
    PROCEDURE get_ta_app_rec (p_ta_approval_seq NUMBER);


    -------------- Changed the return types to clob on 12/29/2010 by Deepthi
    FUNCTION letter_pal (p_action_id              NUMBER,
                         p_awardee_contact_seq    NUMBER,
                         p_awardee_add_seq        NUMBER,
                         p_aotr_id                VARCHAR2)
        RETURN CLOB;

    FUNCTION letter_pml (p_action_id              NUMBER,
                         p_awardee_contact_seq    NUMBER,
                         p_awardee_add_seq        NUMBER,
                         p_aotr_id                VARCHAR2)
        RETURN CLOB;

    FUNCTION letter_nce_ack (p_action_id              NUMBER,
                             p_awardee_contact_seq    NUMBER,
                             p_awardee_add_seq        NUMBER,
                             p_aotr_id                VARCHAR2)
        RETURN CLOB;

    FUNCTION letter_nce_approval (p_action_id              NUMBER,
                                  p_awardee_contact_seq    NUMBER,
                                  p_awardee_add_seq        NUMBER,
                                  p_aotr_id                VARCHAR2)
        RETURN CLOB;

    FUNCTION letter_subgrant_approval (p_action_id              NUMBER,
                                       p_awardee_contact_seq    NUMBER,
                                       p_awardee_add_seq        NUMBER,
                                       p_aotr_id                VARCHAR2)
        RETURN CLOB;

    ---  function letter_ta_approval       (p_action_id number,p_awardee_contact_seq number,p_awardee_add_seq number, p_aotr_id varchar2,  p_ta_approval_seq number)                         return clob;
    FUNCTION letter_ta_approval (p_award_seq              NUMBER,
                                 p_awardee_contact_seq    NUMBER,
                                 p_awardee_add_seq        NUMBER,
                                 p_aotr_id                VARCHAR2,
                                 p_ta_approval_seq        NUMBER)
        RETURN CLOB;

    FUNCTION generate_letter (p_action_id              NUMBER,
                              p_awardee_contact_seq    NUMBER,
                              p_awardee_add_seq        NUMBER,
                              p_aotr_id                VARCHAR2,
                              p_letter_type            VARCHAR2,
                              p_ta_approval_seq        NUMBER,
                              p_award_seq              NUMBER)
        RETURN CLOB;

    --------------

    FUNCTION get_ina_type (p_action_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION get_activity_type (p_action_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION getvalidlettertypes (p_action_id NUMBER, p_award_seq NUMBER)
        RETURN lettertable
        PIPELINED;

    FUNCTION getapplicablelettertypes (p_id_type VARCHAR2, p_id NUMBER)
        RETURN lettertable_adf
        PIPELINED;

    FUNCTION isvalid (p_action_id      NUMBER,
                      p_letter_type    VARCHAR2,
                      p_award_seq      NUMBER)
        RETURN VARCHAR2;

    FUNCTION getcurrentawardnbr (p_mod_action_id NUMBER)
        RETURN procurement_action.current_award_nbr%TYPE;

    FUNCTION getdetailaction (p_action_id NUMBER)
        RETURN procurement_action%ROWTYPE;

    --  function getTAApprovalDetails(p_action_id number)return varchar2;
    FUNCTION gettaapprovaldetails (p_ta_approval_seq NUMBER)
        RETURN VARCHAR2;

    FUNCTION isaotrinmodaction (
        p_aotr_id    VARCHAR2,
        modid        procurement_action.mod_action_id%TYPE)
        RETURN VARCHAR2;

    FUNCTION get_aor_names (
        p_action_id     IN procurement_action.action_id%TYPE,
        p_is_proposed      NUMBER)
        RETURN aor_table;

    FUNCTION getapprovalletterstatus (p_letter_memo_id NUMBER)
        RETURN VARCHAR2;
END;

/
--------------------------------------------------------
--  DDL for Package Body LETTERS_PKG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "APP"."LETTERS_PKG" 
IS                                                                     -- body
    FUNCTION getcurrentawardnbr (p_mod_action_id NUMBER)
        RETURN procurement_action.current_award_nbr%TYPE
    IS
        lmaxcurrentnbr   procurement_action.current_award_nbr%TYPE := NULL;
    BEGIN
        lmaxcurrentnbr := NULL;

        SELECT   MAX (award_nbr)
          INTO   lmaxcurrentnbr
          FROM   procurement_action
         WHERE   NVL (mod_action_id, action_id) = p_mod_action_id
                 AND commit_flag = 'Y';

        RETURN lmaxcurrentnbr;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            error_msg :=
                   error_msg
                || '<b>Current Award Number:</b> Award Nbr cannot be null, '
                || lmaxcurrentnbr
                || CHR (10);
            flag := 1;
            RETURN NULL;
    END;

    FUNCTION getdetailaction (p_action_id NUMBER)
        RETURN procurement_action%ROWTYPE
    IS
        detailpa   procurement_action%ROWTYPE := NULL;
    BEGIN
        detailpa := NULL;

        SELECT   *
          INTO   detailpa
          FROM   procurement_action
         WHERE   action_id = p_action_id;

        IF (detailpa.parent_action_id IS NULL)
        THEN
            error_msg :=
                   error_msg
                || '<b>Parent Action Id:</b> Parent Id cannot be null, '
                || detailpa.parent_action_id
                || CHR (10);
            flag := 1;
        END IF;

        IF (detailpa.approved_amt IS NULL)
        THEN
            error_msg :=
                   error_msg
                || '<b>Approved Amount:</b> Amount cannot be null, '
                || detailpa.approved_amt
                || CHR (10);
            flag := 1;
        END IF;

        IF (detailpa.awardee_code IS NULL)
        THEN
            error_msg :=
                   error_msg
                || '<b>Awardee Code:</b> Awardee cannot be null, '
                || detailpa.awardee_code
                || CHR (10);
            flag := 1;
        END IF;

        RETURN detailpa;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            error_msg :=
                   error_msg
                || '<b>Detail Action ID:</b> Action cannot be null, '
                || p_action_id
                || CHR (10);
            flag := 1;
            RETURN NULL;
    END;

    FUNCTION isaotrinmodaction (
        p_aotr_id    VARCHAR2,
        modid        procurement_action.mod_action_id%TYPE)
        RETURN VARCHAR2
    IS
        mod_action   procurement_action%ROWTYPE := NULL;
        aor_valid    VARCHAR2 (1);
    BEGIN
        /**  select *
              into mod_action
              from procurement_action
              where action_id=modid;
    select  decode(instr(listagg(cto) within group (order by cto)||','||listagg(alternate_cto) within group (order by alternate_cto),p_aotr_id),0,'N','Y') Valid
    into    aor_valid
    from    procurement_action
    where   nvl(mod_action_id,action_id) = modid
    and (cto = p_aotr_id or alternate_cto= p_aotr_id)
    group by nvl(mod_action_id,action_id); */

        SELECT   CASE WHEN COUNT (1) > 0 THEN 'Y' ELSE 'N' END
          INTO   aor_valid
          FROM   procurement_action
         WHERE   NVL (mod_action_id, action_id) = modid
                 AND (cto = p_aotr_id OR alternate_cto = p_aotr_id);


        --if(p_aotr_id in (mod_action.alternate_cto,mod_action.cto))then
        IF (aor_valid = 'Y')
        THEN
            RETURN 'Y';
        ELSE
            error_msg :=
                   error_msg
                || '<b>AOR:</b> Invalid AOR,'
                || p_aotr_id
                || '. Action has AOR='
                || mod_action.cto
                || ' and Alternate AOR='
                || mod_action.alternate_cto
                || '.'
                || CHR (10);
            flag := 1;
            RETURN 'N';
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            error_msg :=
                   error_msg
                || '</b>Action Id:</b> no data found in Action, '
                || modid
                || CHR (10);
            flag := 1;
            NULL;
    END;

    FUNCTION get_ina_type (p_action_id NUMBER)
        RETURN VARCHAR2
    IS
        is_ina     NUMBER;
        ina_type   VARCHAR2 (15) := NULL;
    BEGIN
        SELECT   COUNT ( * )
          INTO   is_ina
          FROM   pa_ina
         WHERE   action_id = p_action_id;

        letters_pkg.get_pa_rec (p_action_id, NULL);

        IF is_ina = 0
        THEN
            IF pa_pkg.get_action_status (p_action_id) != action_obligated
            THEN
                ina_type := 'Create INA';
            END IF;
        ELSE
            IF (pa_pkg.get_action_status (p_action_id) = action_obligated)
            THEN
                ina_type := 'View INA';
            ELSE
                ina_type := 'Edit INA';
            END IF;
        END IF;

        RETURN ina_type;
    END;

    FUNCTION get_ina_type1 (p_action_id NUMBER)
        RETURN VARCHAR2
    IS
        is_ina            NUMBER;
        l_budgetfy        procurement_action.budget_fy%TYPE := 0;
        l_iscurrent       NUMBER := 0;
        c_iscurrent_yes   CONSTANT NUMBER := 1;
        c_iscurrent_no    CONSTANT NUMBER := 2;
        ina_type          VARCHAR2 (15) := NULL;
    BEGIN
        BEGIN
            SELECT   COUNT (pi.action_id) OVER (PARTITION BY pa.budget_fy),
                     pa.budget_fy,
                     CASE
                         WHEN get_fy () >= pa.budget_fy THEN c_iscurrent_yes
                         ELSE c_iscurrent_no
                     END
                         AS iscurrent
              INTO   is_ina, l_budgetfy, l_iscurrent
              FROM       procurement_action pa
                     LEFT OUTER JOIN
                         pa_ina pi
                     ON (pa.action_id = pi.action_id)
             WHERE   pa.action_id = p_action_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                is_ina := 0;
                l_iscurrent := c_iscurrent_no;
                l_budgetfy := 0;
        END;

        letters_pkg.get_pa_rec (p_action_id, NULL);

        IF l_iscurrent = c_iscurrent_yes
        THEN
            --Case 1: Action budget_fy = Current Fiscal year and action status is obligated
            IF pa_pkg.get_action_status (p_action_id) = action_obligated
               AND is_ina > 0
            THEN
                ina_type := 'View INA';
            ELSE
                --Case 2: Action budget_fy = Current Fiscal year and no existing INA
                IF is_ina = 0
                THEN
                    ina_type := 'Create INA';
                --Case 3: Action budget_fy = Current Fiscal year, existing INA and action status is not obligated
                ELSE
                    ina_type := 'Edit INA';
                END IF;
            END IF;
        ELSIF l_iscurrent = c_iscurrent_no
        THEN
            --Case 4: Action budget_fy <> Current Fiscal year
            ina_type := 'View INA';
        END IF;

        RETURN ina_type;
    END;

    FUNCTION get_activity_type (p_action_id NUMBER)
        RETURN VARCHAR2
    IS
        is_activity     NUMBER;
        activity_type   VARCHAR2 (30) := NULL;
    BEGIN
        SELECT   COUNT ( * )
          INTO   is_activity
          FROM   pa_activity
         WHERE   action_id = p_action_id;

        letters_pkg.get_pa_rec (p_action_id, NULL);

        IF is_activity = 0
        THEN
            -- if pa_pkg.get_action_status(p_action_id)!=action_obligated then
            activity_type := 'Create Activity Checklist';
        --end if;
        ELSE
            -- if(pa_pkg.get_action_status(p_action_id)=action_obligated)then
            --   activity_type := 'View Checklist';
            --else
            activity_type := 'Edit Activity Checklist';
        --end if;
        END IF;

        RETURN activity_type;
    END;

    FUNCTION getvalidlettertypes (p_action_id NUMBER, p_award_seq NUMBER)
        RETURN lettertable
        PIPELINED
    AS
        ina_type        VARCHAR2 (15) := NULL;
        activity_type   VARCHAR2 (25) := NULL;
    BEGIN
        ina_type := NULL;
        ina_type := get_ina_type (p_action_id);
        activity_type := get_activity_type (p_action_id);

        FOR cur
        IN (SELECT   pa.action_id,
                     pa.action_type_code,
                     'PAL' letter_label,
                     'PAL' letter_type,
                     pa.award_end_date award_end_date,
                     NULL nce_end_date,
                     NULL days,
                     NULL months
              FROM   procurement_action pa,
                     letter_memo lm,
                     pa_letter_memo plm
             WHERE       pa.funding_action_type IN ('01', '03')
                     AND pa.commit_flag = 'Y'
                     AND pa.action_type_code IN ('N', 'D')
                     --and pa_pkg.get_action_status(p_action_id)!=action_obligated
                     --and pa.budget_fy >= get_fy()
                     AND pa.is_checkbook <> 'N'
                     AND pa.action_id = p_action_id
                     AND pa.action_id = plm.action_id ---added for new letters screen Feb 2016
                     AND plm.letter_memo_id = lm.letter_memo_id
                     AND lm.letter_memo_type_code = 'PAL'
            UNION
            SELECT   pa.action_id,
                     pa.action_type_code,
                     'PML',
                     'PML',
                     pa.award_end_date,
                     NULL,
                     NULL,
                     NULL
              FROM   procurement_action pa,
                     pa_letter_memo plm,
                     letter_memo lm
             WHERE       pa.funding_action_type IN ('01', '03')
                     AND pa.commit_flag = 'Y'
                     AND pa.action_type_code IN ('M', 'Z')
                     -- and nvl(ph_distributed_amt,0) > 0 --Removed as as of Apr 2016 PMl valid for planned, approved actions too
                     AND pa.is_checkbook <> 'N'
                     AND pa.action_id = p_action_id
                     AND pa_pkg.get_action_status (pa.mod_action_id) =
                            action_obligated
                     AND pa.action_id = plm.action_id ---added for new letters screen Feb 2016
                     AND plm.letter_memo_id = lm.letter_memo_id
                     AND lm.letter_memo_type_code = 'PML'
            -- and pa_pkg.get_action_status(p_action_id)!=action_obligated
            -- and pa.budget_fy >= get_fy()
            UNION
            SELECT   pa.action_id,
                     pa.action_type_code,
                     ina_type,
                     'INA',
                     pa.award_end_date,
                     NULL,
                     NULL,
                     NULL
              FROM               procurement_action pa
                             LEFT OUTER JOIN
                                 pa_ina paina
                             ON (pa.action_id = paina.action_id)
                         INNER JOIN
                             pa_letter_memo plm
                         ON (pa.action_id = plm.action_id)
                     INNER JOIN
                         letter_memo lm
                     ON (plm.letter_memo_id = lm.letter_memo_id)
             WHERE       pa.commit_flag = 'Y'
                     AND pa.is_checkbook <> 'N'
                     AND pa.funding_action_type IN ('01', '03')
                     AND pa.action_id = p_action_id
                     AND pa.action_type_code <> 'L'
                     AND ina_type IS NOT NULL
                     AND lm.letter_memo_type_code = 'INA'
                     AND pa.budget_fy > 2020 -- INA will be disabled for OFDA/FFP actions
            --- and (pa.budget_fy >= get_fy() or paina.action_id is not null)
            UNION
            SELECT   pa.action_id,
                     pa.action_type_code,
                     activity_type,
                     'ACL',
                     pa.award_end_date,
                     NULL,
                     NULL,
                     NULL
              FROM               procurement_action pa
                             LEFT OUTER JOIN
                                 pa_activity paa
                             ON (pa.action_id = paa.action_id)
                         INNER JOIN
                             pa_letter_memo plm
                         ON (pa.action_id = plm.action_id)
                     INNER JOIN
                         letter_memo lm
                     ON (plm.letter_memo_id = lm.letter_memo_id)
             WHERE       pa.commit_flag = 'Y'
                     AND pa.is_checkbook <> 'N'
                     AND pa.funding_action_type IN ('01', '03', '21')
                     AND pa.action_id = p_action_id
                     AND activity_type IS NOT NULL
                     AND pa.action_type_code <> 'L'
                     AND pa_pkg.get_action_type (pa.action_type_code) NOT IN
                                ('Unfunded Mod')
                     -- Not for unfunded Action Type
                     AND lm.letter_memo_type_code = 'ACL'
            --    and (pa.budget_fy >= get_fy() or paa.action_id is not null)

            UNION
            SELECT   pa.action_id,
                     pa.action_type_code,
                     CASE
                         WHEN (MONTHS_BETWEEN (pax.nce_date,
                                               pa.award_end_date) > 0
                               AND (MONTHS_BETWEEN (pax.nce_date,
                                                    pa.award_end_date)) <= 3)
                         THEN
                             'NCE Acknowledgement Letter'
                         WHEN (MONTHS_BETWEEN (pax.nce_date,
                                               pa.award_end_date)) > 3
                              AND (MONTHS_BETWEEN (pax.nce_date,
                                                   pa.award_end_date) <= 12)
                         THEN
                             'NCE Approval Letter'
                     END
                         CASE,
                     'NCE',
                     pa.award_end_date,
                     pax.nce_date,
                     pax.nce_date - pa.award_end_date,
                     TRUNC (MONTHS_BETWEEN (pax.nce_date, pa.award_end_date),
                            2)
              FROM   procurement_action pa,
                     pa_x pax,
                     pa_letter_memo plm,
                     letter_memo lm
             WHERE       pa.funding_action_type IN ('01', '03')
                     AND pa.commit_flag = 'Y'
                     AND pa.is_checkbook <> 'N'
                     AND pa.action_id = pax.action_id
                     AND pax.nce_date IS NOT NULL
                     AND pa.action_id = p_action_id
                     AND MONTHS_BETWEEN (pax.nce_date, pa.award_end_date) > 0
                     AND MONTHS_BETWEEN (pax.nce_date, pa.award_end_date) <=
                            12
                     AND pa_pkg.get_action_status (pa.action_id) =
                            action_obligated
                     AND pa.action_id = plm.action_id ---added for new letters screen Feb 2016
                     AND plm.letter_memo_id = lm.letter_memo_id
                     and pa.budget_fy < 2020 ---NCE not applicable for FY 2020 and forward Jan 2020
                     AND lm.letter_memo_type_code = 'NCE'
            UNION
            --  select pa.action_id, pa.action_type_code,
            SELECT   a.award_id,
                     pa.action_type_code,
                     'International Travel Consent',
                     'TA',
                     NULL,
                     NULL,
                     NULL,
                     NULL
              FROM   procurement_action pa,
                     award a,
                     award_letter_memo alm,
                     letter_memo lm
             WHERE       pa.is_checkbook <> 'N'
                     --- and pa.funding_action_type in ('01', '03')
                     --and pa_pkg.get_action_status(nvl(pa.mod_action_id,pa.action_id))=action_obligated
                     AND pa.action_id = a.award_id
                     --and nvl(pan.award_end_date,pa.award_end_date)>=sysdate
                     --and pa.action_type_code in ('D','Z')
                     AND pa.commit_flag = 'Y'
                     AND a.award_seq = p_award_seq              ---p_action_id
                     AND a.active = 'Y'
                     AND a.award_seq = alm.award_seq ---added for new letters screen Feb 2016
                     AND alm.letter_memo_id = lm.letter_memo_id
                     AND lm.letter_memo_type_code = 'TA'
            --and pa.budget_fy >= get_fy()
            --- and pa_pkg.HAS_AWARD_ENDED(action_id)='N'
            UNION
            SELECT   pa.action_id,
                     pa.action_type_code,
                     'Sub Award Approval',
                     'SGA',
                     NULL,
                     NULL,
                     NULL,
                     NULL
              FROM   procurement_action pa,
                     procurement_action pa_parent,
                     letter_memo lm,
                     pa_letter_memo plm
             WHERE       pa.action_id != pa.parent_action_id
                     AND pa.parent_action_id IS NOT NULL
                     AND pa.action_type_code IN ('D', 'Z')
                     AND pa.commit_flag = 'N'
                     AND pa.is_checkbook <> 'N'
                     AND pa.parent_action_id = pa_parent.action_id
                     AND pa_parent.funding_action_type IN ('01', '03')
                     AND pa.funding_action_type IN ('01', '03')
                     AND pa.action_id = p_action_id
                     AND pa_pkg.get_action_status (pa_parent.action_id) =
                            action_obligated
                     AND pa.action_id = plm.action_id ---added for new letters screen Feb 2016
                     AND plm.letter_memo_id = lm.letter_memo_id
                     AND lm.letter_memo_type_code = 'SGA'
            --and pa_pkg.HAS_AWARD_ENDED(pa_parent.action_id)='N'

            ORDER BY   1, 3)
        LOOP
            PIPE ROW (lettertype (cur.action_id,
                                  cur.action_type_code,
                                  cur.letter_label,
                                  cur.letter_type,
                                  cur.award_end_date,
                                  cur.nce_end_date,
                                  cur.days,
                                  cur.months));
        END LOOP;

        RETURN;
    END;

    FUNCTION getapplicablelettertypes (p_id_type VARCHAR2, p_id NUMBER)
        RETURN lettertable_adf
        PIPELINED
    AS
        p_action_id   NUMBER := 0;
        p_award_seq   NUMBER := 0;
    BEGIN
        IF (p_id_type = 'ACTION')
        THEN
            p_action_id := p_id;

            BEGIN
                SELECT   award_seq
                  INTO   p_award_seq
                  FROM   award
                 WHERE   award_id =
                             (SELECT   NVL (procurement_action.mod_action_id,
                                            procurement_action.action_id)
                                FROM   procurement_action
                               WHERE   action_id = p_id);
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    p_award_seq := 0;
            END;
        ELSIF (p_id_type = 'AWARD')
        THEN
            p_award_seq := p_id;
        END IF;

        FOR cur
        IN (SELECT   pa.action_id "LINK_ID",
                     'ACTION' "LETTER_MEMO_GROUP_CODE",
                     'PAL' "LETTER_MEMO_TYPE_CODE",
                     CASE
                         WHEN (pax.action_amt_status IN (1, 2, 3, 4)
                               AND pa.budget_fy >= get_fy ())
                         THEN
                             'Y'
                         ELSE
                             'N'
                     END
                         "IS_VALID",
                     CASE
                         WHEN (pa.budget_fy >= get_fy ()
                               AND (pax.action_amt_status IN (1, 2, 3, 4)
                                    OR palm.letter_memo_id IS NOT NULL))
                         THEN
                             'Y'
                         ELSE
                             'N'
                     END
                         "IS_EDITABLE",
                     CASE
                         WHEN (    pax.action_amt_status IN (1, 2, 3, 4)
                               AND pa.budget_fy >= get_fy ()
                               AND palm.letter_memo_id IS NULL)
                         THEN
                             'Create'
                         WHEN (pa.budget_fy >= get_fy ()
                               AND palm.letter_memo_id IS NOT NULL)
                         THEN
                             'Edit'
                         WHEN palm.letter_memo_id IS NOT NULL
                         THEN
                             'View'
                         ELSE
                             'NA'
                     END
                         "LETTER_STATUS",
                     CASE
                         WHEN (    pax.action_amt_status IN (1, 2, 3, 4)
                               AND pa.budget_fy >= get_fy ()
                               AND palm.letter_memo_id IS NULL)
                         THEN
                             'PAL can be created for an action that is technically approved before obligation for signature by the Agreement Officer'
                         WHEN (pa.budget_fy >= get_fy ()
                               AND palm.letter_memo_id IS NOT NULL)
                         THEN
                             'Existing PAL can be edited up until obligation'
                         WHEN palm.letter_memo_id IS NOT NULL
                         THEN
                             'Existing PAL can be viewed when action is obligated'
                         ELSE
                             'PAL cannot be created or edited for obligated actions'
                     END
                         "DESCRIPTION",
                     TO_CHAR (pal_sent_date, 'MM/dd/yyyy') "DESCRIPTION2",
                     NULL "DESCRIPTION3"
              FROM           procurement_action pa
                         JOIN
                             pa_x pax
                         ON (pa.action_id = pax.action_id)
                     LEFT OUTER JOIN
                         (SELECT   action_id, palm.letter_memo_id
                            FROM       pa_letter_memo palm
                                   JOIN
                                       letter_memo lm
                                   ON (palm.letter_memo_id =
                                           lm.letter_memo_id
                                       AND lm.letter_memo_type_code = 'PAL'))
                         palm
                     ON (pa.action_id = palm.action_id)
             WHERE       pa.funding_action_type IN ('01', '03')
                     AND pa.commit_flag = 'Y'
                     AND pa.action_type_code IN ('N', 'D')
                     AND pa.is_checkbook <> 'N'
                     AND p_id_type = 'ACTION'
                     AND pa.action_id = p_action_id                    --40921
            UNION
            SELECT   pa.action_id "LINK_ID",
                     'ACTION' "LETTER_MEMO_GROUP_CODE",
                     'PML' "LETTER_MEMO_TYPE_CODE",
                     CASE
                         WHEN (    pax.action_amt_status IN (1, 2, 3, 4)
                               AND mod_pax.action_amt_status = 5
                               AND pa.budget_fy >= get_fy ())
                         THEN
                             'Y'
                         ELSE
                             'N'
                     END
                         "IS_VALID",
                     CASE
                         WHEN (mod_pax.action_amt_status = 5
                               AND pa.budget_fy >= get_fy ()
                               AND (pax.action_amt_status IN (1, 2, 3, 4)
                                    OR palm.letter_memo_id IS NOT NULL))
                         THEN
                             'Y'
                         ELSE
                             'N'
                     END,
                     CASE
                         WHEN (    pax.action_amt_status IN (1, 2, 3, 4)
                               AND mod_pax.action_amt_status = 5
                               AND pa.budget_fy >= get_fy ()
                               AND palm.letter_memo_id IS NULL)
                         THEN
                             'Create'
                         WHEN (    mod_pax.action_amt_status = 5
                               AND pa.budget_fy >= get_fy ()
                               AND palm.letter_memo_id IS NOT NULL)
                         THEN
                             'Edit'
                         WHEN palm.letter_memo_id IS NOT NULL
                         THEN
                             'View'
                         ELSE
                             'NA'
                     END
                         "LETTER_STATUS",
                     CASE
                         WHEN (    pax.action_amt_status IN (1, 2, 3, 4)
                               AND mod_pax.action_amt_status = 5
                               AND pa.budget_fy >= get_fy ()
                               AND palm.letter_memo_id IS NULL)
                         THEN
                             'PML can be created for a modification action that is technically approved before obligation for signature by the currently designated AOR for the award.  Original action must be obligated'
                         WHEN (    mod_pax.action_amt_status = 5
                               AND pa.budget_fy >= get_fy ()
                               AND palm.letter_memo_id IS NOT NULL)
                         THEN
                             'Existing PML can be edited up until modification action is obligated'
                         WHEN palm.letter_memo_id IS NOT NULL
                         THEN
                             'Existing PML can be viewed when action is obligated'
                         ELSE
                             'PML cannot be created or edited for obligated actions'
                     END
                         "DESCRIPTION",
                     TO_CHAR (pa.pml_sent_date, 'MM/dd/yyyy'),
                     NULL
              FROM               procurement_action pa
                             JOIN
                                 pa_x pax
                             ON (pa.action_id = pax.action_id)
                         JOIN
                             pa_x mod_pax
                         ON (pa.mod_action_id = mod_pax.action_id)
                     LEFT OUTER JOIN
                         (SELECT   action_id, palm.letter_memo_id
                            FROM       pa_letter_memo palm
                                   JOIN
                                       letter_memo lm
                                   ON (palm.letter_memo_id =
                                           lm.letter_memo_id
                                       AND lm.letter_memo_type_code = 'PML'))
                         palm
                     ON (pa.action_id = palm.action_id)
             WHERE       pa.funding_action_type IN ('01', '03')
                     AND pa.commit_flag = 'Y'
                     AND pa.action_type_code IN ('M', 'Z')
                     AND pa.is_checkbook <> 'N'
                     AND p_id_type = 'ACTION'
                     AND pa.action_id = p_action_id                    --40929
            UNION
            SELECT   pa.action_id "LINK_ID",
                     'ACTION' "LETTER_MEMO_GROUP_CODE",
                     'INA' "LETTER_MEMO_TYPE_CODE",
                     CASE
                         WHEN (pax.action_amt_status IN (1, 2, 3, 4)
                               AND pa.budget_fy >= get_fy() and  pa.budget_fy > 2020) -- INA will be disabled for OFDA/FFP actions
                         THEN
                             'Y'
                         ELSE
                             'N'
                     END
                         "IS_VALID",
                     CASE
                         WHEN (pa.budget_fy >= get_fy () and  pa.budget_fy > 2020
                               AND (pax.action_amt_status IN (1, 2, 3, 4)
                                    OR palm.letter_memo_id IS NOT NULL))
                         THEN
                             'Y'
                         ELSE
                             'N'
                     END
                         "IS_EDITABLE",
                     CASE
                         WHEN (    pax.action_amt_status IN (1, 2, 3, 4)
                               AND pa.budget_fy >= get_fy () and  pa.budget_fy > 2020
                               AND palm.letter_memo_id IS NULL)
                         THEN
                             'Create'
                         WHEN (pa.budget_fy >= get_fy () and  pa.budget_fy > 2020
                               AND palm.letter_memo_id IS NOT NULL)
                         THEN
                             'Edit'
                         WHEN palm.letter_memo_id IS NOT NULL and  pa.budget_fy > 2020
                         THEN
                             'View'
                         ELSE
                             'NA'
                     END
                         "LETTER_STATUS",
                     CASE
                         WHEN (    pax.action_amt_status IN (1, 2, 3, 4)
                               AND pa.budget_fy >= get_fy () and  pa.budget_fy > 2020
                               AND palm.letter_memo_id IS NULL)
                         THEN
                             'INA can be created for an action any time up to obligation'
                         WHEN (pa.budget_fy >= get_fy () and  pa.budget_fy > 2020
                               AND palm.letter_memo_id IS NOT NULL)
                         THEN
                             'Existing INA can be edited up until obligation'
                         WHEN palm.letter_memo_id IS NOT NULL and  pa.budget_fy > 2020
                         THEN
                             'Existing INA cannot be modified for Obligated Actions or Previous fiscal year'
                         WHEN  pa.budget_fy <= 2020
                         THEN
                             'INA cannot be created or edited for actions before 2021.'
                         ELSE
                             'INA cannot be created or edited for obligated actions.'
                     END
                         "DESCRIPTION",
                     NULL,
                     NULL
              FROM           procurement_action pa
                         JOIN
                             pa_x pax
                         ON (pa.action_id = pax.action_id)
                     LEFT OUTER JOIN
                         (SELECT   action_id, palm.letter_memo_id
                            FROM       pa_letter_memo palm
                                   JOIN
                                       letter_memo lm
                                   ON (palm.letter_memo_id =
                                           lm.letter_memo_id
                                       AND lm.letter_memo_type_code = 'INA'))
                         palm
                     ON (pa.action_id = palm.action_id)
             WHERE       pa.funding_action_type IN ('01', '03')
                     AND pa.commit_flag = 'Y'
                     AND pa.action_type_code <> 'L'
                     AND pa.is_checkbook <> 'N'
                     AND p_id_type = 'ACTION'
                     AND pa.action_id = p_action_id                    --40921
            UNION
            SELECT   pa.action_id "LINK_ID",
                     'ACTION' "LETTER_MEMO_GROUP_CODE",
                     'ACL' "LETTER_MEMO_TYPE_CODE",
                     CASE
                         WHEN (pa.budget_fy >= get_fy ()) THEN 'Y'
                         ELSE 'N'
                     END
                         "IS_VALID",
                     CASE
                         WHEN (pa.budget_fy >= get_fy ()) THEN 'Y'
                         ELSE 'N'
                     END,
                     CASE
                         WHEN (pa.budget_fy >= get_fy ()
                               AND palm.letter_memo_id IS NULL)
                         THEN
                             'Create'
                         WHEN (pa.budget_fy >= get_fy ()
                               AND palm.letter_memo_id IS NOT NULL)
                         THEN
                             'Edit'
                         WHEN palm.letter_memo_id IS NOT NULL
                         THEN
                             'View'
                         ELSE
                             'NA'
                     END
                         "LETTER_STATUS",
                     CASE
                         WHEN (pa.budget_fy >= get_fy ()
                               AND palm.letter_memo_id IS NULL)
                         THEN
                             'Activity Checklist can be created for action any time up to obligation'
                         WHEN (pa.budget_fy >= get_fy ()
                               AND palm.letter_memo_id IS NOT NULL)
                         THEN
                             'Existing Activity Checklist can be edited up until obligation'
                         WHEN palm.letter_memo_id IS NOT NULL
                         THEN
                             'Existing Activity Checklist cannot be modified for Obligated Actions or Previous fiscal year'
                         ELSE
                             'Activity Checklists cannot be created for obligated actions'
                     END
                         "DESCRIPTION",
                     NULL,
                     NULL
              FROM           procurement_action pa
                         JOIN
                             pa_x pax
                         ON (pa.action_id = pax.action_id)
                     LEFT OUTER JOIN
                         (SELECT   action_id, palm.letter_memo_id
                            FROM       pa_letter_memo palm
                                   JOIN
                                       letter_memo lm
                                   ON (palm.letter_memo_id =
                                           lm.letter_memo_id
                                       AND lm.letter_memo_type_code = 'ACL'))
                         palm
                     ON (pa.action_id = palm.action_id)
             WHERE       pa.funding_action_type IN ('01', '03', '21')
                     AND pa.commit_flag = 'Y'
                     AND pa.action_type_code NOT IN ('L', 'U')
                     AND pa.is_checkbook <> 'N'
                     AND p_id_type = 'ACTION'
                     AND pa.action_id = p_action_id                   -- 40921
            UNION
        SELECT   pa.action_id "LINK_ID",
                     'ACTION' "LETTER_MEMO_GROUP_CODE",
                     'NCE' "LETTER_MEMO_TYPE_CODE",
                     CASE
                         WHEN ((pax.action_amt_status = 5) and (palm.letter_memo_id IS NOT NULL) )THEN 'Y'
                         ELSE 'N'
                     END
                         "IS_VALID",
                     'N',
                     CASE
                      --   WHEN (pax.action_amt_status = 5
                        --       AND palm.letter_memo_id IS NULL)
                       --  THEN
                      --       'Create'
                      --   WHEN (pax.action_amt_status = 5
                      --         OR palm.letter_memo_id IS NOT NULL)
                      --   THEN
                      --       'Edit'
                         WHEN palm.letter_memo_id IS NOT NULL
                         THEN
                             'View'
                         ELSE
                             'NA'
                     END
                         "LETTER_STATUS",
                     CASE
                    --     WHEN (pax.action_amt_status = 5
                    --           AND palm.letter_memo_id IS NULL)
                    --     THEN
                    --         'NCE can be created for obligated actions up to the award end date'
                     --    WHEN (pax.action_amt_status = 5
                    ----           OR palm.letter_memo_id IS NOT NULL)
                     --    THEN
                    --         'Existing NCE can be edited at any time up to the award end date'
                         WHEN palm.letter_memo_id IS NOT NULL
                         THEN
                             'View'
                         ELSE
                            'NCEs are now processed via unfunded modification.' --- 'NCE cannot be created until action is obligated'  --Jan 2020
                     END
                         "DESCRIPTION",
                     TO_CHAR (pa.nce_sent_date, 'MM/DD/yyyy'),
                     NULL
              FROM           procurement_action pa
                         JOIN
                             pa_x pax
                         ON (pa.action_id = pax.action_id)
                     LEFT OUTER JOIN
                         (SELECT   action_id, palm.letter_memo_id
                            FROM       pa_letter_memo palm
                                   JOIN
                                       letter_memo lm
                                   ON (palm.letter_memo_id =
                                           lm.letter_memo_id
                                       AND lm.letter_memo_type_code = 'NCE'))
                         palm
                     ON (pa.action_id = palm.action_id)
             WHERE       pa.funding_action_type IN ('01', '03')
                     AND pa.commit_flag = 'Y'
                     AND pa.action_type_code NOT IN ('L')
                     AND pa.is_checkbook <> 'N'
                     AND p_id_type = 'ACTION'
                     and pa.budget_fy < 2020 ---NCE not applicable for FY2020 and forward
                     AND pa.action_id = p_action_id                   -- 40681
            UNION
            SELECT   award.award_seq "LINK_ID",
                     'AWARD' "LETTER_MEMO_GROUP_CODE",
                     'TA' "LETTER_MEMO_TYPE_CODE",
                     CASE
                         WHEN (award.active = 'Y'
                               AND award.award_end_date >= SYSDATE)
                         THEN
                             'Y'
                         ELSE
                             'N'
                     END
                         "IS_VALID",
                     CASE
                         WHEN ( (award.active = 'Y'
                                 AND award.award_end_date >= SYSDATE)
                               OR awardlm.letter_memo_id IS NOT NULL)
                         THEN
                             'Y'
                         ELSE
                             'N'
                     END,
                     CASE
                         WHEN (    award.active = 'Y'
                               AND award.award_end_date >= SYSDATE
                               AND awardlm.letter_memo_id IS NULL)
                         THEN
                             'Create'
                         WHEN ( (award.active = 'Y'
                                 AND award.award_end_date >= SYSDATE)
                               OR awardlm.letter_memo_id IS NOT NULL)
                         THEN
                             'Edit'
                         WHEN awardlm.letter_memo_id IS NOT NULL
                         THEN
                             'View'
                         ELSE
                             'NA'
                     END
                         "LETTER_STATUS",
                     CASE
                         WHEN (    award.active = 'Y'
                               AND award.award_end_date >= SYSDATE
                               AND awardlm.letter_memo_id IS NULL)
                         THEN
                             'ITCs can be created for obligated actions any time up to the award end date'
                         WHEN ( (award.active = 'Y'
                                 AND award.award_end_date >= SYSDATE)
                               OR awardlm.letter_memo_id IS NOT NULL)
                         THEN
                             'Existing ITCs can be edited at any time up to the award end date'
                         WHEN awardlm.letter_memo_id IS NOT NULL
                         THEN
                             'View'
                         ELSE
                             'ITCs cannot be created until action is obligated'
                     END
                         "DESCRIPTION",
                     NULL,
                     NULL
              FROM       award
                     LEFT OUTER JOIN
                         (SELECT   award_seq, awardlm.letter_memo_id
                            FROM       award_letter_memo awardlm
                                   JOIN
                                       letter_memo lm
                                   ON (awardlm.letter_memo_id =
                                           lm.letter_memo_id
                                       AND lm.letter_memo_type_code = 'TA'))
                         awardlm
                     ON (award.award_seq = awardlm.award_seq)
             WHERE       award.funding_action_type IN ('01', '03')
                     AND ROWNUM = 1
                     AND award.award_seq = p_award_seq
                     AND p_id_type IN ('ACTION', 'AWARD')
            UNION
            SELECT   pa.action_id,
                     'AWARD',
                     'TA',
                     'N',
                     'N',
                     'NA',
                     'ITCs can be created for obligated actions any time up to the award end date',
                     NULL,
                     NULL
              FROM   procurement_action pa
             WHERE       pa.funding_action_type IN ('01', '03')
                     AND pa.commit_flag = 'Y'
                     AND pa.is_checkbook <> 'N'
                     AND pa.action_id = p_action_id
                     AND p_award_seq = 0
            UNION
            SELECT   award.award_seq "LINK_ID",
                     'AWARD' "LETTER_MEMO_GROUP_CODE",
                     'APV' "LETTER_MEMO_TYPE_CODE",
                     CASE WHEN (award.active = 'Y') THEN 'Y' ELSE 'N' END
                         "IS_VALID",
                     CASE
                         WHEN ( (award.active = 'Y')
                               OR awardlm.letter_memo_id IS NOT NULL)
                         THEN
                             'Y'
                         ELSE
                             'N'
                     END,
                     CASE
                         WHEN (award.active = 'Y'
                               AND awardlm.letter_memo_id IS NULL)
                         THEN
                             'Create'
                         WHEN ( (award.active = 'Y')
                               OR awardlm.letter_memo_id IS NOT NULL)
                         THEN
                             'Edit'
                         WHEN awardlm.letter_memo_id IS NOT NULL
                         THEN
                             'View'
                         ELSE
                             'NA'
                     END
                         "LETTER_STATUS",
                     CASE
                         WHEN (award.active = 'Y'
                               AND awardlm.letter_memo_id IS NULL)
                         THEN
                             'Approval Letter can be created for an award that has at least one Obligated Action'
                         WHEN ( (award.active = 'Y')
                               OR awardlm.letter_memo_id IS NOT NULL)
                         THEN
                             'Existing Approval Letter can be edited at any time'
                         WHEN awardlm.letter_memo_id IS NOT NULL
                         THEN
                             'View'
                         ELSE
                             'Approval Letter can be created for an award that has at least one Obligated Action'
                     END
                         "DESCRIPTION",
                     NULL,
                     NULL
              FROM       award
                     LEFT OUTER JOIN
                         (SELECT   award_seq, awardlm.letter_memo_id
                            FROM       award_letter_memo awardlm
                                   JOIN
                                       letter_memo lm
                                   ON (awardlm.letter_memo_id =
                                           lm.letter_memo_id
                                       AND lm.letter_memo_type_code = 'APV'))
                         awardlm
                     ON (award.award_seq = awardlm.award_seq)
             WHERE       award.funding_action_type IN ('01', '03')
                     AND ROWNUM = 1
                     AND award.award_seq = p_award_seq
            UNION
            SELECT   pa.action_id,
                     'AWARD',
                     'APV',
                     'N',
                     'N',
                     'NA',
                     'Approval Letter can be created for an award that has at least one Obligated Action',
                     NULL,
                     NULL
              FROM   procurement_action pa
             WHERE       pa.funding_action_type IN ('01', '03')
                     AND pa.commit_flag = 'Y'
                     AND pa.is_checkbook <> 'N'
                     AND pa.action_id = p_action_id
                     AND p_award_seq = 0
            UNION
            SELECT   pa.action_id "LINK_ID",
                     'ACTION' "LETTER_MEMO_GROUP_CODE",
                     'SGA' "LETTER_MEMO_TYPE_CODE",
                     CASE
                         WHEN (parent_pax.action_amt_status = 5) THEN 'Y'
                         ELSE 'N'
                     END
                         "IS_VALID",
                     CASE
                         WHEN (parent_pax.action_amt_status = 5
                               OR palm.letter_memo_id IS NOT NULL)
                         THEN
                             'Y'
                         ELSE
                             'N'
                     END,
                     CASE
                         WHEN (parent_pax.action_amt_status = 5
                               AND palm.letter_memo_id IS NULL)
                         THEN
                             'Create'
                         WHEN (parent_pax.action_amt_status = 5
                               OR palm.letter_memo_id IS NOT NULL)
                         THEN
                             'Edit'
                         WHEN palm.letter_memo_id IS NOT NULL
                         THEN
                             'View'
                         ELSE
                             'NA'
                     END
                         "LETTER_STATUS",
                     CASE
                         WHEN (parent_pax.action_amt_status = 5
                               AND palm.letter_memo_id IS NULL)
                         THEN
                             'SSAs can be created at the detail action level once the parent action is obligated.  SSAs can be created up to the award end date'
                         WHEN (parent_pax.action_amt_status = 5
                               OR palm.letter_memo_id IS NOT NULL)
                         THEN
                             'Existing SAAs can be edited at any time up to the award end date'
                         WHEN palm.letter_memo_id IS NOT NULL
                         THEN
                             'View'
                         ELSE
                             'SAAs cannot be created for detail actions until the parent action is obligated'
                     END
                         "DESCRIPTION",
                     NULL,
                     NULL
              FROM                   procurement_action pa
                                 JOIN
                                     pa_x pax
                                 ON (pa.action_id = pax.action_id)
                             LEFT OUTER JOIN
                                 (SELECT   action_id, palm.letter_memo_id
                                    FROM       pa_letter_memo palm
                                           JOIN
                                               letter_memo lm
                                           ON (palm.letter_memo_id =
                                                   lm.letter_memo_id
                                               AND lm.letter_memo_type_code =
                                                      'SGA')) palm
                             ON (pa.action_id = palm.action_id)
                         JOIN
                             procurement_action parent_pa
                         ON (pa.parent_action_id = parent_pa.action_id)
                     JOIN
                         pa_x parent_pax
                     ON (parent_pa.action_id = parent_pax.action_id)
             WHERE       pa.action_id != parent_pa.action_id
                     AND parent_pa.funding_action_type IN ('01', '03')
                     AND pa.funding_action_type IN ('01', '03')
                     AND pa.action_type_code IN ('D', 'Z')
                     AND pa.commit_flag = 'N'
                     AND pa.is_checkbook <> 'N'
                     AND pa.action_id = p_action_id                    --40681
                     AND p_id_type = 'ACTION')
        LOOP
            PIPE ROW (lettertype_adf (cur.link_id,
                                      cur.letter_memo_group_code,
                                      cur.letter_memo_type_code,
                                      cur.is_valid,
                                      cur.is_editable,
                                      cur.letter_status,
                                      cur.description,
                                      cur.description2,
                                      cur.description3));
        END LOOP;

        RETURN;
    END;

    FUNCTION getapprovalletterstatus (p_letter_memo_id NUMBER)
        RETURN VARCHAR2
    IS
        l_lm   letter_memo%ROWTYPE := NULL;
    BEGIN
        SELECT   *
          INTO   l_lm
          FROM   letter_memo
         WHERE   letter_memo_id = p_letter_memo_id
                 AND letter_memo_type_code = 'APV';

        IF (l_lm.apv_distribution_date IS NOT NULL)
        THEN
            RETURN 'Completed';
        END IF;

        IF (l_lm.apv_reject_date IS NOT NULL)
        THEN
            RETURN 'Rejected';
        END IF;

        IF (l_lm.apv_withdraw_date IS NOT NULL)
        THEN
            RETURN 'Withdrawn';
        END IF;

        IF (l_lm.apv_recd_from_op IS NOT NULL)
        THEN
            RETURN 'Received from OAA';
        END IF;

        IF (l_lm.apv_sent_to_op IS NOT NULL)
        THEN
            RETURN 'Sent to OAA';
        END IF;

        IF (l_lm.apv_clearance_spl_date IS NOT NULL)
        THEN
            RETURN 'Cleared';
        END IF;

        IF (l_lm.apv_review_spl_date IS NOT NULL)
        THEN
            RETURN 'Reviewed';
        END IF;

        IF (l_lm.apv_spl_assigned_date IS NOT NULL)
        THEN
            RETURN 'Specialist Assigned';
        END IF;

        IF (l_lm.apv_aor_concur_date IS NOT NULL)
        THEN
            RETURN 'AOR Concurred';
        END IF;

        IF (l_lm.apv_request_date IS NOT NULL)
        THEN
            RETURN 'Letter Requested';
        END IF;

        RETURN 'New';
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            RETURN 'Not Valid';
    END;

    FUNCTION isvalid (p_action_id      NUMBER,
                      p_letter_type    VARCHAR2,
                      p_award_seq      NUMBER)
        RETURN VARCHAR2
    IS
        rtn   VARCHAR2 (20) := '';
    BEGIN
        ---  select letter_type into rtn
        SELECT   CASE
                     WHEN letter_type = 'NCE'
                     THEN
                         CASE WHEN months <= 3 THEN 'NCK' ELSE 'NCA' END
                     ELSE
                         letter_type
                 END
          INTO   rtn
          FROM   table(letters_pkg.getvalidlettertypes (p_action_id,
                                                        p_award_seq))
         WHERE   letter_type = p_letter_type;

        RETURN rtn;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            RETURN NULL;
    END;


    --    function getTAApprovalDetails(p_action_id number)return varchar2 is
    FUNCTION gettaapprovaldetails (p_ta_approval_seq NUMBER)
        RETURN VARCHAR2
    IS
        --    CURSOR c1 IS SELECT * from pa_ta_approval_details where action_id=        p_action_id
        CURSOR c1
        IS
              SELECT   *
                FROM   pa_ta_approval_details
               WHERE   ta_approval_seq = p_ta_approval_seq
            ---   p_action_id
            ORDER BY   trip_nbr;

        details          pa_ta_approval_details%ROWTYPE;
        textd            VARCHAR2 (4000);
        flagt            NUMBER;
        countrows        NUMBER;
        totaltrips       NUMBER;
        totaltravelers   NUMBER;
    BEGIN
        textd := NULL;
        flagt := 0;
        countrows := 0;
        totaltrips := 0;
        totaltravelers := 0;

        OPEN c1;

        LOOP
            FETCH c1 INTO details;

            EXIT WHEN c1%NOTFOUND OR c1%NOTFOUND IS NULL;

            IF (flagt = 0)
            THEN
                textd := '';
                flagt := 1;
            END IF;

            textd :=
                   textd
                || '           '
                || LPAD (details.nbrof_trips, 2, '0')
                || '                                   '
                || LPAD (details.nbrof_individuals, 2, '0')
                || '                                      '
                || details.itinerary
                || CHR (10);
            countrows := countrows + 1;
            totaltrips := totaltrips + details.nbrof_trips;
            totaltravelers := totaltravelers + details.nbrof_individuals;
        END LOOP;

        CLOSE c1;

        /* if(countrows!=0)then
            textd:=textd||' <b>Total '||totaltrips||' Trips              Total '||totaltravelers||' Travelers</b>'||chr(10);
            countrows:=countrows+1;
         end if;*/


        IF (textd IS NULL)
        THEN
            error_msg :=
                error_msg
                || '<b>Travel Approval Details:</b>  No data found in pa_ta_approval_details table, '
                || p_ta_approval_seq
                || CHR (10);
            --                ||p_action_id||chr(10);
            flag := 1;
        END IF;

        /*    while countrows<17 loop
            textd:=textd||chr(10);
            countrows:=countrows+1;
            end loop;*/
        RETURN textd;
    END;

    PROCEDURE get_pa_rec (p_action_id NUMBER, p_aotr_id VARCHAR2)
    IS
        /* If stmts
         -- PAL,PML     proposal_name

         -- PAL,PML,SGA country_region,
                        funding_action_type

         -- PAL         proposal_rec_date,
                        pal_start_date,
                        pal_amount,
                        agreement officer


         -- PML,NCE,SGA,TA aotr_name,
                        phone_no,
                        email (SGA only)


         -- PML        award_start_date, --- removed jun 2016
                       pml_end_date,
                       pml_req_date, --Apr 2013
                       current_award_nbr
                       agreement oficer sep 2018

         -- NCE        award_end_date,
                       NCE_date,

         --NCE,TA      award_nbr


         --SGA         approved_amt
                       Parent Award_nbr

                              */
        pa_parent_rec   procurement_action%ROWTYPE := NULL;
    BEGIN
        SELECT   *
          INTO   pa_rec
          FROM   procurement_action
         WHERE   action_id = p_action_id;

        IF (letter_type IN (pal, pml))
        THEN
            --Check if proposal name is null
            IF (pa_rec.proposal_name IS NOT NULL)
            THEN
                pa_rec_x.proposal_name := pa_rec.proposal_name;
            ELSE
                error_msg :=
                       error_msg
                    || '<b>Proposal Name:</b> Name cannot be null, '
                    || pa_rec.proposal_name
                    || CHR (10);
                flag := 1;
            END IF;
        END IF;

        IF (letter_type IN (pal, pml, sga))
        THEN
            --Check if Country or Region is null
            IF (pa_rec.country_code IS NOT NULL)
            THEN
                pa_rec_x.country_region :=
                       ' ('
                    || get_description ('country_code', pa_rec.country_code)
                    || ')';
            ELSE                                  -- See if region is not null
                IF (pa_rec.region_code IS NOT NULL)
                THEN
                    pa_rec_x.country_region :=
                        ' ('
                        || get_description ('region_code',
                                            pa_rec.region_code)
                        || ')';
                ELSE
                    pa_rec_x.country_region := '';
                END IF;
            END IF;

            --Check if Funding Action Type is null
            IF (pa_rec.funding_action_type IS NOT NULL)
            THEN
                get_funding_action_rec (pa_rec.funding_action_type);
                pa_rec_x.funding_action_name :=
                    LOWER (funding_action_rec.funding_action_description);

                IF (pa_rec_x.funding_action_name IS NULL)
                THEN
                    error_msg :=
                        error_msg
                        || '<b>Funding Action Description:</b> Description cannot be null, '
                        || pa_rec_x.funding_action_name
                        || CHR (10);
                    flag := 1;
                END IF;
            ELSE
                error_msg :=
                       error_msg
                    || '<b>Funding Action Type:</b> Type cannot be null, '
                    || pa_rec.funding_action_type
                    || CHR (10);
                flag := 1;
            END IF;
        END IF;

        IF (letter_type IN (pal))
        THEN
            --Check if proposal_rec_date is null
            IF (pa_rec.proposal_rec_date IS NOT NULL)
            THEN
                pa_rec_x.proposal_rec_date := pa_rec.proposal_rec_date;
            ELSE
                error_msg :=
                       error_msg
                    || '<b>Proposal Receive Date:</b> Date cannot be null, '
                    || pa_rec.proposal_rec_date
                    || CHR (10);
                flag := 1;
            END IF;

            --Check if PAL Start date is null
            IF (pa_rec.pal_start_date IS NOT NULL)
            THEN
                pa_rec_x.pal_start_date := pa_rec.pal_start_date;
            ELSE
                error_msg :=
                       error_msg
                    || '<b>PAL Start Date:</b> Date cannot be null, '
                    || pa_rec.pal_start_date
                    || CHR (10);
                flag := 1;
            END IF;

            --Check if PAL amount is null
            IF (pa_rec.pal_amt IS NOT NULL)
            THEN
                pa_rec_x.pal_amt := pa_rec.pal_amt;
            ELSE
                error_msg :=
                       error_msg
                    || '<b>PAL Amount:</b> Amount cannot be null, '
                    || pa_rec.pal_amt
                    || CHR (10);
                flag := 1;
            END IF;

            --aotr
            IF (p_aotr_id IS NOT NULL)
            THEN
                get_abacus_user_rec (p_aotr_id);

                IF (abacus_user_rec.first_name IS NOT NULL
                    AND abacus_user_rec.last_name IS NOT NULL)
                THEN
                    pa_rec_x.agreement_officer :=
                           abacus_user_rec.first_name
                        || ' '
                        || abacus_user_rec.last_name;
                ELSE
                    error_msg :=
                        error_msg
                        || '<b>Agreement Officer Name</b>: First Name and Last Name cannot be null, '
                        || abacus_user_rec.first_name
                        || ' '
                        || abacus_user_rec.last_name
                        || CHR (10);
                    flag := 1;
                END IF;
            ELSE
                error_msg :=
                       error_msg
                    || '<b>Agreement Officer ID:</b> ID cannot be null, '
                    || p_aotr_id
                    || CHR (10)
;
                flag := 1;
            END IF;
        END IF;                                                   --pal if end

        IF (letter_type IN (nce, sga, ta))
        THEN
            --AOTR Name get Full Name and Phone no
            --check if the p_aotr_id is equal to either aotr or alt_aotr
            IF (p_aotr_id IS NOT NULL)
            THEN
                IF (p_aotr_id IN (pa_rec.cto, pa_rec.alternate_cto))
                THEN
                    get_abacus_user_rec (p_aotr_id);

                    --Get aotr first name and last name
                    IF (abacus_user_rec.first_name IS NOT NULL
                        AND abacus_user_rec.last_name IS NOT NULL)
                    THEN
                        pa_rec_x.aotr_name :=
                               abacus_user_rec.first_name
                            || ' '
                            || abacus_user_rec.last_name;
                    ELSE
                        error_msg :=
                            error_msg
                            || '<b>AOR Name:</b> First Name and Last Name cannot be null, '
                            || abacus_user_rec.first_name
                            || ' '
                            || abacus_user_rec.last_name
                            || CHR (10);
                        flag := 1;
                    END IF;

                    -- Get aotr phone number
                    IF (abacus_user_rec.phone_nbr IS NOT NULL)
                    THEN
                        pa_rec_x.aotr_phone := abacus_user_rec.phone_nbr;
                    ELSE
                        error_msg :=
                            error_msg
                            || '<b>AOR Phone number:</b> Phone cannot be null, '
                            || abacus_user_rec.phone_nbr
                            || CHR (10);
                        flag := 1;
                    END IF;

                    -- Get aotr email
                    IF (letter_type IN (sga))
                    THEN
                        IF (abacus_user_rec.email IS NOT NULL)
                        THEN
                            pa_rec_x.aotr_email := abacus_user_rec.email
;
                        ELSE
                            error_msg :=
                                   error_msg
                                || '<b>AOR Email:</b> Email cannot be null, '
                                || abacus_user_rec.email
                                || CHR (10);
                            flag := 1;
                        END IF;
                    END IF;
                ELSE
                    error_msg :=
                           error_msg
                        || '<b>AOR:</b> Invalid AOR,'
                        || p_aotr_id
                        || '. Action has AOR='
                        || pa_rec.cto
                        || ' and Alternate AOR='
                        || pa_rec.alternate_cto
                        || '.'
                        || CHR (10);
                    flag := 1;
                END IF;
            ELSE
                error_msg :=
                       error_msg
                    || '<b>AOR:</b> ID cannot be null, '
                    || p_aotr_id
                    || CHR (10);
                flag := 1;
            END IF;
        END IF;                                    --end if for in(nce,ta,sga)

        IF (letter_type IN (pml))
        THEN
            --AO Name get Full Name and Phone no
            --check if the p_aotr_id is equal to either aotr or alt_aotr

            IF (p_aotr_id IS NOT NULL)
            THEN
                --  IF (isaotrinmodaction (p_aotr_id, pa_rec.mod_action_id) = 'Y')
                ---  THEN
                get_abacus_user_rec (p_aotr_id);

                --Get aotr first name and last name
                IF (abacus_user_rec.first_name IS NOT NULL
                    AND abacus_user_rec.last_name IS NOT NULL)
                THEN
                    pa_rec_x.agreement_officer :=
                           --                        pa_rec_x.aotr_name :=
                           abacus_user_rec.first_name
                        || ' '
                        || abacus_user_rec.last_name;
                ELSE
                    error_msg :=
                        error_msg
                        || '<b>Agreement Officer Name</b>: First Name and Last Name cannot be null, '
                        || abacus_user_rec.first_name
                        || ' '
                        || abacus_user_rec.last_name
                        || CHR (10);
                    flag := 1;
                END IF;

                -- Get aotr phone number
                /*** IF (abacus_user_rec.phone_nbr IS NOT NULL)
                 THEN
                     pa_rec_x.aotr_phone := abacus_user_rec.phone_nbr;
                 ELSE
                     error_msg :=
                         error_msg
                         || '<b>AOR Phone number:</b> Phone cannot be null, '
                         || abacus_user_rec.phone_nbr
                         || CHR (10);
                     flag := 1;
                 END IF;***/

                -- Get aotr email
                IF (letter_type IN (sga))
                THEN
                    IF (abacus_user_rec.email IS NOT NULL)
                    THEN
                        pa_rec_x.aotr_email := abacus_user_rec.email
;
                    ELSE
                        error_msg :=
                               error_msg
                            || '<b>AOR Email:</b> Email cannot be null, '
                            || abacus_user_rec.email
                            || CHR (10);
                        flag := 1;
                    END IF;
                END IF;
            ELSE
                --error_msg:=error_msg||'<b>AOR:</b> Invalid Proposed AOR,'||p_aotr_id||'. Action has Proposed AOR='||pa_rec.proposed_aotr||' and Proposed Alternate AOR='||pa_rec.proposed_alternate_aotr||'.'||chr(10);
                error_msg :=
                       error_msg
                    || '<b>Agreement Officer ID:</b> ID cannot be null, '
                    || p_aotr_id
                    || CHR (10);
                flag := 1;
            --    END IF;
            --            ELSE
            --              error_msg :=
            --                   error_msg
            --              || '<b>AOR:</b> ID cannot be null, '
            --            || p_aotr_id
            --          || CHR (10);
            --    flag := 1;
            END IF;


            -- award_start_date --- Checked removed from Jun 2016
            /*** if(pa_rec.award_start_date is not null) then
                pa_rec_x.award_start_date:=pa_rec.award_start_date;
             else
                error_msg:=error_msg||'<b>Award Start Date:</b> Date cannot be null, '||pa_rec.award_start_date||chr(10);
                flag:=1;
             end if;****/

            -- pml_end_date modification_amt
            IF (pa_rec.pml_req_date IS NOT NULL)
            THEN            --Apr 2013    changed pml_end date to PML req date
                pa_rec_x.pml_req_date := pa_rec.pml_req_date;
            ELSE
                error_msg :=
                       error_msg
                    || '<b>PML Request Date:</b>Date  cannot be null, '
                    || pa_rec.pml_req_date
                    || CHR (10);
                flag := 1;
            END IF;

            -- Current Award Number
            pa_rec_x.current_award_nbr :=
                NVL (getcurrentawardnbr (pa_rec.mod_action_id),
                     pa_rec.current_award_nbr);

            IF (pa_rec_x.current_award_nbr IS NULL AND flag = 0)
            THEN
                error_msg :=
                    error_msg
                    || '<b>Current Award Number:</b> Award Number cannot be null, '
                    || pa_rec.current_award_nbr
                    || CHR (10);
                flag := 1;
            END IF;
        END IF;                                              -- end if for pml

        IF (letter_type IN (nce))
        THEN
            -- Award_end_date
            IF (pa_rec.award_end_date IS NOT NULL)
            THEN
                pa_rec_x.award_end_date := pa_rec.award_end_date;
            ELSE
                error_msg :=
                       error_msg
                    || '<b>Award End Date:</b> Date cannot be null, '
                    || pa_rec.award_end_date
                    || CHR (10);
                flag := 1;
            END IF;

            -- NCE DATE
            BEGIN
                SELECT   nce_date
                  INTO   pa_rec_x.nce_date
                  FROM   pa_x
                 WHERE   action_id = pa_rec.action_id;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    pa_rec_x.nce_date := NULL;
            END;

            IF (pa_rec_x.nce_date IS NULL)
            THEN
                error_msg :=
                       error_msg
                    || '<b>NCE End Date:</b> Date cannot be null, '
                    || pa_rec_x.nce_date
                    || CHR (10);
                flag := 1;
            END IF;
        END IF;                                               --end if for nce

        IF (letter_type IN (nce, sga, ta))
        THEN
            --Award Number
            IF (pa_rec.award_nbr IS NOT NULL)
            THEN
                pa_rec_x.award_nbr := pa_rec.award_nbr;
            ELSE
                error_msg :=
                       error_msg
                    || '<b>Award Number:</b> Award cannot be null, '
                    || pa_rec.award_nbr
                    || CHR (10);
                flag := 1;
            END IF;
        END IF;                                            --end if for nce,ta
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            error_msg :=
                   error_msg
                || '</b>Action Id:</b> no data found in Action, '
                || p_action_id
                || CHR (10);
            flag := 1;
            NULL;
    END;

    PROCEDURE get_awardee_rec (awardeecode          NUMBER,
                               p_awardee_add_seq    NUMBER,
                               flagp                NUMBER)
    IS
        temp_address   VARCHAR2 (500) := '';
    BEGIN
        /*
         --if flagp==1 then use awardee_p_rec_x
         --if flagp!=1 then use awardee_rec_x
         --if flagp==2 then do not get address.
         --if flag is anything other than 1 then use awardee_rec_x
         --
         -- Get Awardee Name
         -- Get Address and put it in organization_add
            -- Get Address 1    (needed)
            -- Add Address 2    (if not null)
            -- Get City         (needed)
            -- Get State        (needed)
            -- Get Zip          (needed)
            -- Get Country      (needed)


        */
        SELECT   *
          INTO   awardee_rec
          FROM   awardee_lookup
         WHERE   awardee_code = awardeecode;

        --Check if awardee name is null
        IF (awardee_rec.awardee_name IS NOT NULL)
        THEN
            IF (flagp != 1)
            THEN
                awardee_rec_x.organization_name := awardee_rec.awardee_name;
            ELSE
                awardee_p_rec_x.organization_name := awardee_rec.awardee_name;
            END IF;
        ELSE
            error_msg :=
                   error_msg
                || '<b>Awardee Name:</b>Name cannot be null, '
                || awardee_rec.awardee_name
                || CHR (10);
            flag := 1;
        END IF;

        -- Get Awardee Address
        IF (flagp != 2)
        THEN
            get_awardee_add_rec (awardeecode, p_awardee_add_seq);

            IF (awardee_add_rec.awardee_add_seq IS NOT NULL)
            THEN
                --Add address_1 and address_2 to address if not null
                awardee_rec_x.organization_add := '';

                IF (awardee_add_rec.address_1 IS NOT NULL)
                THEN
                    awardee_rec_x.organization_add :=
                           awardee_rec_x.organization_add
                        || awardee_add_rec.address_1
                        || CHR (10);
                ELSE
                    error_msg :=
                           error_msg
                        || '<b>Address Line 1:</b>Line 1 cannot be null, '
                        || awardee_add_rec.address_1
                        || CHR (10);
                    flag := 1;
                END IF;

                -- Add Address_2 line if it is not null
                IF (awardee_add_rec.address_2 IS NOT NULL)
                THEN
                    awardee_rec_x.organization_add :=
                           awardee_rec_x.organization_add
                        || awardee_add_rec.address_2
                        || CHR (10);
                END IF;

                --Check if awardee City is null
                IF (awardee_add_rec.city IS NOT NULL)
                THEN
                    temp_address := temp_address || awardee_add_rec.city;
                ELSE
                    error_msg :=
                           error_msg
                        || '<b>Awardee City:</b> City cannot be null, '
                        || awardee_add_rec.city
                        || CHR (10);
                    flag := 1;
                END IF;

                --Check if awardee state is null
                IF (awardee_add_rec.state IS NOT NULL)
                THEN
                    temp_address :=
                        temp_address || ', ' || awardee_add_rec.state;
                END IF;

                --Check if awardee zip_code is null
                IF (awardee_add_rec.zip_code IS NOT NULL)
                THEN
                    temp_address :=
                        temp_address || ' ' || awardee_add_rec.zip_code;
                END IF;

                --Check if awardee country_code is null
                IF (awardee_add_rec.country_code IS NOT NULL)
                THEN
                    temp_address :=
                        temp_address || CHR (10)
                        || get_description ('country_code',
                                            awardee_add_rec.country_code);
                ELSE
                    error_msg :=
                           error_msg
                        || '<b>Awardee Country:</b> Country cannot be null, '
                        || awardee_add_rec.country_code
                        || CHR (10);
                    flag := 1;
                END IF;

                --Add rest of address
                awardee_rec_x.organization_add :=
                    awardee_rec_x.organization_add || temp_address;
            END IF;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            error_msg :=
                   error_msg
                || '<b>Awardee:</b> No data found in Awardee Lookup , '
                || awardeecode
                || CHR (10);
            flag := 1;
            NULL;
    END;

    PROCEDURE get_contact_rec (p_awardee_contact_seq    NUMBER,
                               flagp                    NUMBER,
                               p_awardee_code           NUMBER)
    IS
    BEGIN
        /*
          -- Get Contact Name
        */
        SELECT   *
          INTO   contact_rec
          FROM   awardee_contacts
         WHERE   awardee_code = p_awardee_code
                 AND awardee_contact_seq = p_awardee_contact_seq;

        IF (contact_rec.contact_name IS NOT NULL)
        THEN
            contact_rec_x.contact_name := contact_rec.contact_name;
        ELSE
            error_msg :=
                   error_msg
                || '<b>Contact Name:</b> Name cannot be null '
                || contact_rec.contact_name
                || CHR (10);
            flag := 1;
        END IF;

        IF (contact_rec.salutation IS NOT NULL)
        THEN
            contact_rec_x.salutation := contact_rec.salutation;
        ELSE
            error_msg :=
                error_msg
                || '<b>Contact Name Salutation:</b> Salutation cannot be null '
                || contact_rec.salutation
                || CHR (10);
            flag := 1;
        END IF;
    -- end if;--pal,pml,nce


    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            error_msg :=
                   error_msg
                || '<b>Contact:</b>  No data found in Awardee Lookup, '
                || p_awardee_contact_seq
                || CHR (10);
            flag := 1;
            NULL;
    END;

    PROCEDURE get_subgrant_app_rec (p_action_id NUMBER)
    IS
    BEGIN
        SELECT   *
          INTO   sga_rec
          FROM   pa_subgrant_letter
         WHERE   action_id = p_action_id;

        -- Get letter Date
        IF (sga_rec.letter_date IS NOT NULL)
        THEN
            subgrant_app_rec_x.letter_date := sga_rec.letter_date;
        ELSE
            error_msg :=
                   error_msg
                || '<b>Letter Date:</b> Date cannot be null '
                || sga_rec.letter_date
                || CHR (10);
            flag := 1;
        END IF;

        --Get RefA
        IF (sga_rec.refa IS NOT NULL)
        THEN
            subgrant_app_rec_x.refa := sga_rec.refa;
        ELSE
            error_msg :=
                   error_msg
                || '<b>Reference A:</b> Reference A cannot be null '
                || sga_rec.refa
                || CHR (10);
            flag := 1;
        END IF;

        --Get Refb
        IF (sga_rec.refb IS NOT NULL)
        THEN
            subgrant_app_rec_x.refb := sga_rec.refb;
        ELSE
            error_msg :=
                   error_msg
                || '<b>Reference B:</b> Reference B cannot be null '
                || sga_rec.refb
                || CHR (10);
            flag := 1;
        END IF;

        --Get Purpose
        IF (sga_rec.purpose IS NOT NULL)
        THEN
            subgrant_app_rec_x.purpose := sga_rec.purpose;
        ELSE
            error_msg :=
                   error_msg
                || '<b>Purpose:</b> Purpose cannot be null '
                || sga_rec.purpose
                || CHR (10);
            flag := 1;
        END IF;

        --Get Details
        IF (sga_rec.details IS NOT NULL)
        THEN
            subgrant_app_rec_x.details := sga_rec.details;
        ELSE
            error_msg :=
                   error_msg
                || '<b>Details:</b> Details cannot be null '
                || sga_rec.details
                || CHR (10);
            flag := 1;
        END IF;

        --Get Details
        IF (sga_rec.effective_date IS NOT NULL)
        THEN
            subgrant_app_rec_x.effective_date := sga_rec.effective_date;
        ELSE
            error_msg :=
                   error_msg
                || '<b>Effective Date:</b> Date cannot be null '
                || sga_rec.effective_date
                || CHR (10);
            flag := 1;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            error_msg :=
                error_msg
                || '<b>SubGrant Approval:</b>  No data found in SubGrant_Approval, '
                || p_action_id
                || CHR (10);
            flag := 1;
            NULL;
    END;

    --    procedure get_ta_app_rec(p_action_id number) is
    PROCEDURE get_ta_app_rec (p_ta_approval_seq NUMBER)
    IS
    BEGIN
        SELECT   *
          INTO   ta_rec
          FROM   pa_ta_approval
         WHERE   ta_approval_seq = p_ta_approval_seq;

        --                        where action_id=p_action_id;
        -- Get letter Date
        IF (ta_rec.letter_date IS NOT NULL)
        THEN
            ta_rec_x.letter_date := ta_rec.letter_date;
        ELSE
            error_msg :=
                   error_msg
                || '<b>Letter Date:</b> Date cannot be null '
                || ta_rec.letter_date
                || CHR (10);
            flag := 1;
        END IF;

        --Get reference subject
        IF (ta_rec.ref_subject IS NOT NULL)
        THEN
            ta_rec_x.ref_subject := ta_rec.ref_subject;
        ELSE
            error_msg :=
                   error_msg
                || '<b>Reference Subject:</b> subject cannot be null '
                || ta_rec.ref_subject
                || CHR (10);
            flag := 1;
        END IF;

        --Get Purpose
        IF (ta_rec.purpose IS NOT NULL)
        THEN
            ta_rec_x.purpose := ta_rec.purpose;
        ELSE
            error_msg :=
                   error_msg
                || '<b>Purpose:</b> Purpose cannot be null '
                || ta_rec.purpose
                || CHR (10);
            flag := 1;
        END IF;

        --Get ref_date
        IF (ta_rec.ref_date IS NOT NULL)
        THEN
            ta_rec_x.ref_date := ta_rec.ref_date;
        ELSE
            error_msg :=
                   error_msg
                || '<b>Reference Date:</b> Date cannot be null '
                || ta_rec.ref_date
                || CHR (10);
            flag := 1;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            error_msg :=
                   error_msg
                || '<b>TA Approval:</b>  No data found in PA_TA_Approval, '
                || p_ta_approval_seq
                --                '<b>TA Approval:</b>  No data found in PA_TA_Approval, '||p_action_id
                || CHR (10);
            flag := 1;
            NULL;
    END;

    PROCEDURE get_funding_action_rec (funding_action_type VARCHAR2)
    IS
    BEGIN
        SELECT   *
          INTO   funding_action_rec
          FROM   funding_action_lookup
         WHERE   funding_action_type = pa_rec.funding_action_type;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            error_msg :=
                error_msg
                || '<b>Funding Action Type:</b> No Match found for funding action type in Funding Action Lookup, '
                || funding_action_type
                || CHR (10);
            flag := 1;
            NULL;
    END;

    PROCEDURE get_abacus_user_rec (p_aotr_id VARCHAR2)
    IS
    BEGIN
        SELECT   *
          INTO   abacus_user_rec
          FROM   abacus_user
         WHERE   userid = p_aotr_id AND active = 'Y';
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            error_msg :=
                error_msg
                || '<b>Agreement Officer/AOR:</b> No Match found for userId in User Manager or User is Not Active, '
                || p_aotr_id
                || CHR (10);
            flag := 1;
            NULL;
    END;

    PROCEDURE get_awardee_add_rec (p_awardee_code       NUMBER,
                                   p_awardee_add_seq    NUMBER)
    IS
    BEGIN
        SELECT   *
          INTO   awardee_add_rec
          FROM   awardee_address
         WHERE   awardee_add_seq = p_awardee_add_seq
                 AND awardee_code = p_awardee_code;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            error_msg :=
                error_msg
                || '<b>Awardee Address:</b> Invalid awardee address. Record does not exist or address does not match with awardee, address_seq='
                || p_awardee_add_seq
                || ' awardee_code='
                || awardee_rec.awardee_code
                || CHR (10);
            flag := 1;
            NULL;
    END;

    FUNCTION generate_letter (p_action_id              NUMBER,
                              p_awardee_contact_seq    NUMBER,
                              p_awardee_add_seq        NUMBER,
                              p_aotr_id                VARCHAR2,
                              p_letter_type            VARCHAR2,
                              p_ta_approval_seq        NUMBER,
                              p_award_seq              NUMBER)
        RETURN CLOB
    IS
        temp   VARCHAR2 (30) := '';
    BEGIN
        letters_pkg.generate_pa_rec := NULL;
        letters_pkg.generate_flag := 0;
        letters_pkg.generate_err_msg := '';
        temp := isvalid (p_action_id, p_letter_type, p_award_seq);

        IF (temp IS NULL)
        THEN
            generate_flag := 1;
            generate_err_msg := 'Invalid Letter';
        END IF;

        IF (generate_flag = 0)
        THEN
            IF (temp = pal)
            THEN                                                         --PAL
                RETURN letters_pkg.letter_pal (p_action_id,
                                               p_awardee_contact_seq,
                                               p_awardee_add_seq,
                                               p_aotr_id);
            ELSE
                IF (temp = pml)
                THEN                                                     --PML
                    RETURN letters_pkg.letter_pml (p_action_id,
                                                   p_awardee_contact_seq,
                                                   p_awardee_add_seq,
                                                   p_aotr_id);
                ELSE
                    IF (temp = nce_ack)
                    THEN                                             --NCE_ACK
                        RETURN letters_pkg.letter_nce_ack (
                                   p_action_id,
                                   p_awardee_contact_seq,
                                   p_awardee_add_seq,
                                   p_aotr_id);
                    ELSE
                        IF (temp = nce_approval)
                        THEN                                    --NCE_APPROVAL
                            RETURN letters_pkg.letter_nce_approval (
                                       p_action_id,
                                       p_awardee_contact_seq,
                                       p_awardee_add_seq,
                                       p_aotr_id);
                        ELSE
                            IF (temp = sga)
                            THEN                           --SubGrant Approval
                                RETURN letters_pkg.letter_subgrant_approval (
                                           p_action_id,
                                           p_awardee_contact_seq,
                                           p_awardee_add_seq,
                                           p_aotr_id);
                            ELSE
                                IF (temp = ta)
                                THEN                             --TA Approval
                                    RETURN letters_pkg.letter_ta_approval (
                                               p_award_seq,
                                               p_awardee_contact_seq,
                                               p_awardee_add_seq,
                                               p_aotr_id,
                                               p_ta_approval_seq);
                                ELSE
                                    RETURN '<font color=red>Unknown letter type</font>';
                                END IF;
                            END IF;
                        END IF;
                    END IF;
                END IF;
            END IF;
        END IF;

        RETURN '<font color=red>' || generate_err_msg || '</font>';
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN 'Error in Generate Letter' || SQLERRM;
    END;

    FUNCTION letter_pal (p_action_id              NUMBER,
                         p_awardee_contact_seq    NUMBER,
                         p_awardee_add_seq        NUMBER,
                         p_aotr_id                VARCHAR2)
        RETURN CLOB
    IS
        letter_body   CLOB;
    BEGIN
        letters_pkg.pa_rec := NULL;
        letters_pkg.awardee_rec := NULL;
        letters_pkg.contact_rec := NULL;
        letters_pkg.error_msg := '';
        letters_pkg.flag := 0;
        letters_pkg.letter_type := pal;

        letters_pkg.get_pa_rec (p_action_id, p_aotr_id);

        IF (letters_pkg.pa_rec.action_id IS NOT NULL)
        THEN
            letters_pkg.get_awardee_rec (letters_pkg.pa_rec.awardee_code,
                                         p_awardee_add_seq,
                                         0);

            IF (letters_pkg.awardee_rec.awardee_code IS NOT NULL)
            THEN
                letters_pkg.get_contact_rec (
                    p_awardee_contact_seq,
                    0,
                    letters_pkg.awardee_rec.awardee_code);
            ELSE
                flag := 1;
            END IF;
        ELSE
            flag := 1;
        END IF;

        letter_body := NULL;

        IF (flag = 0)
        THEN
            letter_body :=
                REPLACE (
                       ''
                    || contact_rec_x.contact_name
                    || CHR (10)
                    || awardee_rec_x.organization_name
                    || CHR (10)
                    || awardee_rec_x.organization_add
                    || CHR (10)
                    || '
<b>Subject:</b>  Pre-Award Expenses '
                    || CHR (10)
                    || '
<b>Reference:</b>  '
                    || awardee_rec_x.organization_name
                    || ' application entitled "'
                    || pa_rec_x.proposal_name
                    || '" in'
                    || pa_rec_x.country_region
                    || ' dated '
                    || TO_CHAR (pa_rec_x.proposal_rec_date, date_format)
                    || '

Dear '
                    || contact_rec_x.salutation
                    || ':

The U.S. Agency for International Development (USAID) intends to enter into negotiations with your organization leading to the award of a '
                    || pa_rec_x.funding_action_name
                    || ' in response to the referenced application.

Per your request, and with the mutual understanding that your organization will proceed at risk in the event an award is not made, any allowable, allocable and reasonable expenses incurred on or after        '
                    || TO_CHAR (pa_rec_x.pal_start_date, date_format)
                    || ' will be reimbursed under the resultant award. Pre-award expenses shall not exceed '
                    || TO_CHAR (pa_rec_x.pal_amt, 'FM$9,999,999,999,999.00')
                    || '.

This authorization is subject to the successful negotiation and award of a '
                    || pa_rec_x.funding_action_name
                    || ' in accordance with the subject application, and in no way obligates the United States Government, or USAID, to reimburse your organization for any expenses incurred in the event an award is not made.

Please note that this letter is not the award itself, and that your organization will be unable to receive funds from USAID until and unless the award is made.

Sincerely,


'
                    || pa_rec_x.agreement_officer
                    || '
Agreement Officer
Office of Acquisition and Assistance
M/OAA/DCHA/OFDA

cc:
DCHA/OFDA/PS Grants Unit',
                    '?',
                    CHR (46));
        END IF;


        IF (letter_body IS NULL)
        THEN
            letter_body := '' || error_msg || '';
            letter_body :=
                   '<b><u>Column Name: Error Message, Value</u></b>'
                || CHR (10)
                || letter_body;
        END IF;


        RETURN letter_body;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN 'Error in Generating PAL Letter' || SQLERRM;
    END;

    FUNCTION letter_pml (p_action_id              NUMBER,
                         p_awardee_contact_seq    NUMBER,
                         p_awardee_add_seq        NUMBER,
                         p_aotr_id                VARCHAR2)
        RETURN CLOB
    IS
        letter_body   CLOB;
    BEGIN
        letters_pkg.pa_rec := NULL;
        letters_pkg.awardee_rec := NULL;
        letters_pkg.contact_rec := NULL;
        letters_pkg.error_msg := '';
        letters_pkg.flag := 0;
        letters_pkg.letter_type := pml;

        letters_pkg.get_pa_rec (p_action_id, p_aotr_id);

        IF (letters_pkg.pa_rec.action_id IS NOT NULL)
        THEN
            letters_pkg.get_awardee_rec (letters_pkg.pa_rec.awardee_code,
                                         p_awardee_add_seq,
                                         0);

            IF (letters_pkg.awardee_rec.awardee_code IS NOT NULL)
            THEN
                letters_pkg.get_contact_rec (
                    p_awardee_contact_seq,
                    0,
                    letters_pkg.awardee_rec.awardee_code);
            ELSE
                flag := 1;
            END IF;
        ELSE
            flag := 1;
        END IF;

        letter_body := NULL;

        IF (flag = 0)
        THEN
            letter_body :=
                REPLACE (
                       ''
                    || TO_CHAR (SYSDATE, date_format)
                    || CHR (10)
                    || '
'
                    || contact_rec_x.contact_name
                    || CHR (10)
                    || awardee_rec_x.organization_name
                    || CHR (10)
                    || awardee_rec_x.organization_add
                    || CHR (10)
                    || '
<b>Subject:</b>  Award No. '
                    || pa_rec_x.current_award_nbr
                    || CHR (10)
                    || '
<b>Reference:</b>  '
                    || awardee_rec_x.organization_name
                    || CHR (39)
                    || 's request letter dated '
                    || TO_CHAR (pa_rec.pml_req_date, date_format)
                    || CHR (10)
                    || CHR (10)
                    || 'Dear '
                    || contact_rec_x.salutation
                    || ':

Per your referenced request, I am writing to update you on the status of '
                    || awardee_rec_x.organization_name
                    || CHR (39)
                    || 's proposal requesting a modification to the above-referenced award. '
                    || awardee_rec_x.organization_name
                    || ' has requested to pursue program activities as outlined in the modification proposal.

At the current time, '
                    || awardee_rec_x.organization_name
                    || ''
                    || CHR (39)
                    || 's proposal has successfully completed OFDA'
                    || CHR (39)
                    || 's programmatic and technical review.  The proposal has been submitted to OFDA'
                    || CHR (39)
                    || 's Grants Unit for negotiation and will subsequently be submitted to me (as the Agreement Officer) for approval. While negotiations are ongoing, '
                    || awardee_rec_x.organization_name
                    || ' may choose to continue incurring costs on its own risk subject to successful completion of negotiations and modification approval. Once the determination has been made to award the modification, costs are covered from the start date of the original award through the end date of the last modification.'
                    || ' However, USAID will not be responsible for any expenses incurred past the agreement end date in the event a modification is not awarded.


Sincerely,


'
                    || pa_rec_x.agreement_officer
                    || '
Agreement Officer

cc:
Regional Advisor
Regional Coordinator
DCHA/OFDA/PS Grants Unit',
                    '?',
                    CHR (46))
;
        END IF;


        IF (letter_body IS NULL)
        THEN
            letter_body := '' || error_msg || '';
            letter_body :=
                   '<b><u>Column Name: Error Message, Value</u></b>'
                || CHR (10)
                || letter_body;
        END IF;


        RETURN letter_body;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN 'Error in Generating PML Letter' || SQLERRM;
    END;

    FUNCTION letter_nce_ack (p_action_id              NUMBER,
                             p_awardee_contact_seq    NUMBER,
                             p_awardee_add_seq        NUMBER,
                             p_aotr_id                VARCHAR2)
        RETURN CLOB
    IS
        letter_body   CLOB;
    BEGIN
        letters_pkg.pa_rec := NULL;
        letters_pkg.awardee_rec := NULL;
        letters_pkg.contact_rec := NULL;
        letters_pkg.error_msg := '';
        letters_pkg.flag := 0;
        letters_pkg.letter_type := nce;

        letters_pkg.get_pa_rec (p_action_id, p_aotr_id);

        IF (letters_pkg.pa_rec.action_id IS NOT NULL)
        THEN
            letters_pkg.get_awardee_rec (letters_pkg.pa_rec.awardee_code,
                                         p_awardee_add_seq,
                                         0);

            IF (letters_pkg.awardee_rec.awardee_code IS NOT NULL)
            THEN
                letters_pkg.get_contact_rec (
                    p_awardee_contact_seq,
                    0,
                    letters_pkg.awardee_rec.awardee_code);
            ELSE
                flag := 1;
            END IF;
        ELSE
            flag := 1;
        END IF;

        letter_body := NULL;

        IF (flag = 0)
        THEN
            letter_body :=
                REPLACE (
                       ''
                    || TO_CHAR (SYSDATE, date_format)
                    || CHR (10)
                    || '
'
                    || contact_rec_x.contact_name
                    || CHR (10)
                    || awardee_rec_x.organization_name
                    || CHR (10)
                    || awardee_rec_x.organization_add
                    || CHR (10)
                    || '
<b>Subject:</b>    Acknowledgement of "No-Cost" Extension of Award No. '
                    || pa_rec_x.award_nbr
                    || '

Dear '
                    || contact_rec_x.salutation
                    || ':

This is to acknowledge your intent to extend the subject award for up to three months under the authority of Section 1.2 of the awards. This "no-cost" extension extends the subject award'
                    || CHR (39)
                    || 's end date, from '
                    || TO_CHAR (pa_rec_x.award_end_date, date_format)
                    || ' to '
                    || TO_CHAR (pa_rec_x.nce_date, date_format)
                    || '.

I hereby acknowledge such extension subject to the explicit understanding that:

    1.  The purpose of the extension is not solely to expend unobligated balances;
    2.  The terms and conditions of the award permit the extension;
    3.  No additional U.S. Government funds are required; and
    4.  The extension does not involve a change in the approved scope or objectives
        of the project.

Please be sure to attach a copy of this letter to all financial reports which include costs incurred after the estimated completion date set forth in the award (as it may have been amended) but within the period of such "no-cost" extension.

Should you have any questions about this extension, please contact me at '
                    || pa_rec_x.aotr_phone
                    || '.

Sincerely,


'
                    || pa_rec_x.aotr_name
                    || '
Agreement Officer'
                    || CHR (39)
                    || 's Representative

cc:
Regional Advisor
Team Lead
DCHA/OFDA/PS Grants Unit
M/FM/Electronic Copy (ei@usaid.gov)',
                    '?',
                    CHR (46))
;
        END IF;


        IF (letter_body IS NULL)
        THEN
            letter_body := '' || error_msg || '';
            letter_body :=
                   '<b><u>Column Name: Error Message, Value</u></b>'
                || CHR (10)
                || letter_body;
        END IF;


        RETURN letter_body;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN 'Error in Generating NCE Acknowledge Letter' || SQLERRM;
    END;

    FUNCTION letter_nce_approval (p_action_id              NUMBER,
                                  p_awardee_contact_seq    NUMBER,
                                  p_awardee_add_seq        NUMBER,
                                  p_aotr_id                VARCHAR2)
        RETURN CLOB
    IS
        letter_body   CLOB;
    BEGIN
        letters_pkg.pa_rec := NULL;
        letters_pkg.awardee_rec := NULL;
        letters_pkg.contact_rec := NULL;
        letters_pkg.error_msg := '';
        letters_pkg.flag := 0;
        letters_pkg.letter_type := nce;

        letters_pkg.get_pa_rec (p_action_id, p_aotr_id);

        IF (letters_pkg.pa_rec.action_id IS NOT NULL)
        THEN
            letters_pkg.get_awardee_rec (letters_pkg.pa_rec.awardee_code,
                                         p_awardee_add_seq,
                                         0);

            IF (letters_pkg.awardee_rec.awardee_code IS NOT NULL)
            THEN
                letters_pkg.get_contact_rec (
                    p_awardee_contact_seq,
                    0,
                    letters_pkg.awardee_rec.awardee_code);
            ELSE
                flag := 1;
            END IF;
        ELSE
            flag := 1;
        END IF;

        letter_body := NULL;

        IF (flag = 0)
        THEN
            letter_body :=
                REPLACE (
                       ''
                    || TO_CHAR (SYSDATE, date_format)
                    || CHR (10)
                    || '
'
                    || contact_rec_x.contact_name
                    || CHR (10)
                    || awardee_rec_x.organization_name
                    || CHR (10)
                    || awardee_rec_x.organization_add
                    || CHR (10)
                    || '
<b>Subject:</b>    Approval of "No-Cost" Extension of Award No. '
                    || pa_rec_x.award_nbr
                    || '

Dear '
                    || contact_rec_x.salutation
                    || ':

This responds to your request for a "no-cost" extension of the subject award'
                    || CHR (39)
                    || 's end date, from '
                    || TO_CHAR (pa_rec_x.award_end_date, date_format)
                    || ' to '
                    || TO_CHAR (pa_rec_x.nce_date, date_format)
                    || '.

Pursuant to Section 1.2 of the award, which permits the Agreement Officer'
                    || CHR (39)
                    || 's Representative (AOR) to approve one-time "no-cost" extensions for more than three months and for twelve months or less, I hereby approve such extension subject to the explicit understanding that:

    1.  The purpose of the extension is not solely to expend unobligated balances;
    2.  The terms and conditions of the award permit the extension;
    3.  No additional U.S. Government funds are required; and
    4.  The extension does not involve a change in the approved scope or objectives
        of the project.

Please be sure to attach a copy of this letter to all financial reports which include costs incurred after the estimated completion date set forth in the award (as it may have been amended) but within the period of such "no-cost" extension.

Should you have any questions about this extension, please contact me at '
                    || pa_rec_x.aotr_phone
                    || '.

Sincerely,


'
                    || pa_rec_x.aotr_name
                    || '
Agreement Officer'
                    || CHR (39)
                    || 's Representative

cc:
Regional Advisor
Team Lead
DCHA/OFDA/PS Grants Unit
M/FM/Electronic Copy (ei@usaid.gov)',
                    '?',
                    CHR (46))
;
        END IF;


        IF (letter_body IS NULL)
        THEN
            letter_body := '' || error_msg || '';
            letter_body :=
                   '<b><u>Column Name: Error Message, Value</u></b>'
                || CHR (10)
                || letter_body;
        END IF;


        RETURN letter_body;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN 'Error in Generating NCE Approval Letter' || SQLERRM;
    END;

    FUNCTION letter_subgrant_approval (p_action_id              NUMBER,
                                       p_awardee_contact_seq    NUMBER,
                                       p_awardee_add_seq        NUMBER,
                                       p_aotr_id                VARCHAR2)
        RETURN CLOB
    IS
        letter_body   CLOB;
        detailpa      procurement_action%ROWTYPE := NULL;
    BEGIN
        letters_pkg.pa_rec := NULL;
        letters_pkg.awardee_rec := NULL;
        --letters_pkg.awardee_p_rec:=null;
        letters_pkg.sga_rec := NULL;
        letters_pkg.contact_rec := NULL;
        letters_pkg.error_msg := '';
        letters_pkg.flag := 0;
        letters_pkg.letter_type := sga;
        detailpa := NULL;
        detailpa := letters_pkg.getdetailaction (p_action_id);

        IF (detailpa.action_id IS NOT NULL)
        THEN
            letters_pkg.get_pa_rec (detailpa.parent_action_id, p_aotr_id);

            IF (letters_pkg.pa_rec.action_id IS NOT NULL)
            THEN
                letters_pkg.get_awardee_rec (detailpa.awardee_code, NULL, 2);

                IF (letters_pkg.awardee_rec.awardee_code IS NULL)
                THEN
                    error_msg :=
                           error_msg
                        || 'Detail Awardee Code '
                        || letters_pkg.awardee_rec.awardee_code;
                    flag := 1;
                END IF;

                letters_pkg.awardee_rec := NULL;
                letters_pkg.get_awardee_rec (letters_pkg.pa_rec.awardee_code,
                                             p_awardee_add_seq,
                                             1);

                IF (letters_pkg.awardee_rec.awardee_code IS NOT NULL)
                THEN
                    letters_pkg.get_contact_rec (
                        p_awardee_contact_seq,
                        1,
                        letters_pkg.awardee_rec.awardee_code);
                ELSE
                    error_msg :=
                           error_msg
                        || 'Parent Awardee Code'
                        || letters_pkg.awardee_rec.awardee_code;
                    flag := 1;
                END IF;

                letters_pkg.get_subgrant_app_rec (p_action_id);

                IF (letters_pkg.sga_rec.letter_seq IS NULL)
                THEN
                    flag := 1;
                END IF;
            ELSE
                flag := 1;
            END IF;
        ELSE
            flag := 1;
        END IF;

        letter_body := NULL;

        IF (flag = 0)
        THEN
            letter_body :=
                REPLACE (
                       TO_CHAR (subgrant_app_rec_x.letter_date, date_format)
                    || CHR (10)
                    || CHR (10)
                    || contact_rec_x.contact_name
                    || CHR (10)
                    || awardee_p_rec_x.organization_name
                    || CHR (10)
                    || awardee_rec_x.organization_add
                    || CHR (10)
                    || CHR (10)
                    || '<b>Subject:</b> Approval to Award a Sub-award to '
                    || awardee_rec_x.organization_name
                    || ' under '
                    || pa_rec_x.funding_action_name
                    || ' Number '
                    || pa_rec_x.award_nbr
                    || ' '
                    || pa_rec_x.country_region
                    || ''
                    || CHR (10)
                    || CHR (10)
                    || '<b>Reference:</b>
(a)'
                    || contact_rec_x.contact_name
                    || ' ('
                    || awardee_p_rec_x.organization_name
                    || ')/'
                    || pa_rec_x.aotr_name
                    || ' (OFDA) '
                    || subgrant_app_rec_x.refa
                    || CHR (10)
                    || '(b) '
                    || subgrant_app_rec_x.refb
                    || CHR (10)
                    || CHR (10)
                    || 'Dear '
                    || contact_rec_x.salutation
                    || ':'
                    || CHR (10)
                    || CHR (10)
                    || 'This responds to reference (a) which requested approval to award a subaward to the '
                    || awardee_rec_x.organization_name
                    || ' under the subject award'
                    || ' in the amount of approximately '
                    || TO_CHAR (detailpa.approved_amt,
                                'FM$9,999,999,999,999.00')
                    || ' '
                    || subgrant_app_rec_x.purpose
                    || CHR (10)
                    || CHR (10)
                    || 'Specifically, '
                    || awardee_p_rec_x.organization_name
                    || '''s subaward to the '
                    || awardee_rec_x.organization_name
                    || ' will: '
                    || CHR (10)
                    || CHR (10)
                    || subgrant_app_rec_x.details
                    || CHR (10)
                    || CHR (10)
                    || CHR (10)
                    || 'As indicated in Attachment 1 The Schedule, 1.14 (C) entitled "Subrecipients and Subawards,"  of the above referenced '
                    || pa_rec_x.funding_action_name
                    || ', the Agreement Officer delegated authority to the AOR to approve the selection of sub-recipients and the substantial provisions of subawards, limited to technical and programmatic matters. In accordance with this delegation of authority and 2CFR 200.308, approval is hereby provided to enter into the proposed subaward with '
                    || awardee_rec_x.organization_name
                    || '.  The effective date of the subaward shall not be prior to '
                    || TO_CHAR (subgrant_app_rec_x.effective_date,
                                date_format)
                    || ', which pursuant to Section 1.2 of the basic award'
                    || ' is the date on or after which costs will be reimbursed.'
                    || CHR (10)
                    || CHR (10)
                    || 'The foregoing approval is provided with the explicit understanding that there are no changes to the contractual, administrative, or financial provisions outlined in the basic '
                    || 'award.  This approval is also provided with the explicit understanding that there are sufficient funds available in the subject '
                    || 'award for this subaward and that the total estimated amount or total obligated amount, whichever is less, of the subject award will not increase, or be exceeded, as a result thereof.'
                    || CHR (10)
                    || CHR (10)
                    || 'Furthermore, the foregoing approval does not constitute a determination of either the acceptability of the subaward terms or conditions (many of which must flow-down from the subject prime '
                    || 'award, or the allowability of any costs under the subaward, nor does it relieve '
                    || awardee_p_rec_x.organization_name
                    || ' of any responsibility for compliance with the terms and conditions of the subject '
                    || 'award, which as previously amended, remain unchanged and in full force and effect.'
                    || CHR (10)
                    || CHR (10)
                    || 'Please note that the Standard Provision entitled "Subagreements," imposes certain requirements on '
                    || awardee_p_rec_x.organization_name
                    || ', including the responsibility for determining that the '
                    || awardee_rec_x.organization_name
                    || ' is responsible and eligible for award of the subaward.  Note, too, that pursuant to paragraphs (e) and (g) of the Standard Provision entitled "Accounting, Audit, and Records," '
                    || awardee_p_rec_x.organization_name
                    || ' is responsible for monitoring the '
                    || awardee_rec_x.organization_name
                    || ' subaward, and the '
                    || awardee_rec_x.organization_name
                    || '/'
                    || awardee_p_rec_x.organization_name
                    || ' subaward must incorporate either paragraph (b) or paragraph (d) of the standard provision (amongst other standard provisions), as applicable.'
                    || CHR (10)
                    || CHR (10)
                    || 'Should you have any questions or concerns regarding this approval, please feel free to contact me at      '
                    || pa_rec_x.aotr_phone
                    || ' or '
                    || pa_rec_x.aotr_email
                    || '.'
                    || CHR (10)
                    || CHR (10)
                    || 'Sincerely,'
                    || CHR (10)
                    || CHR (10)
                    || CHR (10)
                    || pa_rec_x.aotr_name
                    || CHR (10)
                    || 'Agreement Officer''s Representative (AOR)
Office of U.S. Foreign Disaster Assistance
USAID/DCHA/OFDA'
                    || CHR (10)
                    || CHR (10)
                    || 'cc:
DCHA/OFDA/PS Grants Unit
USAID/OFDA Regional Advisor
',
                    '?',
                    CHR (46));
        END IF;


        IF (letter_body IS NULL)
        THEN
            letter_body := '' || error_msg || '';
            letter_body :=
                   '<b><u>Column Name: Error Message, Value</u></b>'
                || CHR (10)
                || letter_body;
        END IF;


        RETURN letter_body;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN 'Error in Generating Subgrant Approval Letter' || SQLERRM;
    END;

    --     function letter_ta_approval(p_action_id number,p_awardee_contact_seq
    FUNCTION letter_ta_approval (p_award_seq              NUMBER,
                                 p_awardee_contact_seq    NUMBER,
                                 p_awardee_add_seq        NUMBER,
                                 p_aotr_id                VARCHAR2,
                                 p_ta_approval_seq        NUMBER)
        RETURN CLOB
    IS
        letter_body    CLOB;
        taappdetails   VARCHAR2 (2000);
        detailpa       procurement_action%ROWTYPE := NULL;
    BEGIN
        letters_pkg.pa_rec := NULL;
        letters_pkg.award_rec := NULL;
        letters_pkg.awardee_rec := NULL;
        letters_pkg.contact_rec := NULL;
        letters_pkg.ta_rec := NULL;
        letters_pkg.error_msg := '';
        letters_pkg.flag := 0;
        letters_pkg.letter_type := ta;
        taappdetails := NULL;
        --detailpa:=null;
        --detailpa:=letters_pkg.getDetailAction(p_action_id);
        --if(detailpa.action_id is not null)then
        --letters_pkg.get_pa_rec(detailpa.parent_action_id, p_aotr_id);
        --- letters_pkg.get_pa_rec(p_action_id, p_aotr_id);
        letters_pkg.get_award_rec (p_award_seq, p_aotr_id);

        --    if(letters_pkg.pa_rec.action_id is not null)then
        IF (letters_pkg.award_rec.award_id IS NOT NULL)
        THEN
            letters_pkg.get_awardee_rec (letters_pkg.award_rec.awardee_code,
                                         p_awardee_add_seq,
                                         0);

            --         letters_pkg.get_awardee_rec(letters_pkg.pa_rec.awardee_code,p_awardee_add_seq,0);
            IF (letters_pkg.awardee_rec.awardee_code IS NOT NULL)
            THEN
                letters_pkg.get_contact_rec (
                    p_awardee_contact_seq,
                    0,
                    letters_pkg.awardee_rec.awardee_code);

                IF (letters_pkg.contact_rec.awardee_contact_seq IS NOT NULL)
                THEN
                    letters_pkg.get_ta_app_rec (p_ta_approval_seq);

                    --              letters_pkg.get_ta_app_rec(p_action_id);
                    ---              if(letters_pkg.ta_rec.action_id is not null) then
                    IF (letters_pkg.ta_rec.ta_approval_seq IS NOT NULL)
                    THEN
                        ---                taappdetails:=letters_pkg.getTAApprovalDetails(p_action_id);
                        taappdetails :=
                            letters_pkg.gettaapprovaldetails (
                                p_ta_approval_seq);

                        IF (taappdetails IS NULL)
                        THEN
                            flag := 1;
                        END IF;
                    ELSE
                        flag := 1;
                    END IF;
                ELSE
                    flag := 1;
                END IF;
            ELSE
                flag := 1;
            END IF;
        ELSE
            flag := 1;
        END IF;

        -- else
        --   flag:=1;
        --  end if;

        letter_body := NULL;

        IF (flag = 0)
        THEN
            letter_body :=
                REPLACE (
                       ''
                    || TO_CHAR (ta_rec_x.letter_date, date_format)
                    || CHR (10)
                    || CHR (10)
                    || contact_rec_x.contact_name
                    || CHR (10)
                    || awardee_rec_x.organization_name
                    || CHR (10)
                    || awardee_rec_x.organization_add
                    || CHR (10)
                    || '
<b>Subject:</b>    Consent of International Travel under Award No.'
                    || award_rec_x.award_nbr
                    || CHR (10)
                    || CHR (10)
                    || 'Reference:'
                    || CHR (10)
                    || '(a) '
                    || award_rec_x.award_nbr
                    || CHR (10)
                    || '(b) '
                    || contact_rec_x.contact_name
                    || ' Letter, SUBJECT '
                    || ta_rec_x.ref_subject
                    || ' dated '
                    || TO_CHAR (ta_rec_x.ref_date, date_format)
                    || CHR (10)
                    || CHR (10)
                    || 'Dear '
                    || contact_rec_x.salutation
                    || ':'
                    || CHR (10)
                    || CHR (10)
                    || 'This responds to correspondence from you, in which you requested consent in reference (b) above for international travel under Award referenced in (a) above.'
                    || CHR (10)
                    || CHR (10)
                    || '<b>Reason(s) for Request:</b>'
                    || CHR (10)
                    || ta_rec_x.purpose
                    || CHR (10)
                    || CHR (10)
                    || 'Pursuant to Section 1.4 of the referenced award, the undersigned hereby provides consent for the following international air travel '
                    || ta_rec_x.purpose
                    || ' outside the host country, subject to the other requirements of the standard provision entitled "Travel and International Air Transportation". '
                    || CHR (10)
                    || CHR (10)
                    || '<b>Number of Trips       Number of Individuals                   Itinerary</b>'
                    || CHR (10)
                    || taappdetails
                    || CHR (10)
                    || 'The foregoing consent is based on the explicit understanding that there are sufficient funds in the subject award to accommodate this travel, and that the total estimated amount or obligated amount, whichever is less, of the referenced award will not increase, or be exceeded, as a result thereof.  All other terms and conditions of the subject award remain unchanged and in full force and effect.'
                    || CHR (10)
                    || CHR (10)
                    || 'Sincerely,'
                    || CHR (10)
                    || CHR (10)
                    || CHR (10)
                    || award_rec_x.aotr_name
                    || CHR (10)
                    || 'Agreement Officer''s Representative (AOR)
Office of U.S. Foreign Disaster Assistance
USAID/DCHA/OFDA'
                    || CHR (10)
                    || CHR (10)
                    || 'cc:  DCHA/OFDA/PS Grants Unit',
                    '?',
                    CHR (46));
        END IF;


        IF (letter_body IS NULL)
        THEN
            letter_body := '' || error_msg || '';
            letter_body :=
                   '<b><u>Column Name: Error Message, Value</u></b>'
                || CHR (10)
                || letter_body;
        END IF;


        RETURN letter_body;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN 'Error in Generating International Travel Approval Letter'
                   || SQLERRM;
    END;

    FUNCTION get_aor_names (
        p_action_id     IN procurement_action.action_id%TYPE,
        p_is_proposed      NUMBER)
        RETURN aor_table
    IS
        l_aor_details   aor_table;
        l_current_fy    NUMBER;
    /**
    * 2 AOR
    * 1 Proposed and valid for INA, PAL only
    */
    BEGIN
        l_current_fy := get_fy ();

        IF p_is_proposed = 2
        THEN
SELECT
 aor_type(source
, val) BULK COLLECT INTO l_aor_details
FROM (
SELECT DISTINCT source  ,val
FROM(
SELECT action.action_id,action.mod_action_id,mod_action.action_id AS mod1_action_id
,mod_action.cto
,u_aor.userid AS aor
,mod_action.alternate_cto
,u_aaor.userid AS alternate_aor
,mod_action.proposed_aotr
,u_p_aor.userid AS proposed_aor
,mod_action.proposed_alternate_aotr
,u_p_aaor.userid AS proposed_alternateaor
FROM procurement_action action
INNER JOIN procurement_action award
ON(award.action_id=NVL(action.mod_action_id,action.action_id))
INNER JOIN procurement_action mod_action
ON(NVL(mod_action.mod_action_id,mod_action.action_id)=award.action_id
AND( (mod_action.budget_fy >= l_current_fy)
OR
(mod_action.budget_fy < l_current_fy AND (SELECT action_amt_status FROM pa_x WHERE action_id=mod_action.action_id)=5)
)
)
LEFT OUTER JOIN abacus_user u_aor
ON(u_aor.userid=mod_action.cto
AND u_aor.active='Y'
AND u_aor.is_cto='Y'
)
LEFT OUTER JOIN abacus_user u_aaor
ON(u_aaor.userid=mod_action.alternate_cto
AND u_aaor.active='Y'
AND u_aaor.is_cto='Y'
)
LEFT OUTER JOIN abacus_user u_p_aor
ON(u_p_aor.userid=mod_action.proposed_aotr
AND u_p_aor.active='Y'
AND u_p_aor.is_cto='Y'
)
LEFT OUTER JOIN abacus_user u_p_aaor
ON(u_p_aaor.userid=mod_action.proposed_alternate_aotr
AND u_p_aaor.active='Y'
AND u_p_aaor.is_cto='Y'
)
WHERE action.action_id=p_action_id)
UNPIVOT (val FOR(source) IN
(
aor AS 'AOR',
alternate_aor AS 'Alternate AOR',
proposed_aor AS 'Proposed AOR',
proposed_alternateaor AS 'Proposed Alternate AOR'
)
)) WHERE
((p_is_proposed=2 AND source NOT LIKE '%Proposed%' )

)           ;
        ELSIF p_is_proposed = 1
        THEN
SELECT
 aor_type(source
, val) BULK COLLECT INTO l_aor_details
FROM (
SELECT DISTINCT source  ,val
FROM(
SELECT action.action_id,action.mod_action_id,action.action_id AS mod1_action_id
,action.cto
,u_aor.userid AS aor
,action.alternate_cto
,u_aaor.userid AS alternate_aor
,action.proposed_aotr
,u_p_aor.userid AS proposed_aor
,action.proposed_alternate_aotr
,u_p_aaor.userid AS proposed_alternateaor
FROM procurement_action action

LEFT OUTER JOIN abacus_user u_aor
ON(u_aor.userid=action.cto
AND u_aor.active='Y'
AND u_aor.is_cto='Y'
)
LEFT OUTER JOIN abacus_user u_aaor
ON(u_aaor.userid=action.alternate_cto
AND u_aaor.active='Y'
AND u_aaor.is_cto='Y'
)
LEFT OUTER JOIN abacus_user u_p_aor
ON(u_p_aor.userid=action.proposed_aotr
AND u_p_aor.active='Y'
AND u_p_aor.is_cto='Y'
)
LEFT OUTER JOIN abacus_user u_p_aaor
ON(u_p_aaor.userid=action.proposed_alternate_aotr
AND u_p_aaor.active='Y'
AND u_p_aaor.is_cto='Y'
)
WHERE action.action_id=p_action_id)
UNPIVOT (val FOR(source) IN
(

proposed_aor AS 'Proposed AOR',
proposed_alternateaor AS 'Proposed Alternate AOR'
)
))          ;
        END IF;

        RETURN l_aor_details;
    END;

    PROCEDURE get_award_rec (p_award_seq NUMBER, p_aotr_id VARCHAR2)
    IS
    --NCE,TA      award_nbr


    --SGA         approved_amt
    --Parent Award_nbr     pa_parent_rec procurement_action%rowtype:=null;
    BEGIN
        SELECT   *
          INTO   award_rec
          FROM   award
         WHERE   award_seq = p_award_seq;

        IF (letter_type IN (ta))
        THEN
            --AOTR Name get Full Name and Phone no
            --check if the p_aotr_id is equal to either aotr or alt_aotr
            IF (p_aotr_id IS NOT NULL)
            THEN
                --                     if(p_aotr_id in (pa_rec.cto,pa_rec.alternate_cto))then
                IF (p_aotr_id IN (award_rec.aor, award_rec.alternate_aor))
                THEN
                    get_abacus_user_rec (p_aotr_id);

                    --Get aotr first name and last name
                    IF (abacus_user_rec.first_name IS NOT NULL
                        AND abacus_user_rec.last_name IS NOT NULL)
                    THEN
                        award_rec_x.aotr_name :=
                               abacus_user_rec.first_name
                            || ' '
                            || abacus_user_rec.last_name;
                    ELSE
                        error_msg :=
                            error_msg
                            || '<b>AOR Name:</b> First Name and Last Name cannot be null, '
                            || abacus_user_rec.first_name
                            || ' '
                            || abacus_user_rec.last_name
                            || CHR (10);
                        flag := 1;
                    END IF;

                    -- Get aotr phone number
                    IF (abacus_user_rec.phone_nbr IS NOT NULL)
                    THEN
                        award_rec_x.aotr_phone := abacus_user_rec.phone_nbr;
                    ELSE
                        error_msg :=
                            error_msg
                            || '<b>AOR Phone number:</b> Phone cannot be null, '
                            || abacus_user_rec.phone_nbr
                            || CHR (10);
                        flag := 1;
                    END IF;
                -- Get aotr email

                ELSE
                    error_msg :=
                           error_msg
                        || '<b>AOR:</b> Invalid AOR,'
                        || p_aotr_id
                        || '. Action has AOR='
                        || award_rec.aor
                        || ' and Alternate AOR='
                        || award_rec.alternate_aor
                        || '.'
                        || CHR (10);
                    flag := 1;
                END IF;
            ELSE
                error_msg :=
                       error_msg
                    || '<b>AOR:</b> ID cannot be null, '
                    || p_aotr_id
                    || CHR (10);
                flag := 1;
            END IF;
        END IF;                                    --end if for in(nce,ta,sga)

        IF (letter_type IN (ta))
        THEN
            --Award Number
            ---if(pa_rec.award_nbr is not null) then
            IF (award_rec.award_nbr IS NOT NULL)
            THEN
                ---   pa_rec_x.award_nbr:=pa_rec.award_nbr;
                award_rec_x.award_nbr := award_rec.award_nbr;
            ELSE
                error_msg :=
                       error_msg
                    || '<b>Award Number:</b> Award cannot be null, '
                    || award_rec.award_nbr
                    || CHR (10);
                flag := 1;
            END IF;
        END IF;                                            --end if for nce,ta
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            error_msg :=
                   error_msg
                || '</b>Action Id:</b> no data found in Action/Award, '
                || p_award_seq
                || CHR (10);
            --                '</b>Action Id:</b> no data found in Action, '||p_action_id||chr(10);
            flag := 1;
            NULL;
    END;
END;

/
