package gov.ofda.abacus.report.parameter.component.model.module;


import gov.ofda.abacus.report.parameter.component.model.base.ReportParameterViewObjectImpl;
import gov.ofda.abacus.report.parameter.component.model.module.common.ReportParameterModule;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.VariableValueManager;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewObject;
import oracle.jbo.server.ApplicationModuleImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu May 21 17:20:17 EDT 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ReportParameterModuleImpl extends ApplicationModuleImpl implements ReportParameterModule {

    /**
     * This is the default constructor (do not remove).
     */
    public ReportParameterModuleImpl() {
    }

    public String[] setDynamicView1(String lovName) {
        String rtn[] = { "Unknown", "false" };
        ReportParameterViewObjectImpl vod;
        vod = this.getDynamicLOV1();
        if (vod != null)
            vod.remove();
        try {

            vod =
                (ReportParameterViewObjectImpl) this.createViewObject("DynamicLOV1",
                                                                      "gov.ofda.abacus.report.parameter.component.model.lov." +
                                                                      lovName);
        } catch (Exception e) {
            vod =
                (ReportParameterViewObjectImpl) this.createViewObject("DynamicLOV1",
                                                                      "gov.ofda.abacus.report.parameter.component.model.lov.DynamicLOV");
            return rtn;
        }
        ViewCriteria vc = vod.getViewCriteria("DefaultCriteria");
        vod.removeApplyViewCriteriaName("DefaultCriteria");
        vod.applyViewCriteria(vc);
        vod.executeQuery();
        rtn[0] = lovName;
        rtn[1] = "true";
        return rtn;
    }

    public String[] setDynamicView(String lovName) {
        String rtn[] = { "Unknown", "false" };
        if (lovName != null && this.findViewObject(lovName) != null) {
            rtn[0] = lovName;
            rtn[1] = "true";
        }
        return rtn;
    }

    public String getLOVNameById(String lovName, String id) {
        ViewObject vo = this.findViewObject(lovName + "2");
        try {
            if (vo == null) {
                ReportParameterViewObjectImpl vod;
                vod =
                    (ReportParameterViewObjectImpl) this.createViewObject(lovName + "2",
                                                                          "gov.ofda.abacus.report.parameter.component.model.lov." +
                                                                          lovName);
                vod.removeApplyViewCriteriaName("findById");
                VariableValueManager vvmTableView = vod.ensureVariableManager();
                vvmTableView.setVariableValue("p_id", id);
                ViewCriteria vc = vod.getViewCriteria("findById");
                vod.applyViewCriteria(vc, false);
                vod.resetCriteria(vc);
                vod.executeQuery();
                RowSetIterator rsi = vod.getRowSetIterator();
                if (rsi.hasNext()) {
                    Row r = rsi.next();
                    String name = (String) r.getAttribute("Name");
                    return name;
                }
            } else {
                VariableValueManager vvm = vo.ensureVariableManager();
                vvm.setVariableValue("p_id", id);
                vo.executeQuery();
                RowSetIterator rsi = vo.createRowSetIterator(null);
                if (rsi.hasNext()) {
                    Row r = rsi.next();
                    String name = (String) r.getAttribute("Name");
                    return name;
                }
            }
        } catch (Exception e) {
            return null;
        }

        return null;
    }

    /**
     * Container's getter for DynamicLOV1.
     * @return DynamicLOV1
     */
    public ReportParameterViewObjectImpl getDynamicLOV1() {
        return (ReportParameterViewObjectImpl) findViewObject("DynamicLOV1");
    }

    /**
     * Container's getter for DynamicLOV2.
     * @return DynamicLOV2
     */
    public ReportParameterViewObjectImpl getDynamicLOV2() {
        return (ReportParameterViewObjectImpl) findViewObject("DynamicLOV2");
    }


    /**
     * Container's getter for ACTION_SEQ_NBR1.
     * @return ACTION_SEQ_NBR1
     */
    public ReportParameterViewObjectImpl getACTION_SEQ_NBR() {
        return (ReportParameterViewObjectImpl) findViewObject("ACTION_SEQ_NBR");
    }

    /**
     * Container's getter for ACTION_STATUS1.
     * @return ACTION_STATUS1
     */
    public ReportParameterViewObjectImpl getACTION_STATUS() {
        return (ReportParameterViewObjectImpl) findViewObject("ACTION_STATUS");
    }

    /**
     * Container's getter for ACTION_TYPE_CODE1.
     * @return ACTION_TYPE_CODE1
     */
    public ReportParameterViewObjectImpl getACTION_TYPE_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("ACTION_TYPE_CODE");
    }

    /**
     * Container's getter for ACTIVE_LOV1.
     * @return ACTIVE_LOV1
     */
    public ReportParameterViewObjectImpl getACTIVE_LOV() {
        return (ReportParameterViewObjectImpl) findViewObject("ACTIVE_LOV");
    }

    /**
     * Container's getter for ADMIN_LEVEL1.
     * @return ADMIN_LEVEL1
     */
    public ReportParameterViewObjectImpl getADMIN_LEVEL() {
        return (ReportParameterViewObjectImpl) findViewObject("ADMIN_LEVEL");
    }

    /**
     * Container's getter for ASSISTANCE_CODE1.
     * @return ASSISTANCE_CODE1
     */
    public ReportParameterViewObjectImpl getASSISTANCE_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("ASSISTANCE_CODE");
    }

    /**
     * Container's getter for AWARD_NBR1.
     * @return AWARD_NBR1
     */
    public ReportParameterViewObjectImpl getAWARD_NBR() {
        return (ReportParameterViewObjectImpl) findViewObject("AWARD_NBR");
    }

    /**
     * Container's getter for AWARD_STATUS1.
     * @return AWARD_STATUS1
     */
    public ReportParameterViewObjectImpl getAWARD_STATUS() {
        return (ReportParameterViewObjectImpl) findViewObject("AWARD_STATUS");
    }

    /**
     * Container's getter for AWARDEE_CODE1.
     * @return AWARDEE_CODE1
     */
    public ReportParameterViewObjectImpl getAWARDEE_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("AWARDEE_CODE");
    }

    /**
     * Container's getter for AWARDEE_TYPE_CODE1.
     * @return AWARDEE_TYPE_CODE1
     */
    public ReportParameterViewObjectImpl getAWARDEE_TYPE_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("AWARDEE_TYPE_CODE");
    }

    /**
     * Container's getter for BUDGET_FY1.
     * @return BUDGET_FY1
     */
    public ReportParameterViewObjectImpl getBUDGET_FY() {
        return (ReportParameterViewObjectImpl) findViewObject("BUDGET_FY");
    }

    /**
     * Container's getter for BUDGET_SOURCE1.
     * @return BUDGET_SOURCE1
     */
    public ReportParameterViewObjectImpl getBUDGET_SOURCE() {
        return (ReportParameterViewObjectImpl) findViewObject("BUDGET_SOURCE");
    }

    /**
     * Container's getter for COMMIT_FLAG1.
     * @return COMMIT_FLAG1
     */
    public ReportParameterViewObjectImpl getCOMMIT_FLAG() {
        return (ReportParameterViewObjectImpl) findViewObject("COMMIT_FLAG");
    }

    /**
     * Container's getter for COUNTRY_CODE1.
     * @return COUNTRY_CODE1
     */
    public ReportParameterViewObjectImpl getCOUNTRY_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("COUNTRY_CODE");
    }

    /**
     * Container's getter for CTO1.
     * @return CTO1
     */
    public ReportParameterViewObjectImpl getCTO() {
        return (ReportParameterViewObjectImpl) findViewObject("CTO");
    }

    /**
     * Container's getter for DD1.
     * @return DD1
     */
    public ReportParameterViewObjectImpl getDD() {
        return (ReportParameterViewObjectImpl) findViewObject("DD");
    }

    /**
     * Container's getter for DEFERRED_CODE1.
     * @return DEFERRED_CODE1
     */
    public ReportParameterViewObjectImpl getDEFERRED_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("DEFERRED_CODE");
    }

    /**
     * Container's getter for DISASTER_CODE1.
     * @return DISASTER_CODE1
     */
    public ReportParameterViewObjectImpl getDISASTER_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("DISASTER_CODE");
    }

    /**
     * Container's getter for DISASTER_FLAG1.
     * @return DISASTER_FLAG1
     */
    public ReportParameterViewObjectImpl getDISASTER_FLAG() {
        return (ReportParameterViewObjectImpl) findViewObject("DISASTER_FLAG");
    }

    /**
     * Container's getter for DOC_TYPE_CODE1.
     * @return DOC_TYPE_CODE1
     */
    public ReportParameterViewObjectImpl getDOC_TYPE_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("DOC_TYPE_CODE");
    }


    /**
     * Container's getter for FUND_CODE1.
     * @return FUND_CODE1
     */
    public ReportParameterViewObjectImpl getFUND_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("FUND_CODE");
    }

    /**
     * Container's getter for FUNDING_ACTION_TYPE1.
     * @return FUNDING_ACTION_TYPE1
     */
    public ReportParameterViewObjectImpl getFUNDING_ACTION_TYPE() {
        return (ReportParameterViewObjectImpl) findViewObject("FUNDING_ACTION_TYPE");
    }

    /**
     * Container's getter for IS_CHECKBOOK1.
     * @return IS_CHECKBOOK1
     */
    public ReportParameterViewObjectImpl getIS_CHECKBOOK() {
        return (ReportParameterViewObjectImpl) findViewObject("IS_CHECKBOOK");
    }

    /**
     * Container's getter for KEYWORD_CODE1.
     * @return KEYWORD_CODE1
     */
    public ReportParameterViewObjectImpl getKEYWORD_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("KEYWORD_CODE");
    }

    /**
     * Container's getter for OFDA_DIVISION_CODE1.
     * @return OFDA_DIVISION_CODE1
     */
    public ReportParameterViewObjectImpl getOFDA_DIVISION_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("OFDA_DIVISION_CODE");
    }

    /**
     * Container's getter for OFDA_TEAM_CODE1.
     * @return OFDA_TEAM_CODE1
     */
    public ReportParameterViewObjectImpl getOFDA_TEAM_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("OFDA_TEAM_CODE");
    }

    /**
     * Container's getter for PA_LOCATION_CODE1.
     * @return PA_LOCATION_CODE1
     */
    public ReportParameterViewObjectImpl getPA_LOCATION_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("PA_LOCATION_CODE");
    }

    /**
     * Container's getter for PARENT_ACTION_ID1.
     * @return PARENT_ACTION_ID1
     */
    public ReportParameterViewObjectImpl getPARENT_ACTION_ID() {
        return (ReportParameterViewObjectImpl) findViewObject("PARENT_ACTION_ID");
    }

    /**
     * Container's getter for PLANNED_QTR1.
     * @return PLANNED_QTR1
     */
    public ReportParameterViewObjectImpl getPLANNED_QTR() {
        return (ReportParameterViewObjectImpl) findViewObject("PLANNED_QTR");
    }

    /**
     * Container's getter for PRIORITY_CODE1.
     * @return PRIORITY_CODE1
     */
    public ReportParameterViewObjectImpl getPRIORITY_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("PRIORITY_CODE");
    }

    /**
     * Container's getter for PROJECT_NBR1.
     * @return PROJECT_NBR1
     */
    public ReportParameterViewObjectImpl getPROJECT_NBR() {
        return (ReportParameterViewObjectImpl) findViewObject("PROJECT_NBR");
    }

    /**
     * Container's getter for REGION_CODE1.
     * @return REGION_CODE1
     */
    public ReportParameterViewObjectImpl getREGION_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("REGION_CODE");
    }

    /**
     * Container's getter for REJECT_CODE1.
     * @return REJECT_CODE1
     */
    public ReportParameterViewObjectImpl getREJECT_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("REJECT_CODE");
    }

    /**
     * Container's getter for REQUESTER1.
     * @return REQUESTER1
     */
    public ReportParameterViewObjectImpl getREQUESTER() {
        return (ReportParameterViewObjectImpl) findViewObject("REQUESTER");
    }

    /**
     * Container's getter for RESTRICTED_GOODS1.
     * @return RESTRICTED_GOODS1
     */
    public ReportParameterViewObjectImpl getRESTRICTED_GOODS() {
        return (ReportParameterViewObjectImpl) findViewObject("RESTRICTED_GOODS");
    }

    /**
     * Container's getter for SECTOR_CODE1.
     * @return SECTOR_CODE1
     */
    public ReportParameterViewObjectImpl getSECTOR_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("SECTOR_CODE");
    }

    /**
     * Container's getter for SPECIALIST1.
     * @return SPECIALIST1
     */
    public ReportParameterViewObjectImpl getSPECIALIST() {
        return (ReportParameterViewObjectImpl) findViewObject("SPECIALIST");
    }

    /**
     * Container's getter for SUB_ASSISTANCE_CODE1.
     * @return SUB_ASSISTANCE_CODE1
     */
    public ReportParameterViewObjectImpl getSUB_ASSISTANCE_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("SUB_ASSISTANCE_CODE");
    }

    /**
     * Container's getter for SUBSECTOR_CODE1.
     * @return SUBSECTOR_CODE1
     */
    public ReportParameterViewObjectImpl getSUBSECTOR_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("SUBSECTOR_CODE");
    }

    /**
     * Container's getter for QUARTER1.
     * @return QUARTER1
     */
    public ReportParameterViewObjectImpl getQUARTER() {
        return (ReportParameterViewObjectImpl) findViewObject("QUARTER");
    }

    /**
     * Container's getter for MANAGEMENT_CODE1.
     * @return MANAGEMENT_CODE1
     */
    public ReportParameterViewObjectImpl getMANAGEMENT_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("MANAGEMENT_CODE");
    }

    /**
     * Container's getter for INDICATOR_CODE1.
     * @return INDICATOR_CODE1
     */
    public ReportParameterViewObjectImpl getINDICATOR_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("INDICATOR_CODE");
    }

    /**
     * Container's getter for TAG_ID1.
     * @return TAG_ID1
     */
    public ReportParameterViewObjectImpl getTAG_ID() {
        return (ReportParameterViewObjectImpl) findViewObject("TAG_ID");
    }

    /**
     * Container's getter for PG_REV_ID1.
     * @return PG_REV_ID1
     */
    public ReportParameterViewObjectImpl getPG_REV_ID() {
        return (ReportParameterViewObjectImpl) findViewObject("PG_REV_ID");
    }

    /**
     * Container's getter for PROJECT_PRIORITY_ID.
     * @return PROJECT_PRIORITY_ID
     */
    public ReportParameterViewObjectImpl getPROJECT_PRIORITY_ID() {
        return (ReportParameterViewObjectImpl) findViewObject("PROJECT_PRIORITY_ID");
    }

    /**
     * Container's getter for BUREAU_CODE.
     * @return BUREAU_CODE
     */
    public ReportParameterViewObjectImpl getBUREAU_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("BUREAU_CODE");
    }

    /**
     * Container's getter for OFFICE_CODE.
     * @return OFFICE_CODE 
     */
    public ReportParameterViewObjectImpl getOFFICE_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("OFFICE_CODE");
    }

    /**
     * Container's getter for PROJECT_TYPE_CODE.
     * @return PROJECT_TYPE_CODE
     */
    public ReportParameterViewObjectImpl getPROJECT_TYPE_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("PROJECT_TYPE_CODE");
    }

    /**
     * Container's getter for PROJECT_TRACKER_ID.
     * @return PROJECT_TRACKER_ID
     */
    public ReportParameterViewObjectImpl getPROJECT_TRACKER_ID() {
        return (ReportParameterViewObjectImpl) findViewObject("PROJECT_TRACKER_ID");
    }

    /**
     * Container's getter for FUND_ACCT_TYPE_CODE.
     * @return FUND_ACCT_TYPE_CODE
     */
    public ReportParameterViewObjectImpl getFUND_ACCT_TYPE_CODE() {
        return (ReportParameterViewObjectImpl) findViewObject("FUND_ACCT_TYPE_CODE");
    }
		
    /**
     * Container's getter for PAX_ACTIVITIES_ID1.
     * @return PAX_ACTIVITIES_ID1
     */
    public ReportParameterViewObjectImpl getPAX_ACTIVITIES_ID() {
        return (ReportParameterViewObjectImpl) findViewObject("PAX_ACTIVITIES_ID");
    }

    /**
     * Container's getter for PROGRAM_TYPE_ID1.
     * @return PROGRAM_TYPE_ID1
     */
    public ReportParameterViewObjectImpl getPROGRAM_TYPE_ID() {
        return (ReportParameterViewObjectImpl) findViewObject("PROGRAM_TYPE_ID");
    }

    /**
     * Container's getter for MODALITY_ID1.
     * @return MODALITY_ID1
     */
    public ReportParameterViewObjectImpl getMODALITY_ID() {
        return (ReportParameterViewObjectImpl) findViewObject("MODALITY_ID");
    }

    /**
     * Container's getter for CONTRIBUTION_LINK_TYPE_ID1.
     * @return CONTRIBUTION_LINK_TYPE_ID1
     */
    public ReportParameterViewObjectImpl getCONTRIBUTION_LINKTYPE_ID() {
        return (ReportParameterViewObjectImpl) findViewObject("CONTRIBUTION_LINKTYPE_ID");
    }

    /**
     * Container's getter for COMMENT_TYPE_ID1.
     * @return COMMENT_TYPE_ID1
     */
    public ReportParameterViewObjectImpl getCOMMENT_TYPE_ID() {
        return (ReportParameterViewObjectImpl) findViewObject("COMMENT_TYPE_ID");
    }

    /**
     * Container's getter for COMMENT_TECH_AREA_ID1.
     * @return COMMENT_TECH_AREA_ID1
     */
    public ReportParameterViewObjectImpl getCOMMENT_TECH_AREA_ID() {
        return (ReportParameterViewObjectImpl) findViewObject("COMMENT_TECH_AREA_ID");
    }

    /**
     * Container's getter for APPL_REVIEW_STATUS_ID1.
     * @return APPL_REVIEW_STATUS_ID1
     */
    public ReportParameterViewObjectImpl getAPPL_REVIEW_STATUS_ID() {
        return (ReportParameterViewObjectImpl) findViewObject("APPL_REVIEW_STATUS_ID");
    }

    /**
     * Container's getter for APPL_REVIEW_STATUS_DETAILS_ID1.
     * @return APPL_REVIEW_STATUS_DETAILS_ID1
     */
    public ReportParameterViewObjectImpl getAPPL_REVIEW_STATUS_DETAILS_ID() {
        return (ReportParameterViewObjectImpl) findViewObject("APPL_REVIEW_STATUS_DETAILS_ID");
    }
}

