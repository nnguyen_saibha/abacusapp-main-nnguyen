package gov.ofda.abacus.report.parameter.component.model.base;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Iterator;

import java.util.Map;

import oracle.jbo.JboException;
import oracle.jbo.Row;
import oracle.jbo.server.Entity;
import oracle.jbo.server.ViewDefImpl;
import oracle.jbo.server.ViewObjectImpl;

import org.codehaus.groovy.runtime.InvokerHelper;

public class ReportParameterViewObjectImpl  extends ViewObjectImpl {
    private Integer currentFY;
    public ReportParameterViewObjectImpl() {
        super();
    }
    public Integer getCurrentFY() {
        if(currentFY==null) {
            PreparedStatement st = null;
            ResultSet rs=null;
            try {
                st = getDBTransaction().createPreparedStatement("select get_fy() from dual", 0);
                rs=st.executeQuery();
                if(rs.next()) {
                    currentFY=rs.getInt(1);
                }
                rs.close();
                st.close();
            } catch (SQLException e) {
                throw new JboException(e);
            }
        }
        return currentFY;
    }
    //http://adfpractice-fedor.blogspot.com/2012/05/working-with-vos-built-in-aggregation.html
    private class AgrFuncHelper extends HashMap {
     private String funcName;
     public AgrFuncHelper(String funcName) {
         super();
         this.funcName=funcName;
     }
     public Object get(Object key) 
     {
       Object o=InvokerHelper.invokeMethod(getDefaultRowSet(), funcName, key);
       return o;
     }

     }
     public Map getSum() 
     {

       Map tmp=new AgrFuncHelper("sum");
       
       return tmp;
     }
}


