package gov.ofda.abacus.report.parameter.component.view;

import gov.ofda.abacus.component.AbacusReportParameterLOV;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.RichQuery;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.fragment.RichDeclarativeComponent;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;
import oracle.adf.view.rich.model.QueryDescriptor;
import oracle.adf.view.rich.model.QueryModel;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.component.UIXSwitcher;

public class AbacusReportParameterBean {
    private RichInputText inputListText;
    private RichQuery paramQuery;
    private RichPopup lovPopup;
    private RichPopup textPopup;
    private RichTable searchResultsTable;

    public AbacusReportParameterBean() {
    }

    public void setInputListText(RichInputText inputListText) {
        this.inputListText = inputListText;
    }

    public RichInputText getInputListText() {
        return inputListText;
    }

    /**
     * Called when user click on search magnifier icon. This will set the dynamic lov and setup popup for multi select if applicable
     * @return
     */
    public String initializePopup() {
        Map viewScopeMap = ADFContext.getCurrent().getViewScope();
        AbacusReportParameterLOV rdc = (AbacusReportParameterLOV) getValueObject("#{comp}", RichDeclarativeComponent.class);
        String lovName = (String) rdc.getAttributes().get("lovName");
        String expression = (String) rdc.getAttributes().get("expression");
        viewScopeMap.put("VOParameterName", rdc.getAttributes().get("lovName"));
        //Set Dynamic view using LOV Name 
        OperationBinding exec=null;
        String rtn[]=null;
        try
        {
         exec= this.getBindings().getOperationBinding("setDynamicView");
        exec.getParamsMap().put("lovName", lovName);
        rtn= (String[]) exec.execute();
        }
        catch(Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Page Expired",  "Due to inactivity this report page expired. To use this report, open another report and come back to this report."));                
            return null;
        }
        //Check if View exists. If not then show input text dummy popup
        if ( rtn == null || rtn.length != 2 || !"true".equals(rtn[1]))
        {
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            this.getTextPopup().show(hints);
            return null;
        }
        //View with LovName exists. Checek if expression is "IN". If so then initialize popup for multi select. 
        if ("IN".equals(expression)) {
            List<ReportParameter> selectedReportParamList =
                (List<ReportParameter>) viewScopeMap.get("selectedReportParamList");
            //If new then create new param list. If existing then skip new
            if (selectedReportParamList == null || !lovName.equals(viewScopeMap.get("lovNameCheck"))) {
                selectedReportParamList = new ArrayList<ReportParameter>();
                viewScopeMap.put("lovNameCheck", lovName);
            }
            
            Object inputValue = this.getInputListText().getValue();
            //Split input value by comma and iterate through each value to find name
            if (inputValue != null) {
                String tmp = inputValue.toString().toUpperCase();
                String t[] = tmp.split(",");
                List<ReportParameter> validList=new ArrayList<ReportParameter>();
                //Iterating through all input values entered by user
                for (int i = 0; i < t.length; i++) {
                    ReportParameter rpTmp=new ReportParameter(t[i],"<span style=\"color:Maroon\">**No Match**</span>");
                    validList.add(rpTmp);
                    //check if value already exists in the list. If not get name by id and set name
                    if (!selectedReportParamList.contains(rpTmp)) {
                            exec = this.getBindings().getOperationBinding("getLOVNameById");
                            exec.getParamsMap().put("id", t[i]);
                            exec.execute();
                            String name = (String) exec.getResult();
                            if (name != null)
                                rpTmp.setName(name);
                        selectedReportParamList.add(rpTmp);
                    }
                }
                //Remove any residual values from the parameter list that might be there from previous call
                selectedReportParamList.retainAll(validList);
            }
            //Clear selected list if input value is null
            else {
                selectedReportParamList.clear();
            }
            viewScopeMap.put("selectedReportParamList", selectedReportParamList);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(rdc);
        //Reset Query component
        if (paramQuery != null) {
            QueryModel queryModel = paramQuery.getModel();
            QueryDescriptor queryDescriptor = paramQuery.getValue();
            queryModel.reset(queryDescriptor);
            paramQuery.refresh(FacesContext.getCurrentInstance());
        }
        //http://andrejusb.blogspot.com/2013/03/adf-generator-for-dynamic-adf-bc-and.html
        BindingContainer bindings = getBindings();
        DCIteratorBinding iter = (DCIteratorBinding) bindings.get("DynamicLOV1Iterator");
        iter.clearForRecreate();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getLovPopup().show(hints);
        return null;
    }
    public DCBindingContainer getBindings() {
        return (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    /**
     * Called when user clicks on OK/Cancel for search popup. Depending on parameter expression, set input value.
     * @param de

     */
    public void popupDialogLsnr(DialogEvent de) {
        AbacusReportParameterLOV rdc = (AbacusReportParameterLOV) getValueObject("#{comp}", RichDeclarativeComponent.class);
        String expression = (String) rdc.getAttributes().get("expression");
        //Check if outcome is ok
        if (de.getOutcome() == DialogEvent.Outcome.ok) {
            //Check if expression is not IN and set Current row Id to input value
            if (!"IN".equals(expression)) {
                DCBindingContainer bindings =
                    (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                DCIteratorBinding iter = bindings.findIteratorBinding("DynamicLOV1Iterator");
                Row r = iter.getCurrentRow();
                if (r != null) {
                    this.getInputListText().setValue(r.getAttribute("Id"));
                    AdfFacesContext.getCurrentInstance().addPartialTarget(this.getInputListText());
                }
            } else {
                //IN expression. Get selected list and convert to comma separated values
                Map viewScopeMap = ADFContext.getCurrent().getViewScope();
                List<ReportParameter> selectedReportParamList =
                    (List<ReportParameter>) viewScopeMap.get("selectedReportParamList");
                if (selectedReportParamList != null) {
                    StringBuilder tmp = new StringBuilder();
                    for (int i = 0; i < selectedReportParamList.size(); i++) {
                        ReportParameter rpTmp=selectedReportParamList.get(i);
                        tmp.append(rpTmp.getId());
                        if (i + 1 < selectedReportParamList.size())
                            tmp.append(",");
                    }
                    this.getInputListText().setValue(tmp.toString());
                    AdfFacesContext.getCurrentInstance().addPartialTarget(this.getInputListText());

                }

            }

        }
    }

    public void setParamQuery(RichQuery paramQuery) {
        this.paramQuery = paramQuery;
    }

    public RichQuery getParamQuery() {
        return paramQuery;
    }

    /**
     * Used by library for multi select add/remove icon to show Add or delete icon
     * @param list
     * @param iCode
     * @return
     
     */
    public static boolean contains(List<Object> list, Object iCode) {
        ReportParameter rpTmp=new ReportParameter((String)iCode,"");
        boolean b = false;
        if (list != null)
            b = list.contains(rpTmp);
        return b;
    }

    /**
     * Used by library for column width. Will get display height and width for attributeName and return height*width for display width
     * @param attributeName
     * @return
     */
    public static Integer getWidth(String attributeName) {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    ELContext elContext = facesContext.getELContext();
    ExpressionFactory expressionFactory =facesContext.getApplication().getExpressionFactory();
    ValueExpression exp1 = expressionFactory.createValueExpression(elContext, "#{attrs.treeBinding.hints."+attributeName+".displayWidth}",Object.class);
    ValueExpression exp2 = expressionFactory.createValueExpression(elContext, "#{attrs.treeBinding.hints."+attributeName+".displayHeight}",Object.class);
    return (Integer) exp1.getValue(elContext)*(Integer)exp2.getValue(elContext);

}

    /**
     * Will add passed Id/Name to selected list
     * @return
     */
    public String addToList() {
        Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String rowId = requestScopeMap.get("rowId").toString();
        Object rowName = requestScopeMap.get("rowName");
        if(rowName==null) {
            rowName=rowId;
        }
        Map viewScopeMap = ADFContext.getCurrent().getViewScope();

        List<ReportParameter> selectedReportParamList =
            (List<ReportParameter>) viewScopeMap.get("selectedReportParamList");
        selectedReportParamList.add(new ReportParameter(rowId, rowName.toString()));
        return null;
    }

    /**
     * Will removed passed id from selected list
     * @return
     */
    public String removeFromList() {
        Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String rowId = requestScopeMap.get("rowId").toString();
        Map viewScopeMap = ADFContext.getCurrent().getViewScope();
        List<ReportParameter> selectedReportParamList =
            (List<ReportParameter>) viewScopeMap.get("selectedReportParamList");
        selectedReportParamList.remove(new ReportParameter(rowId, ""));
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSearchResultsTable());
        return null;
    }
    //Just a helper method for EL evaluation
    public static Object getValueObject(String expr, Class returnType) {
        FacesContext fc = FacesContext.getCurrentInstance();
        ELContext elctx = fc.getELContext();
        ExpressionFactory elFactory = fc.getApplication().getExpressionFactory();
        ValueExpression valueExpr = elFactory.createValueExpression(elctx, expr, returnType);
        return valueExpr.getValue(elctx);
    }

    public void setLovPopup(RichPopup lovPopup) {
        this.lovPopup = lovPopup;
    }

    public RichPopup getLovPopup() {
        return lovPopup;
    }

    public void setTextPopup(RichPopup textPopup) {
        this.textPopup = textPopup;
    }

    public RichPopup getTextPopup() {
        return textPopup;
    }

    /**
     * Called when user clicks on Add/Remove from available list. 
     * Will check if passed Id is part of list if so then removes from list, otherwise will add to list
     * @param ae
     */
    public void addOrRemoveItem(ActionEvent ae) {
        Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String rowId = requestScopeMap.get("rowId").toString();
        Object rowName = requestScopeMap.get("rowName");
         if(rowName==null) {
            rowName=rowId;
        }
        Map viewScopeMap = ADFContext.getCurrent().getViewScope();

        List<ReportParameter> selectedReportParamList =
            (List<ReportParameter>) viewScopeMap.get("selectedReportParamList");
        ReportParameter rpTmp=new ReportParameter(rowId, rowName.toString());
        if(selectedReportParamList.contains(rpTmp))
            selectedReportParamList.remove(rpTmp);
        else
            selectedReportParamList.add(rpTmp);
        AdfFacesContext.getCurrentInstance().addPartialTarget(ae.getComponent());
    }

    public void setSearchResultsTable(RichTable searchResultsTable) {
        this.searchResultsTable = searchResultsTable;
    }

    public RichTable getSearchResultsTable() {
        return searchResultsTable;
    }
}
