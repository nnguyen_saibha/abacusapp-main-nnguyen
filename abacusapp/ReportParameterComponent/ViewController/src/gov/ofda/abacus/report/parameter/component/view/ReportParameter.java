package gov.ofda.abacus.report.parameter.component.view;

import java.io.Serializable;

public class ReportParameter implements Serializable {
    @SuppressWarnings("compatibility:5537207366127399605")
    private static final long serialVersionUID = 1L;
    private String id;
    private String name;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ReportParameter(String id,String name) {
        super();
        this.id=id;
        this.name=name;
    }
    public boolean equals(Object obj) {
      if (obj instanceof ReportParameter)
          if(((ReportParameter)obj).getId().equals(this.getId()))
              return true;
      return false;
    }
}

