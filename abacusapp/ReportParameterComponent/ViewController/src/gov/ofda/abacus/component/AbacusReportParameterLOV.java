package gov.ofda.abacus.component;

import oracle.adf.view.rich.component.rich.fragment.RichDeclarativeComponent;

public class AbacusReportParameterLOV extends RichDeclarativeComponent {
    private String isSearchEnabled="false";

    public void setIsSearchEnabled(String isSearchEnabled) {
        this.isSearchEnabled = isSearchEnabled;
    }

    public String getIsSearchEnabled() {
        return isSearchEnabled;
    }

    public AbacusReportParameterLOV() {
    }
}
