package gov.ofda.abacus.model.abacusapp.action.view;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;

import java.math.BigDecimal;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Aug 03 17:38:32 EDT 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ActionInvolvedUsersViewRowImpl extends AbacusViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ActionId,
        Requester,
        ProgMgrId,
        Specialist,
        ContractOfficer,
        Oaa,
        DistributionList,
        Cto,
        AlternateCto,
        ProposedAotr,
        ProposedAlternateAotr,
        PrSpecialist,
        UpdId,
        AwardeeCode,
        AwardeeTypeCode,
        PkgCompleteEmail,
        TeamLead,
        ProjectName,
        AwardeeName,
        ApplReviewLead;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ACTIONID = AttributesEnum.ActionId.index();
    public static final int REQUESTER = AttributesEnum.Requester.index();
    public static final int PROGMGRID = AttributesEnum.ProgMgrId.index();
    public static final int SPECIALIST = AttributesEnum.Specialist.index();
    public static final int CONTRACTOFFICER = AttributesEnum.ContractOfficer.index();
    public static final int OAA = AttributesEnum.Oaa.index();
    public static final int DISTRIBUTIONLIST = AttributesEnum.DistributionList.index();
    public static final int CTO = AttributesEnum.Cto.index();
    public static final int ALTERNATECTO = AttributesEnum.AlternateCto.index();
    public static final int PROPOSEDAOTR = AttributesEnum.ProposedAotr.index();
    public static final int PROPOSEDALTERNATEAOTR = AttributesEnum.ProposedAlternateAotr.index();
    public static final int PRSPECIALIST = AttributesEnum.PrSpecialist.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int AWARDEECODE = AttributesEnum.AwardeeCode.index();
    public static final int AWARDEETYPECODE = AttributesEnum.AwardeeTypeCode.index();
    public static final int PKGCOMPLETEEMAIL = AttributesEnum.PkgCompleteEmail.index();
    public static final int TEAMLEAD = AttributesEnum.TeamLead.index();
    public static final int PROJECTNAME = AttributesEnum.ProjectName.index();
    public static final int AWARDEENAME = AttributesEnum.AwardeeName.index();
    public static final int APPLREVIEWLEAD = AttributesEnum.ApplReviewLead.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ActionInvolvedUsersViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute ActionId.
     * @return the ActionId
     */
    public BigDecimal getActionId() {
        return (BigDecimal) getAttributeInternal(ACTIONID);
    }

    /**
     * Gets the attribute value for the calculated attribute Requester.
     * @return the Requester
     */
    public String getRequester() {
        return (String) getAttributeInternal(REQUESTER);
    }

    /**
     * Gets the attribute value for the calculated attribute ProgMgrId.
     * @return the ProgMgrId
     */
    public String getProgMgrId() {
        return (String) getAttributeInternal(PROGMGRID);
    }

    /**
     * Gets the attribute value for the calculated attribute Specialist.
     * @return the Specialist
     */
    public String getSpecialist() {
        return (String) getAttributeInternal(SPECIALIST);
    }

    /**
     * Gets the attribute value for the calculated attribute ContractOfficer.
     * @return the ContractOfficer
     */
    public String getContractOfficer() {
        return (String) getAttributeInternal(CONTRACTOFFICER);
    }

    /**
     * Gets the attribute value for the calculated attribute Oaa.
     * @return the Oaa
     */
    public String getOaa() {
        return (String) getAttributeInternal(OAA);
    }

    /**
     * Gets the attribute value for the calculated attribute DistributionList.
     * @return the DistributionList
     */
    public String getDistributionList() {
        return (String) getAttributeInternal(DISTRIBUTIONLIST);
    }

    /**
     * Gets the attribute value for the calculated attribute Cto.
     * @return the Cto
     */
    public String getCto() {
        return (String) getAttributeInternal(CTO);
    }

    /**
     * Gets the attribute value for the calculated attribute AlternateCto.
     * @return the AlternateCto
     */
    public String getAlternateCto() {
        return (String) getAttributeInternal(ALTERNATECTO);
    }

    /**
     * Gets the attribute value for the calculated attribute ProposedAotr.
     * @return the ProposedAotr
     */
    public String getProposedAotr() {
        return (String) getAttributeInternal(PROPOSEDAOTR);
    }


    /**
     * Gets the attribute value for the calculated attribute ProposedAlternateAotr.
     * @return the ProposedAlternateAotr
     */
    public String getProposedAlternateAotr() {
        return (String) getAttributeInternal(PROPOSEDALTERNATEAOTR);
    }


    /**
     * Gets the attribute value for the calculated attribute PrSpecialist.
     * @return the PrSpecialist
     */
    public String getPrSpecialist() {
        return (String) getAttributeInternal(PRSPECIALIST);
    }

    /**
     * Gets the attribute value for the calculated attribute UpdId.
     * @return the UpdId
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Gets the attribute value for the calculated attribute AwardeeCode.
     * @return the AwardeeCode
     */
    public String getAwardeeCode() {
        return (String) getAttributeInternal(AWARDEECODE);
    }

    /**
     * Gets the attribute value for the calculated attribute AwardeeTypeCode.
     * @return the AwardeeTypeCode
     */
    public String getAwardeeTypeCode() {
        return (String) getAttributeInternal(AWARDEETYPECODE);
    }

    /**
     * Gets the attribute value for the calculated attribute PkgCompleteEmail.
     * @return the PkgCompleteEmail
     */
    public String getPkgCompleteEmail() {
        return (String) getAttributeInternal(PKGCOMPLETEEMAIL);
    }

    /**
     * Gets the attribute value for the calculated attribute TeamLead.
     * @return the TeamLead
     */
    public String getTeamLead() {
        return (String) getAttributeInternal(TEAMLEAD);
    }

    /**
     * Gets the attribute value for the calculated attribute ProjectName.
     * @return the ProjectName
     */
    public String getProjectName() {
        return (String) getAttributeInternal(PROJECTNAME);
    }

    /**
     * Gets the attribute value for the calculated attribute AwardeeName.
     * @return the AwardeeName
     */
    public String getAwardeeName() {
        return (String) getAttributeInternal(AWARDEENAME);
    }

    /**
     * Gets the attribute value for the calculated attribute ApplReviewLead.
     * @return the ApplReviewLead
     */
    public String getApplReviewLead() {
        return (String) getAttributeInternal(APPLREVIEWLEAD);
    }
}

