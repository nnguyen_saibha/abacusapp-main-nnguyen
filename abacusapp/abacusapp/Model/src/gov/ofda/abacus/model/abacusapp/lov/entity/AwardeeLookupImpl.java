package gov.ofda.abacus.model.abacusapp.lov.entity;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;

import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;

import oracle.jbo.Key;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.TransactionEvent;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Feb 10 09:55:45 EST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AwardeeLookupImpl extends AbacusEntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        AwardeeCode,
        AwardeeName,
        AwardeeTypeCode,
        AwardeeAcronym,
        DunsNbr,
        TaxIdNbr,
        UpdId,
        UpdDate,
        AwardeeComments,
        LetterOfCredit,
        WebSite,
        Active,
        AwardeeContacts,
        AwardeeAddress;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int AWARDEECODE = AttributesEnum.AwardeeCode.index();
    public static final int AWARDEENAME = AttributesEnum.AwardeeName.index();
    public static final int AWARDEETYPECODE = AttributesEnum.AwardeeTypeCode.index();
    public static final int AWARDEEACRONYM = AttributesEnum.AwardeeAcronym.index();
    public static final int DUNSNBR = AttributesEnum.DunsNbr.index();
    public static final int TAXIDNBR = AttributesEnum.TaxIdNbr.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int AWARDEECOMMENTS = AttributesEnum.AwardeeComments.index();
    public static final int LETTEROFCREDIT = AttributesEnum.LetterOfCredit.index();
    public static final int WEBSITE = AttributesEnum.WebSite.index();
    public static final int ACTIVE = AttributesEnum.Active.index();
    public static final int AWARDEECONTACTS = AttributesEnum.AwardeeContacts.index();
    public static final int AWARDEEADDRESS = AttributesEnum.AwardeeAddress.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AwardeeLookupImpl() {
    }

    /**
     * @param awardeeCode key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(String awardeeCode) {
        return new Key(new Object[] { awardeeCode });
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("gov.ofda.abacus.model.abacusapp.lov.entity.AwardeeLookup");
    }


    @Override
    protected void prepareForDML(int i, TransactionEvent transactionEvent) {
        if(i==DML_UPDATE || i==DML_INSERT)
        {
        ADFContext adfCtx = ADFContext.getCurrent();  
        SecurityContext secCntx = adfCtx.getSecurityContext();  
        String user = secCntx.getUserName(); 
        if(user!=null)
            setAttributeInternal(UPDID,user.toUpperCase());
        }
        super.prepareForDML(i, transactionEvent);
    }
}

