package gov.ofda.abacus.model.abacusapp.view;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;
import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;
import gov.ofda.abacus.model.abacusapp.entity.AbacusDocsImpl;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.domain.BlobDomain;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Dec 08 15:19:47 EST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AwardDocEditViewRowImpl extends AbacusViewRowImpl {
    public static final int ENTITY_AWARDDOC = 0;
    public static final int ENTITY_ABACUSDOCS = 1;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        AwardId,
        DocId,
        Comments,
        UpdId,
        UpdDate,
        AwardSeq,
        DocId1,
        DocLocation,
        DocTypeCode,
        DocUrl,
        FileContents,
        FileDesc,
        FileFormat,
        FileName,
        OrgFileName,
        ReferenceId,
        UpdDate1,
        UpdId1,
        mDocTypeCode,
        MdocTypeCode1,
        MegaDocDocTypeCodeLinkLOV1,
        MegDocLOV1,
        MegaDocDocTypeCodeLinkLOV2;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int AWARDID = AttributesEnum.AwardId.index();
    public static final int DOCID = AttributesEnum.DocId.index();
    public static final int COMMENTS = AttributesEnum.Comments.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int AWARDSEQ = AttributesEnum.AwardSeq.index();
    public static final int DOCID1 = AttributesEnum.DocId1.index();
    public static final int DOCLOCATION = AttributesEnum.DocLocation.index();
    public static final int DOCTYPECODE = AttributesEnum.DocTypeCode.index();
    public static final int DOCURL = AttributesEnum.DocUrl.index();
    public static final int FILECONTENTS = AttributesEnum.FileContents.index();
    public static final int FILEDESC = AttributesEnum.FileDesc.index();
    public static final int FILEFORMAT = AttributesEnum.FileFormat.index();
    public static final int FILENAME = AttributesEnum.FileName.index();
    public static final int ORGFILENAME = AttributesEnum.OrgFileName.index();
    public static final int REFERENCEID = AttributesEnum.ReferenceId.index();
    public static final int UPDDATE1 = AttributesEnum.UpdDate1.index();
    public static final int UPDID1 = AttributesEnum.UpdId1.index();
    public static final int MDOCTYPECODE = AttributesEnum.mDocTypeCode.index();
    public static final int MDOCTYPECODE1 = AttributesEnum.MdocTypeCode1.index();
    public static final int MEGADOCDOCTYPECODELINKLOV1 = AttributesEnum.MegaDocDocTypeCodeLinkLOV1.index();
    public static final int MEGDOCLOV1 = AttributesEnum.MegDocLOV1.index();
    public static final int MEGADOCDOCTYPECODELINKLOV2 = AttributesEnum.MegaDocDocTypeCodeLinkLOV2.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AwardDocEditViewRowImpl() {
    }

    /**
     * Gets AwardDoc entity object.
     * @return the AwardDoc
     */
    public AbacusEntityImpl getAwardDoc() {
        return (AbacusEntityImpl) getEntity(ENTITY_AWARDDOC);
    }

    /**
     * Gets AbacusDocs entity object.
     * @return the AbacusDocs
     */
    public AbacusDocsImpl getAbacusDocs() {
        return (AbacusDocsImpl) getEntity(ENTITY_ABACUSDOCS);
    }

    /**
     * Gets the attribute value for AWARD_ID using the alias name AwardId.
     * @return the AWARD_ID
     */
    public BigDecimal getAwardId() {
        return (BigDecimal) getAttributeInternal(AWARDID);
    }

    /**
     * Sets <code>value</code> as attribute value for AWARD_ID using the alias name AwardId.
     * @param value value to set the AWARD_ID
     */
    public void setAwardId(BigDecimal value) {
        setAttributeInternal(AWARDID, value);
    }

    /**
     * Gets the attribute value for DOC_ID using the alias name DocId.
     * @return the DOC_ID
     */
    public BigDecimal getDocId() {
        return (BigDecimal) getAttributeInternal(DOCID);
    }

    /**
     * Sets <code>value</code> as attribute value for DOC_ID using the alias name DocId.
     * @param value value to set the DOC_ID
     */
    public void setDocId(BigDecimal value) {
        setAttributeInternal(DOCID, value);
    }

    /**
     * Gets the attribute value for COMMENTS using the alias name Comments.
     * @return the COMMENTS
     */
    public String getComments() {
        return (String) getAttributeInternal(COMMENTS);
    }

    /**
     * Sets <code>value</code> as attribute value for COMMENTS using the alias name Comments.
     * @param value value to set the COMMENTS
     */
    public void setComments(String value) {
        setAttributeInternal(COMMENTS, value);
    }

    /**
     * Gets the attribute value for UPD_ID using the alias name UpdId.
     * @return the UPD_ID
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Sets <code>value</code> as attribute value for UPD_ID using the alias name UpdId.
     * @param value value to set the UPD_ID
     */
    public void setUpdId(String value) {
        setAttributeInternal(UPDID, value);
    }

    /**
     * Gets the attribute value for UPD_DATE using the alias name UpdDate.
     * @return the UPD_DATE
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for UPD_DATE using the alias name UpdDate.
     * @param value value to set the UPD_DATE
     */
    public void setUpdDate(Timestamp value) {
        setAttributeInternal(UPDDATE, value);
    }

    /**
     * Gets the attribute value for AWARD_SEQ using the alias name AwardSeq.
     * @return the AWARD_SEQ
     */
    public BigDecimal getAwardSeq() {
        return (BigDecimal) getAttributeInternal(AWARDSEQ);
    }

    /**
     * Sets <code>value</code> as attribute value for AWARD_SEQ using the alias name AwardSeq.
     * @param value value to set the AWARD_SEQ
     */
    public void setAwardSeq(BigDecimal value) {
        setAttributeInternal(AWARDSEQ, value);
    }

    /**
     * Gets the attribute value for DOC_ID using the alias name DocId1.
     * @return the DOC_ID
     */
    public BigDecimal getDocId1() {
        return (BigDecimal) getAttributeInternal(DOCID1);
    }

    /**
     * Sets <code>value</code> as attribute value for DOC_ID using the alias name DocId1.
     * @param value value to set the DOC_ID
     */
    public void setDocId1(BigDecimal value) {
        setAttributeInternal(DOCID1, value);
    }

    /**
     * Gets the attribute value for DOC_LOCATION using the alias name DocLocation.
     * @return the DOC_LOCATION
     */
    public String getDocLocation() {
        return (String) getAttributeInternal(DOCLOCATION);
    }

    /**
     * Sets <code>value</code> as attribute value for DOC_LOCATION using the alias name DocLocation.
     * @param value value to set the DOC_LOCATION
     */
    public void setDocLocation(String value) {
        setAttributeInternal(DOCLOCATION, value);
    }

    /**
     * Gets the attribute value for DOC_TYPE_CODE using the alias name DocTypeCode.
     * @return the DOC_TYPE_CODE
     */
    public String getDocTypeCode() {
        return (String) getAttributeInternal(DOCTYPECODE);
    }

    /**
     * Sets <code>value</code> as attribute value for DOC_TYPE_CODE using the alias name DocTypeCode.
     * @param value value to set the DOC_TYPE_CODE
     */
    public void setDocTypeCode(String value) {
        setAttributeInternal(DOCTYPECODE, value);
    }

    /**
     * Gets the attribute value for DOC_URL using the alias name DocUrl.
     * @return the DOC_URL
     */
    public String getDocUrl() {
        return (String) getAttributeInternal(DOCURL);
    }

    /**
     * Sets <code>value</code> as attribute value for DOC_URL using the alias name DocUrl.
     * @param value value to set the DOC_URL
     */
    public void setDocUrl(String value) {
        setAttributeInternal(DOCURL, value);
    }

    /**
     * Gets the attribute value for FILE_CONTENTS using the alias name FileContents.
     * @return the FILE_CONTENTS
     */
    public BlobDomain getFileContents() {
        return (BlobDomain) getAttributeInternal(FILECONTENTS);
    }

    /**
     * Sets <code>value</code> as attribute value for FILE_CONTENTS using the alias name FileContents.
     * @param value value to set the FILE_CONTENTS
     */
    public void setFileContents(BlobDomain value) {
        setAttributeInternal(FILECONTENTS, value);
    }

    /**
     * Gets the attribute value for FILE_DESC using the alias name FileDesc.
     * @return the FILE_DESC
     */
    public String getFileDesc() {
        return (String) getAttributeInternal(FILEDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for FILE_DESC using the alias name FileDesc.
     * @param value value to set the FILE_DESC
     */
    public void setFileDesc(String value) {
        setAttributeInternal(FILEDESC, value);
    }

    /**
     * Gets the attribute value for FILE_FORMAT using the alias name FileFormat.
     * @return the FILE_FORMAT
     */
    public String getFileFormat() {
        return (String) getAttributeInternal(FILEFORMAT);
    }

    /**
     * Sets <code>value</code> as attribute value for FILE_FORMAT using the alias name FileFormat.
     * @param value value to set the FILE_FORMAT
     */
    public void setFileFormat(String value) {
        setAttributeInternal(FILEFORMAT, value);
    }

    /**
     * Gets the attribute value for FILE_NAME using the alias name FileName.
     * @return the FILE_NAME
     */
    public String getFileName() {
        return (String) getAttributeInternal(FILENAME);
    }

    /**
     * Sets <code>value</code> as attribute value for FILE_NAME using the alias name FileName.
     * @param value value to set the FILE_NAME
     */
    public void setFileName(String value) {
        setAttributeInternal(FILENAME, value);
    }

    /**
     * Gets the attribute value for ORG_FILE_NAME using the alias name OrgFileName.
     * @return the ORG_FILE_NAME
     */
    public String getOrgFileName() {
        return (String) getAttributeInternal(ORGFILENAME);
    }

    /**
     * Sets <code>value</code> as attribute value for ORG_FILE_NAME using the alias name OrgFileName.
     * @param value value to set the ORG_FILE_NAME
     */
    public void setOrgFileName(String value) {
        setAttributeInternal(ORGFILENAME, value);
    }

    /**
     * Gets the attribute value for REFERENCE_ID using the alias name ReferenceId.
     * @return the REFERENCE_ID
     */
    public String getReferenceId() {
        return (String) getAttributeInternal(REFERENCEID);
    }

    /**
     * Sets <code>value</code> as attribute value for REFERENCE_ID using the alias name ReferenceId.
     * @param value value to set the REFERENCE_ID
     */
    public void setReferenceId(String value) {
        setAttributeInternal(REFERENCEID, value);
    }

    /**
     * Gets the attribute value for UPD_DATE using the alias name UpdDate1.
     * @return the UPD_DATE
     */
    public Timestamp getUpdDate1() {
        return (Timestamp) getAttributeInternal(UPDDATE1);
    }

    /**
     * Gets the attribute value for UPD_ID using the alias name UpdId1.
     * @return the UPD_ID
     */
    public String getUpdId1() {
        return (String) getAttributeInternal(UPDID1);
    }

    /**
     * Sets <code>value</code> as attribute value for UPD_ID using the alias name UpdId1.
     * @param value value to set the UPD_ID
     */
    public void setUpdId1(String value) {
        setAttributeInternal(UPDID1, value);
    }

    /**
     * Gets the attribute value for the calculated attribute mDocTypeCode.
     * @return the mDocTypeCode
     */
    public String getmDocTypeCode() {
        //System.out.println("getAttributeInternal(MDOCTYPECODE)  :: " +getAttributeInternal(MDOCTYPECODE) );
        if((String) getAttributeInternal(MDOCTYPECODE) != null){
            return (String) getAttributeInternal(MDOCTYPECODE);
        }
        if(this.getDocTypeCode() == null || "".equalsIgnoreCase(this.getDocTypeCode())){
            return null;
        }
        
        RowSet megaDocLov = this.getMegaDocDocTypeCodeLinkLOV2();
        Key docTypekey = new Key(new Object[] {this.getDocTypeCode()});
        docTypekey.toStringFormat(true);
        Row[] rows = megaDocLov.findByKey(docTypekey, 1);
        if(rows != null && rows.length > 0){
            Row row = rows[0];
            return String.valueOf(row.getAttribute("MdocTypeCode"));
        }
        return null;
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute mDocTypeCode.
     * @param value value to set the  mDocTypeCode
     */
    public void setmDocTypeCode(String value) {
        setAttributeInternal(MDOCTYPECODE, value);
    }

    /**
     * Gets the attribute value for MDOC_TYPE_CODE using the alias name MdocTypeCode1.
     * @return the MDOC_TYPE_CODE
     */
    public BigDecimal getMdocTypeCode1() {
        return (BigDecimal) getAttributeInternal(MDOCTYPECODE1);
    }

    /**
     * Sets <code>value</code> as attribute value for MDOC_TYPE_CODE using the alias name MdocTypeCode1.
     * @param value value to set the MDOC_TYPE_CODE
     */
    public void setMdocTypeCode1(BigDecimal value) {
        setAttributeInternal(MDOCTYPECODE1, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> MegaDocDocTypeCodeLinkLOV1.
     */
    public RowSet getMegaDocDocTypeCodeLinkLOV1() {
        return (RowSet) getAttributeInternal(MEGADOCDOCTYPECODELINKLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> MegDocLOV1.
     */
    public RowSet getMegDocLOV1() {
        return (RowSet) getAttributeInternal(MEGDOCLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> MegaDocDocTypeCodeLinkLOV2.
     */
    public RowSet getMegaDocDocTypeCodeLinkLOV2() {
        return (RowSet) getAttributeInternal(MEGADOCDOCTYPECODELINKLOV2);
    }
}

