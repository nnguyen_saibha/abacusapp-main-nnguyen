package gov.ofda.abacus.model.abacusapp.lov.entityview;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;
import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;

import gov.ofda.abacus.model.abacusapp.lov.entity.ProjectLookupImpl;

import java.sql.Timestamp;

import java.util.Arrays;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.domain.DBSequence;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Feb 01 15:04:49 EST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ProjectLookupViewRowImpl extends AbacusViewRowImpl {


    public static final int ENTITY_PROJECTLOOKUP = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ProjectNbr,
        ProjectNbr1,
        BureauCode,
        OfficeCode,
        RegionCode,
        ProjectName,
        CountryCode,
        OfdaDivisionCode,
        DisasterCode,
        OfdaTeamCode,
        BureauSearch,
        OfficeSearch,
        DivisionSearch,
        TeamSearch,
        CountryName,
        DisasterFlag,
        DisasterDescription,
        Description,
        Active,
        UpdId,
        UpdDate,
        DivisionLOV1,
        TeamLOV1,
        DisasterFlagLOV1,
        DisasterTypeLOV1,
        CountryLOV1,
        RegionLOV1,
        YesNoLOV1,
        BureauLOV2,
        OfficeLOV2,
        ProjectTypeLOV1,
        BureauLOV1,
        OfficeLOV1,
        DivisionLOV2,
        TeamLOV2;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int PROJECTNBR = AttributesEnum.ProjectNbr.index();
    public static final int PROJECTNBR1 = AttributesEnum.ProjectNbr1.index();
    public static final int BUREAUCODE = AttributesEnum.BureauCode.index();
    public static final int OFFICECODE = AttributesEnum.OfficeCode.index();
    public static final int REGIONCODE = AttributesEnum.RegionCode.index();
    public static final int PROJECTNAME = AttributesEnum.ProjectName.index();
    public static final int COUNTRYCODE = AttributesEnum.CountryCode.index();
    public static final int OFDADIVISIONCODE = AttributesEnum.OfdaDivisionCode.index();
    public static final int DISASTERCODE = AttributesEnum.DisasterCode.index();
    public static final int OFDATEAMCODE = AttributesEnum.OfdaTeamCode.index();
    public static final int BUREAUSEARCH = AttributesEnum.BureauSearch.index();
    public static final int OFFICESEARCH = AttributesEnum.OfficeSearch.index();
    public static final int DIVISIONSEARCH = AttributesEnum.DivisionSearch.index();
    public static final int TEAMSEARCH = AttributesEnum.TeamSearch.index();
    public static final int COUNTRYNAME = AttributesEnum.CountryName.index();
    public static final int DISASTERFLAG = AttributesEnum.DisasterFlag.index();
    public static final int DISASTERDESCRIPTION = AttributesEnum.DisasterDescription.index();
    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int ACTIVE = AttributesEnum.Active.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int DIVISIONLOV1 = AttributesEnum.DivisionLOV1.index();
    public static final int TEAMLOV1 = AttributesEnum.TeamLOV1.index();
    public static final int DISASTERFLAGLOV1 = AttributesEnum.DisasterFlagLOV1.index();
    public static final int DISASTERTYPELOV1 = AttributesEnum.DisasterTypeLOV1.index();
    public static final int COUNTRYLOV1 = AttributesEnum.CountryLOV1.index();
    public static final int REGIONLOV1 = AttributesEnum.RegionLOV1.index();
    public static final int YESNOLOV1 = AttributesEnum.YesNoLOV1.index();
    public static final int BUREAULOV2 = AttributesEnum.BureauLOV2.index();
    public static final int OFFICELOV2 = AttributesEnum.OfficeLOV2.index();
    public static final int PROJECTTYPELOV1 = AttributesEnum.ProjectTypeLOV1.index();
    public static final int BUREAULOV1 = AttributesEnum.BureauLOV1.index();
    public static final int OFFICELOV1 = AttributesEnum.OfficeLOV1.index();
    public static final int DIVISIONLOV2 = AttributesEnum.DivisionLOV2.index();
    public static final int TEAMLOV2 = AttributesEnum.TeamLOV2.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ProjectLookupViewRowImpl() {
    }

    /**
     * Gets ProjectLookup entity object.
     * @return the ProjectLookup
     */
    public ProjectLookupImpl getProjectLookup() {
        return (ProjectLookupImpl) getEntity(ENTITY_PROJECTLOOKUP);
    }

    /**
     * Gets the attribute value for PROJECT_NBR using the alias name ProjectNbr.
     * @return the PROJECT_NBR
     */
    public DBSequence getProjectNbr() {
        return (DBSequence) getAttributeInternal(PROJECTNBR);
    }

    /**
     * Sets <code>value</code> as attribute value for PROJECT_NBR using the alias name ProjectNbr.
     * @param value value to set the PROJECT_NBR
     */
    public void setProjectNbr(DBSequence value) {
        setAttributeInternal(PROJECTNBR, value);
    }


    /**
     * Gets the attribute value for the calculated attribute ProjectNbr1.
     * @return the ProjectNbr1
     */
    public String getProjectNbr1() {
        return (String) getAttributeInternal(PROJECTNBR1);
    }

    /**
     * Gets the attribute value for OFDA_DIVISION_CODE using the alias name OfdaDivisionCode.
     * @return the OFDA_DIVISION_CODE
     */
    public String getOfdaDivisionCode() {
        return (String) getAttributeInternal(OFDADIVISIONCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for OFDA_DIVISION_CODE using the alias name OfdaDivisionCode.
     * @param value value to set the OFDA_DIVISION_CODE
     */
    public void setOfdaDivisionCode(String value) {
        setAttributeInternal(OFDADIVISIONCODE, value);
    }

    /**
     * Gets the attribute value for OFDA_TEAM_CODE using the alias name OfdaTeamCode.
     * @return the OFDA_TEAM_CODE
     */
    public String getOfdaTeamCode() {
        return (String) getAttributeInternal(OFDATEAMCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for OFDA_TEAM_CODE using the alias name OfdaTeamCode.
     * @param value value to set the OFDA_TEAM_CODE
     */
    public void setOfdaTeamCode(String value) {
        setAttributeInternal(OFDATEAMCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute BureauSearch.
     * @return the BureauSearch
     */
    public String getBureauSearch() {
        return (String) getAttributeInternal(BUREAUSEARCH);
    }

    /**
     * Gets the attribute value for the calculated attribute OfficeSearch.
     * @return the OfficeSearch
     */
    public String getOfficeSearch() {
        return (String) getAttributeInternal(OFFICESEARCH);
    }

    /**
     * Gets the attribute value for the calculated attribute DivisionSearch.
     * @return the DivisionSearch
     */
    public String getDivisionSearch() {
        return (String) getAttributeInternal(DIVISIONSEARCH);
    }

    /**
     * Gets the attribute value for the calculated attribute TeamSearch.
     * @return the TeamSearch
     */
    public String getTeamSearch() {
        return (String) getAttributeInternal(TEAMSEARCH);
    }

    /**
     * Gets the attribute value for PROJECT_NAME using the alias name ProjectName.
     * @return the PROJECT_NAME
     */
    public String getProjectName() {
        return (String) getAttributeInternal(PROJECTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for PROJECT_NAME using the alias name ProjectName.
     * @param value value to set the PROJECT_NAME
     */
    public void setProjectName(String value) {
        setAttributeInternal(PROJECTNAME, value);
    }

    /**
     * Gets the attribute value for REGION_CODE using the alias name RegionCode.
     * @return the REGION_CODE
     */
    public String getRegionCode() {
        return (String) getAttributeInternal(REGIONCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for REGION_CODE using the alias name RegionCode.
     * @param value value to set the REGION_CODE
     */
    public void setRegionCode(String value) {
        setAttributeInternal(REGIONCODE, value);
        setCountryCode(null);
    }

    /**
     * Gets the attribute value for COUNTRY_CODE using the alias name CountryCode.
     * @return the COUNTRY_CODE
     */
    public String getCountryCode() {
        return (String) getAttributeInternal(COUNTRYCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for COUNTRY_CODE using the alias name CountryCode.
     * @param value value to set the COUNTRY_CODE
     */
    public void setCountryCode(String value) {
        setAttributeInternal(COUNTRYCODE, value);
        if(null != this.getCountryCode()){
            Key key = new Key(new Object[] { this.getCountryCode() });
            RowSet rs = this.getCountryLOV1();
            Row r[] = rs.findByKey(key, 1);
            if (r.length > 0) {
                String regionCode = r[0].getAttribute("RegionCode").toString();
                setAttributeInternal(REGIONCODE, regionCode);
            }
        }
    }

    /**
     * Gets the attribute value for the calculated attribute CountryName.
     * @return the CountryName
     */
    public String getCountryName() {
        return (String) getAttributeInternal(COUNTRYNAME);
    }

    /**
     * Gets the attribute value for DISASTER_FLAG using the alias name DisasterFlag.
     * @return the DISASTER_FLAG
     */
    public String getDisasterFlag() {
        return (String) getAttributeInternal(DISASTERFLAG);
    }

    /**
     * Sets <code>value</code> as attribute value for DISASTER_FLAG using the alias name DisasterFlag.
     * @param value value to set the DISASTER_FLAG
     */
    public void setDisasterFlag(String value) {
        setAttributeInternal(DISASTERFLAG, value);
        setDisasterCode(null);
    }

    /**
     * Gets the attribute value for DISASTER_CODE using the alias name DisasterCode.
     * @return the DISASTER_CODE
     */
    public String getDisasterCode() {
        return (String) getAttributeInternal(DISASTERCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for DISASTER_CODE using the alias name DisasterCode.
     * @param value value to set the DISASTER_CODE
     */
    public void setDisasterCode(String value) {
        setAttributeInternal(DISASTERCODE, value);
        if(null != this.getDisasterCode()){
            Key key = new Key(new Object[] { this.getDisasterCode() });
            RowSet rs = this.getDisasterTypeLOV1();
            Row r[] = rs.findByKey(key, 1);
            if (r.length > 0) {
                String dFlag = r[0].getAttribute("DiasterFlag").toString();
                setAttributeInternal(DISASTERFLAG, dFlag);
            }
        }
    }

    /**
     * Gets the attribute value for the calculated attribute DisasterDescription.
     * @return the DisasterDescription
     */
    public String getDisasterDescription() {
        return (String) getAttributeInternal(DISASTERDESCRIPTION);
    }

    /**
     * Gets the attribute value for ACTIVE using the alias name Active.
     * @return the ACTIVE
     */
    public String getActive() {
        return (String) getAttributeInternal(ACTIVE);
    }

    /**
     * Sets <code>value</code> as attribute value for ACTIVE using the alias name Active.
     * @param value value to set the ACTIVE
     */
    public void setActive(String value) {
        setAttributeInternal(ACTIVE, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description.
     * @return the DESCRIPTION
     */
    public String getDescription() {
        return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for UPD_ID using the alias name UpdId.
     * @return the UPD_ID
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Gets the attribute value for UPD_DATE using the alias name UpdDate.
     * @return the UPD_DATE
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }


    /**
     * Gets the attribute value for BUREAU_CODE using the alias name BureauCode.
     * @return the BUREAU_CODE
     */
    public String getBureauCode() {
        return (String) getAttributeInternal(BUREAUCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for BUREAU_CODE using the alias name BureauCode.
     * @param value value to set the BUREAU_CODE
     */
    public void setBureauCode(String value) {
        setAttributeInternal(BUREAUCODE, value);
    }

    /**
     * Gets the attribute value for OFFICE_CODE using the alias name OfficeCode.
     * @return the OFFICE_CODE
     */
    public String getOfficeCode() {
        return (String) getAttributeInternal(OFFICECODE);
    }

    /**
     * Sets <code>value</code> as attribute value for OFFICE_CODE using the alias name OfficeCode.
     * @param value value to set the OFFICE_CODE
     */
    public void setOfficeCode(String value) {
        setAttributeInternal(OFFICECODE, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> DivisionLOV1.
     */
    public RowSet getDivisionLOV1() {
        return (RowSet) getAttributeInternal(DIVISIONLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> TeamLOV1.
     */
    public RowSet getTeamLOV1() {
        return (RowSet) getAttributeInternal(TEAMLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> DisasterFlagLOV1.
     */
    public RowSet getDisasterFlagLOV1() {
        return (RowSet) getAttributeInternal(DISASTERFLAGLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> DisasterTypeLOV1.
     */
    public RowSet getDisasterTypeLOV1() {
        return (RowSet) getAttributeInternal(DISASTERTYPELOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> CountryLOV1.
     */
    public RowSet getCountryLOV1() {
        return (RowSet) getAttributeInternal(COUNTRYLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> RegionLOV1.
     */
    public RowSet getRegionLOV1() {
        return (RowSet) getAttributeInternal(REGIONLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> YesNoLOV1.
     */
    public RowSet getYesNoLOV1() {
        return (RowSet) getAttributeInternal(YESNOLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> BureauLOV1.
     */
    public RowSet getBureauLOV2() {
        return (RowSet) getAttributeInternal(BUREAULOV2);
    }

    /**
     * Gets the view accessor <code>RowSet</code> OfficeLOV1.
     */
    public RowSet getOfficeLOV2() {
        return (RowSet) getAttributeInternal(OFFICELOV2);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ProjectTypeLOV1.
     */
    public RowSet getProjectTypeLOV1() {
        return (RowSet) getAttributeInternal(PROJECTTYPELOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> BureauLOV1.
     */
    public RowSet getBureauLOV1() {
        return (RowSet) getAttributeInternal(BUREAULOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> OfficeLOV1.
     */
    public RowSet getOfficeLOV1() {
        return (RowSet) getAttributeInternal(OFFICELOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> DivisionLOV2.
     */
    public RowSet getDivisionLOV2() {
        return (RowSet) getAttributeInternal(DIVISIONLOV2);
    }

    /**
     * Gets the view accessor <code>RowSet</code> TeamLOV2.
     */
    public RowSet getTeamLOV2() {
        return (RowSet) getAttributeInternal(TEAMLOV2);
    }

    @Override
    public boolean isAttributeUpdateable(int i) {
        return super.isAttributeUpdateable(i);
    }
}

