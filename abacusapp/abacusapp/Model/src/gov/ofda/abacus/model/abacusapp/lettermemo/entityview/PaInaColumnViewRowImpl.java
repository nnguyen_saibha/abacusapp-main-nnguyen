package gov.ofda.abacus.model.abacusapp.lettermemo.entityview;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;
import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.Row;
import oracle.jbo.domain.DBSequence;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Dec 30 18:19:00 EST 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PaInaColumnViewRowImpl extends AbacusViewRowImpl {


    public static final int ENTITY_PAINACOLUMN = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        PaInaColumnSeq,
        LetterMemoId,
        ColumnName,
        ColumnValue,
        UpdId,
        UpdDate,
        ColumnLabel,
        ColumnType;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int PAINACOLUMNSEQ = AttributesEnum.PaInaColumnSeq.index();
    public static final int LETTERMEMOID = AttributesEnum.LetterMemoId.index();
    public static final int COLUMNNAME = AttributesEnum.ColumnName.index();
    public static final int COLUMNVALUE = AttributesEnum.ColumnValue.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int COLUMNLABEL = AttributesEnum.ColumnLabel.index();
    public static final int COLUMNTYPE = AttributesEnum.ColumnType.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PaInaColumnViewRowImpl() {
    }

    /**
     * Gets PaInaColumn entity object.
     * @return the PaInaColumn
     */
    public AbacusEntityImpl getPaInaColumn() {
        return (AbacusEntityImpl) getEntity(ENTITY_PAINACOLUMN);
    }

    /**
     * Gets the attribute value for PA_INA_COLUMN_SEQ using the alias name PaInaColumnSeq.
     * @return the PA_INA_COLUMN_SEQ
     */
    public DBSequence getPaInaColumnSeq() {
        return (DBSequence) getAttributeInternal(PAINACOLUMNSEQ);
    }

    /**
     * Sets <code>value</code> as attribute value for PA_INA_COLUMN_SEQ using the alias name PaInaColumnSeq.
     * @param value value to set the PA_INA_COLUMN_SEQ
     */
    public void setPaInaColumnSeq(DBSequence value) {
        setAttributeInternal(PAINACOLUMNSEQ, value);
    }

    /**
     * Gets the attribute value for LETTER_MEMO_ID using the alias name LetterMemoId.
     * @return the LETTER_MEMO_ID
     */
    public BigDecimal getLetterMemoId() {
        return (BigDecimal) getAttributeInternal(LETTERMEMOID);
    }

    /**
     * Sets <code>value</code> as attribute value for LETTER_MEMO_ID using the alias name LetterMemoId.
     * @param value value to set the LETTER_MEMO_ID
     */
    public void setLetterMemoId(BigDecimal value) {
        setAttributeInternal(LETTERMEMOID, value);
    }

    /**
     * Gets the attribute value for COLUMN_NAME using the alias name ColumnName.
     * @return the COLUMN_NAME
     */
    public String getColumnName() {
        return (String) getAttributeInternal(COLUMNNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for COLUMN_NAME using the alias name ColumnName.
     * @param value value to set the COLUMN_NAME
     */
    public void setColumnName(String value) {
        setAttributeInternal(COLUMNNAME, value);
    }

    /**
     * Gets the attribute value for COLUMN_VALUE using the alias name ColumnValue.
     * @return the COLUMN_VALUE
     */
    public String getColumnValue() {
        return (String) getAttributeInternal(COLUMNVALUE);
    }

    /**
     * Sets <code>value</code> as attribute value for COLUMN_VALUE using the alias name ColumnValue.
     * @param value value to set the COLUMN_VALUE
     */
    public void setColumnValue(String value) {
        setAttributeInternal(COLUMNVALUE, value);
    }

    /**
     * Gets the attribute value for UPD_ID using the alias name UpdId.
     * @return the UPD_ID
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Sets <code>value</code> as attribute value for UPD_ID using the alias name UpdId.
     * @param value value to set the UPD_ID
     */
    public void setUpdId(String value) {
        setAttributeInternal(UPDID, value);
    }

    /**
     * Gets the attribute value for UPD_DATE using the alias name UpdDate.
     * @return the UPD_DATE
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for UPD_DATE using the alias name UpdDate.
     * @param value value to set the UPD_DATE
     */
    public void setUpdDate(Timestamp value) {
        setAttributeInternal(UPDDATE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ColumnLabel.
     * @return the ColumnLabel
     */
    public String getColumnLabel() {
        return (String) getAttributeInternal(COLUMNLABEL);
    }

    /**
     * Gets the attribute value for the calculated attribute ColumnType.
     * @return the ColumnType
     */
    public String getColumnType() {
        return (String) getAttributeInternal(COLUMNTYPE);
    }


}

