package gov.ofda.abacus.model.abacusapp.type;

import java.sql.SQLException;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.sql.Datum;
import oracle.sql.REF;
import oracle.sql.STRUCT;

public class FavReportParamTypeRef implements ORAData, ORADataFactory
{
  public static final String _SQL_BASETYPE = "APP.FAV_REPORT_PARAM_TYPE";
  public static final int _SQL_TYPECODE = OracleTypes.REF;

  REF _ref;

private static final FavReportParamTypeRef _FavReportParamTypeRefFactory = new FavReportParamTypeRef();

  public static ORADataFactory getORADataFactory()
  { return _FavReportParamTypeRefFactory; }
  /* constructor */
  public FavReportParamTypeRef()
  {
  }

  /* ORAData interface */
  public Datum toDatum(Connection c) throws SQLException
  {
    return _ref;
  }

  /* ORADataFactory interface */
  public ORAData create(Datum d, int sqlType) throws SQLException
  {
    if (d == null) return null; 
    FavReportParamTypeRef r = new FavReportParamTypeRef();
    r._ref = (REF) d;
    return r;
  }

  public static FavReportParamTypeRef cast(ORAData o) throws SQLException
  {
     if (o == null) return null;
     try { return (FavReportParamTypeRef) getORADataFactory().create(o.toDatum(null), OracleTypes.REF); }
     catch (Exception exn)
     { throw new SQLException("Unable to convert "+o.getClass().getName()+" to FavReportParamTypeRef: "+exn.toString()); }
  }

  public FavReportParamType getValue() throws SQLException
  {
     return (FavReportParamType) FavReportParamType.getORADataFactory().create(
       _ref.getSTRUCT(), OracleTypes.REF);
  }

  public void setValue(FavReportParamType c) throws SQLException
  {
    _ref.setValue((STRUCT) c.toDatum(_ref.getJavaSqlConnection()));
  }
}
