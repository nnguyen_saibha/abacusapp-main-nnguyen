package gov.ofda.abacus.model.abacusapp.type;

import java.io.Serializable;

import java.util.ArrayList;

public class PABudget implements Serializable {
    @SuppressWarnings("compatibility:-3499593340269943171")
    private static final long serialVersionUID = 1L;

    
    private String status;
    private String code;
    private ArrayList<String> changedList=new ArrayList<String>();
    private String oldCode;
    private String newCode;
    private String oldPlannedAmt;
    private String newPlannedAmt;
    private String oldApprovedAmt;
    private String newApprovedAmt;


    public PABudget() {
        super();
    }
    public PABudget(String status, String code) {
        super();
        this.status = status;
        this.code = code;
        
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setChangedList(ArrayList<String> changedList) {
        this.changedList = changedList;
    }

    public ArrayList<String> getChangedList() {
        return changedList;
    }

    public void setOldCode(String oldCode) {
        this.oldCode = oldCode;
    }

    public String getOldCode() {
        return oldCode;
    }

    public void setNewCode(String newCode) {
        this.newCode = newCode;
    }

    public String getNewCode() {
        return newCode;
    }

    public void setOldPlannedAmt(String oldPlannedAmt) {
        this.oldPlannedAmt = oldPlannedAmt;
    }

    public String getOldPlannedAmt() {
        return oldPlannedAmt;
    }

    public void setNewPlannedAmt(String newPlannedAmt) {
        this.newPlannedAmt = newPlannedAmt;
    }

    public String getNewPlannedAmt() {
        return newPlannedAmt;
    }

    public void setOldApprovedAmt(String oldApprovedAmt) {
        this.oldApprovedAmt = oldApprovedAmt;
    }

    public String getOldApprovedAmt() {
        return oldApprovedAmt;
    }

    public void setNewApprovedAmt(String newApprovedAmt) {
        this.newApprovedAmt = newApprovedAmt;
    }

    public String getNewApprovedAmt() {
        return newApprovedAmt;
    }
}
