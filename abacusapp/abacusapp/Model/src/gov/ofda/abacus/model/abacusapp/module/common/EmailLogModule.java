package gov.ofda.abacus.model.abacusapp.module.common;

import java.util.List;

import oracle.jbo.ApplicationModule;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Jul 30 13:19:02 EDT 2015
// ---------------------------------------------------------------------
public interface EmailLogModule extends ApplicationModule {
    String getEmailbyUsername(String username);

    void logEmail(String toAdd, String ccAdd, String subject, String content);

    void setProposalReviewEmailSent(String actionID);

    String getActionDetails(String actionID);

    String[] getControlNbrGeneratedList(String actionID);

    String[] getDistributionAmtChangedList(String actionID);

    String[] getAwardTrackingList(String actionID);

    String[] getObligatedAmtChgList(String actionID);

    String[] getAORLetterList(String actionID);

    String[] getAwardPkgIncompleteList(String actionID);

    String[] getAwardPkgCompleteList(String actionID);

    String[] getGUPrintShopPkgCompleteList(String actionID);

    String[] getProposalReviewSpltList(String actionID, String oldPrSpecialist);


    String getEmailList(List<String> userList);

    String getUserName();

    String[] getCostSheetApprovedList(String actionID);

    String[] getCostSheetInternalReviewList(String actionID);

    String[] getAwardPkgClearanceList(String actionID, String oldUserid, String newUserid);

    String[] getIssueLetterEmailList(String actionID);
}

