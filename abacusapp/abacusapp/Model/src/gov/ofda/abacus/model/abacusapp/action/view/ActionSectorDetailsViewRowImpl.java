package gov.ofda.abacus.model.abacusapp.action.view;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Jul 31 15:24:27 EDT 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ActionSectorDetailsViewRowImpl extends AbacusViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ActionId,
        SectorCode,
        SectorName,
        SectorAmt,
        SubAssistanceName,
        MissionCommitmentValue,
        Keywords;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    public static final int ACTIONID = AttributesEnum.ActionId.index();
    public static final int SECTORCODE = AttributesEnum.SectorCode.index();
    public static final int SECTORNAME = AttributesEnum.SectorName.index();
    public static final int SECTORAMT = AttributesEnum.SectorAmt.index();
    public static final int SUBASSISTANCENAME = AttributesEnum.SubAssistanceName.index();
    public static final int MISSIONCOMMITMENTVALUE = AttributesEnum.MissionCommitmentValue.index();
    public static final int KEYWORDS = AttributesEnum.Keywords.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ActionSectorDetailsViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute ActionId.
     * @return the ActionId
     */
    public Long getActionId() {
        return (Long) getAttributeInternal(ACTIONID);
    }

    /**
     * Gets the attribute value for the calculated attribute SectorCode.
     * @return the SectorCode
     */
    public String getSectorCode() {
        return (String) getAttributeInternal(SECTORCODE);
    }

    /**
     * Gets the attribute value for the calculated attribute SectorName.
     * @return the SectorName
     */
    public String getSectorName() {
        return (String) getAttributeInternal(SECTORNAME);
    }

    /**
     * Gets the attribute value for the calculated attribute SectorAmt.
     * @return the SectorAmt
     */
    public String getSectorAmt() {
        return (String) getAttributeInternal(SECTORAMT);
    }

    /**
     * Gets the attribute value for the calculated attribute SubAssistanceName.
     * @return the SubAssistanceName
     */
    public String getSubAssistanceName() {
        return (String) getAttributeInternal(SUBASSISTANCENAME);
    }

    /**
     * Gets the attribute value for the calculated attribute MissionCommitmentValue.
     * @return the MissionCommitmentValue
     */
    public String getMissionCommitmentValue() {
        return (String) getAttributeInternal(MISSIONCOMMITMENTVALUE);
    }

    /**
     * Gets the attribute value for the calculated attribute Keywords.
     * @return the Keywords
     */
    public String getKeywords() {
        return (String) getAttributeInternal(KEYWORDS);
    }
}

