package gov.ofda.abacus.model.abacusapp.user.oim;

import java.util.HashMap;

import oracle.adf.share.logging.ADFLogger;

import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserAlreadyExistsException;
import oracle.iam.identity.exception.UserCreateException;
import oracle.iam.identity.exception.UserDeleteException;
import oracle.iam.identity.exception.UserDisableException;
import oracle.iam.identity.exception.UserEnableException;
import oracle.iam.identity.exception.UserLockException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.UserUnlockException;
import oracle.iam.identity.exception.ValidationFailedException;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;

public class OIMUserManager extends OIMMyClient {
    UserManager userManager;
    private static ADFLogger logger = ADFLogger.createADFLogger(OIMUserManager.class);
    public OIMUserManager() {
        super();
    }
        public UserManager getUserManager() {
            UserManager umgr=null;
            umgr = _oimClient.getService(UserManager.class);
            return umgr;
        }

        public ResultMessage createUser(String userId,HashMap<String,Object> userAttributeValueMap ) {                                             //Function to create User
                    User user = new User(userId, userAttributeValueMap);
            try {
                userManager=this.getUserManager();
                userManager.create(user);
                return new ResultMessage(true,"User: "+userId+" created sucessfully in OIM.");
            } catch (ValidationFailedException e) {
                e.printStackTrace();
                logger.log(logger.ERROR, "User creation validation failed exception. "+e);
                return new ResultMessage(false,"User creation validation failed exception. "+e.getMessage());
                
            } catch (UserAlreadyExistsException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"User creation failed. User already exists. ");
                return new ResultMessage(false,"User creation failed. User already exists.");
            } catch (UserCreateException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"User creation Failed. "+e);
                return new ResultMessage(false,"User creation failed. "+e.getMessage());
            }
        }
        public ResultMessage modifyUser(String userId,HashMap<String,Object> mapAttrs) {                        //Function to disable user
            try {
                User user = new User(userId, mapAttrs);
                userManager=this.getUserManager();
                userManager.modify("User Login", userId, user);
                return new ResultMessage(true,"User: "+userId+" modified sucessfully in OIM.");
            } catch (ValidationFailedException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"Modify user validation failed. "+e);
                return new ResultMessage(false,"Modify user  validation failed. "+e.getMessage());
            } catch (UserModifyException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"User modify exception. "+e);
                return new ResultMessage(false,"User modify exception. "+e.getMessage());
            } catch (NoSuchUserException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"No such user.");
                return new ResultMessage(false,"No such user.");
            } catch (SearchKeyNotUniqueException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"userId matched more than one row.");
                return new ResultMessage(false,"userId matched more than one row.");
        }
    } 
        public ResultMessage deleteUser(String userId) {                        //Function to disable user
            try {
                userManager=this.getUserManager();
                userManager.delete(userId, true);
                return new ResultMessage(true,"User: "+userId+" deleted sucessfully in OIM.");
            } catch (ValidationFailedException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"Delete user validation failed. "+e);
                return new ResultMessage(false,"Delete user  validation failed. "+e.getMessage());
            } catch (UserDeleteException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"User delete exception. "+e);
                return new ResultMessage(false,"User delete exception. "+e.getMessage());
            } catch (NoSuchUserException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"No such user.");
                return new ResultMessage(false,"No such user.");
            }
        }
        public ResultMessage disableUser(String userId) {                        //Function to disable user
            try {
                userManager=this.getUserManager();
                userManager.disable(userId, true);
                return new ResultMessage(true,"User: "+userId+" disabled sucessfully in OIM.");
            } catch (ValidationFailedException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"Disable user validation failed. "+e);
                return new ResultMessage(false,"Disable user  validation failed. "+e.getMessage());
            } catch (UserDisableException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"User disable exception. "+e);
                return new ResultMessage(false,"User disable exception. "+e.getMessage());
            } catch (NoSuchUserException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"No such user.");
                return new ResultMessage(false,"No such user.");
            }
        }
        public ResultMessage enableUser(String userId) {                         //Function to enable user
            try {
                userManager=this.getUserManager();
                userManager.enable(userId, true);
                return new ResultMessage(true,"User: "+userId+" enabled successfully in OIM.");
            } catch (ValidationFailedException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"Disable user validation failed. "+e);
                return new ResultMessage(false,"User enable validation exception. "+e.getMessage());
                
            } catch (UserEnableException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"User enable exception. "+e);
                return new ResultMessage(false,"User enable exception. "+e.getMessage());
            } catch (NoSuchUserException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"No such user.");
                return new ResultMessage(false,"No such user. ");
            }
          
        }
        public ResultMessage resetPassword(String userId){                       //Function to reset user password
     
     
            try {
                userManager=this.getUserManager();
                //userManager.lock(userId, true, true);
                userManager.resetPassword(userId, true,true);          //Random Password will be set and will be sent to user mail if notifications are enabled
                return new ResultMessage(true,"User: "+userId+" password reset successfully. Random Password will be set and will be sent to user mail.");
            } catch (NoSuchUserException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"No such user.");
                return new ResultMessage(false,"User does not exist.");
            } catch (UserManagerException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"User manager exception. "+e);
                return new ResultMessage(false,"User Manager Exception."+e.getMessage());
            }
        }
        public ResultMessage lockUser(String userId) {       //Function to Lock User
            try {
                userManager=this.getUserManager();
                userManager.lock(userId, true,true);
                return new ResultMessage(true,"User: "+userId+" locked successfully.");
            } catch (ValidationFailedException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"User lock validation exception. "+e);
                return new ResultMessage(false,"User lock validation exception. "+e.getMessage());
            } catch (UserLockException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"User lock exception. "+e);
                return new ResultMessage(false,"User lock exception. "+e.getMessage());
            } catch (NoSuchUserException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"No such user.");
                return new ResultMessage(false,"No such user.");
            }
        }
        public ResultMessage unLockUser(String userId) {       //Function to Unlock user
            try {
                userManager=this.getUserManager();
                userManager.unlock(userId, true);
                return new ResultMessage(true,"User: "+userId+" unlocked successfully.");
            } catch (ValidationFailedException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"User unlock validation exception. "+e);
                return new ResultMessage(false,"User unlock validation exception. "+e.getMessage());
            } catch (UserUnlockException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"User unlock exception. "+e);
                return new ResultMessage(false,"User unlock exception. "+e.getMessage());
            } catch (NoSuchUserException e) {
                e.printStackTrace();
                logger.log(logger.ERROR,"No such User.");
                return new ResultMessage(false,"No such User.");
            }
        }
    public User getUserDetails(String userId) {
        userManager=this.getUserManager();
        User usr=null;
        try {
            usr = userManager.getDetails(userId, null, true);
        } catch (NoSuchUserException e) {
            return null;
        } catch (UserLookupException e) {
            return null;
        }
        return usr;
    }
    }