package gov.ofda.abacus.model.abacusapp.budget.view.chart;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;

import java.math.BigDecimal;

import oracle.jbo.RowIterator;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Jun 15 16:16:16 EDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class OfdaDivsionAmtViewRowImpl extends AbacusViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        BudgetFy,
        BureauCode,
        OfficeCode,
        OfdaDivisionCode,
        TotalBudgetAmt,
        SortOrder,
        BudgetAmtFmtShort,
        TeamTotalAmtView,
        TeamTotalAmtView1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int BUDGETFY = AttributesEnum.BudgetFy.index();
    public static final int BUREAUCODE = AttributesEnum.BureauCode.index();
    public static final int OFFICECODE = AttributesEnum.OfficeCode.index();
    public static final int OFDADIVISIONCODE = AttributesEnum.OfdaDivisionCode.index();
    public static final int TOTALBUDGETAMT = AttributesEnum.TotalBudgetAmt.index();
    public static final int SORTORDER = AttributesEnum.SortOrder.index();
    public static final int BUDGETAMTFMTSHORT = AttributesEnum.BudgetAmtFmtShort.index();
    public static final int TEAMTOTALAMTVIEW = AttributesEnum.TeamTotalAmtView.index();
    public static final int TEAMTOTALAMTVIEW1 = AttributesEnum.TeamTotalAmtView1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public OfdaDivsionAmtViewRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute BudgetFy.
     * @return the BudgetFy
     */
    public Integer getBudgetFy() {
        return (Integer) getAttributeInternal(BUDGETFY);
    }

    /**
     * Gets the attribute value for the calculated attribute BureauCode.
     * @return the BureauCode
     */
    public String getBureauCode() {
        return (String) getAttributeInternal(BUREAUCODE);
    }

    /**
     * Gets the attribute value for the calculated attribute OfficeCode.
     * @return the OfficeCode
     */
    public String getOfficeCode() {
        return (String) getAttributeInternal(OFFICECODE);
    }

    /**
     * Gets the attribute value for the calculated attribute OfdaDivisionCode.
     * @return the OfdaDivisionCode
     */
    public String getOfdaDivisionCode() {
        return (String) getAttributeInternal(OFDADIVISIONCODE);
    }


    /**
     * Gets the attribute value for the calculated attribute TotalBudgetAmt.
     * @return the TotalBudgetAmt
     */
    public BigDecimal getTotalBudgetAmt() {
        return (BigDecimal) getAttributeInternal(TOTALBUDGETAMT);
    }


    /**
     * Gets the attribute value for the calculated attribute SortOrder.
     * @return the SortOrder
     */
    public BigDecimal getSortOrder() {
        return (BigDecimal) getAttributeInternal(SORTORDER);
    }

    /**
     * Gets the attribute value for the calculated attribute BudgetAmtFmtShort.
     * @return the BudgetAmtFmtShort
     */
    public String getBudgetAmtFmtShort() {
        if(this.getTotalBudgetAmt()==null)
            return "";
        double amt=this.getTotalBudgetAmt().doubleValue();
        String sname;
        if("Core Unallocated".equals(this.getOfdaDivisionCode()))
            sname="Core Unall:";
        else
            sname=this.getOfdaDivisionCode()+":";
            if (amt < 1000) return sname + amt;
            int exp = (int) (Math.log(amt) / Math.log(1000));
            return sname+String.format("%.1f%c",
                                 amt / Math.pow(1000, exp),
                                 "kMBTPE".charAt(exp-1));
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link TeamTotalAmtView.
     */
    public RowIterator getTeamTotalAmtView() {
        return (RowIterator) getAttributeInternal(TEAMTOTALAMTVIEW);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link TeamTotalAmtView1.
     */
    public RowIterator getTeamTotalAmtView1() {
        return (RowIterator) getAttributeInternal(TEAMTOTALAMTVIEW1);
    }
}

