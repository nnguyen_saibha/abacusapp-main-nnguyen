package gov.ofda.abacus.model.abacusapp.base.framework;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import oracle.adf.share.ADFContext;

import oracle.jbo.JboException;
import oracle.jbo.server.ViewRowImpl;

public class AbacusViewRowImpl extends ViewRowImpl {
    public AbacusViewRowImpl() {
        super();
    }
}