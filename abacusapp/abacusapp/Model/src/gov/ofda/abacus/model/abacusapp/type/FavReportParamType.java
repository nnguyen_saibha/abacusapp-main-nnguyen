package gov.ofda.abacus.model.abacusapp.type;

import java.sql.SQLException;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.sql.Datum;
import oracle.sql.STRUCT;
import oracle.jpub.runtime.MutableStruct;

public class FavReportParamType implements ORAData, ORADataFactory
{
  public static final String _SQL_NAME = "APP.FAV_REPORT_PARAM_TYPE";
  public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

  protected MutableStruct _struct;

  protected static int[] _sqlType =  { 2,12,12,12,2 };
  protected static ORADataFactory[] _factory = new ORADataFactory[5];
  protected static final FavReportParamType _FavReportParamTypeFactory = new FavReportParamType();

  public static ORADataFactory getORADataFactory()
  { return _FavReportParamTypeFactory; }
  /* constructors */
  protected void _init_struct(boolean init)
  { if (init) _struct = new MutableStruct(new Object[5], _sqlType, _factory); }
  public FavReportParamType()
  { _init_struct(true); }
  public FavReportParamType(java.math.BigDecimal favId, String paramName, String paramValue, String paramExpression, java.math.BigDecimal seqId) throws SQLException
  { _init_struct(true);
    setFavId(favId);
    setParamName(paramName);
    setParamValue(paramValue);
    setParamExpression(paramExpression);
    setSeqId(seqId);
  }

  /* ORAData interface */
  public Datum toDatum(Connection c) throws SQLException
  {
    return _struct.toDatum(c, _SQL_NAME);
  }


  /* ORADataFactory interface */
  public ORAData create(Datum d, int sqlType) throws SQLException
  { return create(null, d, sqlType); }
  protected ORAData create(FavReportParamType o, Datum d, int sqlType) throws SQLException
  {
    if (d == null) return null; 
    if (o == null) o = new FavReportParamType();
    o._struct = new MutableStruct((STRUCT) d, _sqlType, _factory);
    return o;
  }
  /* accessor methods */
  public java.math.BigDecimal getFavId() throws SQLException
  { return (java.math.BigDecimal) _struct.getAttribute(0); }

  public void setFavId(java.math.BigDecimal favId) throws SQLException
  { _struct.setAttribute(0, favId); }


  public String getParamName() throws SQLException
  { return (String) _struct.getAttribute(1); }

  public void setParamName(String paramName) throws SQLException
  { _struct.setAttribute(1, paramName); }


  public String getParamValue() throws SQLException
  { return (String) _struct.getAttribute(2); }

  public void setParamValue(String paramValue) throws SQLException
  { _struct.setAttribute(2, paramValue); }


  public String getParamExpression() throws SQLException
  { return (String) _struct.getAttribute(3); }

  public void setParamExpression(String paramExpression) throws SQLException
  { _struct.setAttribute(3, paramExpression); }


  public java.math.BigDecimal getSeqId() throws SQLException
  { return (java.math.BigDecimal) _struct.getAttribute(4); }

  public void setSeqId(java.math.BigDecimal seqId) throws SQLException
  { _struct.setAttribute(4, seqId); }

}
