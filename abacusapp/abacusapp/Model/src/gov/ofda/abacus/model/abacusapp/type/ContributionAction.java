package gov.ofda.abacus.model.abacusapp.type;

import java.io.Serializable;

import java.math.BigDecimal;

public class ContributionAction implements Serializable {
    @SuppressWarnings("compatibility:6452681227360461070")
    private static final long serialVersionUID = 1L;
    private BigDecimal actionId;
    private BigDecimal contributionLinkTypeId;
    private Boolean isPrimaryAction;
    private String name;
    public ContributionAction() {
        super();
    }
    public ContributionAction(BigDecimal actionId,BigDecimal contributionLinkTypeId, Boolean isPrimaryAction,String name) {
        super();
        this.actionId=actionId;
        this.contributionLinkTypeId=contributionLinkTypeId;
        this.isPrimaryAction=isPrimaryAction;
        this.name=name;
            
    }
    public ContributionAction(BigDecimal actionId,BigDecimal contributionLinkTypeId, String name) {
        super();
        this.actionId=actionId;
        this.contributionLinkTypeId=contributionLinkTypeId;
        this.isPrimaryAction=false;
        this.name=name;
            
    }

    public void setActionId(BigDecimal actionId) {
        this.actionId = actionId;
    }

    public BigDecimal getActionId() {
        return actionId;
    }

    public void setContributionLinkTypeId(BigDecimal contributionLinkTypeId) {
        this.contributionLinkTypeId = contributionLinkTypeId;
    }

    public BigDecimal getContributionLinkTypeId() {
        return contributionLinkTypeId;
    }

    public void setIsPrimaryAction(Boolean isPrimaryAction) {
        this.isPrimaryAction = isPrimaryAction;
    }

    public Boolean getIsPrimaryAction() {
        return isPrimaryAction;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    @Override
    public boolean equals(Object o) {
        if (o == this) {
                    return true;
                }
     if(o instanceof ContributionAction) {
         ContributionAction object=(ContributionAction)o;
         if(object.getActionId()!=null && object.getActionId().compareTo(this.getActionId())==0)
            return true;
     }
     else if (o instanceof BigDecimal) {
         BigDecimal object=(BigDecimal)o;
         if(object!=null && object.compareTo(this.getActionId())==0)
           return true;
     }
     return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + actionId.hashCode();
        return result;
    }

}
