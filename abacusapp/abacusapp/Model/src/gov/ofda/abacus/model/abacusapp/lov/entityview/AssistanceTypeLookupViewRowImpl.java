package gov.ofda.abacus.model.abacusapp.lov.entityview;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;
import gov.ofda.abacus.model.abacusapp.lov.entity.AssistanceTypeLookupImpl;

import gov.ofda.abacus.model.abacusapp.lov.entityview.DisasterLookupViewRowImpl.AttributesEnum;

import java.sql.Timestamp;

import java.util.Arrays;

import oracle.jbo.RowSet;
import oracle.jbo.server.Entity;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Sep 26 16:28:35 EDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AssistanceTypeLookupViewRowImpl extends AbacusViewRowImpl {
    public static final int ENTITY_ASSISTANCETYPELOOKUP = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        AssistanceCode,
        AssistanceName,
        AssistanceAcronym,
        Active,
        UpdId,
        UpdDate,
        YesNoLOV1;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int ASSISTANCECODE = AttributesEnum.AssistanceCode.index();
    public static final int ASSISTANCENAME = AttributesEnum.AssistanceName.index();
    public static final int ASSISTANCEACRONYM = AttributesEnum.AssistanceAcronym.index();
    public static final int ACTIVE = AttributesEnum.Active.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int YESNOLOV1 = AttributesEnum.YesNoLOV1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AssistanceTypeLookupViewRowImpl() {
    }

    /**
     * Gets AssistanceTypeLookup entity object.
     * @return the AssistanceTypeLookup
     */
    public AssistanceTypeLookupImpl getAssistanceTypeLookup() {
        return (AssistanceTypeLookupImpl) getEntity(ENTITY_ASSISTANCETYPELOOKUP);
    }

    /**
     * Gets the attribute value for ASSISTANCE_CODE using the alias name AssistanceCode.
     * @return the ASSISTANCE_CODE
     */
    public String getAssistanceCode() {
        return (String) getAttributeInternal(ASSISTANCECODE);
    }

    /**
     * Sets <code>value</code> as attribute value for ASSISTANCE_CODE using the alias name AssistanceCode.
     * @param value value to set the ASSISTANCE_CODE
     */
    public void setAssistanceCode(String value) {
        setAttributeInternal(ASSISTANCECODE, value);
    }

    /**
     * Gets the attribute value for ASSISTANCE_NAME using the alias name AssistanceName.
     * @return the ASSISTANCE_NAME
     */
    public String getAssistanceName() {
        return (String) getAttributeInternal(ASSISTANCENAME);
    }

    /**
     * Sets <code>value</code> as attribute value for ASSISTANCE_NAME using the alias name AssistanceName.
     * @param value value to set the ASSISTANCE_NAME
     */
    public void setAssistanceName(String value) {
        setAttributeInternal(ASSISTANCENAME, value);
    }

    /**
     * Gets the attribute value for ASSISTANCE_ACRONYM using the alias name AssistanceAcronym.
     * @return the ASSISTANCE_ACRONYM
     */
    public String getAssistanceAcronym() {
        return (String) getAttributeInternal(ASSISTANCEACRONYM);
    }

    /**
     * Sets <code>value</code> as attribute value for ASSISTANCE_ACRONYM using the alias name AssistanceAcronym.
     * @param value value to set the ASSISTANCE_ACRONYM
     */
    public void setAssistanceAcronym(String value) {
        setAttributeInternal(ASSISTANCEACRONYM, value);
    }

    /**
     * Gets the attribute value for ACTIVE using the alias name Active.
     * @return the ACTIVE
     */
    public String getActive() {
        return (String) getAttributeInternal(ACTIVE);
    }

    /**
     * Sets <code>value</code> as attribute value for ACTIVE using the alias name Active.
     * @param value value to set the ACTIVE
     */
    public void setActive(String value) {
        setAttributeInternal(ACTIVE, value);
    }

    /**
     * Gets the attribute value for UPD_ID using the alias name UpdId.
     * @return the UPD_ID
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Gets the attribute value for UPD_DATE using the alias name UpdDate.
     * @return the UPD_DATE
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }

    /**
     * Gets the view accessor <code>RowSet</code> YesNoLOV1.
     */
    public RowSet getYesNoLOV1() {
        return (RowSet) getAttributeInternal(YESNOLOV1);
    }
    @Override
    public boolean isAttributeUpdateable(int i) {
        if((this.getEntity(0).getEntityState() > Entity.STATUS_NEW) && i == AttributesEnum.AssistanceCode.index()){
                   return false;
        }
        return super.isAttributeUpdateable(i);
    }
}

