package gov.ofda.abacus.model.abacusapp.lov.entity;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;

import java.sql.Timestamp;

import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;

import oracle.jbo.Key;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.domain.Number;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.TransactionEvent;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Feb 10 10:46:21 EST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CountryLookupImpl extends AbacusEntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CountryCode,
        RegionCode,
        CountryName,
        CountryUpdatedId,
        CountryUpdatedDate,
        RegionLookup;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int COUNTRYCODE = AttributesEnum.CountryCode.index();
    public static final int REGIONCODE = AttributesEnum.RegionCode.index();
    public static final int COUNTRYNAME = AttributesEnum.CountryName.index();
    public static final int COUNTRYUPDATEDID = AttributesEnum.CountryUpdatedId.index();
    public static final int COUNTRYUPDATEDDATE = AttributesEnum.CountryUpdatedDate.index();
    public static final int REGIONLOOKUP = AttributesEnum.RegionLookup.index();

    /**
     * This is the default constructor (do not remove).
     */
    public CountryLookupImpl() {
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("gov.ofda.abacus.model.abacusapp.lov.entity.CountryLookup");
    }


    /**
     * Gets the attribute value for CountryCode, using the alias name CountryCode.
     * @return the value of CountryCode
     */
    public Integer getCountryCode() {
        return (Integer) getAttributeInternal(COUNTRYCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for CountryCode.
     * @param value value to set the CountryCode
     */
    public void setCountryCode(Integer value) {
        setAttributeInternal(COUNTRYCODE, value);
    }

    /**
     * Gets the attribute value for RegionCode, using the alias name RegionCode.
     * @return the value of RegionCode
     */
    public String getRegionCode() {
        return (String) getAttributeInternal(REGIONCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for RegionCode.
     * @param value value to set the RegionCode
     */
    public void setRegionCode(String value) {
        setAttributeInternal(REGIONCODE, value);
    }

    /**
     * Gets the attribute value for CountryName, using the alias name CountryName.
     * @return the value of CountryName
     */
    public String getCountryName() {
        return (String) getAttributeInternal(COUNTRYNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for CountryName.
     * @param value value to set the CountryName
     */
    public void setCountryName(String value) {
        setAttributeInternal(COUNTRYNAME, value);
    }

    /**
     * Gets the attribute value for CountryUpdatedId, using the alias name CountryUpdatedId.
     * @return the value of CountryUpdatedId
     */
    public String getCountryUpdatedId() {
        return (String) getAttributeInternal(COUNTRYUPDATEDID);
    }

    /**
     * Sets <code>value</code> as the attribute value for CountryUpdatedId.
     * @param value value to set the CountryUpdatedId
     */
    public void setCountryUpdatedId(String value) {
        setAttributeInternal(COUNTRYUPDATEDID, value);
    }

    /**
     * Gets the attribute value for CountryUpdatedDate, using the alias name CountryUpdatedDate.
     * @return the value of CountryUpdatedDate
     */
    public Timestamp getCountryUpdatedDate() {
        return (Timestamp) getAttributeInternal(COUNTRYUPDATEDDATE);
    }

    /**
     * @return the associated entity gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl.
     */
    public RegionLookupImpl getRegionLookup() {
        return (RegionLookupImpl) getAttributeInternal(REGIONLOOKUP);
    }

    /**
     * Sets <code>value</code> as the associated entity gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl.
     */
    public void setRegionLookup(RegionLookupImpl value) {
        setAttributeInternal(REGIONLOOKUP, value);
    }


    /**
     * @param countryCode key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(Integer countryCode) {
        return new Key(new Object[] { countryCode });
    }

    /**
     * Add locking logic here.
     */
    public void lock() {
        super.lock();
    }

    /**
     * Custom DML update/insert/delete logic here.
     * @param operation the operation type
     * @param e the transaction event
     */
    protected void doDML(int operation, TransactionEvent e) {
        super.doDML(operation, e);
    }
    @Override
        protected void prepareForDML(int i, TransactionEvent transactionEvent) {
            if(i==DML_UPDATE || i==DML_INSERT)
            {
            ADFContext adfCtx = ADFContext.getCurrent();  
            SecurityContext secCntx = adfCtx.getSecurityContext();  
            String user = secCntx.getUserName(); 
            if(user!=null)
                setAttributeInternal(COUNTRYUPDATEDID,user.toUpperCase());
            }
            super.prepareForDML(i, transactionEvent);
        }

}

