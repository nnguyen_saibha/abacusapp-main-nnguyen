package gov.ofda.abacus.model.abacusapp.action.entityview;

import gov.ofda.abacus.model.abacusapp.action.entity.PaRgImpl;
import gov.ofda.abacus.model.abacusapp.action.entity.ProcurementActionImpl;
import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;
import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.Row;
import oracle.jbo.RowSet;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Sep 14 15:52:11 EDT 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PaRgViewRowImpl extends AbacusViewRowImpl {


    public static final int ENTITY_PARG = 0;
    public static final int ENTITY_PROCUREMENTACTION = 1;

    @Override
    public boolean isAttributeUpdateable(int i) {
        if(getPAEditView()!=null && !(Boolean)(getPAEditView().getAttribute("IsEditable")))
            return false;
        return super.isAttributeUpdateable(i);
    }
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ActionId,
        RgCode,
        Comments,
        UpdId,
        UpdDate,
        PgRevId,
        ActionId1,
        UpdDate1,
        PAEditView,
        RestrictedGoodsLOV1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ACTIONID = AttributesEnum.ActionId.index();
    public static final int RGCODE = AttributesEnum.RgCode.index();
    public static final int COMMENTS = AttributesEnum.Comments.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int PGREVID = AttributesEnum.PgRevId.index();
    public static final int ACTIONID1 = AttributesEnum.ActionId1.index();
    public static final int UPDDATE1 = AttributesEnum.UpdDate1.index();
    public static final int PAEDITVIEW = AttributesEnum.PAEditView.index();
    public static final int RESTRICTEDGOODSLOV1 = AttributesEnum.RestrictedGoodsLOV1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PaRgViewRowImpl() {
    }

    /**
     * Gets PaRg entity object.
     * @return the PaRg
     */
    public PaRgImpl getPaRg() {
        return (PaRgImpl) getEntity(ENTITY_PARG);
    }

    /**
     * Gets ProcurementAction entity object.
     * @return the ProcurementAction
     */
    public ProcurementActionImpl getProcurementAction() {
        return (ProcurementActionImpl) getEntity(ENTITY_PROCUREMENTACTION);
    }

    /**
     * Gets the attribute value for ACTION_ID using the alias name ActionId.
     * @return the ACTION_ID
     */
    public BigDecimal getActionId() {
        return (BigDecimal) getAttributeInternal(ACTIONID);
    }

    /**
     * Sets <code>value</code> as attribute value for ACTION_ID using the alias name ActionId.
     * @param value value to set the ACTION_ID
     */
    public void setActionId(BigDecimal value) {
        setAttributeInternal(ACTIONID, value);
    }

    /**
     * Gets the attribute value for RG_CODE using the alias name RgCode.
     * @return the RG_CODE
     */
    public String getRgCode() {
        return (String) getAttributeInternal(RGCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for RG_CODE using the alias name RgCode.
     * @param value value to set the RG_CODE
     */
    public void setRgCode(String value) {
        setAttributeInternal(RGCODE, value);
    }

    /**
     * Gets the attribute value for COMMENTS using the alias name Comments.
     * @return the COMMENTS
     */
    public String getComments() {
        return (String) getAttributeInternal(COMMENTS);
    }

    /**
     * Sets <code>value</code> as attribute value for COMMENTS using the alias name Comments.
     * @param value value to set the COMMENTS
     */
    public void setComments(String value) {
        setAttributeInternal(COMMENTS, value);
    }

    /**
     * Gets the attribute value for UPD_ID using the alias name UpdId.
     * @return the UPD_ID
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Gets the attribute value for UPD_DATE using the alias name UpdDate.
     * @return the UPD_DATE
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }

    /**
     * Gets the attribute value for PG_REV_ID using the alias name PgRevId.
     * @return the PG_REV_ID
     */
    public BigDecimal getPgRevId() {
        return (BigDecimal) getAttributeInternal(PGREVID);
    }

    /**
     * Sets <code>value</code> as attribute value for PG_REV_ID using the alias name PgRevId.
     * @param value value to set the PG_REV_ID
     */
    public void setPgRevId(BigDecimal value) {
        setAttributeInternal(PGREVID, value);
    }

    /**
     * Gets the attribute value for ACTION_ID using the alias name ActionId1.
     * @return the ACTION_ID
     */
    public BigDecimal getActionId1() {
        return (BigDecimal) getAttributeInternal(ACTIONID1);
    }

    /**
     * Sets <code>value</code> as attribute value for ACTION_ID using the alias name ActionId1.
     * @param value value to set the ACTION_ID
     */
    public void setActionId1(BigDecimal value) {
        setAttributeInternal(ACTIONID1, value);
    }

    /**
     * Gets the attribute value for UPD_DATE using the alias name UpdDate1.
     * @return the UPD_DATE
     */
    public Timestamp getUpdDate1() {
        return (Timestamp) getAttributeInternal(UPDDATE1);
    }

    /**
     * Gets the associated <code>Row</code> using master-detail link PAEditView.
     */
    public Row getPAEditView() {
        return (Row) getAttributeInternal(PAEDITVIEW);
    }

    /**
     * Sets the master-detail link PAEditView between this object and <code>value</code>.
     */
    public void setPAEditView(Row value) {
        setAttributeInternal(PAEDITVIEW, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> RestrictedGoodsLOV1.
     */
    public RowSet getRestrictedGoodsLOV1() {
        return (RowSet) getAttributeInternal(RESTRICTEDGOODSLOV1);
    }
}

