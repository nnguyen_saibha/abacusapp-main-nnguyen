package gov.ofda.abacus.model.abacusapp.pojo;

import java.io.Serializable;

public class CustomPurposeItem implements Serializable {
    @SuppressWarnings("compatibility:7420995740718105684")
    private static final long serialVersionUID = 1L;
    private Long code;
    private String name;
    private String displayName;
    private String isFoodSecurity;
    public CustomPurposeItem() {
        super();
    }
    public CustomPurposeItem(Long code,String name, String displayName,String isFoodSecurity) {
        super();
        this.code=code;
        this.name=name;
        this.displayName=displayName;
        this.isFoodSecurity=isFoodSecurity;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Long getCode() {
        return code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setIsFoodSecurity(String isFoodSecurity) {
        this.isFoodSecurity = isFoodSecurity;
    }

    public String getIsFoodSecurity() {
        return isFoodSecurity;
    }
}
