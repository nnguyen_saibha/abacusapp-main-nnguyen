package gov.ofda.abacus.model.abacusapp.module.common;

import gov.ofda.abacus.model.abacusapp.type.ContributionAction;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import oracle.jbo.ApplicationModule;
import oracle.jbo.domain.Timestamp;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Aug 04 16:41:38 EDT 2014
// ---------------------------------------------------------------------
public interface PAabacusappModule extends ApplicationModule {
    void setOtherViewsCriteria();

    void applyCriteriaToAbacusDocs();

    Map getTotalAmounts();

    void setWhereClauseByID(String setID);

    List<String> uploadFiles(List files);

    void attachDocsToSource(Integer sourceType, List SourceList, List docIdsList);

    void uploadDocuments(List files, Integer sourceType, List sourceList);

    Integer getCurrentFY();


    String deleteActivity(Integer budgetFY, String projectNbr);

    boolean deleteActivityCheck(Integer budgetFY, String projectNbr);

    String getActivityHTMLDetails(Integer budgetFY, String projectNbr);

    String getUserEmail();

    void updateNewProjectList(Integer fy);

    void setNewProjectDetails(Integer fy, String projectNbr);

    void filterByActionAmtStatus(String whereClause);

    void resetAdvancedReportSearch();

    void resetRMTReportSearch();


    void addUserRecentReports();

    void removeUserRecentReports();

    List<String> getRGList();

    Boolean updateRGs(List rgList);

    Timestamp getCurrentDateTime();


    Long createAction(Map list);

    void initializeParentDetailSet(String parentActionId);

    String deleteAction(BigDecimal actionID);

    String deleteActionCheck(BigDecimal actionID);

    void setCreateActionFATLov(String createActionType);

    void setPAEditMode(String readOnly);

    String isLetterMemoValid(String letterMemoType, String letterMemoGroupCode, BigDecimal id);

    void setNewACL();

    void setNewINA();

    List<String> checkMissingINADetails();

    boolean changeProjectForAction(BigDecimal actionID, String budgetFy, String projectNbr);

    Map getFATMap();

    boolean isActionOriginalAward(BigDecimal actionID);

    boolean changeFATForAction(BigDecimal actionID, String fundingActionType);

    boolean changeCATForAction(BigDecimal actionID, String categoryCode, String actionTypeCode,
                               BigDecimal parentActionID, BigDecimal modActionID, String awardNbr);

    void setApplicableLetterMemoVC(boolean isGUApprovalLetters);

    String getAwardId(String awardSeq);

    List<String> getTagList();

    Boolean updateTags(List tagList);

    void saveASISTDetails();

    List<Object> counterListOnASIST();

    Boolean changeCostsheetStatus(BigDecimal paxActivitiesId, BigDecimal newStatus, String comments);

    List<ContributionAction> getContributionLinkedActionsList();

    String getUserName();

    void initCostSheet(BigDecimal paActivityId, BigDecimal paxActivitiesId);

    void recalculateCostSheet(BigDecimal paActivityId);

    boolean updateContributionLinkedActions(BigDecimal paxActivitiesId, List actionList);
}

