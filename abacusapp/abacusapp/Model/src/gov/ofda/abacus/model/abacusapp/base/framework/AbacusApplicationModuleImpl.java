package gov.ofda.abacus.model.abacusapp.base.framework;

import java.util.Hashtable;

import java.util.Iterator;
import java.util.Set;

import oracle.adf.share.ADFContext;

import oracle.adf.share.security.SecurityContext;

import oracle.jbo.server.ApplicationModuleImpl;

import org.w3c.dom.Document;

import org.w3c.dom.Element;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import org.w3c.dom.NodeList;
public class AbacusApplicationModuleImpl extends ApplicationModuleImpl {
    public AbacusApplicationModuleImpl() {
        super();
    }
    public String getUserName() {
        ADFContext adfCtx = ADFContext.getCurrent();
        SecurityContext secCntx = adfCtx.getSecurityContext();
        return (secCntx.getUserName().toUpperCase());
    }
}