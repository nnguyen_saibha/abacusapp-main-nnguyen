package gov.ofda.abacus.model.abacusapp.lov.entity;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;

import oracle.jbo.Key;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.TransactionEvent;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Jul 03 13:15:37 EDT 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CommodityLookupImpl extends AbacusEntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CommCode,
        CommName,
        CommDesc,
        CommType,
        PackingTypeCode,
        WbscmNbr,
        CommPrice,
        CommStatus,
        Active,
        CommPriceStartDate,
        CommPriceEndDate,
        CommPriceUpdDate,
        CommPriceUpdId,
        UpdDate,
        UpdId,
        BudgetFy,
        BudgetQtr;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int COMMCODE = AttributesEnum.CommCode.index();
    public static final int COMMNAME = AttributesEnum.CommName.index();
    public static final int COMMDESC = AttributesEnum.CommDesc.index();
    public static final int COMMTYPE = AttributesEnum.CommType.index();
    public static final int PACKINGTYPECODE = AttributesEnum.PackingTypeCode.index();
    public static final int WBSCMNBR = AttributesEnum.WbscmNbr.index();
    public static final int COMMPRICE = AttributesEnum.CommPrice.index();
    public static final int COMMSTATUS = AttributesEnum.CommStatus.index();
    public static final int ACTIVE = AttributesEnum.Active.index();
    public static final int COMMPRICESTARTDATE = AttributesEnum.CommPriceStartDate.index();
    public static final int COMMPRICEENDDATE = AttributesEnum.CommPriceEndDate.index();
    public static final int COMMPRICEUPDDATE = AttributesEnum.CommPriceUpdDate.index();
    public static final int COMMPRICEUPDID = AttributesEnum.CommPriceUpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int BUDGETFY = AttributesEnum.BudgetFy.index();
    public static final int BUDGETQTR = AttributesEnum.BudgetQtr.index();

    /**
     * This is the default constructor (do not remove).
     */
    public CommodityLookupImpl() {
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("gov.ofda.abacus.model.abacusapp.lov.entity.CommodityLookup");
    }


    /**
     * Gets the attribute value for CommCode, using the alias name CommCode.
     * @return the value of CommCode
     */
    public String getCommCode() {
        return (String) getAttributeInternal(COMMCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for CommCode.
     * @param value value to set the CommCode
     */
    public void setCommCode(String value) {
        setAttributeInternal(COMMCODE, value);
    }

    /**
     * Gets the attribute value for CommName, using the alias name CommName.
     * @return the value of CommName
     */
    public String getCommName() {
        return (String) getAttributeInternal(COMMNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for CommName.
     * @param value value to set the CommName
     */
    public void setCommName(String value) {
        setAttributeInternal(COMMNAME, value);
    }

    /**
     * Gets the attribute value for CommDesc, using the alias name CommDesc.
     * @return the value of CommDesc
     */
    public String getCommDesc() {
        return (String) getAttributeInternal(COMMDESC);
    }

    /**
     * Sets <code>value</code> as the attribute value for CommDesc.
     * @param value value to set the CommDesc
     */
    public void setCommDesc(String value) {
        setAttributeInternal(COMMDESC, value);
    }

    /**
     * Gets the attribute value for CommType, using the alias name CommType.
     * @return the value of CommType
     */
    public String getCommType() {
        return (String) getAttributeInternal(COMMTYPE);
    }

    /**
     * Sets <code>value</code> as the attribute value for CommType.
     * @param value value to set the CommType
     */
    public void setCommType(String value) {
        setAttributeInternal(COMMTYPE, value);
    }

    /**
     * Gets the attribute value for PackingTypeCode, using the alias name PackingTypeCode.
     * @return the value of PackingTypeCode
     */
    public String getPackingTypeCode() {
        return (String) getAttributeInternal(PACKINGTYPECODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for PackingTypeCode.
     * @param value value to set the PackingTypeCode
     */
    public void setPackingTypeCode(String value) {
        setAttributeInternal(PACKINGTYPECODE, value);
    }

    /**
     * Gets the attribute value for WbscmNbr, using the alias name WbscmNbr.
     * @return the value of WbscmNbr
     */
    public BigDecimal getWbscmNbr() {
        return (BigDecimal) getAttributeInternal(WBSCMNBR);
    }

    /**
     * Sets <code>value</code> as the attribute value for WbscmNbr.
     * @param value value to set the WbscmNbr
     */
    public void setWbscmNbr(BigDecimal value) {
        setAttributeInternal(WBSCMNBR, value);
    }

    /**
     * Gets the attribute value for CommPrice, using the alias name CommPrice.
     * @return the value of CommPrice
     */
    public BigDecimal getCommPrice() {
        return (BigDecimal) getAttributeInternal(COMMPRICE);
    }

    /**
     * Sets <code>value</code> as the attribute value for CommPrice.
     * @param value value to set the CommPrice
     */
    public void setCommPrice(BigDecimal value) {
        setAttributeInternal(COMMPRICE, value);
    }

    /**
     * Gets the attribute value for CommStatus, using the alias name CommStatus.
     * @return the value of CommStatus
     */
    public String getCommStatus() {
        return (String) getAttributeInternal(COMMSTATUS);
    }

    /**
     * Sets <code>value</code> as the attribute value for CommStatus.
     * @param value value to set the CommStatus
     */
    public void setCommStatus(String value) {
        setAttributeInternal(COMMSTATUS, value);
    }

    /**
     * Gets the attribute value for Active, using the alias name Active.
     * @return the value of Active
     */
    public String getActive() {
        return (String) getAttributeInternal(ACTIVE);
    }

    /**
     * Sets <code>value</code> as the attribute value for Active.
     * @param value value to set the Active
     */
    public void setActive(String value) {
        setAttributeInternal(ACTIVE, value);
    }

    /**
     * Gets the attribute value for CommPriceStartDate, using the alias name CommPriceStartDate.
     * @return the value of CommPriceStartDate
     */
    public Timestamp getCommPriceStartDate() {
        return (Timestamp) getAttributeInternal(COMMPRICESTARTDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for CommPriceStartDate.
     * @param value value to set the CommPriceStartDate
     */
    public void setCommPriceStartDate(Timestamp value) {
        setAttributeInternal(COMMPRICESTARTDATE, value);
    }

    /**
     * Gets the attribute value for CommPriceEndDate, using the alias name CommPriceEndDate.
     * @return the value of CommPriceEndDate
     */
    public Timestamp getCommPriceEndDate() {
        return (Timestamp) getAttributeInternal(COMMPRICEENDDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for CommPriceEndDate.
     * @param value value to set the CommPriceEndDate
     */
    public void setCommPriceEndDate(Timestamp value) {
        setAttributeInternal(COMMPRICEENDDATE, value);
    }

    /**
     * Gets the attribute value for CommPriceUpdDate, using the alias name CommPriceUpdDate.
     * @return the value of CommPriceUpdDate
     */
    public Timestamp getCommPriceUpdDate() {
        return (Timestamp) getAttributeInternal(COMMPRICEUPDDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for CommPriceUpdDate.
     * @param value value to set the CommPriceUpdDate
     */
    public void setCommPriceUpdDate(Timestamp value) {
        setAttributeInternal(COMMPRICEUPDDATE, value);
    }

    /**
     * Gets the attribute value for CommPriceUpdId, using the alias name CommPriceUpdId.
     * @return the value of CommPriceUpdId
     */
    public String getCommPriceUpdId() {
        return (String) getAttributeInternal(COMMPRICEUPDID);
    }

    /**
     * Sets <code>value</code> as the attribute value for CommPriceUpdId.
     * @param value value to set the CommPriceUpdId
     */
    public void setCommPriceUpdId(String value) {
        setAttributeInternal(COMMPRICEUPDID, value);
    }

    /**
     * Gets the attribute value for UpdDate, using the alias name UpdDate.
     * @return the value of UpdDate
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }


    /**
     * Gets the attribute value for UpdId, using the alias name UpdId.
     * @return the value of UpdId
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Sets <code>value</code> as the attribute value for UpdId.
     * @param value value to set the UpdId
     */
    public void setUpdId(String value) {
        setAttributeInternal(UPDID, value);
    }


    /**
     * Gets the attribute value for BudgetFy, using the alias name BudgetFy.
     * @return the value of BudgetFy
     */
    public Integer getBudgetFy() {
        return (Integer) getAttributeInternal(BUDGETFY);
    }

    /**
     * Sets <code>value</code> as the attribute value for BudgetFy.
     * @param value value to set the BudgetFy
     */
    public void setBudgetFy(Integer value) {
        setAttributeInternal(BUDGETFY, value);
    }

    /**
     * Gets the attribute value for BudgetQtr, using the alias name BudgetQtr.
     * @return the value of BudgetQtr
     */
    public Integer getBudgetQtr() {
        return (Integer) getAttributeInternal(BUDGETQTR);
    }

    /**
     * Sets <code>value</code> as the attribute value for BudgetQtr.
     * @param value value to set the BudgetQtr
     */
    public void setBudgetQtr(Integer value) {
        setAttributeInternal(BUDGETQTR, value);
    }

    /**
     * @param commCode key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(String commCode) {
        return new Key(new Object[] { commCode });
    }

    @Override
        protected void prepareForDML(int i, TransactionEvent transactionEvent) {
            if(i==DML_UPDATE || i==DML_INSERT)
            {
            ADFContext adfCtx = ADFContext.getCurrent();  
            SecurityContext secCntx = adfCtx.getSecurityContext();  
            String user = secCntx.getUserName(); 
            if(user!=null)
                setAttributeInternal(UPDID,user.toUpperCase());
            }
            super.prepareForDML(i, transactionEvent);
        }


}

