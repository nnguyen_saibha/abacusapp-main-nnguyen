package gov.ofda.abacus.model.abacusapp.type;

import java.math.BigDecimal;

import java.util.ArrayList;

public  class PASector {
        public String status;
        public String code;
        public ArrayList<String> changedList=new ArrayList<String>();
        public String oldCode;
        public String newCode;
        public String oldAmt="";
        public String newAmt="";
        public BigDecimal oldAssistanceType;
        public BigDecimal newAssistanceType;
        public String oldMC="";
        public String newMC="";

        public PASector(String status, String code) {
            super();
            this.status = status;
            this.code = code;
            
        }

        public PASector() {
            super();
        }
    }