package gov.ofda.abacus.model.abacusapp.messages;

import oracle.jbo.common.CheckedListResourceBundle;

public class AbacusappMessageBundle extends CheckedListResourceBundle {
    private static final Object[][] sMessageStrings 
      = new String[][] {
         {"27014","You must provide a value for {2}"},
         {"PK_ACTIVITY","This Project for that Fiscal Year has already been created"},
         {"PK_FUND_LOKUP", "Record with Fiscal Year and Fund Code combination already exists"},
         {"PK_ACTIVITY_NEW","This Project for that Fiscal Year has already been created"},
         {"FK_SECTOR_SUBSECTOR","This sector has sub sectors attached. Sector cannot be changed without removing sub sectors"},
         {"26092", "Another user is changing the Data. Cancel your changes and try again after sometime. If this persists, contact Abacus Team abacus@ofda.gov"},
         {"UK_PA_SECTOR","Sector already exists"},
         {"FK_ACTIVITY_PROJECT", "Please delete all Project related actions before deleting Project."},
        {"UK_PA_FUNDING_ACTID_FUND_FLINK","Fund Code, Program Area/Element combination already exists"},
        {"FK_BUDGET_OFDA","Record cannot be deleted as child record exists in Office Budget"},
        {"FK_FA_FUNDCODE","Record cannot be deleted as child record exists in Project Funds"},
        {"FK_FD_FUNDCODE","Record cannot be deleted as child record exists in Division Funds"},
        {"FK_FO_FUNDCODE","Record cannot be deleted as child record exists in Office Funds"},
        {"FK_PHOENIX_FUNDS","Record cannot be deleted as child record exists in Reconciliation"},
        {"PK_ABACUS_REPORTS_PARAMS","Paramater already exists. Choose different parameter or delete one"},
        {"UK_MANAGEMENT_CODE" , "Management Code already exists."},
        {"FK_PASECTOR_PA_PURPOSE_ID","Unexpected Issue. Please cancel your changes and try again. If adding a new Purpose, make sure Purpose Name is entered first and then select in Sectors Tab."}};
    
    public AbacusappMessageBundle() {
        super();
    }

    @Override
    public Object[][] getContents() {
        return sMessageStrings;
    }
}
