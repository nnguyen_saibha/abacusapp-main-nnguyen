package gov.ofda.abacus.model.abacusapp.type;

import java.sql.SQLException;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.sql.Datum;
import oracle.sql.STRUCT;
import oracle.jpub.runtime.MutableStruct;

public class FavReportType implements ORAData, ORADataFactory
{
  public static final String _SQL_NAME = "APP.FAV_REPORT_TYPE";
  public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

  protected MutableStruct _struct;

  protected static int[] _sqlType =  { 2,2,12,12,12,12,12,91,1,2 };
  protected static ORADataFactory[] _factory = new ORADataFactory[10];
  protected static final FavReportType _FavReportTypeFactory = new FavReportType();

  public static ORADataFactory getORADataFactory()
  { return _FavReportTypeFactory; }
  /* constructors */
  protected void _init_struct(boolean init)
  { if (init) _struct = new MutableStruct(new Object[10], _sqlType, _factory); }
  public FavReportType()
  { _init_struct(true); }
  public FavReportType(java.math.BigDecimal favId, java.math.BigDecimal repId, String favRepName, String desName, String outputFormat, String OwnerId, String modifiedBy, java.sql.Timestamp modifiedOn, String active, java.math.BigDecimal favTypeId) throws SQLException
  { _init_struct(true);
    setFavId(favId);
    setRepId(repId);
    setFavRepName(favRepName);
    setDesName(desName);
    setOutputFormat(outputFormat);
    setOwnerId(OwnerId);
    setModifiedBy(modifiedBy);
    setModifiedOn(modifiedOn);
    setActive(active);
    setFavTypeId(favTypeId);
  }

  /* ORAData interface */
  public Datum toDatum(Connection c) throws SQLException
  {
    return _struct.toDatum(c, _SQL_NAME);
  }


  /* ORADataFactory interface */
  public ORAData create(Datum d, int sqlType) throws SQLException
  { return create(null, d, sqlType); }
  protected ORAData create(FavReportType o, Datum d, int sqlType) throws SQLException
  {
    if (d == null) return null; 
    if (o == null) o = new FavReportType();
    o._struct = new MutableStruct((STRUCT) d, _sqlType, _factory);
    return o;
  }
  /* accessor methods */
  public java.math.BigDecimal getFavId() throws SQLException
  { return (java.math.BigDecimal) _struct.getAttribute(0); }

  public void setFavId(java.math.BigDecimal favId) throws SQLException
  { _struct.setAttribute(0, favId); }


  public java.math.BigDecimal getRepId() throws SQLException
  { return (java.math.BigDecimal) _struct.getAttribute(1); }

  public void setRepId(java.math.BigDecimal repId) throws SQLException
  { _struct.setAttribute(1, repId); }


  public String getFavRepName() throws SQLException
  { return (String) _struct.getAttribute(2); }

  public void setFavRepName(String favRepName) throws SQLException
  { _struct.setAttribute(2, favRepName); }


  public String getDesName() throws SQLException
  { return (String) _struct.getAttribute(3); }

  public void setDesName(String desName) throws SQLException
  { _struct.setAttribute(3, desName); }


  public String getOutputFormat() throws SQLException
  { return (String) _struct.getAttribute(4); }

  public void setOutputFormat(String outputFormat) throws SQLException
  { _struct.setAttribute(4, outputFormat); }


  public String getOwnerId() throws SQLException
  { return (String) _struct.getAttribute(5); }

  public void setOwnerId(String ownerId) throws SQLException
  { _struct.setAttribute(5, ownerId); }


  public String getModifiedBy() throws SQLException
  { return (String) _struct.getAttribute(6); }

  public void setModifiedBy(String modifiedBy) throws SQLException
  { _struct.setAttribute(6, modifiedBy); }


  public java.sql.Timestamp getModifiedOn() throws SQLException
  { return (java.sql.Timestamp) _struct.getAttribute(7); }

  public void setModifiedOn(java.sql.Timestamp modifiedOn) throws SQLException
  { _struct.setAttribute(7, modifiedOn); }


  public String getActive() throws SQLException
  { return (String) _struct.getAttribute(8); }

  public void setActive(String active) throws SQLException
  { _struct.setAttribute(8, active); }


  public java.math.BigDecimal getFavTypeId() throws SQLException
  { return (java.math.BigDecimal) _struct.getAttribute(9); }

  public void setFavTypeId(java.math.BigDecimal favTypeId) throws SQLException
  { _struct.setAttribute(9, favTypeId); }

}
