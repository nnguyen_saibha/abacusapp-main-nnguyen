package gov.ofda.abacus.model.abacusapp.type;

import java.sql.SQLException;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.sql.Datum;
import oracle.sql.REF;
import oracle.sql.STRUCT;

public class FavReportTypeRef implements ORAData, ORADataFactory
{
  public static final String _SQL_BASETYPE = "APP.FAV_REPORT_TYPE";
  public static final int _SQL_TYPECODE = OracleTypes.REF;

  REF _ref;

private static final FavReportTypeRef _FavReportTypeRefFactory = new FavReportTypeRef();

  public static ORADataFactory getORADataFactory()
  { return _FavReportTypeRefFactory; }
  /* constructor */
  public FavReportTypeRef()
  {
  }

  /* ORAData interface */
  public Datum toDatum(Connection c) throws SQLException
  {
    return _ref;
  }

  /* ORADataFactory interface */
  public ORAData create(Datum d, int sqlType) throws SQLException
  {
    if (d == null) return null; 
    FavReportTypeRef r = new FavReportTypeRef();
    r._ref = (REF) d;
    return r;
  }

  public static FavReportTypeRef cast(ORAData o) throws SQLException
  {
     if (o == null) return null;
     try { return (FavReportTypeRef) getORADataFactory().create(o.toDatum(null), OracleTypes.REF); }
     catch (Exception exn)
     { throw new SQLException("Unable to convert "+o.getClass().getName()+" to FavReportTypeRef: "+exn.toString()); }
  }

  public FavReportType getValue() throws SQLException
  {
     return (FavReportType) FavReportType.getORADataFactory().create(
       _ref.getSTRUCT(), OracleTypes.REF);
  }

  public void setValue(FavReportType c) throws SQLException
  {
    _ref.setValue((STRUCT) c.toDatum(_ref.getJavaSqlConnection()));
  }
}
