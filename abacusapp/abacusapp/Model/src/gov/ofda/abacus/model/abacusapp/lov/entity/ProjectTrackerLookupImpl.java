package gov.ofda.abacus.model.abacusapp.lov.entity;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;

import oracle.jbo.Key;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.TransactionEvent;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Mar 24 16:00:55 EDT 2020
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ProjectTrackerLookupImpl extends AbacusEntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ProjectTrackerId,
        ProjectTrackerName,
        Description,
        Acronym,
        SortOrder,
        Active,
        UpdId,
        UpdDate;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int PROJECTTRACKERID = AttributesEnum.ProjectTrackerId.index();
    public static final int PROJECTTRACKERNAME = AttributesEnum.ProjectTrackerName.index();
    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int ACRONYM = AttributesEnum.Acronym.index();
    public static final int SORTORDER = AttributesEnum.SortOrder.index();
    public static final int ACTIVE = AttributesEnum.Active.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ProjectTrackerLookupImpl() {
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("gov.ofda.abacus.model.abacusapp.lov.entity.ProjectTrackerLookup");
    }


    /**
     * Gets the attribute value for ProjectTrackerId, using the alias name ProjectTrackerId.
     * @return the value of ProjectTrackerId
     */
    public DBSequence getProjectTrackerId() {
        return (DBSequence) getAttributeInternal(PROJECTTRACKERID);
    }


    /**
     * Sets <code>value</code> as the attribute value for ProjectTrackerId.
     * @param value value to set the ProjectTrackerId
     */
    public void setProjectTrackerId(DBSequence value) {
        setAttributeInternal(PROJECTTRACKERID, value);
    }

    /**
     * Gets the attribute value for ProjectTrackerName, using the alias name ProjectTrackerName.
     * @return the value of ProjectTrackerName
     */
    public String getProjectTrackerName() {
        return (String) getAttributeInternal(PROJECTTRACKERNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for ProjectTrackerName.
     * @param value value to set the ProjectTrackerName
     */
    public void setProjectTrackerName(String value) {
        setAttributeInternal(PROJECTTRACKERNAME, value);
    }

    /**
     * Gets the attribute value for Description, using the alias name Description.
     * @return the value of Description
     */
    public String getDescription() {
        return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as the attribute value for Description.
     * @param value value to set the Description
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for Acronym, using the alias name Acronym.
     * @return the value of Acronym
     */
    public String getAcronym() {
        return (String) getAttributeInternal(ACRONYM);
    }

    /**
     * Sets <code>value</code> as the attribute value for Acronym.
     * @param value value to set the Acronym
     */
    public void setAcronym(String value) {
        setAttributeInternal(ACRONYM, value);
    }

    /**
     * Gets the attribute value for SortOrder, using the alias name SortOrder.
     * @return the value of SortOrder
     */
    public Integer getSortOrder() {
        return (Integer) getAttributeInternal(SORTORDER);
    }

    /**
     * Sets <code>value</code> as the attribute value for SortOrder.
     * @param value value to set the SortOrder
     */
    public void setSortOrder(Integer value) {
        setAttributeInternal(SORTORDER, value);
    }

    /**
     * Gets the attribute value for Active, using the alias name Active.
     * @return the value of Active
     */
    public String getActive() {
        return (String) getAttributeInternal(ACTIVE);
    }

    /**
     * Sets <code>value</code> as the attribute value for Active.
     * @param value value to set the Active
     */
    public void setActive(String value) {
        setAttributeInternal(ACTIVE, value);
    }

    /**
     * Gets the attribute value for UpdId, using the alias name UpdId.
     * @return the value of UpdId
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Sets <code>value</code> as the attribute value for UpdId.
     * @param value value to set the UpdId
     */
    public void setUpdId(String value) {
        setAttributeInternal(UPDID, value);
    }

    /**
     * Gets the attribute value for UpdDate, using the alias name UpdDate.
     * @return the value of UpdDate
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }


    /**
     * @param projectTrackerId key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(DBSequence projectTrackerId) {
        return new Key(new Object[] { projectTrackerId });
    }

    @Override
        protected void prepareForDML(int i, TransactionEvent transactionEvent) {
            if(i==DML_UPDATE || i==DML_INSERT)
            {
            ADFContext adfCtx = ADFContext.getCurrent();  
            SecurityContext secCntx = adfCtx.getSecurityContext();  
            String user = secCntx.getUserName(); 
            if(user!=null)
                setAttributeInternal(UPDID,user.toUpperCase());
            }
            super.prepareForDML(i, transactionEvent);
        }
    
}

