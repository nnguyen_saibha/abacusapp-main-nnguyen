package gov.ofda.abacus.model.abacusapp.lov.view;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewObjectImpl;
import gov.ofda.abacus.model.abacusapp.lov.view.common.FactsLookupStaticView;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Oct 28 17:25:05 EDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class FactsLookupStaticViewImpl extends AbacusViewObjectImpl implements FactsLookupStaticView {
    /**
     * This is the default constructor (do not remove).
     */
    public FactsLookupStaticViewImpl() {
    }

    /**
     * Returns the variable value for pType.
     * @return variable value for pType
     */
    public String getpType() {
        return (String) ensureVariableManager().getVariableValue("pType");
    }

    /**
     * Sets <code>value</code> for variable pType.
     * @param value value to bind as pType
     */
    public void setpType(String value) {
        ensureVariableManager().setVariableValue("pType", value);
    }

    /**
     * Returns the variable value for pObj.
     * @return variable value for pObj
     */
    public String getpObj() {
        return (String) ensureVariableManager().getVariableValue("pObj");
    }

    /**
     * Sets <code>value</code> for variable pObj.
     * @param value value to bind as pObj
     */
    public void setpObj(String value) {
        ensureVariableManager().setVariableValue("pObj", value);
    }

    /**
     * Returns the variable value for pAreaCode.
     * @return variable value for pAreaCode
     */
    public String getpAreaCode() {
        return (String) ensureVariableManager().getVariableValue("pAreaCode");
    }

    /**
     * Sets <code>value</code> for variable pAreaCode.
     * @param value value to bind as pAreaCode
     */
    public void setpAreaCode(String value) {
        ensureVariableManager().setVariableValue("pAreaCode", value);
    }

    /**
     * Returns the variable value for pElementCode.
     * @return variable value for pElementCode
     */
    public String getpElementCode() {
        return (String) ensureVariableManager().getVariableValue("pElementCode");
    }

    /**
     * Sets <code>value</code> for variable pElementCode.
     * @param value value to bind as pElementCode
     */
    public void setpElementCode(String value) {
        ensureVariableManager().setVariableValue("pElementCode", value);
    }

    /**
     * Returns the variable value for pSubElementCode.
     * @return variable value for pSubElementCode
     */
    public String getpSubElementCode() {
        return (String) ensureVariableManager().getVariableValue("pSubElementCode");
    }

    /**
     * Sets <code>value</code> for variable pSubElementCode.
     * @param value value to bind as pSubElementCode
     */
    public void setpSubElementCode(String value) {
        ensureVariableManager().setVariableValue("pSubElementCode", value);
    }
}

