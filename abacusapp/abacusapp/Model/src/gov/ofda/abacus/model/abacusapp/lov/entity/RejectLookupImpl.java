package gov.ofda.abacus.model.abacusapp.lov.entity;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;

import java.sql.Timestamp;

import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;

import oracle.jbo.Key;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.TransactionEvent;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Feb 10 10:32:32 EST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class RejectLookupImpl extends AbacusEntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        RejectCode,
        RejectDescription,
        UpdId,
        UpdDate;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int REJECTCODE = AttributesEnum.RejectCode.index();
    public static final int REJECTDESCRIPTION = AttributesEnum.RejectDescription.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();

    /**
     * This is the default constructor (do not remove).
     */
    public RejectLookupImpl() {
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("gov.ofda.abacus.model.abacusapp.lov.entity.RejectLookup");
    }


    /**
     * Gets the attribute value for RejectCode, using the alias name RejectCode.
     * @return the value of RejectCode
     */
    public DBSequence getRejectCode() {
        return (DBSequence) getAttributeInternal(REJECTCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for RejectCode.
     * @param value value to set the RejectCode
     */
    public void setRejectCode(DBSequence value) {
        setAttributeInternal(REJECTCODE, value);
    }

    /**
     * Gets the attribute value for RejectDescription, using the alias name RejectDescription.
     * @return the value of RejectDescription
     */
    public String getRejectDescription() {
        return (String) getAttributeInternal(REJECTDESCRIPTION);
    }

    /**
     * Sets <code>value</code> as the attribute value for RejectDescription.
     * @param value value to set the RejectDescription
     */
    public void setRejectDescription(String value) {
        setAttributeInternal(REJECTDESCRIPTION, value);
    }

    /**
     * Gets the attribute value for UpdId, using the alias name UpdId.
     * @return the value of UpdId
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Sets <code>value</code> as the attribute value for UpdId.
     * @param value value to set the UpdId
     */
    public void setUpdId(String value) {
        setAttributeInternal(UPDID, value);
    }

    /**
     * Gets the attribute value for UpdDate, using the alias name UpdDate.
     * @return the value of UpdDate
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }


    /**
     * @param rejectCode key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(DBSequence rejectCode) {
        return new Key(new Object[] { rejectCode });
    }

    /**
     * Add locking logic here.
     */
    public void lock() {
        super.lock();
    }

    /**
     * Custom DML update/insert/delete logic here.
     * @param operation the operation type
     * @param e the transaction event
     */
    protected void doDML(int operation, TransactionEvent e) {
        super.doDML(operation, e);
    }

    @Override
        protected void prepareForDML(int i, TransactionEvent transactionEvent) {
            if(i==DML_UPDATE || i==DML_INSERT)
            {
            ADFContext adfCtx = ADFContext.getCurrent();  
            SecurityContext secCntx = adfCtx.getSecurityContext();  
            String user = secCntx.getUserName(); 
            if(user!=null)
                setAttributeInternal(UPDID,user.toUpperCase());
            }
            super.prepareForDML(i, transactionEvent);
        }

}

