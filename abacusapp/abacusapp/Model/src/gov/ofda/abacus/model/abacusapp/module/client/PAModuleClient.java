package gov.ofda.abacus.model.abacusapp.module.client;

import gov.ofda.abacus.model.abacusapp.module.common.PAabacusappModule;

import gov.ofda.abacus.model.abacusapp.type.ContributionAction;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;

import oracle.jbo.client.remote.ApplicationModuleImpl;
import oracle.jbo.domain.Timestamp;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Jan 06 12:12:56 EST 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PAModuleClient extends ApplicationModuleImpl implements PAabacusappModule {
    /**
     * This is the default constructor (do not remove).
     */
    public PAModuleClient() {
    }


    public void addUserRecentReports() {
        Object _ret = this.riInvokeExportedMethod(this, "addUserRecentReports", null, null);
        return;
    }

    public void applyCriteriaToAbacusDocs() {
        Object _ret = this.riInvokeExportedMethod(this, "applyCriteriaToAbacusDocs", null, null);
        return;
    }

    public void attachDocsToSource(Integer sourceType, List SourceList, List docIdsList) {
        Object _ret = this.riInvokeExportedMethod(this, "attachDocsToSource", new String[] {
                                                  "java.lang.Integer", "java.util.List", "java.util.List"
        }, new Object[] { sourceType, SourceList, docIdsList });
        return;
    }

    public boolean changeCATForAction(BigDecimal actionID, String categoryCode, String actionTypeCode,
                                      BigDecimal parentActionID, BigDecimal modActionID, String awardNbr) {
        Object _ret = this.riInvokeExportedMethod(this, "changeCATForAction", new String[] {
                                                  "java.math.BigDecimal", "java.lang.String", "java.lang.String",
                                                  "java.math.BigDecimal", "java.math.BigDecimal", "java.lang.String"
        }, new Object[] { actionID, categoryCode, actionTypeCode, parentActionID, modActionID, awardNbr });
        return ((Boolean) _ret).booleanValue();
    }

    public Boolean changeCostsheetStatus(BigDecimal paxActivitiesId, BigDecimal newStatus, String comments) {
        Object _ret = this.riInvokeExportedMethod(this, "changeCostsheetStatus", new String[] {
                                                  "java.math.BigDecimal", "java.math.BigDecimal", "java.lang.String"
        }, new Object[] { paxActivitiesId, newStatus, comments });
        return (Boolean) _ret;
    }

    public boolean changeFATForAction(BigDecimal actionID, String fundingActionType) {
        Object _ret = this.riInvokeExportedMethod(this, "changeFATForAction", new String[] {
                                                  "java.math.BigDecimal", "java.lang.String" }, new Object[] {
                                                  actionID, fundingActionType });
        return ((Boolean) _ret).booleanValue();
    }

    public boolean changeProjectForAction(BigDecimal actionID, String budgetFy, String projectNbr) {
        Object _ret = this.riInvokeExportedMethod(this, "changeProjectForAction", new String[] {
                                                  "java.math.BigDecimal", "java.lang.String", "java.lang.String"
        }, new Object[] { actionID, budgetFy, projectNbr });
        return ((Boolean) _ret).booleanValue();
    }

    public List<String> checkMissingINADetails() {
        Object _ret = this.riInvokeExportedMethod(this, "checkMissingINADetails", null, null);
        return (List<String>) _ret;
    }

    public List<Object> counterListOnASIST() {
        Object _ret = this.riInvokeExportedMethod(this, "counterListOnASIST", null, null);
        return (List<Object>) _ret;
    }

    public Long createAction(Map list) {
        Object _ret = this.riInvokeExportedMethod(this, "createAction", new String[] { "java.util.Map" }, new Object[] {
                                                  list });
        return (Long) _ret;
    }

    public String deleteAction(BigDecimal actionID) {
        Object _ret = this.riInvokeExportedMethod(this, "deleteAction", new String[] { "java.math.BigDecimal" }, new Object[] {
                                                  actionID });
        return (String) _ret;
    }

    public String deleteActionCheck(BigDecimal actionID) {
        Object _ret = this.riInvokeExportedMethod(this, "deleteActionCheck", new String[] { "java.math.BigDecimal" }, new Object[] {
                                                  actionID });
        return (String) _ret;
    }

    public String deleteActivity(Integer budgetFY, String projectNbr) {
        Object _ret = this.riInvokeExportedMethod(this, "deleteActivity", new String[] {
                                                  "java.lang.Integer", "java.lang.String" }, new Object[] {
                                                  budgetFY, projectNbr });
        return (String) _ret;
    }

    public boolean deleteActivityCheck(Integer budgetFY, String projectNbr) {
        Object _ret = this.riInvokeExportedMethod(this, "deleteActivityCheck", new String[] {
                                                  "java.lang.Integer", "java.lang.String" }, new Object[] {
                                                  budgetFY, projectNbr });
        return ((Boolean) _ret).booleanValue();
    }

    public void filterByActionAmtStatus(String whereClause) {
        Object _ret = this.riInvokeExportedMethod(this, "filterByActionAmtStatus", new String[] { "java.lang.String" }, new Object[] {
                                                  whereClause });
        return;
    }

    public String getActivityHTMLDetails(Integer budgetFY, String projectNbr) {
        Object _ret = this.riInvokeExportedMethod(this, "getActivityHTMLDetails", new String[] {
                                                  "java.lang.Integer", "java.lang.String" }, new Object[] {
                                                  budgetFY, projectNbr });
        return (String) _ret;
    }

    public String getAwardId(String awardSeq) {
        Object _ret = this.riInvokeExportedMethod(this, "getAwardId", new String[] { "java.lang.String" }, new Object[] {
                                                  awardSeq });
        return (String) _ret;
    }

    public List<ContributionAction> getContributionLinkedActionsList() {
        Object _ret = this.riInvokeExportedMethod(this, "getContributionLinkedActionsList", null, null);
        return (List<ContributionAction>) _ret;
    }

    public Timestamp getCurrentDateTime() {
        Object _ret = this.riInvokeExportedMethod(this, "getCurrentDateTime", null, null);
        return (Timestamp) _ret;
    }

    public Integer getCurrentFY() {
        Object _ret = this.riInvokeExportedMethod(this, "getCurrentFY", null, null);
        return (Integer) _ret;
    }

    public Map getFATMap() {
        Object _ret = this.riInvokeExportedMethod(this, "getFATMap", null, null);
        return (Map) _ret;
    }

    public List<String> getRGList() {
        Object _ret = this.riInvokeExportedMethod(this, "getRGList", null, null);
        return (List<String>) _ret;
    }

    public List<String> getTagList() {
        Object _ret = this.riInvokeExportedMethod(this, "getTagList", null, null);
        return (List<String>) _ret;
    }

    public Map getTotalAmounts() {
        Object _ret = this.riInvokeExportedMethod(this, "getTotalAmounts", null, null);
        return (Map) _ret;
    }

    public String getUserEmail() {
        Object _ret = this.riInvokeExportedMethod(this, "getUserEmail", null, null);
        return (String) _ret;
    }

    public String getUserName() {
        Object _ret = this.riInvokeExportedMethod(this, "getUserName", null, null);
        return (String) _ret;
    }

    public void initCostSheet(BigDecimal paActivityId, BigDecimal paxActivitiesId) {
        Object _ret = this.riInvokeExportedMethod(this, "initCostSheet", new String[] {
                                                  "java.math.BigDecimal", "java.math.BigDecimal" }, new Object[] {
                                                  paActivityId, paxActivitiesId
        });
        return;
    }

    public void initializeParentDetailSet(String parentActionId) {
        Object _ret = this.riInvokeExportedMethod(this, "initializeParentDetailSet", new String[] {
                                                  "java.lang.String" }, new Object[] { parentActionId });
        return;
    }

    public boolean isActionOriginalAward(BigDecimal actionID) {
        Object _ret = this.riInvokeExportedMethod(this, "isActionOriginalAward", new String[] {
                                                  "java.math.BigDecimal" }, new Object[] { actionID });
        return ((Boolean) _ret).booleanValue();
    }

    public String isLetterMemoValid(String letterMemoType, String letterMemoGroupCode, BigDecimal id) {
        Object _ret = this.riInvokeExportedMethod(this, "isLetterMemoValid", new String[] {
                                                  "java.lang.String", "java.lang.String", "java.math.BigDecimal"
        }, new Object[] { letterMemoType, letterMemoGroupCode, id });
        return (String) _ret;
    }

    public void recalculateCostSheet(BigDecimal paActivityId) {
        Object _ret = this.riInvokeExportedMethod(this, "recalculateCostSheet", new String[] { "java.math.BigDecimal" }, new Object[] {
                                                  paActivityId });
        return;
    }

    public void removeUserRecentReports() {
        Object _ret = this.riInvokeExportedMethod(this, "removeUserRecentReports", null, null);
        return;
    }

    public void resetAdvancedReportSearch() {
        Object _ret = this.riInvokeExportedMethod(this, "resetAdvancedReportSearch", null, null);
        return;
    }

    public void resetRMTReportSearch() {
        Object _ret = this.riInvokeExportedMethod(this, "resetRMTReportSearch", null, null);
        return;
    }

    public void saveASISTDetails() {
        Object _ret = this.riInvokeExportedMethod(this, "saveASISTDetails", null, null);
        return;
    }

    public void setApplicableLetterMemoVC(boolean isGUApprovalLetters) {
        Object _ret = this.riInvokeExportedMethod(this, "setApplicableLetterMemoVC", new String[] { "boolean" }, new Object[] {
                                                  new Boolean(isGUApprovalLetters) });
        return;
    }

    public void setCreateActionFATLov(String createActionType) {
        Object _ret = this.riInvokeExportedMethod(this, "setCreateActionFATLov", new String[] { "java.lang.String" }, new Object[] {
                                                  createActionType });
        return;
    }

    public void setNewACL() {
        Object _ret = this.riInvokeExportedMethod(this, "setNewACL", null, null);
        return;
    }

    public void setNewINA() {
        Object _ret = this.riInvokeExportedMethod(this, "setNewINA", null, null);
        return;
    }

    public void setNewProjectDetails(Integer fy, String projectNbr) {
        Object _ret = this.riInvokeExportedMethod(this, "setNewProjectDetails", new String[] {
                                                  "java.lang.Integer", "java.lang.String" }, new Object[] {
                                                  fy, projectNbr });
        return;
    }

    public void setOtherViewsCriteria() {
        Object _ret = this.riInvokeExportedMethod(this, "setOtherViewsCriteria", null, null);
        return;
    }

    public void setPAEditMode(String readOnly) {
        Object _ret = this.riInvokeExportedMethod(this, "setPAEditMode", new String[] { "java.lang.String" }, new Object[] {
                                                  readOnly });
        return;
    }

    public void setWhereClauseByID(String setID) {
        Object _ret = this.riInvokeExportedMethod(this, "setWhereClauseByID", new String[] { "java.lang.String" }, new Object[] {
                                                  setID });
        return;
    }

    public boolean updateContributionLinkedActions(BigDecimal paxActivitiesId, List actionList) {
        Object _ret = this.riInvokeExportedMethod(this, "updateContributionLinkedActions", new String[] {
                                                  "java.math.BigDecimal", "java.util.List"
        }, new Object[] { paxActivitiesId, actionList });
        return ((Boolean) _ret).booleanValue();
    }

    public void updateNewProjectList(Integer fy) {
        Object _ret = this.riInvokeExportedMethod(this, "updateNewProjectList", new String[] { "java.lang.Integer" }, new Object[] {
                                                  fy });
        return;
    }

    public Boolean updateRGs(List rgList) {
        Object _ret = this.riInvokeExportedMethod(this, "updateRGs", new String[] { "java.util.List" }, new Object[] {
                                                  rgList });
        return (Boolean) _ret;
    }

    public Boolean updateTags(List tagList) {
        Object _ret = this.riInvokeExportedMethod(this, "updateTags", new String[] { "java.util.List" }, new Object[] {
                                                  tagList });
        return (Boolean) _ret;
    }

    public void uploadDocuments(List files, Integer sourceType, List sourceList) {
        Object _ret = this.riInvokeExportedMethod(this, "uploadDocuments", new String[] {
                                                  "java.util.List", "java.lang.Integer", "java.util.List"
        }, new Object[] { files, sourceType, sourceList });
        return;
    }

    public List<String> uploadFiles(List files) {
        Object _ret = this.riInvokeExportedMethod(this, "uploadFiles", new String[] { "java.util.List" }, new Object[] {
                                                  files });
        return (List<String>) _ret;
    }
}

