package gov.ofda.abacus.model.abacusapp.lov.entityview;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;
import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;


import gov.ofda.abacus.model.abacusapp.lov.entity.CountryLookupImpl;

import gov.ofda.abacus.model.abacusapp.lov.entityview.RegionLookupViewRowImpl.AttributesEnum;

import java.sql.Timestamp;

import java.util.Arrays;

import oracle.jbo.RowSet;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.server.Entity;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Jan 14 11:52:44 EST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CountryLookupViewRowImpl extends AbacusViewRowImpl {


    public static final int ENTITY_COUNTRYLOOKUP = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CountryCode,
        CountryName,
        RegionCode,
        RegionName,
        CountryUpdatedId,
        CountryUpdatedDate,
        LovSessScopeModule_ProjectLOV1_1,
        LovAppScopeModule_RegionLOV1_1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int COUNTRYCODE = AttributesEnum.CountryCode.index();
    public static final int COUNTRYNAME = AttributesEnum.CountryName.index();
    public static final int REGIONCODE = AttributesEnum.RegionCode.index();
    public static final int REGIONNAME = AttributesEnum.RegionName.index();
    public static final int COUNTRYUPDATEDID = AttributesEnum.CountryUpdatedId.index();
    public static final int COUNTRYUPDATEDDATE = AttributesEnum.CountryUpdatedDate.index();
    public static final int LOVSESSSCOPEMODULE_PROJECTLOV1_1 = AttributesEnum.LovSessScopeModule_ProjectLOV1_1.index();
    public static final int LOVAPPSCOPEMODULE_REGIONLOV1_1 = AttributesEnum.LovAppScopeModule_RegionLOV1_1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public CountryLookupViewRowImpl() {
    }

    /**
     * Gets CountryLookup entity object.
     * @return the CountryLookup
     */
    public CountryLookupImpl getCountryLookup() {
        return (CountryLookupImpl) getEntity(ENTITY_COUNTRYLOOKUP);
    }

    /**
     * Gets the attribute value for COUNTRY_CODE using the alias name CountryCode.
     * @return the COUNTRY_CODE
     */
    public Integer getCountryCode() {
        return (Integer) getAttributeInternal(COUNTRYCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for COUNTRY_CODE using the alias name CountryCode.
     * @param value value to set the COUNTRY_CODE
     */
    public void setCountryCode(Integer value) {
        setAttributeInternal(COUNTRYCODE, value);
    }

    /**
     * Gets the attribute value for COUNTRY_NAME using the alias name CountryName.
     * @return the COUNTRY_NAME
     */
    public String getCountryName() {
        return (String) getAttributeInternal(COUNTRYNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for COUNTRY_NAME using the alias name CountryName.
     * @param value value to set the COUNTRY_NAME
     */
    public void setCountryName(String value) {
        setAttributeInternal(COUNTRYNAME, value);
    }

    /**
     * Gets the attribute value for REGION_CODE using the alias name RegionCode.
     * @return the REGION_CODE
     */
    public String getRegionCode() {
        return (String) getAttributeInternal(REGIONCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for REGION_CODE using the alias name RegionCode.
     * @param value value to set the REGION_CODE
     */
    public void setRegionCode(String value) {
        setAttributeInternal(REGIONCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute RegionName.
     * @return the RegionName
     */
    public String getRegionName() {
        return (String) getAttributeInternal(REGIONNAME);
    }

    /**
     * Gets the attribute value for COUNTRY_UPDATED_ID using the alias name CountryUpdatedId.
     * @return the COUNTRY_UPDATED_ID
     */
    public String getCountryUpdatedId() {
        return (String) getAttributeInternal(COUNTRYUPDATEDID);
    }

    /**
     * Gets the attribute value for COUNTRY_UPDATED_DATE using the alias name CountryUpdatedDate.
     * @return the COUNTRY_UPDATED_DATE
     */
    public Timestamp getCountryUpdatedDate() {
        return (Timestamp) getAttributeInternal(COUNTRYUPDATEDDATE);
    }

    /**
     * Gets the view accessor <code>RowSet</code> LovSessScopeModule_ProjectLOV1_1.
     */
    public RowSet getLovSessScopeModule_ProjectLOV1_1() {
        return (RowSet) getAttributeInternal(LOVSESSSCOPEMODULE_PROJECTLOV1_1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> LovAppScopeModule_RegionLOV1_1.
     */
    public RowSet getLovAppScopeModule_RegionLOV1_1() {
        return (RowSet) getAttributeInternal(LOVAPPSCOPEMODULE_REGIONLOV1_1);
    }

    @Override
    public boolean isAttributeUpdateable(int i) {
        if((this.getEntity(0).getEntityState() > Entity.STATUS_NEW) && i == AttributesEnum.CountryCode.index()){
                   return false;
        }
        return super.isAttributeUpdateable(i);
    }
}

