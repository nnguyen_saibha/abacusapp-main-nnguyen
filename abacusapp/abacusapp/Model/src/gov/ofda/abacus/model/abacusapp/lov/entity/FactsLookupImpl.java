package gov.ofda.abacus.model.abacusapp.lov.entity;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;

import java.sql.Timestamp;

import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;

import oracle.jbo.Key;
import oracle.jbo.RowIterator;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.TransactionEvent;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Oct 05 10:33:57 EDT 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class FactsLookupImpl extends AbacusEntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        FLookupSeq,
        LookupType,
        Code,
        Name,
        FactsCode,
        ShortName,
        UpdId,
        UpdDate,
        FactLink,
        FactLink1,
        FactLink2,
        FactLink3;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    public static final int FLOOKUPSEQ = AttributesEnum.FLookupSeq.index();
    public static final int LOOKUPTYPE = AttributesEnum.LookupType.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int FACTSCODE = AttributesEnum.FactsCode.index();
    public static final int SHORTNAME = AttributesEnum.ShortName.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int FACTLINK = AttributesEnum.FactLink.index();
    public static final int FACTLINK1 = AttributesEnum.FactLink1.index();
    public static final int FACTLINK2 = AttributesEnum.FactLink2.index();
    public static final int FACTLINK3 = AttributesEnum.FactLink3.index();

    /**
     * This is the default constructor (do not remove).
     */
    public FactsLookupImpl() {
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("gov.ofda.abacus.model.abacusapp.lov.entity.FactsLookup");
    }

    /**
     * Gets the attribute value for FLookupSeq, using the alias name FLookupSeq.
     * @return the value of FLookupSeq
     */
    public DBSequence getFLookupSeq() {
        return (DBSequence) getAttributeInternal(FLOOKUPSEQ);
    }

    /**
     * Sets <code>value</code> as the attribute value for FLookupSeq.
     * @param value value to set the FLookupSeq
     */
    public void setFLookupSeq(DBSequence value) {
        setAttributeInternal(FLOOKUPSEQ, value);
    }

    /**
     * Gets the attribute value for LookupType, using the alias name LookupType.
     * @return the value of LookupType
     */
    public String getLookupType() {
        return (String) getAttributeInternal(LOOKUPTYPE);
    }

    /**
     * Sets <code>value</code> as the attribute value for LookupType.
     * @param value value to set the LookupType
     */
    public void setLookupType(String value) {
        setAttributeInternal(LOOKUPTYPE, value);
    }

    /**
     * Gets the attribute value for Code, using the alias name Code.
     * @return the value of Code
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for Code.
     * @param value value to set the Code
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for Name, using the alias name Name.
     * @return the value of Name
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for Name.
     * @param value value to set the Name
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for FactsCode, using the alias name FactsCode.
     * @return the value of FactsCode
     */
    public String getFactsCode() {
        return (String) getAttributeInternal(FACTSCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for FactsCode.
     * @param value value to set the FactsCode
     */
    public void setFactsCode(String value) {
        setAttributeInternal(FACTSCODE, value);
    }

    /**
     * Gets the attribute value for ShortName, using the alias name ShortName.
     * @return the value of ShortName
     */
    public String getShortName() {
        return (String) getAttributeInternal(SHORTNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for ShortName.
     * @param value value to set the ShortName
     */
    public void setShortName(String value) {
        setAttributeInternal(SHORTNAME, value);
    }

    /**
     * Gets the attribute value for UpdId, using the alias name UpdId.
     * @return the value of UpdId
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Gets the attribute value for UpdDate, using the alias name UpdDate.
     * @return the value of UpdDate
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }

    /**
     * @return the associated entity oracle.jbo.RowIterator.
     */
    public RowIterator getFactLink() {
        return (RowIterator) getAttributeInternal(FACTLINK);
    }

    /**
     * @return the associated entity oracle.jbo.RowIterator.
     */
    public RowIterator getFactLink1() {
        return (RowIterator) getAttributeInternal(FACTLINK1);
    }

    /**
     * @return the associated entity oracle.jbo.RowIterator.
     */
    public RowIterator getFactLink2() {
        return (RowIterator) getAttributeInternal(FACTLINK2);
    }

    /**
     * @return the associated entity oracle.jbo.RowIterator.
     */
    public RowIterator getFactLink3() {
        return (RowIterator) getAttributeInternal(FACTLINK3);
    }


    /**
     * @param fLookupSeq key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(DBSequence fLookupSeq) {
        return new Key(new Object[] { fLookupSeq });
    }

    @Override
    protected void prepareForDML(int i, TransactionEvent transactionEvent) {
        if(i==DML_UPDATE || i==DML_INSERT)
        {
        ADFContext adfCtx = ADFContext.getCurrent();  
        SecurityContext secCntx = adfCtx.getSecurityContext();  
        String user = secCntx.getUserName(); 
        if(user!=null)
            setAttributeInternal(UPDID,user.toUpperCase());
        }
        super.prepareForDML(i, transactionEvent);
    }
}

