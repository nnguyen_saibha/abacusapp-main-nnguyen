package gov.ofda.abacus.model.abacusapp.base.framework;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import java.util.Map;

import oracle.jbo.JboException;
import oracle.jbo.Row;
import oracle.jbo.server.Entity;
import oracle.jbo.server.ViewDefImpl;
import oracle.jbo.server.ViewObjectImpl;

import org.codehaus.groovy.runtime.InvokerHelper;

public class AbacusViewObjectImpl extends ViewObjectImpl {
    private Integer currentFY;
	private Integer currentQTR;
    public AbacusViewObjectImpl(String string, ViewDefImpl viewDefImpl) {
        super(string, viewDefImpl);
    }

    public AbacusViewObjectImpl() {
        super();
    }


    /**
     * Retruns the row status.
     * Initialized, Modified, Unmodified or New
     * @param row
     * @return status
     */
    public String getRowStatus(Row row) {
        
        AbacusViewRowImpl rwImpl = (AbacusViewRowImpl)row;
        String rwStatus = translateStatusToString(rwImpl.getEntity(0).getEntityState());
        return rwStatus;
    }
    //Code taken from http://www.oracle.com/technetwork/developer-tools/adf/learnmore/06-cancelform-java-169125.pdf

    private String translateStatusToString(byte b) {
        String ret = null;
        switch (b) {
        case Entity.STATUS_INITIALIZED:
            {
                ret = "Initialized";
                break;
            }
        case Entity.STATUS_MODIFIED:
            {
                ret = "Modified";
                break;
            }
        case Entity.STATUS_UNMODIFIED:
            {
                ret = "Unmodified";
                break;
            }
        case Entity.STATUS_NEW:
            {
                ret = "New";
                break;
            }
        }
        return ret;
    }

    public void undoChanges() {
        Iterator iterator = getEntityDef(0).getAllEntityInstancesIterator(getDBTransaction());
        while (iterator.hasNext()) {
            AbacusEntityImpl test = (AbacusEntityImpl)iterator.next();
            if (test.getEntityState() == AbacusEntityImpl.STATUS_DELETED) {
                test.revert();
            }
            if (test.getEntityState() == AbacusEntityImpl.STATUS_INITIALIZED || test.getEntityState() == AbacusEntityImpl.STATUS_NEW)
                test.remove();
            if(test.getEntityState() == AbacusEntityImpl.STATUS_MODIFIED)
                test.refresh(Row.REFRESH_UNDO_CHANGES|Row.REFRESH_WITH_DB_FORGET_CHANGES);
        }
        executeQuery();
    }
    public void setCurrentFY(Integer currentFY) {
        this.currentFY = currentFY;
    }

     public Integer getCurrentFY() {
         Integer currentFY = null;
         try(PreparedStatement st = getDBTransaction().createPreparedStatement("select get_fy() from dual", 0);ResultSet rs = st.executeQuery();) {
             if (rs.next()) {
                 currentFY = rs.getInt(1);
             }
         } catch (SQLException e) {
             throw new JboException(e);
         }
         return currentFY;
     }
    //http://adfpractice-fedor.blogspot.com/2012/05/working-with-vos-built-in-aggregation.html
 private class AgrFuncHelper extends HashMap {
     private String funcName;
     public AgrFuncHelper(String funcName) {
         super();
         this.funcName=funcName;
     }
     public Object get(Object key) 
     {
       //Invoke private method
       //of our DefaultRowSet (sum,count,avg,min,max)
       //key is argument expression for the aggr funcion being called
       //sum("Salary")
       Object o=InvokerHelper.invokeMethod(getDefaultRowSet(), funcName, key);
       return o;
     }

     }
     public Map getSum() 
     {

       Map tmp=new AgrFuncHelper("sum");
       
       return tmp;
     }
	      public void setCurrentQTR(Integer currentQTR) {
         this.currentQTR = currentQTR;
     }

     public Integer getCurrentQTR() {
         
         Integer currentQTR = null;
         try(PreparedStatement st = getDBTransaction().createPreparedStatement("select app.get_qtr(sysdate) from dual", 0);ResultSet rs = st.executeQuery();) {
             if (rs.next()) {
                 currentQTR = rs.getInt(1);
             }
         } catch (SQLException e) {
             throw new JboException(e);
         } 
         return currentQTR;
     }
 }

