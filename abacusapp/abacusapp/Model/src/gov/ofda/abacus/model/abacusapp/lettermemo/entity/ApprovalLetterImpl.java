package gov.ofda.abacus.model.abacusapp.lettermemo.entity;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;

import oracle.jbo.Key;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.TransactionEvent;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Dec 09 18:40:25 EST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ApprovalLetterImpl extends AbacusEntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ApprovalLetterSeq,
        LetterMemoId,
        LetterName,
        Description,
        UpdId,
        UpdDate,
        LetterType,
        LetterMemo;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int APPROVALLETTERSEQ = AttributesEnum.ApprovalLetterSeq.index();
    public static final int LETTERMEMOID = AttributesEnum.LetterMemoId.index();
    public static final int LETTERNAME = AttributesEnum.LetterName.index();
    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int LETTERTYPE = AttributesEnum.LetterType.index();
    public static final int LETTERMEMO = AttributesEnum.LetterMemo.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ApprovalLetterImpl() {
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("gov.ofda.abacus.model.abacusapp.lettermemo.entity.ApprovalLetter");
    }


    /**
     * Gets the attribute value for ApprovalLetterSeq, using the alias name ApprovalLetterSeq.
     * @return the value of ApprovalLetterSeq
     */
    public DBSequence getApprovalLetterSeq() {
        return (DBSequence) getAttributeInternal(APPROVALLETTERSEQ);
    }

    /**
     * Sets <code>value</code> as the attribute value for ApprovalLetterSeq.
     * @param value value to set the ApprovalLetterSeq
     */
    public void setApprovalLetterSeq(DBSequence value) {
        setAttributeInternal(APPROVALLETTERSEQ, value);
    }

    /**
     * Gets the attribute value for LetterMemoId, using the alias name LetterMemoId.
     * @return the value of LetterMemoId
     */
    public BigDecimal getLetterMemoId() {
        return (BigDecimal) getAttributeInternal(LETTERMEMOID);
    }

    /**
     * Sets <code>value</code> as the attribute value for LetterMemoId.
     * @param value value to set the LetterMemoId
     */
    public void setLetterMemoId(BigDecimal value) {
        setAttributeInternal(LETTERMEMOID, value);
    }

    /**
     * Gets the attribute value for LetterName, using the alias name LetterName.
     * @return the value of LetterName
     */
    public String getLetterName() {
        return (String) getAttributeInternal(LETTERNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for LetterName.
     * @param value value to set the LetterName
     */
    public void setLetterName(String value) {
        setAttributeInternal(LETTERNAME, value);
    }

    /**
     * Gets the attribute value for Description, using the alias name Description.
     * @return the value of Description
     */
    public String getDescription() {
        return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as the attribute value for Description.
     * @param value value to set the Description
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for UpdId, using the alias name UpdId.
     * @return the value of UpdId
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Sets <code>value</code> as the attribute value for UpdId.
     * @param value value to set the UpdId
     */
    public void setUpdId(String value) {
        setAttributeInternal(UPDID, value);
    }

    /**
     * Gets the attribute value for UpdDate, using the alias name UpdDate.
     * @return the value of UpdDate
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }

    /**
     * Gets the attribute value for LetterType, using the alias name LetterType.
     * @return the value of LetterType
     */
    public BigDecimal getLetterType() {
        return (BigDecimal) getAttributeInternal(LETTERTYPE);
    }

    /**
     * Sets <code>value</code> as the attribute value for LetterType.
     * @param value value to set the LetterType
     */
    public void setLetterType(BigDecimal value) {
        setAttributeInternal(LETTERTYPE, value);
    }

    /**
     * @return the associated entity LetterMemoImpl.
     */
    public LetterMemoImpl getLetterMemo() {
        return (LetterMemoImpl) getAttributeInternal(LETTERMEMO);
    }

    /**
     * Sets <code>value</code> as the associated entity LetterMemoImpl.
     */
    public void setLetterMemo(LetterMemoImpl value) {
        setAttributeInternal(LETTERMEMO, value);
    }


    /**
     * @param approvalLetterSeq key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(DBSequence approvalLetterSeq) {
        return new Key(new Object[] { approvalLetterSeq });
    }

    @Override
    protected void prepareForDML(int i, TransactionEvent transactionEvent) {
        if(i==DML_UPDATE || i==DML_INSERT)
        {
        ADFContext adfCtx = ADFContext.getCurrent();  
        SecurityContext secCntx = adfCtx.getSecurityContext();  
        String user = secCntx.getUserName(); 
        if(user!=null)
            setAttributeInternal(UPDID,user.toUpperCase());
        }
        super.prepareForDML(i, transactionEvent);
    }
}

