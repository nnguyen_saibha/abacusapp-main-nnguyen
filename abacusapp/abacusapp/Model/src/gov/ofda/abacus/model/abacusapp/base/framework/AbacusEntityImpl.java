package gov.ofda.abacus.model.abacusapp.base.framework;

import java.math.BigDecimal;

import java.text.NumberFormat;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import oracle.jbo.server.Entity;
import oracle.jbo.server.EntityImpl;

public class AbacusEntityImpl extends EntityImpl {
    public AbacusEntityImpl() {
        super();
    }
    public void remove() {
        refresh(REFRESH_WITH_DB_FORGET_CHANGES);
        super.remove();
    }
    public String currencyFormat(BigDecimal number){
        String returnVal = "";
        if(null != number){
            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CANADA);
            double doubleVal = number.doubleValue();
            returnVal = format.format(doubleVal);
        }
        return returnVal;
    }
    public String numberFormat(BigDecimal number){
        String returnVal = "";
        if(null != number){
            NumberFormat format = NumberFormat.getNumberInstance(Locale.CANADA);
            double doubleVal = number.doubleValue();
            returnVal = format.format(doubleVal);
        }
        return returnVal;
    }
   public String dateFormat(Date d) {
       String returnVal="";
       if(d!=null)
       {
       SimpleDateFormat  DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
       returnVal = DATE_FORMAT.format(d);
       }
       return returnVal;
   }

    public ArrayList<EntityImpl> findDeletedItems() {
        //http://www.jobinesh.com/2009/11/retrieving-deleted-entity-objects.html
        ArrayList<EntityImpl> deletedList = new ArrayList<EntityImpl>();
        Iterator iterator = this.getEntityDef().getAllEntityInstancesIterator(this.getDBTransaction());
        while (iterator.hasNext()) {
            EntityImpl emp = (EntityImpl) iterator.next();
            if (emp.getEntityState() == Entity.STATUS_DELETED) {
                deletedList.add(emp);
            }
        }
        return deletedList;
    }
}
