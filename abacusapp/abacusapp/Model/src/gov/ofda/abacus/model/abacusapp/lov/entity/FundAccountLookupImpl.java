package gov.ofda.abacus.model.abacusapp.lov.entity;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;

import java.sql.Timestamp;

import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;

import oracle.jbo.Key;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.TransactionEvent;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Feb 10 10:51:29 EST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class FundAccountLookupImpl extends AbacusEntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        FundAccountCode,
        FundAccountName,
        Active,
        UpdId,
        UpdDate;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int FUNDACCOUNTCODE = AttributesEnum.FundAccountCode.index();
    public static final int FUNDACCOUNTNAME = AttributesEnum.FundAccountName.index();
    public static final int ACTIVE = AttributesEnum.Active.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();

    /**
     * This is the default constructor (do not remove).
     */
    public FundAccountLookupImpl() {
    }

    /**
     * Gets the attribute value for FundAccountCode, using the alias name FundAccountCode.
     * @return the value of FundAccountCode
     */
    public String getFundAccountCode() {
        return (String) getAttributeInternal(FUNDACCOUNTCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for FundAccountCode.
     * @param value value to set the FundAccountCode
     */
    public void setFundAccountCode(String value) {
        setAttributeInternal(FUNDACCOUNTCODE, value);
    }

    /**
     * Gets the attribute value for FundAccountName, using the alias name FundAccountName.
     * @return the value of FundAccountName
     */
    public String getFundAccountName() {
        return (String) getAttributeInternal(FUNDACCOUNTNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for FundAccountName.
     * @param value value to set the FundAccountName
     */
    public void setFundAccountName(String value) {
        setAttributeInternal(FUNDACCOUNTNAME, value);
    }

    /**
     * Gets the attribute value for Active, using the alias name Active.
     * @return the value of Active
     */
    public String getActive() {
        return (String) getAttributeInternal(ACTIVE);
    }

    /**
     * Sets <code>value</code> as the attribute value for Active.
     * @param value value to set the Active
     */
    public void setActive(String value) {
        setAttributeInternal(ACTIVE, value);
    }

    /**
     * Gets the attribute value for UpdId, using the alias name UpdId.
     * @return the value of UpdId
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Sets <code>value</code> as the attribute value for UpdId.
     * @param value value to set the UpdId
     */
    public void setUpdId(String value) {
        setAttributeInternal(UPDID, value);
    }

    /**
     * Gets the attribute value for UpdDate, using the alias name UpdDate.
     * @return the value of UpdDate
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for UpdDate.
     * @param value value to set the UpdDate
     */
    public void setUpdDate(Timestamp value) {
        setAttributeInternal(UPDDATE, value);
    }

    /**
     * @param fundAccountCode key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(String fundAccountCode) {
        return new Key(new Object[] { fundAccountCode });
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("gov.ofda.abacus.model.abacusapp.lov.entity.FundAccountLookup");
    }

    /**
     * Add locking logic here.
     */
    public void lock() {
        super.lock();
    }

    /**
     * Custom DML update/insert/delete logic here.
     * @param operation the operation type
     * @param e the transaction event
     */
    protected void doDML(int operation, TransactionEvent e) {
        super.doDML(operation, e);
    }
    
    @Override
        protected void prepareForDML(int i, TransactionEvent transactionEvent) {
            if(i==DML_UPDATE || i==DML_INSERT)
            {
            ADFContext adfCtx = ADFContext.getCurrent();  
            SecurityContext secCntx = adfCtx.getSecurityContext();  
            String user = secCntx.getUserName(); 
            if(user!=null)
                setAttributeInternal(UPDID,user.toUpperCase());
            }
            super.prepareForDML(i, transactionEvent);
        }
}

