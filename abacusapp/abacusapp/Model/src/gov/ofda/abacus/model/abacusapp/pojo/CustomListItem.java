package gov.ofda.abacus.model.abacusapp.pojo;

import java.io.Serializable;

public class CustomListItem implements Serializable {
    @SuppressWarnings("compatibility:4414464205513224141")
    private static final long serialVersionUID = 1L;
    private String code;
    private String name;
    private String displayName;
    private Boolean active;
    
    
    public CustomListItem() {
        super();
    }
    public CustomListItem(String code, String name) {
        super();
        this.code=code;
        this.name=name;            
    }
    public CustomListItem(String code, String name,String displayName,Boolean active) {
        super();
        this.code=code;
        this.name=name;
        this.displayName=displayName;
        this.active=active;
            
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        if(displayName==null)
            return name;
        return displayName;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getActive() {
        return active;
    }

}
