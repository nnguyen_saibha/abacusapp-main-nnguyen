package gov.ofda.abacus.model.abacusapp.entity.doc;

import java.io.Serializable;


public class Source implements Serializable{
    @SuppressWarnings("compatibility:3449230533944575473")
    private static final long serialVersionUID = 1L;
    private String actionId;
    private String budgetFy;
    private String awardId;
    private String projectNbr;
    private Integer awardSeq;
    private String letterMemoId;

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getActionId() {
        return actionId;
    }

    public void setBudgetFy(String budgetFy) {
        this.budgetFy = budgetFy;
    }

    public String getBudgetFy() {
        return budgetFy;
    }

    public void setAwardId(String awardId) {
        this.awardId = awardId;
    }

    public void setProjectNbr(String projectNbr) {
        this.projectNbr = projectNbr;
    }

    public String getProjectNbr() {
        return projectNbr;
    }

    public String getAwardId() {
        return awardId;
    }

    public void setAwardSeq(Integer awardSeq) {
        this.awardSeq = awardSeq;
    }

    public Integer getAwardSeq() {
        return awardSeq;
    }


    public void setLetterMemoId(String letterMemoId) {
        this.letterMemoId = letterMemoId;
    }

    public String getLetterMemoId() {
        return letterMemoId;
    }

}
