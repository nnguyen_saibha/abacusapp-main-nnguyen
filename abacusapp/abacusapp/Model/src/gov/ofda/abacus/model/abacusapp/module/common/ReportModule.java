package gov.ofda.abacus.model.abacusapp.module.common;

import java.util.Map;

import oracle.jbo.ApplicationModule;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Jun 01 11:30:43 EDT 2015
// ---------------------------------------------------------------------
public interface ReportModule extends ApplicationModule {
    String logReportInfo(String pageName, String reportName, String reportID);


    String logFavoriteReport(String favoriteID);

    long addToFavorites(Map attrs);
}

