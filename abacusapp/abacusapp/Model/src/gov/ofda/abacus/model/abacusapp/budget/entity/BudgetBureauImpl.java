package gov.ofda.abacus.model.abacusapp.budget.entity;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;

import oracle.jbo.Key;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.TransactionEvent;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Apr 01 14:55:55 EDT 2020
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class BudgetBureauImpl extends AbacusEntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        BudgetBureauId,
        BudgetFy,
        BureauCode,
        FundAcctTypeCode,
        FundCode,
        TransferAmt,
        TransferDate,
        UpdId,
        UpdDate,
        Comments;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int BUDGETBUREAUID = AttributesEnum.BudgetBureauId.index();
    public static final int BUDGETFY = AttributesEnum.BudgetFy.index();
    public static final int BUREAUCODE = AttributesEnum.BureauCode.index();
    public static final int FUNDACCTTYPECODE = AttributesEnum.FundAcctTypeCode.index();
    public static final int FUNDCODE = AttributesEnum.FundCode.index();
    public static final int TRANSFERAMT = AttributesEnum.TransferAmt.index();
    public static final int TRANSFERDATE = AttributesEnum.TransferDate.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int COMMENTS = AttributesEnum.Comments.index();

    /**
     * This is the default constructor (do not remove).
     */
    public BudgetBureauImpl() {
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("gov.ofda.abacus.model.abacusapp.budget.entity.BudgetBureau");
    }


    /**
     * Gets the attribute value for BudgetBureauId, using the alias name BudgetBureauId.
     * @return the value of BudgetBureauId
     */
    public BigDecimal getBudgetBureauId() {
        return (BigDecimal) getAttributeInternal(BUDGETBUREAUID);
    }


    /**
     * Gets the attribute value for BudgetFy, using the alias name BudgetFy.
     * @return the value of BudgetFy
     */
    public Integer getBudgetFy() {
        return (Integer) getAttributeInternal(BUDGETFY);
    }


    /**
     * Gets the attribute value for BureauCode, using the alias name BureauCode.
     * @return the value of BureauCode
     */
    public String getBureauCode() {
        return (String) getAttributeInternal(BUREAUCODE);
    }


    /**
     * Gets the attribute value for FundAcctTypeCode, using the alias name FundAcctTypeCode.
     * @return the value of FundAcctTypeCode
     */
    public String getFundAcctTypeCode() {
        return (String) getAttributeInternal(FUNDACCTTYPECODE);
    }


    /**
     * Gets the attribute value for FundCode, using the alias name FundCode.
     * @return the value of FundCode
     */
    public String getFundCode() {
        return (String) getAttributeInternal(FUNDCODE);
    }


    /**
     * Gets the attribute value for TransferAmt, using the alias name TransferAmt.
     * @return the value of TransferAmt
     */
    public BigDecimal getTransferAmt() {
        return (BigDecimal) getAttributeInternal(TRANSFERAMT);
    }


    /**
     * Gets the attribute value for TransferDate, using the alias name TransferDate.
     * @return the value of TransferDate
     */
    public Timestamp getTransferDate() {
        return (Timestamp) getAttributeInternal(TRANSFERDATE);
    }


    /**
     * Gets the attribute value for UpdId, using the alias name UpdId.
     * @return the value of UpdId
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Sets <code>value</code> as the attribute value for UpdId.
     * @param value value to set the UpdId
     */
    public void setUpdId(String value) {
        setAttributeInternal(UPDID, value);
    }

    /**
     * Gets the attribute value for UpdDate, using the alias name UpdDate.
     * @return the value of UpdDate
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }


    /**
     * Gets the attribute value for Comments, using the alias name Comments.
     * @return the value of Comments
     */
    public String getComments() {
        return (String) getAttributeInternal(COMMENTS);
    }

    /**
     * Sets <code>value</code> as the attribute value for Comments.
     * @param value value to set the Comments
     */
    public void setComments(String value) {
        setAttributeInternal(COMMENTS, value);
    }


    /**
     * @param budgetBureauId key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(BigDecimal budgetBureauId) {
        return new Key(new Object[] { budgetBureauId });
    }

    protected void prepareForDML(int i, TransactionEvent transactionEvent) {
        if(i==DML_UPDATE || i==DML_INSERT)
        {
        ADFContext adfCtx = ADFContext.getCurrent();  
        SecurityContext secCntx = adfCtx.getSecurityContext();  
        String user = secCntx.getUserName(); 
        if(user!=null)
            setAttributeInternal(UPDID,user.toUpperCase());
        }
        super.prepareForDML(i, transactionEvent);
    }
}

