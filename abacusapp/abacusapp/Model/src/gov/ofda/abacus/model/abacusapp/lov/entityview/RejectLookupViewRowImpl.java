package gov.ofda.abacus.model.abacusapp.lov.entityview;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;
import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;

import gov.ofda.abacus.model.abacusapp.lov.entity.RejectLookupImpl;

import gov.ofda.abacus.model.abacusapp.lov.entityview.RegionLookupViewRowImpl.AttributesEnum;

import java.sql.Timestamp;

import java.util.Arrays;

import oracle.jbo.domain.DBSequence;
import oracle.jbo.server.Entity;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Feb 01 15:55:35 EST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class RejectLookupViewRowImpl extends AbacusViewRowImpl {


    public static final int ENTITY_REJECTLOOKUP = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        RejectCode,
        Code,
        RejectDescription,
        UpdId,
        UpdDate;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int REJECTCODE = AttributesEnum.RejectCode.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int REJECTDESCRIPTION = AttributesEnum.RejectDescription.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();

    /**
     * This is the default constructor (do not remove).
     */
    public RejectLookupViewRowImpl() {
    }

    /**
     * Gets RejectLookup entity object.
     * @return the RejectLookup
     */
    public RejectLookupImpl getRejectLookup() {
        return (RejectLookupImpl) getEntity(ENTITY_REJECTLOOKUP);
    }

    /**
     * Gets the attribute value for REJECT_CODE using the alias name RejectCode.
     * @return the REJECT_CODE
     */
    public DBSequence getRejectCode() {
        return (DBSequence) getAttributeInternal(REJECTCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for REJECT_CODE using the alias name RejectCode.
     * @param value value to set the REJECT_CODE
     */
    public void setRejectCode(DBSequence value) {
        setAttributeInternal(REJECTCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Code.
     * @return the Code
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Gets the attribute value for REJECT_DESCRIPTION using the alias name RejectDescription.
     * @return the REJECT_DESCRIPTION
     */
    public String getRejectDescription() {
        return (String) getAttributeInternal(REJECTDESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for REJECT_DESCRIPTION using the alias name RejectDescription.
     * @param value value to set the REJECT_DESCRIPTION
     */
    public void setRejectDescription(String value) {
        setAttributeInternal(REJECTDESCRIPTION, value);
    }

    /**
     * Gets the attribute value for UPD_DATE using the alias name UpdDate.
     * @return the UPD_DATE
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }


    /**
     * Gets the attribute value for UPD_ID using the alias name UpdId.
     * @return the UPD_ID
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }


    @Override
    public boolean isAttributeUpdateable(int i) {
        if((this.getEntity(0).getEntityState() > Entity.STATUS_NEW) && i == AttributesEnum.RejectCode.index()){
                   return false;
        }
        return super.isAttributeUpdateable(i);
    }
}

