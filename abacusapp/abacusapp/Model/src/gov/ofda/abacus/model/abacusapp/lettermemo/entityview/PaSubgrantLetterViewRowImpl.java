package gov.ofda.abacus.model.abacusapp.lettermemo.entityview;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;
import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.Row;
import oracle.jbo.domain.DBSequence;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Nov 23 16:13:22 EST 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PaSubgrantLetterViewRowImpl extends AbacusViewRowImpl {


    public static final int ENTITY_PASUBGRANTLETTER = 0;

    @Override
    public boolean isAttributeUpdateable(int i) {
        return super.isAttributeUpdateable(i);
    }

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ActionId,
        LetterSeq,
        LetterDate,
        Refa,
        Refb,
        Purpose,
        Details,
        EffectiveDate1,
        LetterMemoId,
        UpdId,
        UpdDate,
        LetterMemoView;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ACTIONID = AttributesEnum.ActionId.index();
    public static final int LETTERSEQ = AttributesEnum.LetterSeq.index();
    public static final int LETTERDATE = AttributesEnum.LetterDate.index();
    public static final int REFA = AttributesEnum.Refa.index();
    public static final int REFB = AttributesEnum.Refb.index();
    public static final int PURPOSE = AttributesEnum.Purpose.index();
    public static final int DETAILS = AttributesEnum.Details.index();
    public static final int EFFECTIVEDATE1 = AttributesEnum.EffectiveDate1.index();
    public static final int LETTERMEMOID = AttributesEnum.LetterMemoId.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int LETTERMEMOVIEW = AttributesEnum.LetterMemoView.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PaSubgrantLetterViewRowImpl() {
    }

    /**
     * Gets PaSubgrantLetter entity object.
     * @return the PaSubgrantLetter
     */
    public AbacusEntityImpl getPaSubgrantLetter() {
        return (AbacusEntityImpl) getEntity(ENTITY_PASUBGRANTLETTER);
    }

    /**
     * Gets the attribute value for ACTION_ID using the alias name ActionId.
     * @return the ACTION_ID
     */
    public BigDecimal getActionId() {
        return (BigDecimal) getAttributeInternal(ACTIONID);
    }

    /**
     * Sets <code>value</code> as attribute value for ACTION_ID using the alias name ActionId.
     * @param value value to set the ACTION_ID
     */
    public void setActionId(BigDecimal value) {
        setAttributeInternal(ACTIONID, value);
    }

    /**
     * Gets the attribute value for LETTER_SEQ using the alias name LetterSeq.
     * @return the LETTER_SEQ
     */
    public DBSequence getLetterSeq() {
        return (DBSequence) getAttributeInternal(LETTERSEQ);
    }

    /**
     * Sets <code>value</code> as attribute value for LETTER_SEQ using the alias name LetterSeq.
     * @param value value to set the LETTER_SEQ
     */
    public void setLetterSeq(DBSequence value) {
        setAttributeInternal(LETTERSEQ, value);
    }

    /**
     * Gets the attribute value for LETTER_DATE using the alias name LetterDate.
     * @return the LETTER_DATE
     */
    public Timestamp getLetterDate() {
        return (Timestamp) getAttributeInternal(LETTERDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LETTER_DATE using the alias name LetterDate.
     * @param value value to set the LETTER_DATE
     */
    public void setLetterDate(Timestamp value) {
        setAttributeInternal(LETTERDATE, value);
    }

    /**
     * Gets the attribute value for REFA using the alias name Refa.
     * @return the REFA
     */
    public String getRefa() {
        return (String) getAttributeInternal(REFA);
    }

    /**
     * Sets <code>value</code> as attribute value for REFA using the alias name Refa.
     * @param value value to set the REFA
     */
    public void setRefa(String value) {
        setAttributeInternal(REFA, value);
    }

    /**
     * Gets the attribute value for REFB using the alias name Refb.
     * @return the REFB
     */
    public String getRefb() {
        return (String) getAttributeInternal(REFB);
    }

    /**
     * Sets <code>value</code> as attribute value for REFB using the alias name Refb.
     * @param value value to set the REFB
     */
    public void setRefb(String value) {
        setAttributeInternal(REFB, value);
    }

    /**
     * Gets the attribute value for PURPOSE using the alias name Purpose.
     * @return the PURPOSE
     */
    public String getPurpose() {
        return (String) getAttributeInternal(PURPOSE);
    }

    /**
     * Sets <code>value</code> as attribute value for PURPOSE using the alias name Purpose.
     * @param value value to set the PURPOSE
     */
    public void setPurpose(String value) {
        setAttributeInternal(PURPOSE, value);
    }

    /**
     * Gets the attribute value for DETAILS using the alias name Details.
     * @return the DETAILS
     */
    public String getDetails() {
        return (String) getAttributeInternal(DETAILS);
    }

    /**
     * Sets <code>value</code> as attribute value for DETAILS using the alias name Details.
     * @param value value to set the DETAILS
     */
    public void setDetails(String value) {
        setAttributeInternal(DETAILS, value);
    }

    /**
     * Gets the attribute value for EFFECTIVE_DATE using the alias name EffectiveDate1.
     * @return the EFFECTIVE_DATE
     */
    public Timestamp getEffectiveDate1() {
        return (Timestamp) getAttributeInternal(EFFECTIVEDATE1);
    }

    /**
     * Sets <code>value</code> as attribute value for EFFECTIVE_DATE using the alias name EffectiveDate1.
     * @param value value to set the EFFECTIVE_DATE
     */
    public void setEffectiveDate1(Timestamp value) {
        setAttributeInternal(EFFECTIVEDATE1, value);
    }

    /**
     * Gets the attribute value for LETTER_MEMO_ID using the alias name LetterMemoId.
     * @return the LETTER_MEMO_ID
     */
    public BigDecimal getLetterMemoId() {
        return (BigDecimal) getAttributeInternal(LETTERMEMOID);
    }

    /**
     * Sets <code>value</code> as attribute value for LETTER_MEMO_ID using the alias name LetterMemoId.
     * @param value value to set the LETTER_MEMO_ID
     */
    public void setLetterMemoId(BigDecimal value) {
        setAttributeInternal(LETTERMEMOID, value);
    }

    /**
     * Gets the attribute value for UPD_DATE using the alias name UpdDate.
     * @return the UPD_DATE
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }

    /**
     * Gets the attribute value for UPD_ID using the alias name UpdId.
     * @return the UPD_ID
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Gets the associated <code>Row</code> using master-detail link LetterMemoView.
     */
    public Row getLetterMemoView() {
        return (Row) getAttributeInternal(LETTERMEMOVIEW);
    }

    /**
     * Sets the master-detail link LetterMemoView between this object and <code>value</code>.
     */
    public void setLetterMemoView(Row value) {
        setAttributeInternal(LETTERMEMOVIEW, value);
    }
}

