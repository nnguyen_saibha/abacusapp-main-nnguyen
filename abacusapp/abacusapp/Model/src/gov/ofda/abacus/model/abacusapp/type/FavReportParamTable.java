package gov.ofda.abacus.model.abacusapp.type;

import java.sql.SQLException;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.sql.Datum;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.jpub.runtime.MutableArray;

public class FavReportParamTable implements ORAData, ORADataFactory
{
  public static final String _SQL_NAME = "APP.FAV_REPORT_PARAM_TABLE";
  public static final int _SQL_TYPECODE = OracleTypes.ARRAY;

  MutableArray _array;

private static final FavReportParamTable _FavReportParamTableFactory = new FavReportParamTable();

  public static ORADataFactory getORADataFactory()
  { return _FavReportParamTableFactory; }
  /* constructors */
  public FavReportParamTable()
  {
    this((FavReportParamType[])null);
  }

  public FavReportParamTable(FavReportParamType[] a)
  {
    _array = new MutableArray(2002, a, FavReportParamType.getORADataFactory());
  }

  /* ORAData interface */
  public Datum toDatum(Connection c) throws SQLException
  {
    return _array.toDatum(c, _SQL_NAME);
  }

  /* ORADataFactory interface */
  public ORAData create(Datum d, int sqlType) throws SQLException
  {
    if (d == null) return null; 
    FavReportParamTable a = new FavReportParamTable();
    a._array = new MutableArray(2002, (ARRAY) d, FavReportParamType.getORADataFactory());
    return a;
  }

  public int length() throws SQLException
  {
    return _array.length();
  }

  public int getBaseType() throws SQLException
  {
    return _array.getBaseType();
  }

  public String getBaseTypeName() throws SQLException
  {
    return _array.getBaseTypeName();
  }

  public ArrayDescriptor getDescriptor() throws SQLException
  {
    return _array.getDescriptor();
  }

  /* array accessor methods */
  public FavReportParamType[] getArray() throws SQLException
  {
    return (FavReportParamType[]) _array.getObjectArray(
      new FavReportParamType[_array.length()]);
  }

  public FavReportParamType[] getArray(long index, int count) throws SQLException
  {
    return (FavReportParamType[]) _array.getObjectArray(index,
      new FavReportParamType[_array.sliceLength(index, count)]);
  }

  public void setArray(FavReportParamType[] a) throws SQLException
  {
    _array.setObjectArray(a);
  }

  public void setArray(FavReportParamType[] a, long index) throws SQLException
  {
    _array.setObjectArray(a, index);
  }

  public FavReportParamType getElement(long index) throws SQLException
  {
    return (FavReportParamType) _array.getObjectElement(index);
  }

  public void setElement(FavReportParamType a, long index) throws SQLException
  {
    _array.setObjectElement(a, index);
  }

}
