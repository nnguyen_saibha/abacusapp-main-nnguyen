package gov.ofda.abacus.model.abacusapp.lov.entityview;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusEntityImpl;
import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;

import gov.ofda.abacus.model.abacusapp.lov.entity.DeferredLookupImpl;

import gov.ofda.abacus.model.abacusapp.lov.entityview.DisasterLookupViewRowImpl.AttributesEnum;

import java.sql.Timestamp;

import java.util.Arrays;

import oracle.jbo.RowSet;
import oracle.jbo.server.Entity;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Feb 02 14:18:33 EST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class DeferredLookupViewRowImpl extends AbacusViewRowImpl {


    public static final int ENTITY_DEFERREDLOOKUP = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        DeferredCode,
        Code,
        DeferredDescription,
        UpdId,
        UpdDate;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int DEFERREDCODE = AttributesEnum.DeferredCode.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int DEFERREDDESCRIPTION = AttributesEnum.DeferredDescription.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();

    /**
     * This is the default constructor (do not remove).
     */
    public DeferredLookupViewRowImpl() {
    }

    /**
     * Gets DeferredLookup entity object.
     * @return the DeferredLookup
     */
    public DeferredLookupImpl getDeferredLookup() {
        return (DeferredLookupImpl) getEntity(ENTITY_DEFERREDLOOKUP);
    }

    /**
     * Gets the attribute value for DEFERRED_CODE using the alias name DeferredCode.
     * @return the DEFERRED_CODE
     */
    public String getDeferredCode() {
        return (String) getAttributeInternal(DEFERREDCODE);
    }


    /**
     * Sets <code>value</code> as attribute value for DEFERRED_CODE using the alias name DeferredCode.
     * @param value value to set the DEFERRED_CODE
     */
    public void setDeferredCode(String value) {
        setAttributeInternal(DEFERREDCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Code.
     * @return the Code
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Gets the attribute value for DEFERRED_DESCRIPTION using the alias name DeferredDescription.
     * @return the DEFERRED_DESCRIPTION
     */
    public String getDeferredDescription() {
        return (String) getAttributeInternal(DEFERREDDESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for DEFERRED_DESCRIPTION using the alias name DeferredDescription.
     * @param value value to set the DEFERRED_DESCRIPTION
     */
    public void setDeferredDescription(String value) {
        setAttributeInternal(DEFERREDDESCRIPTION, value);
    }

    /**
     * Gets the attribute value for UPD_DATE using the alias name UpdDate.
     * @return the UPD_DATE
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }


    /**
     * Gets the attribute value for UPD_ID using the alias name UpdId.
     * @return the UPD_ID
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }


    @Override
    public boolean isAttributeUpdateable(int i) {
        if((this.getEntity(0).getEntityState() > Entity.STATUS_NEW) && i == AttributesEnum.DeferredCode.index()){
                   return false;
        }
        return super.isAttributeUpdateable(i);
    }
}

