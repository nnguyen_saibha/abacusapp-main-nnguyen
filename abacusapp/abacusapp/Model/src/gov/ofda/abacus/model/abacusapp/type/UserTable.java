package gov.ofda.abacus.model.abacusapp.type;

import java.sql.SQLException;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.sql.Datum;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.jpub.runtime.MutableArray;

public class UserTable implements ORAData, ORADataFactory
{
  public static final String _SQL_NAME = "APP.USER_TABLE";
  public static final int _SQL_TYPECODE = OracleTypes.ARRAY;

  MutableArray _array;

private static final UserTable _UserTableFactory = new UserTable();

  public static ORADataFactory getORADataFactory()
  { return _UserTableFactory; }
  /* constructors */
  public UserTable()
  {
    this((UserType[])null);
  }

  public UserTable(UserType[] a)
  {
    _array = new MutableArray(2002, a, UserType.getORADataFactory());
  }

  /* ORAData interface */
  public Datum toDatum(Connection c) throws SQLException
  {
    return _array.toDatum(c, _SQL_NAME);
  }

  /* ORADataFactory interface */
  public ORAData create(Datum d, int sqlType) throws SQLException
  {
    if (d == null) return null; 
    UserTable a = new UserTable();
    a._array = new MutableArray(2002, (ARRAY) d, UserType.getORADataFactory());
    return a;
  }

  public int length() throws SQLException
  {
    return _array.length();
  }

  public int getBaseType() throws SQLException
  {
    return _array.getBaseType();
  }

  public String getBaseTypeName() throws SQLException
  {
    return _array.getBaseTypeName();
  }

  public ArrayDescriptor getDescriptor() throws SQLException
  {
    return _array.getDescriptor();
  }

  /* array accessor methods */
  public UserType[] getArray() throws SQLException
  {
    return (UserType[]) _array.getObjectArray(
      new UserType[_array.length()]);
  }

  public UserType[] getArray(long index, int count) throws SQLException
  {
    return (UserType[]) _array.getObjectArray(index,
      new UserType[_array.sliceLength(index, count)]);
  }

  public void setArray(UserType[] a) throws SQLException
  {
    _array.setObjectArray(a);
  }

  public void setArray(UserType[] a, long index) throws SQLException
  {
    _array.setObjectArray(a, index);
  }

  public UserType getElement(long index) throws SQLException
  {
    return (UserType) _array.getObjectElement(index);
  }

  public void setElement(UserType a, long index) throws SQLException
  {
    _array.setObjectElement(a, index);
  }

}
