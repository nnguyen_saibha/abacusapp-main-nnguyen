package gov.ofda.abacus.model.abacusapp.user.oid;


import gov.ofda.abacus.model.abacusapp.user.oim.OIMUserManager;

import java.util.Hashtable;

import java.util.Map;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.AttributeModificationException;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import oracle.adf.share.logging.ADFLogger;

import oracle.security.jps.JpsException;
import oracle.security.jps.service.JpsServiceLocator;
import oracle.security.jps.service.ServiceLocator;
import oracle.security.jps.service.credstore.CredStoreException;
import oracle.security.jps.service.credstore.CredentialExpiredException;
import oracle.security.jps.service.credstore.CredentialStore;
import oracle.security.jps.service.credstore.GenericCredential;
import oracle.security.jps.service.credstore.PasswordCredential;

/**
 * This class access a ldap service, queries and adds
 * user to the Group
 *
 *
 */
public class OIDUserRoleManager {
    /*
	 * Tasks 1. Search for an user with username and get the distinguished name 2.
	 * Search for the group with the groupname 3. Add the user as a member in the
	 * group, Revoke
	 */
    private static String ldapAdServer = null;
    private final static String ldapSearchBase = "dc=ofda,dc=gov";

    private static final String ldapUsername = "cn=orcladmin";
    private static char[] ldapPassword = null;


    public static String ldapAccountToLookup = null;
    public static String groupName = null;
    public static LdapContext ctx = null;


    private static ADFLogger logger = ADFLogger.createADFLogger(OIDUserRoleManager.class);


    /**This constructor will access EM for credentials and
     * instantiate the OID accessors
     */
    public OIDUserRoleManager() {
        ServiceLocator locator = JpsServiceLocator.getServiceLocator();
        CredentialStore store = null;
        PasswordCredential pcred = null;
        GenericCredential gcred = null;
        try {
            store = locator.lookup(CredentialStore.class);
            pcred = (PasswordCredential) store.getCredential("OID", "Password");
            gcred = (GenericCredential) store.getCredential("OID", "URL");

        } catch (CredentialExpiredException e) {
            logger.log(e.toString());
        } catch (CredStoreException e) {
            logger.log(e.toString());
        } catch (JpsException e) {
            logger.log(e.toString());
        }
        Map<String, char[]> t = (Map<String, char[]>) gcred.getCredential();
        char tc[] = t.get("providerURL");
        String url = "";
        for (char c : tc)
            url += c;

        ldapPassword = pcred.getPassword();
        ldapAdServer = url;

    }

    /**
     * @param userId
     * @param grpName
     * @return
     * @throws NamingException
     */
    public LdapContext setupContext(String userId, String grpName) throws NamingException {

        ldapAccountToLookup = userId;
        groupName = grpName;

        Hashtable<String, Object> env = new Hashtable<String, Object>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        if (ldapUsername != null) {
            env.put(Context.SECURITY_PRINCIPAL, ldapUsername);
        }
        if (ldapPassword != null) {
            env.put(Context.SECURITY_CREDENTIALS, ldapPassword);
        }
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapAdServer);

        // the following is helpful in debugging errors
        // env.put("com.sun.jndi.ldap.trace.ber", System.err);

        ctx = new InitialLdapContext(env, null);

        return ctx;
    }


    /**
     * @param ctx
     * @param ldapSearchBase
     * @param accountName
     * @return
     * @throws NamingException
     */
    public SearchResult findUserBySAMAccountName(DirContext ctx, String accountName) throws NamingException {

        String searchFilter = "(&(objectClass=person)(uid=" + accountName + "))";

        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        NamingEnumeration<SearchResult> results = null;
        results = ctx.search(ldapSearchBase, searchFilter, searchControls);


        SearchResult searchResult = null;
        if (results.hasMoreElements()) {
            searchResult = (SearchResult) results.nextElement();

            // make sure there is not another item available, there should be only 1 match
            if (results.hasMoreElements()) {
                logger.log(logger.ERROR, "Matched multiple users for the accountName: " + accountName);
                searchResult = new SearchResult("Matched multiple users for the accountName: " + accountName, null, null);
                ctx.close();
                return searchResult;
            }
        }

        return searchResult;
    }

    /**
     * @param ctx
     * @param ldapSearchBase
     * @param sid
     * @return
     * @throws NamingException
     */
    public SearchResult findGroupBySID(DirContext ctx, String sid) throws NamingException {

        String searchFilter = "(&(objectClass=orclIDXGroup)(cn=" + sid + "))";

        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        NamingEnumeration<SearchResult> results = null;

        results = ctx.search(ldapSearchBase, searchFilter, searchControls);

        SearchResult searchResult = null;

        if (results.hasMoreElements()) {
            searchResult = (SearchResult) results.nextElement();

            // make sure there is not another item available, there should be only 1 match
            if (results.hasMoreElements()) {
                logger.log(logger.ERROR, "Matched multiple groups for the group with SID: " + sid);
                
                ctx.close();
                return null;
            }
        }
        return searchResult;
    }

    /**
     * @param ctx
     * @param userName
     * @param groupName
     * @return
     * @throws NamingException
     */
    public static Boolean addUserToGroup(DirContext ctx, SearchResult userName,
                                         String groupName) throws NamingException {
        try {

            ctx.modifyAttributes(groupName, DirContext.ADD_ATTRIBUTE,
                                 new BasicAttributes("uniquemember", userName.getNameInNamespace()));
        } catch (AttributeModificationException ex) {
            System.err.println();
            logger.log(logger.ERROR, "Modification error occurred");
            return false;
        } catch (Exception exception) {
            return false;
        }

        logger.log(logger.TRACE, "User added to the Group");

        return true;
    }

    /**
     * @param ctx
     * @param userName
     * @param groupName
     * @return
     * @throws NamingException
     */
    public static Boolean revokeUserFromGroup(DirContext ctx, SearchResult userName,
                                              String groupName) throws NamingException {
        try {

            ctx.modifyAttributes(groupName, DirContext.REMOVE_ATTRIBUTE,
                                 new BasicAttributes("uniquemember", userName.getNameInNamespace()));
        } catch (AttributeModificationException ex) {
            logger.log(logger.ERROR, "Modification error occurred");
            return false;
        } catch (Exception exception) {
            return false;
        }

        logger.log(logger.TRACE, "user removed from the Group");

        return true;
    }

}
