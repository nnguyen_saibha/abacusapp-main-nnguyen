package gov.ofda.abacus.model.abacusapp.user.entityview;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;
import gov.ofda.abacus.model.abacusapp.user.entity.AbacusUserImpl;


import gov.ofda.abacus.model.abacusapp.user.entityview.common.AbacusUserViewRow;

import java.sql.CallableStatement;
import java.sql.Timestamp;

import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Nov 28 10:21:26 EST 2016
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AbacusUserViewRowImpl extends AbacusViewRowImpl implements AbacusUserViewRow {


    public static final int ENTITY_ABACUSUSER = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        Userid,
        Alias,
        DisplayName,
        LastName,
        FirstName,
        OfdaTeamCode,
        OfdaDivisionCode,
        UpdatedId,
        UpdatedDate,
        Email,
        CcEmail,
        IsProgMgr,
        IsCto,
        IsSpecialist,
        IsContractOfficer,
        IsOaa,
        Active,
        PhoneNbr,
        IsConnect,
        DatabaseUser,
        Active1,
        IsGc,
        BureauCode,
        OfficeCode,
        AbacusUserDetailsView,
        DivisionLOV1,
        TeamLOV1,
        YesNoLOV1,
        BureauLOV1,
        OfficeLOV1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int USERID = AttributesEnum.Userid.index();
    public static final int ALIAS = AttributesEnum.Alias.index();
    public static final int DISPLAYNAME = AttributesEnum.DisplayName.index();
    public static final int LASTNAME = AttributesEnum.LastName.index();
    public static final int FIRSTNAME = AttributesEnum.FirstName.index();
    public static final int OFDATEAMCODE = AttributesEnum.OfdaTeamCode.index();
    public static final int OFDADIVISIONCODE = AttributesEnum.OfdaDivisionCode.index();
    public static final int UPDATEDID = AttributesEnum.UpdatedId.index();
    public static final int UPDATEDDATE = AttributesEnum.UpdatedDate.index();
    public static final int EMAIL = AttributesEnum.Email.index();
    public static final int CCEMAIL = AttributesEnum.CcEmail.index();
    public static final int ISPROGMGR = AttributesEnum.IsProgMgr.index();
    public static final int ISCTO = AttributesEnum.IsCto.index();
    public static final int ISSPECIALIST = AttributesEnum.IsSpecialist.index();
    public static final int ISCONTRACTOFFICER = AttributesEnum.IsContractOfficer.index();
    public static final int ISOAA = AttributesEnum.IsOaa.index();
    public static final int ACTIVE = AttributesEnum.Active.index();
    public static final int PHONENBR = AttributesEnum.PhoneNbr.index();
    public static final int ISCONNECT = AttributesEnum.IsConnect.index();
    public static final int DATABASEUSER = AttributesEnum.DatabaseUser.index();
    public static final int ACTIVE1 = AttributesEnum.Active1.index();
    public static final int ISGC = AttributesEnum.IsGc.index();
    public static final int BUREAUCODE = AttributesEnum.BureauCode.index();
    public static final int OFFICECODE = AttributesEnum.OfficeCode.index();
    public static final int ABACUSUSERDETAILSVIEW = AttributesEnum.AbacusUserDetailsView.index();
    public static final int DIVISIONLOV1 = AttributesEnum.DivisionLOV1.index();
    public static final int TEAMLOV1 = AttributesEnum.TeamLOV1.index();
    public static final int YESNOLOV1 = AttributesEnum.YesNoLOV1.index();
    public static final int BUREAULOV1 = AttributesEnum.BureauLOV1.index();
    public static final int OFFICELOV1 = AttributesEnum.OfficeLOV1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AbacusUserViewRowImpl() {
    }

    /**
     * Gets AbacusUser entity object.
     * @return the AbacusUser
     */
    public AbacusUserImpl getAbacusUser() {
        return (AbacusUserImpl) getEntity(ENTITY_ABACUSUSER);
    }

    /**
     * Gets the attribute value for USERID using the alias name Userid.
     * @return the USERID
     */
    public String getUserid() {
        return (String) getAttributeInternal(USERID);
    }

    /**
     * Sets <code>value</code> as attribute value for USERID using the alias name Userid.
     * @param value value to set the USERID
     */
    public void setUserid(String value) {
        setAttributeInternal(USERID, value != null? value.toUpperCase():null);
    }

    /**
     * Gets the attribute value for ALIAS using the alias name Alias.
     * @return the ALIAS
     */
    public String getAlias() {
        return (String) getAttributeInternal(ALIAS);
    }

    /**
     * Sets <code>value</code> as attribute value for ALIAS using the alias name Alias.
     * @param value value to set the ALIAS
     */
    public void setAlias(String value) {
        setAttributeInternal(ALIAS, value);
    }

    /**
     * Gets the attribute value for DISPLAY_NAME using the alias name DisplayName.
     * @return the DISPLAY_NAME
     */
    public String getDisplayName() {
        return (String) getAttributeInternal(DISPLAYNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for DISPLAY_NAME using the alias name DisplayName.
     * @param value value to set the DISPLAY_NAME
     */
    public void setDisplayName(String value) {
        setAttributeInternal(DISPLAYNAME, value);
    }

    /**
     * Gets the attribute value for LAST_NAME using the alias name LastName.
     * @return the LAST_NAME
     */
    public String getLastName() {
        return (String) getAttributeInternal(LASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_NAME using the alias name LastName.
     * @param value value to set the LAST_NAME
     */
    public void setLastName(String value) {
        setAttributeInternal(LASTNAME, value);
        if(value!=null)
            setAttributeInternal(DISPLAYNAME, value+", "+this.getFirstName());
        else
            setAttributeInternal(DISPLAYNAME, null);
    }

    /**
     * Gets the attribute value for FIRST_NAME using the alias name FirstName.
     * @return the FIRST_NAME
     */
    public String getFirstName() {
        return (String) getAttributeInternal(FIRSTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for FIRST_NAME using the alias name FirstName.
     * @param value value to set the FIRST_NAME
     */
    public void setFirstName(String value) {
        setAttributeInternal(FIRSTNAME, value);
        if(value!=null)
            setAttributeInternal(DISPLAYNAME, this.getLastName()+", "+value);
        else
            setAttributeInternal(DISPLAYNAME, null);        
    }

    /**
     * Gets the attribute value for OFDA_TEAM_CODE using the alias name OfdaTeamCode.
     * @return the OFDA_TEAM_CODE
     */
    public String getOfdaTeamCode() {
        return (String) getAttributeInternal(OFDATEAMCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for OFDA_TEAM_CODE using the alias name OfdaTeamCode.
     * @param value value to set the OFDA_TEAM_CODE
     */
    public void setOfdaTeamCode(String value) {
        setAttributeInternal(OFDATEAMCODE, value);
    }

    /**
     * Gets the attribute value for OFDA_DIVISION_CODE using the alias name OfdaDivisionCode.
     * @return the OFDA_DIVISION_CODE
     */
    public String getOfdaDivisionCode() {
        return (String) getAttributeInternal(OFDADIVISIONCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for OFDA_DIVISION_CODE using the alias name OfdaDivisionCode.
     * @param value value to set the OFDA_DIVISION_CODE
     */
    public void setOfdaDivisionCode(String value) {
        setAttributeInternal(OFDADIVISIONCODE, value);
    }

    /**
     * Gets the attribute value for UPDATED_ID using the alias name UpdatedId.
     * @return the UPDATED_ID
     */
    public String getUpdatedId() {
        return (String) getAttributeInternal(UPDATEDID);
    }

    /**
     * Sets <code>value</code> as attribute value for UPDATED_ID using the alias name UpdatedId.
     * @param value value to set the UPDATED_ID
     */
    public void setUpdatedId(String value) {
        setAttributeInternal(UPDATEDID, value);
    }

    /**
     * Gets the attribute value for UPDATED_DATE using the alias name UpdatedDate.
     * @return the UPDATED_DATE
     */
    public Timestamp getUpdatedDate() {
        return (Timestamp) getAttributeInternal(UPDATEDDATE);
    }

    /**
     * Gets the attribute value for EMAIL using the alias name Email.
     * @return the EMAIL
     */
    public String getEmail() {
        return (String) getAttributeInternal(EMAIL);
    }

    /**
     * Sets <code>value</code> as attribute value for EMAIL using the alias name Email.
     * @param value value to set the EMAIL
     */
    public void setEmail(String value) {
        setAttributeInternal(EMAIL, value);
    }

    /**
     * Gets the attribute value for CC_EMAIL using the alias name CcEmail.
     * @return the CC_EMAIL
     */
    public String getCcEmail() {
        return (String) getAttributeInternal(CCEMAIL);
    }

    /**
     * Sets <code>value</code> as attribute value for CC_EMAIL using the alias name CcEmail.
     * @param value value to set the CC_EMAIL
     */
    public void setCcEmail(String value) {
        setAttributeInternal(CCEMAIL, value);
    }

    /**
     * Gets the attribute value for IS_PROG_MGR using the alias name IsProgMgr.
     * @return the IS_PROG_MGR
     */
    public String getIsProgMgr() {
        return (String) getAttributeInternal(ISPROGMGR);
    }

    /**
     * Sets <code>value</code> as attribute value for IS_PROG_MGR using the alias name IsProgMgr.
     * @param value value to set the IS_PROG_MGR
     */
    public void setIsProgMgr(String value) {
        setAttributeInternal(ISPROGMGR, value);
    }

    /**
     * Gets the attribute value for IS_CTO using the alias name IsCto.
     * @return the IS_CTO
     */
    public String getIsCto() {
        return (String) getAttributeInternal(ISCTO);
    }

    /**
     * Sets <code>value</code> as attribute value for IS_CTO using the alias name IsCto.
     * @param value value to set the IS_CTO
     */
    public void setIsCto(String value) {
        setAttributeInternal(ISCTO, value);
    }

    /**
     * Gets the attribute value for IS_SPECIALIST using the alias name IsSpecialist.
     * @return the IS_SPECIALIST
     */
    public String getIsSpecialist() {
        return (String) getAttributeInternal(ISSPECIALIST);
    }

    /**
     * Sets <code>value</code> as attribute value for IS_SPECIALIST using the alias name IsSpecialist.
     * @param value value to set the IS_SPECIALIST
     */
    public void setIsSpecialist(String value) {
        setAttributeInternal(ISSPECIALIST, value);
    }

    /**
     * Gets the attribute value for IS_CONTRACT_OFFICER using the alias name IsContractOfficer.
     * @return the IS_CONTRACT_OFFICER
     */
    public String getIsContractOfficer() {
        return (String) getAttributeInternal(ISCONTRACTOFFICER);
    }

    /**
     * Sets <code>value</code> as attribute value for IS_CONTRACT_OFFICER using the alias name IsContractOfficer.
     * @param value value to set the IS_CONTRACT_OFFICER
     */
    public void setIsContractOfficer(String value) {
        setAttributeInternal(ISCONTRACTOFFICER, value);
    }

    /**
     * Gets the attribute value for IS_OAA using the alias name IsOaa.
     * @return the IS_OAA
     */
    public String getIsOaa() {
        return (String) getAttributeInternal(ISOAA);
    }

    /**
     * Sets <code>value</code> as attribute value for IS_OAA using the alias name IsOaa.
     * @param value value to set the IS_OAA
     */
    public void setIsOaa(String value) {
        setAttributeInternal(ISOAA, value);
    }

    /**
     * Gets the attribute value for ACTIVE using the alias name Active.
     * @return the ACTIVE
     */
    public String getActive() {
        return (String) getAttributeInternal(ACTIVE);
    }

    /**
     * Sets <code>value</code> as attribute value for ACTIVE using the alias name Active.
     * @param value value to set the ACTIVE
     */
    public void setActive(String value) {
        setAttributeInternal(ACTIVE, value);
    }

    /**
     * Gets the attribute value for PHONE_NBR using the alias name PhoneNbr.
     * @return the PHONE_NBR
     */
    public String getPhoneNbr() {
        return (String) getAttributeInternal(PHONENBR);
    }

    /**
     * Sets <code>value</code> as attribute value for PHONE_NBR using the alias name PhoneNbr.
     * @param value value to set the PHONE_NBR
     */
    public void setPhoneNbr(String value) {
        setAttributeInternal(PHONENBR, value);
    }

    /**
     * Gets the attribute value for IS_CONNECT using the alias name IsConnect.
     * @return the IS_CONNECT
     */
    public String getIsConnect() {
        return (String) getAttributeInternal(ISCONNECT);
    }

    /**
     * Sets <code>value</code> as attribute value for IS_CONNECT using the alias name IsConnect.
     * @param value value to set the IS_CONNECT
     */
    public void setIsConnect(String value) {
        setAttributeInternal(ISCONNECT, value);
    }

    /**
     * Gets the attribute value for DATABASE_USER using the alias name DatabaseUser.
     * @return the DATABASE_USER
     */
    public String getDatabaseUser() {
        return (String) getAttributeInternal(DATABASEUSER);
    }

    /**
     * Sets <code>value</code> as attribute value for DATABASE_USER using the alias name DatabaseUser.
     * @param value value to set the DATABASE_USER
     */
    public void setDatabaseUser(String value) {
        setAttributeInternal(DATABASEUSER, value);
    }

    /**
     * Gets the attribute value for ACTIVE using the alias name Active1.
     * @return the ACTIVE
     */
    public String getActive1() {
        return (String) getAttributeInternal(ACTIVE1);
    }

    /**
     * Sets <code>value</code> as attribute value for ACTIVE using the alias name Active1.
     * @param value value to set the ACTIVE
     */
    public void setActive1(String value) {
        setAttributeInternal(ACTIVE1, value);
    }

    /**
     * Gets the attribute value for IS_GC using the alias name IsGc.
     * @return the IS_GC
     */
    public String getIsGc() {
        return (String) getAttributeInternal(ISGC);
    }

    /**
     * Sets <code>value</code> as attribute value for IS_GC using the alias name IsGc.
     * @param value value to set the IS_GC
     */
    public void setIsGc(String value) {
        setAttributeInternal(ISGC, value);
    }

    /**
     * Gets the attribute value for BUREAU_CODE using the alias name BureauCode.
     * @return the BUREAU_CODE
     */
    public String getBureauCode() {
        return (String) getAttributeInternal(BUREAUCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for BUREAU_CODE using the alias name BureauCode.
     * @param value value to set the BUREAU_CODE
     */
    public void setBureauCode(String value) {
        setAttributeInternal(BUREAUCODE, value);
    }

    /**
     * Gets the attribute value for OFFICE_CODE using the alias name OfficeCode.
     * @return the OFFICE_CODE
     */
    public String getOfficeCode() {
        return (String) getAttributeInternal(OFFICECODE);
    }

    /**
     * Sets <code>value</code> as attribute value for OFFICE_CODE using the alias name OfficeCode.
     * @param value value to set the OFFICE_CODE
     */
    public void setOfficeCode(String value) {
        setAttributeInternal(OFFICECODE, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link AbacusUserDetailsView.
     */
    public RowIterator getAbacusUserDetailsView() {
        return (RowIterator) getAttributeInternal(ABACUSUSERDETAILSVIEW);
    }

    /**
     * Gets the view accessor <code>RowSet</code> DivisionLOV1.
     */
    public RowSet getDivisionLOV1() {
        return (RowSet) getAttributeInternal(DIVISIONLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> TeamLOV1.
     */
    public RowSet getTeamLOV1() {
        return (RowSet) getAttributeInternal(TEAMLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> YesNoLOV1.
     */
    public RowSet getYesNoLOV1() {
        return (RowSet) getAttributeInternal(YESNOLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> BureauLOV1.
     */
    public RowSet getBureauLOV1() {
        return (RowSet) getAttributeInternal(BUREAULOV1);
    }


    /**
     * Gets the view accessor <code>RowSet</code> OfficeLOV1.
     */
    public RowSet getOfficeLOV1() {
        return (RowSet) getAttributeInternal(OFFICELOV1);
    }

    public void grantsDatabaseUser(String p_username, String isadhocuser){
         //String status = null;

        try( CallableStatement st = getDBTransaction().createCallableStatement("begin APP.MAINTAIN_USERS.databaseUsers(?,?); end;",0)) {
        
     st.setString(1, p_username);
     st.setString(2,isadhocuser);
      st.executeUpdate();
     } catch (Exception e) {
       System.out.println(""+ e.getMessage());
     }
                                    //return status;
    }

}