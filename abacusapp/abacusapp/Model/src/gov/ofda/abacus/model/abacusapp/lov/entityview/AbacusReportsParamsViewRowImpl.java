package gov.ofda.abacus.model.abacusapp.lov.entityview;

import gov.ofda.abacus.model.abacusapp.base.framework.AbacusViewRowImpl;
import gov.ofda.abacus.model.abacusapp.lov.entity.AbacusReportsParamsImpl;

import java.sql.Timestamp;

import oracle.jbo.RowSet;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Feb 16 11:12:57 EST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AbacusReportsParamsViewRowImpl extends AbacusViewRowImpl {


    public static final int ENTITY_ABACUSREPORTSPARAMS = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        RepId,
        ParamName,
        SeqId,
        IsRequired,
        Expression,
        DefaultValue,
        UpdId,
        UpdDate,
        Pname,
        YesNoLOV1,
        ColumnInfoParams1,
        ExpressionLOV1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int REPID = AttributesEnum.RepId.index();
    public static final int PARAMNAME = AttributesEnum.ParamName.index();
    public static final int SEQID = AttributesEnum.SeqId.index();
    public static final int ISREQUIRED = AttributesEnum.IsRequired.index();
    public static final int EXPRESSION = AttributesEnum.Expression.index();
    public static final int DEFAULTVALUE = AttributesEnum.DefaultValue.index();
    public static final int UPDID = AttributesEnum.UpdId.index();
    public static final int UPDDATE = AttributesEnum.UpdDate.index();
    public static final int PNAME = AttributesEnum.Pname.index();
    public static final int YESNOLOV1 = AttributesEnum.YesNoLOV1.index();
    public static final int COLUMNINFOPARAMS1 = AttributesEnum.ColumnInfoParams1.index();
    public static final int EXPRESSIONLOV1 = AttributesEnum.ExpressionLOV1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AbacusReportsParamsViewRowImpl() {
    }

    /**
     * Gets AbacusReportsParams entity object.
     * @return the AbacusReportsParams
     */
    public AbacusReportsParamsImpl getAbacusReportsParams() {
        return (AbacusReportsParamsImpl) getEntity(ENTITY_ABACUSREPORTSPARAMS);
    }

    /**
     * Gets the attribute value for REP_ID using the alias name RepId.
     * @return the REP_ID
     */
    public String getRepId() {
        return (String) getAttributeInternal(REPID);
    }

    /**
     * Sets <code>value</code> as attribute value for REP_ID using the alias name RepId.
     * @param value value to set the REP_ID
     */
    public void setRepId(String value) {
        setAttributeInternal(REPID, value);
    }

    /**
     * Gets the attribute value for PARAM_NAME using the alias name ParamName.
     * @return the PARAM_NAME
     */
    public String getParamName() {
        return (String) getAttributeInternal(PARAMNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for PARAM_NAME using the alias name ParamName.
     * @param value value to set the PARAM_NAME
     */
    public void setParamName(String value) {
        setAttributeInternal(PARAMNAME, value);
    }

    /**
     * Gets the attribute value for SEQ_ID using the alias name SeqId.
     * @return the SEQ_ID
     */
    public Integer getSeqId() {
        return (Integer) getAttributeInternal(SEQID);
    }

    /**
     * Sets <code>value</code> as attribute value for SEQ_ID using the alias name SeqId.
     * @param value value to set the SEQ_ID
     */
    public void setSeqId(Integer value) {
        setAttributeInternal(SEQID, value);
    }

    /**
     * Gets the attribute value for IS_REQUIRED using the alias name IsRequired.
     * @return the IS_REQUIRED
     */
    public String getIsRequired() {
        return (String) getAttributeInternal(ISREQUIRED);
    }

    /**
     * Sets <code>value</code> as attribute value for IS_REQUIRED using the alias name IsRequired.
     * @param value value to set the IS_REQUIRED
     */
    public void setIsRequired(String value) {
        setAttributeInternal(ISREQUIRED, value);
    }

    /**
     * Gets the attribute value for EXPRESSION using the alias name Expression.
     * @return the EXPRESSION
     */
    public String getExpression() {
        return (String) getAttributeInternal(EXPRESSION);
    }

    /**
     * Sets <code>value</code> as attribute value for EXPRESSION using the alias name Expression.
     * @param value value to set the EXPRESSION
     */
    public void setExpression(String value) {
        setAttributeInternal(EXPRESSION, value);
    }

    /**
     * Gets the attribute value for DEFAULT_VALUE using the alias name DefaultValue.
     * @return the DEFAULT_VALUE
     */
    public String getDefaultValue() {
        return (String) getAttributeInternal(DEFAULTVALUE);
    }

    /**
     * Sets <code>value</code> as attribute value for DEFAULT_VALUE using the alias name DefaultValue.
     * @param value value to set the DEFAULT_VALUE
     */
    public void setDefaultValue(String value) {
        setAttributeInternal(DEFAULTVALUE, value);
    }

    /**
     * Gets the attribute value for UPD_ID using the alias name UpdId.
     * @return the UPD_ID
     */
    public String getUpdId() {
        return (String) getAttributeInternal(UPDID);
    }

    /**
     * Gets the attribute value for UPD_DATE using the alias name UpdDate.
     * @return the UPD_DATE
     */
    public Timestamp getUpdDate() {
        return (Timestamp) getAttributeInternal(UPDDATE);
    }


    /**
     * Gets the attribute value for the calculated attribute Pname.
     * @return the Pname
     */
    public String getPname() {
        return (String) getAttributeInternal(PNAME);
    }

    /**
     * Gets the view accessor <code>RowSet</code> YesNoLOV1.
     */
    public RowSet getYesNoLOV1() {
        return (RowSet) getAttributeInternal(YESNOLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ColumnInfoParams1.
     */
    public RowSet getColumnInfoParams1() {
        return (RowSet) getAttributeInternal(COLUMNINFOPARAMS1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ExpressionLOV1.
     */
    public RowSet getExpressionLOV1() {
        return (RowSet) getAttributeInternal(EXPRESSIONLOV1);
    }


}

