 function startAsistTour(evt) {
          var intro = introJs();
          var indexId=rowKey = evt.getSource().getProperty("indexId");
          setIntroOptions(intro,indexId)
         intro.onbeforechange(function(targetElement){
              if(intro._currentStep ==10 &&  intro._currentStep <=23 && document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pc2')==null)
              {
                   var bene = document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:sdi1::disAcr');
                   var currentStep=intro._currentStep;
                   bene.click(setTimeout(function(){intro.exit();setIntroOptions(intro,indexId);intro.start().goToStepNumber(currentStep+1);},1000));
              }
          });

          intro.start();
    }
function setIntroOptions(intro,indexId) {
    intro.setOption('showProgress', true);
    intro.setOption('showStepNumbers', true);
    intro.setOption('tooltipPosition', 'auto');
    intro.setOption('disableInteraction', true);
    intro.setOption('scrollToElement', true);
    intro.setOption('positionPrecedence', ['bottom','left', 'right', 'top']);
    intro.setOptions({
            steps: [
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pgl2'),
                intro: "Award Details<ul><li>This section contains an overview of the award.  Hide this section to view ASIST details.<\/li><\/ul>"
              },
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:plam7'),
                intro: "AOR/COR<ul><li>This is the current AOR listed in Abacus<\/li><\/ul>"
              },
              //pt1:pt_region7:0:r1:0:pt1:pt_soc1
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:plam9'),
                intro: "Alternate AOR/COR<ul><li>This is the current Alternate AOR listed in Abacus<\/li><\/ul>"
              },
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pt_soc1'),
                intro: "GU Specialist<ul><li>This is the current GU Specialist listed in Abacus.<\/li><\/ul>"
              },
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pgl13'),
                intro: "Save and Cancel<ul><li>Use the Save button to save all changes.  Use the Cancel button to revert to the previously saved content.<\/li><\/ul>"
              },
              {
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:plam12'),
                intro: "USAID ASIST Webpage<ul><li>Click View to open the USAID ASIST Webpage.  USAID network access is required for the webpage to open.<\/li><\/ul>"
              },
              {
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:plam13'),
                intro: "USAID ASIST Training<ul><li>Click View to open the USAID ASIST Webpage.  USAID network access is required for the webpage to open.<\/li><\/ul>"
              },
              {
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:plam14'),
                intro: "Filing Responsibilities<ul><li>Click Go to review filing responsibilities by role.<\/li><\/ul>"
              }
              ,
              {
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:plam15'),
                intro: "AOR Status<ul><li>Summary of upload status for AORs.  This section is updated when documents are marked in the Uploaded into ASIST column.<\/li><\/ul>"
              }
              ,
              {
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:plam16'),
                intro: "GU Status<ul><li>Summary of upload status for the GU. This section is updated when documents are marked in the Uploaded into ASIST column.<\/li><\/ul>"
              },
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pgl12'), 
                intro: "AOR and GU Comments<ul><li>Include information on overall award filing updates or issues.<\/li><\/ul>"
              },
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pt2::tabh::cbc'), //pt1:pt_region1:0:r1:0:pt1:pt2::tabh::cbc
                intro: "Documents shows 3 tabs. <ul><li>All Documents: shows all documents in Abacus by award, along with ASIST section or sub-section where each document type belongs.<\/li><li>AOR Documents: shows all documents that AOR is responsible to upload to ASIST. AORs can mark each document as uploaded and edit or view comments for each document.<\/li><li>GU Documents: shows all documents that GU is responsible to upload to ASIST. The GU Specialist can mark each document as uploaded and edit or view comments for each document<\/li><\/ul>"
              },
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pc2:t4:0:sbc4::content'), //pt1:pt_region1:0:r1:0:pt1:pc2:t4:0:sbc4::content
                intro: "Select for download<ul><li>Check the box to select an individual document to download into a temporary file, where it can be uploaded into ASIST.<\/li><\/ul>"
              },
              
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pc2:t5::oc'), //pt1:pt_region1:0:r1:0:pt1:pc2:t5::oc
                intro: "Download All/Selected<ul>Click <b>Download All<\/b> as appropriate to select all documents to download into a temporary file, where they can be uploaded into ASIST.  If specific documents have been selected, this button will display <b>Download Selected.<\/b><\/li><\/ul>"
              },

              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pc2:t4:c23'),
                intro: "ASIST Section<ul><li>Identifies the section in ASIST where the document should be filed.<\/li><\/ul>"
              }
              ,
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pc2:t4:c25'),
                intro: "ASIST Subsection<ul><li>Identifies the subsection in ASIST where the document should be filed.<\/li><\/ul>"
              }
              ,
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pc2:t4:c27'),
                intro: "File name.<ul><li>When a file is downloaded, the file name will change to an ASIST-compatible name, along with the section and sub-section where it should be filed.<\/li><\/ul>"
              }
              ,
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pc2:t4:c29'),
                intro: "Abacus Document Group<ul><li>This column identifies the Abacus Document Group for each document.<\/li><\/ul>"
              }
              ,
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pc2:t4:c30'),
                intro: "Abacus Document Type<ul><li>This column identifies the Abacus Document Group for each document.<\/li><\/ul>"
              }
              ,
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pc2:t4:c34'),
                intro: "Uploaded into ASIST<ul><li>This checkbox must be manually selected once upload into ASIST is complete.  Each checkbox in this column changes the Document Upload Status above, identifying the number of pending document uploads required.  Once all documents in the award are checked, the overall status will changed to Uploaded.<\/li><\/ul>"
              }
             ,
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pc2:t4:c31'),
                intro: "Comments.<ul<li>Click Edit\/View to edit or view ASIST comments for the document.<\/li><\/ul>"
              }
            ,
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pc2:t4:c32'),
                intro: "Updated By<ul><li>This column identifies the staff member who most recently updated the ASIST details.<\/li><\/ul>"
              }
              ,
              { 
                element: document.getElementById('pt1:pt_region'+indexId+':0:r1:0:pt1:pc2:t4:c33'),
                intro: "Updated on<ul><li>This column identifies the date the ASIST details were last updated.<\/li><\/ul>"
              }
            ]
          });
}
