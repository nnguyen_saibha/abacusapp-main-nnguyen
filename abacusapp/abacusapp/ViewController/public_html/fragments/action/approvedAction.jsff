<?xml version='1.0' encoding='UTF-8'?>
<ui:composition xmlns:ui="http://java.sun.com/jsf/facelets" xmlns:af="http://xmlns.oracle.com/adf/faces/rich"
                xmlns:f="http://java.sun.com/jsf/core">
    <af:pageTemplate viewId="/WEB-INF/templates/editAction.jsf" value="#{bindings.ptb1}" id="pt1">
        <f:facet name="content">
            <af:panelFormLayout id="pfl1" partialTriggers="soc1 soc2">
                <af:separator id="s1" visible="false"/>
                <af:group id="g5"
                          rendered="#{pageFlowScope.isCheckbook ne 'N'}"
                          title="Approve Action">
                    <af:panelLabelAndMessage label="#{bindings.ApprovedAmt.hints.label}" id="plam5"
                                             rendered="#{bindings.ActionTypeCode.inputValue ne 'U'}"
                                             visible="#{securityContext.userInRole['APP_ADMIN']}">
                        <af:panelGroupLayout id="pgl3" layout="horizontal">
                            <af:inputNumberSpinbox value="#{bindings.ApprovedAmt.inputValue}"
                                                   label="#{bindings.ApprovedAmt.hints.label}"
                                                   required="#{bindings.ApprovedAmt.hints.mandatory}"
                                                   columns="#{bindings.ApprovedAmt.hints.displayWidth}"
                                                   shortDesc="#{bindings.ApprovedAmt.hints.tooltip}" id="it2"
                                                   simple="true" contentStyle="width:140px;"
                                                   readOnly="true">
                                <f:validator binding="#{bindings.ApprovedAmt.validator}"/>
                                <af:convertNumber type="currency" maxFractionDigits="2" minFractionDigits="2"
                                                  maxIntegerDigits="10"/>
                                <af:showPopupBehavior popupId="p1" triggerType="mouseHover" align="endBefore"/>
                            </af:inputNumberSpinbox>
                            <af:spacer width="10" height="10" id="s3"/>
                            <af:popup childCreation="deferred" autoCancel="disabled" id="p1">
                                <af:noteWindow id="nw1"
                                               inlineStyle="outline-color:InactiveCaption; outline-style:solid; background-color:InactiveCaption; outline-width:thin; width:300px;">
                                    <af:panelFormLayout id="pfl2" labelWidth="125"
                                                        inlineStyle="background-color:InfoBackground;" fieldWidth="150"
                                                        rows="3" maxColumns="1" labelAlignment="start">
                                        <f:facet name="footer"/>
                                        <af:panelLabelAndMessage label="#{bindings.PlannedAmt.hints.label}" id="plam1"
                                                                 rendered="#{bindings.ActionTypeCode.inputValue ne 'U'}"
                                                                 inlineStyle="text-align:right;">
                                            <af:outputText value="#{bindings.PlannedAmt.inputValue}"
                                                           shortDesc="#{bindings.PlannedAmt.hints.tooltip}" id="ot1"
                                                           inlineStyle="display:block; text-align:right; width:140px;">
                                                <af:convertNumber groupingUsed="false"
                                                                  pattern="#{bindings.PlannedAmt.format}"/>
                                            </af:outputText>
                                        </af:panelLabelAndMessage>
                                        <af:panelLabelAndMessage label="Total Sector Amt" id="plam2"
                                                                 rendered="#{bindings.ActionTypeCode.inputValue ne 'U'}"
                                                                 inlineStyle="text-align:right;">
                                            <af:outputText value="#{bindings.EditPaSectorView1Iterator.viewObject.sum['SectorAmt']}"
                                                           id="ot41"
                                                           inlineStyle="display:block; text-align:right; width:140px;">
                                                <af:convertNumber groupingUsed="true" pattern="$##,###,###,###.00"
                                                                  type="currency"/>
                                            </af:outputText>
                                        </af:panelLabelAndMessage>
                                        <af:panelLabelAndMessage label="Available Budget Amt" id="plam3">
                                            <af:outputText value="#{bindings.BudBalanceAmt.inputValue}"
                                                           inlineStyle="display:block; text-align:right; width:140px;"
                                                           shortDesc="#{bindings.BudBalanceAmt.hints.tooltip}" id="ot2">
                                                <af:convertNumber groupingUsed="false"
                                                                  pattern="#{bindings.BudBalanceAmt.format}"/>
                                            </af:outputText>
                                        </af:panelLabelAndMessage>
                                    </af:panelFormLayout>
                                </af:noteWindow>
                            </af:popup>
                        </af:panelGroupLayout>
                    </af:panelLabelAndMessage>
                    <af:inputDate value="#{bindings.ApprovedDate.inputValue}" 
                                  label="#{bindings.ApprovedDate.hints.label}"
                                  required="#{bindings.ApprovedDate.hints.mandatory}"
                                  columns="#{bindings.ApprovedDate.hints.displayWidth}"
                                  shortDesc="#{bindings.ApprovedDate.hints.tooltip}" id="id1"
                                  disabled="#{bindings.ActionTypeCode.inputValue ne 'U'}"  
                                  contentStyle="width:140px;" readOnly="#{pageFlowScope.isEditMode ne 'Y'}"
                                  autoSubmit="true">
                        <f:validator binding="#{bindings.ApprovedDate.validator}"/>
                        <af:convertDateTime pattern="#{bindings.ApprovedDate.format}"/>
                    </af:inputDate>
                    <af:group id="g2"
                              rendered="#{not ((bindings.FundingActionType.attributeValue eq '06') or (bindings.FundingActionType.attributeValue eq '12') or (bindings.FundingActionType.attributeValue eq '14'))}"
                              startBoundary="hide">
                        <af:inputListOfValues id="progMgrIdId"
                                              popupTitle="Search and Select: #{bindings.ProgMgrId.hints.label}"
                                              value="#{bindings.ProgMgrId.inputValue}"
                                              label="#{bindings.ProgMgrId.hints.label}"
                                              model="#{bindings.ProgMgrId.listOfValuesModel}"
                                              required="#{bindings.ProgMgrId.hints.mandatory}"
                                              columns="#{bindings.ProgMgrId.hints.displayWidth}"
                                              shortDesc="#{bindings.ProgMgrId.hints.tooltip}" autoSubmit="true"
                                              contentStyle="width:140px;" readOnly="#{pageFlowScope.isEditMode ne 'Y'}">
                            <f:validator binding="#{bindings.ProgMgrId.validator}"/>
                        </af:inputListOfValues>
                        <af:inputText value="#{bindings.NmsRequestNbr.inputValue}"
                                      label="#{bindings.NmsRequestNbr.hints.label}"
                                      required="#{bindings.NmsRequestNbr.hints.mandatory}"
                                      columns="#{bindings.NmsRequestNbr.hints.displayWidth}"
                                      maximumLength="#{bindings.NmsRequestNbr.hints.precision}"
                                      shortDesc="#{bindings.NmsRequestNbr.hints.tooltip}" id="it1"
                                      contentStyle="width:140px;"
                                      disabled="#{bindings.CommitFlag.attributeValue eq 'N' and (bindings.ActionTypeCode.attributeValue eq 'Z' or bindings.ActionTypeCode.attributeValue eq 'D')}"
                                      autoSubmit="true" readOnly="#{pageFlowScope.isEditMode ne 'Y'}">
                            <f:validator binding="#{bindings.NmsRequestNbr.validator}"/>
                        </af:inputText>
                    </af:group>
                    <af:group id="g10" rendered="#{bindings.ActionTypeCode.inputValue ne 'U'}" title="Approved Amt">
                        <af:panelCollection id="pc2" featuresOff="statusBar detach viewMenu"
                                            styleClass="AFStretchWidth">
                            <f:facet name="menus"/>
                            <f:facet name="toolbar">
                            <af:toolbar id="t6"
                                            rendered="#{pageFlowScope.isEditMode eq 'Y' &amp;&amp; securityContext.userInRole['APP_USER']}">
                                    <af:button actionListener="#{bindings.CreateInsert.execute}" text="Add"
                                               disabled="#{!bindings.CreateInsert.enabled}" id="b1t6"
                                               icon="/images/add.png"/>
                                    <af:button text="Add Project Budget" id="b3" action="addBudget" useWindow="true"
                                               windowHeight="500" windowWidth="950"
                                               returnListener="#{pageFlowScope.editActionBean.addBudgetReturnLsnr}"
                                               icon="/images/budget_add.png"
                                               rendered="#{pageFlowScope.isEditMode eq 'Y'  and (securityContext.userInRole['APP_BBL'] or securityContext.userInRole['APP_DL'] or securityContext.userInRole['APP_TL'] or  securityContext.userInRole['APP_OL']) and bindings.CommitFlag.inputValue eq 'Y' and bindings.ActionTypeCode.inputValue ne 'U'}"/>
                                </af:toolbar>
                            </f:facet>
                            <f:facet name="statusbar"/>
                            <af:table value="#{bindings.EditPaBudgetView1.collectionModel}" var="row"
                                      rows="#{bindings.EditPaBudgetView1.rangeSize}"
                                      emptyText="#{bindings.EditPaBudgetView1.viewable ? 'No data to display.' : 'Access Denied.'}"
                                      rowBandingInterval="0"
                                      selectedRowKeys="#{bindings.EditPaBudgetView1.collectionModel.selectedRow}"
                                      selectionListener="#{bindings.EditPaBudgetView1.collectionModel.makeCurrent}"
                                      rowSelection="single" fetchSize="#{bindings.EditPaBudgetView1.rangeSize}" id="t5"
                                      contentDelivery="immediate" disableColumnReordering="true"
                                      columnResizing="disabled" editingMode="clickToEdit" width="890"
                                      partialTriggers="::b1t6 l3 ::b3">
                                <af:column headerText="#{bindings.EditPaBudgetView1.hints.FundAcctTypeCode.label}" rendered="#{pageFlowScope.isEditMode eq 'Y'}"
                                           id="c9" width="350" showRequired="true" minimumWidth="12">
                                    <af:selectOneChoice value="#{row.bindings.FundAcctTypeCode.inputValue}"
                                                        label="#{row.bindings.FundAcctTypeCode.label}"
                                                        shortDesc="#{bindings.EditPaBudgetView1.hints.FundAcctTypeCode.tooltip}"
                                                        id="soc8" autoSubmit="true" required="true" showRequired="true"
                                                        unselectedLabel="Click here to select the Fund Account Type"
                                                        requiredMessageDetail=" Fund Account Type is required">
                                        <f:selectItems value="#{row.bindings.FundAcctTypeCode.items}" id="si10"/>
                                        <f:validator binding="#{row.bindings.FundAcctTypeCode.validator}"/>
                                    </af:selectOneChoice>
                                </af:column>
                                <af:column headerText="#{bindings.EditPaBudgetView1.hints.FundAcctTypeCode1.label}" rendered="#{pageFlowScope.isEditMode ne 'Y'}"
                                           id="c1"  width="250" minimumWidth="12">
                                    <af:selectOneChoice value="#{row.bindings.FundAcctTypeCode1.inputValue}" readOnly="true"
                                                        label="#{row.bindings.FundAcctTypeCode1.label}"
                                                        required="#{bindings.EditPaBudgetView1.hints.FundAcctTypeCode1.mandatory}"
                                                        shortDesc="#{bindings.EditPaBudgetView1.hints.FundAcctTypeCode1.tooltip}"
                                                        id="soc3">
                                        <f:selectItems value="#{row.bindings.FundAcctTypeCode1.items}" id="si3"/>
                                        <f:validator binding="#{row.bindings.FundAcctTypeCode1.validator}"/>
                                    </af:selectOneChoice>
                                </af:column>
                                <af:column headerText="#{bindings.EditPaBudgetView1.hints.PlannedAmt.label}" id="c10"
                                           width="120" showRequired="true" align="right">
                                    <af:inputNumberSpinbox value="#{row.bindings.PlannedAmt.inputValue}"
                                                           label="#{bindings.EditPaBudgetView1.hints.PlannedAmt.label}"
                                                           columns="#{bindings.EditPaBudgetView1.hints.PlannedAmt.displayWidth}"
                                                           shortDesc="#{bindings.EditPaBudgetView1.hints.PlannedAmt.tooltip}"
                                                           id="it5" autoSubmit="true"
                                                           readOnly="#{pageFlowScope.isEditMode ne 'Y'}"
                                                           showRequired="true" required="true"
                                                           requiredMessageDetail="Planned Amount is required">
                                        <f:validator binding="#{row.bindings.PlannedAmt.validator}"/>
                                        <af:convertNumber type="currency" maxIntegerDigits="10" minFractionDigits="2"
                                                          maxFractionDigits="2"/>
                                    </af:inputNumberSpinbox>
                                    <f:facet name="footer">
                                                        <af:outputText value="#{bindings.EditPaBudgetView1Iterator.viewObject.sum['PlannedAmt']}"
                                                                       id="ot6" inlineStyle="font-weight:bold;"
                                                                       partialTriggers="it5">
                                                            <af:convertNumber type="currency"
                                                                              pattern="$###,###,###,##0.00"/>
                                                        </af:outputText>
                                    </f:facet>
                                </af:column>
                                <af:column headerText="#{bindings.EditPaBudgetView1.hints.ApprovedAmt.label}" id="c11"
                                           rendered="#{!pageFlowScope.ignoreApprovedAction}" width="120" align="right">
                                    <af:inputNumberSpinbox value="#{row.bindings.ApprovedAmt.inputValue}"
                                                           label="#{bindings.EditPaBudgetView1.hints.ApprovedAmt.label}"
                                                           required="#{bindings.EditPaBudgetView1.hints.ApprovedAmt.mandatory}"
                                                           columns="#{bindings.EditPaBudgetView1.hints.ApprovedAmt.displayWidth}"
                                                           shortDesc="#{bindings.EditPaBudgetView1.hints.ApprovedAmt.tooltip}"
                                                           id="it6" autoSubmit="true"
                                                           readOnly="#{pageFlowScope.isEditMode ne 'Y'}">
                                        <f:validator binding="#{row.bindings.ApprovedAmt.validator}"/>
                                        <af:convertNumber type="currency" maxIntegerDigits="10" minFractionDigits="2"
                                                          maxFractionDigits="2"/>
                                    </af:inputNumberSpinbox>
                                    <f:facet name="footer">
                                                        <af:outputText value="#{bindings.EditPaBudgetView1Iterator.viewObject.sum['ApprovedAmt']}"
                                                                       id="ot3" inlineStyle="font-weight:bold;"
                                                                       partialTriggers="it6">
                                                            <af:convertNumber type="currency"
                                                                              pattern="$###,###,###,##0.00"/>
                                                        </af:outputText>
                                    </f:facet>
                                </af:column>
                                <af:column headerText="#{bindings.EditPaBudgetView1.hints.Comments.label}" id="c12"
                                           width="200">
                                    <af:inputText value="#{row.bindings.Comments.inputValue}"
                                                  label="#{bindings.EditPaBudgetView1.hints.Comments.label}"
                                                  required="#{bindings.EditPaBudgetView1.hints.Comments.mandatory}"
                                                  columns="#{bindings.EditPaBudgetView1.hints.Comments.displayWidth}"
                                                  maximumLength="#{bindings.EditPaBudgetView1.hints.Comments.precision}"
                                                  shortDesc="#{bindings.EditPaBudgetView1.hints.Comments.tooltip}"
                                                  id="it8" autoSubmit="true"
                                                  readOnly="#{pageFlowScope.isEditMode ne 'Y'}">
                                        <f:validator binding="#{row.bindings.Comments.validator}"/>
                                    </af:inputText>
                                </af:column>
                                <af:column headerText="#{bindings.EditPaBudgetView1.hints.UpdId.label}" id="c13"
                                           rendered="true" visible="false">
                                    <af:outputText value="#{row.UpdId}"
                                                   shortDesc="#{bindings.EditPaBudgetView1.hints.UpdId.tooltip}"
                                                   id="ot4"/>
                                </af:column>
                                <af:column headerText="#{bindings.EditPaBudgetView1.hints.UpdDate.label}" id="c14"
                                           rendered="true" visible="false">
                                    <af:outputText value="#{row.UpdDate}"
                                                   shortDesc="#{bindings.EditPaBudgetView1.hints.UpdDate.tooltip}"
                                                   id="ot5">
                                        <af:convertDateTime pattern="#{bindings.EditPaBudgetView1.hints.UpdDate.format}"/>
                                    </af:outputText>
                                </af:column>
                                <af:column id="c15" headerText="Delete" width="35"
                                           rendered="#{pageFlowScope.isEditMode eq 'Y' &amp;&amp; securityContext.userInRole['APP_USER']}">
                                    <af:link disabled="#{!bindings.Delete.enabled}" id="l3" immediate="true"
                                             icon="/images/delete.png" shortDesc="Delete Fund" partialSubmit="true"
                                             actionListener="#{pageFlowScope.editActionBean.DeleteBudgetRowActionLsnr}"/>
                                </af:column>
                            </af:table>
                        </af:panelCollection>
                    </af:group>
                    <af:group id="g1" rendered="#{pageFlowScope.ignoreAwardTracking}"
                              title="Duration/Period of Performance">
                        <af:inputDate value="#{bindings.AwardStartDate.inputValue}"
                                      label="#{bindings.AwardStartDate.hints.label}"
                                      required="#{bindings.AwardStartDate.hints.mandatory}"
                                      columns="#{bindings.AwardStartDate.hints.displayWidth}"
                                      shortDesc="#{bindings.AwardStartDate.hints.tooltip}" id="id2" autoSubmit="true"
                                      contentStyle="width:140px;" readOnly="#{pageFlowScope.isEditMode ne 'Y'}">
                            <f:validator binding="#{bindings.AwardStartDate.validator}"/>
                            <af:convertDateTime pattern="#{bindings.AwardStartDate.format}"/>
                        </af:inputDate>
                        <af:inputDate value="#{bindings.AwardEndDate.inputValue}"
                                      label="#{bindings.AwardEndDate.hints.label}"
                                      required="#{bindings.AwardEndDate.hints.mandatory}"
                                      columns="#{bindings.AwardEndDate.hints.displayWidth}"
                                      shortDesc="#{bindings.AwardEndDate.hints.tooltip}" id="id3" autoSubmit="true"
                                      contentStyle="width:140px;" readOnly="#{pageFlowScope.isEditMode ne 'Y'}">
                            <f:validator binding="#{bindings.AwardEndDate.validator}"/>
                            <af:convertDateTime pattern="#{bindings.AwardEndDate.format}"/>
                        </af:inputDate>
                        <af:button text="Award Details" id="b2" clientComponent="true"
                                   rendered="#{pageFlowScope.isEditMode ne 'N'}">
                            <af:showPopupBehavior popupId="pt_p2" triggerType="action"/>
                        </af:button>
                    </af:group>
                    <af:panelLabelAndMessage label="Sub Award Approval" id="plam4"
                                             rendered="#{bindings.ValidLetters.inputValue.contains('SGA')}">
                        <af:button text="Go" id="b1" action="lettersMemo" useWindow="true" windowHeight="768"
                                   windowWidth="1100">
                            <af:setActionListener from="SGA" to="#{requestScope.letterType}"/>
                            <af:showPopupBehavior popupId="pt_p1" disabled="#{!bindings.Commit.enabled}"/>
                        </af:button>
                    </af:panelLabelAndMessage>
                </af:group>
                <af:group id="g4" title="Reject Action">
                    <af:selectOneChoice value="#{bindings.RejectCode.inputValue}" label="Reason"
                                        required="#{bindings.RejectCode.hints.mandatory}"
                                        shortDesc="#{bindings.RejectCode.hints.tooltip}" id="soc2"
                                        autoSubmit="true" contentStyle="width:350px"
                                        validator="#{viewScope.proposalReviewBean.rejectValidator}"
                                        readOnly="#{pageFlowScope.isEditMode ne 'Y'}">
                        <f:selectItems value="#{bindings.RejectCode.items}" id="si2"/>
                        <f:validator binding="#{bindings.RejectCode.validator}"/>
                    </af:selectOneChoice>
                    <af:inputDate value="#{bindings.RejectDate.inputValue}" label="Date"
                                  columns="#{bindings.RejectDate.hints.displayWidth}"
                                  shortDesc="#{bindings.RejectDate.hints.tooltip}" id="id5"
                                  visible="#{not empty bindings.RejectCode.attributeValue}"
                                  binding="#{viewScope.proposalReviewBean.rejectDate}" showRequired="true"
                                  autoSubmit="true" requiredMessageDetail="Rejected Date is required"
                                  contentStyle="width:140px;" readOnly="#{pageFlowScope.isEditMode ne 'Y'}">
                        <f:validator binding="#{bindings.RejectDate.validator}"/>
                        <af:convertDateTime pattern="#{bindings.RejectDate.format}"/>
                    </af:inputDate>
                    <af:inputText value="#{bindings.RejectComments.inputValue}" label="Comments"
                                  required="#{bindings.RejectComments.hints.mandatory}"
                                  columns="#{bindings.RejectComments.hints.displayWidth}"
                                  maximumLength="#{bindings.RejectComments.hints.precision}"
                                  shortDesc="#{bindings.RejectComments.hints.tooltip}" id="it4"
                                  visible="#{not empty bindings.RejectCode.attributeValue}"
                                  binding="#{viewScope.proposalReviewBean.rejectComments}" showRequired="true"
                                  autoSubmit="true" rows="4" requiredMessageDetail="Comments are required"
                                  readOnly="#{pageFlowScope.isEditMode ne 'Y'}">
                        <f:validator binding="#{bindings.RejectComments.validator}"/>
                    </af:inputText>
                </af:group>
                <af:group id="g3" title="Defer Action">
                    <af:selectOneChoice value="#{bindings.DeferredCode.inputValue}" label="Reason"
                                        required="#{bindings.DeferredCode.hints.mandatory}"
                                        shortDesc="#{bindings.DeferredCode.hints.tooltip}" id="soc1" autoSubmit="true"
                                        contentStyle="width:350px"
                                        validator="#{viewScope.proposalReviewBean.rejectValidator}"
                                        readOnly="#{pageFlowScope.isEditMode ne 'Y'}">
                        <f:selectItems value="#{bindings.DeferredCode.items}" id="si1"/>
                        <f:validator binding="#{bindings.DeferredCode.validator}"/>
                    </af:selectOneChoice>
                    <af:inputDate value="#{bindings.DeferredDate.inputValue}" label="Date"
                                  columns="#{bindings.DeferredDate.hints.displayWidth}"
                                  shortDesc="#{bindings.DeferredDate.hints.tooltip}" id="id4"
                                  visible="#{not empty bindings.DeferredCode.attributeValue}"
                                  binding="#{viewScope.proposalReviewBean.deferredDate}" showRequired="true"
                                  autoSubmit="true" requiredMessageDetail="Deferred Date is required"
                                  contentStyle="width:140px;" readOnly="#{pageFlowScope.isEditMode ne 'Y'}">
                        <f:validator binding="#{bindings.DeferredDate.validator}"/>
                        <af:convertDateTime pattern="#{bindings.DeferredDate.format}"/>
                    </af:inputDate>
                    <af:inputText value="#{bindings.DeferredComments.inputValue}" label="Comments"
                                  columns="#{bindings.DeferredComments.hints.displayWidth}"
                                  maximumLength="#{bindings.DeferredComments.hints.precision}"
                                  shortDesc="#{bindings.DeferredComments.hints.tooltip}" id="it3"
                                  visible="#{not empty bindings.DeferredCode.attributeValue}"
                                  binding="#{viewScope.proposalReviewBean.deferredComments}" showRequired="true"
                                  autoSubmit="true" rows="4" requiredMessageDetail="Comments are required"
                                  readOnly="#{pageFlowScope.isEditMode ne 'Y'}">
                        <f:validator binding="#{bindings.DeferredComments.validator}"/>
                    </af:inputText>
                </af:group>
            </af:panelFormLayout>
        </f:facet>
        <f:facet name="toolbar"/>
        <f:attribute name="displayName" value="Approve/Reject"/>
    </af:pageTemplate>
</ui:composition>