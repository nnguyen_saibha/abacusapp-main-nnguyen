package gov.ofda.abacus.view.bean.lookup;

import com.tangosol.dev.assembler.Areturn;

import gov.ofda.abacus.view.base.Shuttle;
import gov.ofda.abacus.view.base.FactsLinkShuttle;
import java.io.Serializable;

import java.util.HashMap;
import java.util.List;

import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.QueryEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;

import oracle.jbo.ViewObject;

import oracle.jbo.server.ViewObjectImpl;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

public class FactsLink implements Serializable {
    @SuppressWarnings("compatibility:4668579188490117923")
    private static final long serialVersionUID = 1L;

    private String lovIterator;
    private String viewIterator;
    private String codeValue;
    
    private List selectedValuesList;

    private List objectiveAreaCodeListSelected;
    private RichPopup factsLinkPopup;
    private static final String LINK_TYPE = "linkType";
    private static final String LOV_ITERATOR = "lovIterator";
    private static final String VIEW_ITERATOR = "viewIterator";
    private static final String CODE_VALUE = "codeValue";
    private static final String AREA_FACTS_CODE_MAP = "areaFactsCodeMap";
    private static final String ELEMENT_FACTS_CODE_MAP = "elementsFactsCodeMap";
    private static final String SUB_ELEMENT_FACTS_CODE_MAP = "subElementsFactsCodeMap";
    
    public void setSelectedValuesList(List selectedValuesList) {
        this.selectedValuesList = selectedValuesList;
    }

    public List getSelectedValuesList() {
        return selectedValuesList;
    }


    public void setLovIterator(String lovIterator) {
        this.lovIterator = lovIterator;
    }

    public String getLovIterator() {
        return lovIterator;
    }

    public void setViewIterator(String viewIterator) {
        this.viewIterator = viewIterator;
    }

    public String getViewIterator() {
        return viewIterator;
    }

    public void setCodeValue(String codeValue) {
        this.codeValue = codeValue;
    }

    public String getCodeValue() {
        return codeValue;
    }
    
    public void setFactsLinkPopup(RichPopup factsLinkPopup) {
        this.factsLinkPopup = factsLinkPopup;
    }

    public RichPopup getFactsLinkPopup() {
        return factsLinkPopup;
    }
    


    public void setObjectiveAreaCodeListSelected(List objectiveAreaCodeListSelected) {
        this.objectiveAreaCodeListSelected = objectiveAreaCodeListSelected;
    }

    public List getObjectiveAreaCodeListSelected() {
        this.objectiveAreaCodeListSelected = Shuttle.getSelected("FactLinkViewObjectiveListIterator", "AreaCode");
        return objectiveAreaCodeListSelected;
    }

    public List getAvailableList(){
        if(null == this.codeValue || null == this.viewIterator || null == this.lovIterator) {
            Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            return Shuttle.getAll(String.valueOf(map.get(LOV_ITERATOR)), String.valueOf(map.get(VIEW_ITERATOR)), String.valueOf(map.get(CODE_VALUE)),
                      "Name");
        }else{
            return   Shuttle.getAll(this.lovIterator, this.viewIterator, this.codeValue,
                                          "Name");
        }
    }
    
    

    /**
     * Used by shuttle select items, This add all the available presumptive exceptions for the sector and already selected
     * presumptive excetions incase they are made inactive in which case they will not be in InaBmLookupView_Available_ListIterator.
     * @return
     */
    public List getAllAreaCodeList() {
        return Shuttle.getAll("FactsLookupAreaListViewIterator", "FactLinkViewObjectiveListIterator", "AreaCode",
                              "Name");
    }

    public void objectiveAreaLinkTabDisclosureListener(DisclosureEvent disclosureEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("FactsLookupObjectiveListIterator");
        Row row = iter.getCurrentRow();
        String objective = (String) row.getAttribute("Code");
        //setObjectiveAndExecute
        OperationBinding exec = bindings.getOperationBinding("setObjectiveAndExecute");
        exec.getParamsMap().put("pObj", objective);
        exec.execute();
    }


    public void areaLinkObjectiveValueChangeListener(ValueChangeEvent valueChangeEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("FactsLookupObjectiveListIterator");
        Row row = iter.getCurrentRow();
        String objective = (String) row.getAttribute("Code");
        //setObjectiveAndExecute
        OperationBinding exec = bindings.getOperationBinding("setObjectiveAndExecute");
        exec.getParamsMap().put("pObj", objective);
        exec.execute();

    }


    public void linkAreaCodeActionListner(ActionEvent actionEvent) {
        // set shuttle values
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        setLovIterator("FactsLookupAreaListViewIterator");
        setViewIterator("FactLinkViewObjectiveListIterator");
        setCodeValue("AreaCode");
        map.put(LOV_ITERATOR, getLovIterator());
        map.put(VIEW_ITERATOR, getViewIterator());
        map.put(CODE_VALUE, getCodeValue());
        map.put(LINK_TYPE, getCodeValue());
        
        if(null == map.get(AREA_FACTS_CODE_MAP)){
            Map areaFactsMap = new HashMap(); 
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iterator = bindings.findIteratorBinding("FactsLookupAreaListViewIterator");
            Row[] rowSet = iterator.getAllRowsInRange();
            for (Row row : rowSet) {
                areaFactsMap.put(row.getAttribute("AreaCode"), row.getAttribute("FactsCode"));
            }
            map.put(AREA_FACTS_CODE_MAP, areaFactsMap);
        }
        
        // set selected values
        this.selectedValuesList = Shuttle.getSelected(this.viewIterator, this.codeValue);
        RichPopup popup = (RichPopup) this.factsLinkPopup;
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
    }

    public void linkElementCodeActionListener(ActionEvent actionEvent) {
        // Add event code here... FactLinkElementViewIterator  FactsLookupElementListViewIterator
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        setLovIterator("FactsLookupElementListViewIterator");
        setViewIterator("FactLinkElementViewIterator");
        setCodeValue("ElementCode");
        map.put(LOV_ITERATOR, getLovIterator());
        map.put(VIEW_ITERATOR, getViewIterator());
        map.put(CODE_VALUE, getCodeValue());
        map.put(LINK_TYPE, getCodeValue());

        if(null == map.get(ELEMENT_FACTS_CODE_MAP)){
            Map elementFactsMap = new HashMap(); 
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iterator = bindings.findIteratorBinding("FactsLookupElementListViewIterator");
            Row[] rowSet = iterator.getAllRowsInRange();
            for (Row row : rowSet) {
                elementFactsMap.put(row.getAttribute("ElementCode"), row.getAttribute("FactsCode"));
            }
            map.put(ELEMENT_FACTS_CODE_MAP, elementFactsMap);
        }


        // set selected values
        this.selectedValuesList = Shuttle.getSelected(this.viewIterator, this.codeValue);
        RichPopup popup = (RichPopup) this.factsLinkPopup;
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);

    }

    public void linkSubElementCodeActionListener(ActionEvent actionEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        setLovIterator("FactsLookupSubElementListIterator");
        setViewIterator("FactsLinkSubElementViewIterator");
        setCodeValue("SubElementCode");
        map.put(LOV_ITERATOR, getLovIterator());
        map.put(VIEW_ITERATOR, getViewIterator());
        map.put(CODE_VALUE, getCodeValue());
        map.put(LINK_TYPE, getCodeValue());

        if(null == map.get(SUB_ELEMENT_FACTS_CODE_MAP)){
            Map subElementFactsMap = new HashMap(); 
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iterator = bindings.findIteratorBinding("FactsLookupSubElementListIterator");
            Row[] rowSet = iterator.getAllRowsInRange();
            for (Row row : rowSet) {
                subElementFactsMap.put(row.getAttribute("SubElementCode"), row.getAttribute("FactsCode"));
            }
            map.put(SUB_ELEMENT_FACTS_CODE_MAP, subElementFactsMap);
        }


        // set selected values
        this.selectedValuesList = Shuttle.getSelected(this.viewIterator, this.codeValue);
        RichPopup popup = (RichPopup) this.factsLinkPopup;
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);

    }



    public void factsLinkPopupDialogListner(DialogEvent dialogEvent) {
        
       if(DialogEvent.Outcome.ok == dialogEvent.getOutcome()){
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            
            DCIteratorBinding iter1 = bindings.findIteratorBinding("FactsLookupObjectiveListIterator");
            Key currentRowKey = iter1.getCurrentRow().getKey();
            
            
            //if(null != this.selectedValuesList && this.selectedValuesList.size() > 0){
                if(String.valueOf(map.get(LINK_TYPE)).equalsIgnoreCase("AreaCode")){
                    DCIteratorBinding iter = bindings.findIteratorBinding("FactsLookupObjectiveListIterator");
                    Row row = iter.getCurrentRow();
                    String objective = (String) row.getAttribute("ObjectiveCode");
                    
                    
                    FactsLinkShuttle.setObjectivesAreaSelected(this.selectedValuesList, String.valueOf(map.get(VIEW_ITERATOR)), String.valueOf(map.get(CODE_VALUE)), "DeleteObjectiveAreaLink",
                                        "CreateObjectiveAreaLink", objective, (Map)map.get(AREA_FACTS_CODE_MAP));
                    OperationBinding exec = bindings.getOperationBinding("Commit");
                    exec.execute();
                    
                    if(!exec.getErrors().isEmpty()){
                        exec = bindings.getOperationBinding("Rollback");
                        exec.execute();
                        FacesMessage message =
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, " Link cannot be deleted as child record exists!" , "");
                        FacesContext.getCurrentInstance().addMessage(null, message);
                    }
                    
                }else if(String.valueOf(map.get(LINK_TYPE)).equalsIgnoreCase("ElementCode")){
                    DCIteratorBinding iter = bindings.findIteratorBinding("FactLinkViewObjectiveListIterator");
                    Row row = iter.getCurrentRow();
                    String objective = (String) row.getAttribute("ObjectiveCode");
                    String areaCode = (String) row.getAttribute("AreaCode");
                    
                    
                    FactsLinkShuttle.setObjectivesElementSelected(this.selectedValuesList, String.valueOf(map.get(VIEW_ITERATOR)), String.valueOf(map.get(CODE_VALUE)), "DeleteElementLink",
                                        "CreateElementLink", objective, (Map)map.get(ELEMENT_FACTS_CODE_MAP), areaCode);
                    OperationBinding exec = bindings.getOperationBinding("Commit");
                    exec.execute();
                    if(!exec.getErrors().isEmpty()){
                        exec = bindings.getOperationBinding("Rollback");
                        exec.execute();
                        FacesMessage message =
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, " Link cannot be deleted as child record exists!" , "");
                        FacesContext.getCurrentInstance().addMessage(null, message);
                    }
    
                } else if (String.valueOf(map.get(LINK_TYPE)).equalsIgnoreCase("SubElementCode")){
                    DCIteratorBinding iter = bindings.findIteratorBinding("FactLinkElementViewIterator");
                    Row row = iter.getCurrentRow();
                    String objective = (String) row.getAttribute("ObjectiveCode");
                    String areaCode = (String) row.getAttribute("AreaCode");
                    String elementCode = (String) row.getAttribute("ElementCode");
                    
                    
                    FactsLinkShuttle.setObjectivesSubElementSelected(this.selectedValuesList, String.valueOf(map.get(VIEW_ITERATOR)), String.valueOf(map.get(CODE_VALUE)), "DeleteSubElementLink",
                                        "CreateSubElementLink", objective, (Map)map.get(SUB_ELEMENT_FACTS_CODE_MAP), areaCode, elementCode);
                    OperationBinding exec = bindings.getOperationBinding("Commit");
                    exec.execute();
                    if(!exec.getErrors().isEmpty()){
                        exec = bindings.getOperationBinding("Rollback");
                        exec.execute();
                        FacesMessage message =
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, " Link cannot be deleted as child record exists!" , "");
                        FacesContext.getCurrentInstance().addMessage(null, message);
                    }
               // } 
            }
            
            
            OperationBinding exec = bindings.getOperationBinding("Execute");
            exec.execute();
            
            if(null != iter1.findRowByKeyString(currentRowKey.toStringFormat(true))){
                iter1.setCurrentRowWithKey(currentRowKey.toStringFormat(true));
            }
       }        
    }

    public void processQueryListener(QueryEvent queryEvent) {
        // Add event code here... #{bindings.FactsLookupStaticViewCriteria1Query.processQuery}
        //FactsLookupObjectiveListIterator
        
        String obj = null;
        String areaCode = null;
        String elementCode = null;
        String subElementCode = null;
        
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("FactsLookupObjectiveListIterator");
        ViewObjectImpl voImpl = (ViewObjectImpl) iter.getViewObject();
        
        // get Bind Variables value
        if(voImpl.getNamedWhereClauseParam("pObj")!= null){
            obj = String.valueOf(voImpl.getNamedWhereClauseParam("pObj"));
        }
        
        if(voImpl.getNamedWhereClauseParam("pAreaCode")!= null){
            areaCode = String.valueOf(voImpl.getNamedWhereClauseParam("pAreaCode"));
        }
        if(voImpl.getNamedWhereClauseParam("pElementCode")!= null){
            elementCode = String.valueOf(voImpl.getNamedWhereClauseParam("pElementCode"));
        }
        
        if(voImpl.getNamedWhereClauseParam("pSubElementCode")!= null){
            subElementCode = String.valueOf(voImpl.getNamedWhereClauseParam("pSubElementCode"));
        }
        
        OperationBinding exec = bindings.getOperationBinding("setFactsLinkViewCriteria");
        exec.getParamsMap().put("obj", obj);
        exec.getParamsMap().put("areaCode", areaCode);
        exec.getParamsMap().put("elementCode", elementCode);
        exec.getParamsMap().put("subElementCode", subElementCode);
        exec.execute();
        
        
        invokeEL("#{bindings.FactsLookupStaticViewCriteria1Query.processQuery}", new Class[] { QueryEvent.class }, new Object[] {
                 queryEvent });
        
    }
    
    public static Object invokeEL(String el, Class[] paramTypes, Object[] params) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ELContext elContext = facesContext.getELContext();
        ExpressionFactory expressionFactory = facesContext.getApplication().getExpressionFactory();
        MethodExpression exp = expressionFactory.createMethodExpression(elContext, el, Object.class, paramTypes);
    return exp.invoke(elContext, params);
    }

}
