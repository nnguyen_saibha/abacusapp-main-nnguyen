package gov.ofda.abacus.view.bean.activity;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.binding.OperationBinding;

public class ActivityToolbar {
    private RichPopup uploadDocPopup;

    public void saveData(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding executeWithParams1 = bindings.getOperationBinding("Commit");
        executeWithParams1.execute();
        //AdfFacesContext.getCurrentInstance().getPageFlowScope().put("timeVar", System.currentTimeMillis());
    }

    public void cancel(ActionEvent actionEvent) {
        rollBackTransaction();
        //AdfFacesContext.getCurrentInstance().getPageFlowScope().put("timeVar", System.currentTimeMillis());

    }

    public void closeUploadDocPopup(ActionEvent actionEvent) {
        rollBackTransaction();
        RichPopup popup = this.uploadDocPopup;
        popup.hide();
        // Add event code here...
    }
    
    private void rollBackTransaction(){
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding executeWithParams1 = bindings.getOperationBinding("Rollback");
        executeWithParams1.execute();
    }
    
    public void setUploadDocPopup(RichPopup uploadDocPopup) {
        this.uploadDocPopup = uploadDocPopup;
    }

    public RichPopup getUploadDocPopup() {
        return uploadDocPopup;
    }

}
