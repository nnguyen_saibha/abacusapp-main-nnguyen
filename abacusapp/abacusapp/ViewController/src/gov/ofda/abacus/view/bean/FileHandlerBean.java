package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.model.abacusapp.entity.doc.Source;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.io.Serializable;

import java.math.BigDecimal;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.BlobDomain;

import org.apache.commons.io.IOUtils;
import org.apache.myfaces.trinidad.model.UploadedFile;


public class FileHandlerBean implements Serializable{
    
    private static final long serialVersionUID = -2866230967026238943L;
    private List<UploadedFile> uploadedFile;
    private List<String> resultDocIds;
    private List<Source> sourceIdList;
    private String message;
    private static final String SOURCE_TYPE_ACTION = "Action";
    private static final String SELECT_DOC_GROUP_AND_TYPE  = "*Please select Document Group and Document Type";

    public void setUploadedFile(List<UploadedFile> uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public List<UploadedFile> getUploadedFile() {
        return uploadedFile;
    }

    private Object createBlobDomain(UploadedFile file) {

        // init the internal variables
        InputStream in = null;
        BlobDomain blobDomain = null;
        OutputStream out = null;

        try {

            in = file.getInputStream();
            blobDomain = new BlobDomain();

            out = blobDomain.getBinaryOutputStream();
            IOUtils.copy(in, out);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.fillInStackTrace();
        }


        return blobDomain;
    }

    /**
     * This method deletes the Document reference from
     *      Action Docs (PA_DOCS) table for the given Doc ID,
     *          and doesn't delete the parent reference in Docs ( ABACUS_DOCS) table.
     * @param actionEvent
     */
    public void deleteDocumentFromActionDocs(ActionEvent actionEvent) {
        BigDecimal dcId =
            (BigDecimal) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("docId");

        Long actionId = (Long) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("actionId");

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcIteratorBinding = bindings.findIteratorBinding("PADocsView2Iterator");
        ViewObject view = dcIteratorBinding.getViewObject();
        view.setWhereClause("Doc_Id in (" + dcId + ")");
        view.executeQuery();

        RowSetIterator rsi = view.createRowSetIterator(null);
        while (rsi.hasNext()) {
            Row row = rsi.next();
            row.remove();
        }
        
        dcIteratorBinding = bindings.findIteratorBinding("LetterMemoDocView2Iterator");
        ViewObject view1 = dcIteratorBinding.getViewObject();
        view1.setWhereClause("Doc_Id in (" + dcId + ")");
        view1.executeQuery();

        RowSetIterator rsi2 = view1.createRowSetIterator(null);
        while (rsi2.hasNext()) {
            Row row = rsi2.next();
            row.remove();
        }
        
        OperationBinding executeWithParams1 = bindings.getOperationBinding("Commit");
        executeWithParams1.execute();

        OperationBinding executeWithParams3 = bindings.getOperationBinding("Execute");
        executeWithParams3.execute();

       // OperationBinding executeWithParams2 = bindings.getOperationBinding("ExecuteWithParams");
        //executeWithParams2.execute();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("timeVar", System.currentTimeMillis());
        AdfFacesContext.getCurrentInstance().addPartialTarget(actionEvent.getComponent().getParent().getParent());
    }

    /**
     *  This method deletes the Document reference from
     *      Activity Docs (ACTIVITY_DOCS) table for the given Doc ID,
     *          and doesn't delete the parent reference in Docs ( ABACUS_DOCS) table.
     * @param actionEvent
     */
    public void deleteDocumentFromActivity(ActionEvent actionEvent) {

        BigDecimal dcId =
            (BigDecimal) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("docId");

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcIteratorBinding = bindings.findIteratorBinding("ActivityEditView1_1Iterator");
        ViewObject view = dcIteratorBinding.getViewObject();
        view.setWhereClause("Doc_Id in (" + dcId + ")");
        view.executeQuery();

        RowSetIterator rsi = view.createRowSetIterator(null);
        while (rsi.hasNext()) {
            Row row = rsi.next();
            row.remove();
        }
        OperationBinding executeWithParams1 = bindings.getOperationBinding("Commit");
        executeWithParams1.execute();

        OperationBinding executeWithParams2 = bindings.getOperationBinding("ExecuteWithParams1");
        executeWithParams2.execute();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("timeVar", System.currentTimeMillis());
        AdfFacesContext.getCurrentInstance().addPartialTarget(actionEvent.getComponent().getParent().getParent());
    }

    /**
     * this method saves document table changes.
     * @param actionEvent
     */
    public void saveData(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding commitOper = bindings.getOperationBinding("Commit");
        commitOper.execute();
        FacesMessage message =
            new FacesMessage(FacesMessage.SEVERITY_INFO, "Your changes have been succesfully saved!", "");
        FacesContext.getCurrentInstance().addMessage(null, message);

        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("timeVar", System.currentTimeMillis());
    }

    /**
     * this method rollback all the changes on the page and refresh the page.
     * @param actionEvent
     */
   public void cancel(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding rollBackOper = bindings.getOperationBinding("Rollback");
        rollBackOper.execute();
        OperationBinding executeWithParams1 = bindings.getOperationBinding("ExecuteWithParams1");
        executeWithParams1.execute();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("timeVar", System.currentTimeMillis());

    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * This method validates the if any action's uploaded documents are
     * without Document Group / Document Type then returns a validation message.
     * @return
     */
    public String getMessage() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcIteratorBinding = bindings.findIteratorBinding("ActionDocEditView1Iterator");
        ViewObject docView = dcIteratorBinding.getViewObject();
        RowSetIterator iterator = docView.createRowSetIterator(null);
        boolean isDocTypeNull = false;
        while (iterator.hasNext()) {
            if (null == iterator.next().getAttribute("DocTypeCode")) {
                isDocTypeNull = true;
                break;
            }
        }
        if (isDocTypeNull) {
            return SELECT_DOC_GROUP_AND_TYPE;
        }
        return "";
    }
    
    /**
     * This method validates the if any project's uploaded documents are
     * without Document Group / Document Type then returns a validation message.
     * @return
     */
    public String getActivityMessage() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcIteratorBinding = bindings.findIteratorBinding("ActivityDocEditView1Iterator");
        ViewObject docView = dcIteratorBinding.getViewObject();
        RowSetIterator iterator = docView.createRowSetIterator(null);
        boolean isDocTypeNull = false;
        while (iterator.hasNext()) {
            if (null == iterator.next().getAttribute("DocTypeCode")) {
                isDocTypeNull = true;
                break;
            }
        }
        if (isDocTypeNull) {
            return SELECT_DOC_GROUP_AND_TYPE;
        }
        return "";
    }
    
    /**
     * This method validates the if any project's uploaded documents are
     * without Document Group / Document Type then returns a validation message.
     * @return
     */
    public String getAwardMessage() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcIteratorBinding = bindings.findIteratorBinding("LetterMemoDocEditView1Iterator");
        ViewObject docView = dcIteratorBinding.getViewObject();
        RowSetIterator iterator = docView.createRowSetIterator(null);
        boolean isDocTypeNull = false;
        while (iterator.hasNext()) {
            if (null == iterator.next().getAttribute("DocTypeCode")) {
                isDocTypeNull = true;
                break;
            }
        }
        if (isDocTypeNull) {
            return SELECT_DOC_GROUP_AND_TYPE;
        }
        return "";
    }

    /**
     * This method will retrieve document(s) from Input file component add to database when user click on 'upload' button.
     * @param actionEvent
     */
    public void uploadDocuments(ActionEvent actionEvent) {
        
        Boolean addFlg = false;
        List<UploadedFile> fileList = this.getUploadedFile();
        List<UploadedFile> removedDuplicateFilesLst = new ArrayList<UploadedFile>();
        if (fileList != null && fileList.size() > 0) {
            for (UploadedFile file : fileList) {
                boolean addFile = false;
                for (UploadedFile uploadedFile : removedDuplicateFilesLst) {
                    if (file.getFilename().equalsIgnoreCase(uploadedFile.getFilename())) {
                        addFile = true;
                    }
                }
                if (!addFile) {
                    removedDuplicateFilesLst.add(file);
                }
            }
        }
       
        if (fileList != null) {
            if (sourceIdList == null) {
                sourceIdList = new ArrayList<Source>();
            }
            Integer sourceType = null;
            String sourceTypeStr = null;
            Map pageFlowScopeMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            sourceTypeStr = (String) pageFlowScopeMap.get("sourceType");
            String fromLetter = (String) pageFlowScopeMap.get("fromLetter");
            sourceType = new Integer(sourceTypeStr);
            if (sourceType == 1) {
                Object actionIdObj = AdfFacesContext.getCurrentInstance().getPageFlowScope().get("actionId");
                //String actionId = "36579";
                Source source = new Source();
                source.setActionId(String.valueOf(actionIdObj));
                for (Source iSource : sourceIdList) {
                    if (iSource.getActionId().equalsIgnoreCase(source.getActionId())) {
                        addFlg = true;
                    }
                }
                if (!addFlg) {
                    sourceIdList.add(source);
                }

            }

            if (sourceType == 2) {
                Object fiscalYear =
                    AdfFacesContext.getCurrentInstance().getPageFlowScope().get("fiscalYear");
                String projectNbr = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("projectNbr");
                Source source = new Source();
                source.setBudgetFy(String.valueOf(fiscalYear));
                source.setProjectNbr(projectNbr);

                for (Source iSource : sourceIdList) {
                    if ((iSource.getBudgetFy().equalsIgnoreCase(source.getBudgetFy())) &&
                        (iSource.getProjectNbr().equalsIgnoreCase(source.getProjectNbr()))) {
                        addFlg = true;
                    }
                }
                if (!addFlg) {
                    sourceIdList.add(source);
                }
            }
            
            if(sourceType == 3){
                String awardId = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("awardId");
                String awardSeq = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("awardSeq");
                Source source = new Source();
                source.setAwardId(awardId);
                source.setAwardSeq(Integer.valueOf(awardSeq));
                if(null != fromLetter && fromLetter.equalsIgnoreCase("true")){
                    source.setLetterMemoId(String.valueOf(pageFlowScopeMap.get("letterMemoId")));
                }
                for (Source iSource : sourceIdList) {
                            if (iSource.getAwardId().equalsIgnoreCase(source.getAwardId()) &&
                                iSource.getAwardSeq().equals(source.getAwardSeq())) {
                                addFlg = true;
                            }
                }
                
                if (!addFlg) {
                    sourceIdList.add(source);
                }

            }
            
            if(sourceType == 4){
                Source source = new Source();
                source.setLetterMemoId(String.valueOf(pageFlowScopeMap.get("letterMemoId")));
                for (Source iSource : sourceIdList) {
                            if (iSource.getLetterMemoId().equalsIgnoreCase(source.getLetterMemoId())) {
                                addFlg = true;
                            }
                }
                
                if (!addFlg) {
                    sourceIdList.add(source);
                }

            }
            
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding fileAttachExecute = bindings.getOperationBinding("uploadDocuments");
            fileAttachExecute.getParamsMap().put("files", removedDuplicateFilesLst);
            fileAttachExecute.getParamsMap().put("sourceType", sourceType);
            fileAttachExecute.getParamsMap().put("sourceList", sourceIdList);
            fileAttachExecute.execute();
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put("timeVar", System.currentTimeMillis());
            // reset upload file
            setUploadedFile(null);
        }
        
        
    }
    
    /**
     * This method roll back all the changes made to the page,
     * and closes/hides the popup.
     * @param actionEvent
     */
    public void cancelPopup(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding executeWithParams1 = bindings.getOperationBinding("Rollback");
        executeWithParams1.execute();

        UIComponent component = findParentPopUp(actionEvent.getComponent());
        RichPopup popUp = (RichPopup) component; 
        popUp.hide();
    }


    private UIComponent findParentPopUp(UIComponent component) {
        UIComponent returnComp = null;
        if(component instanceof RichPopup){
             returnComp = component;
        }else{
             returnComp = findParentPopUp(component.getParent());
        }
        return returnComp;
    }

    public String setActionId() {
        String actionId = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("actionId");
        
        if(null != actionId && actionId.split("-").length == 2){
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put("actionId", actionId.split("-")[1]);
        }
        
        return "next";
    }

    public String setAwardId() {
     
     Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
     String awardId = (String) pageFlowMap.get("awardId");
     String awardSeq = (String) pageFlowMap.get("awardSeq");
     if(awardId == null ){
         if(null != awardSeq && awardSeq.split("-").length == 2){
             AdfFacesContext.getCurrentInstance().getPageFlowScope().put("awardSeq", awardSeq.split("-")[1]);
             //getAwardId
             DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
             OperationBinding exec = bindings.getOperationBinding("getAwardId");
             exec.getParamsMap().put("awardSeq", awardSeq.split("-")[1]);
             exec.execute();
             String result = (String) exec.getResult();
             AdfFacesContext.getCurrentInstance().getPageFlowScope().put("awardId", result);
         }
        }
        return "next";
    }
    
    public String getAwardId(){
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String awardId = (String) pageFlowMap.get("awardId");
        String awardSeq = (String) pageFlowMap.get("awardSeq");
        String returnVal = "";
        if(awardId != null ){
            returnVal = awardId;
        }else{
            if(null != awardSeq && awardSeq.split("-").length == 2){
                AdfFacesContext.getCurrentInstance().getPageFlowScope().put("awardSeq", awardSeq.split("-")[1]);
                //getAwardId
                DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding exec = bindings.getOperationBinding("getAwardId");
                exec.getParamsMap().put("awardSeq", awardSeq.split("-")[1]);
                exec.execute();
                String result = (String) exec.getResult();
                returnVal = result;
            }
        }
        return returnVal;
    }

    public void deleteDocumentFromAwardDocs(ActionEvent actionEvent) {
        BigDecimal dcId =
            (BigDecimal) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("docId");

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcIteratorBinding = bindings.findIteratorBinding("AwardDocView1Iterator");
        ViewObject view = dcIteratorBinding.getViewObject();
        view.setWhereClause("Doc_Id in (" + dcId + ")");
        view.executeQuery();

        RowSetIterator rsi = view.createRowSetIterator(null);
        while (rsi.hasNext()) {
            Row row = rsi.next();
            row.remove();
        }
        
        
        OperationBinding executeWithParams1 = bindings.getOperationBinding("Commit");
        executeWithParams1.execute();

        OperationBinding executeWithParams2 = bindings.getOperationBinding("ExecuteWithParams1");
        executeWithParams2.execute();
    
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("timeVar", System.currentTimeMillis());
        AdfFacesContext.getCurrentInstance().addPartialTarget(actionEvent.getComponent().getParent().getParent());
        
    }

    public void deleteDocumentsFromLetterMemoDocs(ActionEvent actionEvent) {
        BigDecimal dcId =
            (BigDecimal) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("docId");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcIteratorBinding = bindings.findIteratorBinding("LetterMemoDocView2Iterator");
        ViewObject view1 = dcIteratorBinding.getViewObject();
        view1.setWhereClause("Doc_Id in (" + dcId + ")");
        view1.executeQuery();

        RowSetIterator rsi2 = view1.createRowSetIterator(null);
        while (rsi2.hasNext()) {
            Row row = rsi2.next();
            row.remove();
        }
        
//        dcIteratorBinding = bindings.findIteratorBinding("AbacusDocsView2Iterator");
//                ViewObject view = dcIteratorBinding.getViewObject();
//                view.setWhereClause("Doc_Id in (" + dcId + ")");
//                view.executeQuery();
//
//                RowSetIterator rsi = view.createRowSetIterator(null);
//                while (rsi.hasNext()) {
//                    Row row = rsi.next();
//                    row.remove();
//                }
//                
        OperationBinding executeWithParams1 = bindings.getOperationBinding("Commit");
        executeWithParams1.execute();
        
        OperationBinding executeWithParams3 = bindings.getOperationBinding("Execute");
        executeWithParams3.execute();
        
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("timeVar", System.currentTimeMillis());
        AdfFacesContext.getCurrentInstance().addPartialTarget(actionEvent.getComponent().getParent().getParent());
    }

    public String setAwardOrActionId() {
        String actionId = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("actionId");
        Map pageFlowScopeMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        
        if(null != actionId && actionId.split("-").length == 2){
            if(null != actionId.split("-")[0] && SOURCE_TYPE_ACTION.equalsIgnoreCase(actionId.split("-")[0])){
                pageFlowScopeMap.put("iSourceType", 3);
                pageFlowScopeMap.put("actionId", actionId.split("-")[1]);
            }else{
                pageFlowScopeMap.put("iSourceType", 2);
                //getAwardId
                DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding exec = bindings.getOperationBinding("getAwardId");
                exec.getParamsMap().put("awardSeq", actionId.split("-")[1]);
                exec.execute();
                String result = (String) exec.getResult();
                pageFlowScopeMap.put("actionId", result);
            }
        }
        
        return "next";
    }
}
