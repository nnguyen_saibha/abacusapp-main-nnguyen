package gov.ofda.abacus.view.bean.report;

import gov.ofda.abacus.model.abacusapp.type.FavReportParamTable;
import gov.ofda.abacus.model.abacusapp.type.FavReportParamType;
import gov.ofda.abacus.model.abacusapp.type.FavReportType;
import gov.ofda.abacus.view.bean.AppBean;

import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;


import java.math.BigDecimal;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanRadio;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

/**
 * Used in report parameter taskflow to generate report URLs
 */
public class ReportParameterBean implements Serializable {
    @SuppressWarnings("compatibility:6157387859094899347")
    private static final long serialVersionUID = 1L;

    public ReportParameterBean() {
        super();
    }

    /**
     * Used by oracle reports, this will check for required parameter and construct oracle report url based on selected report format.
     * @param actionEvent
     */
    public void generateReport(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ReportView1Iterator");
        Row r = iter.getCurrentRow();
        StringBuilder missingReqParams = new StringBuilder();
        Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        /*
         * Set default format. If report format is null then default to PDF.
         */
        String format = "Pdf";
        if (requestScopeMap.get("reportFormat") != null)
            format = (String) requestScopeMap.get("reportFormat");
        //Check for current row
        if (r != null) {
            //Construct base URL
            Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
            StringBuilder paramURL = new StringBuilder();
            StringBuilder baseURL = new StringBuilder();
            baseURL.append(appScope.get("URL") + "/reports/rwservlet?Rep" + appScope.get("INSTANCE") + format);
            baseURL.append("+report=" + r.getAttribute("FileName"));
            //Iterate through report parameters and add parameters to report URL
            DCIteratorBinding paramIter = bindings.findIteratorBinding("ReportsParameterView1Iterator");
            RowSetIterator rsi = paramIter.getViewObject().createRowSetIterator(null);
            //Create a list to add to favorite report for all new reports
            List<FavReportParamType> frpTypeList = new ArrayList<FavReportParamType>();

            while (rsi.hasNext()) {
                Row paramRow = rsi.next();
                //Check for any missing required parameters
                if ("Y".equals(paramRow.getAttribute("IsRequired")) && paramRow.getAttribute("ParamValue") == null) {
                    missingReqParams.append(paramRow.getAttribute("ColumnDispName") + "; ");
                }
                if (paramRow.getAttribute("ParamValue") != null) {
                    paramURL.append("+" + paramRow.getAttribute("ParamName") + "=" +
                                    (paramRow.getAttribute("ParamValue").toString()).toUpperCase());
                    //Check if new report and add it to param list
                    if ("Y".equals(r.getAttribute("Isnew"))) {
                        FavReportParamType frpType = new FavReportParamType();
                        try {
                            frpType.setSeqId(new BigDecimal((Integer) paramRow.getAttribute("SeqId")));
                            frpType.setParamName((String) paramRow.getAttribute("ParamName"));
                            frpType.setParamExpression((String) paramRow.getAttribute("Expression"));
                            frpType.setParamValue((paramRow.getAttribute("ParamValue").toString()).toUpperCase());
                            frpTypeList.add(frpType);
                        } catch (SQLException e) {
                        }
                    }
                }
            }
            //Check for any missing parameters
            if (missingReqParams.length() > 0) {
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null,
                              new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                               "Missing required parameter(s) : " + missingReqParams.toString(), null));
            } else {

                if ("Pdf".equals(format))
                    baseURL.append("+DESNAME=" + r.getAttribute("FileName").toString().substring(0, 3) + "." +
                                   format.toLowerCase());
                else
                    baseURL.append("+saveas=" + r.getAttribute("FileName").toString().substring(0, 3) + "." +
                                   format.toLowerCase());
                
                FacesContext context = FacesContext.getCurrentInstance();
                ExtendedRenderKitService service = Service.getRenderKitService(context, ExtendedRenderKitService.class);
                //If report is new add to favorite report and get favorite ID
                if ("Y".equals(r.getAttribute("Isnew"))) {
                    try {
                        FavReportType frtype = new FavReportType();
                        frtype.setRepId(new BigDecimal((String) r.getAttribute("RepId")));
                        frtype.setFavRepName((String) r.getAttribute("RepName"));
                        frtype.setOutputFormat(format.toUpperCase());
                        frtype.setOwnerId(ADFContext.getCurrent().getSecurityContext().getUserName().toUpperCase());
                        frtype.setActive("N");
                        FavReportParamTable frpTable = new FavReportParamTable();
                        FavReportParamType[] frptArray = new FavReportParamType[frpTypeList.size()];
                        frptArray = frpTypeList.toArray(frptArray);
                        frpTable.setArray(frptArray);

                        OperationBinding exec = bindings.getOperationBinding("addToFavorites");
                        Map m = exec.getParamsMap();
                        Map<String, Object> t = new HashMap<String, Object>();
                        t.put("FavReportType", frtype);
                        t.put("FavReportParamTable", frpTable);
                        m.put("attrs", t);
                        exec.execute();
                        Long favID = (Long) exec.getResult();
                        service.addScript(context, "window.open(\"" + baseURL.toString()+"+P_FAV_ID="+favID + "\",\"_blank\")");
                        if(favID==0) {
                            MessageBean mb = new MessageBean();
                            mb.sendAdminEmail("Error creating favorite report", "Error: " + baseURL.toString()+paramURL.toString());
                        }
                        exec = bindings.getOperationBinding("ExecuteFavoriteReport");
                        exec.execute();
                        if (actionEvent != null)
                            AdfFacesContext.getCurrentInstance().addPartialTarget(actionEvent.getComponent().getParent().getParent().getParent());
                    } catch (Exception e) {
                        MessageBean mb = new MessageBean();
                        mb.sendAdminEmail("Issue adding favorite report", "Error: " + e.getStackTrace());
                    }
                }
                else {
                    service.addScript(context, "window.open(\"" + baseURL.toString()+paramURL.toString() + "\",\"_blank\")");
                }
                //Add to reports log
                try {
                    OperationBinding exec = bindings.getOperationBinding("logReportInfo");
                    Map m = exec.getParamsMap();
                    m.put("pageName", "ReportParameterBean");
                    m.put("reportName", r.getAttribute("FileName"));
                    m.put("reportID", r.getAttribute("RepId"));
                    exec.execute();
                    String result = (String) exec.getResult();
                    if (!"true".equals(result)) {
                        MessageBean mb = new MessageBean();
                        mb.sendAdminEmail("Issue logging reports", "Error: " + result);
                    }
                } catch (Exception e) {
                    MessageBean mb = new MessageBean();
                    mb.sendAdminEmail("Issue logging reports", "Error: " + e.getMessage());
                }
            }
        }
    }

    /**
     * This is called when users click on generate report directly from favorite report
     * @param actionEvent
     */
    public void generateFavoriteReport(ActionEvent actionEvent) {
        Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String favoriteID = (String) requestScopeMap.get("favoriteID");
        String reportFormat = (String) requestScopeMap.get("reportFormat");
        String fileName = (String) requestScopeMap.get("fileName");
        String desName = "DESNAME";

        if (favoriteID != null && reportFormat != null && fileName != null) {
            if ("PDF".equals(reportFormat))
                reportFormat = "Pdf";
            else {
                reportFormat = "Xls";
                desName = "saveas";
            }
            //Construct url with favorite id
            Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
            StringBuilder reportURL = new StringBuilder();
            reportURL.append(appScope.get("URL") + "/reports/rwservlet?Rep" + appScope.get("INSTANCE") + reportFormat);
            reportURL.append("+report=" + fileName + "+P_FAV_ID=" + favoriteID.toString() + "+" + desName + "=" +
                             fileName.substring(0, 3) + "." + reportFormat.toLowerCase());

            //Open report in new window
            FacesContext context = FacesContext.getCurrentInstance();
            ExtendedRenderKitService service = Service.getRenderKitService(context, ExtendedRenderKitService.class);
            service.addScript(context, "window.open(\"" + reportURL.toString() + "\",\"_blank\")");
            //log report to favorite reports log table
            try {
                DCBindingContainer bindings =
                    (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding exec = bindings.getOperationBinding("logFavoriteReport");
                Map m = exec.getParamsMap();
                m.put("favoriteID", favoriteID);
                exec.execute();
            } catch (Exception e) {
                MessageBean mb = new MessageBean();
                mb.sendAdminEmail("Issue adding favorite report log", "Error: " + e.getMessage());
            }
        } else {
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(actionEvent.getComponent().getId(),
                          new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                           "Select recently used first from below and try again", null));

        }
    }

    /**
     * @param actionEvent
     *  Used by BI reports, this will check for required parameter and construct BI url based on selected report format.
     */
    public void generateBIReport(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String reportFormat = (String) requestScopeMap.get("reportFormat");
        DCIteratorBinding iter = bindings.findIteratorBinding("ReportView1Iterator");
        Row r = iter.getCurrentRow();
        StringBuilder missingReqParams = new StringBuilder();
        //Check for current row
        if (r != null) {
            //Construct base URL
            Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
            StringBuilder reportURL = new StringBuilder();
            reportURL.append(appScope.get("URL") + r.getAttribute("RepUrl").toString());
            //Iterate through report parameters and add parameters to report URL
            DCIteratorBinding paramIter = bindings.findIteratorBinding("ReportsParameterView1Iterator");
            RowSetIterator rsi = paramIter.getViewObject().createRowSetIterator(null);
            while (rsi.hasNext()) {
                Row paramRow = rsi.next();
                //Check for any missing required parameters
                if ("Y".equals(paramRow.getAttribute("IsRequired")) && paramRow.getAttribute("ParamValue") == null) {
                    missingReqParams.append(paramRow.getAttribute("ColumnDispName") + "; ");
                }
                if (paramRow.getAttribute("ParamValue") != null) {
                    String t[] = ((String)paramRow.getAttribute("ParamValue")).split(",");
                    for(String i:t)
                    reportURL.append("&_params" + paramRow.getAttribute("ParamName") + "=" +
                                     i.toUpperCase());
                }
            }
            //Check for any missing parameters
            if (missingReqParams.length() > 0) {
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null,
                              new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                               "Missing required parameter(s) : " + missingReqParams.toString(), null));
            } else {
                if (reportFormat != null) {
                    if ("analyze".equals(reportFormat))
                        reportURL.append("&_xmode=3");
                    else
                        reportURL.append("&_xmode=4");
                    reportURL.append("&_xf=" + reportFormat);
                }
                //add script to open report in new window
                FacesContext context = FacesContext.getCurrentInstance();
                ExtendedRenderKitService service = Service.getRenderKitService(context, ExtendedRenderKitService.class);
                service.addScript(context, "window.open(\"" + reportURL.toString() + "\",\"_blank\")");
                //Add to reports log
                try {
                    OperationBinding exec = bindings.getOperationBinding("logReportInfo");
                    Map m = exec.getParamsMap();
                    m.put("pageName", "ReportParameterBean");
                    m.put("reportName", r.getAttribute("RepUrl"));
                    m.put("reportID", r.getAttribute("RepId"));
                    exec.execute();
                    String result = (String) exec.getResult();
                    if (!"true".equals(result)) {
                        MessageBean mb = new MessageBean();
                        mb.sendAdminEmail("Issue logging reports", "Error: " + result);
                    }
                } catch (Exception e) {
                    MessageBean mb = new MessageBean();
                    mb.sendAdminEmail("Issue logging reports", "Error: " + e.getMessage());
                }
            }
        }
    }

    /**
     * Not Used in V15.3b
     * @param actionEvent
     */
    public void setFavoriteReport(ActionEvent actionEvent) {
        Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String favoriteID = (String) requestScopeMap.get("favoriteID");
        if (favoriteID != null && !"0".equals(favoriteID)) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = bindings.getOperationBinding("setFavoriteReport");
            exec.getParamsMap().put("p_fav_id", favoriteID.toString());
            exec.execute();
            Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            pfp.put("favoriteID", "0");
        } else {
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(actionEvent.getComponent().getId(),
                          new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                           "Select recently used first from below and try again", null));

        }
    }
}
