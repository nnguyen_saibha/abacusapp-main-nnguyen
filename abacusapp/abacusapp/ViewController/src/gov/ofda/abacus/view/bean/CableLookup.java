package gov.ofda.abacus.view.bean;


import gov.ofda.abacus.view.base.JSFUtils;
import gov.ofda.abacus.view.base.UIControl;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.PopupCanceledEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.NavigatableRowIterator;
import oracle.jbo.Row;

import oracle.jbo.domain.DBSequence;

import org.apache.myfaces.trinidad.util.ComponentReference;

public class CableLookup extends UIControl implements Serializable {
    private static ADFLogger logger = ADFLogger.createADFLogger(CableLookup.class);
    @SuppressWarnings("compatibility:-2240372897545715675")
    private static final long serialVersionUID = 1L;
    private RichTable clookupTable;
    private RichPanelHeader cPanelHeader;
    private RichPopup cablePopup;
    private boolean addFlag = false;

    public void setAddFlag(boolean addFlag) {
        this.addFlag = addFlag;
    }

    public boolean isAddFlag() {
        return addFlag;
    }
    private boolean saveORcancelFlag = false;

    public void setSaveORcancelFlag(boolean saveORcancelFlag) {
        this.saveORcancelFlag = saveORcancelFlag;
    }

    public boolean isSaveORcancelFlag() {
        return saveORcancelFlag;
    }
    final String OLD_CURR_KEY_VIEWSCOPE_ATTR = "__oldCurrentRowKey__";


    public CableLookup() {
        super();
    }

    public void setClookupTable(RichTable clookupTable) {
        this.clookupTable = clookupTable;
    }

    public RichTable getClookupTable() {
        return clookupTable;
    }

    public void setCablePopup(RichPopup cablePopup) {
        this.cablePopup = cablePopup;
    }

    public RichPopup getCablePopup() {
        return cablePopup;
    }

    public void setCPanelHeader(RichPanelHeader cPanelHeader) {
        this.cPanelHeader = cPanelHeader;
    }

    public RichPanelHeader getCPanelHeader() {
        return cPanelHeader;
    }

    public void addNew(ActionEvent actionEvent) {
        OperationBinding oper1 = null;
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dciter = (DCIteratorBinding) bindings.get("CableLookupView1Iterator");
        Row oldCcurrentRow = dciter.getCurrentRow();
        //ADFContext is a convenient way to access all kinds of memory
        //scopes. If you like to be persistent in your ADF coding then this
        //is what you want to use
        ADFContext adfCtx = ADFContext.getCurrent();
        if (oldCcurrentRow != null)
            adfCtx.getViewScope().put(OLD_CURR_KEY_VIEWSCOPE_ATTR, oldCcurrentRow.getKey().toStringFormat(true));
        NavigatableRowIterator nav = dciter.getNavigatableRowIterator();
        Row newRow = nav.createRow();
        newRow.setNewRowState(Row.STATUS_INITIALIZED);
        Row currentRow = nav.getCurrentRow();
        int currentRowIndex = nav.getRangeIndexOf(currentRow);
        nav.insertRowAtRangeIndex(currentRowIndex + 1, newRow);
        dciter.setCurrentRowWithKey(newRow.getKey().toStringFormat(true));
        addFlag = true;
        RichPopup popup = (RichPopup) this.getCablePopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
    }

    public void editCable(ActionEvent actionEvent) {

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dciter = (DCIteratorBinding) bindings.get("CableLookupView1Iterator");
        Row oldCcurrentRow = dciter.getCurrentRow();
        logger.log("Entered with Key: " + oldCcurrentRow.getAttribute("SeqNo"));
        ADFContext adfCtx = ADFContext.getCurrent();
        adfCtx.getViewScope().put(OLD_CURR_KEY_VIEWSCOPE_ATTR, oldCcurrentRow.getKey().toStringFormat(true));
        addFlag = false;
        RichPopup popup = (RichPopup) this.getCablePopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
    }

    public void deleteCableDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec1 = bindings.getOperationBinding("Delete");
            exec1.execute();
            exec1 = bindings.getOperationBinding("Commit");
            exec1.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getClookupTable());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getCPanelHeader());
        }
    }

    public void saveCable(ActionEvent actionEvent) {
        DCBindingContainer bc = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterator = bc.findIteratorBinding("CableLookupView1Iterator");
        Row r = iterator.getCurrentRow();
        r.setAttribute("CableDescription", r.getAttribute("Subject"));

        OperationBinding ob1 = null;
        ob1 = bc.getOperationBinding("Commit");
        ob1.execute();
        saveORcancelFlag = true;
        AdfFacesContext.getCurrentInstance().addPartialTarget(clookupTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(cPanelHeader);
    }

    public void cancelCable(ActionEvent actionEvent) {
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = (DCIteratorBinding) bindingContainer.findIteratorBinding("CableLookupView1Iterator");
        Key key = iter.getCurrentRow().getKey();
        logger.log("Entered with Key: " + key.getKeyValues());

        OperationBinding operationBinding = bindingContainer.getOperationBinding("Rollback");
        operationBinding.execute();
        iter.setCurrentRowWithKey(key.toStringFormat(true));
        ADFContext adfCtx = ADFContext.getCurrent();

        saveORcancelFlag = true;
    }


    public void closeCable(ActionEvent actionEvent) {
        Boolean b = ((Boolean) JSFUtils.resolveExpression("#{bindings.Commit.enabled}"));
        logger.log("Commit flag:" + b);
        logger.log("addFlag flag:" + addFlag);
        if ((!addFlag) && b) {
            FacesMessage msg = new FacesMessage("Please Save or Cancel the changes before you close");
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage(null, msg);
        }

        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = (DCIteratorBinding) bindingContainer.findIteratorBinding("CableLookupView1Iterator");
        Row row = iter.getCurrentRow();
        Key key = iter.getCurrentRow().getKey();
       
        DBSequence cableSeqNo = (DBSequence) row.getAttribute("SeqNo");
        
        logger.log("SeqNo Entered with Key: " +cableSeqNo.getValue() );
        if (!addFlag) {
            OperationBinding operationBinding = bindingContainer.getOperationBinding("Rollback");
            operationBinding.execute();
            iter.setCurrentRowWithKey(key.toStringFormat(true));
        } 
        if(addFlag && (cableSeqNo.getValue() < 0)) {
            OperationBinding exec1 = bindingContainer.getOperationBinding("Delete");
            exec1.execute();
            exec1 = bindingContainer.getOperationBinding("Commit");
            exec1.execute();
        }
        ADFContext adfCtx = ADFContext.getCurrent();
        cablePopup.cancel();
        AdfFacesContext.getCurrentInstance().addPartialTarget(clookupTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(cablePopup);

    }
}

