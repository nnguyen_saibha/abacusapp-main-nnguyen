package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.view.base.UIControl;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.QueryEvent;

import oracle.binding.OperationBinding;

public class DocumentNavigator extends UIControl{
    @SuppressWarnings("compatibility:-9100116524404278051")
    private static final long serialVersionUID = -7650570691765828370L;

    public DocumentNavigator() {
        super();
    }
    public void searchQueryLsnr(QueryEvent queryEvent) {
        invokeEL("#{bindings.ActivityDocsUnionAllViewCriteriaQuery.processQuery}", new Class[] { QueryEvent.class },new Object[] { queryEvent });
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("applyCriteriaToAbacusDocs");
        exec.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(queryEvent.getComponent().getParent().getParent());
    }
}
