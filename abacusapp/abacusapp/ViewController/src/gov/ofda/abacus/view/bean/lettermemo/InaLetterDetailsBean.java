package gov.ofda.abacus.view.bean.lettermemo;

import gov.ofda.abacus.view.base.JSFUtils;
import gov.ofda.abacus.view.base.Shuttle;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.io.Serializable;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.sql.Timestamp;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.ComponentReference;
import org.apache.myfaces.trinidad.util.Service;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;

public class InaLetterDetailsBean implements Serializable{
    @SuppressWarnings("compatibility:3195678421334033592")
    private static final long serialVersionUID = 1L;
    private List preExceptionsSelected;
    private List substantialInvolvementListSelected;
    private List restrictedGoodsSelected;
    private ComponentReference downloadINAButton;

    public void setPreExceptionsSelected(List preExceptionsSelected) {
        this.preExceptionsSelected = preExceptionsSelected;
    }

    public List getPreExceptionsSelected() {
        this.preExceptionsSelected = Shuttle.getSelected("PaInaBmView1Iterator", "BmSeq");
        return preExceptionsSelected;
    }
    
    public void setSubstantialInvolvementListSelected(List substantialInvolvementListSelected) {
        this.substantialInvolvementListSelected = substantialInvolvementListSelected;
    }

    public List getSubstantialInvolvementListSelected() {
        this.substantialInvolvementListSelected = Shuttle.getSelected("PaInaSubstantialView1Iterator", "SubstantialSeq");
        return substantialInvolvementListSelected;
    }
    
    public void setRestrictedGoodsSelected(List restrictedGoodsSelected) {
        this.restrictedGoodsSelected = restrictedGoodsSelected;
    }

    public List getRestrictedGoodsSelected() {
        this.restrictedGoodsSelected = Shuttle.getSelected("PaRgInaView1Iterator", "RgCode");
        return restrictedGoodsSelected;
    }

    /**
     * Used by shuttle select items, This add all the available presumptive exceptions for the sector and already selected
     * presumptive excetions incase they are made inactive in which case they will not be in InaBmLookupView_Available_ListIterator.
     * @return
     */
    public List getAllPreExceptionList() {
        return Shuttle.getAll("InaBmLookupView_Available_ListIterator", "PaInaBmView1Iterator", "BmSeq",
                              "BmText");
    }
    
    /**
     * Used by shuttle select items, This add all the available substantial involvement list for the sector and already selected 
     * substantial involvement incase they are made inactive in which case they will not be in InaSubstantialLookupView1Iterator.
     * @return
     */
    public List getAllSubstantialInvolvementList() {
        return Shuttle.getAll("InaSubstantialLookupView1Iterator", "PaInaSubstantialView1Iterator", "SubstantialSeq",
                              "SubstantialText");
    }
    
    /**
     * Used by shuttle select items, This add all the available Restricted Goods list for the sector and already selected 
     * Restricted Goods incase they are made inactive in which case they will not be in PaRgInaLookupView1Iterator.
     * @return
     */
    public List getAllRestrictedGoodsList() {
        return Shuttle.getAll("PaRgInaLookupView1Iterator", "PaRgInaView1Iterator", "RgCode",
                              "RgName");
    }
    
    
    /**
     * Dialog listener for Presumptive Exceptions.
     * Here the Presumptive Exceptions are inserted and/or deleted based on the list presumptive Exceptions.
     * @param dialogEvent
     */
    public void presumptiveExceptionsDialogListner(DialogEvent dialogEvent) {
        
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            Shuttle.setSelected(this.preExceptionsSelected, "PaInaBmView1Iterator", "BmSeq", "Delete",
                                "CreateInsert");
        }
    }
    
    /**
     * Dialog listener for Substantial Involvement.
     * Here the Substantical Involvement(s) are inserted and/or deleted based on the list substantical involvement(s).
     * @param dialogEvent
     */
    public void substanticalInvolvementDialogListner(DialogEvent dialogEvent) {
        
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            Shuttle.setSelected(this.substantialInvolvementListSelected, "PaInaSubstantialView1Iterator", "SubstantialSeq", "Delete1",
                                "CreateInsert1");
        }
    }

    /**
     * Dialog listener for Restricted Goods.
     * Here the Restricted Goods are inserted and/or deleted based on the list restricted goods.
     * @param dialogEvent
     */
    public void restrictedGoodsDialogListner(DialogEvent dialogEvent) {
        
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            Shuttle.setSelected(this.restrictedGoodsSelected, "PaRgInaView1Iterator", "RgCode", "Delete2",
                                "CreateInsert2");
        }
    }
    public void exportINAToWord(FacesContext facesContext, OutputStream outputStream) {
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

        try (OutputStream out = outputStream) {
            response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            
            String path="/WEB-INF/templates/abacus_template.docx";
            String realPath=FacesContext.getCurrentInstance().getExternalContext().getRealPath(path);
            XWPFDocument document = new XWPFDocument(new FileInputStream(realPath));
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding proposal = bindings.findIteratorBinding("InaPADetails1Iterator");
            Row r=proposal.getCurrentRow();
            String fname="ina_"+r.getAttribute("BudgetFy")+"_"+r.getAttribute("ProjectNbr")+"_"+r.getAttribute("ActionSeqNbr")+".docx";
            response.setHeader("Content-disposition", "attachment;filename=\""+fname+"\"");
            XWPFParagraph main = document.createParagraph();
            main.setStyle("MainSection");
            XWPFRun mainrun = main.createRun();
            mainrun.setText("Application");
            
            XWPFTable table = document.createTable(1,2);
            CTTbl tableb        = table.getCTTbl();
            CTTblPr pr         = tableb.getTblPr();
            CTTblWidth  tblW = pr.getTblW();
            tblW.setW(BigInteger.valueOf(4500));
            tblW.setType(STTblWidth.PCT);
            pr.setTblW(tblW);
            CTJc jc = pr.addNewJc();        
            jc.setVal(STJc.RIGHT);
            pr.setJc(jc);
            tableb.setTblPr(pr);
            table.setStyleID("AbacusInfoTable");
            table.setInsideHBorder(XWPFTable.XWPFBorderType.NONE, 0, 0, null);
            table.setInsideVBorder(XWPFTable.XWPFBorderType.NONE, 0, 0, null);
            table.getCTTbl().getTblPr().unsetTblBorders();
            XWPFTableRow trow = table.getRow(0);
//                    trow.getCell(0).setText("Date Submitted to Grants Unit");
//                    trow.getCell(1).setText("");
//                    trow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(140));
//                    trow = table.createRow();
            trow.getCell(0).setText("Awardee");
            trow.getCell(1).setText((String) r.getAttribute("AwardeeName"));
            trow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(140));
            trow = table.createRow();
            trow.getCell(0).setText("Project Name");
            trow.getCell(1).setText((String) r.getAttribute("ProjectName"));
            trow = table.createRow();
            trow.getCell(0).setText("Country/Region");
            trow.getCell(1).setText((String) r.getAttribute("CountryName"));
            trow = table.createRow();
            trow.getCell(0).setText("Activity Title");
            trow.getCell(1).setText((String) r.getAttribute("ProposalName"));
            trow = table.createRow();
            trow.getCell(0).setText("Application Date Submitted");
            if(r.getAttribute("ProposalRecDate")!=null)
            trow.getCell(1).setText((String) (new SimpleDateFormat("MMMM dd, yyyy").format((Timestamp)r.getAttribute("ProposalRecDate"))));
            trow = table.createRow();
            trow.getCell(0).setText("Requester");
            trow.getCell(1).setText((String) r.getAttribute("RequesterName"));
            trow = table.createRow();
            trow.getCell(0).setText("Review Lead");
            if(r.getAttribute("ApplReviewLeadName")!=null)
               trow.getCell(1).setText((String) r.getAttribute("ApplReviewLeadName"));
            trow = table.createRow();           
            trow.getCell(0).setText("Proposed AOR");
            trow.getCell(1).setText((String) r.getAttribute("ProposedAotrName"));
//            trow = table.createRow();
//            trow.getCell(0).setText("Post-Award AOR");
//            trow.getCell(1).setText("");
            trow = table.createRow();
            trow.getCell(0).setText("Proposed Alt AOR");
            trow.getCell(1).setText((String) r.getAttribute("ProposedAltAotrName"));
            
            trow = table.createRow();
            trow.getCell(0).setText("Fund Type");
            trow.getCell(1).setText((String) r.getAttribute("ActionTypeName"));
            
            
            main = document.createParagraph();
            main.setStyle("MainSection");
            mainrun = main.createRun();
            mainrun.setText("Declaration of Humanitarian Need");
            XWPFParagraph textd = document.createParagraph();
            textd.setStyle("Text");
            XWPFRun rund = textd.createRun();
          
            StringBuilder DDtext=new StringBuilder();
            if( (Integer)r.getAttribute("DisasterFlagInaOption" ) == 1 )
            {
                DDtext.append("This award is a response to the Declaration of Humanitarian Need for the "+r.getAttribute("ProjectName")+", "+r.getAttribute("ProjectNbr") );
         
                if(r.getAttribute("DdDate") != null)
                  {
                    DDtext.append(", ");
                    DDtext.append((String) (new SimpleDateFormat("MMMM dd, yyyy").format((Timestamp)r.getAttribute("DdDate"))));
                  }
                DDtext.append("."); 
                
                rund.setText(DDtext.toString());
            }
            else if ((Integer)r.getAttribute("DisasterFlagInaOption" ) == 3 )
                rund.setText("This award is an early response for an anticipated emergency for the "+r.getAttribute("ProjectName")+","+r.getAttribute("ProjectNbr")+", for which a Declaration of Humanitarian Need has not yet occurred.");
            else              
                rund.setText("This award is for the "+r.getAttribute("ProjectName")+","+r.getAttribute("ProjectNbr")+", for which a Declaration of Humanitarian Need is not required.");
            
            DCIteratorBinding section = bindings.findIteratorBinding("PaInaSectionView1Iterator");
            Row[] srows = section.getAllRowsInRange();
            for (Row sr : srows) {
                if (sr.getAttribute("MainSecName") != null) {
                    XWPFParagraph mainp = document.createParagraph();
                    mainp.setStyle("MainSection");
                    XWPFRun run = mainp.createRun();
                    run.setText((String) sr.getAttribute("MainSecName"));
                }
                if (sr.getAttribute("SubSecName") != null) {
                    XWPFParagraph subp = document.createParagraph();
                    subp.setStyle("SubSection");
                    XWPFRun run = subp.createRun();
                    run.setText((String) sr.getAttribute("SubSecName"));
                    
                }
                if (sr.getAttribute("InaPrintText") != null||sr.getAttribute("InaText")!=null) {
                    XWPFParagraph text = document.createParagraph();
                    text.setStyle("Text");
                    XWPFRun run = text.createRun();
                    String tmp=null;
                    if("Y".equals(sr.getAttribute("InaOptionShowInputText")))
                        tmp=(String) sr.getAttribute("InaText");
                    else
                        tmp=(String) sr.getAttribute("InaPrintText");
                    if(tmp!=null) {
                        if(tmp.contains("\n")) {
                            String lines[]=tmp.split("\n");
                            run.setText(lines[0],0);
                            for(int i=1;i<lines.length;i++) {
                                run.addBreak();
                                run.setText(lines[i]);
                            }
                        }
                        else
                         run.setText(tmp,0);
                    }
                }
                
              if(sr.getAttribute("AutoPopulate")!=null && sr.getAttribute("AutoPopulate").toString().contains("Presumptive Exceptions")) {
                  DCIteratorBinding bm = bindings.findIteratorBinding("PaInaBmView1Iterator");
                  Row[] bmrows = bm.getAllRowsInRange();
                  if(bmrows.length>0) {
                      XWPFParagraph text = document.createParagraph();
                      text.setStyle("Text");
                      XWPFRun run = text.createRun();
                      run.setBold(true);
                      run.setText("Presumptive Exceptions List");
                      for(Row bmrow:bmrows) {
                          text = document.createParagraph();
                          text.setStyle("BulletSection");
                          run = text.createRun();
                          run.setText((String)bmrow.getAttribute("BmText"));
                      }
                  }
              }
            if("ACTIVITY".equals(sr.getAttribute("InaSection"))) {
                
                if( r.getAttribute("IsCostsheetApplicable")!=null && "Y".equals((String) r.getAttribute("IsCostsheetApplicable")) ) {
                    
                table = document.createTable(1, 3);
                tableb = table.getCTTbl();
                pr = tableb.getTblPr();
                tblW = pr.getTblW();
                tblW.setW(BigInteger.valueOf(4500));
                tblW.setType(STTblWidth.PCT);
                pr.setTblW(tblW);
                jc = pr.addNewJc(); 
                jc.setVal(STJc.RIGHT);
                pr.setJc(jc);
                tableb.setTblPr(pr);
                table.setStyleID("AbacusSectorTable");
                table.setInsideHBorder(XWPFTable.XWPFBorderType.NONE, 0, 0, null);
                table.setInsideVBorder(XWPFTable.XWPFBorderType.NONE, 0, 0, null);
                trow = table.getRow(0);
                mergeCellHorizontally(table, 0, 0, 2);
                trow.getCell(0).setText("In-Kind Food Commodities Estimates");
               
                trow = table.createRow();
                trow.getCell(0).setText("Total MTs");

                trow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1480));
                if (r.getAttribute("SumTotalMts") != null)
                    setRightAlignForTableColumn(trow.getCell(1),NumberFormat.getNumberInstance().format(r.getAttribute("SumTotalMts")));
                trow.getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(240));

                trow = table.createRow();
                trow.getCell(0).setText("Commodity");
                    if (r.getAttribute("SumCommPrice") != null)
                        setRightAlignForTableColumn(trow.getCell(1),NumberFormat.getCurrencyInstance().format(r.getAttribute("SumCommPrice")));
                
                trow = table.createRow();
                trow.getCell(0).setText("Ocean Freight");                
                if (r.getAttribute("SumOfAmt") != null)
                    setRightAlignForTableColumn(trow.getCell(1),NumberFormat.getCurrencyInstance().format(r.getAttribute("SumOfAmt")));
                
                trow = table.createRow();
                trow.getCell(0).setText("Inland Freight");
                if (r.getAttribute("SumIfAmt") != null)
                        setRightAlignForTableColumn(trow.getCell(1),NumberFormat.getCurrencyInstance().format(r.getAttribute("SumIfAmt")));
                    
                trow = table.createRow();
                trow.getCell(0).setText("ITSH");
                if (r.getAttribute("SumTotalItshAmt") != null)
                   setRightAlignForTableColumn(trow.getCell(1),NumberFormat.getCurrencyInstance().format(r.getAttribute("SumTotalItshAmt")));
                
                trow = table.createRow();
                trow.getCell(0).setText("202e");
                if (r.getAttribute("SumTotal202eAmt") != null)
                   setRightAlignForTableColumn(trow.getCell(1),NumberFormat.getCurrencyInstance().format(r.getAttribute("SumTotal202eAmt")));
                
                trow = table.createRow(); 
                trow.getCell(0).setText("Bag Amount");
                if (r.getAttribute("SumTotalBagAmt") != null)
                    setRightAlignForTableColumn(trow.getCell(1),NumberFormat.getCurrencyInstance().format(r.getAttribute("SumTotalBagAmt")));
                
                trow = table.createRow();
                trow.getCell(0).setText("Total");
                if (r.getAttribute("SumTotalAmt") != null)
                    setRightAlignForTableColumn(trow.getCell(1),NumberFormat.getCurrencyInstance().format(r.getAttribute("SumTotalAmt")));  
 
 
            }
                
                
                DCIteratorBinding sectors = bindings.findIteratorBinding("InaPASectorDetails1Iterator");
                Row[] sectorRows = sectors.getAllRowsInRange();
                XWPFParagraph sectorsp = document.createParagraph();
                sectorsp.setIndentationLeft(900);
                XWPFRun sectorrun =sectorsp.createRun();
                sectorrun.setBold(true);
                sectorrun.addCarriageReturn();
                sectorrun.setFontSize(12);
                sectorrun.setText("Sector Details");
                
                

                for (Row srr : sectorRows) {
                    table = document.createTable(1, 2);
                    tableb = table.getCTTbl();
                    pr = tableb.getTblPr();
                    tblW = pr.getTblW();
                    tblW.setW(BigInteger.valueOf(4500));
                    tblW.setType(STTblWidth.PCT);
                    pr.setTblW(tblW);
                    jc = pr.addNewJc();
                    jc.setVal(STJc.RIGHT);
                    pr.setJc(jc);
                    tableb.setTblPr(pr);
                    table.setStyleID("AbacusSectorTable");
                    table.setInsideHBorder(XWPFTable.XWPFBorderType.NONE, 0, 0, null);
                    table.setInsideVBorder(XWPFTable.XWPFBorderType.NONE, 0, 0, null);
                    trow = table.getRow(0);
                    mergeCellHorizontally(table, 0, 0, 1);
                    trow.getCell(0).setText((String) srr.getAttribute("SectorName"));
                    trow = table.createRow();
                    trow.getCell(0).setText("Sector Amount");
                    trow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(120));
                    if (srr.getAttribute("SectorAmt") != null)
                        trow.getCell(1).setText(NumberFormat.getCurrencyInstance().format(srr.getAttribute("SectorAmt")));
                    trow = table.createRow();
                    trow.getCell(0).setText("Assistance Type");
                    trow.getCell(1).setText((String) srr.getAttribute("SubAssistanceName"));
                    trow = table.createRow();
                    trow.getCell(0).setText("Mission Commitment");
                    trow.getCell(1).setText((String) srr.getAttribute("MissionCommitmentValue"));
                    if(srr.getAttribute("Objective")!=null)
                    {
                        trow = table.createRow();
                        trow.getCell(0).setText("Objective");
                        trow.getCell(1).setText((String) srr.getAttribute("Objective"));
                    }
                    trow = table.createRow();
                    trow.getCell(0).setText("Number of Unique People Targeted");
                    if (srr.getAttribute("PlannedTargeted") != null)
                        trow.getCell(1).setText(NumberFormat.getIntegerInstance().format(srr.getAttribute("PlannedTargeted")));
                    trow = table.createRow();
                    trow.getCell(0).setText("Nubmer of IDPs Targeted");
                    if (srr.getAttribute("IdpBeneficiariesPlanned") != null)
                        trow.getCell(1).setText(NumberFormat.getIntegerInstance().format(srr.getAttribute("IdpBeneficiariesPlanned")));
                    trow = table.createRow();
                    trow.getCell(0).setText("Number of Refugees Targeted");
                    if (srr.getAttribute("RefugeesTargeted") != null)
                        trow.getCell(1).setText(NumberFormat.getIntegerInstance().format(srr.getAttribute("RefugeesTargeted")));
                    trow = table.createRow();
                    trow.getCell(0).setText("Subsectors");
                    trow.getCell(1).setText((String) srr.getAttribute("SubsectorList"));
                    trow = table.createRow();
                    trow.getCell(0).setText("Locations");
                    trow.getCell(1).setText((String) srr.getAttribute("LocationList"));
                    document.createParagraph();
                }
            }
            else if("SUBSTANTIAL".equals(sr.getAttribute("InaSection"))) {
                DCIteratorBinding bm = bindings.findIteratorBinding("PaInaSubstantialView1Iterator");
                Row[] bmrows = bm.getAllRowsInRange();
                if(bmrows.length>0) {
                    XWPFParagraph text = document.createParagraph();
                    text.setStyle("Text");
                    XWPFRun run = text.createRun();
                    run.setBold(true);
                    run.setText("Substantial Involvement List");
                    for(Row bmrow:bmrows) {
                        text = document.createParagraph();
                        text.setStyle("BulletSection");
                        run = text.createRun();
                        run.setText((String)bmrow.getAttribute("SubstantialText"));
                    }
                }
            }
            else if("RGAWARDEE".equals(sr.getAttribute("InaSection"))) {
                if(r.getAttribute("RgGoodsInaOption")!=null) {
                    XWPFParagraph subp = document.createParagraph();
                    subp.setStyle("SubSection");
                    XWPFRun run = subp.createRun();
                    run.setText("Restricted Goods");
                    XWPFParagraph text = document.createParagraph();
                    text.setStyle("Text");
                    run = text.createRun();
                    if(((BigDecimal)r.getAttribute("RgGoodsInaOption")).intValue()==1) {
                        run.setText("The application includes the following restricted goods:");
                    //ExecutePARGINA
                    OperationBinding exec = bindings.getOperationBinding("ExecutePARGINA");
                    exec.execute();
                    DCIteratorBinding rg = bindings.findIteratorBinding("PaRgInaView1Iterator");
                    Row[] rgrows = rg.getAllRowsInRange();
                    if(rgrows.length>0) {
                        for(Row rgrow:rgrows) {
                            text = document.createParagraph();
                            text.setStyle("BulletSection");
                            run = text.createRun();
                            run.setBold(true);
                            run.setText((String)rgrow.getAttribute("RgName"));
                            
                            text = document.createParagraph();
                            text.setStyle("BulletSectionText");
                            run = text.createRun();
                            run.setText((String)rgrow.getAttribute("InaPrintText"));
                        }
                    }
                    }
                    else
                        run.setText("No restricted goods are included in the Recipient's application.");
                }
            }
            }

            XWPFParagraph signature=document.createParagraph();
            signature.setPageBreak(true);
            XWPFRun run=signature.createRun();
            run.setBold(true);
            run.setFontSize(14);
            run.setText("AOR/Alternate AOR Concurrence");
            DCIteratorBinding lm = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
            Row lmr=lm.getCurrentRow();
            signature=document.createParagraph();
            signature.setIndentFromLeft(100);
            run=signature.createRun();
            run.setFontSize(12);
            run.setBold(true);
            run.setText("Proposed AOR/Alternate");
            run=signature.createRun();
            run.setFontSize(12);
            run.setBold(false);
            if(lmr.getAttribute("Aor")!=null)
                    run.setText("      "+lmr.getAttribute("AorName"));
            run.addBreak();
            run.addBreak(); 
            run=signature.createRun();
            run.setFontSize(12);
            run.setBold(false);
            run.setText(lmr.getAttribute("AorConcurredName") + " has provided their concurrence of the information provided in the INA.");
            run.addBreak();
            run.addBreak();
            run=signature.createRun();
            run.setFontSize(12);
            run.setBold(true);
            run.setText("Date");
            run=signature.createRun();
            run.setFontSize(12);
            run.setBold(false);
           if(lmr.getAttribute("AorConcurredDate")!=null)
                       run.setText("                                            "+(String) (new SimpleDateFormat("MMMM dd, yyyy").format((Timestamp)lmr.getAttribute("AorConcurredDate"))));
            
            document.write(out);
            out.flush();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    static void mergeCellHorizontally(XWPFTable table, int row, int fromCol, int toCol) {
     for(int colIndex = fromCol; colIndex <= toCol; colIndex++){
      CTHMerge hmerge = CTHMerge.Factory.newInstance();
      if(colIndex == fromCol){
       // The first merged cell is set with RESTART merge value
       hmerge.setVal(STMerge.RESTART);
      } else {
       // Cells which join (merge) the first one, are set with CONTINUE
       hmerge.setVal(STMerge.CONTINUE);
      }
      XWPFTableCell cell = table.getRow(row).getCell(colIndex);
      // Try getting the TcPr. Not simply setting an new one every time.
      CTTcPr tcPr = cell.getCTTc().getTcPr();
      if (tcPr != null) {
       tcPr.setHMerge(hmerge);
      } else {
       // only set an new TcPr if there is not one already
       tcPr = CTTcPr.Factory.newInstance();
       tcPr.setHMerge(hmerge);
       cell.getCTTc().setTcPr(tcPr);
      }
     }
    }

    public void setDownloadINAButton(UIComponent downloadINAButton) {
        this.downloadINAButton = ComponentReference.newUIComponentReference(downloadINAButton);
    }

    public UIComponent getDownloadINAButton() {
        return downloadINAButton== null ? null : downloadINAButton.getComponent();
    }

    public void generateINA() {
        StringBuilder sb = new StringBuilder();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Boolean isCommitEnabled=(Boolean)JSFUtils.resolveExpression("#{bindings.Commit.enabled}");
        if(isCommitEnabled) {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_WARN, "Unsaved Changes","Unsaved Changes. Please save changes first and try again!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;
        }
        DCIteratorBinding iterLetterEdit = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
        Row r = iterLetterEdit.getCurrentRow();
        if (r.getAttribute("Aor") == null)
            sb.append("<li>AOR</li>");
        OperationBinding exec = bindings.getOperationBinding("checkMissingINADetails");
        exec.execute();
        List<String> missingSecList=(List<String>)exec.getResult();
        for(String m:missingSecList)
          sb.append("<li>"+m+"</li>");
        if(sb.length()>0)
        {
            FacesMessage msg =
                new FacesMessage("<html><body><b>Missing required information:</b><ul>"+sb.toString()+"</ul></body></html>");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;
        }
        else if(this.getDownloadINAButton()!=null){
            FacesContext fctx = FacesContext.getCurrentInstance();
            ExtendedRenderKitService erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
            StringBuilder script = new StringBuilder();
            script.append("document.getElementById('"+this.getDownloadINAButton().getClientId()+"').click();");
            erks.addScript(FacesContext.getCurrentInstance(), script.toString());
        }
    }
    private void setRightAlignForTableColumn(XWPFTableCell c,String t) {
        XWPFParagraph p = c.getParagraphs().get(0);
        p.setAlignment(ParagraphAlignment.RIGHT);
        XWPFRun cr = p.createRun();
        cr.setText(t);
        
    }
}
