package gov.ofda.abacus.view.bean;

import gov.ofda.menu.facade.MenuDetailFacade;


public class MenuCacheBean {
    
    
    private MenuDetailFacade facade = new MenuDetailFacade();
    
    public String clearMenuCache(){
       
        facade.clearMenuCache();
       
        return "goToNavigator";
    }
}
