package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.view.base.JSFUtils;
import gov.ofda.abacus.view.bean.activity.ViewActivityBean;

import java.io.Serializable;

import java.util.Map;

import javax.faces.application.NavigationHandler;

import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.view.rich.component.rich.RichQuery;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.model.QueryDescriptor;
import oracle.adf.view.rich.model.QueryModel;

import oracle.binding.OperationBinding;

public class EventBean implements Serializable {
    @SuppressWarnings("compatibility:7360610552526177616")
    private static final long serialVersionUID = 2357679900036751558L;

    public EventBean() {
        super();
    }
    public void handleEvent(Object payload) {
    }
    public Object publishEvent(Object payload) {
        return payload;
    }
    public void handleFormCloseEvent(Object payload) {
    }
    public Object publishFormCloseEvent(Object payload) {
        return payload;
    }
    public void handleEventRefreshViewAction(Object payload) {
            NavigationHandler  nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            if(nvHndlr!=null)
                nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "refresh");
    }
  public void publishNewProjectCreatedEvent() {
      
  }
  public void handleNewProjectCreatedEvent() {
      Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
      pageFlowAttrs.put("isNew",false);
      Map tmp=((ViewActivityBean)pageFlowAttrs.get("viewActivityBean")).getRegionTaskflowMap();
      tmp.put("isNew", false);
  }
  
    public void handleLookupNavigatorEvent(Object payload) {
        LookupsBean lb=(LookupsBean)JSFUtils.resolveExpression("#{pageFlowScope.LookupsBean}");
        RichQuery queryComp = (RichQuery)lb.getCommonLookupQuery();  
        if(queryComp!=null)
        {
        QueryModel queryModel = queryComp.getModel();  
        QueryDescriptor queryDescriptor = queryComp.getValue();  
        queryModel.reset(queryDescriptor);  
        queryComp.refresh(FacesContext.getCurrentInstance());  
        AdfFacesContext.getCurrentInstance().addPartialTarget(lb.getCommonLookupQuery());
        }
        
    }
    public Object publishLookupNavigatorEvent(Object payload) {
        return payload;
    }
    
}
