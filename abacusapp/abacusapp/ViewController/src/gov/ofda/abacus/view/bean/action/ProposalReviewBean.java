package gov.ofda.abacus.view.bean.action;

import gov.ofda.abacus.view.base.UIControl;

import java.io.Serializable;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;


import oracle.jbo.Row;

import org.apache.myfaces.trinidad.util.ComponentReference;

/**
 * ProposalReviewBean is the managed bean for the proposal review tab. It has logic to disable the tabs after it is rejected or diferred
 *
 */
public class ProposalReviewBean extends UIControl implements Serializable {
    @SuppressWarnings("compatibility:993502673289917925")
    private static final long serialVersionUID = -403671368917714479L;
    private ComponentReference deferredDate;
    private ComponentReference deferredComments;
    private ComponentReference rejectDate;
    private ComponentReference rejectComments;


    public ProposalReviewBean() {
        super();
    }


    public void setDeferredDate(UIComponent deferredDate) {
        this.deferredDate = ComponentReference.newUIComponentReference(deferredDate);

    }

    public UIComponent getDeferredDate() {
        return deferredDate == null ? null : deferredDate.getComponent();
    }

    public void setDeferredComments(UIComponent deferredComments) {
        this.deferredComments = ComponentReference.newUIComponentReference(deferredComments);

    }

    public UIComponent getDeferredComments() {
        return deferredComments == null ? null : deferredComments.getComponent();
    }

    public void setRejectDate(UIComponent rejectDate) {
        this.rejectDate = ComponentReference.newUIComponentReference(rejectDate);

    }

    public UIComponent getRejectDate() {
        return rejectDate == null ? null : rejectDate.getComponent();
    }

    public void setRejectComments(UIComponent rejectComments) {
        this.rejectComments = ComponentReference.newUIComponentReference(rejectComments);

    }

    public UIComponent getRejectComments() {
        return rejectComments == null ? null : rejectComments.getComponent();
    }

    /**
     *This method retrieves the action status Amt
     * @return
     */

    private Integer retrieveActionStatusAmt() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        exec = bindings.getOperationBinding("getCalculatedActionAmtStatus");
        exec.execute();
        Integer retActionAmtStatus = null;
        retActionAmtStatus = (Integer) exec.getResult();
        return retActionAmtStatus;
    }

    /**
     *  This validator method is used for both Reject code and defer code
     * @param facesContext
     * @param uIComponent
     * @param object
     */

    public void rejectValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        Integer actionAmtStatus = retrieveActionStatusAmt();       
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if (object != null && actionAmtStatus > 1 && actionAmtStatus <= 5) {
            throw new ValidatorException(new FacesMessage("Only Planned Action can be rejected"));
        } else if (object == null && actionAmtStatus > 5) {           
            pfm.put("isCheckbook", "Y");
        }
        else {           
            pfm.put("isCheckbook", "N");
        }


    }

}
