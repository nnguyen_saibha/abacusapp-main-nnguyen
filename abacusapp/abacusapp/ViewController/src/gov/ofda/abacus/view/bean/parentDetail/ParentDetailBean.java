package gov.ofda.abacus.view.bean.parentDetail;

import java.io.Serializable;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class ParentDetailBean implements Serializable{
    @SuppressWarnings("compatibility:7855910097969312412")
    private static final long serialVersionUID = 1L;
    
    public ParentDetailBean(){
        super(); 
    }

    public String initializeParentDetailSet() {
        Object parentActionId = AdfFacesContext.getCurrentInstance().getPageFlowScope().get("parentActionID");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("initializeParentDetailSet");
        exec.getParamsMap().put("parentActionId", parentActionId);
        exec.execute();
        return "true";
    }
}
