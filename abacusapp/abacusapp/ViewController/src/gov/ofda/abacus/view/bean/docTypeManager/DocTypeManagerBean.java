package gov.ofda.abacus.view.bean.docTypeManager;

import gov.ofda.abacus.view.base.JSFUtils;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.QueryEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class DocTypeManagerBean {
    public String setCurrentRow() {
        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
               DCIteratorBinding iter=null;
                Key key=(Key)AdfFacesContext.getCurrentInstance().getPageFlowScope().get("rowKey");
                if(key==null)
                {
                    iter = (DCIteratorBinding)bindings.findIteratorBinding("MegaDocTypeLookupView1Iterator");
                    key=iter.getCurrentRow().getKey();
                }
                OperationBinding exec = bindings.getOperationBinding("Execute");
                exec.execute();
                iter = (DCIteratorBinding)bindings.findIteratorBinding("MegaDocTypeLookupView1Iterator");
                RowSetIterator rsi = iter.getRowSetIterator(); 
                if(rsi.findByKey(key, 1).length>0) 
                {
                  iter.setCurrentRowWithKey(key.toStringFormat(true));
                }
        return "next";
    }

    public void deleteMegaDocumentDialogListner(DialogEvent dialogEvent) {
        if( DialogEvent.Outcome.yes == dialogEvent.getOutcome()){
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            
            DCIteratorBinding iter = bindings.findIteratorBinding("MegaDocTypeLookupView1Iterator");
            Row currentRow = iter.getCurrentRow();
            
            String mDocName = (String) currentRow.getAttribute("MdocTypeName");
            
            OperationBinding execDelete = bindings.getOperationBinding("Delete");
            execDelete.execute();
            
            OperationBinding execCommit = bindings.getOperationBinding("Commit");
            execCommit.execute();
            if(!execCommit.getErrors().isEmpty()){
                OperationBinding execRollback = bindings.getOperationBinding("Rollback");
                execRollback.execute();
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Document cannot be deleted as child record exists!", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
                ((RichPopup)dialogEvent.getComponent().getParent()).cancel();
            }else{
                OperationBinding exec = bindings.getOperationBinding("Execute");
                exec.execute();
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, mDocName +" has been deleted successfully." , "");
                FacesContext.getCurrentInstance().addMessage(null, message); 
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(dialogEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
    }

    public void deleteDocumentDialogListner(DialogEvent dialogEvent) {
        // Add event code here... deleteMdocTypeLink
        if( DialogEvent.Outcome.yes == dialogEvent.getOutcome()){
            NavigationHandler  nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            if(nvHndlr!=null)
                nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "deleteMdocTypeLink");
            return;
        }
    }

    public void searchQueryListner(QueryEvent queryEvent) {
        // Add event code here...#{bindings.MegaDocTypeLookupViewCriteriaQuery.processQuery}
        
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        //OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        SecurityContext scntx = ADFContext.getCurrent().getSecurityContext();
        Boolean isCommitEnabled=(Boolean)JSFUtils.resolveExpression("#{bindings.Commit.enabled}");
        if(isCommitEnabled) {
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_WARN, "Please save or cancel your changes", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }else{
            FacesContext fctx = FacesContext.getCurrentInstance();
            Application application = fctx.getApplication();
            ExpressionFactory expressionFactory = application.getExpressionFactory();
            ELContext elctx = fctx.getELContext();

            MethodExpression methodExpression =
                expressionFactory.createMethodExpression(elctx, "#{bindings.MegaDocTypeLookupViewCriteriaQuery.processQuery}",
                                                         Object.class, new Class[] { QueryEvent.class });
            methodExpression.invoke(elctx, new Object[] { queryEvent });
        }
    }

    public void saveActionListner(ActionEvent actionEvent) {
        //Commit
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding execCommit = bindings.getOperationBinding("Commit");
        if(execCommit.isOperationEnabled()){
            execCommit.execute();
            if(execCommit.getErrors().isEmpty()){
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        } else {
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "No changes to save.", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        
        NavigationHandler  nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        if(nvHndlr!=null)
            nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "save");
    }

    public void cancelActionListener(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding execRollback = bindings.getOperationBinding("Rollback");
        if(execRollback.isOperationEnabled()){
            execRollback.execute();
            if(execRollback.getErrors().isEmpty()){
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancelled successfully", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
             }
           }
        }
}
