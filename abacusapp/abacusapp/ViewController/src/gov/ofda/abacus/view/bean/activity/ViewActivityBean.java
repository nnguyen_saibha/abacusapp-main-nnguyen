package gov.ofda.abacus.view.bean.activity;

import gov.ofda.abacus.view.base.AbacusConstants;
import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.ApplicationModule;

import org.apache.myfaces.trinidad.event.ReturnEvent;

public class ViewActivityBean implements Serializable {
    @SuppressWarnings("compatibility:8787040547340746590")
    private static final long serialVersionUID = 1L;
    private String taskFlowId="/WEB-INF/tflows/activity/activity-details-flow.xml#activity-details-flow";
    private Map<String, Object> regionTaskflowMap = new HashMap<String, Object>();
    private final String saveErrorMessage = "Please save or cancel changes to continue.";
    
    private MessageBean messageBean;
    
    public void setRegionTaskflowMap(Map<String, Object> regionTaskflowMap) {
        this.regionTaskflowMap = regionTaskflowMap;
    }

    public Map<String, Object> getRegionTaskflowMap() {
        return regionTaskflowMap;
    }

    public ViewActivityBean() {
        super();
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        regionTaskflowMap.clear();
        regionTaskflowMap.put("budget_year", pageFlowAttrs.get("budget_year"));
        regionTaskflowMap.put("project_number", pageFlowAttrs.get("project_number"));
        regionTaskflowMap.put("showView", "tabs");
        SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
        if (sc.isUserInRole("APP_FM") || sc.isUserInRole("APP_FINANCE"))
            regionTaskflowMap.put("showColumns", "Funds");
        else if (sc.isUserInRole("APP_BBL") || sc.isUserInRole("APP_OL") || sc.isUserInRole("APP_DL") || sc.isUserInRole("APP_TL"))
            regionTaskflowMap.put("showColumns", "Budget");
        else
            regionTaskflowMap.put("showColumns", "ProjectDetails");
        if((Boolean)pageFlowAttrs.get("isNew") || (Boolean)pageFlowAttrs.get("isEdit")) {
            taskFlowId="/WEB-INF/tflows/activity/edit-activity-tf.xml#edit-activity-tf";
            regionTaskflowMap.put("isNew", pageFlowAttrs.get("isNew"));
            regionTaskflowMap.put("trainStop",pageFlowAttrs.get("trainStop"));
            pageFlowAttrs.put("defaultView","editActivity");  
        }
        else
        {
         taskFlowId="/WEB-INF/tflows/activity/activity-details-flow.xml#activity-details-flow";
         pageFlowAttrs.put("defaultView","viewActivity");   
        }
    }
    public TaskFlowId getDynamicTaskFlowId() {

        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }
    public String viewActivityDetails() {
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            setDynamicTaskFlowId("/WEB-INF/tflows/activity/activity-details-flow.xml#activity-details-flow");
            regionTaskflowMap.clear();
            SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
            if (sc.isUserInRole("APP_FM") || sc.isUserInRole("APP_FINANCE"))
                regionTaskflowMap.put("showColumns", "Funds");
            else if (sc.isUserInRole("APP_BBL") ||sc.isUserInRole("APP_OL") || sc.isUserInRole("APP_DL") || sc.isUserInRole("APP_TL"))
                regionTaskflowMap.put("showColumns", "Budget");
            else
                regionTaskflowMap.put("showColumns", "ProjectDetails");
            regionTaskflowMap.put("budget_year", pageFlowAttrs.get("budget_year"));
            regionTaskflowMap.put("project_number", pageFlowAttrs.get("project_number"));
            regionTaskflowMap.put("tabContext",pageFlowAttrs.get("tabContext"));
            regionTaskflowMap.put("showView", "tabs");
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }

        return null;
    }
    public String editActivity() {
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            setDynamicTaskFlowId("/WEB-INF/tflows/activity/edit-activity-tf.xml#edit-activity-tf");
            regionTaskflowMap.clear();
            regionTaskflowMap.put("budget_year", pageFlowAttrs.get("budget_year"));
            regionTaskflowMap.put("project_number", pageFlowAttrs.get("project_number"));
            regionTaskflowMap.put("isNew", pageFlowAttrs.get("isNew"));
            regionTaskflowMap.put("tabContext",pageFlowAttrs.get("tabContext"));
            SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
            if (sc.isUserInRole("APP_FM") || sc.isUserInRole("APP_FINANCE"))
                regionTaskflowMap.put("trainStop", "Funds");
            else if (sc.isUserInRole("APP_BBL") || sc.isUserInRole("APP_OL") || sc.isUserInRole("APP_DL") || sc.isUserInRole("APP_TL"))
                regionTaskflowMap.put("trainStop", "Budget");
            else
                regionTaskflowMap.put("trainStop", "Basic");
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }

        return null;
    }
    private boolean checkForDirtyPage() {        
        Boolean dirtyFlg = false;
        if (this.taskFlowId.equals("/WEB-INF/tflows/activity/edit-activity-tf.xml#edit-activity-tf")) {
            dirtyFlg =
                this.getApplicationModule("#{data.PAabacusappModuleDataControl.dataProvider}").getTransaction().isDirty();
            if(dirtyFlg){
                AdfFacesContext.getCurrentInstance().getPageFlowScope().put("defaultView","editActivity");              
            }
            
        }
        return dirtyFlg;
    }
    private ApplicationModule getApplicationModule(String dataProvider) {
        FacesContext fc = FacesContext.getCurrentInstance();
        Application app = fc.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = fc.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, dataProvider, Object.class);
        return (ApplicationModule) valueExp.getValue(elContext);
    }

    private static Object evaluateEL(String el) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ELContext elContext = facesContext.getELContext();
        ExpressionFactory expressionFactory = facesContext.getApplication().getExpressionFactory();
        ValueExpression exp = expressionFactory.createValueExpression(elContext, el, Object.class);
        return exp.getValue(elContext);
    }
    
    public long getCurrentYear(){
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("getCurrentFY");
        exec.execute();
        Integer currentYear = (Integer) exec.getResult();
        return currentYear;
    }

    public void addBudgetReturnLsnr(ReturnEvent returnEvent) {
        Map pfp=AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) pfp.get("confirmMessage");
        messageBean = new MessageBean();
        messageBean.sendActivityBudgetTransferEmail(result, (Map) pfp.get("transferDetails"),(Boolean) pfp.get("isSendEmail"));
    }

    public void addFundsReturnLnsr(ReturnEvent returnEvent) {
        Map pfp=AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) pfp.get("confirmMessage");
        messageBean = new MessageBean();
        messageBean.sendActivityFundTransferEmail(result, (Map) pfp.get("transferDetails"),(Boolean) pfp.get("isSendEmail"));
    }
    /**
     * this method will be triggered when budget recovery popup closed, 
     * based on the popup return type method will send an associated email to concerned users.
     * @param returnEvent
     */
    public void budgetRecoveryReturnListner(ReturnEvent returnEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) map.get("confirmMessage");
        messageBean = new MessageBean();
        messageBean.sendBudgetRecoveryEmail(result, (Map)map.get("transferDetails"), (Boolean)map.get("isSendEmail"));
    }
    
    /**
     * this method will be triggered when fund recovery popup closed, 
     * based on the popup return type method will send an associated email to concerned users.
     * @param returnEvent
     */
    public void fundRecoveryPopupReturnListner(ReturnEvent returnEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) map.get(AbacusConstants.CONFIRM_MESSAGE);
        messageBean = new MessageBean();
        messageBean.sendFundRecoveryEmail(result, (Map)map.get(AbacusConstants.TRANSFER_DETAILS), (Boolean)map.get(AbacusConstants.SEND_EMAIL));
        
    }
}
