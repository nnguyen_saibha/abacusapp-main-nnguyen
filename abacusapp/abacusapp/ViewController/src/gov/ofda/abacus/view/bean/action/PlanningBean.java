package gov.ofda.abacus.view.bean.action;

import gov.ofda.abacus.view.base.JSFUtils;
import gov.ofda.abacus.view.base.UIControl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.util.ComponentReference;

public class PlanningBean  extends UIControl implements Serializable {
    @SuppressWarnings("compatibility:3768304632569362964")
    private static final long serialVersionUID = 1L;

    public PlanningBean() {
        super();
    }
    List<String> manageTagList = new ArrayList<String>();
    private ComponentReference tagListAddRemoveCol;
    private ComponentReference tagListSelectedListTable;
    private ComponentReference tagLookupTable;

    public void setTagListSelectedListTable(UIComponent tagListSelectedListTable) {
        this.tagListSelectedListTable = ComponentReference.newUIComponentReference(tagListSelectedListTable);
    }

    public UIComponent getTagListSelectedListTable() {
        return tagListSelectedListTable == null ? null : tagListSelectedListTable.getComponent();
    }


    public String addTagToList(ActionEvent ae) {
        String tagId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("TagId");
        if (!manageTagList.contains(tagId)) {
            manageTagList.add(tagId);
        } else {
            manageTagList.remove(tagId);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(tagListAddRemoveCol.getComponent());
        AdfFacesContext.getCurrentInstance().addPartialTarget(tagListSelectedListTable.getComponent());
        return null;
    }


    public void setTagListAddRemoveCol(UIComponent tagListAddRemoveCol) {
        this.tagListAddRemoveCol = ComponentReference.newUIComponentReference(tagListAddRemoveCol);
    }

    public UIComponent getTagListAddRemoveCol() {
        return tagListAddRemoveCol == null ? null : tagListAddRemoveCol.getComponent();
    }


    public void setManageTagList(List<String> manageTagList) {
        this.manageTagList = manageTagList;
    }

    public List<String> getManageTagList() {
        return manageTagList;
    }

    /**
     * This method handles the dialog listener for Restricted Goods.
     * 
     * @param dialogEvent
     */
    public void manageTagDialogLsnr(DialogEvent dialogEvent) {
        // Add event code here...
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            //Get current row key to keep track of currrent row.
            DCIteratorBinding iter = bindings.findIteratorBinding("PaTagView1Iterator");
            Key key = null;
            if (iter != null && iter.getCurrentRow() != null)
                key = iter.getCurrentRow().getKey();

            OperationBinding ctrl = bindings.getOperationBinding("updateTags");
            ctrl.getParamsMap().put("tagList", this.getManageTagList());
            Boolean t = (Boolean) ctrl.execute();
            if (this.getManageTagList() != null) {
                OperationBinding exec = bindings.getOperationBinding("PaTagView1Execute");
                exec.execute();
                if (key != null) {
                    iter =  bindings.findIteratorBinding("PaTagView1Iterator");
                    RowSetIterator rsi = iter.getRowSetIterator();
                    if (rsi.findByKey(key, 1).length > 0)
                        iter.setCurrentRowWithKey(key.toStringFormat(true));
                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(tagLookupTable.getComponent().getParent());
            }
        }
    }

    /**
     * @param popupFetchEvent
     * Called when Manage Location popup is fetched.
     * It will get the existing location list and add the selected location list.
     */
    public void manageTagFetchLsnr(PopupFetchEvent popupFetchEvent) {
        // Add event code here...getLocationsList
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding getList = bindings.getOperationBinding("getTagList");
        manageTagList = (List<String>) getList.execute();                                                                                       
        AdfFacesContext.getCurrentInstance().addPartialTarget(tagLookupTable.getComponent());
        AdfFacesContext.getCurrentInstance().addPartialTarget(tagListSelectedListTable.getComponent());
    }


    /**
     * @param popupCanceledEvent
     * Called when popup is cancelled or closed.
     * It will clear the filter for select location table
     */
    public void manageTagCancelLsnr(PopupCanceledEvent popupCanceledEvent) {
        clearFilter((RichTable) tagLookupTable.getComponent());
    }

    public void setTagLookupTable(UIComponent tagLookupTable) {
        this.tagLookupTable = ComponentReference.newUIComponentReference(tagLookupTable);
        ;
    }

    public UIComponent getTagLookupTable() {
        return tagLookupTable == null ? null : tagLookupTable.getComponent();
    }
    public void costSheetValueChangeLsnr(ValueChangeEvent valueChangeEvent) {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfm.put("isCostsheetApplicable",valueChangeEvent.getNewValue());        
           
        if("N".equals(valueChangeEvent.getNewValue())) { 
            
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
            Row r = iter.getCurrentRow();
            if (r != null) {                
                   r.setAttribute("ContributionLinktypeId", null);
                   r.setAttribute("PaxActivitiesId", null);
            } 
      
        }
    }

}
