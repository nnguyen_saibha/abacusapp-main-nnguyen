package gov.ofda.abacus.view.bean.award;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class AwardCloseoutBean {
    public AwardCloseoutBean() {
    }

    public void saveCloseoutDetails() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Commit");
        
        if(exec.isOperationEnabled()){
            exec.execute();
            exec = bindings.getOperationBinding("Execute");
            exec.execute();
            if(exec.getErrors().isEmpty()){
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }else{
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "No changes to save", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void cancelCloseoutDetails() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Rollback");
        
      exec.execute();
            if(exec.getErrors().isEmpty()){
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancelled successfully", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
    
}

    public void setParamsforClsoutDocuments(PopupFetchEvent popupFetchEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.execute();
    }
}
