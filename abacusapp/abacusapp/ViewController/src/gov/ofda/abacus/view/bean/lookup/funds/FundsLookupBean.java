package gov.ofda.abacus.view.bean.lookup.funds;

import gov.ofda.abacus.view.base.JSFUtils;

import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.controller.ControllerContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.QueryEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;

import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;

public class FundsLookupBean {
    private RichPopup addNewRowPopup;

    public void deleteFundsLookupDialogListner(DialogEvent dialogEvent) {
        if( DialogEvent.Outcome.yes == dialogEvent.getOutcome()){
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            
            DCIteratorBinding iter = bindings.findIteratorBinding("FundLookupView1Iterator");
            Row currentRow = iter.getCurrentRow();
            
            Integer fiscalYear = (Integer) currentRow.getAttribute("BudgetFy");
            String fundCode = (String) currentRow.getAttribute("FundCode");
            
            OperationBinding execDelete = bindings.getOperationBinding("Delete");
            execDelete.execute();
            
            OperationBinding execCommit = bindings.getOperationBinding("Commit");
            execCommit.execute();
            if(!execCommit.getErrors().isEmpty()){
                OperationBinding execRollback = bindings.getOperationBinding("Rollback");
                execRollback.execute();
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Record cannot be deleted as child record exists!", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
                ((RichPopup)dialogEvent.getComponent().getParent()).cancel();
            }else{
                OperationBinding exec = bindings.getOperationBinding("Execute");
                exec.execute();
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, fiscalYear + ", " + fundCode +" has been deleted successfully." , "");
                FacesContext.getCurrentInstance().addMessage(null, message); 
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(dialogEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
    }

    public String showAddRowPopup() {
        // Add event code here...
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pageFlowMap.put("addReconciliationRecord", true);
        pageFlowMap.put("addNew", true);
        RichPopup popup = (RichPopup) this.getAddNewRowPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
        return "next";
    }

    public void setAddNewRowPopup(RichPopup addNewRowPopup) {
        this.addNewRowPopup = addNewRowPopup;
    }

    public RichPopup getAddNewRowPopup() {
        return addNewRowPopup;
    }

    public void addNewFundCodePopupListener(DialogEvent dialogEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        if(DialogEvent.Outcome.ok == dialogEvent.getOutcome()){
            OperationBinding exec = bindings.getOperationBinding("Commit");
            if(exec.isOperationEnabled()){
                exec.execute(); 
                if(exec.getErrors().isEmpty()){
                    FacesMessage message =
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully", "");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                }
                
                DCIteratorBinding fLookupIter = bindings.findIteratorBinding("FundLookupView1Iterator");
                Key currentRowKey = fLookupIter.getCurrentRow().getKey();
                
                
                //pageFlowScope.addReconciliationRecord
                Boolean reconciliationFlag =
                    (Boolean) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("addReconciliationRecord");
                if(null != reconciliationFlag && reconciliationFlag){
                    DCIteratorBinding iter = bindings.findIteratorBinding("FundLookupView1Iterator");
                    Row currentRow = iter.getCurrentRow();
                    Integer budgetFy = (Integer) currentRow.getAttribute("BudgetFy");
                    String fundCode = (String) currentRow.getAttribute("FundCode");
                    
                    OperationBinding exec1 = bindings.getOperationBinding("InsertIntoPhoenixFunds");
                    exec1.getParamsMap().put("BudgetFy", budgetFy);
                    exec1.getParamsMap().put("FundCode", fundCode);
                    exec1.execute();
                    exec.execute();
                }
                
                OperationBinding execExecute = bindings.getOperationBinding("Execute");
                execExecute.execute();
                
                if(null != fLookupIter.findRowByKeyString(currentRowKey.toStringFormat(true))){
                    fLookupIter.setCurrentRowWithKey(currentRowKey.toStringFormat(true));
                }
            }
        }else{
            NavigationHandler  nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            if(nvHndlr!=null)
                nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "cancel");
        }
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(dialogEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
    }

    public void addOrEditFundCodePopupCancelListener(PopupCanceledEvent popupCanceledEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Commit");
        if(exec.isOperationEnabled()){
            OperationBinding execRollback = bindings.getOperationBinding("Rollback");
            execRollback.execute();
            
            OperationBinding execExecute = bindings.getOperationBinding("Execute");
            execExecute.execute();

            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancelled successfully", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(popupCanceledEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
    }

    public String showCancelMessage() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Commit");
        if(exec.isOperationEnabled()){
            OperationBinding execRollback = bindings.getOperationBinding("Rollback");
            execRollback.execute();
            
            OperationBinding execExecute = bindings.getOperationBinding("Execute");
            execExecute.execute();

            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancelled successfully", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return "next";
    }

    public String editFundsLookup() {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pageFlowMap.put("addReconciliationRecord", false);
        pageFlowMap.put("addNew", false);
        RichPopup popup = (RichPopup) this.getAddNewRowPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
        return "next";
    }
}
