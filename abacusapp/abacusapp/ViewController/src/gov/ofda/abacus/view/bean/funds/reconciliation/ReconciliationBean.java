package gov.ofda.abacus.view.bean.funds.reconciliation;

import gov.ofda.abacus.view.base.JSFUtils;
import gov.ofda.abacus.view.base.UIControl;
import gov.ofda.abacus.view.bean.AppBean;
import gov.ofda.abacus.view.bean.action.ProposalReceivedBean;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.adf.view.rich.event.QueryEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.util.ComponentReference;

public class ReconciliationBean extends UIControl implements Serializable {
    
    @SuppressWarnings("compatibility:4487405085275575094")
    private static final long serialVersionUID = 1L;

    List<String> manageFundList = new ArrayList<String>();
    private ComponentReference fundListAddRemoveCol;
    private ComponentReference fundListSelectedListTable;
    private ComponentReference fundLookupTable;
    private ComponentReference managefundsPopup;

    public void setManagefundsPopup(UIComponent managefundsPopup) {
        this.managefundsPopup = ComponentReference.newUIComponentReference(managefundsPopup);
    }

    public UIComponent getManagefundsPopup() {
        return managefundsPopup == null ? null : managefundsPopup.getComponent();
    }

    public void setFundListSelectedListTable(UIComponent fundListSelectedListTable) {
        this.fundListSelectedListTable = ComponentReference.newUIComponentReference(fundListSelectedListTable);
    }

    public UIComponent getFundListSelectedListTable() {
        return fundListSelectedListTable == null ? null : fundListSelectedListTable.getComponent();
    }

    public ReconciliationBean() {
        super();
    }
   public String addFundToList(ActionEvent ae) {
        String fundCode = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("FundCode");
        if (!manageFundList.contains(fundCode)) {
            manageFundList.add(fundCode);
        } else {
            manageFundList.remove(fundCode);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(fundListAddRemoveCol.getComponent());
        AdfFacesContext.getCurrentInstance().addPartialTarget(fundListSelectedListTable.getComponent());
        return null;
    }


    public void setFundListAddRemoveCol(UIComponent fundListAddRemoveCol) {
        this.fundListAddRemoveCol = ComponentReference.newUIComponentReference(fundListAddRemoveCol);
    }

    public UIComponent getFundListAddRemoveCol() {
        return fundListAddRemoveCol == null ? null : fundListAddRemoveCol.getComponent();
    }


    public void setManageFundList(List<String> manageFundList) {
        this.manageFundList = manageFundList;
    }

    public List<String> getManageFundList() {
        return manageFundList;
    }

    /**
     * This method handles the listener to display unsaved changes msg and show Add/Delete Popup.
     * 
     * @param dialogEvent
     */
    public void addOrDeleteFunds(ActionEvent actionEvent) {
        Boolean isCommitEnabled=(Boolean)JSFUtils.resolveExpression("#{bindings.Commit.enabled}");
        if(isCommitEnabled) {
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_WARN, "Unsaved changes. Please save changes first and try again!", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        else{
        RichPopup popup = (RichPopup) this.getManagefundsPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
        }
    }
    /**
     * This method handles the dialog listener for Add Fund Code.
     * 
     * @param dialogEvent
     */
    public void manageFundsDialogLsnr(DialogEvent dialogEvent) {
        // Add event code here...
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            //Get current row key to keep track of currrent row.
            DCIteratorBinding iter = bindings.findIteratorBinding("FundsReconciliationEditView1Iterator");
            Key key = null;
            if (iter != null && iter.getCurrentRow() != null)
                key = iter.getCurrentRow().getKey();

            OperationBinding ctrl = bindings.getOperationBinding("updateFunds");
            ctrl.getParamsMap().put("fundList", this.getManageFundList());
            Boolean t = (Boolean) ctrl.execute();
            OperationBinding exec = bindings.getOperationBinding("Commit");
            exec.execute();
            if (this.getManageFundList() != null) {
            OperationBinding exec1 = bindings.getOperationBinding("FundsReconciliationEditView1Execute");
                exec1.execute();
                
                if (key != null) {
                    iter =  bindings.findIteratorBinding("FundsReconciliationEditView1Iterator");
                    RowSetIterator rsi = iter.getRowSetIterator();
                    if (rsi.findByKey(key, 1).length > 0)
                        iter.setCurrentRowWithKey(key.toStringFormat(true));
                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(fundLookupTable.getComponent().getParent());
            }
        }
    }

    /**
     * @param popupFetchEvent
     * Called when Manage Fund popup is fetched.
     * It will get the existing location list and add the selected location list.
     */
   public void manageFundFetchLsnr(PopupFetchEvent popupFetchEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding setFY=bindings.getOperationBinding("setFYViewsCriteria");
        setFY.execute();
        OperationBinding getList = bindings.getOperationBinding("getFundList");
        manageFundList = (List<String>) getList.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(fundLookupTable.getComponent());
        AdfFacesContext.getCurrentInstance().addPartialTarget(fundListSelectedListTable.getComponent());
    }
    
    public void saveChanges() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        Boolean isCommitEnabled = (Boolean) JSFUtils.resolveExpression("#{bindings.Commit.enabled}");
        if (!isCommitEnabled) {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "No changes","No changes to save");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;
        }
        exec = bindings.getOperationBinding("Commit");
        exec.execute();
        if (exec.getErrors().isEmpty()) {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully","Saved successfully");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
   


    /**
     * @param popupCanceledEvent
     * Called when popup is cancelled or closed.
     * It will clear the filter for select location table
     */
    public void manageFundCancelLsnr(PopupCanceledEvent popupCanceledEvent) {
        //clearFilter((RichTable) fundLookupTable.getComponent());
    }

    public void setFundLookupTable(UIComponent fundLookupTable) {
        this.fundLookupTable = ComponentReference.newUIComponentReference(fundLookupTable);
    }

    public UIComponent getFundLookupTable() {
        return fundLookupTable == null ? null : fundLookupTable.getComponent();
    }

    public void cancelChanges() {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding Iter = (DCIteratorBinding)bindings.get("FundsReconciliationEditView1Iterator");
            Key key = Iter.getCurrentRow().getKey();
            OperationBinding exec = bindings.getOperationBinding("Rollback");
            exec.execute();
            if (exec.getErrors().isEmpty())
            {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancelled successfully","Cancelled successfully");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        Iter.setCurrentRowWithKey(key.toStringFormat(true));
    }
    
    public void searchQueryListner(QueryEvent queryEvent) {
        // Add event code here... #{bindings.fundsLookupSearchCriteriaQuery.processQuery}
        
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        //OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        SecurityContext scntx = ADFContext.getCurrent().getSecurityContext();
        Boolean isCommitEnabled=(Boolean)JSFUtils.resolveExpression("#{bindings.Commit.enabled}");
        if(isCommitEnabled) {
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_WARN, "Unsaved changes. Please save changes first and try again!", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }else{
            FacesContext fctx = FacesContext.getCurrentInstance();
            Application application = fctx.getApplication();
            ExpressionFactory expressionFactory = application.getExpressionFactory();
            ELContext elctx = fctx.getELContext();

            MethodExpression methodExpression =
                expressionFactory.createMethodExpression(elctx, "#{bindings.FundsReconciliationEditViewCriteriaQuery.processQuery}",
                                                         Object.class, new Class[] { QueryEvent.class });
            methodExpression.invoke(elctx, new Object[] { queryEvent });
        }
    }   
}
