package gov.ofda.abacus.view.bean.action;

import gov.ofda.abacus.view.base.JSFUtils;
import gov.ofda.abacus.view.base.UIControl;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputListOfValues;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import oracle.sqlj.runtime.Oracle;

/**
 * Used to manage Sectors tab in Edit Action
 */
public class SectorBean extends UIControl {
    @SuppressWarnings("compatibility:-764917577636057893")
    private static final long serialVersionUID = 1L;

    public SectorBean() {
        super();
    }

    /**
     * @param vce 
     * Called when a Sector is changed. It will check if sector can be changed.
     * If it is not updatable. Show a generic message and reset the value back to old value
     */
    public void sectorChgLnsr(ValueChangeEvent vce) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("EditPaSectorView1Iterator");
        Row r = iter.getCurrentRow();
        if (r != null) {
            Boolean isUpdatable = (Boolean) r.getAttribute("IsUpdatable");
            if (!isUpdatable) {
                vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                ((RichInputListOfValues) vce.getComponent()).setValue(vce.getOldValue());
                vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                FacesContext.getCurrentInstance().addMessage(vce.getComponent().getId(),
                                                             new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                              "Sector cannot be changed. Sector has Subsectors, Keywords, Locations, Sector Overview, Modality and/or Commodity data associated with it."));
            }

        }
    }

    /**
     * @return Remaining Amount return html message with formatted and colored remaining amount.
     * Green if total sector amount < Approved Amt(Planned Amt)
     * Red   if total sector amount > Approved Amt(Planned Amt)
     * black if total sector amount = Approved Amt(Planned Amt)
     * Compares Approved Amount (or Planned Amount if approved is null) with total sector amount.
     * 
     */
    public String getRemainingAmount() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        Row r = iter.getCurrentRow();
        if (r != null) {
            BigDecimal approvedAmt = (BigDecimal) r.getAttribute("ApprovedAmt");
            BigDecimal plannedAmt = (BigDecimal) r.getAttribute("PlannedAmt");
            BigDecimal totalSectorAmt =new BigDecimal(0);
            oracle.jbo.domain.Number tmp=(oracle.jbo.domain.Number) JSFUtils.resolveExpression("#{bindings.EditPaSectorView1Iterator.viewObject.sum['SectorAmt']}");
            if(tmp!=null)    
                totalSectorAmt =tmp.bigDecimalValue();
            BigDecimal balanceAmt = null;
            if (approvedAmt != null)
                balanceAmt = approvedAmt.subtract(totalSectorAmt);
            else if (plannedAmt != null)
                balanceAmt = plannedAmt.subtract(totalSectorAmt);
            if (balanceAmt != null) {
                if (balanceAmt.compareTo(BigDecimal.ZERO) > 0)
                    return ("<span style=\"color:green\">" + currencyFormat(balanceAmt) + "</span>");
                else if (balanceAmt.compareTo(BigDecimal.ZERO) < 0)
                    return ("<span style=\"color:#800000\">" + currencyFormat(balanceAmt) + "</span>");
                else
                    return ("<span>" + currencyFormat(balanceAmt) + "</span>");
            }


        }
        return null;
    }

    /**
     * @param de
     * Depending on dialog event, if user selected Yes, then this will delete current (selected) sector
     */
    public void deleteDialogLsnr(DialogEvent de) {
        if (DialogEvent.Outcome.yes == de.getOutcome()) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = null;
            exec = bindings.getOperationBinding("DeleteSector");
            exec.execute();
        }
    }
}
