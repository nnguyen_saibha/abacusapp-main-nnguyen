package gov.ofda.abacus.view.bean.action;

import gov.ofda.abacus.view.base.UIControl;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputListOfValues;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.LaunchPopupEvent;

import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.util.ComponentReference;

public class CreateActionBean extends UIControl {
    @SuppressWarnings("compatibility:4369009505727904804")
    private static final long serialVersionUID = 1L;
    private  ComponentReference modRefInputLOV; //RichInputListOfValues
    private  ComponentReference parentRefInputLOV; //RichInputListOfValues
    private  ComponentReference projectInputLOV; //RichInputListOfValues
    private  ComponentReference awardeeInputLOV; //RichInputListOfValues

    public CreateActionBean() {
        super();
    }

    public String initialize() {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pageFlowScope.put("fundingActionType", "01");
        if (pageFlowScope.get("actionTypeCode") == null)
            pageFlowScope.put("actionTypeCode", "N");
        pageFlowScope.put("categoryTypeCode", "R");
        pageFlowScope.put("detailFundingActionType", "01");
        pageFlowScope.put("actionID", null);
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        if (pageFlowScope.get("parentActionID") != null) {
            exec = bindings.getOperationBinding("FindActionIDExecuteWithParams");
            exec.getParamsMap().put("p_aid", pageFlowScope.get("parentActionID"));
            exec.execute();
            DCIteratorBinding iter = bindings.findIteratorBinding("PATableFindByActionIDIterator");
            RowSetIterator rsi = iter.getRowSetIterator();
            Row r = rsi.getCurrentRow();
            DCIteratorBinding parentIter = null;
            if (r != null) {
                exec = bindings.getOperationBinding("getCurrentFY");
                exec.execute();
                Integer currentFY = (Integer) exec.getResult();
                String cf = (String) r.getAttribute("CommitFlag");
                String bf = (String) r.getAttribute("BudgetFlag");
                String fat = (String) r.getAttribute("FundingActionType");
                pageFlowScope.put("fundingActionType", fat);
                if ("Y".equals(cf) && "Y".equals(bf)) { //Regular
                    exec = bindings.getOperationBinding("SetRegularExecuteWithParams");
                    exec.getParamsMap().put("p_fat", fat);
                    exec.getParamsMap().put("p_id", pageFlowScope.get("parentActionID"));
                    exec.execute();
                    parentIter = bindings.findIteratorBinding("PARegularParentRefView1Iterator");
                    pageFlowScope.put("categoryTypeCode", "R");
                } else if ("N".equals(cf) && "N".equals(bf)) { //Macro
                    exec = bindings.getOperationBinding("SetMacroExecuteWithParams");
                    exec.getParamsMap().put("p_fat", fat);
                    exec.getParamsMap().put("p_id", pageFlowScope.get("parentActionID"));
                    exec.getParamsMap().put("p_fy", currentFY);
                    exec.execute();
                    parentIter = bindings.findIteratorBinding("PAMarcoParentRefView1Iterator");
                    pageFlowScope.put("categoryTypeCode", "M");
                } else if ("Y".equals(cf) && "N".equals(bf)) { //Incremental
                    exec = bindings.getOperationBinding("SetIncrementalExecuteWithParams");
                    exec.getParamsMap().put("p_fat", fat);
                    exec.getParamsMap().put("p_id", pageFlowScope.get("parentActionID"));
                    exec.getParamsMap().put("p_fy", currentFY);
                    exec.execute();
                    parentIter = bindings.findIteratorBinding("PAIncrementalParentRefView1Iterator");
                    pageFlowScope.put("categoryTypeCode", "I");
                }
                RowSetIterator parentRSIter = parentIter.getRowSetIterator();
                Row rParent = parentRSIter.getCurrentRow();
                if (rParent != null) { //Check passed parent is a valid one.
                    pageFlowScope.put("pOfdaDivisionCode", rParent.getAttribute("OfdaDivisionCode"));
                    pageFlowScope.put("pBudgetFy", rParent.getAttribute("BudgetFy"));
                    pageFlowScope.put("pProjectNbr", rParent.getAttribute("ProjectNbr1"));
                    pageFlowScope.put("pActionSeqNbr", rParent.getAttribute("ActionSeqNbr"));
                    pageFlowScope.put("pProjectName", rParent.getAttribute("ProjectName"));
                    pageFlowScope.put("pAwardee", rParent.getAttribute("Awardee"));
                    pageFlowScope.put("pAwardNbr", rParent.getAttribute("AwardNbr"));
                    pageFlowScope.put("pActionAmt", currencyFormat((BigDecimal) rParent.getAttribute("ActionAmt")));
                    pageFlowScope.put("isParentRefReadOnly", true);
                } else {
                    pageFlowScope.put("errorMessage", "Details cannot be created for selected Action");
                    return "error";
                }
                if (!("D".equals(pageFlowScope.get("actionTypeCode")) ||
                      "Z".equals(pageFlowScope.get("actionTypeCode"))))
                    pageFlowScope.put("actionTypeCode", "D");
                pageFlowScope.put("isReadOnly", true);

            } else {
                pageFlowScope.put("errorMessage",
                                  "System cannot find Parent Action. Action ID: " +
                                  pageFlowScope.get("parentActionID"));
                return "error";
            }
        }
        if (pageFlowScope.get("modActionID") != null) {
            exec = bindings.getOperationBinding("FindActionIDExecuteWithParams");
            exec.getParamsMap().put("p_aid", pageFlowScope.get("modActionID"));
            exec.execute();
            DCIteratorBinding iter = bindings.findIteratorBinding("PATableFindByActionIDIterator");
            RowSetIterator rsi = iter.getRowSetIterator();
            Row r = rsi.getCurrentRow();
            if (r != null) {
                if ((pageFlowScope.get("parentActionID") == null ||
                     (pageFlowScope.get("fundingActionType").toString()).equals(r.getAttribute("FundingActionType").toString()))) //Check for Mod Detail. Valid only when both parent and original action are same FAT)
                {
                    String fat = (String) r.getAttribute("FundingActionType");
                    pageFlowScope.put("fundingActionType", fat);
                    exec = bindings.getOperationBinding("ModRefExecuteWithParams");
                    exec.getParamsMap().put("p_fat", fat);
                    exec.getParamsMap().put("p_aid", pageFlowScope.get("modActionID"));
                    exec.execute();
                    DCIteratorBinding modIter = bindings.findIteratorBinding("PAModRefView1Iterator");
                    RowSetIterator modRSIter = modIter.getRowSetIterator();
                    Row rMod = modRSIter.getCurrentRow();
                    if (rMod != null) {
                        pageFlowScope.put("mBudgetFy", rMod.getAttribute("BudgetFy"));
                        pageFlowScope.put("mActionSeqNbr", rMod.getAttribute("ActionSeqNbr"));
                        pageFlowScope.put("mProjectName", rMod.getAttribute("ProjectName"));
                        pageFlowScope.put("awardee", rMod.getAttribute("Awardee"));
                        pageFlowScope.put("awardeeCode", rMod.getAttribute("AwardeeCode1"));
                        pageFlowScope.put("mAwardNbr", rMod.getAttribute("AwardNbr"));
                        pageFlowScope.put("mActionAmt", currencyFormat((BigDecimal) rMod.getAttribute("ActionAmt")));
                        if (!("M".equals(pageFlowScope.get("actionTypeCode")) ||
                              "U".equals(pageFlowScope.get("actionTypeCode")) ||
                              "Z".equals(pageFlowScope.get("actionTypeCode"))))
                            pageFlowScope.put("actionTypeCode", "M");
                        pageFlowScope.put("isReadOnly", true);
                        pageFlowScope.put("isModRefReadOnly", true);
                    } else {
                        pageFlowScope.put("errorMessage", "Mod cannot be created for selected Action");
                        return "error";
                    }
                } else {
                    pageFlowScope.put("errorMessage",
                                      "Detail Mod cannot be created as Funding Type for Parent Action and Original Action are not same.");
                    return "error";
                }
            } else {
                pageFlowScope.put("errorMessage",
                                  "System cannot find Original Action. Action ID: " + pageFlowScope.get("modActionID"));
                return "error";
            }
        }
        return "next";
    }

    public List<SelectItem> getActionTypeItems() {
        List<SelectItem> actionTypeItems = new ArrayList<SelectItem>();
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String fat = (String) pageFlowScope.get("fundingActionType");
        String actionType = (String) pageFlowScope.get("actionTypeCode");
        String categoryTypeCode = (String) pageFlowScope.get("categoryTypeCode");
        actionTypeItems.add(new SelectItem("N", "New", null));

        if (!("06".equals(fat) || "12".equals(fat) || "17".equals(fat))) {
            actionTypeItems.add(new SelectItem("M", "Mod", null));
            actionTypeItems.add(new SelectItem("U", "Unfunded Mod", null));
            actionTypeItems.add(new SelectItem("D", "Detail", null));
            actionTypeItems.add(new SelectItem("Z", "Mod Detail", null));
        } else {
            actionTypeItems.add(new SelectItem("D", "Detail", null));
            if(!("N".equals(actionType)||"D".equals(actionType)))
                setReferences(fat,"N",categoryTypeCode);
        }
        return actionTypeItems;
    }

    public List<SelectItem> getCategoryTypeItems() {
        List<SelectItem> cTypeItems = new ArrayList<SelectItem>();
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String fat = (String) pageFlowScope.get("fundingActionType");
        String categoryTypeCode = (String) pageFlowScope.get("categoryTypeCode");
        cTypeItems.add(new SelectItem("R", "Regular", null));
        if ("01".equals(fat) || "02".equals(fat) || "03".equals(fat) || "08".equals(fat) || "17".equals(fat)) {
            cTypeItems.add(new SelectItem("M", "Macro", null));
            //cTypeItems.add(new SelectItem("I","Incremental",null));

        } else {
            if ("M".equals(categoryTypeCode) || "I".equals(categoryTypeCode))
                pageFlowScope.put("categoryTypeCode", "R");
        }

        return cTypeItems;
    }

    public boolean isCategoryVisible() {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String fat = (String) pageFlowScope.get("fundingActionType");
        String aTypeCode = (String) pageFlowScope.get("actionTypeCode");
        if (("01".equals(fat) || "02".equals(fat) || "03".equals(fat) ||
             "08".equals(fat))) //&& ("N".equals(aTypeCode)||"M".equals(aTypeCode)||"U".equals(aTypeCode)))
            return true;
        return false;
    }

    public void fatChgLnsr(ValueChangeEvent vce) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String fat = (String) vce.getNewValue();
        String aTypeCode = (String) pageFlowScope.get("actionTypeCode");
        String cTypeCode = (String) pageFlowScope.get("categoryTypeCode");
        this.setReferences(fat, aTypeCode, cTypeCode);
    }

    public void actionTypeCodeChgLnsr(ValueChangeEvent vce) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String fat = (String) pageFlowScope.get("fundingActionType");
        String aTypeCode = (String) vce.getNewValue();
        String cTypeCode = (String) pageFlowScope.get("categoryTypeCode");
        this.setReferences(fat, aTypeCode, cTypeCode);
    }

    public void catChgLnsr(ValueChangeEvent vce) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String fat = (String) pageFlowScope.get("fundingActionType");
        String aTypeCode = (String) pageFlowScope.get("actionTypeCode");
        String cTypeCode = (String) vce.getNewValue();
        this.setReferences(fat, aTypeCode, cTypeCode);
    }

    private void setReferences(String fat, String aTypeCode, String cTypeCode) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if (("06".equals(fat) || "12".equals(fat) || "17".equals(fat)) &&
            ("M".equals(aTypeCode) || "Z".equals(aTypeCode) || "U".equals(aTypeCode)))
            pageFlowScope.put("actionTypeCode", "N");
        if ("17".equals(fat)) {
            pageFlowScope.put("categoryTypeCode", "M");
        } else if (!("01".equals(fat) || "02".equals(fat) || "03".equals(fat) || "08".equals(fat)) &&
                   ("M".equals(cTypeCode) || "I".equals(cTypeCode)))
            pageFlowScope.put("categoryTypeCode", "R");

        pageFlowScope.put("parentActionID", null);
        pageFlowScope.put("pOfdaDivisionCode", null);
        pageFlowScope.put("pBudgetFy", null);
        pageFlowScope.put("pProjectNbr", null);
        pageFlowScope.put("pActionSeqNbr", null);
        pageFlowScope.put("pProjectName", null);
        pageFlowScope.put("pAwardee", null);
        pageFlowScope.put("pAwardNbr", null);
        pageFlowScope.put("pActionAmt", null);
        if (this.getParentRefInputLOV() != null)
            ((RichInputListOfValues)this.getParentRefInputLOV()).setValue(null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getParentRefInputLOV());

        pageFlowScope.put("modActionID", null);
        pageFlowScope.put("mBudgetFy", null);
        pageFlowScope.put("mActionSeqNbr", null);
        pageFlowScope.put("mProjectName", null);
        pageFlowScope.put("mAwardNbr", null);
        pageFlowScope.put("mActionAmt", null);
        if (this.getModRefInputLOV() != null)
            ((RichInputListOfValues)this.getModRefInputLOV()).setValue(null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getModRefInputLOV());
        
        pageFlowScope.put("awardee", null);
        pageFlowScope.put("awardeeCode", null);
        if (this.getAwardeeInputLOV() != null)
            ((RichInputListOfValues)this.getAwardeeInputLOV()).setValue(null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAwardeeInputLOV());
        
    }

    public void createAction() {
        Map<String, Object> pfs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        Map<String, Object> t = new HashMap<String, Object>();
        StringBuffer sb = new StringBuffer();
        if (("D".equals(pfs.get("actionTypeCode")) || "Z".equals(pfs.get("actionTypeCode"))) &&
            pfs.get("parentActionID") != null) {
            if (!"M".equals(pfs.get("categoryTypeCode")) || !"17".equals(pfs.get("fundingActionType"))) {
                t.put("budgetFy", pfs.get("pBudgetFy"));
                t.put("ofdaDivisionCode", pfs.get("pOfdaDivisionCode"));
                t.put("projectNbr", pfs.get("pProjectNbr"));
            } else {
                t.put("budgetFy", pfs.get("budgetFy"));
                t.put("ofdaDivisionCode", pfs.get("ofdaDivisionCode"));
                t.put("projectNbr", pfs.get("projectNbr"));
            }
            t.put("parentActionID", pfs.get("parentActionID"));
        } else if (pfs.get("budgetFy") != null) {
            t.put("budgetFy", pfs.get("budgetFy"));
            t.put("ofdaDivisionCode", pfs.get("ofdaDivisionCode"));
            t.put("projectNbr", pfs.get("projectNbr"));
        }
        if ("D".equals(pfs.get("actionTypeCode")) &&
            ("06".equals(pfs.get("fundingActionType")) || "12".equals(pfs.get("fundingActionType")) ||
             "17".equals(pfs.get("fundingActionType"))))
            t.put("fundingActionType", pfs.get("detailFundingActionType"));
        else
            t.put("fundingActionType", pfs.get("fundingActionType"));

        t.put("actionTypeCode", pfs.get("actionTypeCode"));
        t.put("categoryTypeCode", pfs.get("categoryTypeCode"));

        if (pfs.get("modActionID") != null &&
            ("M".equals(pfs.get("actionTypeCode")) || "U".equals(pfs.get("actionTypeCode")) ||
             "Z".equals(pfs.get("actionTypeCode")))) {
            t.put("modActionID", pfs.get("modActionID"));
            t.put("awardNbr", pfs.get("mAwardNbr"));
        }
        if (pfs.get("awardeeCode") != null)
            t.put("awardeeCode", pfs.get("awardeeCode"));
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("createAction");
        exec.getParamsMap().put("list", t);
        exec.execute();
        Long actionID = (Long) exec.getResult();
        pfs.put("actionID", actionID);
        exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.getParamsMap().put("p_action_id", actionID);
        exec.execute();
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        Row r = iter.getCurrentRow();
        if (r != null) {
            Long asLong = (Long) r.getAttribute("ActionSeqNbr");
            String pnbr = (String) r.getAttribute("ProjectNbr");
            Integer budgetFy = (Integer) r.getAttribute("BudgetFy");
            sb.append(budgetFy.toString());
            sb.append("-");
            sb.append(pnbr);
            sb.append("-");
            sb.append(asLong.toString());
            pfs.put("id", sb.toString());
        }
    }

    public void parentRefLaunchPopupLnsr(LaunchPopupEvent launchPopupEvent) {
        Map<String, Object> pfs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        OperationBinding exec1 = bindings.getOperationBinding("getCurrentFY");
        exec1.execute();
        Integer currentFY = (Integer) exec1.getResult();
        if ("R".equals(pfs.get("categoryTypeCode")))
            exec = bindings.getOperationBinding("SetRegularExecuteWithParams");
        else if ("M".equals(pfs.get("categoryTypeCode")))
            exec = bindings.getOperationBinding("SetMacroExecuteWithParams");
        else
            exec = bindings.getOperationBinding("SetIncrementalExecuteWithParams");
        exec.getParamsMap().put("p_fat", pfs.get("fundingActionType"));
        exec.getParamsMap().put("p_id", null);
        exec.getParamsMap().put("p_fy", currentFY);
        exec.execute();
    }

    public void parentRefReturnPopupLnsr(ReturnPopupEvent rpe) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding parentIter = null;

        if ("R".equals(pageFlowScope.get("categoryTypeCode")))
            parentIter = bindings.findIteratorBinding("PAParentRefView1Iterator");
        else if ("M".equals(pageFlowScope.get("categoryTypeCode")))
            parentIter = bindings.findIteratorBinding("PAMarcoParentRefView1Iterator");
        else
            parentIter = bindings.findIteratorBinding("PAIncrementalParentRefView1Iterator");

        RowSetIterator parentRSIter = parentIter.getRowSetIterator();
        Row r = parentRSIter.getCurrentRow();
        if (r != null) {
            String parentId = (String) r.getAttribute("ParentActionId");
            if (parentId == null)
                parentId = r.getAttribute("ActionId").toString();
            pageFlowScope.put("parentActionID", parentId);
            String divCode = (String) r.getAttribute("OfdaDivisionCode");
            String budgetFy = (String) r.getAttribute("BudgetFy");
            String projectNbr = (String) r.getAttribute("ProjectNbr1");
            pageFlowScope.put("pOfdaDivisionCode", divCode);
            pageFlowScope.put("pBudgetFy", budgetFy);
            pageFlowScope.put("pProjectNbr", projectNbr);
            pageFlowScope.put("pActionSeqNbr", r.getAttribute("ActionSeqNbr"));
            pageFlowScope.put("pProjectName", r.getAttribute("ProjectName"));
            pageFlowScope.put("pAwardee", r.getAttribute("Awardee"));
            pageFlowScope.put("pAwardNbr", r.getAttribute("AwardNbr"));
            pageFlowScope.put("pActionAmt", currencyFormat((BigDecimal) r.getAttribute("ActionAmt")));
        }
    }

    public void modRefLaunchPopupLnsr(LaunchPopupEvent launchPopupEvent) {
        Map<String, Object> pfs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        exec = bindings.getOperationBinding("ModRefExecuteWithParams");
        exec.getParamsMap().put("p_fat", pfs.get("fundingActionType"));
        exec.getParamsMap().put("p_id", null);
        exec.execute();
    }

    public void modRefReturnPopupLnsr(ReturnPopupEvent returnPopupEvent) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding modIter = bindings.findIteratorBinding("PAModRefView1Iterator");
        RowSetIterator modRSIter = modIter.getRowSetIterator();
        Row r = modRSIter.getCurrentRow();
        if (r != null) {
            BigDecimal modId = (BigDecimal) r.getAttribute("ModActionId");
            if (modId == null)
                modId = (BigDecimal) r.getAttribute("ActionId");
            pageFlowScope.put("modActionID", modId);
            pageFlowScope.put("mBudgetFy", r.getAttribute("BudgetFy"));
            pageFlowScope.put("mActionSeqNbr", r.getAttribute("ActionSeqNbr"));
            pageFlowScope.put("mProjectName", r.getAttribute("ProjectName"));
            pageFlowScope.put("awardee", r.getAttribute("Awardee"));
            pageFlowScope.put("awardeeCode", r.getAttribute("AwardeeCode1"));
            pageFlowScope.put("mAwardNbr", r.getAttribute("AwardNbr"));
            pageFlowScope.put("mActionAmt", currencyFormat((BigDecimal) r.getAttribute("ActionAmt")));
        }

    }

    public void activityReturnPopupLsnr(ReturnPopupEvent returnPopupEvent) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding activityIter = bindings.findIteratorBinding("ActivityLOV1Iterator");
        RowSetIterator activityRSIter = activityIter.getRowSetIterator();
        Row r = activityRSIter.getCurrentRow();
        if (r != null) {
            String budgetFY = (String) r.getAttribute("BudgetFy");
            String projectNbr = (String) r.getAttribute("ProjectNbr");
            String divCode = (String) r.getAttribute("OfdaDivisionCode");
            String projectName = (String) r.getAttribute("ProjectName");
            pageFlowScope.put("budgetFy", budgetFY);
            pageFlowScope.put("projectNbr", projectNbr);
            pageFlowScope.put("projectName", projectName);
            pageFlowScope.put("ofdaDivisionCode", divCode);
            pageFlowScope.put("projectKey", r.getAttribute("ProjectKey"));
        }
    }

    public void awardeeReturnPopupLsnr(ReturnPopupEvent returnPopupEvent) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding activityIter = bindings.findIteratorBinding("AwardeeLOV1Iterator");
        RowSetIterator activityRSIter = activityIter.getRowSetIterator();
        Row r = activityRSIter.getCurrentRow();
        if (r != null) {
            String awardee = (String) r.getAttribute("AwardeeName");
            if (r.getAttribute("AwardeeAcronym") != null)
                awardee += " (" + r.getAttribute("AwardeeAcronym") + ")";
            pageFlowScope.put("awardee", awardee);
            pageFlowScope.put("awardeeCode", r.getAttribute("AwardeeCode"));
        }
    }

    public void setModRefInputLOV(UIComponent modRefInputLOV) {
        this.modRefInputLOV = ComponentReference.newUIComponentReference(modRefInputLOV);
    }

    public UIComponent getModRefInputLOV() {
        return modRefInputLOV==null?null:modRefInputLOV.getComponent();
    }

    public void setParentRefInputLOV(UIComponent parentRefInputLOV) {
        this.parentRefInputLOV = ComponentReference.newUIComponentReference(parentRefInputLOV);
    }

    public UIComponent getParentRefInputLOV() {
        return parentRefInputLOV==null?null:parentRefInputLOV.getComponent();
    }

    public void setProjectInputLOV(UIComponent projectInputLOV) {
        this.projectInputLOV = ComponentReference.newUIComponentReference(projectInputLOV);
    }

    public UIComponent getProjectInputLOV() {
        return projectInputLOV==null?null:projectInputLOV.getComponent();
    }

    public void setAwardeeInputLOV(UIComponent awardeeInputLOV) {
        this.awardeeInputLOV = ComponentReference.newUIComponentReference(awardeeInputLOV);
}

    public UIComponent getAwardeeInputLOV() {
        return awardeeInputLOV==null?null:awardeeInputLOV.getComponent();
    }
}
