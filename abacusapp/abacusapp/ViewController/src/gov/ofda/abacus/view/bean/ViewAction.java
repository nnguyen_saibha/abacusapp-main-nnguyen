package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.view.base.UIControl;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.binding.OperationBinding;

public class ViewAction extends UIControl {
    @SuppressWarnings("compatibility:4333476632565932734")
    private static final long serialVersionUID = 1L;

    public ViewAction() {
        super();
    }
    public void refreshPage(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("ExecuteAction");
        exec.execute();
    }
}
