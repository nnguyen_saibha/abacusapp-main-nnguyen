package gov.ofda.abacus.view.bean.user;


import gov.ofda.abacus.view.base.JSFUtils;
import gov.ofda.abacus.view.base.Shuttle;
import gov.ofda.abacus.view.base.UIControl;

import java.io.Serializable;

import java.util.List;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.output.RichOutputFormatted;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class EditUserBean  extends UIControl implements Serializable{
    @SuppressWarnings("compatibility:-7320006269124214877")
    private static final long serialVersionUID = 1L;
    private List selectedRoles;
    private List duplicateUserRoles;

    public String actionAndRoleCheck() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        SecurityContext scntx = ADFContext.getCurrent().getSecurityContext();
        String action = (String) pfm.get("action");
        if(scntx.isUserInRole("APP_ADMIN"))
        {
            if("ADD".equals(action)||"EDIT".equals(action)){//||"DUPLICATE".equals(action)||"MIGRATE".equals(action))  {
              return "next";
          }
          else {
              pfm.put("message","Unknown action ("+action+"). Valid values are ADD, EDIT");
              return "error";
          }
        } else if (scntx.isUserInRole("APP_USER")) {
                pfm.put("action", "EDIT");
                pfm.put("username",scntx.getUserName().toUpperCase());
                return "next";
        }
        else {
            pfm.put("message","Insufficient privileges.");
            return "error";
        }
    }

    public String checkUserExists() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        if(pfm.get("username")==null) {
            pfm.put("message", "Missing Username");
            return "error";
        }
        OperationBinding exec = bindings.getOperationBinding("checkUserExists");
        exec.execute();
        if(exec.getErrors().isEmpty())
        {
         Boolean result = (Boolean) exec.getResult();
         if(result)
            return "next";
         else
            pfm.put("message", "Unable to find user ("+pfm.get("username")+") in Abacus.");
        }
        else {
            pfm.put("message","Unexpected error checking user ("+pfm.get("username")+").");
        }
        return "error";
    }
    
    private void setDuplicateUserRoles(List duplicateUserRoles) {
        this.duplicateUserRoles = duplicateUserRoles;
    }

    public List getDuplicateUserRoles() {
        return duplicateUserRoles;
    }

    public EditUserBean() {
        super();
    }
    //Start Roles functions

    /**
     * Getter for selectedRoles. Used in Roles shuttle, this adds all the existing roles 
     * into selected list.
     * @return
     */
    public List getSelectedRoles() {
        this.selectedRoles = Shuttle.getSelected("EditAbacusUserDetailsView1Iterator", "AbacusRole");
        return this.selectedRoles;
    }

    /**
     * Setter for selectedRoles. Any changes made to shuttle values are set here.
     * @param selectedValues
     */
    public void setSelectedRoles(List selectedValues) {
        this.selectedRoles = selectedValues;
    }

    /**
     * Used by shuttle select items, This add all the available roles for the sector and already selected 
     * Roles incase they are made inactive in which case they will not be in View.
     * @return
     */
    public List getAllRoles() {
        return Shuttle.getAll("AbacusUserRoleLOV1Iterator", "EditAbacusUserDetailsView1Iterator", "AbacusRole",
                              "AbacusRole");
    }

    /**
     * Dialog listener for roles.
     * Here the roles are inserted and/or deleted based on the list selectedRoles.
     * @param dialogEvent
     */
    public void rolesMgrDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            Shuttle.setSelected(this.selectedRoles, "EditAbacusUserDetailsView1Iterator", "AbacusRole", "deleteRole",
                                "createRole");
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = bindings.getOperationBinding("Commit");
            exec.execute();
            if(exec.getErrors().isEmpty()) {
                syncRolesWithOIM("syncUserRoles");
            }
        }
    }
    //End Roles functions
public String syncRolesWithOIM() {
    syncRolesWithOIM("syncUserRoles");
    return null;
}
private void syncRolesWithOIM(String action) {
    DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
    OperationBinding operationBinding = bindings.getOperationBinding(action);
    String rm=(String)operationBinding.execute();
    if(rm!=null)
    {
        UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
        UIComponent component=viewRoot.findComponent("p2");
        RichPopup popup=(RichPopup)component;
        component = viewRoot.findComponent("ofinfo1");
        RichOutputFormatted rotext=(RichOutputFormatted)component;
        rotext.setValue(rm);
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
    }
}

    public String activateUser() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("EditAbacusUserView1Iterator");
        Row rw = iter.getCurrentRow();
        if (rw != null) {
            rw.setAttribute("Active", "Y");
            rw.setAttribute("IsConnect", "Y");
            OperationBinding exec = bindings.getOperationBinding("Commit");
            exec.execute();
            if(exec.getErrors().isEmpty()) {
                syncRolesWithOIM("syncUserRoles");
            }
        }
        return null;
    }

    public String deactivateUser() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("EditAbacusUserView1Iterator");
        Row rw = iter.getCurrentRow();
        if (rw != null) {
            rw.setAttribute("Active", "N");
            rw.setAttribute("IsConnect", "N");
            OperationBinding exec = bindings.getOperationBinding("Commit");
            exec.execute();
            if(exec.getErrors().isEmpty()) {
                syncRolesWithOIM("revokeAllUserRoles");
            }
        }
        return null;
    }

    public void duplicateUser() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        OperationBinding exec = bindings.getOperationBinding("CreateInsert");
        exec.execute();
        String action = (String) pfm.get("action");
        if(exec.getErrors().isEmpty())
        {
            if(pfm.get("username")!=null)
            {
            exec = bindings.getOperationBinding("duplicateUser");
            exec.execute();
            if(exec.getResult()!=null)
                this.setDuplicateUserRoles((List) exec.getResult());
                if("MIGRATE".equals(action))
                {
                 pfm.put("migrateUsername",true);
                 pfm.put("deactivateOldUsername",true);
                }
            }
            else {
                pfm.put("action","ADD");
            }
        }
    }

    public void saveChanges(ActionEvent actionEvent) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            OperationBinding exec = bindings.getOperationBinding("Commit");
            exec.execute();
            if (exec.getErrors().isEmpty()) {
                if("DUPLICATE".equals(pfm.get("action"))||"MIGRATE".equals(pfm.get("action")))
                {
                    String oldUsername = (String) pfm.get("username");
                    pfm.put("action", "EDIT");
                    pfm.put("username",null);
                    Shuttle.setSelected(this.getDuplicateUserRoles(), "EditAbacusUserDetailsView1Iterator", "AbacusRole", "deleteRole",
                                        "createRole");
                    exec = bindings.getOperationBinding("Commit");
                    exec.execute();
                    if(exec.getErrors().isEmpty()) {
                        syncRolesWithOIM("syncUserRoles");
                    }
                    Boolean isMigrate = (Boolean) pfm.get("migrateUsername");
                    Boolean isDeactivate = (Boolean) pfm.get("deactivateOldUsername");
                    if(isMigrate) {
                        DCIteratorBinding iter = bindings.findIteratorBinding("EditAbacusUserView1Iterator");
                        Row rw = iter.getCurrentRow();
                        if (rw != null) {
                            String newUsername = (String) rw.getAttribute("Userid");
                            String databaseUser = (String) rw.getAttribute("DatabaseUser");

                            //migrateUser
                            exec = bindings.getOperationBinding("migrateUser");
                            exec.getParamsMap().put("oldUserid", oldUsername);
                            exec.getParamsMap().put("newUserid", newUsername);
                            exec.getParamsMap().put("deactivateOldUserid", isDeactivate);
                            exec.execute();
                        }
                    }
                    
                }
                if(pfm.get("username")==null)
                {
                    DCIteratorBinding iter = bindings.findIteratorBinding("EditAbacusUserView1Iterator");
                    Row rw = iter.getCurrentRow();
                    if (rw != null) {
                        pfm.put("username", rw.getAttribute("Userid"));
                        pfm.put("action", "EDIT");
                    }
                }
                String oldDatabaseUser = (String) pfm.get("databaseUser");

                DCIteratorBinding iter = bindings.findIteratorBinding("EditAbacusUserView1Iterator");
                Row rw = iter.getCurrentRow();
                if (rw != null) {
                    
                    String databaseUser = (String) rw.getAttribute("DatabaseUser");

                    if (!databaseUser.equals(oldDatabaseUser)){
                        OperationBinding operationBinding = bindings.getOperationBinding("grantsDatabaseUser");
                        operationBinding.execute();
 
                    }
                    
                }
            }
    }

    public void setTrackingValues() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCIteratorBinding iter = bindings.findIteratorBinding("EditAbacusUserView1Iterator");
        Row rw = iter.getCurrentRow();
        if (rw != null) {
            pfm.put("databaseUser", rw.getAttribute("DatabaseUser"));

        }
        // Add event code here...
    }
}
