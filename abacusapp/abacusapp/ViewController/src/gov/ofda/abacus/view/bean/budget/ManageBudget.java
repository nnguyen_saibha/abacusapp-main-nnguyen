package gov.ofda.abacus.view.bean.budget;

import gov.ofda.abacus.model.abacusapp.pojo.CustomListItem;
import gov.ofda.abacus.view.bean.AppBean;

import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;

import java.math.BigDecimal;

import java.text.NumberFormat;
import java.text.ParseException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;

public class ManageBudget implements Serializable{
    @SuppressWarnings("compatibility:-53128111041496716")
    private static final long serialVersionUID = 1L;
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.view.bean.budget.ManageBudget.class);
    private static final String BUREAU = "Bureau";
    private static final String OFFICE = "Office";
    private static final String DIVISION = "Division";
    private static final String TEAM = "Team";
    
    private static final String REMAINING_PLANNED_AMT = "remainingPlannedAmt";
    private static final String PROJECT_BUDGET_REQUIRED = "projectBudgetReq";
    private static final String TRANSFER_AMOUNT = "transferAmt";
    private static final String BUREAU_AVAILABLE_BUDGET = "bureauBudgetAvail";
    private static final String OFFICE_AVAILABLE_BUDGET = "officeBudgetAvail";
    private static final String DIVISION_AVAILABLE_BUDGET = "divisionBudgetAvail"; 
    private static final String TEAM_AVAILABLE_BUDGET = "teamBudgetAvail";
    private String emailList=null;
    private List<CustomListItem> fundAcctTypeList=new ArrayList<CustomListItem>();
    
    
    public ManageBudget() {
        super();
    }
    
    public void setActivityBudgetInfoView() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        OperationBinding exec = bindings.getOperationBinding("setActivityBudgetInfoView");
        exec.execute();
        if (exec.getErrors().isEmpty()) {
            DCIteratorBinding iter = bindings.findIteratorBinding("ActivityBudgetInfoView1Iterator");
            if (iter.getCurrentRow() != null) {
                Row r = iter.getCurrentRow();
                BigDecimal remainingPlannedAmt = (BigDecimal) r.getAttribute("RemainingPlannedAmt");
                BigDecimal projectBudgetReq = (BigDecimal) r.getAttribute("RequiredAmt");
                pfp.put("remainingPlannedAmt", remainingPlannedAmt);    
                pfp.put(PROJECT_BUDGET_REQUIRED, projectBudgetReq);
                pfp.put(PROJECT_BUDGET_REQUIRED+"_fmt", formatNumber(projectBudgetReq));
                pfp.put(TEAM_AVAILABLE_BUDGET, r.getAttribute("TeamAvailableAmt"));
                pfp.put(TEAM_AVAILABLE_BUDGET+"_fmt", formatNumber((BigDecimal)r.getAttribute("TeamAvailableAmt")));
                pfp.put(DIVISION_AVAILABLE_BUDGET, r.getAttribute("DivisionAvailableAmt"));
                pfp.put(DIVISION_AVAILABLE_BUDGET+"_fmt", formatNumber((BigDecimal)r.getAttribute("DivisionAvailableAmt")));
                pfp.put(OFFICE_AVAILABLE_BUDGET, r.getAttribute("OfficeAvailableAmt"));
                pfp.put(OFFICE_AVAILABLE_BUDGET+"_fmt",formatNumber((BigDecimal)r.getAttribute("OfficeAvailableAmt")));
                pfp.put(BUREAU_AVAILABLE_BUDGET, r.getAttribute("BureauAvailableAmt"));
                pfp.put(BUREAU_AVAILABLE_BUDGET+"_fmt",formatNumber((BigDecimal)r.getAttribute("BureauAvailableAmt")));
                pfp.put("projBudgetAvail", r.getAttribute("ProjectAvailableAmt"));
                pfp.put("projBudgetAvail_fmt",formatNumber((BigDecimal)r.getAttribute("ProjectAvailableAmt")));
                pfp.put("plannedAmt",  r.getAttribute("PlannedAmt"));
                pfp.put("plannedAmt_fmt", formatNumber((BigDecimal)r.getAttribute("PlannedAmt")));
                pfp.put("projectBudget", r.getAttribute("ProjectBudgetAmt"));
                pfp.put("projectBudget_fmt", formatNumber((BigDecimal)r.getAttribute("ProjectBudgetAmt")));
                
            }
        
        }
        
    }
    public String setActivityDetails() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        OperationBinding exec;
        //ActivityEdit
        exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.execute();
        //setFundAccountType
        exec = bindings.getOperationBinding("setFundAccountType");
        exec.execute();
        
        AttributeBinding attrBinding = (AttributeBinding) bindings.get("BudgetSourceType");
        String budgetSourceType = (String) attrBinding.getInputValue();
        attrBinding = (AttributeBinding) bindings.get("BudgetSource");
        String budgetSource = (String) attrBinding.getInputValue();
        
        setActivityBudgetInfoView();
   
        pfp.put("from",null);
        pfp.put("isSendEmail", true);
        pfp.put("projectBudgetSourceType", budgetSourceType);
        if(!"M".equals(budgetSourceType))
            pfp.put("budgetSource", budgetSource);
        
        if(pfp.get("actionPlannedAmt")!=null){
            Object val = pfp.get("actionPlannedAmt");
            if(val instanceof String){
                pfp.put("actionPlannedAmtFmt",val);  
            }else{
                pfp.put("actionPlannedAmtFmt",formatNumber((BigDecimal)pfp.get("actionPlannedAmt")));
            }
        }
        return "next";
    }

    public String transferToActivity() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        FacesContext context = FacesContext.getCurrentInstance();
        OperationBinding exec = bindings.getOperationBinding("transferBudget");
        exec.execute();
        String result = (String) exec.getResult();
        pfp.put("confirmMessage", result);
        
        AttributeBinding attrBinding = (AttributeBinding) bindings.get("OfdaTeamCode");
        String teamCode = (String) attrBinding.getInputValue();
        
        AttributeBinding attrBindingDiv = (AttributeBinding) bindings.get("OfdaDivisionCode");
        String divCode = (String) attrBindingDiv.getInputValue();
        
        AttributeBinding attrBindingPName = (AttributeBinding) bindings.get("ProjectName");
        String projName = (String) attrBindingPName.getInputValue();
        
        String bureauCode = (String) ((AttributeBinding) bindings.get("BureauCode")).getInputValue();
        String officeCode = (String) ((AttributeBinding) bindings.get("OfficeCode")).getInputValue();
        String fundAcctTypeName = (String) ((AttributeBinding) bindings.get("FundAcctTypeName")).getInputValue();
        
        Map<String, Object> returnMap = new HashMap<String,Object>();
        returnMap.put("bureauCode", bureauCode);
        returnMap.put("officeCode", officeCode);
        returnMap.put("ofdaTeamCode", teamCode);
        returnMap.put("ofdaDivisionCode", divCode);
        returnMap.put("projName", projName);
        returnMap.put("budgetFY", pfp.get("budgetFY"));
        returnMap.put("projectNbr", pfp.get("projectNbr"));
        returnMap.put("fundAcctTypeName", fundAcctTypeName);
        BigDecimal transferAmt = (BigDecimal) pfp.get("transferAmt");
        returnMap.put("transferAmt", formatNumber(transferAmt));
        returnMap.put("comments", pfp.get("comments"));
        String from = (String) pfp.get("from");
        
        BigDecimal projectBudgetAvl = (BigDecimal) pfp.get("projBudgetAvail");
        projectBudgetAvl=projectBudgetAvl.add(transferAmt);
        returnMap.put("from", from);
        returnMap.put("availBudgetTo", formatNumber(projectBudgetAvl));
        DCIteratorBinding iter = bindings.findIteratorBinding("BudgetSourceLOV1Iterator");
        Key key = new Key(new Object[] { pfp.get("budgetSource") });
        Row r[] = iter.getRowSetIterator().findByKey(key, 1);
        if(r.length==1)
        {
            returnMap.put("budgetSource",r[0].getAttribute("BudgetSourceName") );
        }
        pfp.put("transferDetails", returnMap);
        pfp.put("isSendEmail", pfp.get("isSendEmail"));
        return (String) result;
        
    }

    public void projectSourceChgLsnr(ValueChangeEvent vce) {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pageFlowMap.put(TRANSFER_AMOUNT, null);
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("getValidFundAccountTypeList");
        exec.getParamsMap().put("transferFrom",vce.getNewValue());
        exec.execute();
        fundAcctTypeList = (List<CustomListItem>) exec.getResult();
        AdfFacesContext.getCurrentInstance().addPartialTarget(vce.getComponent().getParent().getParent());
    }
    
    public String formatNumber(BigDecimal number){
        String returnVal = "";
        if(null != number){
            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CANADA);
            double doubleVal = number.doubleValue();
            returnVal = format.format(doubleVal);
        }
        return returnVal;
    }
    public void pReqAmtDoubleClick(ClientEvent clientEvent) {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String from = (String) pageFlowMap.get("from");
        BigDecimal required = (BigDecimal) pageFlowMap.get("projectBudgetReq");
        pageFlowMap.put("transferAmt",required);
        String id = "transferAmt_" +from;
        BigDecimal avail = null;
        if(from.equalsIgnoreCase(BUREAU)){
            avail = (BigDecimal) pageFlowMap.get(BUREAU_AVAILABLE_BUDGET);
        }else if(from.equalsIgnoreCase(OFFICE)){
            avail = (BigDecimal) pageFlowMap.get(OFFICE_AVAILABLE_BUDGET);
        }else if(from.equalsIgnoreCase(DIVISION)){
            avail = (BigDecimal)  pageFlowMap.get(DIVISION_AVAILABLE_BUDGET);
        }else if(from.equalsIgnoreCase(TEAM)){
            avail = (BigDecimal)  pageFlowMap.get(TEAM_AVAILABLE_BUDGET);
        }
        
        if(required.compareTo(avail) == 1){
            FacesContext.getCurrentInstance().addMessage(id,new FacesMessage(FacesMessage.SEVERITY_ERROR, null,  "You entered an amount of "+ formatNumber(required)+". Enter an amount that is less than or equal to available amount "+formatNumber(avail))); 
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(clientEvent.getComponent().getParent().getParent());
    
    }
    
    public void pActionPlannedAmtDoubleClick(ClientEvent clientEvent){
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String from = (String) pageFlowMap.get("from");
        Object actionPAmountObj = pageFlowMap.get("actionPlannedAmt");
        BigDecimal actionPlannedAmt = null;
        if(null != actionPAmountObj){
            if(actionPAmountObj instanceof String){
                try {
                    Number val = NumberFormat.getCurrencyInstance().parse(actionPAmountObj.toString());
                    actionPlannedAmt = new BigDecimal(val.toString());
                    pageFlowMap.put(TRANSFER_AMOUNT,actionPlannedAmt);
                } catch (ParseException e) {
                }
            }else{
                actionPlannedAmt = (BigDecimal) pageFlowMap.get("actionPlannedAmt");
                pageFlowMap.put(TRANSFER_AMOUNT,actionPlannedAmt);
            }
        }
        String id = "transferAmt_" +from;
        BigDecimal avail = null;
        if(from.equalsIgnoreCase(BUREAU)){
                avail = (BigDecimal) pageFlowMap.get(BUREAU_AVAILABLE_BUDGET);
        }else if(from.equalsIgnoreCase(OFFICE)){
            avail = (BigDecimal) pageFlowMap.get(OFFICE_AVAILABLE_BUDGET);
        }else if(from.equalsIgnoreCase(DIVISION)){
            avail = (BigDecimal)  pageFlowMap.get(DIVISION_AVAILABLE_BUDGET);
        }else if(from.equalsIgnoreCase(TEAM)){
            avail = (BigDecimal)  pageFlowMap.get(TEAM_AVAILABLE_BUDGET);
        }
        
        if(actionPlannedAmt != null && actionPlannedAmt.compareTo(avail) == 1){
            FacesContext.getCurrentInstance().addMessage(id,new FacesMessage(FacesMessage.SEVERITY_ERROR, null,  "You entered an amount of "+ formatNumber(actionPlannedAmt)+". Enter an amount that is less than or equal to available amount "+formatNumber(avail))); 
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(clientEvent.getComponent().getParent().getParent());
        

    }
    
    public void pAvailAmtDoubleClick(ClientEvent clientEvent){
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String from = (String) pageFlowMap.get("from");
        BigDecimal avail=null;
        String id = "transferAmt_" +from;

        if(from.equalsIgnoreCase(BUREAU)){
                avail = (BigDecimal) pageFlowMap.get(BUREAU_AVAILABLE_BUDGET);
        }else if(from.equalsIgnoreCase(OFFICE)){
            avail = (BigDecimal) pageFlowMap.get(OFFICE_AVAILABLE_BUDGET);
        }else if(from.equalsIgnoreCase(DIVISION)){
            avail = (BigDecimal)  pageFlowMap.get(DIVISION_AVAILABLE_BUDGET);
        }else if(from.equalsIgnoreCase(TEAM)){
            avail = (BigDecimal)  pageFlowMap.get(TEAM_AVAILABLE_BUDGET);
        }
        if(avail!=null) {
               pageFlowMap.put("transferAmt",avail);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(clientEvent.getComponent().getParent().getParent());
        
        BigDecimal plannedAmt = (BigDecimal) pageFlowMap.get("plannedAmt");
        if(avail.compareTo(plannedAmt)== 1){
            FacesContext.getCurrentInstance().addMessage(id, new FacesMessage(FacesMessage.SEVERITY_WARN, null,  "Transfer amount is greater than Planned Amt(" + pageFlowMap.get("plannedAmt_fmt")+") of the Project."));
        }
        
        
    }
    
    public String getEmailAddr(){
        if(emailList==null)
        {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        String budgetFY = ""+((AttributeBinding) bindings.get("BudgetFy")).getInputValue();
        String projectNbr = (String) ((AttributeBinding) bindings.get("ProjectNbr")).getInputValue();
        MessageBean mbean = new MessageBean();
        Map<String, String> emailMap = mbean.getActivityBudgetTransferEmailList(budgetFY,projectNbr);
        emailList=emailMap.get("emailList");
        }
        return (emailList);
    }

    private Number getAmt(String attrName) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        AttributeBinding attrBinding = (AttributeBinding) bindings.get(attrName);
        String tmp = (String) attrBinding.getInputValue();
        Number availAmt=0;
        if(tmp!=null) {
            try {
                availAmt = NumberFormat.getCurrencyInstance().parse(tmp);
            } catch (Exception e) {
                availAmt=0;
            }
        }
        return availAmt;
    }

    public void transferAmtValueChangeListner(ValueChangeEvent valueChangeEvent) {
        BigDecimal value = (BigDecimal) valueChangeEvent.getNewValue();
        if(value != null){
            Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            BigDecimal plannedAmt = (BigDecimal) map.get("plannedAmt");
            BigDecimal avail = null;
            String from = (String) map.get("from");
            if(from.equalsIgnoreCase(BUREAU)){
                avail = (BigDecimal) map.get(BUREAU_AVAILABLE_BUDGET);
            }else if(from.equalsIgnoreCase(OFFICE)){
                    avail = (BigDecimal) map.get(OFFICE_AVAILABLE_BUDGET);
                }else if(from.equalsIgnoreCase(DIVISION)){
                    avail = (BigDecimal)  map.get(DIVISION_AVAILABLE_BUDGET);
                }else if(from.equalsIgnoreCase(TEAM)){
                    avail = (BigDecimal)  map.get(TEAM_AVAILABLE_BUDGET);
                }

            // check if value is less than or equal to zero
            if(value.doubleValue() <= 0){
                FacesContext.getCurrentInstance().addMessage(valueChangeEvent.getComponent().getId(), new FacesMessage(FacesMessage.SEVERITY_ERROR, null,  "You entered an amount of "+ formatNumber(value) + ". Enter an amount that is at least $0.01"));                
            }
            // compare against planned amt
            if(value.compareTo(plannedAmt)== 1){
                FacesContext.getCurrentInstance().addMessage(valueChangeEvent.getComponent().getId(), new FacesMessage(FacesMessage.SEVERITY_WARN, null,  "You entered an amount of " + formatNumber(value) + ". Entered amount is greater than planned amount " + map.get("plannedAmt_fmt")));
            }
            
            // compare against Available Amount
            if(value.compareTo(avail) == 1){
                    FacesContext.getCurrentInstance().addMessage(valueChangeEvent.getComponent().getId(), new FacesMessage(FacesMessage.SEVERITY_ERROR, null,  "You entered an amount of "+ formatNumber(value)+". Enter an amount that is less than or equal to available amount "+formatNumber(avail)));
                }
        }
        
    }
    public void fundAccountTypeChgLnsr(ValueChangeEvent vce) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        //isVActivityAmtValid
        pfp.put("fundAcctTypeCode", vce.getNewValue());
        setActivityBudgetInfoView();
        String from = (String) pfp.get("from");
        if(from!=null)
        {
        BigDecimal avail=new BigDecimal(0);
        if(from.equalsIgnoreCase(BUREAU)){
            avail = (BigDecimal) pfp.get(BUREAU_AVAILABLE_BUDGET);
        }else if(from.equalsIgnoreCase(OFFICE)){
                avail = (BigDecimal) pfp.get(OFFICE_AVAILABLE_BUDGET);
            }else if(from.equalsIgnoreCase(DIVISION)){
                avail = (BigDecimal)  pfp.get(DIVISION_AVAILABLE_BUDGET);
            }else if(from.equalsIgnoreCase(TEAM)){
                avail = (BigDecimal)  pfp.get(TEAM_AVAILABLE_BUDGET);
            }
        if(avail!=null && avail.equals(BigDecimal.ZERO))
           pfp.put("from", null);
        }
        pfp.put(TRANSFER_AMOUNT, null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(vce.getComponent().getParent().getParent().getParent().getParent());
    }

    public List<CustomListItem> getFundAcctTypeList() {
        return fundAcctTypeList;
    }
}
