package gov.ofda.abacus.view.bean.budget;

import gov.ofda.abacus.model.abacusapp.pojo.CustomListItem;
import gov.ofda.abacus.view.base.AbacusEmail;
import gov.ofda.abacus.view.base.UIControl;

import gov.ofda.abacus.view.bean.AppBean;
import gov.ofda.abacus.view.bean.message.MessageBean;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.LaunchPopupEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.ReturnPopupEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class AddOrRecoverODTBudgetBean extends UIControl {
    @SuppressWarnings("compatibility:-5965509895513390148")
    private static final long serialVersionUID = 1L;
    private static final String TRANSFER = "Transfer";
    private static final String RECOVER = "Recover";
    private static final String TRANSFER_FROM_BUREAU = "Bureau";
    private static final String TRANSFER_FROM_OFFICE = "Office";
    private static final String TRANSFER_FROM_DIVISION = "Division";
    private static final String TRANSFER_FROM_TEAM = "Team";
    private List<CustomListItem> fundAcctTypeList=new ArrayList<CustomListItem>();
    public AddOrRecoverODTBudgetBean() {
        super();
    }

    public String checkValidity() {
        String missingInfo = "";
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String transferType = (String) pfm.get("transferType");
        String transferLevel = (String) pfm.get("transferLevel");
        Integer fiscalYear = (Integer) pfm.get("fiscalYear");
        String bureau = (String) pfm.get("bureauCode");
        String office = (String) pfm.get("officeCode");
        String division = (String) pfm.get("division");
        String team = (String) pfm.get("team");
        if (!("Transfer".equals(transferType) || "Recover".equals(transferType))) {
            pfm.put("message", "Unknown Transfer Type: Valid values are 'Transfer' & 'Recover'");
            return "error";
        }
        SecurityContext ctx = ADFContext.getCurrent().getSecurityContext();
        if (("Bureau".equals(transferLevel) && !ctx.isUserInRole("APP_BBL")) ||
            ("Office".equals(transferLevel) && !ctx.isUserInRole("APP_BBL")) ||
            ("Division".equals(transferLevel) && !ctx.isUserInRole("APP_OL")) ||
            ("Team".equals(transferLevel) && !ctx.isUserInRole("APP_DL"))) {
            pfm.put("message", "User does not have necessary role to do " + transferLevel + " " + transferType);
            return "error";
        }
        // remaining validations
        if (fiscalYear == null)
            missingInfo = "Fiscal Year,";
        switch(transferLevel) {
        case TRANSFER_FROM_BUREAU:
            missingInfo+=bureau != null?"":"Bureau,";
            break;
        case TRANSFER_FROM_OFFICE:
            missingInfo+=bureau != null?"":"Bureau,";
            missingInfo+=office != null?"":"Office,";
            break;
        case TRANSFER_FROM_DIVISION:
            missingInfo+=bureau != null?"":"Bureau,";
            missingInfo+=office != null?"":"Office,";
            missingInfo+=division != null?"":"Division,";
            break;
        case TRANSFER_FROM_TEAM:
            missingInfo+=bureau != null?"":"Bureau,";
            missingInfo+=office != null?"":"Office,";
            missingInfo+=division != null?"":"Division,";
            missingInfo+=team != null?"":"Team,";
            break;
            
        }
        if(fiscalYear!=null && (fiscalYear<2001 || fiscalYear>9999)) {
            pfm.put("message",
                    "Fiscal Year must be between 2001 and 9999");
            return "error";
        }
        if (missingInfo.length() == 0){
            return "next";
        }
        pfm.put("message",
                 "Select or enter missing required value(s) in Search: " + missingInfo);

        return "error";
    }

    public String transferBudget() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        FacesContext context = FacesContext.getCurrentInstance();
        OperationBinding exec = bindings.getOperationBinding("addOrReoverODTBudget");
        exec.execute();
        String result = (String) exec.getResult();
        pfm.put("confirmMessage", result);

        Map<String, Object> returnMap = new HashMap<String, Object>();
        BigDecimal transferAmt = (BigDecimal) pfm.get("transferAmt");
        returnMap.put("fundCode", pfm.get("fundCode"));
        returnMap.put("fundAcctTypeCode", pfm.get("fundAcctTypeCode"));
        returnMap.put("transferAmt", currencyFormat(transferAmt));
        returnMap.put("comments", pfm.get("comments"));
        returnMap.put("parentLevel", pfm.get("parentLevel"));
        returnMap.put("parentName", pfm.get("parentName"));
        returnMap.put("detailName", pfm.get("detailName"));
        returnMap.put("emailTo", pfm.get("emailTo"));
        returnMap.put("emailCC", pfm.get("emailCC"));
        pfm.put("transferDetails", returnMap);
        pfm.put("isSendEmail", pfm.get("isSendEmail"));
        return (String) result;
    }

    public String setupTransferValidation() {
        //Check if row exists
        //depending on transfer details setup min/max
        //If no available amt show message and close
        //Setup Fund Code if needed fund code validation
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec;
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String transferType = (String) pageFlowMap.get("transferType");
        String transferLevel = (String) pageFlowMap.get("transferLevel");
        Integer fiscalYear = (Integer) pageFlowMap.get("fiscalYear");
        String bureau = (String) pageFlowMap.get("bureauCode");
        String office = (String) pageFlowMap.get("officeCode");
        String division = (String) pageFlowMap.get("division");
        String team = (String) pageFlowMap.get("team");
        String detailName=null;
        exec = bindings.getOperationBinding("ExecuteFundLookup");
        exec.execute();
        exec = bindings.getOperationBinding("ExecuteFundAcctTypeLookup");
        exec.execute();
        exec = bindings.getOperationBinding("setBudgetInfoView");
        exec.execute();
        if (exec.getErrors().isEmpty()) {
            DCIteratorBinding iter = bindings.findIteratorBinding("BudgetInfoView1Iterator");
            if (iter.getCurrentRow() != null) {
                Row r = iter.getCurrentRow();
                BigDecimal availableAmt = new BigDecimal(0);
                BigDecimal fromAvailableAmt = new BigDecimal(0);
                BigDecimal toAvailableAmt = new BigDecimal(0);
                String parentLevel = null;
                String parentName = null;
                switch (transferType + transferLevel) {
                case "TransferBureau":
                        availableAmt = null;
                        toAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailBudget");
                        break;
                case "RecoverBureau":
                        availableAmt = (BigDecimal) r.getAttribute("BureauAvailBudget");
                        fromAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailBudget");
                        parentLevel = "Bureau";
                        break;
                case "TransferOffice":
                    availableAmt = (BigDecimal) r.getAttribute("BureauAvailBudget");
                    parentLevel = "Bureau";
                    parentName=bureau;
                    detailName=office;
                    fromAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailBudget");
                    toAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailBudget");
                    break;
                case "RecoverOffice":
                    availableAmt = (BigDecimal) r.getAttribute("OfficeAvailBudget");
                    parentLevel = "Bureau";
                    parentName=bureau;
                    detailName=office;
                    toAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailBudget");
                    fromAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailBudget");
                    break;
                case "TransferDivision":
                    availableAmt = (BigDecimal) r.getAttribute("OfficeAvailBudget");
                    parentLevel = "Office";
                    parentName=office;
                    detailName=division;
                    fromAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailBudget");
                    toAvailableAmt = (BigDecimal) r.getAttribute("DivisionAvailBudget");
                    break;
                case "RecoverDivision":
                    availableAmt = (BigDecimal) r.getAttribute("DivisionAvailBudget");
                    parentLevel = "Office";
                    parentName=office;
                    detailName=division;
                    toAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailBudget");
                    fromAvailableAmt = (BigDecimal) r.getAttribute("DivisionAvailBudget");
                    break;
                case "TransferTeam":
                    availableAmt = (BigDecimal) r.getAttribute("DivisionAvailBudget");
                    parentLevel = "Division";
                    parentName=division;
                    detailName=team;
                    fromAvailableAmt = (BigDecimal) r.getAttribute("DivisionAvailBudget");
                    toAvailableAmt = (BigDecimal) r.getAttribute("TeamAvailBudget");
                    break;
                case "RecoverTeam":
                    availableAmt = (BigDecimal) r.getAttribute("TeamAvailBudget");
                    parentLevel = "Division";
                    parentName=division;
                    detailName=team;
                    toAvailableAmt = (BigDecimal) r.getAttribute("DivisionAvailBudget");
                    fromAvailableAmt = (BigDecimal) r.getAttribute("TeamAvailBudget");
                    break;
                }
                if ((TRANSFER_FROM_OFFICE.equalsIgnoreCase(transferLevel) || TRANSFER_FROM_DIVISION.equalsIgnoreCase(transferLevel)|| TRANSFER_FROM_TEAM.equalsIgnoreCase(transferLevel)) && availableAmt != null &&
                    availableAmt.compareTo(BigDecimal.ZERO) == 0) {
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "No available amt",
                                         "No available amount to " +
                                         (RECOVER.equalsIgnoreCase(transferType) ? "recover " : "add") +
                                         ("Transfer".equals(transferType) ?
                                          " to " + transferLevel + " from " + parentLevel + " "+parentName:
                                          " from " + transferLevel + " to " + parentLevel + " "+parentName));
                    FacesContext.getCurrentInstance().addMessage(null, msg);

                }

                if (TRANSFER_FROM_BUREAU.equalsIgnoreCase(transferLevel) && RECOVER.equalsIgnoreCase(transferType) &&
                    availableAmt != null && availableAmt.compareTo(BigDecimal.ZERO) == 0) {
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "No available amt",
                                         "No available amount to recover.");
                    FacesContext.getCurrentInstance().addMessage(null, msg);

                }
                pageFlowMap.put("availableAmt", availableAmt);
                pageFlowMap.put("toAvailableAmt", toAvailableAmt);
                pageFlowMap.put("fromAvailableAmt", fromAvailableAmt);
                if (availableAmt != null)
                    pageFlowMap.put("availableAmtFormat", currencyFormat(availableAmt));
                pageFlowMap.put("parentLevel", parentLevel);
                pageFlowMap.put("parentName", parentName);
                pageFlowMap.put("detailName", detailName);
                pageFlowMap.put("isSendEmail", false);
                
                if(TRANSFER_FROM_BUREAU.equalsIgnoreCase(transferLevel))
                    pageFlowMap.put("isSendEmail",false);
                else
                    pageFlowMap.put("isSendEmail", true);
                if (transferLevel.equals("Team")) {
                    //SetTeamTotalByBudgetSourceView
                    exec = bindings.getOperationBinding("SetTeamTotalByBudgetSourceView");
                    exec.execute();
                }
                exec = bindings.getOperationBinding("getBudgetTransferEmailList");
                exec.execute();
                String transferList = (String) exec.getResult();
                    Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
                    MessageBean mb = new MessageBean();
                    String emailCC = mb.getUserEmail();
                    String emailTo = ""+appScope.get("FIN_EMAIL");
                    if(transferList!=null)
                        emailTo+="; "+transferList;
                pageFlowMap.put("emailTo", emailTo);
                pageFlowMap.put("emailCC", emailCC);
                exec = bindings.getOperationBinding("getValidFundAccountTypeList");
                exec.execute();
                fundAcctTypeList = (List<CustomListItem>) exec.getResult();
                return "next";
            } else {
                pageFlowMap.put("message",
                                "Error finding Budget information. " + transferType + " : " + transferLevel + " : " +
                                fiscalYear + " : " + division + " : " + "");
                return "error";
            }
        } else {
            pageFlowMap.put("message",
                            "Error finding Budget information. " + transferType + " : " + transferLevel + " : " +
                            fiscalYear + " : " + division + " : " + "");
            return "error";
        }
    }

    public void availableAmtDoubleClick(ClientEvent clientEvent) {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfm.put("transferAmt", pfm.get("availableAmt"));
        AdfFacesContext.getCurrentInstance().addPartialTarget(clientEvent.getComponent().getParent().getParent());
    }

    public void fundCodeLOVReturnLnsr(ReturnPopupEvent returnPopupEvent) {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("FundLookupView1Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row r = rsi.getCurrentRow();
        if (r != null) {
            pfm.put("fundCode", r.getAttribute("FundCode"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent());
        }
        rsi.closeRowSetIterator();
    }

    public void fundCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        String value = (String) object;
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if (value != null) {
            value = value.toUpperCase();
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("FundLookupViewCheckIterator");
            Key fundkey = new Key(new Object[] { pfm.get("fiscalYear"), value });
            Row r[] = iter.getRowSetIterator().findByKey(fundkey, 1);
            if (r.length < 1) {
                facesContext.addMessage(uIComponent.getId(),
                                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                         "Invalid Fund Code. Use Search to find and select correct Fund Code"));
            }

        }

    }

    public void transferAmtValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        BigDecimal value = (BigDecimal) object;
        if (value != null) {
            Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            BigDecimal avail = (BigDecimal) pfm.get("availableAmt");

            // check if value is less than or equal to zero
            if (value.doubleValue() <= 0) {
                FacesContext.getCurrentInstance().addMessage(uIComponent.getId(),
                                                             new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                              "You entered an amount of " +
                                                                              currencyFormat(value) +
                                                                              ". Enter an amount that is at least $0.01"));
            }
            // compare against Available Amount
            if (avail != null && value.compareTo(avail) == 1) {
                FacesContext.getCurrentInstance().addMessage(uIComponent.getId(),
                                                             new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                              "You entered an amount of " +
                                                                              currencyFormat(value) +
                                                                              ". Enter an amount that is less than or equal to available amount " +
                                                                              currencyFormat(avail)));
            }
        }

    }

    public void setBudgetInfoAndSendEmail() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        Map transferDetails = (Map) pfm.get("transferDetails");
        String result = (String) pfm.get("confirmMessage");
        if ("true".equals(result)) {
            OperationBinding exec = bindings.getOperationBinding("setBudgetInfoView");
            exec.getParamsMap().put("fundAcctTypeCode",transferDetails.get("fundAcctTypeCode"));
            exec.execute();
            if ((Boolean) pfm.get("isSendEmail")) {
                DCIteratorBinding iter = bindings.findIteratorBinding("BudgetInfoView1Iterator");
                Row r = iter.getCurrentRow();
                if (r != null) {
                    Map<String, Object> tDMap = new HashMap<String, Object>();
                    tDMap.put("transferType", pfm.get("transferType"));
                    tDMap.put("transferLevel", pfm.get("transferLevel"));
                    tDMap.put("fiscalYear", pfm.get("fiscalYear"));
                    tDMap.put("bureauCode", pfm.get("bureauCode"));
                    tDMap.put("officeCode", pfm.get("officeCode"));
                    tDMap.put("division", pfm.get("division"));
                    tDMap.put("team", pfm.get("team"));
                    tDMap.put("fundAcctTypeCode", transferDetails.get("fundAcctTypeCode"));
                    tDMap.put("fundAcctTypeName",r.getAttribute("FundAcctTypeName"));
                    tDMap.put("newTotalAmt",
                              currencyFormat((BigDecimal) r.getAttribute(pfm.get("transferLevel") + "TotalBudget")));
                    tDMap.put("newAvailAmt",
                              currencyFormat((BigDecimal) r.getAttribute(pfm.get("transferLevel") + "AvailBudget")));
                    tDMap.put("parentLevel", transferDetails.get("parentLevel"));
                    tDMap.put("parentName", transferDetails.get("parentName"));
                    tDMap.put("detailName", transferDetails.get("detailName"));
                    tDMap.put("transferAmt", transferDetails.get("transferAmt"));
                    tDMap.put("comments", transferDetails.get("comments"));
                    tDMap.put("fundCode", transferDetails.get("fundCode"));
                    tDMap.put("emailTo", transferDetails.get("emailTo"));
                    tDMap.put("emailCC", transferDetails.get("emailCC"));
                    
                    String emailList = sendAddorRecoverODTBudgetEmail(result, tDMap, (Boolean) pfm.get("isSendEmail"));
                    
                    if (emailList != null)
                        pfm.put("emailList", emailList);
                }
            }
        }
    }

    public String sendAddorRecoverODTBudgetEmail(String result, Map transferDetails, Boolean isSendEmail) {
        AbacusEmail email = new AbacusEmail();
        MessageBean mb = new MessageBean();
        String toAdd = (String) transferDetails.get("emailTo");
        String ccAdd = (String) transferDetails.get("emailCC");
        FacesContext context = FacesContext.getCurrentInstance();
        String transferLevel = (String) transferDetails.get("transferLevel");
        String transferType = (String) transferDetails.get("transferType");
        if ("true".equals(result)) {
            if (null != isSendEmail && isSendEmail) {
                String username = ADFContext.getCurrent().getSecurityContext().getUserName();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                String fdate = sdf.format(new Date());

               StringBuilder builder = new StringBuilder();
               builder.append("<table border=\"1\" style=\"border-collapse:collapse\" cellpadding=\"5\">" + "<tbody>");
                //if("OFDA".equals(transferLevel)){
                    builder.append("<tr>" +
                                        "<td colspan=\"2\" style=\"text-align:left\"> <b> " + transferLevel +" Details</b> </td>" + 
                                    "</tr>");
                    
                    builder.append("<tr>" + 
                                           // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" + 
                                            "<td style=\"text-align:right\">Fiscal Year</td>" + 
                                            "<td style=\"text-align:left\">" +transferDetails.get("fiscalYear") +"</td>" + 
                                    "</tr>");
                    builder.append("<tr>" + 
                                       // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" + 
                                        "<td style=\"text-align:right\">Bureau</td>" + 
                                        "<td style=\"text-align:left\">" +transferDetails.get("bureauCode") +"</td>" + 
                                "</tr>");
                    if("Office".equals(transferLevel)||"Team".equals(transferLevel)||"Division".equals(transferLevel)){
                        builder.append("<tr>" + 
                                               // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" + 
                                                "<td style=\"text-align:right\">Office</td>" + 
                                                "<td style=\"text-align:left\">" +transferDetails.get("officeCode") +"</td>" + 
                                        "</tr>");
                    }
                    if("Team".equals(transferLevel)||"Division".equals(transferLevel)){
                        builder.append("<tr>" + 
                                               // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" + 
                                                "<td style=\"text-align:right\">Division</td>" + 
                                                "<td style=\"text-align:left\">" +transferDetails.get("division") +"</td>" + 
                                        "</tr>");
                    }

                    if("Team".equals(transferLevel)){
                        builder.append("<tr>" + 
                                               // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" + 
                                                "<td style=\"text-align:right\">Team</td>" + 
                                                "<td style=\"text-align:left\">" +transferDetails.get("team") +"</td>" + 
                                        "</tr>");
                    }
                
                    builder.append("<tr>" +
                                    "<td colspan=\"2\" style=\"text-align:left\"> <b>" + ("Transfer".equals(transferType) ? "Transfer Details" : " Recovery Details") + " </b> </td>" + 
                                "</tr>");

                     if(!"Bureau".equals(transferLevel)){
                    

                        builder.append("<tr>" + 
                                               // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" + 
                                                "<td style=\"text-align:right\">"+  ("Transfer".equals(transferType) ? "Transfer From" : " Recovery To") +" </td>" + 
                                       "<td style=\"text-align:left\">"+transferDetails.get("parentLevel")+" " +(transferDetails.get("parentName")!=null?transferDetails.get("parentName"):"") +"</td>" + 
                                        "</tr>");
                    }
                builder.append("<tr>" + 
                               // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" + 
                                "<td style=\"text-align:right\">Fund Account Type</td>" + 
                                "<td style=\"text-align:left\">" +transferDetails.get("fundAcctTypeName") +"</td>" + 
                        "</tr>");
                if("Bureau".equals(transferLevel)){
                    builder.append("<tr>" + 
                                       // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" + 
                                        "<td style=\"text-align:right\">Fund Code</td>" + 
                                        "<td style=\"text-align:left\">" +transferDetails.get("fundCode") +"</td>" + 
                                "</tr>");
                }
                    builder.append("<tr>" + 
                                   // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" + 
                                    "<td style=\"text-align:right\">" +("Transfer".equals(transferType) ? "Transfer Amt" : "Recovery Amt")+ "</td>" + 
                                    "<td style=\"text-align:left\">" +transferDetails.get("transferAmt") +"</td>" + 
                            "</tr>");
                
                    builder.append("<tr>" + 
                                  //  "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" + 
                                    "<td style=\"text-align:right\">Available Budget</td>" + 
                                    "<td style=\"text-align:left\">" +transferDetails.get("newAvailAmt") +"</td>" + 
                            "</tr>");
                    
                    builder.append("<tr>" + 
                                        "<td colspan=\"2\" style=\"text-align:left\"> <b>Comments: </b> "+transferDetails.get("comments") +"</td>" + 
                                    "</tr>");
                    
                    builder.append("<tr>" + 
                                        "<td colspan=\"2\" style=\"text-align:left\"> <b>Updated By </b>" + username + 
                                        " <b> On </b>" + fdate + "</td>" + "</tr>" + "</tbody>" + "</table>");

                String htmlMsg = builder.toString();
                
               // System.out.println("str  :: " +htmlMsg);
                String sub = "";
                if("Bureau".equals(transferLevel)) {
                    sub = transferDetails.get("fiscalYear") + " " + transferLevel +" "+transferDetails.get("bureauCode") + " Budget " + ("Transfer".equals(transferType) ? "Transfer" : " Recovery");
                }
                else {
                    StringBuilder sb=new StringBuilder();
                    sb.append(transferDetails.get("fiscalYear") + " Budget ");
                    if("Transfer".equals(transferType)) {
                        sb.append("transferred from "+transferDetails.get("parentLevel")+ " "+transferDetails.get("parentName")+" to ");
                        sb.append(transferLevel+" "+transferDetails.get("detailName"));
                    }
                    else {
                        sb.append("recovered from "+transferLevel+" "+transferDetails.get("detailName")+" to ");
                        sb.append(transferDetails.get("parentLevel")+ " "+transferDetails.get("parentName"));
                    }
                    sub =sb.toString();  
                }
                 
                Boolean mailSent = email.sendHtmlEmail(toAdd, ccAdd, sub, htmlMsg, null, null);
                if (mailSent) {
                    return "Email sent to " + toAdd + " " + ccAdd;
                }
            }
        }
        return null;
    }

    public void fundCodeLaunchLister(LaunchPopupEvent launchPopupEvent) {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String value = (String) launchPopupEvent.getSubmittedValue();
        if (value != null && value.length() > 0) {
            value = value.toUpperCase();
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("FundLookupViewCheckIterator");
            Key fundkey = new Key(new Object[] { pfm.get("fiscalYear"), value });
            Row r[] = iter.getRowSetIterator().findByKey(fundkey, 1);
            if (r.length == 1) {
                launchPopupEvent.setLaunchPopup(false);
                pfm.put("fundCode", value);
                AdfFacesContext.getCurrentInstance().addPartialTarget(launchPopupEvent.getComponent().getParent());
            }
        }
    }


    public void manageFundsCloseLnsr(PopupCanceledEvent popupCanceledEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("FundLookupExecute");
        exec.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(popupCanceledEvent.getComponent().getParent().getParent());
    }


    public String checkAddNewParameterValidity() {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String bureau = (String) pageFlowMap.get("bureauCode");
        String office = (String) pageFlowMap.get("officeCode");
        String division = (String) pageFlowMap.get("division");
        String team = (String) pageFlowMap.get("team");

        String transferLevel = (String) pageFlowMap.get("transferLevel");

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            //Validate Bureau
            if (bureau!=null && (TRANSFER_FROM_BUREAU.equals(transferLevel)||TRANSFER_FROM_OFFICE.equals(transferLevel)|| TRANSFER_FROM_DIVISION.equals(transferLevel) ||TRANSFER_FROM_TEAM.equals(transferLevel))) {
            
                DCIteratorBinding iter = bindings.findIteratorBinding("BureauLOV1Iterator");
                Key key = new Key(new Object[] { bureau });
                Row r[] = iter.getRowSetIterator().findByKey(key, 1);
                if (r.length < 1) {
                    pageFlowMap.put("bureauCode", null);
                    StringBuilder message = new StringBuilder("<html><body>");
                    message.append("Bureau <b> \"" + bureau +
                                   "  \" </b> is invalid/inactive. Please select a valid Bureau");
                    message.append("</body></html>");
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Invalid Bureau", message.toString());
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
            }
            //Validate Office
            if (office!=null && (TRANSFER_FROM_OFFICE.equals(transferLevel)|| TRANSFER_FROM_DIVISION.equals(transferLevel) ||TRANSFER_FROM_TEAM.equals(transferLevel))) {
            
                DCIteratorBinding iter = bindings.findIteratorBinding("OfficeLOV1Iterator");
                Key key = new Key(new Object[] { office });
                Row r[] = iter.getRowSetIterator().findByKey(key, 1);
                if (r.length < 1) {
                    pageFlowMap.put("officeCode", null);
                    StringBuilder message = new StringBuilder("<html><body>");
                    message.append("Office <b> \"" + office +
                                   "  \" </b> is invalid/inactive. Please select a valid Office");
                    message.append("</body></html>");
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Invalid Office", message.toString());
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
            }
            //Validate division
            if (division != null && (TRANSFER_FROM_DIVISION.equals(transferLevel) ||TRANSFER_FROM_TEAM.equals(transferLevel))) {
        
                DCIteratorBinding iter = bindings.findIteratorBinding("DivisionActiveLOV1Iterator");
                Key key = new Key(new Object[] { division });
                Row r[] = iter.getRowSetIterator().findByKey(key, 1);
                if (r.length < 1) {
                    pageFlowMap.put("division", null);
                    StringBuilder message = new StringBuilder("<html><body>");
                    message.append("Division <b> \"" + division +
                                   "  \" </b> is invalid/inactive. Please select a valid Division");
                    message.append("</body></html>");
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Invalid Division", message.toString());
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
            }
            //Validate Team
            if (team != null && (TRANSFER_FROM_TEAM.equals(transferLevel))) {
            
                DCIteratorBinding iter = bindings.findIteratorBinding("TeamActiveLOV1Iterator");
                Key key = new Key(new Object[] { team });
                Row r[] = iter.getRowSetIterator().findByKey(key, 1);
                if (r.length < 1) {
                    pageFlowMap.put("team", null);
                    StringBuilder message = new StringBuilder("<html><body>");
                    message.append("Team <b> \"" + team +
                                   "  \" </b> is invalid/inactive. Please select a valid Team");
                    message.append("</body></html>");
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Invalid Team", message.toString());
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
            }
            return "next";
        }

    public void fundAccountTypeChgLnsr(ValueChangeEvent vce) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec;
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String transferType = (String) pageFlowMap.get("transferType");
        String transferLevel = (String) pageFlowMap.get("transferLevel");
        String parentLevel = (String) pageFlowMap.get("parentLevel");
        String parentName = (String) pageFlowMap.get("parentName");
        exec = bindings.getOperationBinding("setBudgetInfoView");
        exec.getParamsMap().put("fundAcctTypeCode", vce.getNewValue());
        exec.execute();
        if (transferLevel.equals("Team")) {
            //SetTeamTotalByBudgetSourceView
            exec = bindings.getOperationBinding("SetTeamTotalByBudgetSourceView");
            exec.getParamsMap().put("p_fundAcctTypeCode", vce.getNewValue());
            exec.execute();
        }
        if (exec.getErrors().isEmpty()) {
            DCIteratorBinding iter = bindings.findIteratorBinding("BudgetInfoView1Iterator");
            if (iter.getCurrentRow() != null) {
                Row r = iter.getCurrentRow();
                BigDecimal availableAmt = new BigDecimal(0);
                BigDecimal fromAvailableAmt = new BigDecimal(0);
                BigDecimal toAvailableAmt = new BigDecimal(0);
                switch (transferType + transferLevel) {
                case "TransferBureau":
                        availableAmt = null;
                        toAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailBudget");
                        break;
                case "RecoverBureau":
                        availableAmt = (BigDecimal) r.getAttribute("BureauAvailBudget");
                        fromAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailBudget");
                        break;
                case "TransferOffice":
                    availableAmt = (BigDecimal) r.getAttribute("BureauAvailBudget");
                    fromAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailBudget");
                    toAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailBudget");
                    break;
                case "RecoverOffice":
                    availableAmt = (BigDecimal) r.getAttribute("OfficeAvailBudget");
                    toAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailBudget");
                    fromAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailBudget");
                    break;
                case "TransferDivision":
                    availableAmt = (BigDecimal) r.getAttribute("OfficeAvailBudget");
                    fromAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailBudget");
                    toAvailableAmt = (BigDecimal) r.getAttribute("DivisionAvailBudget");
                    break;
                case "RecoverDivision":
                    availableAmt = (BigDecimal) r.getAttribute("DivisionAvailBudget");
                    toAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailBudget");
                    fromAvailableAmt = (BigDecimal) r.getAttribute("DivisionAvailBudget");
                    break;
                case "TransferTeam":
                    availableAmt = (BigDecimal) r.getAttribute("DivisionAvailBudget");
                    fromAvailableAmt = (BigDecimal) r.getAttribute("DivisionAvailBudget");
                    toAvailableAmt = (BigDecimal) r.getAttribute("TeamAvailBudget");
                    break;
                case "RecoverTeam":
                    availableAmt = (BigDecimal) r.getAttribute("TeamAvailBudget");
                    toAvailableAmt = (BigDecimal) r.getAttribute("DivisionAvailBudget");
                    fromAvailableAmt = (BigDecimal) r.getAttribute("TeamAvailBudget");
                    break;
                }
                if ((TRANSFER_FROM_OFFICE.equalsIgnoreCase(transferLevel) || TRANSFER_FROM_DIVISION.equalsIgnoreCase(transferLevel)|| TRANSFER_FROM_TEAM.equalsIgnoreCase(transferLevel)) && availableAmt != null &&
                    availableAmt.compareTo(BigDecimal.ZERO) == 0) {
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "No available amt",
                                         "No available amount to " +
                                         (RECOVER.equalsIgnoreCase(transferType) ? "recover " : "add") +
                                         ("Transfer".equals(transferType) ?
                                          " to " + transferLevel + " from " + parentLevel + " "+parentName:
                                          " from " + transferLevel + " to " + parentLevel + " "+parentName));
                    FacesContext.getCurrentInstance().addMessage(null, msg);

                }

                if (TRANSFER_FROM_BUREAU.equalsIgnoreCase(transferLevel) && RECOVER.equalsIgnoreCase(transferType) &&
                    availableAmt != null && availableAmt.compareTo(BigDecimal.ZERO) == 0) {
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "No available amt",
                                         "No available amount to recover.");
                    FacesContext.getCurrentInstance().addMessage(null, msg);

                }
                pageFlowMap.put("availableAmt", availableAmt);
                pageFlowMap.put("toAvailableAmt", toAvailableAmt);
                pageFlowMap.put("fromAvailableAmt", fromAvailableAmt);
                if (availableAmt != null)
                    pageFlowMap.put("availableAmtFormat", currencyFormat(availableAmt));
    }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(vce.getComponent().getParent().getParent().getParent().getParent());
    }
    public void divisionCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...
        String value = (String) object;
        if (value != null) {
            value = value.toUpperCase();
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("DivisionActiveCheckLOV1Iterator");
            Key division = new Key(new Object[] { value });
            Row r[] = iter.getRowSetIterator().findByKey(division, 1);
            if (r.length < 1) {
                facesContext.addMessage(uIComponent.getId(),
                                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                         "Invalid Division Code. Use Search to find and select correct Division Code"));
            }

        }


    }

    public void selectDivisionCodeLaunchPopupListener(LaunchPopupEvent launchPopupEvent) {
        // Add event code here...
    }

    public void selectDivisionCodePopupReturnListener(ReturnPopupEvent returnPopupEvent) {
        // Add event code here...
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("DivisionActiveLOV1Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row r = rsi.getCurrentRow();
        if (r != null) {
            pfm.put("division", r.getAttribute("OfdaDivisionCode"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent());
        }
        rsi.closeRowSetIterator();

    }
    
    public void teamCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...
        String value = (String) object;
        if (value != null) {
            value = value.toUpperCase();
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("TeamActiveCheckLOV1Iterator");
            Key division = new Key(new Object[] { value });
            Row r[] = iter.getRowSetIterator().findByKey(division, 1);
            if (r.length < 1) {
                facesContext.addMessage(uIComponent.getId(),
                                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                         "Invalid Team Code. Use Search to find and select correct Team Code"));
            }

        }


    }

    public void selectTeamCodeLaunchPopupListener(LaunchPopupEvent launchPopupEvent) {
        // Add event code here...
    }

    public void selectTeamCodePopupReturnListener(ReturnPopupEvent returnPopupEvent) {
        // Add event code here...
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("TeamActiveLOV1Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row r = rsi.getCurrentRow();
        if (r != null) {
            pfm.put("team", r.getAttribute("OfdaTeamCode"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent());
        }
        rsi.closeRowSetIterator();

    }

    public List<CustomListItem> getFundAcctTypeList() {
        return fundAcctTypeList;
    }
}
