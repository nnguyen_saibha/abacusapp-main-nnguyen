package gov.ofda.abacus.view.bean.action;

import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputListOfValues;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.LaunchPopupEvent;
import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class awardTrackingBean {
    public awardTrackingBean() {
    }

    public void proposalSplLaunchPopupLnsr(LaunchPopupEvent launchPopupEvent) {
        launchPopupLsnr(launchPopupEvent,"PrSpecialist");
    }

    public void proposalSplReturnPopupLsnr(ReturnPopupEvent returnPopupEvent) {
        returnPopupLnsr(returnPopupEvent);
    }
    public void awardSplLaunchPopupLnsr(LaunchPopupEvent launchPopupEvent) {
        launchPopupLsnr(launchPopupEvent,"Specialist");
    }

    public void awardSplReturnPopupLsnr(ReturnPopupEvent returnPopupEvent) {
        returnPopupLnsr(returnPopupEvent);
    }
    private void launchPopupLsnr(LaunchPopupEvent launchPopupEvent, String spl) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        Row r = iter.getCurrentRow();
        if (r != null) {
            OperationBinding exec = bindings.getOperationBinding("setSpecialistLOV");
            exec.getParamsMap().put("p_user_id",r.getAttribute(spl));
            exec.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(launchPopupEvent.getComponent().getParent());
        }
    }
    private void returnPopupLnsr(ReturnPopupEvent returnPopupEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("GUSpecialistWorkLoadEditLOV1Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row r = rsi.getCurrentRow();
        if (r != null) {
            ((RichInputListOfValues)returnPopupEvent.getComponent()).setValue(r.getAttribute("Userid"));
            ((RichInputListOfValues)returnPopupEvent.getComponent()).processUpdates(FacesContext.getCurrentInstance());
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent());
        }
        rsi.closeRowSetIterator();
    }
}
