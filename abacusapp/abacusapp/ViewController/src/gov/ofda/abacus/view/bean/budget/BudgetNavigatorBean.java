package gov.ofda.abacus.view.bean.budget;

import gov.ofda.abacus.view.base.UIControl;

import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.QueryEvent;
import oracle.adf.view.rich.event.QueryOperationEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;

import org.apache.myfaces.trinidad.event.ReturnEvent;

public class BudgetNavigatorBean extends UIControl implements Serializable {
    @SuppressWarnings("compatibility:458506340990238898")
    private static final long serialVersionUID = 1L;
    private static final String TRANSFER = "Transfer";
    private static final String RECOVER = "Recover";
    private static final String TRANSFER_FROM_BUREAU= "Bureau";
    private static final String TRANSFER_FROM_OFFICE = "Office";
    private static final String TRANSFER_FROM_DIVISION = "Division";
    private static final String TRANSFER_FROM_TEAM = "Team";
    private static final String TRANSFER_FROM_PROJECT = "Project";
    public BudgetNavigatorBean() {
        super();
    }

    public void commonQueryListener(QueryEvent queryEvent) {
        // #{bindings.ActivityBudgetViewCriteriaQuery.processQuery}
        invokeEL("#{bindings.ActivityBudgetViewCriteriaQuery.processQuery}", new Class[] { QueryEvent.class }, new Object[] {
                 queryEvent });
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("setOfdaDivisionTeamViewsCriteria");
        exec.execute();
    }

    public void addBudgetReturnLsnr(ReturnEvent returnEvent) {
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) pfp.get("confirmMessage");
        MessageBean mb = new MessageBean();
        mb.sendActivityBudgetTransferEmail(result, (Map) pfp.get("transferDetails"), (Boolean) pfp.get("isSendEmail"));

    }

    public void budgetRecoveryReturnListner(ReturnEvent returnEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) map.get("confirmMessage");
        MessageBean bean = new MessageBean();
        bean.sendBudgetRecoveryEmail(result, (Map) map.get("transferDetails"), (Boolean) map.get("isSendEmail"));
    }

    public String checkAndSetParams() {
        // Add event code here...
        String missingInfo = "";
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String transferType = (String) requestMap.get("transferType");
        String transferLevel = (String) requestMap.get("transferLevel");
        Integer fiscalYear = (Integer) requestMap.get("fiscalYear");
        String bureau = (String) requestMap.get("bureauCode");
        String office = (String) requestMap.get("officeCode");
        String division = (String) requestMap.get("division");
        String team = (String) requestMap.get("team");
        String projectNbr = (String) requestMap.get("projectNbr");
        String isNew = (String) requestMap.get("isNew");
        
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        
        
        // validate transfer or recover type
        if (!((TRANSFER.equals(transferType) || RECOVER.equals(transferType)))) {
            pageFlowMap.put("message",
                    "Unknown Transfer Type: Valid values are 'Transfer' & 'Recover'");
            return "error";
        }
        
        if(null != isNew && isNew.equalsIgnoreCase("true")){
            
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = bindings.getOperationBinding("getp_fy");
            exec.execute();
            requestMap.put("fiscalYear", exec.getResult());
            
            if(TRANSFER_FROM_BUREAU.equalsIgnoreCase(transferLevel)||TRANSFER_FROM_OFFICE.equalsIgnoreCase(transferLevel)|| TRANSFER_FROM_DIVISION.equalsIgnoreCase(transferLevel)||TRANSFER_FROM_TEAM.equalsIgnoreCase(transferLevel)){
                exec = bindings.getOperationBinding("getp_bureauCode");
                exec.execute();
                requestMap.put("bureauCode", exec.getResult());
            }else{
                requestMap.put("bureauCode", null);
            }
            
            if(TRANSFER_FROM_OFFICE.equalsIgnoreCase(transferLevel)||TRANSFER_FROM_DIVISION.equalsIgnoreCase(transferLevel)||TRANSFER_FROM_TEAM.equalsIgnoreCase(transferLevel)){
                exec = bindings.getOperationBinding("getp_officeCode");
                exec.execute();
                requestMap.put("officeCode", exec.getResult());
            }else{
                requestMap.put("officeCode", null);
            }
            
            if(TRANSFER_FROM_DIVISION.equalsIgnoreCase(transferLevel)||TRANSFER_FROM_TEAM.equalsIgnoreCase(transferLevel)){
                exec = bindings.getOperationBinding("getp_div");
                exec.execute();
                requestMap.put("division", exec.getResult());
            }else{
                requestMap.put("division", null);
            }
            if(TRANSFER_FROM_TEAM.equalsIgnoreCase(transferLevel)){
                exec = bindings.getOperationBinding("getp_team");
                exec.execute();
                requestMap.put("team", exec.getResult());
            }else{
                requestMap.put("team", null);
            }
            
            return "new";
        }
        
        // remaining validations
        if (fiscalYear == null)
            missingInfo = "Fiscal Year,";
        switch(transferLevel) {
        case TRANSFER_FROM_BUREAU:
            missingInfo+=bureau != null?"":"Bureau,";
            break;
        case TRANSFER_FROM_OFFICE:
            missingInfo+=bureau != null?"":"Bureau,";
            missingInfo+=office != null?"":"Office,";
            break;
        case TRANSFER_FROM_DIVISION:
            missingInfo+=bureau != null?"":"Bureau,";
            missingInfo+=office != null?"":"Office,";
            missingInfo+=division != null?"":"Division,";
            break;
        case TRANSFER_FROM_TEAM:
                missingInfo+=bureau != null?"":"Bureau,";
                missingInfo+=office != null?"":"Office,";
                missingInfo+=division != null?"":"Division,";
                missingInfo+=team != null?"":"Team,";
                break;    
        }
        if(fiscalYear!=null && (fiscalYear<2001 || fiscalYear>9999)) {
            pageFlowMap.put("message",
                    "Fiscal Year must be between 2001 and 9999");
            return "error";
        }
        if (missingInfo.length() == 0){
            return "next";
        }
        pageFlowMap.put("message",
                 "Select or enter missing required value(s) in Search: " + missingInfo);
        return "back";
    }

    public void setAllViews() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("getCurrentFY");
        exec.execute();
        Object t=exec.getResult();
        exec = bindings.getOperationBinding("ExecuteBureau");
        exec.getParamsMap().put("p_fy", t);
        exec.execute();
        exec = bindings.getOperationBinding("ExecuteOFDA");
        exec.getParamsMap().put("p_fy", t);
        exec.execute();
        exec = bindings.getOperationBinding("ExecuteDivision");
        exec.getParamsMap().put("p_fy", t);
        exec.execute();
        exec = bindings.getOperationBinding("ExecuteTeam");
        exec.getParamsMap().put("p_fy", t);
        exec.execute();
    }
}
