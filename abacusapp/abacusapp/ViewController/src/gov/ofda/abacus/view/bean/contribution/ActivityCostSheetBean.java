package gov.ofda.abacus.view.bean.contribution;

import gov.ofda.abacus.model.abacusapp.type.ContributionAction;
import gov.ofda.abacus.portal.controller.UIControl.GrowlType;
import gov.ofda.abacus.view.base.AbacusConstants;
import gov.ofda.abacus.view.base.JSFUtils;
import gov.ofda.abacus.view.base.UIControl;


import gov.ofda.abacus.view.bean.message.MessageBean;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.layout.RichPanelStretchLayout;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.PopupCanceledEvent;

import oracle.adf.view.rich.event.PopupFetchEvent;
import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;

import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.DBSequence;

import org.apache.myfaces.trinidad.event.ReturnEvent;

public class ActivityCostSheetBean extends UIControl {
    @SuppressWarnings("compatibility:3165101812967795879")
    private static final long serialVersionUID = 1L;
    private RichPopup confirmPopup;
    private int delay=1;
    private String showColumns=null;
    private List<ContributionAction> linkedActionsList=new ArrayList<ContributionAction>();
   

    public ActivityCostSheetBean() {
        super();
    }
    
    public void initCostsheet()
    {  
        invokeMethod("initCostSheet");
        
    }
    
    // Yes/No popup for deleting adjustments
    public void deleteAdjstmentDialogLsnr(DialogEvent dialogEvent) {
        
        if (DialogEvent.Outcome.yes == dialogEvent.getOutcome()) {
            invokeMethod("DeleteAdj");            
        }
    }
   

    public void saveCostsheet(ActionEvent actionEvent) {
        invokeMethod("recalculateCostSheet");
        //showSaveConfirmation("Commit");
        //FacesMessage message =
           // new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully", "");
       // FacesContext.getCurrentInstance().addMessage(null, message);
        addToGrowl(GrowlType.notice,"Saved successfully",0,5000);         
        Map pfp=AdfFacesContext.getCurrentInstance().getPageFlowScope();
         pfp.put("isNew",null);          
    }
    
    public void cancelCostsheet(ActionEvent actionEvent) {     
        addToGrowl(gov.ofda.abacus.view.base.UIControl.GrowlType.notice,"Cancelled successfully",0,5000);        
    }
    
    
    public void showSaveConfirmation(String bindingName){
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding(bindingName);//"Commit"
        
        if(exec.isOperationEnabled()){
            exec.execute();
            if(exec.getErrors().isEmpty()){
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }else{
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "No changes to save", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void deleteActivityCostsheet(DialogEvent dialogEvent) {
        if (DialogEvent.Outcome.yes == dialogEvent.getOutcome()) {
            invokeMethod("Delete");        
            invokeMethod("ExecuteSummaryCS");
        }
    }

    public void deleteCommodityDialogLsnr(DialogEvent dialogEvent) {
        if (DialogEvent.Outcome.yes == dialogEvent.getOutcome()) 
            invokeMethod("DeleteCommodity");    
    }
    
    public void handleStatusChange(ActionEvent actionEvent) {
           Map pf=AdfFacesContext.getCurrentInstance().getPageFlowScope();
           
           Boolean isCommitEnabled=JSFUtils.resolveExpressionAsBoolean("#{bindings.Commit.enabled}");
           if(isCommitEnabled!=null && isCommitEnabled) {
               FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Unsaved changes. Please save changes and try again.", "");
               FacesContext.getCurrentInstance().addMessage(null, message);
               return;
           }
           
           Object buttonName =  pf.get("buttonName") ; 
           RichPopup cp = getConfirmPopup(); 
          
           if(buttonName != null && "Draft".equals(buttonName)){
                    pf.put("confirmMsg", "Cost sheet will be editable again. Do you want to continue?") ; 
                    pf.put("newStatus", new BigDecimal(80)) ;
                    
                    cp.show(new RichPopup.PopupHints());
                    return;
           }
           
           DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
           DCIteratorBinding iter1 = bindings.findIteratorBinding("PaActivitiesCSSummaryViewIterator");
           if( iter1.getCurrentRow() == null){
               FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Contribution status cannot be changed. No Cost Sheets added.", "");
               FacesContext.getCurrentInstance().addMessage(null, message);
               return ; 
           }
         
            
           DCIteratorBinding iter = bindings.findIteratorBinding("PaxActivitiesView1Iterator");
           Row rw = iter.getCurrentRow();
           if (rw != null) {
                if(buttonName != null && "Internal Review".equals(buttonName)){
                    
                    if(rw.getAttribute("ActionId") == null){
                          FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Contribution status cannot be changed. Primary Action for Contribution is not selected.", "");
                              FacesContext.getCurrentInstance().addMessage(null, message);
                              return ; 
                    }
                    
                        pf.put("primaryActionId" ,(BigDecimal) rw.getAttribute("ActionId"));
                        pf.put("confirmMsg", "Once a Contribution Summary is sent for Internal Review, POD can no longer edit it unless changed back to Draft. Do you want to continue?"); 
                        pf.put("newStatus", new BigDecimal(81)) ;
                        cp.show(new RichPopup.PopupHints());
                        return;
                }
           
                if(buttonName != null && "Approved".equals(buttonName)){
                    
                    if(rw.getAttribute("ActionId") == null){
                          FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Contribution status cannot be changed. Primary Action for Contribution is not selected.", "");
                              FacesContext.getCurrentInstance().addMessage(null, message);
                              return ; 
                    }
                    
                    pf.put("primaryActionId" ,(BigDecimal) rw.getAttribute("ActionId")); 
                    
                    Boolean isPaaLinked = (Boolean)rw.getAttribute("IsPrimaryActionLinkedtoPaa");
                    if(!isPaaLinked){
                       FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Contribution status cannot be changed. Primary Action is not linked to an Application.", "");
                           FacesContext.getCurrentInstance().addMessage(null, message);
                           return ; 
                    }
                    
                   pf.put("confirmMsg", "Once a Contribution Summary is marked as Approved, it will now be available for Partners for review. Do you want to continue?") ; 
                   pf.put("newStatus", new BigDecimal(82)) ;
                   cp.show(new RichPopup.PopupHints());
                   return;
                }
                   
                if(buttonName != null && "Actuals Closed".equals(buttonName)){
                    
                    Boolean isObligated = (Boolean)rw.getAttribute("IsPrimaryActionObligated");
                    BigDecimal status=(BigDecimal)rw.getAttribute("CostsheetStatus");
                    if(status!=null && status.compareTo(new BigDecimal(80))==0) {
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Contribution status cannot be changed from Draft to Actuals Closed.", "");
                            FacesContext.getCurrentInstance().addMessage(null, message);
                            return ; 
                    }
                    else if(!isObligated){
                       FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Contribution status cannot be changed. Primary Action for Contribution is not Obligated.", "");
                           FacesContext.getCurrentInstance().addMessage(null, message);
                           return ; 
                     }
                    else
                    {
                    pf.put("confirmMsg", "Once Contribution Summary is Closed, Actuals will be read only and cannot be changed. Do you want to continue?") ; 
                    pf.put("newStatus", new BigDecimal(79)) ;
                    cp.show(new RichPopup.PopupHints());
                    return; 
                    }
                }
           
                if(buttonName != null && "Final".equals(buttonName)){
                    pf.put("confirmMsg", "Contribution Summary will be marked as Final. Do you want to continue?") ; 
                    pf.put("newStatus", new BigDecimal(64)) ;
                    cp.show(new RichPopup.PopupHints());
                    return;
                 }
      
           }
                
       }
       public void setConfirmPopup(RichPopup confirmPopup) {
           this.confirmPopup = confirmPopup;
       }

       public RichPopup getConfirmPopup() {
           return confirmPopup;
       }

       public void handleConfirmDlg(DialogEvent dialogEvent) {
           Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            
           
           if (DialogEvent.Outcome.yes == dialogEvent.getOutcome()) {               
              Boolean result = (Boolean) invokeMethod("changeCostsheetStatus");  
              Object buttonName =  pfm.get("buttonName") ; 
                  String subject = null;
                  String message = null;
                  
              if(result && buttonName != null && "Internal Review".equals(buttonName)) 
              {
                  
                        subject= "Contribution for Internal Review.";
                        message="Contribution Ready for BHA Internal Review" ;
                    
                    MessageBean mb = new MessageBean();
                    Boolean t = mb.sendCostSheetInternalReviewEmail(subject, message, "" + pfm.get("primaryActionId"), pfm.get("paxActivitiesId").toString());
                    
                    if(t) {
                        addToGrowl(GrowlType.mailok,"Contribution Review email sent to internal team.",300*delay++,(1000*(delay*5)));
                    }else
                        addToGrowl(GrowlType.mailerror,"Contribution Review email failed to send to internal team!",300*delay++,(1000*(delay*5)));
              } 
              else if(result && buttonName != null && "Approved".equals(buttonName)){  
                    subject= "Contribution Ready for Review.";
                    message="A Contribution Summary and Cost Sheet(s) are available in the BHA Application and Award Management Portal (AAMP) for your review. Please follow this link to log in and validate or request adjustments." ;
                    
                    MessageBean mb = new MessageBean();
                    Boolean t = mb.sendCostSheetApprovedEmail(subject, message, "" + pfm.get("primaryActionId"));
                    
                    if(t) {
                    addToGrowl(GrowlType.mailok,"Contribution Ready for Review email sent to Applicant.",300*delay++,(1000*(delay*5)));
                    }else
                    addToGrowl(GrowlType.mailerror,"Contribution Ready for Review email failed to send to Applicant!",300*delay++,(1000*(delay*5)));
                     
              }
              if(result)  {
                  DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                  OperationBinding exec = bindings.getOperationBinding("Commit");
                  exec.execute();
                  
                  addToGrowl(GrowlType.notice,"Status successfully changed",0,5000);
              }
              else 
                    addToGrowl(GrowlType.error,"Unable to change status. Please try again or contact support team.",0,5000);
           
              
               pfm.put("buttonName" , null);
               pfm.put("newStatus" , null);
               pfm.put("confirmMsg" , null);  
               refreshSummary();
           }
       }

    public void newCSReturnLsnr(ReturnEvent returnEvent) {
        refreshSummary();        
    }
    
    private void refreshSummary(){      
        invokeMethod("ExecuteSummaryCS");
        invokeMethod("ExecutePAX");
        invokeMethod("ExecutePAEditView");
    }

    public void changeStatusCancelLsnr(PopupCanceledEvent popupCanceledEvent) {
        refreshSummary();        
    }

    public void refreshActivityCostSheet(ReturnEvent returnEvent) {
        invokeMethod("Execute");
    }
    
    public void setShowColumns(String showColumns) {
        this.showColumns = showColumns;
    }

    public String getShowColumns() {
        if(showColumns==null) {
            Boolean isPrimaryActionObligated=JSFUtils.resolveExpressionAsBoolean("#{bindings.IsPrimaryActionObligated.inputValue}");
            if(isPrimaryActionObligated!= null && isPrimaryActionObligated) {
                showColumns="BestValues";
            }
            else
            showColumns="Estimates";
        }
        return showColumns;
    }
    
    public void showColumnsActionListener(ActionEvent ae) {
        UIComponent uic = ae.getComponent();
        showColumns = (String) (uic.getAttributes()).get("showColumn");
        AdfFacesContext.getCurrentInstance().addPartialTarget(uic.getParent());
    }

    public void handlePrimaryActionChange(ReturnPopupEvent returnPopupEvent) {
        invokeMethod("ExecutePAEditView");
    }
    
    
    /**
     * When user changes the Region code, this method sets the country code to null,
     * so that user can pick respective Country code based on the Region.
     * @param valueChangeEvent
     */
    public void regionValueChangeListner(ValueChangeEvent valueChangeEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding activityIter = bindings.findIteratorBinding("PaxActivitiesView1Iterator");
        Row row = activityIter.getCurrentRow();
        row.setAttribute(AbacusConstants.COUNTRY_CODE, "");
    }

    public void unlinkActionDialogLsnr(DialogEvent de) {
        if (DialogEvent.Outcome.yes == de.getOutcome()) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView2Iterator");
            Row r = iter.getCurrentRow();
            r.setAttribute("IsCostsheetApplicable","N");
            r.setAttribute("PaxActivitiesId",null);
            r.setAttribute("ContributionLinktypeId",null);
            invokeMethod("ExecutePAEditView");
        }
    }
    /**
     * @return
     * Rollback all changes.
     * Set initial values after rollback.
     */
    public String cancelChanges() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Rollback");
        exec.execute();
        if (exec.getErrors().isEmpty())
        {
            addToGrowl(gov.ofda.abacus.view.base.UIControl.GrowlType.notice,"Cancelled changes",0,5000);
            return "Rollback";
        }
        else
            addToGrowl(gov.ofda.abacus.view.base.UIControl.GrowlType.error,"Error cancelling changes",0,5000);
        return "back";
    }

    public void saveChanges() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Boolean isCommitEnabled = (Boolean) JSFUtils.resolveExpression("#{bindings.Commit.enabled}");
        if (!(isCommitEnabled)) {
            addToGrowl(gov.ofda.abacus.view.base.UIControl.GrowlType.notice, "No changes to save", 0, 5000);
            return;
        }
        OperationBinding exec = bindings.getOperationBinding("Commit");
        exec.execute();
        if (exec.getErrors().isEmpty())
            addToGrowl(gov.ofda.abacus.view.base.UIControl.GrowlType.notice, "Saved successfully", 0, 5000);
        else
            addToGrowl(gov.ofda.abacus.view.base.UIControl.GrowlType.error,
                       "Unable to save changes. Cancel your changes and try again", 0, 5000);
    }

    public void linkActionDialogLnsr(DialogEvent de) {
        // PAEditViewExecuteByActionId
        if (DialogEvent.Outcome.ok == de.getOutcome()) {
            BigDecimal paxActivitiesId =
                ((DBSequence) JSFUtils.resolveExpression("#{bindings.PaxActivitiesId.inputValue}")).getSequenceNumber().bigDecimalValue();
            Map pfm=AdfFacesContext.getCurrentInstance().getPageFlowScope();
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("PAContributionNotLinkedViewIterator");
            Row r = iter.getCurrentRow();
            if(r!=null)
            {
                    JSFUtils.setExpressionValue("#{bindings.IsCostsheetApplicable.inputValue}","Y");
                    JSFUtils.setExpressionValue("#{bindings.PaxActivitiesId1.inputValue}",paxActivitiesId);
                    invokeMethod("ExecutePAEditView");
                invokeMethod("ExecutePAContributionNotLinkedView");
                    AdfFacesContext.getCurrentInstance().addPartialTarget(de.getComponent().getParent().getParent().getParent());
            }
        }
    }

    public void actionLinkUnlinkPopupFetchLsnr(PopupFetchEvent popupFetchEvent) {
        invokeMethod("ExecuteContributionUnlinkedActionsView");
        this.linkedActionsList =(List<ContributionAction>) invokeMethod("getContributionLinkedActionsList");
    }

    public void actionLinkUnlinkPopupCanceledLsnr(PopupCanceledEvent popupCanceledEvent) {
        // Add event code here...
    }

    public void actionLinkUnlinkDialogLsnr(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            invokeMethod("updateContributionLinkedActions");
            NavigationHandler  nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            if(nvHndlr!=null)
                nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "refresh");
        }
    }

    public void setLinkedActionsList(List<ContributionAction> linkedActionsList) {
        this.linkedActionsList = linkedActionsList;
    }

    public List<ContributionAction> getLinkedActionsList() {
        return linkedActionsList;
    }

    public void linkUnlinkActionLnsr(ActionEvent actionEvent) {
        BigDecimal actionId = (BigDecimal) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("actionId");
        String actionName = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("actionName");
        ContributionAction a=new ContributionAction(actionId,new BigDecimal(1),actionName);
        if (!linkedActionsList.contains(a)) {
            linkedActionsList.add(a);
        } else {
            linkedActionsList.remove(a);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(actionEvent.getComponent().getParent().getParent().getParent());
    }
}
