package gov.ofda.abacus.view.bean.activity;


import gov.ofda.abacus.view.base.AbacusConstants;
import gov.ofda.abacus.view.base.UIControl;
import gov.ofda.abacus.view.bean.message.MessageBean;
import gov.ofda.uishell.TabContext;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;
import oracle.adf.view.rich.event.QueryEvent;
import oracle.adf.view.rich.event.ReturnPopupEvent;
import oracle.adf.view.rich.model.AttributeCriterion;
import oracle.adf.view.rich.model.AttributeDescriptor;
import oracle.adf.view.rich.model.ConjunctionCriterion;
import oracle.adf.view.rich.model.Criterion;
import oracle.adf.view.rich.model.FilterableQueryDescriptor;
import oracle.adf.view.rich.model.QueryDescriptor;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;

import org.apache.myfaces.trinidad.change.RowKeySetAttributeChange;
import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.event.ReturnEvent;
import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.RowKeySetImpl;

public class ActivityNavigatorBean extends UIControl {
    @SuppressWarnings("compatibility:4350360040825153808")
    private static final long serialVersionUID = 1L;

    public ActivityNavigatorBean() {
        super();
    }

    public void newProjectDialogLnsr(DialogEvent de) {
        if (DialogEvent.Outcome.ok == de.getOutcome()) {
            Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();

            /*DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("NewProjectListView1Iterator");
            Row r = iter.getCurrentRow();
            if (r != null) {
                pageFlowMap.put("projectNbr", r.getAttribute("ProjectNbr"));
                pageFlowMap.put("projectName", r.getAttribute("ProjectName"));
            }*/
            Object projNbr = pageFlowMap.get("projectNbr");
            if (null == projNbr) {

                /* FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                                     "Oops!! something went wrong!! Will get back to you shortly!!!", "");
                FacesContext.getCurrentInstance().addMessage(null, message);*/
            } else {
                Map map = new HashMap<String, Object>();
                String id = "" + pageFlowMap.get("projectFY") + "-" + projNbr;
                map.put("budget_year", pageFlowMap.get("projectFY"));
                map.put("project_number", projNbr);
                map.put("isNew", true);
                map.put("isEdit", true);
                // now launch the tab
                _launchActivity(id, " New Project " + id,
                                "/WEB-INF/tflows/activity/view-activity-flow.xml#view-project-flow", false, map);
            }
        }
    }

    public void newProjectPopupFetchLsnr(PopupFetchEvent popupFetchEvent) {
        RichTable rt = null;
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        if (pageFlowMap.get("currentFY") == null) {
            OperationBinding exec = bindings.getOperationBinding("getCurrentFY");
            exec.execute();
            Integer fy = (Integer) exec.getResult();
            pageFlowMap.put("currentFY", fy);
        }
        rt = (RichTable) pageFlowMap.get("newProjectTable");
        if(rt != null){
            rt.getSelectedRowKeys().clear();
            RowKeySetAttributeChange rks =    new RowKeySetAttributeChange
                   (rt.getClientId(), 
                       "selectedRowKeys", new RowKeySetImpl());
               RequestContext.getCurrentInstance().getChangeManager().
                   addComponentChange(FacesContext.getCurrentInstance(),rt, rks);
        }
        pageFlowMap.put("projectFY", pageFlowMap.get("currentFY"));
        pageFlowMap.put("projectNbr", null);
        pageFlowMap.put("projectName", null);
        pageFlowMap.put("divisionCode", null);
        pageFlowMap.put("teamCode", null);
        pageFlowMap.put("BureauCode", null);
        pageFlowMap.put("OfficeCode", null);
        pageFlowMap.put("BureauCode1", null);
        pageFlowMap.put("OfficeCode1", null);
        pageFlowMap.put("OfdaDivisionCode1", null);
        pageFlowMap.put("OfdaTeamCode1", null);
        updateProjectAvailList((Integer) pageFlowMap.get("currentFY"));
        //ExecuteProjectList
    }

    public void updateProjectAvailList(Object fy) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("updateNewProjectList");
        exec.getParamsMap().put("fy", fy);
        exec.execute();

    }

    public void newProjectFYChgLnsr(ValueChangeEvent vce) {
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("projectFY", vce.getNewValue());
        updateProjectAvailList(((BigDecimal) vce.getNewValue()).longValue());
        //updateProjectAvailList((Long)vce.getNewValue());
    }

    private void _launchActivity(String id, String title, String taskflowId, boolean newTab, Map map) {

        try {
            if (newTab) {
                TabContext.getCurrentInstance().addTab(id, title, taskflowId, map);
            } else {
                TabContext.getCurrentInstance().addOrSelectTab(id, title, taskflowId, map);
            }
        } catch (TabContext.TabOverflowException toe) {
            // causes a dialog to be displayed to the user saying that there are
            // too many tabs open - the new tab will not be opened...
            toe.handleDefault();
        }
    }

    private void deleteActivity() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("deleteActivityCheck");
        Boolean isDelete = (Boolean) exec.execute();
        if (isDelete) {
            exec = bindings.getOperationBinding("deleteActivity");
            String result = (String) exec.execute();
            if ("true".equals(result)) {
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Project Deleted Successfully", null));
                exec = bindings.getOperationBinding("Execute");
                exec.execute();
            } else {
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, result, null));
            }
        } else {
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null,
                          new FacesMessage(FacesMessage.SEVERITY_INFO,
                                           "Project has Budget/Allocated amount, cannot be deleted.", null));
        }
    }

    public void activityDeleteDialogLsnr(DialogEvent de) {
        if (DialogEvent.Outcome.yes == de.getOutcome()) {
            deleteActivity();
        }
    }

    public List<SelectItem> getProjectNameList(String name) {
        List<SelectItem> list = new ArrayList<SelectItem>();
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        Object fy = pageFlowMap.get("projectFY");
        Object division = pageFlowMap.get("divisionCode");
        Object team = pageFlowMap.get("teamCode");

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        //DCIteratorBinding iter = bindings.findIteratorBinding("NewProjectListView1Iterator");
        OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.getParamsMap().put("p_fy", fy);
        exec.getParamsMap().put("p_projName", name);
        if (null != division) {
            exec.getParamsMap().put("p_divisionCode", division);
        }
        if (null != team) {
            exec.getParamsMap().put("p_teamCode", team);
        }
        exec.execute();

        DCIteratorBinding iter = bindings.findIteratorBinding("NewProjectListView1Iterator");
        ViewObject view = iter.getViewObject();
        RowSetIterator iterator = view.createRowSetIterator(null);

        while (iterator.hasNext()) {
            Row row = iterator.next();
            list.add(new SelectItem(row.getAttribute("ProjectName")));
        }
        return list;
    }

    public void validateProjectName(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            String projectName = object.toString();
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            // reset the view criteria
            OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
            Object fy = AdfFacesContext.getCurrentInstance().getPageFlowScope().get("key");
            exec.getParamsMap().put("p_fy", fy);
            exec.execute();
            // Iterate trhough projects
            DCIteratorBinding iter = bindings.findIteratorBinding("NewProjectListView1Iterator");
            ViewObject view = iter.getViewObject();
            RowSetIterator iterator = view.createRowSetIterator(null);

            boolean validate = false;
            while (iterator.hasNext()) {
                Row row = iterator.next();
                if (projectName.equalsIgnoreCase(String.valueOf(row.getAttribute("ProjectName"))) ||
                    projectName.equalsIgnoreCase(String.valueOf(row.getAttribute("ProjectNbr")))) {
                    AdfFacesContext.getCurrentInstance().getPageFlowScope().put("projectNbr",
                                                                                row.getAttribute("ProjectNbr"));
                    validate = true;
                    break;
                }
            }
            if (!validate) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                              "Please enter a valid Project Name or Number"));
            }
        }
    }

    private boolean isNumeric(Object object) {

        try {
            int i = Integer.parseInt(String.valueOf(object));
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public void newProjectDivisionChangeListner(ValueChangeEvent valueChangeEvent) {

        String divisionCode = (String) valueChangeEvent.getNewValue();
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        map.put("divisionCode", divisionCode);
        Object fy = map.get("projectFY");
        //Object projectNbr = map.get("projectNbr");
        Object teamCode = map.get("teamCode");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.getParamsMap().put("p_fy", fy);
        if (divisionCode != null) {
            exec.getParamsMap().put("p_divisionCode", divisionCode);
        }
        if (teamCode != null) {
            exec.getParamsMap().put("p_teamCode", teamCode);
        }
        exec.execute();

    }

    public void newProjectTeamChangeListner(ValueChangeEvent valueChangeEvent) {
        String teamCode = (String) valueChangeEvent.getNewValue();
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        map.put("teamCode", teamCode);
        Object fy = map.get("projectFY");
        //Object projectNbr = map.get("projectNbr");
        Object divisionCode = map.get("divisionCode");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.getParamsMap().put("p_fy", fy);
        if (divisionCode != null) {
            exec.getParamsMap().put("p_divisionCode", divisionCode);
        }
        if (teamCode != null) {
            exec.getParamsMap().put("p_teamCode", teamCode);
        }
        exec.execute();
    }
    
    public void newProjectBureauChangeListner(ValueChangeEvent valueChangeEvent) {
        String bureauCode = (String) valueChangeEvent.getNewValue();
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        map.put("bureauCode", bureauCode);
        Object fy = map.get("projectFY");
        //Object projectNbr = map.get("projectNbr");
        Object divisionCode = map.get("bureauCode");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.getParamsMap().put("p_fy", fy);
        if (bureauCode != null) {
            exec.getParamsMap().put("p_bureauCode", bureauCode);
        }
       
        exec.execute();
    }
    
    public void newProjectOfficeChangeListner(ValueChangeEvent valueChangeEvent) {
        String officeCode = (String) valueChangeEvent.getNewValue();
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        map.put("officeCode", officeCode);
        Object fy = map.get("projectFY");
        //Object projectNbr = map.get("projectNbr");
        Object divisionCode = map.get("officeCode");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.getParamsMap().put("p_fy", fy);
        if (officeCode != null) {
            exec.getParamsMap().put("p_officeCode", officeCode);
        }
       
        exec.execute();
    }

    public void paTableActionsQueryListnerHandler(QueryEvent queryEvent) {
        // Add event code here...  #{bindings.PATableActionsView2_1Query.processQuery}
        // #{bindings.PATableActionsView2_1Query.processQuery}

        ArrayList<Object> departmentIdArray = null;
        FilterableQueryDescriptor fqd = (FilterableQueryDescriptor) queryEvent.getDescriptor();

        //current criteria
        Map _criteriaMap = fqd.getFilterCriteria();
        StringBuffer deptIdFilterString = new StringBuffer();
        if (_criteriaMap.get("ActionAmtStatus") != null) {
            Object obj = _criteriaMap.get("ActionAmtStatus");
            String query = "";
            if (obj instanceof Integer) {
                query = "action_amt_status != " + obj;
                DCBindingContainer bindings =
                    (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding exec = bindings.getOperationBinding("filterByActionAmtStatus");
                exec.getParamsMap().put("whereClause", query);
                exec.execute();
            } else if (obj instanceof ArrayList) {
                ArrayList<Integer> list = (ArrayList<Integer>) obj;
                if (list != null && list.size() == 1) {
                    query = "action_amt_status = " + list.get(0);
                } else if (list != null && list.size() > 1) {
                    StringBuffer buffer = new StringBuffer();
                    for (int index = 0; index < list.size(); index++) {
                        if (index == 0) {
                            buffer.append(list.get(index));
                        } else {
                            buffer.append(", ");
                            buffer.append(list.get(index));
                        }
                    }
                    query = "action_amt_status IN (" + buffer.toString() + " )";
                }
                DCBindingContainer bindings =
                    (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding exec = bindings.getOperationBinding("filterByActionAmtStatus");
                exec.getParamsMap().put("whereClause", query);
                exec.execute();
            }


        }

        FacesContext fctx = FacesContext.getCurrentInstance();
        Application application = fctx.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elctx = fctx.getELContext();

        MethodExpression methodExpression =
            expressionFactory.createMethodExpression(elctx, "#{bindings.PATableActionsView2_1Query.processQuery}",
                                                     Object.class, new Class[] { QueryEvent.class });
        methodExpression.invoke(elctx, new Object[] { queryEvent });
    }

    public void addBudgetReturnLsnr(ReturnEvent returnEvent) {
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) pfp.get("confirmMessage");
        MessageBean mb = new MessageBean();
        mb.sendActivityBudgetTransferEmail(result, (Map) pfp.get("transferDetails"), (Boolean) pfp.get("isSendEmail"));

    }

    public void addFundsReturnLsnr(ReturnEvent returnEvent) {
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) pfp.get("confirmMessage");
        MessageBean mb = new MessageBean();
        mb.sendActivityFundTransferEmail(result, (Map) pfp.get("transferDetails"), (Boolean) pfp.get("isSendEmail"));
    }

    public void budgetRecoveryReturnListner(ReturnEvent returnEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) map.get("confirmMessage");
        MessageBean bean = new MessageBean();
        bean.sendBudgetRecoveryEmail(result, (Map)map.get("transferDetails"), (Boolean)map.get("isSendEmail"));
    }
    
    public void newProjectSelectRtnLsnr(ReturnPopupEvent returnPopupEvent) {
        Integer budgetFy = null;
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("NewProjectListView1Iterator");
        Row r = iter.getCurrentRow();
        if (r != null) {
            pfp.put("projectNbr", r.getAttribute("ProjectNbr"));
            pfp.put("projectName", r.getAttribute("ProjectName"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent().getParent());
        }
    }

    /**
     * Validates whether BudgetFy is greater than current fY
     * http://www.oracle.com/technetwork/developer-tools/adf/learnmore/85-querycomponent-fieldvalidation-427197.pdf
     * @param queryEvent
     */
    public void OnCreateProjectSearchQuery(QueryEvent queryEvent) {
        QueryDescriptor qdesc = (QueryDescriptor) queryEvent.getDescriptor();
        ConjunctionCriterion conCrit = qdesc.getConjunctionCriterion();
        //access the list of search fields
        List<Criterion> criterionList = conCrit.getCriterionList();
        RichTable rt = null;
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        OperationBinding exec = bindings.getOperationBinding("getCurrentFY");
        exec.execute();
        Integer currentFy = (Integer) exec.getResult();
        Integer budgetFy = null;
        if (currentFy != null && currentFy > 0) {
            //iterate over the attributes to find budgetFy
            for (Criterion criterion : criterionList) {
                AttributeDescriptor attrDescriptor = ((AttributeCriterion) criterion).getAttribute();
                if (attrDescriptor.getName().equalsIgnoreCase("BudgetFy")) {
                    budgetFy = (Integer) ((AttributeCriterion) criterion).getValues().get(0);

                }
            }
            if (budgetFy != null && budgetFy >= currentFy) {
               /* pageFlowMap.put("projectFY", budgetFy);
                pageFlowMap.put("projectNbr", null);
                pageFlowMap.put("projectName", null);
                pageFlowMap.put("OfdaDivisionCode1", null);
                pageFlowMap.put("OfdaTeamCode1", null);
                rt = (RichTable) pageFlowMap.get("newProjectTable");
                if(rt != null){
                    rt.getSelectedRowKeys().clear();
                    RowKeySetAttributeChange rks =    new RowKeySetAttributeChange
                           (rt.getClientId(), 
                               "selectedRowKeys", new RowKeySetImpl());
                       RequestContext.getCurrentInstance().getChangeManager().
                           addComponentChange(FacesContext.getCurrentInstance(),rt, rks);
                }*/
                invokeMethodExpression("#{bindings.fetchProjectListQuery1.processQuery}", queryEvent);
            } else {
                FacesContext fctx = FacesContext.getCurrentInstance();
                fctx.addMessage("q1",
                                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                 "Invalid Fiscal Year: Projects can not be created for previous fiscal year",
                                                 "Invalid Fiscal Year: Projects can not be created for previous fiscal year"));
                fctx.renderResponse();

            }

        }


    }

    //helper method to execute the QueryListener EL
    private void invokeMethodExpression(String expr, QueryEvent queryEvent) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ELContext elContext = fctx.getELContext();
        ExpressionFactory eFactory = fctx.getApplication().getExpressionFactory();
        MethodExpression mexpr = eFactory.createMethodExpression(elContext, expr, Object.class, new Class[] {
                                                                 QueryEvent.class });
        mexpr.invoke(elContext, new Object[] { queryEvent });
    }

    public void createSelectionLsnr(SelectionEvent selectionEvent) {
        invokeEL("#{bindings.NewProjectListView1.collectionModel.makeCurrent}", new Class[] { SelectionEvent.class }, new Object[] {
                 selectionEvent });
        Map<String, Object> pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("NewProjectListView1Iterator");
        Row r = iter.getCurrentRow();
        if (r != null) {
            pageFlowMap.put("projectFY", r.getAttribute("BudgetFy"));
            pageFlowMap.put("projectNbr", r.getAttribute("ProjectNbr"));
            pageFlowMap.put("projectName", r.getAttribute("ProjectName"));
            pageFlowMap.put("OfdaDivisionCode1", r.getAttribute("OfdaDivisionCode1"));
            pageFlowMap.put("OfdaTeamCode1", r.getAttribute("OfdaTeamCode1"));
            pageFlowMap.put("BureauCode1", r.getAttribute("BureauCode1"));
            pageFlowMap.put("OfficeCode1", r.getAttribute("OfficeCode1"));
        }
    }

    public void createProjectQueryLsnr(QueryEvent queryEvent) {
        invokeEL("#{bindings.fetchProjectListQuery1.processQuery}", new Class[] { QueryEvent.class }, new Object[] {
                 queryEvent });
        Map<String, Object> pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pageFlowMap.put("projectFY",null);
        pageFlowMap.put("projectNbr", null);
        pageFlowMap.put("projectName", null);
        pageFlowMap.put("OfdaDivisionCode1", null);
        pageFlowMap.put("OfdaTeamCode1", null);

    }
    public void setDefaultColumnView() {
        Map<String,Object> pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
        if (sc.isUserInRole("APP_FM") || sc.isUserInRole("APP_FINANCE"))
            pageFlowMap.put("showColumns", "Funds");
        else if (sc.isUserInRole("APP_BBL") || sc.isUserInRole("APP_OL") || sc.isUserInRole("APP_DL") || sc.isUserInRole("APP_TL"))
            pageFlowMap.put("showColumns", "Budget");
        else
            pageFlowMap.put("showColumns", "Amounts");
    }

    public String refreshView() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("ExecuteActivity");
        exec.execute();
        exec = bindings.getOperationBinding("ExecuteBudgetView");
        exec.execute();
        exec = bindings.getOperationBinding("ExecuteFundsView");
        exec.execute();
        exec = bindings.getOperationBinding("ExecuteActionsView");
        exec.execute();
        return null;
    }

    /**
     * This method continue functionality of send email or error message once user recover funds.
     * @param returnEvent
     */
    public void recoverFundsListner(ReturnEvent returnEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        MessageBean messageBean = new MessageBean();
        messageBean.sendFundRecoveryEmail((String)map.get(AbacusConstants.CONFIRM_MESSAGE), (Map)map.get(AbacusConstants.TRANSFER_DETAILS), (Boolean)map.get(AbacusConstants.SEND_EMAIL));
    }

    public void addNewProjectLookupReturnListner(ReturnEvent returnEvent) {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String projectNbr = (String) pageFlowMap.get("lookupProjectNbr");
        pageFlowMap.put("projectNbr", projectNbr);
        pageFlowMap.put("OfdaDivisionCode1", pageFlowMap.get("lookupDivision"));
        pageFlowMap.put("OfdaTeamCode1", pageFlowMap.get("lookupTeam"));
        pageFlowMap.put("projectName", pageFlowMap.get("lookupProjectName"));
    }
}
