package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.model.abacusapp.entity.doc.Source;
import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.event.ReturnEvent;

public class ActionToolbar implements Serializable {
	@SuppressWarnings("compatibility:5706431462910894863")
    private static final long serialVersionUID = 1L;

    private List<String> resultDocIds;
    
    private List<Source> sourceIdList;
    private RichPopup docUploadPopup;
    private RichPopup uploadDocsPopup;
    private RichPopup viewDocumentsPopup;
    private RichPopup viewAwardDocsPopup;
    private RichPopup viewAwardDetailPopup1;
    private RichPopup awardDetailsPopup2;

    public ActionToolbar() {
    }

    public void returnLsnr(ReturnEvent returnEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("publishFormCloseEvent");
        exec.execute();
        exec = bindings.getOperationBinding("Execute");
        exec.execute();
    }

public void attachToSource(ActionEvent actionEvent) {
        if(sourceIdList == null){
            sourceIdList = new ArrayList<Source>();            
        }
        Integer sourceType = null;
        String sourceTypeStr= "1";
        //sourceTypeStr = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("sourceType");
        sourceType = new Integer(sourceTypeStr);
        if(sourceType == 1){
            BigDecimal actionId = (BigDecimal) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("actionID");
            Source source = new Source();
            source.setActionId(String.valueOf(actionId));
            sourceIdList.add(source);
        }
        
        if(sourceType == 3){
            String awardId = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("awardId");
            String awardSeq = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("awardSeq");
            Source source = new Source();
            source.setAwardId(awardId);
            source.setAwardSeq(Integer.parseInt(awardSeq));
            sourceIdList.add(source);
        }
        
        if(sourceType == 2){
            String projectNbr = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("projectNbr");
            String budgetFy = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("budgetFy");
            Source source = new Source();
            source.setProjectNbr(projectNbr);
            source.setBudgetFy(budgetFy);
            sourceIdList.add(source);
        }
        
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding executeWithParams1 = bindings.getOperationBinding("Commit");
        executeWithParams1.execute();
        OperationBinding fileAttachExecute = bindings.getOperationBinding("attachDocsToSource");
        fileAttachExecute.getParamsMap().put("sourceType", 1);
        fileAttachExecute.getParamsMap().put("SourceList", sourceIdList);
        fileAttachExecute.getParamsMap().put("docIdsList", resultDocIds);
        fileAttachExecute.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(actionEvent.getComponent().getParent());

        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("timeVar", System.currentTimeMillis());
    }

    public void setUploadDocsPopup(RichPopup uploadDocsPopup) {
        this.uploadDocsPopup = uploadDocsPopup;
    }

    public RichPopup getUploadDocsPopup() {
        return uploadDocsPopup;
    }

    public void cancelDocsPopup(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding executeWithParams1 = bindings.getOperationBinding("Rollback");
        executeWithParams1.execute();
        RichPopup popup = this.uploadDocsPopup;
        popup.hide();
        // Add event code here...
    }
    
    public void addBudgetReturnLsnr(ReturnEvent returnEvent) {
        Map pfp=AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) pfp.get("confirmMessage");
        MessageBean mb=new MessageBean();
        mb.sendActivityBudgetTransferEmail(result, (Map) pfp.get("transferDetails"),(Boolean) pfp.get("isSendEmail"));

    }
    public void addFundsReturnLsnr(ReturnEvent returnEvent) {
        Map pfp=AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) pfp.get("confirmMessage");
        MessageBean mb=new MessageBean();
        mb.sendActivityFundTransferEmail(result, (Map) pfp.get("transferDetails"),(Boolean) pfp.get("isSendEmail"));
    }
    
    public void budgetRecoveryReturnListner(ReturnEvent returnEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) map.get("confirmMessage");
        MessageBean bean = new MessageBean();
        bean.sendBudgetRecoveryEmail(result, (Map)map.get("transferDetails"), (Boolean)map.get("isSendEmail"));
    }

    public void setViewDocumentsPopup(RichPopup viewDocumentsPopup) {
        this.viewDocumentsPopup = viewDocumentsPopup;
    }

    public RichPopup getViewDocumentsPopup() {
        return viewDocumentsPopup;
    }

    public void closeViewDocumentsPopupListner(ActionEvent actionEvent) {
        RichPopup popUp = this.viewDocumentsPopup;
        popUp.hide();
    }

    public void setViewAwardDocsPopup(RichPopup viewAwardDocsPopup) {
        this.viewAwardDocsPopup = viewAwardDocsPopup;
    }

    public RichPopup getViewAwardDocsPopup() {
        return viewAwardDocsPopup;
    }

    public void closeAwardDocsPopupListner(ActionEvent actionEvent) {
        RichPopup popUp = this.viewAwardDocsPopup;
        popUp.hide();
    }

    public void setViewAwardDetailPopup1(RichPopup viewAwardDetailPopup1) {
        this.viewAwardDetailPopup1 = viewAwardDetailPopup1;
    }

    public RichPopup getViewAwardDetailPopup1() {
        return viewAwardDetailPopup1;
    }

    public void closeAwardDetailsPopup1Listner(ActionEvent actionEvent) {
        RichPopup popUp = this.viewAwardDetailPopup1;
        popUp.hide();
    }

    public void setAwardDetailsPopup2(RichPopup awardDetailsPopup2) {
        this.awardDetailsPopup2 = awardDetailsPopup2;
    }

    public RichPopup getAwardDetailsPopup2() {
        return awardDetailsPopup2;
    }

    public void closeAwardDetailPopup2Listner(ActionEvent actionEvent) {
        RichPopup popUp = this.awardDetailsPopup2;
        popUp.hide();
    }
}
