package gov.ofda.abacus.view.base;


public final class AbacusConstants {
    
    // private constructor
    private AbacusConstants(){
        
    }
    
    public static final String BUREAU = "Bureau";
    public static final String OFFICE = "Office";
    public static final String DIVISION = "Division";
    public static final String TEAM = "Team";
    public static final String RECOVER_TO = "to";
    public static final String FUND_CODE = "fundCode";
    public static final String RECOVER_AMT = "recoverAmt";
    public static final String FUNDS_AVAILABLE = "fundsAval";
    public static final String SEND_EMAIL = "isSendEmail";
    public static final String CONFIRM_MESSAGE = "confirmMessage";
    public static final String BUREAU_CODE = "BureauCode";
    public static final String OFFICE_CODE = "OfficeCode";
    public static final String OFDA_DIVISION_CODE = "OfdaDivisionCode";
    public static final String OFDA_TEAM_CODE = "OfdaTeamCode";
    public static final String PROJECT_NAME = "ProjectName";
    public static final String PROJECT_NUMBER = "projectNbr";
    public static final String COMMENTS = "comments";
    public static final String BUDGET_FY = "budgetFY";
    public static final String TRANSFER_DETAILS = "transferDetails";
    public static final String APP_BEAN = "appBean";
    public static final String RECOVER_BY = "recoverBy";
    public static final String EMAIL_LIST = "emailList";
    public static final String REGION_CODE = "RegionCode";
    public static final String COUNTRY_CODE ="CountryCode";

}
