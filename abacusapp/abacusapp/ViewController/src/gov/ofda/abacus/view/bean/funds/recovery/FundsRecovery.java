package gov.ofda.abacus.view.bean.funds.recovery;

import gov.ofda.abacus.view.base.AbacusConstants;
import gov.ofda.abacus.view.base.UIControl;
import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;

import java.math.BigDecimal;

import java.text.NumberFormat;

import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.ReturnPopupEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class FundsRecovery extends UIControl implements Serializable{
    @SuppressWarnings("compatibility:-1313272863778099908")
    private static final long serialVersionUID = 1L;
    
    private static final String AVAL_FUND_FROM_PROJ_TO_DIV = "avalFundFromProjToDiv";
    private static final String AVAL_FUND_FROM_PROJ_TO_DIV_FMT = "avalFundFromProjToDiv_fmt";
    private static final String FUNDS_REQUIRED="fundsRequired";
    private static final String PROJECT_BUDGET="projectBudget";
    private static final String PROJECT_FUNDS_AVAILABLE="projectAvailFunds";
    private static final String BY_ALL_AVAL_RECOVERABLE_AMT = "byAllAvailAmt";
    private static final String BY_FUND_CODE = "byFundCode";


    /**
     * This method initializes the Funds recovery page fields,
     * example set the project, retrieve the available recoverable amount from project to division .. etc.
     */
    public String initializeFundsRecovery() {
        
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        
        // get pageflow map
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        OperationBinding binding = bindingContainer.getOperationBinding("isVActivityAmtValid");
        Boolean isValid = (Boolean) binding.execute();
        if(!isValid) {
            pageFlowMap.put("confirmMessage", "Error initializing funds recovery "+" FY:"+pageFlowMap.get("budgetFY")+" PNbr:"+pageFlowMap.get("projectNbr"));
            return "close";
        }
        
        // set Activity Edit view
        binding = bindingContainer.getOperationBinding("ExecuteWithParams");
        binding.execute();
        
        // set available recoverable view
        binding = bindingContainer.getOperationBinding("setVFundsRecoverrableView");
        binding.execute();
        
        
        // get available recoverable amount
        binding = bindingContainer.getOperationBinding("getAvailRecoverableFromProjToDiv");
        binding.execute();
        BigDecimal avalFundFromProjToDiv = (BigDecimal) binding.getResult();
        pageFlowMap.put(AVAL_FUND_FROM_PROJ_TO_DIV, avalFundFromProjToDiv);
        pageFlowMap.put(AVAL_FUND_FROM_PROJ_TO_DIV_FMT, currencyFormat(avalFundFromProjToDiv));
        
        // initialize additional fields amount view
        BigDecimal approvedAmt=new BigDecimal(getAmt("ApprovedAmt").toString());
        BigDecimal distributedAmt=new BigDecimal(getAmt("PhDistributedAmt").toString());
        BigDecimal allocatedAmt=new BigDecimal(getAmt("AllocatedAmt").toString());
        BigDecimal projectBudget=new BigDecimal(getAmt("BudgetAmt").toString());
        BigDecimal availAmt=new BigDecimal(getAmt("AvailablePhAmt").toString());
        
        pageFlowMap.put("remainingApprovedAmt",approvedAmt.subtract(distributedAmt));
        pageFlowMap.put(FUNDS_REQUIRED, approvedAmt.subtract(allocatedAmt));
        pageFlowMap.put(FUNDS_REQUIRED+"Fmt", currencyFormat(approvedAmt.subtract(allocatedAmt)));
        pageFlowMap.put(PROJECT_BUDGET, projectBudget);
        pageFlowMap.put("projectBudgetFmt", currencyFormat(projectBudget));
        pageFlowMap.put(PROJECT_FUNDS_AVAILABLE,availAmt);
        
        // initialize send email
        pageFlowMap.put(AbacusConstants.SEND_EMAIL, false);
        
        SecurityContext ctx=ADFContext.getCurrent().getSecurityContext();

            if(ctx.isUserInRole("APP_BFL")){
                pageFlowMap.put(AbacusConstants.RECOVER_TO, "Bureau");
            }else if(ctx.isUserInRole("APP_FM")){
                pageFlowMap.put(AbacusConstants.RECOVER_TO, "Office");
            }
        pageFlowMap.put(AbacusConstants.RECOVER_TO, "Bureau");
        pageFlowMap.put(AbacusConstants.RECOVER_BY,null);
        
        return "next";
    }
    
    private Number getAmt(String attrName) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        AttributeBinding attrBinding = (AttributeBinding) bindings.get(attrName);
        String tmp = (String) attrBinding.getInputValue();
        Number availAmt=0;
        if(tmp!=null) {
            try {
                availAmt = NumberFormat.getCurrencyInstance().parse(tmp);
            } catch (Exception e) {
                availAmt=0;
            }
        }
        return availAmt;
    }

    private void setFundCodeAmts(String iterName) {
        Map<String,Object> pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding(iterName);
        Row r = iter.getCurrentRow();
        if(r!=null)
        {
            BigDecimal fundsAvail = (BigDecimal) r.getAttribute("RecoverableAmt");
            //BigDecimal transferAmt=(BigDecimal)pfp.get(TRANSFER_AMOUNT);
            pfp.put(AbacusConstants.FUNDS_AVAILABLE,fundsAvail);
            pfp.put("fundsAvailFmt", currencyFormat(fundsAvail));
            pfp.put(AbacusConstants.FUND_CODE,r.getAttribute("FundCode"));
        }
    }
    
    /**
     * this method will be called when user click on save once enter the recover amount and recover to.
     * and triggers the procedure to recover the amount.
     * 
     */
    public String recoverFunds() {
        // Add event code here...
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        
        OperationBinding binding;
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        // get amount recover to and recoverBy fields
        String recoverTo = (String) pageFlowMap.get(AbacusConstants.RECOVER_TO);
        String recoverBy = (String) pageFlowMap.get(AbacusConstants.RECOVER_BY);
        String result;
        String functionName=null;
        
        if(BY_ALL_AVAL_RECOVERABLE_AMT.equalsIgnoreCase(recoverBy)) {
            switch(recoverTo){
            case AbacusConstants.BUREAU:
                functionName="recoverByAllAvailableFundAmountFromProjectToDivToOfficeToBureau";
                break;
            case AbacusConstants.OFFICE:
                functionName="recoverByAllAvailableFundAmountFromProjectToDivToOffice";
                break;
            case AbacusConstants.DIVISION:
                functionName="recoverByAllAvailableFundAmountFromProjectToDiv";
                break;
            }
        }
        else if(BY_FUND_CODE.equalsIgnoreCase(recoverBy)){
            switch(recoverTo){
            case AbacusConstants.BUREAU:
                functionName="recoverFundAmtByFundCodeFromProjToDivToOfficeToBureau";
                break;
            case AbacusConstants.OFFICE:
                functionName="recoverFundAmtByFundCodeFromProjToDivToOffice";
                break;
            case AbacusConstants.DIVISION:
                functionName="recoverFromProjectToDiv";
                break;
            }
        }
        if(functionName!=null) {
            binding = bindingContainer.getOperationBinding(functionName);
            binding.execute();
            result = (String) binding.getResult();
            pageFlowMap.put(AbacusConstants.CONFIRM_MESSAGE, result);
            AttributeBinding attrBindingDiv = (AttributeBinding) bindingContainer.get(AbacusConstants.OFDA_DIVISION_CODE);
            String divCode = (String) attrBindingDiv.getInputValue();
            
            AttributeBinding attrBindingPName = (AttributeBinding) bindingContainer.get(AbacusConstants.PROJECT_NAME);
            String projName = (String) attrBindingPName.getInputValue();
            
            String bureauCode =(String) ((AttributeBinding) bindingContainer.get(AbacusConstants.BUREAU_CODE)).getInputValue();
            String officeCode =(String) ((AttributeBinding) bindingContainer.get(AbacusConstants.OFFICE_CODE)).getInputValue();
            String teamCode =(String) ((AttributeBinding) bindingContainer.get(AbacusConstants.OFDA_TEAM_CODE)).getInputValue();

            Map<String, Object> transferDetails = new HashMap<String, Object>();
            transferDetails.put(AbacusConstants.RECOVER_TO, recoverTo);
            transferDetails.put(AbacusConstants.RECOVER_BY, recoverBy);
            transferDetails.put(AbacusConstants.RECOVER_AMT, currencyFormat((BigDecimal)pageFlowMap.get(AbacusConstants.RECOVER_AMT)));
            transferDetails.put(AbacusConstants.BUDGET_FY, pageFlowMap.get(AbacusConstants.BUDGET_FY));
            transferDetails.put(AbacusConstants.BUREAU_CODE, bureauCode);
            transferDetails.put(AbacusConstants.OFFICE_CODE, officeCode);
            transferDetails.put(AbacusConstants.OFDA_DIVISION_CODE, divCode);
            transferDetails.put(AbacusConstants.OFDA_TEAM_CODE, teamCode);
            transferDetails.put(AbacusConstants.PROJECT_NAME, projName);
            transferDetails.put(AbacusConstants.PROJECT_NUMBER, pageFlowMap.get(AbacusConstants.PROJECT_NUMBER));
            transferDetails.put(AbacusConstants.FUND_CODE, pageFlowMap.get(AbacusConstants.FUND_CODE));
            transferDetails.put(AbacusConstants.SEND_EMAIL, pageFlowMap.get(AbacusConstants.SEND_EMAIL));
            transferDetails.put(AbacusConstants.COMMENTS, pageFlowMap.get(AbacusConstants.COMMENTS));
            
            
            if (pageFlowMap.get(AbacusConstants.RECOVER_BY).equals(BY_FUND_CODE)){
                OperationBinding exec1 = bindingContainer.getOperationBinding("getAvailFunds");
                exec1.execute();
            BigDecimal latestFundsAvail = (BigDecimal) exec1.getResult();
             
            transferDetails.put(AbacusConstants.FUNDS_AVAILABLE, currencyFormat(latestFundsAvail) );
            }
            pageFlowMap.put(AbacusConstants.TRANSFER_DETAILS, transferDetails);
            return result;
        }
        return null;
       
    }
    
    /**
     * This method validates the fund recover amount against the fund available amount.
     * @param facesContext
     * @param uIComponent
     * @param object
     */
    public void validateRecoverAmt(FacesContext facesContext, UIComponent uIComponent, Object object) {
        BigDecimal value = (BigDecimal) object;
        if(value != null){
            Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            BigDecimal availFundAmt = (BigDecimal) map.get(AbacusConstants.FUNDS_AVAILABLE);
                
            // check if value is less than or equal to zero
            if(value.doubleValue() <= 0){
                //throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, null,  "You entered an amount of "+ NumberFormat.getCurrencyInstance().format(value) + ". Enter an amount that is at least $0.01"));
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, null,  "Recover amount must be greater than $0.00"));
            }
            
            if(null != availFundAmt){
                if(value.compareTo(availFundAmt) == 1){
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, null,  "You entered an amount of " + NumberFormat.getCurrencyInstance().format(value) + ". Enter an amount that is less than or equal to available amount " + currencyFormat(availFundAmt)));
                }
            }
        }

    }
    
    /**
     * This method copy the available amount into 
     * @param clientEvent
     */
    public void copyAvailableAmtServerLisnter(ClientEvent clientEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        map.put(AbacusConstants.RECOVER_AMT, map.get(AbacusConstants.FUNDS_AVAILABLE));
        AdfFacesContext.getCurrentInstance().addPartialTarget(clientEvent.getComponent().getParent().getParent().getParent());
    }

    /**
     * this method sets the available fund amount once user selected the fund.
     * @param returnPopupEvent
     */
    public void recoverableAmtInputListOfValuesReturnPopupListner(ReturnPopupEvent returnPopupEvent) {
        setFundCodeAmts("VFundsRecoverableView1Iterator");
        AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent().getParent());

    }
    
    /**
     * This method sets the recover amount to all available recover amount based on the recover by radio option value.
     * @param valueChangeEvent
     */
    public void recoverByValueChangeListner(ValueChangeEvent valueChangeEvent) {
        String value = (String) valueChangeEvent.getNewValue();
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(value.equalsIgnoreCase(BY_ALL_AVAL_RECOVERABLE_AMT)){
            map.put(AbacusConstants.RECOVER_AMT, map.get(AVAL_FUND_FROM_PROJ_TO_DIV));
        }else{
            map.put(AbacusConstants.RECOVER_AMT, null);
            map.put(AbacusConstants.FUNDS_AVAILABLE,null);
            map.put("fundsAvailFmt", null);
            map.put(AbacusConstants.FUND_CODE,null);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(valueChangeEvent.getComponent().getParent().getParent());
    }
    
    /**
     * this method serves as a tooltip for send email check box.
     * @return
     */
    public String getEmailAddr(){
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        String budgetFY = ""+((AttributeBinding) bindings.get("BudgetFy")).getInputValue();
        String projectNbr = (String) ((AttributeBinding) bindings.get("ProjectNbr")).getInputValue();
        MessageBean mbean = new MessageBean();
        Map<String, String> emailMap = mbean.getActivityFundTransferEmailList(budgetFY,projectNbr);
        return emailMap.get(AbacusConstants.EMAIL_LIST);
    }
}
