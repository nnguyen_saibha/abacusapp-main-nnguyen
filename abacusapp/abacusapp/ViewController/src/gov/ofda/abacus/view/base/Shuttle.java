package gov.ofda.abacus.view.base;

import java.util.*;
import javax.faces.model.*;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class Shuttle {
    public Shuttle() {
    }

    /**
     * Given an iterator name, a value attribute name and a display attribute name
     * this method will invoke selectItemsForIterator method to return a List of SelectItem values
     * http://www.gebs.ro/blog/oracle/oracle-adf-select-many-shuttle/
     * Author: Bogdan Petridan
     * ADF Select Many Shuttle September 30th, 2010 | Posted by Bogdan Petridean in Oracle ADF
     * @param iteratorName - parent VO iterator
     * @param valueAttrName
     * @param displayAttrName
     * @return
     */
    public static List<SelectItem> getAll(String iteratorName, String iteratorName1,String valueAttrName, String displayAttrName) {
        DCIteratorBinding iter =
            ((DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry()).findIteratorBinding(iteratorName);
        DCIteratorBinding iter1 =
            ((DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry()).findIteratorBinding(iteratorName1);
        List<SelectItem> selectItems = new ArrayList<SelectItem>();
        List<String> selectCodes=new ArrayList<String>();
        for (Row r : iter.getAllRowsInRange()) {
            selectItems.add(new SelectItem(r.getAttribute(valueAttrName), (String)r.getAttribute(displayAttrName)));
            selectCodes.add(r.getAttribute(valueAttrName).toString());
        }
        for(Row r : iter1.getAllRowsInRange()){
           if(!selectCodes.contains(r.getAttribute(valueAttrName).toString()))
            selectItems.add(new SelectItem(r.getAttribute(valueAttrName), (String)r.getAttribute(displayAttrName)));
        }
        
        return selectItems;

    }

    /**
     *Given an iterator name and an attribute name this method will return a List of items
     *
     * @param iteratorName - nested VO iterator
     * @param attrName
     * @return
     */
    public static List getSelected(String iteratorName, String attrName) {
        List selected = new ArrayList();
        DCIteratorBinding iterator =
            ((DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry()).findIteratorBinding(iteratorName);
        Row[] rowSet = iterator.getAllRowsInRange();
        for (Row r : rowSet) {
            selected.add(r.getAttribute(attrName));
        }
        return selected;
    }

    /**
     *
     * @param selectedValues
     * @param fkIteratorName - nested VO iterator
     * @param fk1AttName
     * @param deleteOpName - delete operation should be defined in page bindings
     * @param createInsertOpName - createInsert operation should be defined in page bindings
     */
    public static void setSelected(List selectedValues, String fkIteratorName, String fk1AttName, String deleteOpName, String createInsertOpName) {
        if (selectedValues == null)
            selectedValues = new ArrayList(0);
        DCBindingContainer dcbindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterator = dcbindings.findIteratorBinding(fkIteratorName);
        OperationBinding deleteOp = dcbindings.getOperationBinding(deleteOpName);
        OperationBinding createInsertOp = dcbindings.getOperationBinding(createInsertOpName);

        Row[] rowSet = iterator.getAllRowsInRange();
        for (Row row : rowSet) {
            Object fk2Att = row.getAttribute(fk1AttName);
            if (!selectedValues.contains(fk2Att)) {
                iterator.setCurrentRowWithKey(row.getKey().toStringFormat(true));
                deleteOp.execute();
            } else {
                selectedValues.remove(fk2Att);
            }
        }
        for (Object val : selectedValues) {
            createInsertOp.execute();
            Row row = iterator.getCurrentRow();
            row.setAttribute(fk1AttName, val);
        }
    }
    
    public static List getSelectedLookup(String iteratorName, String attrName) {
       List selected = new ArrayList();
       DCIteratorBinding iterator =
           ((DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry()).findIteratorBinding(iteratorName);
       Row[] rowSet = iterator.getAllRowsInRange();
       for (Row r : rowSet) {
           selected.add(r.getAttribute(attrName));
       }
       return selected;
    }
    public static void setSelectedLookup(List selectedValues, String fkIteratorName, String fk1AttId,String deleteOpName, String createInsertOpName) {
        if (selectedValues == null)
            selectedValues = new ArrayList(0);
        DCBindingContainer dcbindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterator = dcbindings.findIteratorBinding(fkIteratorName);
        OperationBinding deleteOp = dcbindings.getOperationBinding(deleteOpName);
        OperationBinding createInsertOp = dcbindings.getOperationBinding(createInsertOpName);
        Row[] rowSet = iterator.getAllRowsInRange();
        for (Row row : rowSet) {
                iterator.setCurrentRowWithKey(row.getKey().toStringFormat(true));
                deleteOp.execute();
        }
        int ct=1;
        Object x=null;
        for (Object val : selectedValues) { 
            createInsertOp.execute();
            Row row = iterator.getCurrentRow();
            row.setAttribute(fk1AttId,val);
            row.setAttribute("IndTypeLevel",ct++);
            row.setAttribute("ParentIndTypeId",x);
            x = val;
            
        }       
    }
}
