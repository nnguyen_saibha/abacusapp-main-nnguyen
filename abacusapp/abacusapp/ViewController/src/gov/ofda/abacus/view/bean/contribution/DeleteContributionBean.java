package gov.ofda.abacus.view.bean.contribution;

import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class DeleteContributionBean implements Serializable {
    @SuppressWarnings("compatibility:6219709877308842071")
    private static final long serialVersionUID = 1L;

    public DeleteContributionBean() {
        super();
    }
    public void deleteContributionDialogLsnr(DialogEvent de) {
            if (DialogEvent.Outcome.yes == de.getOutcome()) {
                DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding exec = bindings.getOperationBinding("deleteContribution");
                exec.execute();
                FacesContext fc = FacesContext.getCurrentInstance();
                String errorMsg = (String) exec.getResult();
                if(errorMsg==null)
                    fc.addMessage(null,
                                  new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                   "Contribution deleted successfully.", null));
                else
                {
                  if(errorMsg.contains("Unexpected Error")) {
                      fc.addMessage(null,
                                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                     "Unexpected Error. Please contact Abacus Team (abacus@ofda.gov) with contribution information.", null));
                      MessageBean mb=new MessageBean();
                      mb.sendAdminEmail("Error Delete Contribution", errorMsg);
                  }
                  else
                    fc.addMessage(null,
                                  new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                   errorMsg, null));
                }
            }
    }
    public String getDeleteContributionValid() {
       DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
       OperationBinding exec = bindings.getOperationBinding("deleteContributionCheck");
       exec.execute();
       String errorMsg = (String) exec.getResult();
       Map viewScope=AdfFacesContext.getCurrentInstance().getViewScope();
       viewScope.put("deleteContributionErrorMsg", errorMsg);
       return errorMsg;
    }
}
