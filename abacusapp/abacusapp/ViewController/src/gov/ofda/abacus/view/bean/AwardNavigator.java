package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.view.base.UIControl;

import java.io.Serializable;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.context.AdfFacesContext;

public class AwardNavigator extends UIControl implements Serializable{
    @SuppressWarnings("compatibility:-7231134257826170536")
    private static final long serialVersionUID = 1L;


    private String showColumns = "Details";
    
    public AwardNavigator() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if("ASIST".equals(pfm.get("defaultVC"))){
            showColumns="ASIST"; 
        }
      
    }


    public void setShowColumns(String showColumns) {
        this.showColumns = showColumns;
    }

    public String getShowColumns() {
        return showColumns;
    }
    public void showColumnsActionListener(ActionEvent actionEvent) {
        UIComponent uic = actionEvent.getComponent();
        showColumns = (String) (uic.getAttributes()).get("showColumn");
        AdfFacesContext.getCurrentInstance().addPartialTarget(uic.getParent());
    }
}
