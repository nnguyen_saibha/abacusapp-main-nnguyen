package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.view.bean.message.MessageBean;
import gov.ofda.uishell.TabContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.ApplicationModule;

import org.apache.myfaces.trinidad.event.ReturnEvent;

public class ActionRegionSwitcher implements Serializable {
    @SuppressWarnings("compatibility:5564805145295752323")
    private static final long serialVersionUID = 3672350510212156112L;
    private String taskFlowId = "/WEB-INF/tflows/action/view-action-btf.xml#view-action-btf";

    private Map<String, Object> regionTaskflowMap = new HashMap<String, Object>();
    
    private final String saveErrorMessage = "Please save or cancel changes to continue.";
    private String priorDefaultView =null;

    public void setRegionTaskflowMap(Map<String, Object> regionTaskflowMap) {
        this.regionTaskflowMap = regionTaskflowMap;
    }

    public Map<String, Object> getRegionTaskflowMap() {
        return regionTaskflowMap;
    }

    public ActionRegionSwitcher() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        regionTaskflowMap.clear();
        regionTaskflowMap.put("actionID", pageFlowAttrs.get("actionid"));
        regionTaskflowMap.put("defaultActionTab", pageFlowAttrs.get("defaultActionTab"));
        regionTaskflowMap.put("showView", "tabs");
        regionTaskflowMap.put("showToolbarView", "toolbar");
        regionTaskflowMap.put("tabContext", TabContext.getCurrentInstance());
        regionTaskflowMap.put("defaultReportType", "Pdf");
    }
    
    
    public String viewAwardFlow() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.execute();
        if (!this.checkForDirtyPage()) {
            
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            regionTaskflowMap.clear();
            regionTaskflowMap.put("awardId", pageFlowAttrs.get("awardId"));
            regionTaskflowMap.put("dontShowToolbar", true);
            regionTaskflowMap.put("tabContext",pageFlowAttrs.get("tabContext"));
            if(pageFlowAttrs.get("defaultView") != null && pageFlowAttrs.get("defaultView").equals("ASIST")){
                setDynamicTaskFlowId("/WEB-INF/tflows/award/asist-upload-docs-tf.xml#asist-upload-docs-tf");
                pageFlowAttrs.put("tabContext",pageFlowAttrs.get("tabContext"));
                pageFlowAttrs.put("defaultView", "viewASISTDocumentsbtf");
            }else{
                setDynamicTaskFlowId("/WEB-INF/tflows/view-award-flow.xml#view-award-flow");
                pageFlowAttrs.put("defaultView", "viewAwardbtf");
            }
            
            return "next";
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }

        return null;
    }

    public TaskFlowId getDynamicTaskFlowId() {

        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }

    public String viewactionbtf() {
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            setDynamicTaskFlowId("/WEB-INF/tflows/action/view-action-btf.xml#view-action-btf");
            regionTaskflowMap.clear();
            regionTaskflowMap.put("actionID", pageFlowAttrs.get("actionid"));
            regionTaskflowMap.put("defaultActionTab", pageFlowAttrs.get("defaultActionTab"));
            regionTaskflowMap.put("showView", "tabs");
            regionTaskflowMap.put("showToolbarView", "toolbar");
            regionTaskflowMap.put("defaultReportType", "Pdf");
            regionTaskflowMap.put("tabContext", TabContext.getCurrentInstance());
            pageFlowAttrs.put("defaultView", "viewAwardbtf");
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }

        return null;
    }


    public String uploadActionDocuments() {
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            setDynamicTaskFlowId("/WEB-INF/tflows/uploadDoc-flow.xml#uploadDoc-flow");
            regionTaskflowMap.clear();
            regionTaskflowMap.put("sourceType", "1");
            regionTaskflowMap.put("actionId", pageFlowAttrs.get("actionid"));

        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }
        return null;
    }

    public String manageSectorDetails() {
        ADFContext adfCtx = ADFContext.getCurrent();
        SecurityContext secCntx = adfCtx.getSecurityContext();
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            setDynamicTaskFlowId("/WEB-INF/sector-manager-btf.xml#sector-manager-btf");
            regionTaskflowMap.clear();
            regionTaskflowMap.put("actionID", pageFlowAttrs.get("actionid"));
            regionTaskflowMap.put("userID", secCntx.getUserName());
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }
        return null;
    }

    public String manageRestrictedGoods() {
        ADFContext adfCtx = ADFContext.getCurrent();
        SecurityContext secCntx = adfCtx.getSecurityContext();
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            setDynamicTaskFlowId("/WEB-INF/rg-manager-btf.xml#rg-manager-btf");
            regionTaskflowMap.clear();
            regionTaskflowMap.put("actionID", pageFlowAttrs.get("actionid"));
            regionTaskflowMap.put("userID", secCntx.getUserName());
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,saveErrorMessage, null));
        }
        return null;
    }

    public String manageRestrictedDocs() {
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            setDynamicTaskFlowId("/WEB-INF/restricted-docs-btf.xml#restricted-docs-btf");
            regionTaskflowMap.clear();
            regionTaskflowMap.put("actionID", pageFlowAttrs.get("actionid"));
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }
        return null;
    }

    public String viewActionDocuments() {
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            setDynamicTaskFlowId("/WEB-INF/DocsViewTaskflow.xml#DocsViewTaskflow");
            regionTaskflowMap.clear();
            regionTaskflowMap.put("inSuppSourceId", "0");
            regionTaskflowMap.put("inSourceId", pageFlowAttrs.get("actionid"));
            regionTaskflowMap.put("inSourceType", "3");
            regionTaskflowMap.put("inOfdaFlg", "Y");
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }
        return null;
    }

    public String viewAwardDocuments() {
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            setDynamicTaskFlowId("/WEB-INF/DocsViewTaskflow.xml#DocsViewTaskflow");
            regionTaskflowMap.clear();
            regionTaskflowMap.put("inSuppSourceId", "0");
            //#{bindings.AwardId.inputValue ne null?bindings.AwardId.inputValue:pageFlowScope.actionID}
            regionTaskflowMap.put("inSourceId",
                                  evaluateEL("#{bindings.AwardId.inputValue ne null?bindings.AwardId.inputValue:pageFlowScope.actionID}"));
            regionTaskflowMap.put("inSourceType", "2");
            regionTaskflowMap.put("inOfdaFlg", "Y");
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }
        return null;
    }

    //
    public String editAction() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        setDynamicTaskFlowId("/WEB-INF/tflows/open-form-fragments.xml#open-form-fragments");
        regionTaskflowMap.clear();

        regionTaskflowMap.put("actionid", pageFlowAttrs.get("actionid"));
        regionTaskflowMap.put("formName", "PROCUREMENT_PLANNING");
        return null;
    }

    public String viewAwardbtf() {
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            setDynamicTaskFlowId("/WEB-INF/tflows/view-award-flow.xml#view-award-flow");
            regionTaskflowMap.clear();
            regionTaskflowMap.put("awardId", evaluateEL("#{bindings.AwardId.inputValue}"));
            regionTaskflowMap.put("dontShowToolbar", Boolean.TRUE);
            regionTaskflowMap.put("tabContext", TabContext.getCurrentInstance());


        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }

        return null;
    }

    public String viewProjectbtf() {
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            setDynamicTaskFlowId("/WEB-INF/tflows/activity/activity-details-flow.xml#activity-details-flow");
            regionTaskflowMap.clear();
            regionTaskflowMap.put("budget_year", evaluateEL("#{bindings.BudgetFy.inputValue}"));
            regionTaskflowMap.put("project_number", evaluateEL("#{bindings.ProjectNbr.inputValue}"));
            regionTaskflowMap.put("showView", "tabs");
            SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
            if (sc.isUserInRole("APP_FM") || sc.isUserInRole("APP_FINANCE"))
                regionTaskflowMap.put("showColumns", "Funds");
            else if (sc.isUserInRole("APP_BBL") || sc.isUserInRole("APP_OL") || sc.isUserInRole("APP_DL") || sc.isUserInRole("APP_TL"))
                regionTaskflowMap.put("showColumns", "Budget");
            else
                regionTaskflowMap.put("showColumns", "ProjectDetails");
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }

        return null;
    }
    
    

    private boolean checkForDirtyPage() {        
        Boolean dirtyFlg = false;
        if (this.taskFlowId.equals("/WEB-INF/tflows/uploadDoc-flow.xml#uploadDoc-flow")) {
            dirtyFlg =
                this.getApplicationModule("#{data.PAabacusappModuleDataControl.dataProvider}").getTransaction().isDirty();
            if(dirtyFlg){
                AdfFacesContext.getCurrentInstance().getPageFlowScope().put("defaultView","uploadActionDocuments");              
            }
            
        }
        if (this.taskFlowId.equals("/WEB-INF/tflows/activity/edit-activity-tf.xml#edit-activity-tf")) {
            dirtyFlg =
                this.getApplicationModule("#{data.PAabacusappModuleDataControl.dataProvider}").getTransaction().isDirty();
            if(dirtyFlg){
                AdfFacesContext.getCurrentInstance().getPageFlowScope().put("defaultView","editActivity");              
            }            
        }
        if (this.taskFlowId.equals("/WEB-INF/sector-manager-btf.xml#sector-manager-btf")) {
            dirtyFlg = this.getApplicationModule("#{data.PAModuleDataControl.dataProvider}").getTransaction().isDirty();
            if(dirtyFlg){
                AdfFacesContext.getCurrentInstance().getPageFlowScope().put("defaultView","manageSectorDetails");               
            }
        }
        if (this.taskFlowId.equals("/WEB-INF/rg-manager-btf.xml#rg-manager-btf")) {
            dirtyFlg = this.getApplicationModule("#{data.PAModuleDataControl.dataProvider}").getTransaction().isDirty();
            if(dirtyFlg){
                AdfFacesContext.getCurrentInstance().getPageFlowScope().put("defaultView","manageRestrictedGoods");               
            }
        }
        if (this.taskFlowId.equals("/WEB-INF/restricted-docs-btf.xml#restricted-docs-btf")) {
            dirtyFlg = this.getApplicationModule("#{data.PAModuleDataControl.dataProvider}").getTransaction().isDirty();
            if(dirtyFlg){
                AdfFacesContext.getCurrentInstance().getPageFlowScope().put("defaultView","manageRestrictedDocs");               
            }
        }
        if (this.taskFlowId.equals("/WEB-INF/review/AppReview-BTF.xml#AppReview-BTF")) {
            dirtyFlg = this.getApplicationModule("#{data.DocumentAMDataControl.dataProvider}").getTransaction().isDirty();
            if(dirtyFlg){
                if(priorDefaultView !=null){
                    AdfFacesContext.getCurrentInstance().getPageFlowScope().put("defaultView",priorDefaultView); 
                }else {
                    AdfFacesContext.getCurrentInstance().getPageFlowScope().put("defaultView","appReviewSite");                   
                }
                
            }
        }
        return dirtyFlg;
    }


    private ApplicationModule getApplicationModule(String dataProvider) {
        FacesContext fc = FacesContext.getCurrentInstance();
        Application app = fc.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = fc.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, dataProvider, Object.class);
        return (ApplicationModule) valueExp.getValue(elContext);
    }

    private static Object evaluateEL(String el) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ELContext elContext = facesContext.getELContext();
        ExpressionFactory expressionFactory = facesContext.getApplication().getExpressionFactory();
        ValueExpression exp = expressionFactory.createValueExpression(elContext, el, Object.class);
        return exp.getValue(elContext);
    }

    public String editActivity() {
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
            setDynamicTaskFlowId("/WEB-INF/tflows/activity/edit-activity-tf.xml#edit-activity-tf");
            regionTaskflowMap.clear();
            String isEdit = (String) requestScopeMap.get("isEdit");
            regionTaskflowMap.put("budgetFY", requestScopeMap.get("budgetFy"));
            regionTaskflowMap.put("projectNbr", requestScopeMap.get("projectNbr"));
            regionTaskflowMap.put("trainStop", requestScopeMap.get("trainStop"));
            regionTaskflowMap.put("isNew", false);
            if("true".equals(isEdit))
                regionTaskflowMap.put("isEdit",true);   
            else
                regionTaskflowMap.put("isEdit",false);
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }

        return null;

    }
    public void addBudgetReturnLsnr(ReturnEvent returnEvent) {
        Map pfp=AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) pfp.get("confirmMessage");
        MessageBean mb=new MessageBean();
        mb.sendActivityBudgetTransferEmail(result, (Map) pfp.get("transferDetails"),(Boolean) pfp.get("isSendEmail"));

    }
    public void addFundsReturnLsnr(ReturnEvent returnEvent) {
        Map pfp=AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) pfp.get("confirmMessage");
        MessageBean mb=new MessageBean();
        mb.sendActivityFundTransferEmail(result, (Map) pfp.get("transferDetails"),(Boolean) pfp.get("isSendEmail"));
    }
    public void budgetRecoveryReturnListner(ReturnEvent returnEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) map.get("confirmMessage");
        MessageBean bean = new MessageBean();
        bean.sendBudgetRecoveryEmail(result, (Map)map.get("transferDetails"), (Boolean)map.get("isSendEmail"));
    }

    public String viewParentDetailsSet() {
            Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
            setDynamicTaskFlowId("/WEB-INF/tflows/parent-detail/parent-detail-set-tf.xml#parent-detail-set-tf");
            regionTaskflowMap.clear();
            regionTaskflowMap.put("parentActionID", requestScopeMap.get("parentActionID"));
            regionTaskflowMap.put("actionID", requestScopeMap.get("actionID"));
            regionTaskflowMap.put("tabContext", TabContext.getCurrentInstance());
        return null;
    }

    public String viewAwardSummary() {
        Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        setDynamicTaskFlowId("/WEB-INF/tflows/award/award-status-view-tf.xml#award-status-view-tf");
        regionTaskflowMap.clear();
        regionTaskflowMap.put("actionId", requestScopeMap.get("actionId"));
        regionTaskflowMap.put("modActionId", requestScopeMap.get("modActionId"));
        regionTaskflowMap.put("tabContext", TabContext.getCurrentInstance());
        return null;
    }

    public String viewLetterMemo() {
        setDynamicTaskFlowId("/WEB-INF/tflows/lettermemo/letters-memo-list-tf.xml#letters-memo-list-tf");
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        regionTaskflowMap.clear();
        regionTaskflowMap.put("id", pageFlowAttrs.get("actionid"));
        regionTaskflowMap.put("idType", "ACTION");
        return null;
    }

    public String viewAwardCloseout() {
        Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        setDynamicTaskFlowId("/WEB-INF/tflows/award/award-closeout-tf.xml#award-closeout-tf");
        regionTaskflowMap.clear();
        regionTaskflowMap.put("awardId", requestScopeMap.get("awid"));
      //  regionTaskflowMap.put("isEdit",false);
        return null;
    }
    
    public String viewAsistUpload() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        Map requestScopeMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        setDynamicTaskFlowId("/WEB-INF/tflows/award/asist-upload-docs-tf.xml#asist-upload-docs-tf");
        regionTaskflowMap.clear();
        regionTaskflowMap.put("awardId", requestScopeMap.get("awid"));
        regionTaskflowMap.put("tabContext",pageFlowAttrs.get("tabContext"));
      //  regionTaskflowMap.put("isEdit",false);
        return null;
    }
    
    public String appReviewSite() {
        ADFContext adfCtx = ADFContext.getCurrent();
        SecurityContext secCntx = adfCtx.getSecurityContext();
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            setDynamicTaskFlowId("/WEB-INF/review/AppReview-BTF.xml#AppReview-BTF");
            setPriorDefaultView("appReviewSite");
            regionTaskflowMap.clear();
            regionTaskflowMap.put("pActionId", pageFlowAttrs.get("actionid"));            
            regionTaskflowMap.put("pApplicationId", pageFlowAttrs.get("paaId"));
            regionTaskflowMap.put("pPackageType", "REVIEW");
            regionTaskflowMap.put("pFolderType", "1");
            regionTaskflowMap.put("pIsEditable", "true");
            
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }
        return null;
    }
    
    public String awardPkgForGUSite() {
        ADFContext adfCtx = ADFContext.getCurrent();
        SecurityContext secCntx = adfCtx.getSecurityContext();
        if (!this.checkForDirtyPage()) {
            Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            setDynamicTaskFlowId("/WEB-INF/review/AppReview-BTF.xml#AppReview-BTF");
            setPriorDefaultView("awardPkgForGUSite");
            regionTaskflowMap.clear();
            regionTaskflowMap.put("pActionId", pageFlowAttrs.get("actionid"));            
            regionTaskflowMap.put("pApplicationId", pageFlowAttrs.get("paaId"));
            regionTaskflowMap.put("pPackageType", "AWARD");
            regionTaskflowMap.put("pFolderType", "2");
            regionTaskflowMap.put("pIsEditable", "true");
            
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }
        return null;
    }


    public void setPriorDefaultView(String priorDefaultView) {
        this.priorDefaultView = priorDefaultView;
    }

    public String getPriorDefaultView() {
        return priorDefaultView;
    }
}
