package gov.ofda.abacus.view.bean.activity;

import java.io.Serializable;

import java.util.Map;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class ActivityLookupBean implements Serializable {
    @SuppressWarnings("compatibility:2287837763874965077")
    private static final long serialVersionUID = 1L;
    
    private RichPopup editDialogPopup;


    public void setEditDialogPopup(RichPopup editDialogPopup) {
        this.editDialogPopup = editDialogPopup;
    }

    public RichPopup getEditDialogPopup() {
        return editDialogPopup;
    }

    public void saveProject() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Commit");
        exec.execute();
        
        DCIteratorBinding iter = (DCIteratorBinding) bindings.findIteratorBinding("EditProjectLookupViewIterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row row = rsi.getCurrentRow();
        Object dbSeq =  row.getAttribute("ProjectNbr");
        if(dbSeq != null){
            String projectNbr = String.valueOf(dbSeq);
            Map<String, Object> pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope(); 
            pageFlowMap.put("lookupProjectNbr", projectNbr);
            pageFlowMap.put("lookupDivision", row.getAttribute("OfdaDivisionCode"));
            pageFlowMap.put("lookupTeam", row.getAttribute("OfdaTeamCode"));
            pageFlowMap.put("lookupProjectName", row.getAttribute("ProjectName"));
        }
    }
}
