package gov.ofda.abacus.view.bean.action;

import gov.ofda.abacus.view.bean.AppBean;

import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.el.MethodExpression;

import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.PhaseId;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.TaskFlowContext;
import oracle.adf.controller.TaskFlowTrainModel;
import oracle.adf.controller.TaskFlowTrainStopModel;
import oracle.adf.controller.ViewPortContext;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.fragment.RichRegion;
import oracle.adf.view.rich.component.rich.nav.RichCommandNavigationItem;
import oracle.adf.view.rich.event.DialogEvent;


import oracle.adf.view.rich.model.TrainStopModel;

import oracle.jbo.ApplicationModule;

/**
 *
 * http://www.oracle.com/technetwork/developer-tools/adf/learnmore/97-deferred-trainstop-navigation-1528557.pdf
 *
 */
public class TrainHandlerBean {
    /**
     * Managed bean property reference to a helper bean in view scope
     */
    private TrainHandlerBeanHelper trainHandlerBeanHelper = null;


    private final String DOCUMENTS_TRAINSTOP = "(.*)documents";

    private final String SECTORDETAILS_TRAINSTOP = "(.*)sectorDetails";
    private final String NEGMEMO_TRAINSTOP = "(.*)negmemo";
    private final String saveErrorMessage = "Please save or cancel changes to continue.";
    private final String APPREVIEWDOCS_TRAINSTOP = "(.*)applicationReviewDocs";
    private final String AWARDPKGDOCS_TRAINSTOP = "(.*)awardPackageGUDocs";

    public TrainHandlerBean() {
        super();
    }

    /**
     * Action method that is referenced from the train stop command
     * action to defer train navigation and allow developers to
     * interact with the user. In this sample a popup dialog is opened
     * for the user to confirm train navigation.
     *
     * The use case for deferred trains top navigation includes manual
     * complex field validation, to check for
     * uncommitted data, e.g.
     * ControllerContext.getInstance().getCurrentViewPort()
     * .isDataDirty() , etc.
     *
     * @param actionEvent
     */
    public void onTrainStopSelect(ActionEvent actionEvent) {
        RichCommandNavigationItem rni = (RichCommandNavigationItem) actionEvent.getSource();
        TaskFlowTrainStopModel selectedTrainStop = (TaskFlowTrainStopModel) rni.getAttributes().get("trainStopNode");
        String outcome = selectedTrainStop.getOutcome();
        Map pScopeMap = ADFContext.getCurrent().getPageFlowScope();
        if (!isDirty(outcome)) {
            pScopeMap.put("previousOutCome", outcome);
            trainHandlerBeanHelper.setSelectedTrainStopOutcome(outcome);
            queueTrainStopEventToRegion("#{viewScope.trainHandlerBeanHelper.getSelectedTrainStopOutcome}",
                                        actionEvent.getComponent());
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
        }

    }

    public void onTrainButtonBarSelect(ActionEvent actionEvent) {
        TaskFlowTrainModel trainModel = getTrainModel();
        TaskFlowTrainStopModel currentStop = trainModel.getCurrentStop();
        Map pScopeMap = ADFContext.getCurrent().getPageFlowScope();
        pScopeMap.put("previousOutCome", currentStop.getOutcome());
        String newTrainStopStr = null;
        TaskFlowTrainStopModel newTrainStop = null;
        if (actionEvent != null) {
            if ((actionEvent.getComponent().getClientId()).matches("(.*)back(.*)")) {
                newTrainStop = (TaskFlowTrainStopModel) getPrevious();

            } else if ((actionEvent.getComponent().getClientId()).matches("(.*)next(.*)")) {
                newTrainStop = (TaskFlowTrainStopModel) getNext();
            }
            if (newTrainStop != null) {
                newTrainStopStr = newTrainStop.getOutcome();
                if (!isDirty(newTrainStopStr)) {
                    //pScopeMap.put("previousOutCome", newTrainStopStr);
                    trainHandlerBeanHelper.setSelectedTrainStopOutcome(newTrainStopStr);
                    queueTrainStopEventToRegion("#{viewScope.trainHandlerBeanHelper.getSelectedTrainStopOutcome}",
                                                actionEvent.getComponent());
                } else {
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, saveErrorMessage, null));
                }
            }
        }
    }

    /**
     * Method that finds the af:region container for a bounded task
     * flow exposed in a region. Don't use this method outside of ADF
     * regions.
     * @param uiComponent Component within the region that is the
     * starting point for the search
     * @return The RichRegion instance as UIComponent
     */
    private RichRegion findRichRegionContainer(UIComponent uiComponent) {
        UIComponent currentComponent = uiComponent;
        while (!(currentComponent instanceof RichRegion)) {
            //task flows in a region always have a RichRegion container
            //sonewhere.
            currentComponent = currentComponent.getParent();
        }
        return (RichRegion) currentComponent;
    }


    /**
     * Wrapper method around the RichRegion queueActionEventInRegion
     * API.
     * @param outcomeEL The EL to access a managed bean that returns
     * the navigation string to to follow. This can be an outcome for a
     * navigation case, or as in this sample, the implicit navigation
     * case used in trains
     *
     * @param searchComponentInRegion To queue the action, we need a
     * handle to the RichRegion component. Providing us with a
     * component residing in a region is all that is
     * needed to search the af:region container
     */
    private void queueTrainStopEventToRegion(String outcomeEL, UIComponent searchComponentInRegion) {
        //This sample assumes bounded task flows to be exposed in an
        //af:region. So it is safe to assume a parent component to be of
        //type RichRegion. Let's find it to queue the train stop
        //outcome event for navigation.
        RichRegion adfRegion = null;
        adfRegion = this.findRichRegionContainer(searchComponentInRegion);
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExpressionFactory expressionFactory = fctx.getApplication().getExpressionFactory();
        ELContext elctx = fctx.getELContext();
        MethodExpression methodExpression = expressionFactory.createMethodExpression(elctx, outcomeEL, String.class, new Class[] {
                                                                                     });
        //queue action in region
        adfRegion.queueActionEventInRegion(methodExpression, null, null, false, 0, 0, PhaseId.INVOKE_APPLICATION);
    }

    public void setTrainHandlerBeanHelper(TrainHandlerBeanHelper trainHandlerBeanHelper) {
        this.trainHandlerBeanHelper = trainHandlerBeanHelper;
    }

    public TrainHandlerBeanHelper getTrainHandlerBeanHelper() {
        return trainHandlerBeanHelper;
    }

    private TaskFlowTrainModel getTrainModel() {
        ControllerContext controllerContext = ControllerContext.getInstance();
        ViewPortContext currentViewPortCtx = controllerContext.getCurrentViewPort();
        TaskFlowContext taskFlowCtx = currentViewPortCtx.getTaskFlowContext();
        TaskFlowTrainModel trainModel = taskFlowCtx.getTaskFlowTrainModel();
        return trainModel;
    }


    private ApplicationModule getApplicationModule(String dataProvider) {
        FacesContext fc = FacesContext.getCurrentInstance();
        Application app = fc.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = fc.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, dataProvider, Object.class);
        return (ApplicationModule) valueExp.getValue(elContext);
    }

    /**
     * This Method checks if the datacontrol is dirty during the navigation to/from the train stops(Sector Details, Documents)
     * @param newTrainStop
     * @return
     */
    public boolean isDirty(String newTrainStop) {
        Boolean dirtyFlg = false;
        Map pScopeMap = ADFContext.getCurrent().getPageFlowScope();
        String currentTrainStop = (String) pScopeMap.get("previousOutCome");
      
        if ((newTrainStop.matches(DOCUMENTS_TRAINSTOP)) ||
            (currentTrainStop != null && currentTrainStop.matches(DOCUMENTS_TRAINSTOP))) {

            dirtyFlg =
                this.getApplicationModule("#{data.PAabacusappModuleDataControl.dataProvider}").getTransaction().isDirty();

        }
        if ((newTrainStop.matches(SECTORDETAILS_TRAINSTOP)) ||
            (currentTrainStop != null && currentTrainStop.matches(SECTORDETAILS_TRAINSTOP))) {
            if (!dirtyFlg)
                dirtyFlg =
                    this.getApplicationModule("#{data.PAabacusappModuleDataControl.dataProvider}").getTransaction().isDirty();
            if (!dirtyFlg)
                dirtyFlg =
                    this.getApplicationModule("#{data.PAModuleDataControl.dataProvider}").getTransaction().isDirty();
        }
        if ((newTrainStop.matches(NEGMEMO_TRAINSTOP)) ||
            (currentTrainStop != null && currentTrainStop.matches(NEGMEMO_TRAINSTOP))) {
            if (!dirtyFlg)
                dirtyFlg =
                    this.getApplicationModule("#{data.PAabacusappModuleDataControl.dataProvider}").getTransaction().isDirty();
            if (!dirtyFlg)
                dirtyFlg =
                    this.getApplicationModule("#{data.PAModuleDataControl.dataProvider}").getTransaction().isDirty();
        }
        if ((newTrainStop.matches(APPREVIEWDOCS_TRAINSTOP)) ||
            (currentTrainStop != null && currentTrainStop.matches(APPREVIEWDOCS_TRAINSTOP))) {
            if (!dirtyFlg)
                dirtyFlg =
                    this.getApplicationModule("#{data.PAabacusappModuleDataControl.dataProvider}").getTransaction().isDirty();
            if (!dirtyFlg)
                dirtyFlg =
                    this.getApplicationModule("#{data.DocumentAMDataControl.dataProvider}").getTransaction().isDirty();
        }
        if ((newTrainStop.matches(AWARDPKGDOCS_TRAINSTOP)) ||
            (currentTrainStop != null && currentTrainStop.matches(AWARDPKGDOCS_TRAINSTOP))) {
            if (!dirtyFlg)
                dirtyFlg =
                    this.getApplicationModule("#{data.PAabacusappModuleDataControl.dataProvider}").getTransaction().isDirty();
            if (!dirtyFlg)
                dirtyFlg =
                    this.getApplicationModule("#{data.DocumentAMDataControl.dataProvider}").getTransaction().isDirty();
        }
        return dirtyFlg;
    }

    /**
     *
     * @return
     */
    public boolean getPreviousDisabled() {
        TaskFlowTrainModel trainModel = getTrainModel();
        TaskFlowTrainStopModel currentStop = trainModel.getCurrentStop();
        TrainStopModel previoudStopModel = (TrainStopModel) trainModel.getPreviousStop(currentStop);
        if (previoudStopModel == null) {
            return true;
        }
        return false;
    }


    /**
     *
     * @return
     */
    public TrainStopModel getNext() {
        TaskFlowTrainModel trainModel = getTrainModel();
        TaskFlowTrainStopModel currentStop = trainModel.getCurrentStop();
        TrainStopModel nextStopModel = (TrainStopModel) trainModel.getNextStop(currentStop);
        if (nextStopModel != null) {
            while (nextStopModel.isDisabled()) {
                currentStop = (TaskFlowTrainStopModel) nextStopModel;
                nextStopModel = (TrainStopModel) trainModel.getNextStop(currentStop);
            }
        }
        return nextStopModel;
    }

    /**
     *
     * @return
     */
    public TrainStopModel getPrevious() {
        TaskFlowTrainModel trainModel = getTrainModel();
        TaskFlowTrainStopModel currentStop = trainModel.getCurrentStop();
        TrainStopModel prevStopModel = (TrainStopModel) trainModel.getPreviousStop(currentStop);
        if (prevStopModel != null) {
            while (prevStopModel.isDisabled()) {
                currentStop = (TaskFlowTrainStopModel) prevStopModel;
                prevStopModel = (TrainStopModel) trainModel.getPreviousStop(currentStop);
            }
        }
        return prevStopModel;
    }
}
