package gov.ofda.abacus.view.bean.action;

import java.io.Serializable;

import java.math.BigDecimal;

import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputListOfValues;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandNavigationItem;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.LaunchPopupEvent;
import oracle.adf.view.rich.event.ReturnPopupEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class CreateActionBackingBean implements Serializable {
    @SuppressWarnings("compatibility:5248925486832898178")
    private static final long serialVersionUID = 5440361884321385184L;

    private static final String REGULAR_NEW = "1";
    private static final String REGULAR_MOD = "2";
    private static final String REGULAR_UNFUNDED_MOD = "3";
    private static final String REGULAR_DETAIL = "4";
    private static final String REGULAR_MOD_DETAIL = "5";
    private static final String CABLE_PARENT = "6";
    private static final String CABLE_DETAIL = "7";
    private static final String MACRO_PARENT = "8";
    private static final String MACRO_DETAIL = "9";
    private static final String MACRO_MOD_DETAIL = "10";
    private static final String SOLICITATION_PARENT = "11";
    private static final String SOLICITATION_DETAIL = "12";
    private static final String SOLICITATION_FAT = "17";
    private String title = null;


    private RichSelectOneChoice categorySelectOneChoice;
    private RichSelectOneChoice actionSelectOneChoice;
    private RichSelectOneChoice fundingActionTypeCode;
    private RichPanelLabelAndMessage modReferenceLabelMessage;
    private RichPanelLabelAndMessage parentReferenceLabelAndMessage;
    private RichPanelLabelAndMessage awardeeLabelMessage;
    private RichPanelLabelAndMessage projectLabelMessage;
    private RichInputListOfValues projectInputLOV;

    private RichButton createButton;
    private RichInputListOfValues modRefLOV;
    private RichInputListOfValues parentRefLOV;
    private RichPanelHeader firstFacetPanelHeader;
    private RichCommandNavigationItem regularNewCommandNavigationItem;
    private RichPanelGroupLayout secondFacetPanelGroupLayout;
    private RichSelectOneRadio plannedFYQtr;


    public CreateActionBackingBean() {
        super();
    }

    public String intialize() {
        String retVal = "error";
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        // reset();
        Object parentActionID = pageFlowScope.get("parentActionID");
        Object modActionID = pageFlowScope.get("modActionID");
        String actionTypeCode = (String) pageFlowScope.get("actionTypeCode");
        if (parentActionID != null || modActionID != null || actionTypeCode != null) {
            pageFlowScope.put("serviceInvoke", "true");
        }
        retVal = "load";
        //resetAndNavigate();
        return retVal;
    }


    public List<SelectItem> getCategoryTypeSelectItemList() {
        List<SelectItem> categoryItems = null;
        categoryItems = new ArrayList<SelectItem>();
        categoryItems.add(new SelectItem("R", "Regular", null));
        categoryItems.add(new SelectItem("M", "Macro", null));
        categoryItems.add(new SelectItem("S", "Solicitation", null));
        categoryItems.add(new SelectItem("C", "Cable", null));
        return categoryItems;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setCategorySelectOneChoice(RichSelectOneChoice categorySelectOnceChoice) {
        this.categorySelectOneChoice = categorySelectOnceChoice;
    }

    public RichSelectOneChoice getCategorySelectOneChoice() {
        return categorySelectOneChoice;
    }

    /**
     *
     * @return
     */
    public String resetAndNavigate() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        reset();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        if (createActionTypeStr != null) {
            if (REGULAR_NEW.equalsIgnoreCase(createActionTypeStr)) {
                initializeForRegularNew();
                title = "Regular New";
            } else if (REGULAR_MOD.equalsIgnoreCase(createActionTypeStr)) {
                initializeForRegularMod();
                title = "Regular Mod";
            } else if (REGULAR_UNFUNDED_MOD.equalsIgnoreCase(createActionTypeStr)) {
                initializeForRegularUnfundedMod();
                title = "Regular Unfunded Mod";
            } else if (REGULAR_DETAIL.equalsIgnoreCase(createActionTypeStr)) {
                initializeForRegularDetail();
                title = "Regular New Detail";
            } else if (REGULAR_MOD_DETAIL.equalsIgnoreCase(createActionTypeStr)) {
                initializeForRegularModDetail();
                title = "Regular Mod Detail";
            } else if (CABLE_PARENT.equalsIgnoreCase(createActionTypeStr)) {
                initializeCableParent();
                title = "Cable Parent";
            } else if (CABLE_DETAIL.equalsIgnoreCase(createActionTypeStr)) {
                initializeCableDetail();
                title = "Cable New Detail";
            } else if (MACRO_PARENT.equalsIgnoreCase(createActionTypeStr)) {
                initializeMacroParent();
                title = "Macro Parent (New/Mod/Unfunded)";
            } else if (MACRO_DETAIL.equalsIgnoreCase(createActionTypeStr)) {
                initializeMacroDetail();
                title = "Macro New Detail";
            } else if (MACRO_MOD_DETAIL.equalsIgnoreCase(createActionTypeStr)) {
                initializeMacroModDetail();
                title = "Macro Mod Detail";
            } else if (SOLICITATION_PARENT.equalsIgnoreCase(createActionTypeStr)) {
                initializeSolicitationParent();
                title = "Solicitation Parent";
            } else if (SOLICITATION_DETAIL.equalsIgnoreCase(createActionTypeStr)) {
                initializeSolicitationDetail();
                title = "Solicitation New Detail";
            }
        }
        return null;
    }

    private void reset() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pageFlowAttrs.remove("categoryTypeCode");
        pageFlowAttrs.remove("displayCategoryTypeCode");
        pageFlowAttrs.remove("actionTypeCode");
        pageFlowAttrs.remove("fundingActionType");
        pageFlowAttrs.remove("budgetFy");
        pageFlowAttrs.remove("projectNbr");
        pageFlowAttrs.remove("projectName");
        pageFlowAttrs.remove("ofdaDivisionCode");
        pageFlowAttrs.remove("projectKey");
       
        

        /*Mod Reference*/
        pageFlowAttrs.remove("modActionID");
        pageFlowAttrs.remove("mBudgetFy");
        pageFlowAttrs.remove("mActionSeqNbr");
        pageFlowAttrs.remove("mProjectName");
        pageFlowAttrs.remove("awardee");
        pageFlowAttrs.remove("awardeeCode");
        pageFlowAttrs.remove("mAwardee");
        pageFlowAttrs.remove("mAwardeeCode");
        pageFlowAttrs.remove("mAwardNbr");
        pageFlowAttrs.remove("mActionAmt");
        pageFlowAttrs.remove("mFundingType");

        modReferenceLabelMessage.setVisible(false);

        /*Parent Reference*/
        pageFlowAttrs.remove("parentActionID");
        pageFlowAttrs.remove("pOfdaDivisionCode");
        pageFlowAttrs.remove("pBudgetFy");
        pageFlowAttrs.remove("pProjectNbr");
        pageFlowAttrs.remove("pActionSeqNbr");
        pageFlowAttrs.remove("pProjectName");
        pageFlowAttrs.remove("pAwardee");
        pageFlowAttrs.remove("pFundingActionType");
        pageFlowAttrs.remove("pAwardNbr");
        pageFlowAttrs.remove("pActionAmt");
        parentReferenceLabelAndMessage.setVisible(false);

        pageFlowAttrs.remove("awardee");
        pageFlowAttrs.remove("awardeeCode");
        pageFlowAttrs.remove("plannedFYQtr");

        pageFlowAttrs.remove("facetCategoryType");

        projectInputLOV.setVisible(true);
        projectInputLOV.setReadOnly(false);

        projectLabelMessage.setVisible(true);
        fundingActionTypeCode.setVisible(true);
        fundingActionTypeCode.setReadOnly(false);

        if (pageFlowAttrs.get("serviceInvoke") != null) {
            pageFlowAttrs.remove("parentActionID");
            pageFlowAttrs.remove("modActionID");
            pageFlowAttrs.remove("actionTypeCode");
        }
        pageFlowAttrs.remove("serviceInvoke");
    }


    /**
     * Category - Readonly - Default(Regular)
     * Action Type - Readonly - Default(New)
     * FAT: not in (06,12,17)
     * Project
     * Awardee
     *
     */
    private void initializeForRegularNew() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        pageFlowAttrs.put("categoryTypeCode", "R");
        pageFlowAttrs.put("displayCategoryTypeCode", "R");

        pageFlowAttrs.put("facetCategoryType", "R");
        categorySelectOneChoice.setReadOnly(true);
        categorySelectOneChoice.setVisible(true);

        pageFlowAttrs.put("actionTypeCode", "N");
        actionSelectOneChoice.setReadOnly(true);
        actionSelectOneChoice.setVisible(true);

        retrieveFATLOV(createActionTypeStr);
        fundingActionTypeCode.setReadOnly(false);

        parentReferenceLabelAndMessage.setVisible(false);


    }

    /**
     * Category -  Readonly - Default(Regular)
     * Action Type - Readonly - Default(Mod)
     * Mod Reference:
     * Funding Action Type - Readonly
     * Project
     * Awardee- Populated from Original(Mod reference)
     *
     */
    private void initializeForRegularMod() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        pageFlowAttrs.put("categoryTypeCode", "R");
        pageFlowAttrs.put("facetCategoryType", "R");
        pageFlowAttrs.put("displayCategoryTypeCode", "R");
        categorySelectOneChoice.setReadOnly(true);
        categorySelectOneChoice.setVisible(true);

        pageFlowAttrs.put("actionTypeCode", "M");
        actionSelectOneChoice.setReadOnly(true);
        actionSelectOneChoice.setVisible(true);

        retrieveFATLOV(createActionTypeStr);
        fundingActionTypeCode.setReadOnly(true);
        fundingActionTypeCode.setVisible(true);

        modReferenceLabelMessage.setVisible(true);

        parentReferenceLabelAndMessage.setVisible(false);
    }

    /**
     * Unfunded Mod: 06,12,17,15,04,21,07
     *
     * Category - Readonly - Default(Regular)
     * Action Type - Readonly - Default(Unfunded Mod)
     * Mod Reference:
     * Funding Action Type - Readonly - Populated from Mod
     * Project
     * Awardee
     *
     */

    private void initializeForRegularUnfundedMod() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        pageFlowAttrs.put("categoryTypeCode", "R");
        pageFlowAttrs.put("facetCategoryType", "U");
        pageFlowAttrs.put("displayCategoryTypeCode", "R");
        categorySelectOneChoice.setReadOnly(true);
        categorySelectOneChoice.setVisible(true);

        pageFlowAttrs.put("actionTypeCode", "U");
        actionSelectOneChoice.setReadOnly(true);
        actionSelectOneChoice.setVisible(true);

        retrieveFATLOV(createActionTypeStr);
        fundingActionTypeCode.setReadOnly(true);
        fundingActionTypeCode.setVisible(true);
        modReferenceLabelMessage.setVisible(true);

        parentReferenceLabelAndMessage.setVisible(false);
    }

    /**
     * Action Type: Readonly  - Default( Detail)
     * Category Type: Readonly - Default (Regular)
     * Parent Reference:
     * Funding Action Type: Readonly - populated from parent Reference
     * Project: Readonly- (Info copied from Parent)
     * Awardee:
     *
     */

    private void initializeForRegularDetail() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        pageFlowAttrs.put("categoryTypeCode", "R");
        pageFlowAttrs.put("facetCategoryType", "R");
        pageFlowAttrs.put("displayCategoryTypeCode", "R");
        categorySelectOneChoice.setReadOnly(true);
        categorySelectOneChoice.setVisible(true);

        pageFlowAttrs.put("actionTypeCode", "D");
        actionSelectOneChoice.setReadOnly(true);
        actionSelectOneChoice.setVisible(true);

        retrieveFATLOV(createActionTypeStr);
        //fundingActionTypeCode.setReadOnly(true);
        //fundingActionTypeCode.setVisible(false);

        parentReferenceLabelAndMessage.setVisible(true);
        projectLabelMessage.setVisible(true);
        projectInputLOV.setReadOnly(true);

        modReferenceLabelMessage.setVisible(false);

    }

    /**
     *  Category: Readonly - Default(Regular)
     *  Action Type: Readonly - Default (Mod Detail)
     *  Mod Reference:
     *  Parent Reference:
     *  FAT: Readonly(Default)  Validate whether both Mod and Parent FAT matches
     *  Awardee:
     */

    private void initializeForRegularModDetail() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        pageFlowAttrs.put("categoryTypeCode", "R");
        pageFlowAttrs.put("facetCategoryType", "R");
        pageFlowAttrs.put("displayCategoryTypeCode", "R");
        categorySelectOneChoice.setReadOnly(true);
        categorySelectOneChoice.setVisible(true);

        pageFlowAttrs.put("actionTypeCode", "Z");
        actionSelectOneChoice.setReadOnly(true);
        actionSelectOneChoice.setVisible(true);
        projectInputLOV.setReadOnly(true);

        retrieveFATLOV(createActionTypeStr);


        modReferenceLabelMessage.setVisible(true);

        parentReferenceLabelAndMessage.setVisible(true);
        projectLabelMessage.setVisible(true);
    }

    /**
     * Category: Readonly - Default (Regular)
     * Action Type: Readonly - Default (New)
     * FAT: in( Embassy, Mission)
     * Project:
     * Awardee:
     */
    private void initializeCableParent() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        pageFlowAttrs.put("categoryTypeCode", "R");
        pageFlowAttrs.put("facetCategoryType", "C");
        pageFlowAttrs.put("displayCategoryTypeCode", "C");
        categorySelectOneChoice.setReadOnly(true);
        categorySelectOneChoice.setVisible(true);

        pageFlowAttrs.put("actionTypeCode", "N");
        actionSelectOneChoice.setReadOnly(true);
        actionSelectOneChoice.setVisible(true);

        retrieveFATLOV(createActionTypeStr);
        fundingActionTypeCode.setReadOnly(false);


        parentReferenceLabelAndMessage.setVisible(false);
    }

    /**
     * Parent Ref:
     * Action Type: Readonly - Default(Detail)
     * Project:  Readonly - populated from parent reference
     * Detail Funding Action Type:
     * Awardee:
     */
    private void initializeCableDetail() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        pageFlowAttrs.put("categoryTypeCode", "R");
        pageFlowAttrs.put("facetCategoryType", "C");
        pageFlowAttrs.put("displayCategoryTypeCode", "C");
        categorySelectOneChoice.setReadOnly(true);
        categorySelectOneChoice.setVisible(true);
        modReferenceLabelMessage.setVisible(false);
        pageFlowAttrs.put("actionTypeCode", "D");
        actionSelectOneChoice.setReadOnly(true);
        actionSelectOneChoice.setVisible(true);

        retrieveFATLOV(createActionTypeStr);
        fundingActionTypeCode.setReadOnly(false);
        projectLabelMessage.setVisible(true);
        projectInputLOV.setReadOnly(true);

        parentReferenceLabelAndMessage.setVisible(true);
    }

    /**
     * Category Type:  Readonly - Default(Macro)
     * Action Type:  in (New, Mod, Unfunded Mod)
     * FAT: In (Grant, COOP, InterAgency, Contract)
     * Project:
     * Awardee:
     *
     */

    private void initializeMacroParent() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        pageFlowAttrs.put("categoryTypeCode", "M");
        pageFlowAttrs.put("facetCategoryType", "M");
        pageFlowAttrs.put("displayCategoryTypeCode", "M");
        categorySelectOneChoice.setReadOnly(true);
        categorySelectOneChoice.setVisible(true);

        actionSelectOneChoice.setReadOnly(false);
        actionSelectOneChoice.setVisible(true);

        retrieveFATLOV(createActionTypeStr);
        fundingActionTypeCode.setReadOnly(false);

        parentReferenceLabelAndMessage.setVisible(false);
    }

    /**
     * Category Type: Readonly - Default(Macro)
     * Action Type:  Readonly - Default(Detail)
     * Parent Ref:
     * FAT:  Readonly - Default from Parent (Grant, COOP, InterAgency, Contract)
     * Project:
     * Awardee:
     */
    private void initializeMacroDetail() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        pageFlowAttrs.put("categoryTypeCode", "M");
        pageFlowAttrs.put("facetCategoryType", "M");
        pageFlowAttrs.put("displayCategoryTypeCode", "M");
        categorySelectOneChoice.setReadOnly(true);
        categorySelectOneChoice.setVisible(true);

        pageFlowAttrs.put("actionTypeCode", "D");
        actionSelectOneChoice.setReadOnly(true);
        actionSelectOneChoice.setVisible(true);
        modReferenceLabelMessage.setVisible(false);
        retrieveFATLOV(createActionTypeStr);
        fundingActionTypeCode.setReadOnly(false);
        //fundingActionTypeCode.setVisible(false);

        parentReferenceLabelAndMessage.setVisible(true);
    }

    /**
     *  Category Type:  Readonly - Default(Macro)
     * Action Type:  Readonly - Default(Mod Detail)
     * Mod Ref:
     * Parent Ref:
     * FAT: Readonly - Default from Parent (Grant, COOP, InterAgency, Contract) ? Validate whether both Mod and Parent FAT matches
     * Project:
     * Awardee:
     */
    private void initializeMacroModDetail() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        pageFlowAttrs.put("categoryTypeCode", "M");
        pageFlowAttrs.put("facetCategoryType", "M");
        pageFlowAttrs.put("displayCategoryTypeCode", "M");
        categorySelectOneChoice.setReadOnly(true);
        categorySelectOneChoice.setVisible(true);

        pageFlowAttrs.put("actionTypeCode", "Z");
        actionSelectOneChoice.setReadOnly(true);
        actionSelectOneChoice.setVisible(true);

        retrieveFATLOV(createActionTypeStr);
        fundingActionTypeCode.setReadOnly(false);
        //fundingActionTypeCode.setVisible(false);

        modReferenceLabelMessage.setVisible(true);

        parentReferenceLabelAndMessage.setVisible(true);
    }

    /**
     *  Category Type:  Readonly - Default(Macro)
     *  Action Type:  Readonly - Default(New)
     *  FAT: Readonly - Default(Solicitation)
     *  Project:
     *  Awardee:
     */

    private void initializeSolicitationParent() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        pageFlowAttrs.put("categoryTypeCode", "M");
        pageFlowAttrs.put("facetCategoryType", "S");
        pageFlowAttrs.put("displayCategoryTypeCode", "S");
        categorySelectOneChoice.setVisible(true);

        pageFlowAttrs.put("actionTypeCode", "N");
        actionSelectOneChoice.setReadOnly(true);
        actionSelectOneChoice.setVisible(true);

        retrieveFATLOV(createActionTypeStr);
        fundingActionTypeCode.setReadOnly(true);
        pageFlowAttrs.put("fundingActionType", "17");

        categorySelectOneChoice.setReadOnly(true);
        parentReferenceLabelAndMessage.setVisible(false);
    }

    /**
     *  Category Type:  Readonly - Default(Macro)
     *  Action Type:  Readonly - Default(Detail)
     *  Parent Ref:
     *  Detail FAT:
     *  Project:
     *  Awardee:
     */

    private void initializeSolicitationDetail() {
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        pageFlowAttrs.put("categoryTypeCode", "M");
        pageFlowAttrs.put("facetCategoryType", "S");
        pageFlowAttrs.put("displayCategoryTypeCode", "S");
        categorySelectOneChoice.setVisible(true);

        pageFlowAttrs.put("actionTypeCode", "D");
        actionSelectOneChoice.setReadOnly(true);
        actionSelectOneChoice.setVisible(true);

        retrieveFATLOV(createActionTypeStr);
        fundingActionTypeCode.setReadOnly(false);

        categorySelectOneChoice.setReadOnly(true);
        parentReferenceLabelAndMessage.setVisible(true);

        modReferenceLabelMessage.setVisible(false);
    }

    public List<SelectItem> getActionTypeItems() {
        List<SelectItem> actionTypeItems = new ArrayList<SelectItem>();
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();

        String createActionTypeStr = (String) pageFlowScope.get("createActionType");
        actionTypeItems.add(new SelectItem("N", "New", null));
        actionTypeItems.add(new SelectItem("M", "Mod", null));
        actionTypeItems.add(new SelectItem("U", "Unfunded Mod", null));

        if (!MACRO_PARENT.equals(createActionTypeStr)) {
            actionTypeItems.add(new SelectItem("D", "New/D", null));
            actionTypeItems.add(new SelectItem("Z", "Mod/D", null));
        }
        return actionTypeItems;
    }

    public List<SelectItem> getFundingActionTypeItems() {
        List<SelectItem> fundingActionTypeItems = new ArrayList<SelectItem>();
        return fundingActionTypeItems;
    }

    public void setActionSelectOneChoice(RichSelectOneChoice actionSelectOneChoice) {
        this.actionSelectOneChoice = actionSelectOneChoice;
    }

    public RichSelectOneChoice getActionSelectOneChoice() {
        return actionSelectOneChoice;
    }

    public void setFundingActionTypeCode(RichSelectOneChoice fundingActionTypeCode) {
        this.fundingActionTypeCode = fundingActionTypeCode;
    }

    public RichSelectOneChoice getFundingActionTypeCode() {
        return fundingActionTypeCode;
    }

    public void setAwardeeLabelMessage(RichPanelLabelAndMessage awardeeLabelMessage) {
        this.awardeeLabelMessage = awardeeLabelMessage;
    }

    public RichPanelLabelAndMessage getAwardeeLabelMessage() {
        return awardeeLabelMessage;
    }

    private void retrieveFATLOV(String createActionType) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("setCreateActionFATLov");
        exec.getParamsMap().put("createActionType", createActionType);
        exec.execute();

    }

    public void activityReturnPopupLsnr(ReturnPopupEvent returnPopupEvent) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding activityIter = bindings.findIteratorBinding("ActivityLOV1Iterator");
        RowSetIterator activityRSIter = activityIter.getRowSetIterator();
        Row r = activityRSIter.getCurrentRow();
        if (r != null) {
            String budgetFY = (String) r.getAttribute("BudgetFy");
            String projectNbr = (String) r.getAttribute("ProjectNbr");
            String divCode = (String) r.getAttribute("OfdaDivisionCode");
            String projectName = (String) r.getAttribute("ProjectName");
            pageFlowScope.put("budgetFy", budgetFY);
            pageFlowScope.put("projectNbr", projectNbr);
            pageFlowScope.put("projectName", projectName);
            pageFlowScope.put("ofdaDivisionCode", divCode);
            pageFlowScope.put("projectKey", r.getAttribute("ProjectKey"));
        }
    }

    public void modRefLaunchPopupLnsr(LaunchPopupEvent launchPopupEvent) {
        Map<String, Object> pfs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        exec = bindings.getOperationBinding("ModRefExecuteWithParams");
        exec.getParamsMap().put("p_fat", pfs.get("fundingActionType"));
        exec.getParamsMap().put("p_id", null);
        exec.execute();

    }

    public void modRefReturnPopupLnsr(ReturnPopupEvent returnPopupEvent) {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding modIter = bindings.findIteratorBinding("PAModRefView1Iterator");
        RowSetIterator modRSIter = modIter.getRowSetIterator();
        Row r = modRSIter.getCurrentRow();
        if (r != null) {

            BigDecimal modId = (BigDecimal) r.getAttribute("ModActionId");
            if (modId == null)
                modId = (BigDecimal) r.getAttribute("ActionId");
            pageFlowScope.put("modActionID", modId);
            pageFlowScope.put("mBudgetFy", r.getAttribute("BudgetFy"));
            pageFlowScope.put("mActionSeqNbr", r.getAttribute("ActionSeqNbr"));
            pageFlowScope.put("mProjectName", r.getAttribute("ProjectName"));
            pageFlowScope.put("awardee", r.getAttribute("Awardee"));
            pageFlowScope.put("awardeeCode", r.getAttribute("AwardeeCode1"));
            pageFlowScope.put("mAwardee", r.getAttribute("Awardee"));
            pageFlowScope.put("mAwardeeCode", r.getAttribute("AwardeeCode1"));
            pageFlowScope.put("mAwardNbr", r.getAttribute("AwardNbr"));
            pageFlowScope.put("mActionAmt", currencyFormat((BigDecimal) r.getAttribute("ActionAmt")));
            pageFlowScope.put("mFundingType", r.getAttribute("FundingActionType"));
            pageFlowScope.put("fundingActionType", r.getAttribute("FundingActionType"));
            fundingActionTypeCode.setValue(r.getAttribute("FundingActionType"));
            fundingActionTypeCode.setVisible(true);
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAwardeeLabelMessage());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getFundingActionTypeCode());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getModReferenceLabelMessage());

        }
    }

    public String currencyFormat(BigDecimal number) {
        String returnVal = "";
        if (null != number) {
            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CANADA);
            double doubleVal = number.doubleValue();
            returnVal = format.format(doubleVal);
        }
        return returnVal;
    }

    public void setModReferenceLabelMessage(RichPanelLabelAndMessage modReferenceLabelMessage) {
        this.modReferenceLabelMessage = modReferenceLabelMessage;
    }

    public RichPanelLabelAndMessage getModReferenceLabelMessage() {
        return modReferenceLabelMessage;
    }

    public void actionTypeCodeChangeListener(ValueChangeEvent vce) {
        // Add event code here...
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String fat = (String) pageFlowScope.get("fundingActionType");
        String aTypeCode = (String) vce.getNewValue();
        if ("M".equals(aTypeCode) || ("U".equals(aTypeCode)) || ("Z".equals(aTypeCode))) {
            modReferenceLabelMessage.setVisible(true);
        } else {
            modReferenceLabelMessage.setVisible(false);
            pageFlowScope.remove("modActionID");
            pageFlowScope.remove("mBudgetFy");
            pageFlowScope.remove("mActionSeqNbr");
            pageFlowScope.remove("mProjectName");
            pageFlowScope.remove("awardee");
            pageFlowScope.remove("awardeeCode");
            pageFlowScope.remove("mAwardNbr");
            pageFlowScope.remove("mActionAmt");
        }
    }

    public void setParentReferenceLabelAndMessage(RichPanelLabelAndMessage parentReferenceLabelAndMessage) {
        this.parentReferenceLabelAndMessage = parentReferenceLabelAndMessage;
    }

    public RichPanelLabelAndMessage getParentReferenceLabelAndMessage() {
        return parentReferenceLabelAndMessage;
    }

    public void parentRefLaunchPopupLnsr(LaunchPopupEvent launchPopupEvent) {
        Map<String, Object> pfs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pfs.get("createActionType");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        OperationBinding exec1 = bindings.getOperationBinding("getCurrentFY");
        exec1.execute();
        Integer currentFY = (Integer) exec1.getResult();
        if ("R".equals(pfs.get("facetCategoryType")))
            exec = bindings.getOperationBinding("SetRegularExecuteWithParams");
        else if ("M".equals(pfs.get("facetCategoryType")))
            exec = bindings.getOperationBinding("SetMacroExecuteWithParams");
        else if ("C".equals(pfs.get("facetCategoryType")))
            exec = bindings.getOperationBinding("SetCableExecuteWithParams");
        else if ("S".equals(pfs.get("facetCategoryType")))
            exec = bindings.getOperationBinding("SetSolicitationExecuteWithParams");

        if (SOLICITATION_DETAIL.equalsIgnoreCase(createActionTypeStr)) {
            exec.getParamsMap().put("p_fat", SOLICITATION_FAT);
        } else if ((MACRO_MOD_DETAIL.equalsIgnoreCase(createActionTypeStr) ||
                    REGULAR_MOD_DETAIL.equalsIgnoreCase(createActionTypeStr) ||
                    REGULAR_DETAIL.equalsIgnoreCase(createActionTypeStr)) && (pfs.get("fundingActionType") != null)) {
            exec.getParamsMap().put("p_fat", pfs.get("fundingActionType"));
        } else {
            exec.getParamsMap().put("p_fat", null);
        }
        exec.getParamsMap().put("p_id", null);
        exec.getParamsMap().put("p_fy", currentFY);
        exec.execute();
    }

    public void parentRefReturnPopupLnsr(ReturnPopupEvent rpe) {
        FacesContext fc = FacesContext.getCurrentInstance();
        FacesMessage fm = null;
        String messageText = null;
        Row r = null;
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding parentIter = null;
        String actionTypeCodeStr = (String) pageFlowScope.get("actionTypeCode");
        String fundingActionTypeStr = (String) pageFlowScope.get("fundingActionType");
        if ("R".equals(pageFlowScope.get("facetCategoryType")))
            parentIter = bindings.findIteratorBinding("PAParentRefView1Iterator");
        else if ("M".equals(pageFlowScope.get("facetCategoryType")))
            parentIter = bindings.findIteratorBinding("PAMarcoParentRefView1Iterator");
        else if ("C".equals(pageFlowScope.get("facetCategoryType")))
            parentIter = bindings.findIteratorBinding("PACableParentRefView1Iterator");
        else if ("S".equals(pageFlowScope.get("facetCategoryType")))
            parentIter = bindings.findIteratorBinding("PASolicitationParentRefView1Iterator");
        RowSetIterator parentRSIter = parentIter.getRowSetIterator();
        r = parentRSIter.getCurrentRow();


        if (r != null) {


            String parentId = (String) r.getAttribute("ParentActionId");
            if (parentId == null)
                parentId = r.getAttribute("ActionId").toString();
            pageFlowScope.put("parentActionID", parentId);
            String divCode = (String) r.getAttribute("OfdaDivisionCode");
            String budgetFy = (String) r.getAttribute("BudgetFy");
            String projectNbr = (String) r.getAttribute("ProjectNbr1");
            pageFlowScope.put("pOfdaDivisionCode", divCode);
            pageFlowScope.put("pBudgetFy", budgetFy);
            pageFlowScope.put("pProjectNbr", projectNbr);
            pageFlowScope.put("pActionSeqNbr", r.getAttribute("ActionSeqNbr"));
            pageFlowScope.put("pProjectName", r.getAttribute("ProjectName"));
            pageFlowScope.put("pAwardee", r.getAttribute("Awardee"));
            pageFlowScope.put("pAwardNbr", r.getAttribute("AwardNbr"));
            pageFlowScope.put("pFundingActionType", r.getAttribute("FundingActionType"));
            pageFlowScope.put("pActionAmt", currencyFormat((BigDecimal) r.getAttribute("ActionAmt")));
            if (REGULAR_DETAIL.equals(pageFlowScope.get("createActionType")) ||
                REGULAR_MOD_DETAIL.equals(pageFlowScope.get("createActionType")) ||
                CABLE_DETAIL.equals(pageFlowScope.get("createActionType")) ||
                SOLICITATION_DETAIL.equals(pageFlowScope.get("createActionType"))) {
                pageFlowScope.put("budgetFy", budgetFy);
                pageFlowScope.put("projectNbr", projectNbr);
                pageFlowScope.put("projectName", r.getAttribute("ProjectName"));
                pageFlowScope.put("ofdaDivisionCode", divCode);
                pageFlowScope.put("projectKey", budgetFy + "-" + projectNbr);
                projectLabelMessage.setVisible(true);
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getProjectLabelMessage());

            }
            if ("R".equals(pageFlowScope.get("displayCategoryTypeCode")) ||
                "M".equals(pageFlowScope.get("displayCategoryTypeCode"))) {
                fundingActionTypeCode.setValue(r.getAttribute("FundingActionType"));
                fundingActionTypeCode.setVisible(true);
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getFundingActionTypeCode());
            }


            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getParentReferenceLabelAndMessage());

        } else {
            validateParentReference();
        }
    }

    public void awardeeReturnPopupLsnr(ReturnPopupEvent returnPopupEvent) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding activityIter = bindings.findIteratorBinding("AwardeeLOV1Iterator");
        RowSetIterator activityRSIter = activityIter.getRowSetIterator();
        Row r = activityRSIter.getCurrentRow();
        if (r != null) {
            String awardee = (String) r.getAttribute("AwardeeName");
            if (r.getAttribute("AwardeeAcronym") != null)
                awardee += " (" + r.getAttribute("AwardeeAcronym") + ")";
            pageFlowScope.put("awardee", awardee);
            pageFlowScope.put("awardeeCode", r.getAttribute("AwardeeCode"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAwardeeLabelMessage());
        }
    }

    public void createAction() {
        Map<String, Object> pfs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        Map<String, Object> t = new HashMap<String, Object>();
        StringBuffer sb = new StringBuffer();
        if (("D".equals(pfs.get("actionTypeCode")) || "Z".equals(pfs.get("actionTypeCode"))) &&
            pfs.get("parentActionID") != null) {
            /* Removing unwanted logic as the parent return launcher sets the activity
             if (!"M".equals(pfs.get("categoryTypeCode")) || !"17".equals(pfs.get("fundingActionType"))) {
                t.put("budgetFy", pfs.get("pBudgetFy"));
                t.put("ofdaDivisionCode", pfs.get("pOfdaDivisionCode"));
                t.put("projectNbr", pfs.get("pProjectNbr"));
            } else {
                t.put("budgetFy", pfs.get("budgetFy"));
                t.put("ofdaDivisionCode", pfs.get("ofdaDivisionCode"));
                t.put("projectNbr", pfs.get("projectNbr"));
            }*/
            t.put("budgetFy", pfs.get("budgetFy"));
            t.put("ofdaDivisionCode", pfs.get("ofdaDivisionCode"));
            t.put("projectNbr", pfs.get("projectNbr"));
            t.put("parentActionID", pfs.get("parentActionID"));
        } else if (pfs.get("budgetFy") != null) {
            t.put("budgetFy", pfs.get("budgetFy"));
            t.put("ofdaDivisionCode", pfs.get("ofdaDivisionCode"));
            t.put("projectNbr", pfs.get("projectNbr"));
        }
        if ("D".equals(pfs.get("actionTypeCode")) &&
            ("06".equals(pfs.get("fundingActionType")) || "12".equals(pfs.get("fundingActionType")) ||
             "17".equals(pfs.get("fundingActionType"))))
            t.put("fundingActionType",
                  pfs.get("fundingActionType")); //t.put("fundingActionType", pfs.get("detailFundingActionType"));
        else
            t.put("fundingActionType", pfs.get("fundingActionType"));

        t.put("actionTypeCode", pfs.get("actionTypeCode"));
        t.put("categoryTypeCode", pfs.get("categoryTypeCode"));

        if (pfs.get("modActionID") != null &&
            ("M".equals(pfs.get("actionTypeCode")) || "U".equals(pfs.get("actionTypeCode")) ||
             "Z".equals(pfs.get("actionTypeCode")))) {
            t.put("modActionID", pfs.get("modActionID"));
            t.put("awardNbr", pfs.get("mAwardNbr"));
        }
              
        if (pfs.get("awardeeCode") != null)
            t.put("awardeeCode", pfs.get("awardeeCode"));
        
        if (pfs.get("plannedFYQtr") != null)
            t.put("plannedFYQtr", pfs.get("plannedFYQtr"));  
        
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("createAction");
        exec.getParamsMap().put("list", t);
        exec.execute();
        Long actionID = (Long) exec.getResult();
        pfs.put("actionID", actionID);
        exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.getParamsMap().put("p_action_id", actionID);
        exec.execute();
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        Row r = iter.getCurrentRow();
        if (r != null) {
            Long asLong = (Long) r.getAttribute("ActionSeqNbr");
            String pnbr = (String) r.getAttribute("ProjectNbr");
            Integer budgetFy = (Integer) r.getAttribute("BudgetFy");
            sb.append(budgetFy.toString());
            sb.append("-");
            sb.append(pnbr);
            sb.append("-");
            sb.append(asLong.toString());
            pfs.put("id", sb.toString());
        }
    }

    public void loadPage(ClientEvent clientEvent) {
        // Add event code here...
        Map<String, Object> pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String createActionTypeStr = (String) pageFlowAttrs.get("createActionType");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec1 = bindings.getOperationBinding("getCurrentFY");
        exec1.execute();
        Integer currentFY = (Integer) exec1.getResult();
        /*if (createActionTypeStr == null) {
            pageFlowAttrs.put("createActionType", REGULAR_NEW);
            resetAndNavigate();
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getRegularNewCommandNavigationItem());
            //AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSecondFacetPanelHeader());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSecondFacetPanelGroupLayout());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getRegularNewCommandNavigationItem().getParent().getParent());

        } else*/
        if (pageFlowAttrs.get("serviceInvoke") != null) {
            if (REGULAR_MOD.equalsIgnoreCase(createActionTypeStr)) {
                initializeForRegularMod();
                title = "Regular Mod";
            } else if (REGULAR_UNFUNDED_MOD.equalsIgnoreCase(createActionTypeStr)) {
                initializeForRegularUnfundedMod();
                title = "Regular Unfunded Mod";
            } else if (REGULAR_DETAIL.equalsIgnoreCase(createActionTypeStr)) {
                initializeForRegularDetail();
                title = "Regular New Detail";
            } else if (REGULAR_MOD_DETAIL.equalsIgnoreCase(createActionTypeStr)) {
                initializeForRegularModDetail();
                title = "Regular Mod Detail";
            } else if (CABLE_DETAIL.equalsIgnoreCase(createActionTypeStr)) {
                initializeCableDetail();
                title = "Cable New Detail";
            } else if (MACRO_PARENT.equalsIgnoreCase(createActionTypeStr)) {
                initializeMacroParent();
                title = "Macro Parent  New/Mod/Unfunded";
            } else if (MACRO_DETAIL.equalsIgnoreCase(createActionTypeStr)) {
                initializeMacroDetail();
                title = "Macro New Detail";
            } else if (MACRO_MOD_DETAIL.equalsIgnoreCase(createActionTypeStr)) {
                initializeMacroModDetail();
                title = "Macro Mod Detail";
            } else if (SOLICITATION_DETAIL.equalsIgnoreCase(createActionTypeStr)) {
                initializeSolicitationDetail();
                title = "Solicitation New Detail";
            }
            Object parentActionID = pageFlowAttrs.get("parentActionID");
            Object modActionID = pageFlowAttrs.get("modActionID");
            String actionTypeCode = (String) pageFlowAttrs.get("actionTypeCode");

            OperationBinding exec = null;
            if (modActionID != null) {
                exec = bindings.getOperationBinding("ModRefExecuteWithParams");
                exec.getParamsMap().put("p_id", modActionID);
                exec.execute();
                modRefReturnPopupLnsr(null);
            }
            if (parentActionID != null) {
                if ("R".equals(pageFlowAttrs.get("facetCategoryType")))
                    exec = bindings.getOperationBinding("SetRegularExecuteWithParams");
                else if ("M".equals(pageFlowAttrs.get("facetCategoryType")))
                    exec = bindings.getOperationBinding("SetMacroExecuteWithParams");
                else if ("C".equals(pageFlowAttrs.get("facetCategoryType")))
                    exec = bindings.getOperationBinding("SetCableExecuteWithParams");
                else if ("S".equals(pageFlowAttrs.get("facetCategoryType")))
                    exec = bindings.getOperationBinding("SetSolicitationExecuteWithParams");
                exec.getParamsMap().put("p_id", parentActionID);
                exec.getParamsMap().put("p_fy", currentFY);
                exec.execute();
                parentRefReturnPopupLnsr(null);
            }

            //AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSecondFacetPanelHeader());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSecondFacetPanelGroupLayout());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getCreateButton());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getFundingActionTypeCode());

        }


    }


    public void setProjectLabelMessage(RichPanelLabelAndMessage projectLabelMessage) {
        this.projectLabelMessage = projectLabelMessage;
    }

    public RichPanelLabelAndMessage getProjectLabelMessage() {
        return projectLabelMessage;
    }

    public void setProjectInputLOV(RichInputListOfValues projectInputLOV) {
        this.projectInputLOV = projectInputLOV;
    }

    public RichInputListOfValues getProjectInputLOV() {
        return projectInputLOV;
    }


    public void setCreateButton(RichButton createButton) {
        this.createButton = createButton;
    }

    public RichButton getCreateButton() {
        return createButton;
    }


    public String validateCreateAndProceed() {
        // Add event code here...
        Map<String, Object> pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        boolean actRefValBool = validateActionType();
        boolean modRefValBool = validateModReference();
        boolean parentRefValBool = validateParentReference();
        boolean fatValBool = validateFundingActionType();
        boolean projRefValBool = validateProjectReference();
        boolean qtrRefValBool = validatePlannedQtr();

        if (fatValBool || actRefValBool || modRefValBool || parentRefValBool || projRefValBool || qtrRefValBool) {

            FacesContext context = FacesContext.getCurrentInstance();
            /*  context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_WARN, "Please Enter the Mandatory Fields", null));
*/
        } else {
            Boolean bool =
                (Boolean) evaluateEL("#{ ( empty pageFlowScope.createActionType)   or ( empty pageFlowScope.plannedFYQtr)   or  ( ((empty pageFlowScope.fundingActionType) or( empty pageFlowScope.actionTypeCode) or ((pageFlowScope.actionTypeCode eq 'M' or pageFlowScope.actionTypeCode eq 'Z'or pageFlowScope.actionTypeCode eq 'U') and pageFlowScope.modActionID eq null) or ((pageFlowScope.actionTypeCode eq 'D' or pageFlowScope.actionTypeCode eq 'Z') and pageFlowScope.parentActionID eq null) or ( (pageFlowScope.actionTypeCode eq 'N' or pageFlowScope.actionTypeCode eq 'M' or pageFlowScope.actionTypeCode eq 'U' or ((pageFlowScope.actionTypeCode eq 'D' or pageFlowScope.actionTypeCode eq 'Z') and pageFlowScope.categoryTypeCode eq 'M')) and pageFlowScope.budgetFy eq null)))}");
            if (bool.booleanValue() == false) {
                return "create";
            } else {
                FacesContext context = FacesContext.getCurrentInstance();
                /*  context.addMessage(null,
                                   new FacesMessage(FacesMessage.SEVERITY_WARN, "Please Enter the Mandatory Fields", null));*/
            }
        }
      

        return null;
    }

    private static Object evaluateEL(String el) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ELContext elContext = facesContext.getELContext();
        ExpressionFactory expressionFactory = facesContext.getApplication().getExpressionFactory();
        ValueExpression exp = expressionFactory.createValueExpression(elContext, el, Object.class);
        return exp.getValue(elContext);
    }

    /**
     * Mod reference must be selected
     * @return
     */
    private boolean validateModReference() {
        boolean retVal = false;
        Map<String, Object> pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        FacesContext context = FacesContext.getCurrentInstance();
        String actionTypeCode = (String) pageFlowAttrs.get("actionTypeCode");
        Object modActionID = pageFlowAttrs.get("modActionID");
        FacesMessage fm = null;
        String messageText = null;
        if (actionTypeCode != null &&
            (("M".equalsIgnoreCase(actionTypeCode)) || ("U".equalsIgnoreCase(actionTypeCode)) ||
             ("Z".equalsIgnoreCase(actionTypeCode)))) {
            if (modActionID == null) {
                messageText = "Mod Reference must be selected";
                fm = new FacesMessage("Invalid Mod Reference", messageText);
                fm.setSeverity(FacesMessage.SEVERITY_ERROR);
                context.addMessage(getModRefLOV().getClientId(context), fm);
                retVal = true;
            }
        }
        return retVal;
    }

    /**
     *
     * Funded Detail Actions for Macros and Solicitation cannot be created for Previous years
     *
     *@return
     */

    private boolean validateParentReference() {
        boolean retVal = false;
        Row r = null;
        Map<String, Object> pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        FacesContext context = FacesContext.getCurrentInstance();
        String actionTypeCode = (String) pageFlowAttrs.get("actionTypeCode");
        Object parentActionID = pageFlowAttrs.get("parentActionID");
        Object pBudgetFy = pageFlowAttrs.get("pBudgetFy");
        Object pProjectNbr = pageFlowAttrs.get("pProjectNbr");
        Object pActionSeqNbr = pageFlowAttrs.get("pActionSeqNbr");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Integer budgetFyInt = null;
        FacesMessage fm = null;
        String messageText = null;
        if (actionTypeCode != null &&
            (("D".equalsIgnoreCase(actionTypeCode)) || ("Z".equalsIgnoreCase(actionTypeCode)))) {
            if (parentActionID == null) {
                messageText = "Parent Reference must be selected";
                fm = new FacesMessage("Invalid Parent Reference", messageText);
                fm.setSeverity(FacesMessage.SEVERITY_ERROR);
                context.addMessage(getParentRefLOV().getClientId(context), fm);
                retVal = true;
            } else {
                OperationBinding exec1 = bindings.getOperationBinding("getCurrentFY");
                exec1.execute();
                Integer currentFY = (Integer) exec1.getResult();
                if (pBudgetFy == null || pProjectNbr == null || pActionSeqNbr == null) {
                    messageText = "Select a valid Parent Reference";

                }
                OperationBinding exec = bindings.getOperationBinding("FindActionIDExecuteWithParams");
                exec.getParamsMap().put("p_aid", parentActionID);
                exec.execute();
                DCIteratorBinding iter = bindings.findIteratorBinding("PATableFindByActionIDIterator");
                RowSetIterator rsi = iter.getRowSetIterator();
                r = rsi.getCurrentRow();
                if (r != null) {
                    String cf = (String) r.getAttribute("CommitFlag");
                    String bf = (String) r.getAttribute("BudgetFlag");
                    String fat = (String) r.getAttribute("FundingActionType");
                    String budgetFyStr = (String) r.getAttribute("BudgetFy");
                    Integer budgetFy = new Integer(budgetFyStr);
                    if ("N".equals(cf) && "N".equals(bf) && budgetFy < currentFY) {
                        messageText = "Detail Action(Funded) cannot be created for Previous year";

                    }
                }
                if (messageText != null) {
                    fm = new FacesMessage("Invalid Parent Reference", messageText);
                    fm.setSeverity(FacesMessage.SEVERITY_ERROR);
                    context.addMessage(getParentRefLOV().getClientId(context), fm);
                    retVal = true;
                }
            }
        }
        return retVal;
    }

    /**
     * Funding Action type is mandatory for action creation
     * @return
     */
    private boolean validateFundingActionType() {
        Map<String, Object> pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        FacesContext context = FacesContext.getCurrentInstance();
        String fundingActionType = (String) pageFlowAttrs.get("fundingActionType");
        String categoryTypeCode = (String) pageFlowAttrs.get("categoryTypeCode");
        FacesMessage fm = null;
        String messageText = null;
        boolean retVal = false;
        if (fundingActionType == null) {
            messageText = "Funding Action Type must be selected";
            fm = new FacesMessage("Invalid Funding Action Type", messageText);
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage(getFundingActionTypeCode().getClientId(context), fm);
            retVal = true;
        }
        return retVal;
    }

    /**
     * Action Type is mandatory for action creation
     * @return
     */
    private boolean validateActionType() {
        Map<String, Object> pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        FacesContext context = FacesContext.getCurrentInstance();
        String fundingActionType = (String) pageFlowAttrs.get("actionTypeCode");
        FacesMessage fm = null;
        String messageText = null;
        boolean retVal = false;
        if (fundingActionType == null) {
            messageText = "Action Type must be selected";
            fm = new FacesMessage("Invalid Action Type", messageText);
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage(getActionSelectOneChoice().getClientId(context), fm);
            retVal = true;
        }
        return retVal;
    }
    
    private boolean validatePlannedQtr() {
        Map<String, Object> pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        FacesContext context = FacesContext.getCurrentInstance();
        Object plannedFYQtr =  pageFlowAttrs.get("plannedFYQtr");
        FacesMessage fm = null;
        String messageText = null;
        boolean retVal = false;
        if (plannedFYQtr == null) {
            messageText = "Planned FY QTR must be selected";
            fm = new FacesMessage("Invalid Planned FY QTR", messageText);
            fm.setSeverity(FacesMessage.SEVERITY_ERROR); 
            context.addMessage(getPlannedFYQtr().getParent().getParent().getClientId(context), fm);
            retVal = true;
        }
        return retVal;
    }

    /**
     *
     * New, Mod and Unfunded Mod actions cannot be created for previous fiscal year
     * Detail and Mod Details cannot be created for Macro actions
     *
     *@return
     */
    private boolean validateProjectReference() {
        boolean retVal = false;
        Map<String, Object> pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        FacesContext context = FacesContext.getCurrentInstance();
        String categoryTypeCode = (String) pageFlowAttrs.get("categoryTypeCode");
        String actionTypeCode = (String) pageFlowAttrs.get("actionTypeCode");
        String budgetFy = (String) pageFlowAttrs.get("budgetFy");
        String projectNbr = (String) pageFlowAttrs.get("projectNbr");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec1 = bindings.getOperationBinding("getCurrentFY");
        exec1.execute();
        Integer currentFY = (Integer) exec1.getResult();
        Integer budgetFyInt = null;
        FacesMessage fm = null;
        String messageText = null;
        if (budgetFy == null || projectNbr == null) {
            messageText = "Project must be selected";
            fm = new FacesMessage("Invalid Project", messageText);
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage(getProjectInputLOV().getClientId(context), fm);
            retVal = true;
        }
        if (actionTypeCode != null &&
            ((("N".equalsIgnoreCase(actionTypeCode)) || ("M".equalsIgnoreCase(actionTypeCode)) ||
              ("U".equalsIgnoreCase(actionTypeCode))) ||
             (categoryTypeCode != null &&
              ("M".equals(categoryTypeCode) &&
               (("D".equalsIgnoreCase(actionTypeCode)) || ("Z".equalsIgnoreCase(actionTypeCode))))))) {


            if (budgetFy != null) {
                budgetFyInt = new Integer(budgetFy);
                if (budgetFyInt < currentFY) {
                    messageText = "Action cannot be created for Previous Fiscal Year: " + budgetFy;
                    fm = new FacesMessage("Invalid Project", messageText);
                    fm.setSeverity(FacesMessage.SEVERITY_ERROR);
                    context.addMessage(getProjectInputLOV().getClientId(context), fm);
                    retVal = true;
                }
            }


        }
        return retVal;
    }

    public void setModRefLOV(RichInputListOfValues modRefLOV) {
        this.modRefLOV = modRefLOV;
    }

    public RichInputListOfValues getModRefLOV() {
        return modRefLOV;
    }


    public void setParentRefLOV(RichInputListOfValues parentRefLOV) {
        this.parentRefLOV = parentRefLOV;
    }

    public RichInputListOfValues getParentRefLOV() {
        return parentRefLOV;
    }

    public void setFirstFacetPanelHeader(RichPanelHeader firstFacetPanelHeader) {
        this.firstFacetPanelHeader = firstFacetPanelHeader;
    }

    public RichPanelHeader getFirstFacetPanelHeader() {
        return firstFacetPanelHeader;
    }

    public void setRegularNewCommandNavigationItem(RichCommandNavigationItem regularNewCommandNavigationItem) {
        this.regularNewCommandNavigationItem = regularNewCommandNavigationItem;
    }

    public RichCommandNavigationItem getRegularNewCommandNavigationItem() {
        return regularNewCommandNavigationItem;
    }

    public void setSecondFacetPanelGroupLayout(RichPanelGroupLayout secondFacetPanelGroupLayout) {
        this.secondFacetPanelGroupLayout = secondFacetPanelGroupLayout;
    }

    public RichPanelGroupLayout getSecondFacetPanelGroupLayout() {
        return secondFacetPanelGroupLayout;
    }

    public void setPlannedFYQtr(RichSelectOneRadio plannedFYQtr) {
        System.out.println();
        this.plannedFYQtr = plannedFYQtr;
    }

    public RichSelectOneRadio getPlannedFYQtr() {
        return plannedFYQtr;
    }
}
