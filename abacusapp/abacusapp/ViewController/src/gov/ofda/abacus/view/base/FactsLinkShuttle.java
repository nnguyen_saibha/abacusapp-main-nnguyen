package gov.ofda.abacus.view.base;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import java.util.Map;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class FactsLinkShuttle implements Serializable {
    @SuppressWarnings("compatibility:-8516014626270470525")
    private static final long serialVersionUID = 1L;
    
    /**
     *
     * @param selectedValues
     * @param fkIteratorName - nested VO iterator
     * @param fk1AttName
     * @param deleteOpName - delete operation should be defined in page bindings
     * @param createInsertOpName - createInsert operation should be defined in page bindings
     */
    public static void setObjectivesAreaSelected(List selectedValues, String iteratorName, String fk1AttName, String deleteOpName, String createInsertOpName, String objectiveCode, Map map) {
        
        if (selectedValues == null)
            selectedValues = new ArrayList(0);
        DCBindingContainer dcbindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterator = dcbindings.findIteratorBinding(iteratorName);
        OperationBinding deleteOp = dcbindings.getOperationBinding(deleteOpName);
        OperationBinding createInsertOp = dcbindings.getOperationBinding(createInsertOpName);

        Row[] rowSet = iterator.getAllRowsInRange();
        for (Row row : rowSet) {
            Object fk2Att = row.getAttribute(fk1AttName);
            if (!selectedValues.contains(fk2Att)) {
                iterator.setCurrentRowWithKey(row.getKey().toStringFormat(true));
                deleteOp.execute();
            } else {
                selectedValues.remove(fk2Att);
            }
        }

        for (Object val : selectedValues) {
            createInsertOp.execute();
            Row row = iterator.getCurrentRow();
            row.setAttribute(fk1AttName, val);
            row.setAttribute("ObjectiveCode", objectiveCode);
            row.setAttribute("LinkType", 'A');
            row.setAttribute("FactsCode", map.get(val));
        }
    }

    public static void setObjectivesElementSelected(List selectedValues, String iteratorName, String fk1AttName, String deleteOpName, String createInsertOpName, String objectiveCode, Map map,
                                                    String areaCode) {
        
        if (selectedValues == null)
            selectedValues = new ArrayList(0);
        DCBindingContainer dcbindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterator = dcbindings.findIteratorBinding(iteratorName);
        OperationBinding deleteOp = dcbindings.getOperationBinding(deleteOpName);
        OperationBinding createInsertOp = dcbindings.getOperationBinding(createInsertOpName);

        Row[] rowSet = iterator.getAllRowsInRange();
        for (Row row : rowSet) {
            Object fk2Att = row.getAttribute(fk1AttName);
            if (!selectedValues.contains(fk2Att)) {
                iterator.setCurrentRowWithKey(row.getKey().toStringFormat(true));
                deleteOp.execute();
            } else {
                selectedValues.remove(fk2Att);
            }
        }

        for (Object val : selectedValues) {
            createInsertOp.execute();
            Row row = iterator.getCurrentRow();
            row.setAttribute(fk1AttName, val);
            row.setAttribute("ObjectiveCode", objectiveCode);
            row.setAttribute("LinkType", 'E');
            row.setAttribute("AreaCode", areaCode);
            row.setAttribute("FactsCode", map.get(val));
            
        }

    }

    public static void setObjectivesSubElementSelected(List selectedValues, String iteratorName, String fk1AttName, String deleteOpName, String createInsertOpName, String objectiveCode, Map map,
                                                    String areaCode, String elementCode) {
        
        if (selectedValues == null)
            selectedValues = new ArrayList(0);
        DCBindingContainer dcbindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterator = dcbindings.findIteratorBinding(iteratorName);
        OperationBinding deleteOp = dcbindings.getOperationBinding(deleteOpName);
        OperationBinding createInsertOp = dcbindings.getOperationBinding(createInsertOpName);

        Row[] rowSet = iterator.getAllRowsInRange();
        for (Row row : rowSet) {
            Object fk2Att = row.getAttribute(fk1AttName);
            if (!selectedValues.contains(fk2Att)) {
                iterator.setCurrentRowWithKey(row.getKey().toStringFormat(true));
                deleteOp.execute();
            } else {
                selectedValues.remove(fk2Att);
            }
        }

        for (Object val : selectedValues) {
            createInsertOp.execute();
            Row row = iterator.getCurrentRow();
            row.setAttribute(fk1AttName, val);
            row.setAttribute("ObjectiveCode", objectiveCode);
            row.setAttribute("LinkType", 'S');
            row.setAttribute("ElementCode", elementCode);
            row.setAttribute("AreaCode", areaCode);
            row.setAttribute("FactsCode", map.get(val));
            
        }

    }
}
