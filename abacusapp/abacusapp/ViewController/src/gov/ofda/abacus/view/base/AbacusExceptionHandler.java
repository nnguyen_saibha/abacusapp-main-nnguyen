package gov.ofda.abacus.view.base;

import gov.ofda.abacus.view.bean.message.MessageBean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.context.ExceptionHandler;
/**
 * AbacusExceptionHandler.java
 * Purpose: To handle exceptions and suppress them.
 * @version 1.0 09/09/2012
 */
public class AbacusExceptionHandler extends ExceptionHandler {
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.view.base.AbacusExceptionHandler.class);

    /**
     * Class Constructor
     */
    public AbacusExceptionHandler() {
        super();
    }

    /**
     * handles exceptions.
     * @param facesContext
     * @param throwable
     * @param phaseId
     * @throws Throwable
     */
    @Override
    public void handleException(FacesContext facesContext, Throwable throwable, PhaseId phaseId) throws Throwable {
        String error_msg;
        error_msg=throwable.getMessage();
        //Handle Active data exception when user tries import to excel and then tries to navigate.
        if(error_msg != null &&
            error_msg.indexOf("ADF_FACES-60003") > -1) {
            logger.log(logger.TRACE,"Handled: ADF_FACES-60003");
        }
        else
        {
            //Default ADFc exception handler
            
           /* String msg[]=(""+throwable.getCause()).split(":");
            if(msg.length>1)
                facesContext.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,msg[1],null));
            else
                facesContext.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,""+throwable.getCause(),null));*/
            
            if(error_msg != null && error_msg.contains("JBO-25014")){
                facesContext.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Another user has changed the Data. Cancel your changes and try again",null));
                logger.log(logger.TRACE,"Another user has changed the Data. Cancel your changes and try again" + error_msg);
            }else{
                facesContext.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Unexpected Error. If issue persists, contact Abacus Team (abacus@ofda.gov)",null));
                MessageBean mb=new MessageBean();
                mb.sendAdminEmail("Unexpected Issue",throwable.getMessage());
                throw new Throwable("Unexpected Error. If issue persists, contact Abacus Team (abacus@ofda.gov)");
            }
        }
    }
}
