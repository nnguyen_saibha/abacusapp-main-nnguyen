package gov.ofda.abacus.view.bean.action;

import gov.ofda.abacus.view.base.UIControl;

import gov.ofda.abacus.view.bean.AppBean;

import java.io.Serializable;

import java.io.UnsupportedEncodingException;

import java.net.URLEncoder;

import java.util.ArrayList;
import java.util.List;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;

import oracle.binding.OperationBinding;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.jbo.Key;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.util.ComponentReference;

/**
 * ProposalReceivedBean is the managed bean(View Scope) for the Proposal Received Tab
 */
public class ProposalReceivedBean extends UIControl implements Serializable {
    @SuppressWarnings("compatibility:4487405085275575094")
    private static final long serialVersionUID = 1L;

    List<String> manageRgList = new ArrayList<String>();
    private ComponentReference rgListAddRemoveCol;
    private ComponentReference rgListSelectedListTable;
    private ComponentReference rgLookupTable;

    public void setRgListSelectedListTable(UIComponent rgListSelectedListTable) {
        this.rgListSelectedListTable = ComponentReference.newUIComponentReference(rgListSelectedListTable);
    }

    public UIComponent getRgListSelectedListTable() {
        return rgListSelectedListTable == null ? null : rgListSelectedListTable.getComponent();
    }

    public ProposalReceivedBean() {
        super();
    }

    public String addRGToList(ActionEvent ae) {
        String rgCode = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("RgCode");
        if (!manageRgList.contains(rgCode)) {
            manageRgList.add(rgCode);
        } else {
            manageRgList.remove(rgCode);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(rgListAddRemoveCol.getComponent());
        AdfFacesContext.getCurrentInstance().addPartialTarget(rgListSelectedListTable.getComponent());
        return null;
    }


    public void setRgListAddRemoveCol(UIComponent rgListAddRemoveCol) {
        this.rgListAddRemoveCol = ComponentReference.newUIComponentReference(rgListAddRemoveCol);
    }

    public UIComponent getRgListAddRemoveCol() {
        return rgListAddRemoveCol == null ? null : rgListAddRemoveCol.getComponent();
    }


    public void setManageRgList(List<String> manageRgList) {
        this.manageRgList = manageRgList;
    }

    public List<String> getManageRgList() {
        return manageRgList;
    }

    /**
     * This method handles the dialog listener for Restricted Goods.
     * 
     * @param dialogEvent
     */
    public void manageRGDialogLsnr(DialogEvent dialogEvent) {
        // Add event code here...
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            //Get current row key to keep track of currrent row.
            DCIteratorBinding iter = bindings.findIteratorBinding("EditPaRgView1Iterator");
            Key key = null;
            if (iter != null && iter.getCurrentRow() != null)
                key = iter.getCurrentRow().getKey();

            OperationBinding ctrl = bindings.getOperationBinding("updateRGs");
            ctrl.getParamsMap().put("rgList", this.getManageRgList());
            Boolean t = (Boolean) ctrl.execute();
            if (this.getManageRgList() != null) {
                OperationBinding exec = bindings.getOperationBinding("EditPaRgView1Execute");
                exec.execute();
                if (key != null) {
                    iter =  bindings.findIteratorBinding("EditPaRgView1Iterator");
                    RowSetIterator rsi = iter.getRowSetIterator();
                    if (rsi.findByKey(key, 1).length > 0)
                        iter.setCurrentRowWithKey(key.toStringFormat(true));
                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(rgLookupTable.getComponent().getParent());
            }
        }
    }

    /**
     * @param popupFetchEvent
     * Called when Manage Location popup is fetched.
     * It will get the existing location list and add the selected location list.
     */
    public void manageRGFetchLsnr(PopupFetchEvent popupFetchEvent) {
        // Add event code here...getLocationsList
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding getList = bindings.getOperationBinding("getRGList");
        manageRgList = (List<String>) getList.execute();
        //setPGRevIdForRestrictedGoods
        OperationBinding exec=bindings.getOperationBinding("setPGRevIdForRestrictedGoods");
        exec.execute();                                                                                           
        AdfFacesContext.getCurrentInstance().addPartialTarget(rgLookupTable.getComponent());
        AdfFacesContext.getCurrentInstance().addPartialTarget(rgListSelectedListTable.getComponent());
    }


    /**
     * @param popupCanceledEvent
     * Called when popup is cancelled or closed.
     * It will clear the filter for select location table
     */
    public void manageRGCancelLsnr(PopupCanceledEvent popupCanceledEvent) {
        clearFilter((RichTable) rgLookupTable.getComponent());
    }

    public void setRgLookupTable(UIComponent rgLookupTable) {
        this.rgLookupTable = ComponentReference.newUIComponentReference(rgLookupTable);
        ;
    }

    public UIComponent getRgLookupTable() {
        return rgLookupTable == null ? null : rgLookupTable.getComponent();
    }

    /**
     * This method populates the sharepoint link to view the proposal details
     *  eg: https://intranet.ofda.gov/org/demo/Pages/apiPrs.aspx?Action=redirect&AS=33&Project=5489999&FY=2015
     *  PROPOSAL_RCVD_SPOINT_URL - Value is stored in abacus constants file
     *  AS - maps to actionSeqNbr of procurement action
     *  Project - ProjectNbr from procurement action
     *  FY - Fiscal year of the action
     *
     * @return URL to view the proposal information
     */
    public String getSharepointProposalLink() {
        String retVal = null;
        StringBuffer sb = new StringBuffer();
        Map aScopeMap = ADFContext.getCurrent().getApplicationScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        AppBean appBean = (AppBean) aScopeMap.get("appBean");

        String proposalSharepointURL = appBean.getConstants().get("PROPOSAL_RCVD_SPOINT_URL");
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        Row rw = iter.getCurrentRow();
        if (rw != null) {
            Integer budgetFyInt = (Integer) rw.getAttribute("BudgetFy");
            Long actionSeqNbrLong = (Long) rw.getAttribute("ActionSeqNbr");
            String projectNbr = (String) rw.getAttribute("ProjectNbr");
            sb.append(proposalSharepointURL);
            sb.append("&AS=");
            sb.append(actionSeqNbrLong.toString());
            sb.append("&Project=");
            sb.append(projectNbr);
            sb.append("&FY=");
            sb.append(budgetFyInt.toString());
            retVal = sb.toString();
        }
        return retVal;
    }
    /**
     * This method populates the sharepoint link to create/view the proposal details
     *  eg: https://intranet.ofda.gov/org/demo/Pages/apiPrs.aspx?Action=redirect&AS=33&Project=5489999&FY=2015
     *  PROPOSAL_RCVD_SPOINT_URL - Value is stored in abacus constants file
     *  AS - maps to actionSeqNbr of procurement action
     *  Project - ProjectNbr from procurement action
     *  FY - Fiscal year of the action
     *
     * @return URL to view the proposal information
     */
    public String getSharepointProposalCreateLink(){
        String retVal = null;
        StringBuffer sb = new StringBuffer();
        Map aScopeMap = ADFContext.getCurrent().getApplicationScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        AppBean appBean = (AppBean) aScopeMap.get("appBean");

        String proposalSharepointURL = "https://intranet.ofda.gov/org/proposal/Proposal2017/default.aspx?Action=redirect";
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        Row rw = iter.getCurrentRow();
        if (rw != null) {
            Integer budgetFyInt = (Integer) rw.getAttribute("BudgetFy");
            Long actionSeqNbrLong = (Long) rw.getAttribute("ActionSeqNbr");
            String projectNbr = (String) rw.getAttribute("ProjectNbr");
            sb.append(proposalSharepointURL);
            sb.append("&AS=");
            sb.append(actionSeqNbrLong.toString());
            sb.append("&Project=");
            sb.append(projectNbr);
            sb.append("&FY=");
            sb.append(budgetFyInt.toString());
            sb.append("&AbacusProjectName=");
            sb.append((String) rw.getAttribute("ProjectName"));
            sb.append("&AwardeeName=");
            sb.append((String) rw.getAttribute("AwardeeName"));
        
           retVal=sb.toString();
          
        }
        return retVal;
    }
}
