/**
 * This class hold the Funds Navigator functionality.
 */
package gov.ofda.abacus.view.bean.funds;

import com.tangosol.dev.assembler.New;

import gov.ofda.abacus.view.base.AbacusConstants;
import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;

import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContainer;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.QueryEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.server.ViewObjectImpl;

import org.apache.myfaces.trinidad.event.ReturnEvent;

public class FundsNavigator implements Serializable{
    @SuppressWarnings("compatibility:1718539124516319700")
    private static final long serialVersionUID = 1L;

    private static final String TRANSFER = "Transfer";
    private static final String RECOVER = "Recover";
    private static final String TRANSFER_FROM_BUREAU= "Bureau";
    private static final String TRANSFER_FROM_OFFICE = "Office";
    private static final String TRANSFER_FROM_DIV = "Division";
    private static final String TRANSFER_FROM_PROJECT = "Project";


    public void processQueryListener(QueryEvent queryEvent) {
        // Add event code here...
        //#{bindings.FundsActivityViewCriteriaQuery.processQuery}
        
        Integer fiscalYear = null;
        String bureau = null;
        String office = null;
        String division = null;
        String divisionName = null;
        String projectNbr = null;
        String fundCode = null;
        String fundName = null;
        String projectName = null;
        
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        
        DCIteratorBinding iter = bindings.findIteratorBinding("FundsActivityView1Iterator");
        ViewObjectImpl voImpl = (ViewObjectImpl) iter.getViewObject();
        
        // get Bind Variables value
        if(voImpl.getNamedWhereClauseParam("p_fy")!= null){
            fiscalYear = Integer.valueOf(voImpl.getNamedWhereClauseParam("p_fy").toString());
        }
        
        if(voImpl.getNamedWhereClauseParam("p_bureauCode")!= null){
            bureau = String.valueOf(voImpl.getNamedWhereClauseParam("p_bureauCode"));
        }
        
        if(voImpl.getNamedWhereClauseParam("p_officeCode")!= null){
            office = String.valueOf(voImpl.getNamedWhereClauseParam("p_officeCode"));
        }
        
        
        if(voImpl.getNamedWhereClauseParam("p_div")!= null){
            division = String.valueOf(voImpl.getNamedWhereClauseParam("p_div"));
        }
        if(voImpl.getNamedWhereClauseParam("p_divisionName")!= null){
            divisionName = String.valueOf(voImpl.getNamedWhereClauseParam("p_divisionName"));
        }
        
        if(voImpl.getNamedWhereClauseParam("p_fc")!= null){
            fundCode = String.valueOf(voImpl.getNamedWhereClauseParam("p_fc"));
        }
        
        
        if(voImpl.getNamedWhereClauseParam("p_fn")!= null){
            fundName = String.valueOf(voImpl.getNamedWhereClauseParam("p_fn"));
        }
        
        if(voImpl.getNamedWhereClauseParam("p_projectName")!= null){
            projectName = String.valueOf(voImpl.getNamedWhereClauseParam("p_projectName"));
        }
        
        if(voImpl.getNamedWhereClauseParam("p_pnbr")!= null){
            projectNbr = String.valueOf(voImpl.getNamedWhereClauseParam("p_pnbr"));
        }
        
        
        
        OperationBinding exec = bindings.getOperationBinding("setFundsViewCriteria");
        exec.getParamsMap().put("fiscalYear", fiscalYear);
        exec.getParamsMap().put("bureauCode", bureau);
        exec.getParamsMap().put("officeCode", office);
        exec.getParamsMap().put("division", division);
        exec.getParamsMap().put("divisionName", divisionName);
        exec.getParamsMap().put("projectNbr", projectNbr);
        exec.getParamsMap().put("projectName", projectName);
        exec.getParamsMap().put("fundName", fundName);
        exec.getParamsMap().put("fundCode", fundCode);
        exec.execute();
        
        //setFundsViewCriteria
//        if(voImpl.getNamedWhereClauseParam("p_fy")!= null){
//            fiscalYear = Integer.parseInt(voImpl.getNamedWhereClauseParam("p_fy").toString());
//        }
        
        
        invokeEL("#{bindings.FundsActivityViewCriteriaQuery.processQuery}", new Class[] { QueryEvent.class }, new Object[] {
                 queryEvent });
        //OperationBinding exec = bindings.getOperationBinding("setOfdaDivisionViewsCriteria");
    }
    
    public static Object invokeEL(String el, Class[] paramTypes, Object[] params) {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    ELContext elContext = facesContext.getELContext();
    ExpressionFactory expressionFactory = facesContext.getApplication().getExpressionFactory();
    MethodExpression exp = expressionFactory.createMethodExpression(elContext, el, Object.class, paramTypes);
    return exp.invoke(elContext, params);
    }

    public String setParameters() {
        // Add event code here...
        String missingInfo = "";
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String transferType = (String) requestMap.get("transferType");
        String transferLevel = (String) requestMap.get("transferLevel");
        Integer fiscalYear = (Integer) requestMap.get("fiscalYear");
        String bureau = (String) requestMap.get("bureauCode");
        String office = (String) requestMap.get("officeCode");
        String division = (String) requestMap.get("division");
        String projectNbr = (String) requestMap.get("projectNbr");
        String fundCode = (String) requestMap.get("fundCode");
        String isNew = (String) requestMap.get("isNew");
        
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        
        
        // validate transfer or recover type
        if (!((TRANSFER.equals(transferType) || RECOVER.equals(transferType)))) {
            pageFlowMap.put("message",
                    "Unknown Transfer Type: Valid values are 'Transfer' & 'Recover'");
            return "error";
        }
        
        if(null != isNew && isNew.equalsIgnoreCase("true")){
            
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = bindings.getOperationBinding("getp_fy");
            exec.execute();
            requestMap.put("fiscalYear", exec.getResult());
            
            if(TRANSFER_FROM_BUREAU.equalsIgnoreCase(transferLevel)|| TRANSFER_FROM_DIV.equalsIgnoreCase(transferLevel)||TRANSFER_FROM_OFFICE.equalsIgnoreCase(transferLevel)){
                exec = bindings.getOperationBinding("getp_bureauCode");
                exec.execute();
                requestMap.put("bureauCode", exec.getResult());
            }else{
                requestMap.put("bureauCode", null);
            }
            
            if(TRANSFER_FROM_DIV.equalsIgnoreCase(transferLevel)||TRANSFER_FROM_OFFICE.equalsIgnoreCase(transferLevel)){
                exec = bindings.getOperationBinding("getp_officeCode");
                exec.execute();
                requestMap.put("officeCode", exec.getResult());
            }else{
                requestMap.put("officeCode", null);
            }
            
            if(TRANSFER_FROM_DIV.equalsIgnoreCase(transferLevel)){
                exec = bindings.getOperationBinding("getp_div");
                exec.execute();
                requestMap.put("division", exec.getResult());
            }else{
                requestMap.put("division", null);
            }
            
            exec = bindings.getOperationBinding("getp_fc");
            exec.execute();
            requestMap.put("fundCode", exec.getResult());
            return "next";
        }
        
        // validate roles
        SecurityContext context=ADFContext.getCurrent().getSecurityContext();
        if(!(context.isUserInRole("APP_FM")||context.isUserInRole("APP_BFL"))){
            pageFlowMap.put("message",
                    "User does not have necessary role to Add or Recover Funds");
            return "error";
        }
        
        // remaining validations
        if (fiscalYear == null)
            missingInfo = "Fiscal Year,";
        switch(transferLevel) {
        case TRANSFER_FROM_BUREAU:
            missingInfo+=bureau != null?"":"Bureau,";
            break;
        case TRANSFER_FROM_OFFICE:
            missingInfo+=bureau != null?"":"Bureau,";
            missingInfo+=office != null?"":"Office,";
            break;
        case TRANSFER_FROM_DIV:
            missingInfo+=bureau != null?"":"Bureau,";
            missingInfo+=office != null?"":"Office,";
            missingInfo+=division != null?"":"Division,";
            break;
            
        }
        if(fiscalYear!=null && (fiscalYear<2001 || fiscalYear>9999)) {
            pageFlowMap.put("message",
                    "Fiscal Year must be between 2001 and 9999");
            return "error";
        }
        if (missingInfo.length() == 0){
            return "next";
        }
        pageFlowMap.put("message",
                 "Select or enter missing required value(s) in Search: " + missingInfo);

        return "error";
        // Add event code here...
    }

    public void addFundsReturnListener(ReturnEvent returnEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfp=AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) pfp.get("confirmMessage");
        MessageBean messageBean = new MessageBean();
        messageBean.sendActivityFundTransferEmail(result, (Map) pfp.get("transferDetails"),(Boolean) pfp.get("isSendEmail"));;
    }

    public void recoverFundsListener(ReturnEvent returnEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) map.get(AbacusConstants.CONFIRM_MESSAGE);
        MessageBean bean = new MessageBean();
        bean.sendFundRecoveryEmail(result, (Map)map.get(AbacusConstants.TRANSFER_DETAILS), (Boolean)map.get(AbacusConstants.SEND_EMAIL));

    }
}
