package gov.ofda.abacus.view.ui.validate;

import java.sql.Timestamp;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * This class is used for date validation for abacus application.
 */
public class DateValidator implements Validator {
    public DateValidator() {
        super();
    }

    /**
     * validates the given date and throws a validation exception.
     * @param facesContext
     * @param uIComponent
     * @param object
     * @throws ValidatorException
     */
    @Override
    public void validate(FacesContext facesContext, UIComponent uIComponent, Object object) throws ValidatorException {
        
        
        if(null != object){
            Timestamp timeStamp = (Timestamp) object;
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");

            try {
                format.parse(object.toString());
            } catch (ParseException e) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "The date is not in the correct format", "Enter a date in the same format as this example: 11/29/1998"));
            }
            
            String timePart[] = object.toString().split(" ");
            String datePart[] = timePart[0].split("-");
            if(datePart[0].length() > 4 ){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "The date is not in the correct format", "Enter a date in the same format as this example: 11/29/1998"));
            }else if(datePart[1].length() > 2 || Integer.parseInt(datePart[1]) > 12){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Please enter valid Month in MM/dd/yyyy format"));
            }else if(datePart[2].length() > 2 || Integer.parseInt(datePart[2]) > 31){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Please enter valid Day in MM/dd/yyyy format"));
            }
            int day = Integer.parseInt(datePart[2]);
            int month = Integer.parseInt(datePart[1]);
            int year = Integer.parseInt(datePart[0]);
            if(month == 2){
                if(year%4 == 0){
                    if(month > 29 ){
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Please enter valid Month in MM/dd/yyyy format"));
                    }
                }else{
                    if(month > 28 ){
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Please enter valid Month in MM/dd/yyyy format"));
                    }
                }
            }
        }
       
    }
}
