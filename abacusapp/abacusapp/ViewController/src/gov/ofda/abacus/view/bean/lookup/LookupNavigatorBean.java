package gov.ofda.abacus.view.bean.lookup;

import gov.ofda.abacus.view.base.JSFUtils;

import java.io.Serializable;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.component.rich.fragment.RichRegion;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.component.UIXSwitcher;
import org.apache.myfaces.trinidad.util.ComponentReference;

public class LookupNavigatorBean{
    public LookupNavigatorBean() {
    }

    public void lookupLinkLnsr(ActionEvent actionEvent) {
                Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
                Map rsm = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
                pfm.put("viewName", rsm.get("viewName"));
                pfm.put("displayName", rsm.get("displayName"));
                pfm.put("roleName", rsm.get("roleName"));
                DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding execDelete = bindings.getOperationBinding("publishLookupNavigatorEvent");
                execDelete.execute();
    }
}
