package gov.ofda.abacus.view.bean.lookup;

import java.io.Serializable;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.PopupCanceledEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;

public class ProjectLookup implements Serializable {
    @SuppressWarnings("compatibility:-1942053659718286003")
    private static final long serialVersionUID = 1L;
    private RichPopup addOrEditProjectPopup;
    private RichPopup projectNameChangePopup;


    public void addNewProjectDialogListener(DialogEvent dialogEvent) {
       DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
       DCIteratorBinding projectLookupIter = bindings.findIteratorBinding("EditProjectLookupViewIterator");
        
        if(DialogEvent.Outcome.ok == dialogEvent.getOutcome()){
            OperationBinding exec = bindings.getOperationBinding("Commit");
            if(exec.isOperationEnabled()){
                exec.execute(); 
                if(exec.getErrors().isEmpty()){
                    if(AdfFacesContext.getCurrentInstance().getPageFlowScope().get("addNew").toString().equalsIgnoreCase("true")){
                        Row currentRow = projectLookupIter.getCurrentRow();
                        Object projectNbr = currentRow.getAttribute("ProjectNbr");
                        String projectName = (String) currentRow.getAttribute("ProjectName");
                        FacesMessage message =
                            new FacesMessage(FacesMessage.SEVERITY_INFO, projectNbr + ", " + projectName +" has been added successfully." , "");
                        FacesContext.getCurrentInstance().addMessage(null, message); 
                    }else{
                        FacesMessage message =
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully", "");
                        FacesContext.getCurrentInstance().addMessage(null, message);
                    }
                }
                
                Key currentRowKey = projectLookupIter.getCurrentRow().getKey();
                
                if(null != projectLookupIter.findRowByKeyString(currentRowKey.toStringFormat(true))){
                    projectLookupIter.setCurrentRowWithKey(currentRowKey.toStringFormat(true));
                }
            }
        }else{
            OperationBinding exec1 = bindings.getOperationBinding("Rollback");
            exec1.execute();
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(dialogEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
    }

    public void addOrEditProjectPopupCancelListener(PopupCanceledEvent popupCanceledEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Commit");
        if(exec.isOperationEnabled()){
            OperationBinding execRollback = bindings.getOperationBinding("Rollback");
            execRollback.execute();
            
            OperationBinding execExecute = bindings.getOperationBinding("Execute");
            execExecute.execute();

            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancelled successfully", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(popupCanceledEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
    }

    public void addNewProjectActionListener(ActionEvent actionEvent) {
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("addNew", "true");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding createInsertExec = bindings.getOperationBinding("CreateInsert");
        createInsertExec.execute();
        RichPopup popup = (RichPopup) this.getAddOrEditProjectPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
    }

    public void setAddOrEditProjectPopup(RichPopup addOrEditProjectPopup) {
        this.addOrEditProjectPopup = addOrEditProjectPopup;
    }

    public RichPopup getAddOrEditProjectPopup() {
        return addOrEditProjectPopup;
    }

    public void editProjectActionListener(ActionEvent actionEvent) {
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("addNew", "false");
        RichPopup popup = (RichPopup) this.getAddOrEditProjectPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
    }

    public void deleteProjectDialogListener(DialogEvent dialogEvent) {
        if( DialogEvent.Outcome.yes == dialogEvent.getOutcome()){
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            
            DCIteratorBinding iter = bindings.findIteratorBinding("EditProjectLookupViewIterator");
            Row currentRow = iter.getCurrentRow();
            
            Object projectNbr = currentRow.getAttribute("ProjectNbr");
            String projectName = (String) currentRow.getAttribute("ProjectName");
            
            OperationBinding execDelete = bindings.getOperationBinding("Delete");
            execDelete.execute();
            
            OperationBinding execCommit = bindings.getOperationBinding("Commit");
            execCommit.execute();
            if(!execCommit.getErrors().isEmpty()){
                OperationBinding execRollback = bindings.getOperationBinding("Rollback");
                execRollback.execute();
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Record cannot be deleted as child record exists!", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
                ((RichPopup)dialogEvent.getComponent().getParent()).cancel();
            }else{
                OperationBinding exec = bindings.getOperationBinding("Execute");
                exec.execute();
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, projectNbr + ", " + projectName +" has been deleted successfully." , "");
                FacesContext.getCurrentInstance().addMessage(null, message); 
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(dialogEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
    }

    public void projectNameValueChangeListener(ValueChangeEvent valueChangeEvent) {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String oldValue = null;
        String newValue = null;
        if(null != valueChangeEvent.getOldValue()){
            oldValue = (String) valueChangeEvent.getOldValue();
        }
        if(null != valueChangeEvent.getNewValue()){
            newValue = (String) valueChangeEvent.getNewValue();
        }
        
        if(null != oldValue && null != newValue){
            pageFlowMap.put("oldProjectValue", oldValue);
            pageFlowMap.put("newProjectValue", newValue);
            RichPopup popup = (RichPopup) this.getProjectNameChangePopup();
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            popup.show(ph);
        }
    }

    public void projectNameChangeDialogListener(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().equals(DialogEvent.Outcome.no)){
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("EditProjectLookupViewIterator");
                Row currentRow = iter.getCurrentRow();
                currentRow.setAttribute("ProjectName", AdfFacesContext.getCurrentInstance().getPageFlowScope().get("oldProjectValue"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(dialogEvent.getComponent().getParent().getParent());
        }
    }

    public void setProjectNameChangePopup(RichPopup projectNameChangePopup) {
        this.projectNameChangePopup = projectNameChangePopup;
    }

    public RichPopup getProjectNameChangePopup() {
        return projectNameChangePopup;
    }
}
