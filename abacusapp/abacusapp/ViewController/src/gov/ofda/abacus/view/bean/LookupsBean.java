package gov.ofda.abacus.view.bean;

//import gov.ofda.abacus.base.JSFUtils;


import gov.ofda.abacus.view.base.JSFUtils;

import java.io.Serializable;

import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.RichQuery;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.QueryEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.NavigatableRowIterator;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;

import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.util.ComponentReference;

public class LookupsBean implements Serializable {
    @SuppressWarnings("compatibility:-3381931774093993059")
    private static final long serialVersionUID = 1L;
    private static ADFLogger logger = ADFLogger.createADFLogger(LookupsBean.class);
    final String OLD_CURR_KEY_VIEWSCOPE_ATTR = "__oldCurrentRowKey__";
    private ComponentReference editPopup;
    private ComponentReference tableLookup;
    private ComponentReference deleteAwardeeAdressPopup;
    private ComponentReference inactivateAwardeeContactPopup;
    private ComponentReference commonLookupQuery;

    public void setTableLookup(UIComponent tableLookup) {
        this.tableLookup = ComponentReference.newUIComponentReference(tableLookup);
    }

    public UIComponent getTableLookup() {
        return tableLookup == null ? null : tableLookup.getComponent();
    }

    public void cancelAction(ActionEvent actionEvent) {
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = (DCIteratorBinding) bindingContainer.findIteratorBinding("EditLookupViewIterator");
        Key key = iter.getCurrentRow().getKey();
        logger.log("Entered with Key: " + key.getKeyValues());

        OperationBinding operationBinding = bindingContainer.getOperationBinding("Rollback");
        operationBinding.execute();
        RowSetIterator rsi = iter.getRowSetIterator();

        if (key != null && rsi.findByKey(key, 1).length > 0) {
            iter.setCurrentRowWithKey(key.toStringFormat(true));
            ADFContext adfCtx = ADFContext.getCurrent();
        }

    }

    public void setEditPopup(UIComponent editPopup) {
        this.editPopup = ComponentReference.newUIComponentReference(editPopup);
    }

    public UIComponent getEditPopup() {
        return editPopup == null ? null : editPopup.getComponent();
    }


    public void closePopupAction(ActionEvent actionEvent) {
        RichPopup popup = (RichPopup) this.getEditPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.hide();
    }

    public void addAwardeeAddressSave() {

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Commit");
        exec.execute();

        DCIteratorBinding iter = (DCIteratorBinding) bindings.findIteratorBinding("AwardeeAddressView2Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row row = rsi.getCurrentRow();
        Object dbSeq = row.getAttribute("AwardeeAddSeq");
        if (dbSeq != null) {
            String addressSeq = String.valueOf(dbSeq);
            Map<String, Object> pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            pageFlowMap.put("newAwardeeAddressSeq", addressSeq);
        }
    }

    public void addAwardeeContactSave() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Commit");
        exec.execute();

        DCIteratorBinding iter = (DCIteratorBinding) bindings.findIteratorBinding("AwardeeContactsView2Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row row = rsi.getCurrentRow();
        Object dbSeq = row.getAttribute("AwardeeContactSeq");
        if (dbSeq != null) {
            String contactSeq = String.valueOf(dbSeq);
            Map<String, Object> pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            pageFlowMap.put("newAwardeeContactSeq", contactSeq);
        }

    }

    public void editAwardeeSave() {
        // Add event code here...
    }

    public void editAwardeeCancel() {
        // Add event code here...
    }

    /**
     * this method insert a new Awardee Address for an Awardee by calling Create Insert
     * on the AwardeeAddressView.
     * which uses the edit-awardee-tf task flow and editAwardee.jsf
     * @param actionEvent
     */
    public void addAwardeeAddress(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("CreateInsertAwardeeAddress");
        exec.execute();
    }

    /**
     * this method deletes Awardee Address for an Awardee by calling Delete
     * on the AwardeeAddressView.
     * which uses the edit-awardee-tf task flow and editAwardee.jsf
     * @param actionEvent
     */
    public void deleteAwardeeAddress(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("DeleteAwardeeAddress");
        exec.execute();
        exec = bindings.getOperationBinding("Commit");
        exec.execute();
    }

    /**
     * this method insert a new Awardee Contact for an Awardee by calling Create Insert
     * on the AwardeeContactView.
     * which uses the edit-awardee-tf task flow and editAwardee.jsf
     * @param actionEvent
     */
    public void addAwardeeContact(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("CreateInsertAwardeeContact");
        exec.execute();
    }

    /**
     * this method deletes Awardee Contact for an Awardee by calling Delete
     * on the AwardeeContactView.
     * which uses the edit-awardee-tf task flow and editAwardee.jsf
     * @param actionEvent
     */
    public void deleteAwardeeContact(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("DeleteAwardeeContact");
        exec.execute();
        exec = bindings.getOperationBinding("Commit");
        exec.execute();
    }

    public void saveAwardeeManager() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Commit");
        
        if(exec.isOperationEnabled()){
            exec.execute();
            if(exec.getErrors().isEmpty()){
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }

            Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            if (map.get("awardeeCode") == null) {
                DCIteratorBinding iterator = bindings.findIteratorBinding("AwardeeLookupView1Iterator");
                RowSetIterator itr = iterator.getRowSetIterator();
                Row currentRow = itr.getCurrentRow();
                map.put("awardeeCode", currentRow.getAttribute("AwardeeCode"));
            }
        }else{
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "No changes to save", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void cancelAwardeeManager(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Rollback");
        exec.execute();
    }

    public void testEntry(ActionEvent actionEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("CreateInsert");
        exec.execute();

        RichPopup popup = (RichPopup) this.getEditPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);


    }


    public void addNew(ActionEvent actionEvent) {
        OperationBinding oper1 = null;
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dciter = (DCIteratorBinding) bindings.get("EditLookupViewIterator");
        Row oldCcurrentRow = dciter.getCurrentRow();
        //ADFContext is a convenient way to access all kinds of memory
        //scopes. If you like to be persistent in your ADF coding then this
        //is what you want to use
        ADFContext adfCtx = ADFContext.getCurrent();
        if (oldCcurrentRow != null)
            adfCtx.getViewScope().put(OLD_CURR_KEY_VIEWSCOPE_ATTR, oldCcurrentRow.getKey().toStringFormat(true));
        NavigatableRowIterator nav = dciter.getNavigatableRowIterator();
        Row newRow = nav.createRow();
        newRow.setNewRowState(Row.STATUS_INITIALIZED);
        Row currentRow = nav.getCurrentRow();
        int currentRowIndex = nav.getRangeIndexOf(currentRow);
        nav.insertRowAtRangeIndex(currentRowIndex + 1, newRow);
        dciter.setCurrentRowWithKey(newRow.getKey().toStringFormat(true));

    }

    public void addNewEntry(ActionEvent actionEvent) {
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("addNew", "true");
        RichPopup popup = (RichPopup) this.getEditPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("CreateInsert");
        exec.execute();

    }

    public void AddNewDialogListner(DialogEvent dialogEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        String addNew = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("addNew");
        OperationBinding exec = bindings.getOperationBinding("Commit");
        if(exec.isOperationEnabled()){
            if (DialogEvent.Outcome.ok.equals(dialogEvent.getOutcome())) {
                exec.execute();
                if(exec.getErrors().isEmpty()){
                    if(null != addNew && addNew.equalsIgnoreCase("true")){
                        FacesMessage message =
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Added successfully!", "");
                        FacesContext.getCurrentInstance().addMessage(null, message);
                    }else{
                        FacesMessage message =
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully!", "");
                        FacesContext.getCurrentInstance().addMessage(null, message);
                    }
                }
                else {
                    FacesMessage message =
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ""+exec.getErrors().toString(), "");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                }
            } 
            DCBindingContainer bindingContainer =
                (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindingContainer.findIteratorBinding("EditLookupViewIterator");
            Key key = iter.getCurrentRow().getKey();

            OperationBinding operationBinding = bindingContainer.getOperationBinding("Execute");
            operationBinding.execute();
            RowSetIterator rsi = iter.getRowSetIterator();

            if (key!=null && !key.isNull()  && rsi.findByKey(key, 1).length > 0) {
                iter.setCurrentRowWithKey(key.toStringFormat(true));
            }
        }
    }

    public void popupCancelListner(PopupCanceledEvent popupCanceledEvent) {
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindingContainer.findIteratorBinding("EditLookupViewIterator");
        Key key = iter.getCurrentRow().getKey();

        OperationBinding operationBinding = bindingContainer.getOperationBinding("Rollback");
        operationBinding.execute();
        RowSetIterator rsi = iter.getRowSetIterator();

        if (key!=null && !key.isNull()  && rsi.findByKey(key, 1).length > 0) {
            iter.setCurrentRowWithKey(key.toStringFormat(true));
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(popupCanceledEvent.getComponent().getParent());
    }

    public void deleteDialogListner(DialogEvent dialogEvent) {
        String currentKeyValStr = null;
        StringBuilder sb = new StringBuilder();
        Key rowKey = null;
        if (DialogEvent.Outcome.yes.equals(dialogEvent.getOutcome())) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = (DCIteratorBinding) bindings.findIteratorBinding("EditLookupViewIterator");
            rowKey = iter.getCurrentRow().getKey();
            if (rowKey != null) {
                for (Object keyVal : rowKey.getAttributeValues()) {
                    sb.append(keyVal);

                }
                currentKeyValStr = sb.toString();
            }
            OperationBinding exec1 = bindings.getOperationBinding("Delete");
            exec1.execute();
            exec1 = bindings.getOperationBinding("Commit");
            exec1.execute();
            if (!exec1.getErrors().isEmpty()) {

                exec1 = bindings.getOperationBinding("Rollback");
                exec1.execute();

                iter.getViewObject().executeQuery();
                iter.setCurrentRowWithKey(rowKey.toStringFormat(true));
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                                     "<html><body> Record: <b>" + currentKeyValStr +
                                     "</b> cannot be deleted as child record exists </body></html>", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }else{
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                                     "<html><body> Deleted successfully </body></html>", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }
        //AdfFacesContext.getCurrentInstance().addPartialTarget(tableLookup);
    }

    public void deleteAction(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec1 = bindings.getOperationBinding("Delete");
        exec1.execute();
        exec1 = bindings.getOperationBinding("Commit");
        exec1.execute();
    }

    public void onTableSelect(SelectionEvent selectionEvent) {

        RichTable table = (RichTable) selectionEvent.getSource();
        CollectionModel tableModel = (CollectionModel) table.getValue();
        JUCtrlHierBinding adfTableBinding = (JUCtrlHierBinding) tableModel.getWrappedData();
        DCIteratorBinding tableIteratorBinding = adfTableBinding.getDCIteratorBinding();
        String key = tableIteratorBinding.getCurrentRowKeyString();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("selectedRowKey", key);
    }


    /*  public void setSearchTable(RichTable searchTable) {
        this.searchTable = searchTable;
    }
    public RichTable getSearchTable()
    {
        DCBindingContainer dcBindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry() ;
        DCIteratorBinding iter= dcBindings.findIteratorBinding("LookupListLOV1Iterator");
        if (null != AdfFacesContext.getCurrentInstance().getPageFlowScope().get("selectedRowKey"))
        {
          String key  = (String)AdfFacesContext.getCurrentInstance().getPageFlowScope().get("selectedRowKey");
          iter.setCurrentRowWithKey(key.trim());
          AdfFacesContext.getCurrentInstance().getPageFlowScope().put("selectedRowKey", null);
        }
        return searchTable;
    }*/

    /*public Boolean setPermissions() {
      DCBindingContainer dcBindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry() ;
      DCIteratorBinding iter= dcBindings.findIteratorBinding("EditLookupViewIterator");
      RowSetIterator rsi = iter.getRowSetIterator();
      Row currentRow = rsi.getCurrentRow();
     // Boolean b = currentRow.
      //return b;
  }*/

    public void showSaveSuccessMessage() {
        FacesMessage message =
            new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully", "");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void showChangesCancelledMessage() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Commit");
        if(exec.isOperationEnabled()){
            exec = bindings.getOperationBinding("Rollback");
            exec.execute();
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancelled successfully", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void closeAwardeePopupActionListner(ActionEvent actionEvent) {
        //AwardeeLookupView1Iterator
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = (DCIteratorBinding) bindings.get("AwardeeLookupView1Iterator");
        Row row = iter.getCurrentRow();
        String awardeeName = (String) row.getAttribute("AwardeeName");
        String awardeeCode = (String) row.getAttribute("AwardeeCode");
        String awardeeAcronym = (String) row.getAttribute("AwardeeAcronym");
        String taxIdNbr = (String) row.getAttribute("TaxIdNbr");
        String dunsNbr = (String) row.getAttribute("DunsNbr");
        String awardeeTypeCode = (String) row.getAttribute("AwardeeTypeCode");
        String webSite = (String) row.getAttribute("WebSite");
        String letterOfCredit = (String) row.getAttribute("LetterOfCredit");
        String awardeeComments = (String) row.getAttribute("AwardeeComments");


        boolean isEmptyAwardee = false;
        if (awardeeName == null && null == awardeeAcronym && null == taxIdNbr && null == dunsNbr &&
            null == awardeeTypeCode && null == webSite && null == letterOfCredit && null == awardeeComments) {
            //               FacesMessage message =
            //                   new FacesMessage(FacesMessage.SEVERITY_INFO, "Your changes have been succesfully cancelled!", "");
            //               FacesContext.getCurrentInstance().addMessage(null, message);
            isEmptyAwardee = true;
        }
        SecurityContext scntx = ADFContext.getCurrent().getSecurityContext();
        Boolean isCommitEnabled = (Boolean) JSFUtils.resolveExpression("#{bindings.Commit.enabled}");

        if (isEmptyAwardee && isCommitEnabled) {
            NavigationHandler nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            if (nvHndlr != null)
                nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "close");
            return;
        } else if (!isCommitEnabled) {
            NavigationHandler nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            if (nvHndlr != null)
                nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "close");
            return;
            
            
        } else if (isCommitEnabled) {
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_WARN,
                                 "Please save or cancel your changes to Close Add/Edit Awardee!", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void awardeeManagerSearchQueryListner(QueryEvent queryEvent) {
        // Add event code here...#{bindings.searchAwardeeQuery.processQuery}
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        //OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        SecurityContext scntx = ADFContext.getCurrent().getSecurityContext();
        Boolean isCommitEnabled = (Boolean) JSFUtils.resolveExpression("#{bindings.Commit.enabled}");
        if (isCommitEnabled) {
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_WARN, "Please save or cancel your changes", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        } else {
            FacesContext fctx = FacesContext.getCurrentInstance();
            Application application = fctx.getApplication();
            ExpressionFactory expressionFactory = application.getExpressionFactory();
            ELContext elctx = fctx.getELContext();

            MethodExpression methodExpression =
                expressionFactory.createMethodExpression(elctx, "#{bindings.searchAwardeeQuery.processQuery}",
                                                         Object.class, new Class[] { QueryEvent.class });
            methodExpression.invoke(elctx, new Object[] { queryEvent });
        }
    }

    public void deleteAwardeeAddressDialogListner(DialogEvent dialogEvent) {
        if (DialogEvent.Outcome.yes.equals(dialogEvent.getOutcome())) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            // get the current row
            DCIteratorBinding iter = (DCIteratorBinding) bindings.get("AwardeeAddressView2Iterator");
            Row row = iter.getCurrentRow();
            Object addressSeq = row.getAttribute("AwardeeAddSeq");

            OperationBinding exec = bindings.getOperationBinding("checkAwardeeContactOrAddressFReference");
            exec.getParamsMap().put("addressSeq", addressSeq);
            exec.getParamsMap().put("type", "address");
            exec.execute();
            String val = (String) exec.getResult();
            if (val.equalsIgnoreCase("true")) {
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                ((RichPopup) this.getDeleteAwardeeAdressPopup()).show(hints);
            } else if (val.equalsIgnoreCase("false")) {
                exec = bindings.getOperationBinding("DeleteAwardeeAddress");
                exec.execute();
            } else if (val.equalsIgnoreCase("error")) {
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                                     "Opps something went wrong, Please contact Abacus Team at Abacus@ofda.gov", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(dialogEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
    }

    public void deleteAwardeeContactDialogListner(DialogEvent dialogEvent) {
        if (DialogEvent.Outcome.yes.equals(dialogEvent.getOutcome())) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            // get the current row
            DCIteratorBinding iter = (DCIteratorBinding) bindings.get("AwardeeContactsView2Iterator");
            Row row = iter.getCurrentRow();
            Object contactSeq = row.getAttribute("AwardeeContactSeq");

            OperationBinding exec = bindings.getOperationBinding("checkAwardeeContactOrAddressFReference");
            exec.getParamsMap().put("contactSeq", contactSeq);
            exec.getParamsMap().put("type", "contact");
            exec.execute();
            String val = (String) exec.getResult();
            if (val.equalsIgnoreCase("true")) {
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                ((RichPopup) this.getInactivateAwardeeContactPopup()).show(hints);
            } else if (val.equalsIgnoreCase("false")) {
                exec = bindings.getOperationBinding("DeleteAwardeeContact");
                exec.execute();
            } else if (val.equalsIgnoreCase("error")) {
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                                     "Opps something went wrong, Please contact Abacus Team at Abacus@ofda.gov", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(dialogEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
    }

    public void setDeleteAwardeeAdressPopup(UIComponent deleteAwardeeAdressPopup) {
        this.deleteAwardeeAdressPopup = ComponentReference.newUIComponentReference(deleteAwardeeAdressPopup);
    }

    public UIComponent getDeleteAwardeeAdressPopup() {
        return deleteAwardeeAdressPopup == null ? null : deleteAwardeeAdressPopup.getComponent();
    }

    public void inactiveAwardeeAddressDailogListner(DialogEvent dialogEvent) {
        if (DialogEvent.Outcome.yes.equals(dialogEvent.getOutcome())) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            // get the current row
            DCIteratorBinding iter = (DCIteratorBinding) bindings.get("AwardeeAddressView2Iterator");
            Row row = iter.getCurrentRow();
            row.setAttribute("Active", "N");
            OperationBinding exec = bindings.getOperationBinding("Commit");
            exec.execute();
        }
    }

    public void inactivateAwardeeContactDialogListner(DialogEvent dialogEvent) {
        if (DialogEvent.Outcome.yes.equals(dialogEvent.getOutcome())) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            // get the current row
            DCIteratorBinding iter = (DCIteratorBinding) bindings.get("AwardeeContactsView2Iterator");
            Row row = iter.getCurrentRow();
            row.setAttribute("Active", "N");
            OperationBinding exec = bindings.getOperationBinding("Commit");
            exec.execute();
        }
    }

    public void setInactivateAwardeeContactPopup(UIComponent inactivateAwardeeContactPopup) {
        this.inactivateAwardeeContactPopup = ComponentReference.newUIComponentReference(inactivateAwardeeContactPopup);
    }

    public UIComponent getInactivateAwardeeContactPopup() {
        return inactivateAwardeeContactPopup == null ? null : inactivateAwardeeContactPopup.getComponent();
    }

    public void checkViewExists() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        OperationBinding exec = bindings.getOperationBinding("IsViewExists");
        exec.execute();
        if (exec.getErrors().isEmpty() && (Boolean) exec.getResult()) {
            pfm.put("viewExists", true);
        } else {
            pfm.put("viewExists", false);
            pfm.put("viewName", "EditDivisionLookupView");
        }
    }

    public void setCommonLookupQuery(UIComponent commonLookupQuery) {
        this.commonLookupQuery = ComponentReference.newUIComponentReference(commonLookupQuery);
    }

    public UIComponent getCommonLookupQuery() {
        return commonLookupQuery == null ? null : commonLookupQuery.getComponent();
    }

    public void editEntry(ActionEvent actionEvent) {
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("addNew", "false");
        RichPopup popup = (RichPopup) this.getEditPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
    }

    public void deleteAwardeeLookupDialogListner(DialogEvent dialogEvent) {
        if( DialogEvent.Outcome.yes == dialogEvent.getOutcome()){
            StringBuffer  sb = new StringBuffer();
            String awardee = "";
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = (DCIteratorBinding) bindings.findIteratorBinding("AwardeeLookupView1Iterator");
            RowSetIterator iterator = iter.getRowSetIterator();
            Key rowKey = iterator.getCurrentRow().getKey();
            Row currentRow = iterator.getCurrentRow();
            if(rowKey != null){
                sb.append(currentRow.getAttribute("AwardeeCode"));
                sb.append(" , ");
                sb.append(currentRow.getAttribute("AwardeeName"));
                sb.append(" ");
                awardee = sb.toString();
            }

            OperationBinding execDelete = bindings.getOperationBinding("Delete");
            execDelete.execute();
            OperationBinding exec = bindings.getOperationBinding("Commit");
            exec.execute();
            if (!exec.getErrors().isEmpty()) {

                exec = bindings.getOperationBinding("Rollback");
                exec.execute();

                if(null != rowKey){
                    iter.getViewObject().executeQuery();
                    iter.setCurrentRowWithKey(rowKey.toStringFormat(true));
                }
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                     "<html><body> Record: <b>" + awardee +
                                     "</b> cannot be deleted as child record exists </body></html>", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
                ((RichPopup)dialogEvent.getComponent().getParent()).cancel();
            }else{
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                                     "<html><body> Deleted successfully </body></html>", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(dialogEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
    }

    public void deleteRowLookupActionListener(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = (DCIteratorBinding) bindings.findIteratorBinding("EditLookupViewIterator");
        RowSetIterator iterator = iter.getRowSetIterator();
        Key rowKey = iterator.getCurrentRow().getKey();
        
    }
}
