package gov.ofda.abacus.view.bean.funds;

import gov.ofda.abacus.view.base.AbacusEmail;
import gov.ofda.abacus.view.bean.AppBean;
import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;

import java.math.BigDecimal;

import java.text.NumberFormat;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.LaunchPopupEvent;

import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class AddTransferRecoverFunds implements Serializable {
    @SuppressWarnings("compatibility:4427973853204493970")
    private static final long serialVersionUID = 1L;

    private static final String TRANSFER = "Transfer";
    private static final String RECOVER = "Recover";
    private static final String TRANSFER_FROM_BUREAU = "Bureau";
    private static final String TRANSFER_FROM_OFFICE = "Office";
    private static final String TRANSFER_FROM_DIV = "Division";

    /**
     * This method validates the input fields such as
     *      fiscal year, division, etc and User roles whether user is eligible for transfer or recover.
     * @return
     */
    public String validateInput() {

        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String isNew = (String) pageFlowMap.get("isNew");
        String bureau = (String) pageFlowMap.get("bureauCode");
        String office = (String) pageFlowMap.get("officeCode");
        String division = (String) pageFlowMap.get("division");
        String fundCode = (String) pageFlowMap.get("fundCode");

        String transferLevel = (String) pageFlowMap.get("transferLevel");

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        if("true".equals(isNew))
        {
            //Validate Bureau
            if (bureau!=null && (TRANSFER_FROM_BUREAU.equals(transferLevel)|| TRANSFER_FROM_DIV.equals(transferLevel) ||TRANSFER_FROM_OFFICE.equals(transferLevel))) {
            
                DCIteratorBinding iter = bindings.findIteratorBinding("BureauLOV1Iterator");
                Key key = new Key(new Object[] { bureau });
                Row r[] = iter.getRowSetIterator().findByKey(key, 1);
                if (r.length < 1) {
                    pageFlowMap.put("bureauCode", null);
                    StringBuilder message = new StringBuilder("<html><body>");
                    message.append("Bureau <b> \"" + bureau +
                                   "  \" </b> is invalid/inactive. Please select a valid Bureau");
                    message.append("</body></html>");
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Invalid Bureau", message.toString());
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
            }
            //Validate Office
            if (office!=null && (TRANSFER_FROM_DIV.equals(transferLevel) ||TRANSFER_FROM_OFFICE.equals(transferLevel))) {
            
                DCIteratorBinding iter = bindings.findIteratorBinding("OfficeLOV1Iterator");
                Key key = new Key(new Object[] { office });
                Row r[] = iter.getRowSetIterator().findByKey(key, 1);
                if (r.length < 1) {
                    pageFlowMap.put("officeCode", null);
                    StringBuilder message = new StringBuilder("<html><body>");
                    message.append("Office <b> \"" + office +
                                   "  \" </b> is invalid/inactive. Please select a valid Office");
                    message.append("</body></html>");
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Invalid Office", message.toString());
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
            }
            //Validate division
            if (TRANSFER_FROM_DIV.equals(transferLevel) && division != null) {
    
                DCIteratorBinding iter = bindings.findIteratorBinding("DivisionLOV1Iterator");
                Key key = new Key(new Object[] { division });
                Row r[] = iter.getRowSetIterator().findByKey(key, 1);
                if (r.length < 1) {
                    pageFlowMap.put("division", null);
                    StringBuilder message = new StringBuilder("<html><body>");
                    message.append("Division <b> \"" + division +
                                   "  \" </b> is invalid/inactive. Please select a valid Division");
                    message.append("</body></html>");
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Invalid Division", message.toString());
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
            }
    
            // validate Fund code
            if (null != fundCode) {
                DCIteratorBinding iter = bindings.findIteratorBinding("FundLookupViewCheckIterator");
                if (null != pageFlowMap.get("fiscalYear")) {
                    OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
                    exec.getParamsMap().put("p_fy", pageFlowMap.get("fiscalYear"));
                    exec.execute();
                    Key fundkey = new Key(new Object[] { pageFlowMap.get("fiscalYear"), fundCode });
                    Row r[] = iter.getRowSetIterator().findByKey(fundkey, 1);
                    if (r.length < 1) {
                        pageFlowMap.put("fundCode", null);
                        StringBuilder message = new StringBuilder("<html><body>");
                        message.append("Fund Code <b> \"" + fundCode +
                                       "\" </b> is invalid. Please select a valid Fund Code");
                        message.append("</body></html>");
                        FacesMessage msg =
                            new FacesMessage(FacesMessage.SEVERITY_WARN, "Invalid Fund Code", message.toString());
                        FacesContext.getCurrentInstance().addMessage(null, msg);
                    }
                }
            }
        }

        if (null != isNew && isNew.equalsIgnoreCase("true")) {
            return "new";
        } else {
            return "next";
        }

        //DivisionLOV1Iterator
    }

    public String initializeTransfer() {
        // Add event code here...
        //Check if row exists
        //depending on transfer details setup min/max
        //If no available amt show message and close
        //Setup Fund Code if needed fund code validation
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec;
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String transferType = (String) pageFlowMap.get("transferType");
        String transferLevel = (String) pageFlowMap.get("transferLevel");
        Integer fiscalYear = (Integer) pageFlowMap.get("fiscalYear");
        String bureau = (String) pageFlowMap.get("bureauCode");
        String office = (String) pageFlowMap.get("officeCode");
        String division = (String) pageFlowMap.get("division");
        String fundCode = (String) pageFlowMap.get("fundCode");

        //        exec = bindings.getOperationBinding("ExecuteFundLookup");
        //        exec.execute();
        exec = bindings.getOperationBinding("setFundsInfoView");
        exec.execute();
        if (exec.getErrors().isEmpty()) {
            DCIteratorBinding iter = bindings.findIteratorBinding("FundsInfoView1Iterator");
            if (iter.getCurrentRow() != null) {
                Row r = iter.getCurrentRow();
                BigDecimal availableAmt = new BigDecimal(0);
                BigDecimal fromAvailableAmt = new BigDecimal(0);
                BigDecimal toAvailableAmt = new BigDecimal(0);
                String parentLevel = null;
                String parentName = null;
                switch (transferType + transferLevel) {
                case "TransferBureau":
                        availableAmt = null;
                        toAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailFunds");
                        break;
                case "RecoverBureau":
                        availableAmt = (BigDecimal) r.getAttribute("BureauAvailFunds");
                        fromAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailFunds");
                        parentLevel = "Bureau";
                        break;
                case "TransferOffice":
                    availableAmt = (BigDecimal) r.getAttribute("BureauAvailFunds");
                    parentLevel = "Bureau";
                    parentName=bureau;
                    fromAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailFunds");
                    toAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailFunds");
                    break;
                case "RecoverOffice":
                    availableAmt = (BigDecimal) r.getAttribute("OfficeAvailFunds");
                    parentLevel = "Bureau";
                    parentName=bureau;
                    toAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailFunds");
                    fromAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailFunds");
                    break;
                case "TransferDivision":
                    availableAmt = (BigDecimal) r.getAttribute("OfficeAvailFunds");
                    parentLevel = "Office";
                    parentName=office;
                    fromAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailFunds");
                    toAvailableAmt = (BigDecimal) r.getAttribute("DivisionAvailFunds");
                    break;
                case "RecoverDivision":
                    availableAmt = (BigDecimal) r.getAttribute("DivisionAvailFunds");
                    parentLevel = "Office";
                    parentName=office;
                    toAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailFunds");
                    fromAvailableAmt = (BigDecimal) r.getAttribute("DivisionAvailFunds");
                    break;
                }
                if ((TRANSFER_FROM_OFFICE.equalsIgnoreCase(transferLevel) || TRANSFER_FROM_DIV.equalsIgnoreCase(transferLevel)) && availableAmt != null &&
                    availableAmt.compareTo(BigDecimal.ZERO) == 0) {
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "No available amt",
                                         "No available amount to " +
                                         (RECOVER.equalsIgnoreCase(transferType) ? "recover " : "add") +
                                         ("Transfer".equals(transferType) ?
                                          " to " + transferLevel + " from " + parentLevel + " "+parentName:
                                          " from " + transferLevel + " to " + parentLevel + " "+parentName));
                    FacesContext.getCurrentInstance().addMessage(null, msg);

                }

                if (TRANSFER_FROM_BUREAU.equalsIgnoreCase(transferLevel) && RECOVER.equalsIgnoreCase(transferType) &&
                    availableAmt != null && availableAmt.compareTo(BigDecimal.ZERO) == 0) {
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "No available amt",
                                         "No available amount to recover.");
                    FacesContext.getCurrentInstance().addMessage(null, msg);

                }
                pageFlowMap.put("availableAmt", availableAmt);
                pageFlowMap.put("toAvailableAmt", toAvailableAmt);
                pageFlowMap.put("fromAvailableAmt", fromAvailableAmt);
                if (availableAmt != null)
                    pageFlowMap.put("availableAmtFormat", currencyFormat(availableAmt));
                pageFlowMap.put("parentLevel", parentLevel);
                pageFlowMap.put("parentName", parentName);
                pageFlowMap.put("isSendEmail", false);
                
                if (transferLevel.equals("Team")) {
                    exec = bindings.getOperationBinding("ChartExecuteWithParams");
                    exec.execute();
                    exec = bindings.getOperationBinding("ChartAPlusBExecuteWithParams");
                    exec.execute();
                    /*                     exec = bindings.getOperationBinding("ChartTeamExecuteWithParams");
                    exec.execute(); */
                }
                exec = bindings.getOperationBinding("getFundTransferEmailList");
                exec.execute();
                String transferList = (String) exec.getResult();
                    Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
                    MessageBean mb = new MessageBean();
                    String emailCC = mb.getUserEmail();
                    String emailTo = ""+appScope.get("FIN_EMAIL");
                    if(transferList!=null)
                        emailTo+="; "+transferList;
                pageFlowMap.put("emailTo", emailTo);
                pageFlowMap.put("emailCC", emailCC);
                
                return "next";
            } else {
                pageFlowMap.put("message",
                                "Error finding Funds information. " + transferType + " : " + transferLevel + " : " +
                                fiscalYear + " : " + division + " : " + "");
                return "error";
            }
        } else {
            pageFlowMap.put("message",
                            "Error finding Funds information. " + transferType + " : " + transferLevel + " : " +
                            fiscalYear + " : " + division + " : " + "");
            return "error";
        }
    }

    private String currencyFormat(BigDecimal number) {
        String returnVal = "";
        if (null != number) {
            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CANADA);
            double doubleVal = number.doubleValue();
            returnVal = format.format(doubleVal);
        }
        return returnVal;
    }

    public String transferFunds() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        FacesContext context = FacesContext.getCurrentInstance();
        OperationBinding exec = bindings.getOperationBinding("addOrrecoverFunds");
        exec.execute();
        String result = (String) exec.getResult();
        pfm.put("confirmMessage", result);
        if("true".equals(result)) {
            exec = bindings.getOperationBinding("ExecuteWithParams");
            exec.execute();
            DCIteratorBinding iter = bindings.findIteratorBinding("FundsInfoView1Iterator");
            Row r = iter.getCurrentRow();
            BigDecimal fromAvailableAmt = new BigDecimal(0);
            BigDecimal toAvailableAmt = new BigDecimal(0);
            String transferType = (String) pfm.get("transferType");
            String transferLevel = (String) pfm.get("transferLevel");
            if(r!=null)
            {
                switch (transferType + transferLevel) {
                case "TransferBureau":
                    toAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailFunds");
                    break;
                case "RecoverBureau":
                    fromAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailFunds");
                    break;
                case "TransferOffice":
                    fromAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailFunds");
                    toAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailFunds");
                    break;
                case "RecoverOffice":
                    toAvailableAmt = (BigDecimal) r.getAttribute("BureauAvailFunds");
                    fromAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailFunds");
                    break;
                case "TransferDivision":
                    fromAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailFunds");
                    toAvailableAmt = (BigDecimal) r.getAttribute("DivisionAvailFunds");
                    break;
                case "RecoverDivision":
                    toAvailableAmt = (BigDecimal) r.getAttribute("OfficeAvailFunds");
                    fromAvailableAmt = (BigDecimal) r.getAttribute("DivisionAvailFunds");
                    break;
                }
            }
            pfm.put("toAvailableAmt", toAvailableAmt);
            pfm.put("fromAvailableAmt", fromAvailableAmt);
            
            Map<String, Object> returnMap = new HashMap<String, Object>();
            BigDecimal transferAmt = (BigDecimal) pfm.get("transferAmt");
            returnMap.put("transferType",pfm.get("transferType"));
            returnMap.put("transferLevel",pfm.get("transferLevel"));
            returnMap.put("fiscalYear",pfm.get("fiscalYear"));
            returnMap.put("fundCode",pfm.get("fundCode"));
            returnMap.put("bureauCode",pfm.get("bureauCode"));
            returnMap.put("officeCode",pfm.get("officeCode"));
            returnMap.put("division",pfm.get("division"));
            returnMap.put("fundCode", pfm.get("fundCode"));
            returnMap.put("transferAmt", currencyFormat(transferAmt));
            returnMap.put("comments", pfm.get("comments"));
            returnMap.put("parentLevel", pfm.get("parentLevel"));
            returnMap.put("parentName", pfm.get("parentName"));

            returnMap.put("toAvailableAmt",currencyFormat(toAvailableAmt));
            returnMap.put("fromAvailableAmt",currencyFormat(fromAvailableAmt));
            returnMap.put("emailTo", pfm.get("emailTo"));
            returnMap.put("emailCC", pfm.get("emailCC"));
            pfm.put("transferDetails", returnMap);
            pfm.put("isSendEmail", pfm.get("isSendEmail"));

            
        }
        return (String) result;
    }

    public void fundCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...
        String value = (String) object;
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if (value != null) {
            value = value.toUpperCase();
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("FundLookupViewCheckIterator");
            Key fundkey = new Key(new Object[] { pfm.get("fiscalYear"), value });
            Row r[] = iter.getRowSetIterator().findByKey(fundkey, 1);
            if (r.length < 1) {
                facesContext.addMessage(uIComponent.getId(),
                                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                         "Invalid Fund Code. Use Search to find and select correct Fund Code"));
            }

        }


    }

    public void selectFundCodeLaunchPopupListener(LaunchPopupEvent launchPopupEvent) {
        // Add event code here...
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams1");
        if (null != pageFlowMap.get("fiscalYear")) {
            exec.getParamsMap().put("p_fy", pageFlowMap.get("fiscalYear"));
            exec.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(launchPopupEvent.getComponent().getParent());
        }
    }

    public void selectFundCodePopupReturnListener(ReturnPopupEvent returnPopupEvent) {
        // Add event code here...
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("FundLOV1Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row r = rsi.getCurrentRow();
        if (r != null) {
            pfm.put("fundCode", r.getAttribute("FundCode"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent());
        }
        rsi.closeRowSetIterator();

    }

    public String sendConfirmationEmail() {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        Map transferDetails = (Map) pageFlowMap.get("transferDetails");
        String result = (String) pageFlowMap.get("confirmMessage");
        if ("true".equals(result)) {
            if ((Boolean) pageFlowMap.get("isSendEmail")) {
                    String emailList = sendFundsConfirmationEmail(result, transferDetails, (Boolean) pageFlowMap.get("isSendEmail"));
                    if (emailList != null)
                        pageFlowMap.put("emailList", emailList);
                }
            }
        return "true";
    }

    private String sendFundsConfirmationEmail(String result, Map transferDetails, Boolean isSendEmail) {
        Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
        AbacusEmail email = new AbacusEmail();
        MessageBean mb = new MessageBean();
        String toAdd = (String) transferDetails.get("emailTo");
        String ccAdd = (String) transferDetails.get("emailCC");

        FacesContext context = FacesContext.getCurrentInstance();
        String transferLevel = (String) transferDetails.get("transferLevel");
        String transferType = (String) transferDetails.get("transferType");
        String parentLevel = (String) transferDetails.get("parentLevel");
        String parentName = (String) transferDetails.get("parentName");
        if ("true".equals(result)) {
            if (null != isSendEmail && isSendEmail) {
                String username = ADFContext.getCurrent().getSecurityContext().getUserName();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                String fdate = sdf.format(new Date());

                StringBuilder builder = new StringBuilder();
                builder.append("<table border=\"1\" style=\"border-collapse:collapse; width: 500px;\" cellpadding=\"5\">" +
                               "<tbody>");
                if (!"Bureau".equals(transferLevel)) {

                    builder.append("<tr>" + "<td colspan=\"2\" style=\"text-align:left\"> <b> " +
                                   (transferType.equalsIgnoreCase("Transfer") ? "From " : "To ") + " </b> </td>" +
                                   "</tr>");
                    builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Fiscal Year</td>" +
                                   "<td style=\"text-align:left\">" + transferDetails.get("fiscalYear") + "</td>" +
                                   "</tr>");
                    builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">" +
                                   (transferType.equalsIgnoreCase("Transfer") ? "From " : "To ") + "</td>" +
                                   "<td style=\"text-align:left\"> "+parentLevel+" "+parentName+" </td>" + "</tr>");
                    builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Fund Code</td>" +
                                   "<td style=\"text-align:left\">" + transferDetails.get("fundCode") + "</td>" +
                                   "</tr>");
                    builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Available Fund Amt</td>" +
                                   "<td style=\"text-align:left\">" + (transferType.equalsIgnoreCase("Transfer") ? transferDetails.get("fromAvailableAmt"): transferDetails.get("toAvailableAmt")) + "</td>" +
                                   "</tr>");
                }


                builder.append("<tr>" + "<td colspan=\"2\" style=\"text-align:left\"> <b> " +
                               (transferType.equalsIgnoreCase("Transfer") ? "To " : "From ") + " </b> </td>" + "</tr>");

                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Fiscal Year</td>" +
                               "<td style=\"text-align:left\">" + transferDetails.get("fiscalYear") + "</td>" +
                               "</tr>");
                if ("Bureau".equals(transferLevel)||"Office".equals(transferLevel)||"Division".equals(transferLevel)) {
                    builder.append("<tr>" +
                        "<td style=\"text-align:right; width:30%;\">Bureau</td>" + "<td style=\"text-align:left\">" +
                        transferDetails.get("bureauCode") + "</td>" + "</tr>");
                }
                if ("Office".equals(transferLevel)||"Division".equals(transferLevel)) {
                    builder.append("<tr>" +
                        "<td style=\"text-align:right; width:30%;\">Office</td>" + "<td style=\"text-align:left\">" +
                        transferDetails.get("officeCode") + "</td>" + "</tr>");
                }
                if ("Division".equals(transferLevel)) {
                    builder.append("<tr>" +
                        "<td style=\"text-align:right; width:30%;\">Division</td>" + "<td style=\"text-align:left\">" +
                        transferDetails.get("division") + "</td>" + "</tr>");
                }
                builder.append("<tr>" + "<td style=\"text-align:right\">Fund Code</td>" +
                               "<td style=\"text-align:left\">" + transferDetails.get("fundCode") + "</td>" + "</tr>");
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">" + ("Fund Amt") + "</td>" +
                               "<td style=\"text-align:left\">" + transferDetails.get("transferAmt") + "</td>" +
                               "</tr>");

                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Available Fund Amt</td>" +
                               "<td style=\"text-align:left\">" + (transferType.equalsIgnoreCase("Transfer") ? transferDetails.get("toAvailableAmt"): transferDetails.get("fromAvailableAmt")) + "</td>" +
                               "</tr>");

                builder.append("<tr>" +
                               "<td colspan=\"2\" style=\"text-align:left; word-wrap: break-word;\"> <b>Comments: </b> " +
                               transferDetails.get("comments") + "</td>" + "</tr>");

                builder.append("<tr>" + "<td colspan=\"2\" style=\"text-align:left\"> <b>Updated By </b>" + username +
                               " <b> On </b>" + fdate + "</td>" + "</tr>" + "</tbody>" + "</table>");
                
                String htmlMsg = builder.toString();
                String sub = "";
                sub =transferLevel +
                    (TRANSFER.equals(transferType) ? " Add" : " Recover") + " Funds ";
                Boolean mailSent = email.sendHtmlEmail(toAdd, ccAdd, sub, htmlMsg, null, null);
                if (mailSent) {
                    return "Email sent to " + toAdd + " " + ccAdd;
                }
            }
        }
        return null;
    }

    public void addOrRecoverFundsAvailableAmtDoubleClick(ClientEvent clientEvent) {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pageFlowMap.put("transferAmt", pageFlowMap.get("availableAmt"));
        AdfFacesContext.getCurrentInstance().addPartialTarget(clientEvent.getComponent().getParent().getParent());
    }

    public void validateTransferAmount(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...
        BigDecimal value = (BigDecimal) object;
        if (value != null) {
            Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            BigDecimal avail = (BigDecimal) pageFlowMap.get("availableAmt");

            // check if value is less than or equal to zero
            if (value.doubleValue() <= 0) {
                FacesContext.getCurrentInstance().addMessage(uIComponent.getId(),
                                                             new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                              "You entered an amount of " +
                                                                              currencyFormat(value) +
                                                                              ". Enter an amount that is at least $0.01"));
            }

            if (!(TRANSFER.equalsIgnoreCase(String.valueOf(pageFlowMap.get("transferType"))) &&
                  TRANSFER_FROM_OFFICE.equalsIgnoreCase(String.valueOf(pageFlowMap.get("transferLevel"))))) {
                if (avail != null && value.compareTo(avail) == 1) {
                    FacesContext.getCurrentInstance().addMessage(uIComponent.getId(),
                                                                 new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                                  "You entered an amount of " +
                                                                                  currencyFormat(value) +
                                                                                  ". Enter an amount that is less than or equal to available amount " +
                                                                                  currencyFormat(avail)));

                }
            }
        }
    }

    public String getEmailToolTip() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        return "An email will be sent to " + pfm.get("emailTo") + "; " + pfm.get("emailCC");
    }

    public void fiscalYearValueChangeListner(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        if (null != valueChangeEvent.getNewValue()) {
            Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            pageFlowMap.put("fundCode", null);
            AdfFacesContext.getCurrentInstance().addPartialTarget(valueChangeEvent.getComponent().getParent().getParent());
        }
    }
    
    public void divisionCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...
        String value = (String) object;
        if (value != null) {
            value = value.toUpperCase();
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("DivisionLookupViewCheckIterator");
            Key division = new Key(new Object[] { value });
            Row r[] = iter.getRowSetIterator().findByKey(division, 1);
            if (r.length < 1) {
                facesContext.addMessage(uIComponent.getId(),
                                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                         "Invalid Division Code. Use Search to find and select correct Division Code"));
            }

        }


    }

    public void selectDivisionCodeLaunchPopupListener(LaunchPopupEvent launchPopupEvent) {
        // Add event code here...
    }

    public void selectDivisionCodePopupReturnListener(ReturnPopupEvent returnPopupEvent) {
        // Add event code here...
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("DivisionActiveLOV1Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row r = rsi.getCurrentRow();
        if (r != null) {
            pfm.put("division", r.getAttribute("OfdaDivisionCode"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent());
        }
        rsi.closeRowSetIterator();

    }

}

