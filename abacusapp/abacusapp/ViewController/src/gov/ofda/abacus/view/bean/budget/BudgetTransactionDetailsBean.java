package gov.ofda.abacus.view.bean.budget;

import gov.ofda.abacus.view.base.JSFUtils;

import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class BudgetTransactionDetailsBean {
    public BudgetTransactionDetailsBean() {
    }

    public String checkForRequiredFields() {
        String missingInfo = "";
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String level = (String) pfm.get("level");
        Integer fiscalYear = (Integer) pfm.get("fiscalYear");
        String bureauCode = (String) pfm.get("bureauCode");
        String officeCode = (String) pfm.get("officeCode");
        String division = (String) pfm.get("division");
        String team = (String) pfm.get("team");
        String projectNbr=(String)pfm.get("projectNbr");
        if(!(level.equals("Bureau")||level.equals("Office")||level.equals("Division")||level.equals("Team")||level.equals("Project"))) {
            pfm.put("message",
                     "Unknown Level. Valid values are Bureau, Office, Division, Team and Project");
            return "error";
        }
        SecurityContext ctx=ADFContext.getCurrent().getSecurityContext();
        if(("Bureau".equals(level) && ctx.isUserInRole("APP_BBL"))
           ||  ("Office".equals(level) && ctx.isUserInRole("APP_BBL"))
           ||("Division".equals(level) && ctx.isUserInRole("APP_OL"))
           ||("Team".equals(level) && ctx.isUserInRole("APP_DL"))
           ||("Project".equals(level) && (ctx.isUserInRole("APP_BBL")||ctx.isUserInRole("APP_OL")||ctx.isUserInRole("APP_DL")||ctx.isUserInRole("APP_TL")))
          ) {
            pfm.put("isEditable",true);
        }
        else
            pfm.put("isEditable",false);
        
            
        if(fiscalYear==null)
            missingInfo+="Fiscal Year, ";
        if((level.equals("Bureau")||level.equals("Office")||level.equals("Division")||level.equals("Team"))&& bureauCode==null)
            missingInfo+="Bureau, ";
        if((level.equals("Office")||level.equals("Division")||level.equals("Team"))&& officeCode==null)
            missingInfo+="Office, ";
        if((level.equals("Division")||level.equals("Team"))&& division==null)
            missingInfo+="Division, ";
        if(level.equals("Team")&& team==null)
            missingInfo+="Team, ";
        if(level.equals("Project")&& projectNbr==null)
            missingInfo+="Project, ";
        
        if (missingInfo.length() == 0)
            return level;
        pfm.put("message",
                 "Missing required value(s): " + missingInfo);
        return "error";
    }
    
    public void saveChanges() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        Boolean isCommitEnabled = (Boolean) JSFUtils.resolveExpression("#{bindings.Commit.enabled}");
        if (!isCommitEnabled) {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "No changes","No changes to save");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;
        }
        exec = bindings.getOperationBinding("Commit");
        exec.execute();
        if (exec.getErrors().isEmpty()) {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully","Saved successfully");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    public void cancelChanges() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Rollback");
        exec.execute();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if (exec.getErrors().isEmpty())
        {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancelled successfully","Cancelled successfully");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            NavigationHandler  nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            if(nvHndlr!=null)
                nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "cancel");
        }
    }
}
