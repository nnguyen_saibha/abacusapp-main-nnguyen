package gov.ofda.abacus.view.bean;

import java.io.Serializable;

import java.util.Map;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.binding.OperationBinding;

public class AppBean implements Serializable {
    @SuppressWarnings("compatibility:166060771578360955")
    private static final long serialVersionUID = 5586899952192092631L;
    private Map<String,String> constants;



    public AppBean() {
        super();
        
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getAppConstants");
        exec.execute();
        constants = (Map<String, String>) exec.getResult();        
    }
    public void setConstants(Map<String, String> constants) {
        this.constants = constants;
    }

    public Map<String, String> getConstants() {
        return constants;
    }
}
