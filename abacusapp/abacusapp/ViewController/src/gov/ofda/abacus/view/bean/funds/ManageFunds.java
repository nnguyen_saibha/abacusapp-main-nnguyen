package gov.ofda.abacus.view.bean.funds;

import gov.ofda.abacus.view.base.UIControl;
import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;

import java.math.BigDecimal;

import java.text.NumberFormat;

import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.ReturnPopupEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class ManageFunds extends UIControl implements Serializable {
    @SuppressWarnings("compatibility:-2802850962886768940")
    private static final long serialVersionUID = 1L;
    private static final String TRANSFER_AMOUNT = "transferAmt";
    private static final String FUNDS_REQUIRED = "fundsRequired";
    private static final String PROJECT_BUDGET = "projectBudget";
    private static final String BUREAU = "Bureau";
    private static final String OFFICE = "Office";
    private static final String DIVISION = "Division";
    private static final String FUNDS_AVAILABLE = "fundsAvail";
    private static final String PROJECT_FUNDS_AVAILABLE = "projectAvailFunds";
    private static final String FUND_CODE = "fundCode";
    private static final String ACITON_APPROVED_AMT = "actionApprovedAmt";
    private static final String FROM = "from";

    public ManageFunds() {
        super();
    }

    public String setActivityDetails() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map<String, Object> pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        OperationBinding exec = null;
        exec = bindings.getOperationBinding("isVActivityAmtValid");
        Boolean isValid = (Boolean) exec.execute();
        if (!isValid) {
            pfp.put("confirmMessage",
                    "Error initializing add project funds" + " FY:" + pfp.get("budgetFY") + " PNbr:" +
                    pfp.get("projectNbr"));
            return "close";
        }
        //Set ActivityEditView
        exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.execute();

        exec = bindings.getOperationBinding("setVFundsBureauView");
        exec.execute();
        String vFundsBureauViewError = (String) exec.getResult();
        if (vFundsBureauViewError != null) {
            pfp.put("confirmMessage",
                    vFundsBureauViewError + " FY:" + pfp.get("budgetFY") + " PNbr:" + pfp.get("projectNbr"));
            return "close";
        }
        
        exec = bindings.getOperationBinding("setVFundsOFDAView");
        exec.execute();
        String vFundsOFDAViewError = (String) exec.getResult();
        if (vFundsOFDAViewError != null) {
            pfp.put("confirmMessage",
                    vFundsOFDAViewError + " FY:" + pfp.get("budgetFY") + " PNbr:" + pfp.get("projectNbr"));
            return "close";
        }

        exec = bindings.getOperationBinding("setVFundsDivisionView");
        exec.execute();
        String vFundsDivisonViewError = (String) exec.getResult();
        if (vFundsDivisonViewError != null) {
            pfp.put("confirmMessage",
                    vFundsDivisonViewError + " FY: " + pfp.get("budgetFY") + " PNbr: " + pfp.get("projectNbr"));
            return "close";
        }

        BigDecimal approvedAmt = new BigDecimal(getAmt("ApprovedAmt").toString());
        BigDecimal distributedAmt = new BigDecimal(getAmt("PhDistributedAmt").toString());
        BigDecimal allocatedAmt = new BigDecimal(getAmt("AllocatedAmt").toString());
        BigDecimal projectBudget = new BigDecimal(getAmt("BudgetAmt").toString());
        BigDecimal availAmt = new BigDecimal(getAmt("AvailablePhAmt").toString());

        pfp.put("remainingApprovedAmt", approvedAmt.subtract(distributedAmt));
        pfp.put(FUNDS_REQUIRED, approvedAmt.subtract(allocatedAmt));
        pfp.put(FUNDS_REQUIRED + "Fmt", currencyFormat(approvedAmt.subtract(allocatedAmt)));
        pfp.put(PROJECT_BUDGET, projectBudget);
        pfp.put("projectBudgetFmt", currencyFormat(projectBudget));
        pfp.put(PROJECT_FUNDS_AVAILABLE, availAmt);
        BigDecimal actionApprovedAmt = (BigDecimal) pfp.get(ACITON_APPROVED_AMT);
        if (null != actionApprovedAmt) {
            pfp.put("actionApprovedAmtFmt", currencyFormat(actionApprovedAmt));
        }
        //        if(pfp.get(ACITON_APPROVED_AMT)!=null)
        //            pfp.put(TRANSFER_AMOUNT,pfp.get(ACITON_APPROVED_AMT));
        pfp.put("isSendEmail", false);

        // Make the source or From defaults to OFDA Funds
        SecurityContext ctx=ADFContext.getCurrent().getSecurityContext();

            if(ctx.isUserInRole("APP_BFL")){
                pfp.put(FROM, BUREAU);
            }else if(ctx.isUserInRole("APP_FM")){
                pfp.put(FROM, OFFICE);
            }
        
        return "next";
    }

    public String transferToActivity() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map<String, Object> pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        OperationBinding exec = bindings.getOperationBinding("transferFunds");
        exec.execute();
        pfp.put("confirmMessage", exec.getResult());
        HashMap<String, String> td = new HashMap<String, String>();
        String from = (String) pfp.get("from");
        switch (from) {
        case BUREAU:
                td.put("from", "Bureau");
                break;
        case OFFICE:
            td.put("from", "Office");
            break;
        case DIVISION:
            td.put("from", "Division");
            break;
        default:
            td.put("from", from);
        }
        td.put("budgetFY", "" + pfp.get("budgetFY"));
        td.put("projectNbr", (String) pfp.get("projectNbr"));
        td.put("projectName", (String) ((AttributeBinding) bindings.getControlBinding("ProjectName")).getInputValue());
        td.put(FUND_CODE, (String) pfp.get(FUND_CODE));
        td.put(TRANSFER_AMOUNT, NumberFormat.getCurrencyInstance().format(pfp.get(TRANSFER_AMOUNT)));
        td.put("comments", (String) pfp.get("comments"));
        td.put("bureauCode",
               (String) ((AttributeBinding) bindings.getControlBinding("BureauCode")).getInputValue());
        td.put("officeCode",
               (String) ((AttributeBinding) bindings.getControlBinding("OfficeCode")).getInputValue());
        td.put("divisionCode",
               (String) ((AttributeBinding) bindings.getControlBinding("OfdaDivisionCode")).getInputValue());
        td.put("teamCode", (String) ((AttributeBinding) bindings.getControlBinding("OfdaTeamCode")).getInputValue());
        BigDecimal tmp = ((BigDecimal) pfp.get(TRANSFER_AMOUNT)).add((BigDecimal) pfp.get(PROJECT_FUNDS_AVAILABLE));
        td.put(PROJECT_FUNDS_AVAILABLE, NumberFormat.getCurrencyInstance().format(tmp));
        td.put("projectFundsAvailable", NumberFormat.getCurrencyInstance().format(tmp));
        OperationBinding exec1 = bindings.getOperationBinding("getAvailFunds");
        exec1.execute();
        BigDecimal fundsAvail = (BigDecimal) exec1.getResult();

        td.put("fundsAvailable", NumberFormat.getCurrencyInstance().format(fundsAvail));


        pfp.put("transferDetails", td);
        pfp.put("isSendEmail", pfp.get("isSendEmail"));
        return (String) exec.getResult();
    }

    public void projectSourceChgLsnr(ValueChangeEvent vce) {
        Map<String, Object> pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        //        if(pfp.get(ACITON_APPROVED_AMT)!=null)
        //            pfp.put(TRANSFER_AMOUNT,pfp.get(ACITON_APPROVED_AMT));
        //        else
        pfp.put(TRANSFER_AMOUNT, null);
        pfp.put(FUNDS_AVAILABLE, null);
        pfp.put("fundsAvailFmt", null);
        pfp.put(FUND_CODE, null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(vce.getComponent().getParent().getParent().getParent().getParent());
    }

    private void setFundCodeAmts(String iterName) {
        Map<String, Object> pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding(iterName);
        Row r = iter.getCurrentRow();
        if (r != null) {
            BigDecimal fundsAvail = (BigDecimal) r.getAttribute("AvailableFundsAmt");
            BigDecimal transferAmt = (BigDecimal) pfp.get(TRANSFER_AMOUNT);
            pfp.put(FUNDS_AVAILABLE, fundsAvail);
            pfp.put("fundsAvailFmt", currencyFormat(fundsAvail));
            pfp.put(FUND_CODE, r.getAttribute("FundCode"));
            /* if(transferAmt!=null && fundsAvail.compareTo(transferAmt)==-1)
        pfp.put(TRANSFER_AMOUNT, fundsAvail);*/
            OperationBinding exec = bindings.getOperationBinding("getAvailFunds");
            exec.execute();
            BigDecimal t = (BigDecimal) exec.getResult();
            if (t == null)
                pfp.put("toAvailableAmt", BigDecimal.ZERO);
            else
                pfp.put("toAvailableAmt", t);
        }
    }
    
    public void bureauPopupReturnLnsr(ReturnPopupEvent returnPopupEvent) {
        setFundCodeAmts("VFundsBureauView1Iterator");
        AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
    }

    public void ofdaPopupReturnLnsr(ReturnPopupEvent returnPopupEvent) {
        setFundCodeAmts("VFundsOFDAView1Iterator");
        AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
    }

    public void divPopupReturnLnsr(ReturnPopupEvent returnPopupEvent) {
        setFundCodeAmts("VFundsDivisionView1Iterator");
        AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent().getParent().getParent().getParent().getParent());
    }

    public void setProjectReqAmtClick(ClientEvent clientEvent) {
        Map<String, Object> pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        BigDecimal req = (BigDecimal) pfp.get(FUNDS_REQUIRED);
        pfp.put(TRANSFER_AMOUNT, req);
        AdfFacesContext.getCurrentInstance().addPartialTarget(clientEvent.getComponent().getParent().getParent());
    }

    public void setProjectAvailAmtClick(ClientEvent clientEvent) {
        Map<String, Object> pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        BigDecimal avail = (BigDecimal) pfp.get(FUNDS_AVAILABLE);
        pfp.put(TRANSFER_AMOUNT, avail);
        AdfFacesContext.getCurrentInstance().addPartialTarget(clientEvent.getComponent().getParent().getParent());
    }

    public void setProjectBudgetAmtClick(ClientEvent clientEvent) {
        Map<String, Object> pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        BigDecimal budget = (BigDecimal) pfp.get(PROJECT_BUDGET);
        pfp.put(TRANSFER_AMOUNT, budget);
        AdfFacesContext.getCurrentInstance().addPartialTarget(clientEvent.getComponent().getParent().getParent());
    }

    public String getEmailAddr() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        String budgetFY = ""+((AttributeBinding) bindings.get("BudgetFy")).getInputValue();
        String projectNbr = (String) ((AttributeBinding) bindings.get("ProjectNbr")).getInputValue();
        MessageBean mbean = new MessageBean();
        Map<String, String> emailMap = mbean.getActivityFundTransferEmailList(budgetFY,projectNbr);
        return (emailMap.get("emailList"));

    }

    private Number getAmt(String attrName) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        AttributeBinding attrBinding = (AttributeBinding) bindings.get(attrName);
        String tmp = (String) attrBinding.getInputValue();
        Number availAmt = 0;
        if (tmp != null) {
            try {
                availAmt = NumberFormat.getCurrencyInstance().parse(tmp);
            } catch (Exception e) {
                availAmt = 0;
            }
        }
        return availAmt;
    }

    public void copyActionApprovedAmt(ClientEvent clientEvent) {
        Map<String, Object> pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        BigDecimal actionApprovedAmt = (BigDecimal) pfp.get(ACITON_APPROVED_AMT);
        if (null != actionApprovedAmt) {
            pfp.put(TRANSFER_AMOUNT, actionApprovedAmt);
        }
        BigDecimal fundsAvail = (BigDecimal) pfp.get(FUNDS_AVAILABLE);
        int compareFundsAvail = actionApprovedAmt.compareTo(fundsAvail);

        BigDecimal projBudget = (BigDecimal) pfp.get(PROJECT_BUDGET);
        int compareProjBudget = actionApprovedAmt.compareTo(projBudget);

        if (compareFundsAvail == 1 && compareProjBudget == 1) {
            FacesContext.getCurrentInstance().addMessage("transferAmtFundsId",
                                                         new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                          "You entered an amount of " +
                                                                          currencyFormat(actionApprovedAmt) +
                                                                          ". Enter an amount that is less than or equal to available amount " +
                                                                          currencyFormat(fundsAvail) +
                                                                          " and budget amount " +
                                                                          currencyFormat(projBudget)));
        } else if (compareFundsAvail == 1) {
            FacesContext.getCurrentInstance().addMessage("transferAmtFundsId",
                                                         new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                          "You entered an amount of " +
                                                                          currencyFormat(actionApprovedAmt) +
                                                                          ". Enter an amount that is less than or equal to available amount " +
                                                                          currencyFormat(fundsAvail)));
        } else if (compareProjBudget == 1) {
            FacesContext.getCurrentInstance().addMessage("transferAmtFundsId",
                                                         new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                          "You entered an amount of " +
                                                                          currencyFormat(actionApprovedAmt) +
                                                                          ". Enter an amount that is less than or equal to budget amount " +
                                                                          currencyFormat(projBudget)));
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(clientEvent.getComponent().getParent().getParent());
    }

    public void transferAmtValueChnageListner(ValueChangeEvent valueChangeEvent) {
        BigDecimal value = (BigDecimal) valueChangeEvent.getNewValue();
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        BigDecimal fundsAvail = (BigDecimal) pageFlowMap.get(FUNDS_AVAILABLE);
        int compareFundsAvail = value.compareTo(fundsAvail);

        BigDecimal projBudget = (BigDecimal) pageFlowMap.get(PROJECT_BUDGET);
        int compareProjBudget = value.compareTo(projBudget);

        if (null != value) {

            if (value.doubleValue() <= 0) {
                FacesContext.getCurrentInstance().addMessage(valueChangeEvent.getComponent().getId(),
                                                             new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                              "You entered an amount of " +
                                                                              currencyFormat(value) +
                                                                              ". Enter an amount that is at least $0.01"));
            }
            // compare against planned amt
            if (compareFundsAvail == 1 && compareProjBudget == 1) {
                FacesContext.getCurrentInstance().addMessage(valueChangeEvent.getComponent().getId(),
                                                             new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                              "You entered an amount of " +
                                                                              currencyFormat(value) +
                                                                              ". Enter an amount that is less than or equal to available amount " +
                                                                              currencyFormat(fundsAvail) +
                                                                              " and budget amount " +
                                                                              currencyFormat(projBudget)));
            } else if (compareFundsAvail == 1) {
                FacesContext.getCurrentInstance().addMessage(valueChangeEvent.getComponent().getId(),
                                                             new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                              "You entered an amount of " +
                                                                              currencyFormat(value) +
                                                                              ". Enter an amount that is less than or equal to available amount " +
                                                                              currencyFormat(fundsAvail)));
            } else if (compareProjBudget == 1) {
                FacesContext.getCurrentInstance().addMessage(valueChangeEvent.getComponent().getId(),
                                                             new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                              "You entered an amount of " +
                                                                              currencyFormat(value) +
                                                                              ". Enter an amount that is less than or equal to budget amount " +
                                                                              currencyFormat(projBudget)));
            }
        }
    }
}
