package gov.ofda.abacus.view.base;

import java.io.IOException;

import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.security.auth.Subject;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.adf.share.ADFContext;

/*import weblogic.security.URLCallbackHandler;
import weblogic.security.services.Authentication;

import weblogic.servlet.security.ServletAuthentication;*/

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.share.security.SecurityContext;

import oracle.adf.share.security.identitymanagement.AttributeFilter;
import oracle.adf.share.security.identitymanagement.UserManager;

import oracle.adf.share.security.identitymanagement.UserProfile;

import oracle.security.idm.IdentityStore;


/**
 * LoginHandler.java
 * Purpose: To handle login for the application.
 *
 * @version 1.0 09/09/2011
 */
public class LoginHandler {
    private String _username;
    private String _password;
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.view.base.LoginHandler.class);
    private static String initialPage = "/faces/welcome";

    /**
     * Class Constructor
     */
    public LoginHandler() {
    }

    /**
     * @param _username setter method for _username
     */
    public void setUsername(String _username) {
        this._username = _username;
    }

    /**
     * @return getter method for _username
     */
    public String getUsername() {
        return _username;
    }

    /**
     * @param _password setter method for _password
     */
    public void setPassword(String _password) {
        this._password = _password;
    }

    /**
     * @return getter method for _password
     */
    public String getPassword() {
        return _password;
    }

    /**
     * Will authenticate the user and redirect to default page upon successful redirection.
     * Will display a message if authentication is not successful or user is Inactive.
     * @return will return null in all cases.
     */
    public String performLogin() {
    /*    byte[] pw = _password.getBytes();
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
        try {
            Subject mySubject = Authentication.login(new URLCallbackHandler(_username, pw));
            ServletAuthentication.runAs(mySubject, request);
            String loginUrl = "/adfAuthentication?success_url="; // + ctx.getViewRoot().getViewId();
            loginUrl = loginUrl.concat(initialPage);
            logger.log(logger.NOTIFICATION, "login URL: " + loginUrl.toString());
            HttpServletResponse response = (HttpServletResponse)ctx.getExternalContext().getResponse();
            sendForward(request, response, loginUrl, true);
        } catch (FailedLoginException fle) {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Inactive or Incorrect Username or Password",
                                 "An Inactive or Incorrect Username or Password was specified");
            FacesContext.getCurrentInstance().addMessage("pt_it1", msg);
            logger.log(logger.ERROR, "Inactive or Incorrect Username or Password " + this.getUsername());
        } catch (LoginException le) {
            reportUnexpectedLoginError("loginException", le);
            logger.log(logger.ERROR, le.toString());
        }
        _password = null;*/
        return null;
    }

    /**
     * To forward and complete the response.
     *
     * @param request
     * @param response
     * @param loginUrl
     * @param islogin If True will check if the current user is active or not in appx_user database table .
     */
    private void sendForward(HttpServletRequest request, HttpServletResponse response, String loginUrl,
                             boolean islogin) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        RequestDispatcher dispatcher = request.getRequestDispatcher(loginUrl);
        try {
            dispatcher.forward(request, response);

        } catch (ServletException se) {
            logger.log(logger.ERROR, "The user is unable to login due to the exception", se);
            reportUnexpectedLoginError("ServletException", se);
        } catch (IOException ie) {
            logger.log(logger.ERROR, "The user is unable to login due to the exception", ie);
            reportUnexpectedLoginError("IOException", ie);
        }
        ctx.responseComplete();
    }

    /**
     *To report Unexpected Login Errors other than invalid username and password.
     *
     * @param errType
     * @param e
     */
    private void reportUnexpectedLoginError(String errType, Exception e) {
        FacesMessage msg =
            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected error During Login", "Unexpected Error during Login (" +
                             errType + "), please consult logs for detail");
        FacesContext.getCurrentInstance().addMessage("pt_it1", msg);
        e.printStackTrace();
    }

    /**
     * To log the user out of the application and redirect to searchAward.
     * @return
     */
    public String performLogout() {
        boolean oamConfigured = false;
        String oamConfiguredStr = null;
        StringBuffer sb = new StringBuffer();
        FacesContext ctx = FacesContext.getCurrentInstance();        
        logger.log(logger.NOTIFICATION, this.getUsername() + " logged out");
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse)ctx.getExternalContext().getResponse();        
        if (oamConfigured) {
            sb.append("/adfAuthentication?logout=true");    
        } else {
            sb.append("/adfAuthentication?logout=true&en_url=/faces/welcome");
        }
        logger.log(logger.NOTIFICATION, "logout URL: " + sb.toString());
        
        sendForward(request, response, sb.toString(), false);
        return null;
    }

    /**
     * getDisplayName fetches the username from the user store.
     * @return displayName
     */
    public String getDisplayName() 
    {
      /*  String displayName = null;
        int sizeLimit = 1;
        UserProfile userProfile = null;
        UserManager userManager = null;
        ArrayList userProfiles = null;
        String userName = null;
        SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
        if (sc != null) {
            userName = sc.getUserName();
            if (userName != null) {
                userManager = new UserManager();
                AttributeFilter[] filter = { new AttributeFilter("USER_ID", userName) };
                userProfiles = userManager.getUserProfileList(sizeLimit, filter);
                if (userProfiles!=null && userProfiles.size() > 0) {
                    userProfile = (UserProfile)userProfiles.get(0);
                    displayName = userProfile.getDisplayName();
                }
                if(displayName==null)
                    displayName=userName.toUpperCase();
        }
        }
        return displayName;*/
      return ADFContext.getCurrent().getSecurityContext().getUserName();
    }
   
      
}

