package gov.ofda.abacus.view.bean;

import java.io.Serializable;

import java.util.Map;

import oracle.adf.view.rich.context.AdfFacesContext;

public class OpenForm implements Serializable {
    @SuppressWarnings("compatibility:1507453620066145843")
    private static final long serialVersionUID = -4664611146552359340L;

    public OpenForm() {
        super();
    }

    public String getServerArgs() {

        String args = "escapeParams=true module=WELCOME.fmx";
        String id;
        Map pageFlowAttrs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String formName = (String) pageFlowAttrs.get("formName");
        switch (formName) {
        case "PROCUREMENT_PLANNING":
            args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y ACTION_ID="; //P_EXIT=Y
            id = "" + pageFlowAttrs.get("actionid");
            args += id;
            break;
        case "DRR_LETTER":
            args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y ACTION_ID="; //P_EXIT=Y
            id = "" + pageFlowAttrs.get("actionid");
            args += id;
            if(pageFlowAttrs.get("letterType")!=null)
                args+=" LETTER_TYPE="+pageFlowAttrs.get("letterType");
            if(pageFlowAttrs.get("letterMemoID")!=null)
                args+=" LETTER_MEMO_ID="+pageFlowAttrs.get("letterMemoID");
            break;
        case "ACTION_CAT":
            args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y"; //P_EXIT=Y
            if (pageFlowAttrs.get("modActionID") != null)
                args += " MOD_ACTION_ID=" + pageFlowAttrs.get("modActionID");
            else if (pageFlowAttrs.get("parentActionID") != null)
                args += " PARENT_ACTION_ID=" + pageFlowAttrs.get("parentActionID");
            if (pageFlowAttrs.get("actionTypeCode") != null)
                args += " ACTION_TYPE=" + pageFlowAttrs.get("actionTypeCode");
            if(pageFlowAttrs.get("projectNbr") != null){
                args += " PROJECT_NBR="+pageFlowAttrs.get("projectNbr");
                args += " BUDGET_FY="+pageFlowAttrs.get("budgetFy");
            }
            break;
            
        case "ACTION_CHANGE":
            args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y"; //P_EXIT=Y
            args += " ACTION_ID=" + pageFlowAttrs.get("actionid");
            break;
        case "ACTION_STATUS":
            args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y"; //P_EXIT=Y
            args += " STATUS_ACTION_ID=" + pageFlowAttrs.get("actionid");
            if (pageFlowAttrs.get("modActionID") != null)
                args += " MOD_ACTION_ID=" + pageFlowAttrs.get("modActionID");
            else if (pageFlowAttrs.get("parentActionID") != null)
                args += " PARENT_ACTION_ID=" + pageFlowAttrs.get("parentActionID");
            break;


        case "WELCOME":
            args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y P_TAB=BUDGET";
            break;

        case "REPORTS_REPOSITORY":
             if(pageFlowAttrs.get("repId") != null){
                args = "escapeParams=true module=" + formName + ".fmx P_REP_ID="+pageFlowAttrs.get("repId") +" P_EXIT=Y";
            }
            else{
                args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y";
            }
            break;

        case "WELCOME_LOOKUP":
            args = "escapeParams=true module=WELCOME.fmx P_EXIT=Y P_TAB=LOOKUPS";
            break;

        case "USER_MANAGER":
            args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y";
            break;

        case "WELCOME_FUNDS":
            args = "escapeParams=true module=WELCOME.fmx P_EXIT=Y P_TAB=FINANCE";
            break;

        case "ADHOC_NAVIGATOR":
            args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y";
            break;

        case "ACTION_NAVIGATOR":
            args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y";
            break;
        case "LOGISTICS":
            args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y";
            break;
        case "ACTIVITY":
                args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y";
                args+=" BUDGET_FY="+pageFlowAttrs.get("budgetFy")+" OFDA_DIVISION_CODE="+pageFlowAttrs.get("ofdaDivisionCode")+" PROJECT_NBR="+pageFlowAttrs.get("projectNbr");
                break;
        case "BUDGET_ACTIVITY":
            args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y";
            args +=
                " BUDGET_FY=" + pageFlowAttrs.get("budgetFy") + " OFDA_DIVISION_CODE=" +
                pageFlowAttrs.get("ofdaDivisionCode") + " PROJECT_NBR=" + pageFlowAttrs.get("projectNbr");
            break;
        case "FUNDS_ACTIVITY":
            args = "escapeParams=true module=" + formName + ".fmx P_EXIT=Y";
            args +=
                " BUDGET_FY=" + pageFlowAttrs.get("budgetFy") + " OFDA_DIVISION_CODE=" +
                pageFlowAttrs.get("ofdaDivisionCode") + " PROJECT_NBR=" + pageFlowAttrs.get("projectNbr");
            break;
        case "CREATE_PROJECT":
            args = "escapeParams=true module=ACTIVITY.fmx P_EXIT=Y";
            break;
        default:
            args = "escapeParams=true module=WELCOME.fmx";

        }
        return args;
    }
}
