package gov.ofda.abacus.view.bean.activity;

import gov.ofda.abacus.view.base.AbacusConstants;
import gov.ofda.abacus.view.base.UIControl;
import gov.ofda.abacus.view.bean.AppBean;
import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;

import java.io.UnsupportedEncodingException;

import java.net.URLEncoder;

import java.sql.Timestamp;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.PopupCanceledEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.ApplicationModule;
import oracle.jbo.DMLConstraintException;
import oracle.jbo.Row;

import org.apache.myfaces.trinidad.event.ReturnEvent;

public class EditActivityBean extends UIControl implements Serializable {
    @SuppressWarnings("compatibility:8253880821549069592")
    
    private static final long serialVersionUID = 1L;
    
    private static final String BUREAU_CODE = "BureauCode";
    private static final String OFFICE_CODE ="OfficeCode";
    private static final String DIVISION_CODE = "OfdaDivisionCode";
    private static final String TEAM_CODE = "OfdaTeamCode";
    private static final String REGION_CODE = "RegionCode";
    private static final String COUNTRY_CODE ="CountryCode";
    
    private static final String ORIGINAL_BUREAU = "originalBureau";
    private static final String ORIGINAL_OFFICE = "originalOffice";
    private static final String ORIGINAL_DIVISION = "originalDivision";
    private static final String ORIGINAL_COUNTRY = "originalCountry";
    private static final String ORIGINAL_TEAM = "originalTeam";
    private static final String ORIGINAL_REGION = "originalRegion";
    private static final String JBO_25014_EXCEPTION = "JBO-25014"; 
    
    private String taskFlowId = "/WEB-INF/tflows/activity/edit-activity-tf.xml#edit-activity-tf";
    
    private static ADFLogger logger =
        ADFLogger.createADFLogger(gov.ofda.abacus.view.bean.activity.EditActivityBean.class);
    private RichPopup updateProjectLookupPopup;
    private RichInputComboboxListOfValues cableLocation;
    private MessageBean mb=new MessageBean();
    private RichPopup budgetSourceTypePopup;

    public EditActivityBean() {
        super();
    }

    public void setParams() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ActivityEditView1Iterator");
        Row rw = iter.getCurrentRow();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("disasterFlag", rw.getAttribute("DisasterFlag"));

    }

    public String cancelChanges() {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Rollback");
        exec.execute();
        if ((Boolean) pageFlowMap.get("isNew"))
            return "new";
        else {
            exec = bindings.getOperationBinding("ExecuteWithParams");
            exec.execute();
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null,
                      new FacesMessage(FacesMessage.SEVERITY_INFO,
                                       "Cancelled changes", null));
        return null;
    }

    /**
     * This method save the updated/modified Project fields into database. 
     * Before saving it does validation for required fields, if fails return to the corresponding page.
     * @return
     */
    public String saveChanges() {
        
        
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ActivityEditView1Iterator");
        String username = ADFContext.getCurrent().getSecurityContext().getUserName();
        FacesContext fc = FacesContext.getCurrentInstance();
        Row rw = iter.getCurrentRow();

        String resTime = (String) rw.getAttribute("respondedTime");
        Timestamp  respondedTime = (Timestamp) rw.getAttribute("RespondedTimestamp");
        
        if(null != respondedTime){
            rw.setAttribute("RespondedTimestamp", constructTimeStampWithDateAndTime(resTime, respondedTime));
        }
        
        Timestamp ddDate = (Timestamp) rw.getAttribute("DdDate"); 
        String ddTime = (String) rw.getAttribute("ddTime");

        if(ddDate != null){
            rw.setAttribute("DdDate", constructTimeStampWithDateAndTime(ddTime, ddDate));
        }
        
        if(rw.getAttribute("ProjectPriorityId")==null) {
            fc.addMessage(null,
                          new FacesMessage(FacesMessage.SEVERITY_WARN,
                                           "Missing Project Priority", null));
            return "missingFD";
        }

        // add validation check for required fields Disaster Flag & Is Dod Involved.
        if("D".equals(rw.getAttribute("DisasterFlag")) && (rw.getAttribute("IsDodInvolved")==null || rw.getAttribute("Dd")==null)){
            fc.addMessage(null,
                          new FacesMessage(FacesMessage.SEVERITY_WARN,
                                           "Missing Declaration Type Details or DOD Involved", null));
            return "missingDD";
        }
        // Add validation check for Project Date as Project Date is required.
        if(null == rw.getAttribute("ProjectDate")){
            fc.addMessage(null,
                          new FacesMessage(FacesMessage.SEVERITY_WARN,
                                           "Missing Project Date", null));
            return "missingDD";
        }
        
        
        /**
         * Validation check for First Responded Date & DD Date. 
         * First Responded Date always after the DD date.
         */
        if(null != ddDate && null != respondedTime){
            Timestamp ddTimeStamp = (Timestamp) rw.getAttribute("DdDate");
            Timestamp resTimeStamp = (Timestamp) rw.getAttribute("RespondedTimestamp");
            if(resTimeStamp.before(ddTimeStamp)){
                fc.addMessage(null,
                              new FacesMessage(FacesMessage.SEVERITY_WARN,
                                               "First Responded Date must be after the Declaration Date", null));
                return "missingDD";
            }
        }
        if(rw.getAttribute("BudgetSourceType")==null) {
            fc.addMessage(null,
                          new FacesMessage(FacesMessage.SEVERITY_WARN,
                                           "Missing Budget Source Type", null));
            return "missingFD";
        }
        if(rw.getAttribute("BudgetSource")==null) {
            fc.addMessage(null,
                          new FacesMessage(FacesMessage.SEVERITY_WARN,
                                           "Missing Budget Source Default", null));
            return "missingFD";
        }
        
        if(rw.getAttribute("IsFormalDeclaration")==null) {
            fc.addMessage(null,
                          new FacesMessage(FacesMessage.SEVERITY_WARN,
                                           "Missing Formal Declaration", null));
            return "missingFD";
        }
        if(rw.getAttribute("IsEarlyResponse")==null) {
            fc.addMessage(null,
                          new FacesMessage(FacesMessage.SEVERITY_WARN,
                                           "Missing Early Response", null));
            return "missingFD";
        }
        
        
        
        // check if the page is dirty then commit the changes 
        // else  don't commit it
        if(isDirtyPage()){
            if (username != null)
                rw.setAttribute("UpdId", username.toUpperCase());
            OperationBinding exec = bindings.getOperationBinding("Commit");
            exec.execute();
            if (exec.getErrors().isEmpty()) {
                if ((Boolean) pageFlowMap.get("isNew")) {
                    pageFlowMap.put("isNew", false);
                    exec = bindings.getOperationBinding("publishNewProjectCreatedEvent");
                    exec.execute();
                    exec = bindings.getOperationBinding("getActivityHTMLDetails");
                    String details = (String) exec.execute();
                    //getUserEmail
                    exec = bindings.getOperationBinding("getUserEmail");
                    String userEmail = (String) exec.execute();
                    exec = bindings.getOperationBinding("getEmailbyUsername");
                    String teamLeadEmail = (String) exec.execute();
                    pageFlowMap.put("teamLeadEmail", teamLeadEmail);
                    
                    MessageBean messageBean = new MessageBean();
                    messageBean.sendProjectCreatedEmail(details);
                }
                else 
                fc.addMessage(null,
                              new FacesMessage(FacesMessage.SEVERITY_INFO,
                                               "Saved changes", null));
            } else if ((Boolean) pageFlowMap.get("isNew")){
                Object tmp=exec.getErrors().get(0);
                if("DMLConstraintException".equals(tmp.getClass().getSimpleName()))
                {
                    fc.addMessage(null,
                                  new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                   ""+((DMLConstraintException)tmp).getMessage(), null));
                    logger.log(ADFLogger.ERROR,"Project creation failed: "+exec.getErrors());
                }

                pageFlowMap.put("isNew", false);
                exec = bindings.getOperationBinding("publishNewProjectCreatedEvent");
                exec.execute();
                cancelChanges();
                return "cancel";
                
            }
            else {
                
                boolean isRecordDirty = false;
                
                List errors = exec.getErrors();
                
                for (Object error : errors){
                    if(String.valueOf(error).contains(JBO_25014_EXCEPTION)){
                        isRecordDirty = true;
                        break;
                    }
                }
                
                if(isRecordDirty){
                    fc.addMessage(null,
                                  new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                   "Another user has changed the Data. Cancel your changes and try again.", null));
                }else {
                    fc.addMessage(null,
                                  new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                   "Failed to save changes", null));
                }
                logger.log(ADFLogger.ERROR, ""+exec.getErrors());
            }
        } else{
            fc.addMessage(null,
                          new FacesMessage(FacesMessage.SEVERITY_INFO,
                                           "No changes to save", null));
        }

        return null;
        
    }

    /**
     * Initialize the project fields ( division, team, project name etc ) from Lookup.
     */
    public void initializeFromLookup() {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("setNewProjectDetails");
        exec.getParamsMap().put("fy", map.get("budgetFY"));
        exec.getParamsMap().put("projectNbr", map.get("projectNbr"));
        exec.execute();
        
        DCIteratorBinding iter = bindings.findIteratorBinding("ActivityEditView1Iterator");
        Row row = iter.getCurrentRow();
        map.put(ORIGINAL_BUREAU, row.getAttribute("BureauCode"));
        map.put(ORIGINAL_OFFICE, row.getAttribute("OfficeCode"));
        String division = (String) row.getAttribute("OfdaDivisionCode");
        map.put(ORIGINAL_DIVISION, division);
        map.put(ORIGINAL_TEAM, row.getAttribute("OfdaTeamCode"));
        map.put(ORIGINAL_REGION, row.getAttribute("RegionCode"));
        map.put(ORIGINAL_COUNTRY,row.getAttribute("CountryCode"));
        
        /**
         *  populate USAID SITE LINK
         */
        if(null != row.getAttribute("CountryCode")){
            OperationBinding countryExec = bindings.getOperationBinding("getCountryName");
            countryExec.execute();
            String countryName = (String) countryExec.getResult();
            String encodedCountryName = null;
            try {
                 encodedCountryName = URLEncoder.encode(countryName, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
            Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
            String url = (String) appScope.get("USAID_SITE_LINK");
            // if country has space between the words, replace with - 
            if(null != encodedCountryName){
                encodedCountryName.replace(" ", "-");
            }
            row.setAttribute("UsaidSiteLink", url+encodedCountryName);
        }
    }

    public void BDOTChangeDialogListner(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().equals(DialogEvent.Outcome.yes)){
            Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = bindings.getOperationBinding("setProjectLookup");
            exec.getParamsMap().put("p_pnbr", map.get("projectNbr"));
            exec.execute();
            
            DCIteratorBinding activityIter = bindings.findIteratorBinding("ActivityEditView1Iterator");
            Row activityRow = activityIter.getCurrentRow();
            
            DCIteratorBinding iter = bindings.findIteratorBinding("EditProjectLookupViewIterator");
            
            Row row = iter.getCurrentRow();
            
            if(String.valueOf(map.get("changeField")).equalsIgnoreCase("bureau")){
                row.setAttribute(BUREAU_CODE, activityRow.getAttribute(BUREAU_CODE));
                map.put(ORIGINAL_BUREAU, activityRow.getAttribute(BUREAU_CODE));
            } else if(String.valueOf(map.get("changeField")).equalsIgnoreCase("office")){
                row.setAttribute(OFFICE_CODE, activityRow.getAttribute(OFFICE_CODE));            
                map.put(ORIGINAL_OFFICE, activityRow.getAttribute(OFFICE_CODE));
            } else if (String.valueOf(map.get("changeField")).equalsIgnoreCase("division")){
                row.setAttribute(DIVISION_CODE, activityRow.getAttribute(DIVISION_CODE));            
                map.put(ORIGINAL_DIVISION, activityRow.getAttribute(DIVISION_CODE));
            } else if(String.valueOf(map.get("changeField")).equalsIgnoreCase("team")){
                row.setAttribute(TEAM_CODE, activityRow.getAttribute(TEAM_CODE));            
                map.put(ORIGINAL_TEAM, activityRow.getAttribute(TEAM_CODE));
            } else if(String.valueOf(map.get("changeField")).equalsIgnoreCase("region")){
                row.setAttribute(REGION_CODE, activityRow.getAttribute(REGION_CODE));
                map.put(ORIGINAL_REGION, activityRow.getAttribute(REGION_CODE));
            } else if(String.valueOf(map.get("changeField")).equalsIgnoreCase("country")){
                row.setAttribute(COUNTRY_CODE, activityRow.getAttribute(COUNTRY_CODE));            
                map.put(ORIGINAL_COUNTRY, activityRow.getAttribute(COUNTRY_CODE));
            }

            OperationBinding execCommit = bindings.getOperationBinding("LovEditModuleCommit");
            execCommit.execute();

        }
    }

    public void setUpdateProjectLookupPopup(RichPopup updateProjectLookupPopup) {
        this.updateProjectLookupPopup = updateProjectLookupPopup;
    }

    public RichPopup getUpdateProjectLookupPopup() {
        return updateProjectLookupPopup;
    }
    
    public void bureauValueChangeListner(ValueChangeEvent valueChangeEvent) {
        if(null != valueChangeEvent.getNewValue()){
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            String orgBureau = (String) map.get(ORIGINAL_BUREAU);
            if(null == orgBureau || !orgBureau.equalsIgnoreCase(String.valueOf(valueChangeEvent.getNewValue()))){
            map.put("changeField", "bureau");
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            this.getUpdateProjectLookupPopup().show(hints);
        }
    }
    }
    
    public void officeValueChangeListner(ValueChangeEvent valueChangeEvent) {
        if(null != valueChangeEvent.getNewValue()){
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            String orgoffice = (String) map.get(ORIGINAL_COUNTRY);
            if(null == orgoffice || !orgoffice.equalsIgnoreCase(String.valueOf(valueChangeEvent.getNewValue()))){
            map.put("changeField", "office");
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            this.getUpdateProjectLookupPopup().show(hints);
        }
    }
    }

    public void divisionValueChangeListner(ValueChangeEvent valueChangeEvent) {
        if(null != valueChangeEvent.getNewValue()){
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            String orgDiv = (String) map.get(ORIGINAL_DIVISION);
            if(null == orgDiv ||  !orgDiv.equalsIgnoreCase(String.valueOf(valueChangeEvent.getNewValue()))){
            map.put("changeField", "division");
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            this.getUpdateProjectLookupPopup().show(hints);
            }     
    }
 }


    public void teamValueChangeListner(ValueChangeEvent valueChangeEvent) {
        if(null != valueChangeEvent.getNewValue()){
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            String orgTeam = (String) map.get(ORIGINAL_TEAM);
            if(null == orgTeam || !orgTeam.equalsIgnoreCase(String.valueOf(valueChangeEvent.getNewValue()))){
            map.put("changeField", "team");
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            this.getUpdateProjectLookupPopup().show(hints);
        }
    }
    }

    public void regionValueChangeListner(ValueChangeEvent valueChangeEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding activityIter = bindings.findIteratorBinding("ActivityEditView1Iterator");
//        Row row = activityIter.getCurrentRow();
//        row.setAttribute(COUNTRY_CODE, "");
        if(null != valueChangeEvent){
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            String orgRegion = (String) map.get(ORIGINAL_REGION);
            if(null == orgRegion || !orgRegion.equalsIgnoreCase(String.valueOf(valueChangeEvent.getNewValue()))){
                map.put(ORIGINAL_COUNTRY, null);
            map.put("changeField", "region");
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            this.getUpdateProjectLookupPopup().show(hints);
        }
    }
    }

    public void countryValueChangeListner(ValueChangeEvent valueChangeEvent) {
        if(null != valueChangeEvent.getNewValue()){
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            String orgCountry = (String) map.get(ORIGINAL_COUNTRY);
            if(null == orgCountry || !orgCountry.equalsIgnoreCase(String.valueOf(valueChangeEvent.getNewValue()))){
            map.put("changeField", "country");
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            this.getUpdateProjectLookupPopup().show(hints);
        }
    }
    }
    
    private String getConstructedTime(String orgTime) {
        String orgTimeParts[] = orgTime.split(" ");
        String hrs = orgTimeParts[0];
        if("AM".equalsIgnoreCase(orgTimeParts[1])){
            return hrs;
        }else {
            String hrParts[] = hrs.split(":");
            int intHrs = Integer.parseInt(hrParts[0]);
            if(intHrs < 12){
                intHrs = intHrs + 12;                
            }
            StringBuilder constructTime = new StringBuilder();
            constructTime.append(String.valueOf(intHrs));
            constructTime.append(":");
            constructTime.append(hrParts[1]);
            return constructTime.toString();
        }
    }

    public void cableLocationValueChangeListner(ValueChangeEvent valueChangeEvent) {
        if( valueChangeEvent.getNewValue()!= null){
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("ActivityEditView1Iterator");
            Row rw = iter.getCurrentRow();
            rw.setAttribute("cableNumber", null);       
            rw.setAttribute("DisasterCableInfoSeqNo", null);    
        }
           
    }

    public void cableNumberValueChangeListner(ValueChangeEvent valueChangeEvent) {
        if(valueChangeEvent.getNewValue() != null){
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("ActivityEditView1Iterator");
            Row row = iter.getCurrentRow();
            if(null == row.getAttribute("cableLocation")){
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.cableLocation);
            }
        }
        
    }

    public void setCableLocation(RichInputComboboxListOfValues cableLocation) {
        this.cableLocation = cableLocation;
    }

    public RichInputComboboxListOfValues getCableLocation() {
        return cableLocation;
    }

    /**
     * This method does the validation check against the DD date, 
     * As First Responded Date always after the DD Date.
     * @param valueChangeEvent
     */
    public void firstRespondedDateValueChangeListner(ValueChangeEvent valueChangeEvent) {
        Timestamp  frDate = (Timestamp) valueChangeEvent.getNewValue();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ActivityEditView1Iterator");
        Row row = iter.getCurrentRow();
        Timestamp ddDate = (Timestamp) row.getAttribute("DdDate");
        String ddTime = (String) row.getAttribute("ddTime");
        String frTime = (String) row.getAttribute("respondedTime");
        
        if(null == frTime || "00:00 AM".equalsIgnoreCase(frTime)){
        if(null != frDate && null != ddDate){
                if(ddDate.after(frDate)){
                    FacesContext.getCurrentInstance().addMessage(valueChangeEvent.getComponent().getClientId(), new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "First Responded Date must be after the Declaration Date"));
                }
            }
        }else if(null != frDate && null != ddDate){
            Timestamp ddTimestamp = constructTimeStampWithDateAndTime(ddTime, ddDate);
            Timestamp frTimestamp = constructTimeStampWithDateAndTime(frTime, frDate);
            
            if(ddTimestamp.after(frTimestamp)){
                
                FacesContext.getCurrentInstance().addMessage(valueChangeEvent.getComponent().getClientId(), new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "First Responded Date must be after the Declaration Date"));
            }
        }
    }

    private Timestamp constructTimeStampWithDateAndTime(String resTime, Timestamp respondedTime) {
        Timestamp constructedTimestamp = respondedTime;
        String strTime = respondedTime.toString();
        String[] parts = strTime.split(" ");
        if(parts.length > 1 && null != resTime ){
            StringBuilder timeStrBldr = new StringBuilder();
            timeStrBldr.append(parts[0]);
            timeStrBldr.append(" ");
            String constructedTime = getConstructedTime(resTime);
            timeStrBldr.append(constructedTime);
            timeStrBldr.append(":00");
            constructedTimestamp = Timestamp.valueOf(timeStrBldr.toString());
        }
        return constructedTimestamp;
    }

    public void ddTypeValueChangeListner(ValueChangeEvent valueChangeEvent) {
        Boolean isNew = (Boolean) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("isNew");
        if(isNew){
            String value = (String) valueChangeEvent.getNewValue();
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("ActivityEditView1Iterator");
            Row currentRow = iter.getCurrentRow();
            if(null != value && (value.equalsIgnoreCase("D") || value.equalsIgnoreCase("N"))){
                
                currentRow.setAttribute("BudgetSource", "R");
            }else{
                currentRow.setAttribute("BudgetSource", "C");
            }
        }
    }

    public void validateEmailAddress(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(null != object &&object.toString().length()>0){
            String email[] = object.toString().split(";");
            if(email.length >0){
                for(String mail : email){
                    String regex = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{3,4}";
                    Pattern pattern = Pattern.compile(regex);
                    Matcher matcher = pattern.matcher(mail);
                    if(matcher.matches()){
                        
                    }else {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,null,"Invalid Email address : " + object.toString()));
                    }
                }
            }
        }

    }

    public void addBudgetReturnLsnr(ReturnEvent returnEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfp=AdfFacesContext.getCurrentInstance().getPageFlowScope();
        FacesContext context = FacesContext.getCurrentInstance();
        String result = (String) pfp.get("confirmMessage");
        if("true".equals(result))
        {
         OperationBinding exec = bindings.getOperationBinding("Execute");
         exec.execute();
         AdfFacesContext.getCurrentInstance().addPartialTarget(returnEvent.getComponent());
        }
        mb.sendActivityBudgetTransferEmail(result, (Map) pfp.get("transferDetails"),(Boolean) pfp.get("isSendEmail"));

    }
    public void addFundsReturnLsnr(ReturnEvent returnEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfp=AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) pfp.get("confirmMessage");
        if("true".equals(result))
        {
         OperationBinding exec = bindings.getOperationBinding("Execute");
         exec.execute();
         AdfFacesContext.getCurrentInstance().addPartialTarget(returnEvent.getComponent());
        }
        mb.sendActivityFundTransferEmail(result, (Map) pfp.get("transferDetails"),(Boolean) pfp.get("isSendEmail"));;
    }
    
    private boolean isDirtyPage(){
        return this.getApplicationModule("#{data.PAabacusappModuleDataControl.dataProvider}").getTransaction().isDirty();
    }
    
    private ApplicationModule getApplicationModule(String dataProvider) {
        FacesContext fc = FacesContext.getCurrentInstance();
        Application app = fc.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = fc.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, dataProvider, Object.class);
        return (ApplicationModule) valueExp.getValue(elContext);
    }
    
    public void budgetRecoveryReturnListner(ReturnEvent returnEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) map.get("confirmMessage");
        MessageBean bean = new MessageBean();
        bean.sendBudgetRecoveryEmail(result, (Map)map.get("transferDetails"), (Boolean)map.get("isSendEmail"));
    }
    
    
    /**
     * this method continue will send the status to customer about the funds recovery they did.
     * @param returnEvent
     */
    public void recoverFundsListner(ReturnEvent returnEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) map.get(AbacusConstants.CONFIRM_MESSAGE);
        MessageBean bean = new MessageBean();
            bean.sendFundRecoveryEmail(result, (Map)map.get(AbacusConstants.TRANSFER_DETAILS), (Boolean)map.get(AbacusConstants.SEND_EMAIL));
    }

    public void setIsNew() {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pageFlowMap.put("isNew", false);
    }

    public void budgetSourceTypeValueChangeListener(ValueChangeEvent valueChangeEvent) {
        String oldValue = null;
        String newValue = null;
        if(null != valueChangeEvent.getOldValue()){
            oldValue = (String) valueChangeEvent.getOldValue();
        }
        
        if(null != valueChangeEvent.getNewValue()){
            newValue = (String) valueChangeEvent.getNewValue();
        }
        
        if((null != oldValue && oldValue.equalsIgnoreCase("M")) && (null != newValue && newValue.equalsIgnoreCase("S"))){
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put("valueChangeType", "BudgetSourceType");
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            this.getBudgetSourceTypePopup().show(hints);         
        }
    }

    public void budgetSourceTypeDialogListener(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().equals(DialogEvent.Outcome.no)){
            Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            String val = (String) pageFlowMap.get("valueChangeType");
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("ActivityEditView1Iterator");
            if(null != val && val.equalsIgnoreCase("BudgetSourceType")){
                Row currentRow = iter.getCurrentRow();
                currentRow.setAttribute("BudgetSourceType", "M");
            }
            
            if(null != val && val.equalsIgnoreCase("BudgetSourceDefault")){
                Row currentRow = iter.getCurrentRow();
                currentRow.setAttribute("BudgetSource", pageFlowMap.get("budgetSourceDefaultOldValue"));
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(dialogEvent.getComponent().getParent().getParent());
        }
    }

    public void budgetSourcePopupCancelListener(PopupCanceledEvent popupCanceledEvent) {
        // change the 
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String val = (String) pageFlowMap.get("valueChangeType");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ActivityEditView1Iterator");
        if(null != val && val.equalsIgnoreCase("BudgetSourceType")){
            Row currentRow = iter.getCurrentRow();
            currentRow.setAttribute("BudgetSourceType", "M");
        }
        
        if(null != val && val.equalsIgnoreCase("BudgetSourceDefault")){
            Row currentRow = iter.getCurrentRow();
            currentRow.setAttribute("BudgetSource", pageFlowMap.get("budgetSourceDefaultOldValue"));
        }
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(popupCanceledEvent.getComponent().getParent().getParent());
    }

    public void setBudgetSourceTypePopup(RichPopup budgetSourceTypePopup) {
        this.budgetSourceTypePopup = budgetSourceTypePopup;
    }

    public RichPopup getBudgetSourceTypePopup() {
        return budgetSourceTypePopup;
    }

    public void budgetSourceDefaultValueChangeListener(ValueChangeEvent valueChangeEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ActivityEditView1Iterator");
        Row currentRow = iter.getCurrentRow();
        String budgetSourceType = (String) currentRow.getAttribute("BudgetSourceType");
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(null != valueChangeEvent.getOldValue()){
            pageFlowMap.put("budgetSourceDefaultOldValue" , valueChangeEvent.getOldValue());
        }
        if(null != budgetSourceType && budgetSourceType.equalsIgnoreCase("S")){
            if(null != valueChangeEvent.getNewValue()){
                pageFlowMap.put("valueChangeType", "BudgetSourceDefault");
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                this.getBudgetSourceTypePopup().show(hints);    
            }
        }
    }

    public void editDDSummaryPopupActionListener(ActionEvent actionEvent) {
        RichPopup popUp = (RichPopup) actionEvent.getComponent().getParent().getParent().getParent().getParent();
        popUp.hide();
    }
}
