package gov.ofda.abacus.view.base;

import gov.ofda.abacus.view.bean.AppBean;

import java.io.File;

import java.io.Serializable;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.share.ADFContext;

import oracle.adf.share.logging.ADFLogger;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;


public class AbacusEmail implements Serializable{
    @SuppressWarnings("compatibility:-1398785794013404830")
    private static final long serialVersionUID = 1L;
    private String hostName;
    private int port = 25;
    private Boolean sslOn = false;
    private String fromAdd;
    private String subjectHeader;
    private String abacusAdminEmail;
    private Map appScope;
    private transient FacesContext ctx;
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.view.base.AbacusEmail.class);
    public AbacusEmail() {
        super();
        appScope = ((AppBean)ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
        ctx = FacesContext.getCurrentInstance();
        hostName = (String) appScope.get("EMAIL_SERVER");
        fromAdd = (String) appScope.get("FROM_EMAIL");
        subjectHeader=(String)appScope.get("APP_NAME");
        abacusAdminEmail=(String)appScope.get("ABACUS_ADMIN_EMAIL");
    }

    public boolean sendHtmlEmail(String toAdd, String ccAdd, String subject, String htmlMsg, String altMsg,File[] attachments) {
        // Create the email message
        if(toAdd==null || subject==null || htmlMsg==null) {
            logger.log(logger.ERROR, "Missing Required Values To:"+toAdd+" Subject: "+subject+" Msg:"+htmlMsg);
            return false;
        }
        HtmlEmail email = new HtmlEmail();
        email.setHostName(hostName);
        URL url=null;
        try {
            ServletContext servletContext = (ServletContext) ctx.getExternalContext().getContext();
            url = servletContext.getResource("/images/USAID_logo.png");
        } catch (MalformedURLException e) {
            url=null;
        }

        String instanceName=(String)appScope.get("INSTANCE");
        if(!"OFDAP".equals(instanceName)) {
            htmlMsg+="<br><br><b>Email intended for</b><br><b>To: </b>"+toAdd;
            if(ccAdd!=null)
                htmlMsg+="<br><b>Cc: </b>"+ccAdd;
            toAdd=abacusAdminEmail;
            ccAdd=ADFContext.getCurrent().getSecurityContext().getUserName()+"@ofda.gov";
        }
        try {
            String invalidAddressList="";
            //To Address
            AbacusAddress to=new AbacusAddress(toAdd);
            if(to.getValidList().size()==0)
                email.addTo(abacusAdminEmail);
            else
               email.setTo(to.getValidList());
            invalidAddressList+=to.getInvalidList();
            email.setFrom(fromAdd);
            
            if(ccAdd!=null) {
                AbacusAddress cc=new AbacusAddress(ccAdd);
                if(cc.getValidList().size()!=0)
                   email.setCc(cc.getValidList());
                invalidAddressList+=cc.getInvalidList();
            }
            email.setSubject(subjectHeader+": "+subject);
            if(url!=null) {
                String cid=email.embed(url, "USAID");
                email.setHtmlMsg("<html><table><tr><td style=\"text-align:right\"><img src=\"cid:"+cid+"\"><td><td style=\"vertical-align:top;text-align:left\"><span style=\"font-family: Tahoma, Verdana, Helvetica, sans-serif;font-weight: bold;font-size: 22px;color:black;\">Abacus</span></td></tr></table><br>"
                                 +htmlMsg+(invalidAddressList.length()!=0?"<br><br> Invalid Email List:"+invalidAddressList:"")+"</html>");
            }
            else
                email.setHtmlMsg("<html>"+htmlMsg+"</html>");
            if(altMsg!=null)
                email.setTextMsg(altMsg);
            else
                email.setTextMsg("Your email client does not support HTML messages");
            if(attachments!=null)
                for(int i=0;i<attachments.length;i++) {
                    email.attach(attachments[i]);
                }
            email.send();
            logger.log(logger.TRACE, "Email Sent to:"+toAdd+" cc: "+ccAdd+" Sub:"+subject);
            BindingContext bindingContext = BindingContext.getCurrent();
            if(bindingContext!=null)
            {
            DCBindingContainer dcBindingContainer =bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
            OperationBinding exec = dcBindingContainer.getOperationBinding("logEmail");
            Map t=exec.getParamsMap();
            t.put("toAdd", toAdd);
            t.put("ccAdd", ccAdd);
            t.put("subject", subject);
            t.put("content", htmlMsg);
            exec.execute();
            }
            return true;
        } catch (EmailException e) {
            logger.log(logger.ERROR, "Email failed: "+e.getMessage() );
            return false;
        }
    }
    private class AbacusAddress{
        private List<InternetAddress> validList;
        private String invalidList;
        public void setValidList(List<InternetAddress> validList) {
            this.validList = validList;
        }

        public List<InternetAddress> getValidList() {
            return validList;
        }

        public void setInvalidList(String invalidList) {
            this.invalidList = invalidList;
        }

        public String getInvalidList() {
            return invalidList;
        }

        public AbacusAddress(String list) {
            validList=new ArrayList<InternetAddress>();
            StringBuilder invalidTmp=new StringBuilder();
            list=list.replace(",", ";");
            list=list.replace(" ", ";");
            list=list.replace(":", ";");
            String tmp[]=list.split(";");
            for(int i=0;i<tmp.length;i++) {
                try {
                    InternetAddress ia = new InternetAddress(tmp[i]);
                    ia.validate();
                    validList.add(ia);
                } catch (AddressException e) {
                  invalidTmp.append(" "+tmp[i]);
                }
            }
            invalidList=invalidTmp.toString();
        }
    }
}
