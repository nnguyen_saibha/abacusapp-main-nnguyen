package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.view.base.UIControl;

import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;

import javax.faces.component.UIComponent;

import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputFormatted;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.adf.view.rich.event.QueryEvent;

import oracle.adf.view.rich.event.QueryOperationEvent;

import oracle.adf.view.rich.model.AttributeCriterion;
import oracle.adf.view.rich.model.ConjunctionCriterion;
import oracle.adf.view.rich.model.Criterion;
import oracle.adf.view.rich.model.QueryDescriptor;

import oracle.binding.OperationBinding;

public class ActionNavigator extends UIControl implements Serializable{
    @SuppressWarnings("compatibility:2941399241843107537")
    private static final long serialVersionUID = 1L;
    private String showColumns = "Details";
    private String reportFormat="Pdf";
    private Map<String,String> fundingTotals=null;

    public void setFundingTotals(Map<String, String> fundingTotals) {
        this.fundingTotals = fundingTotals;
    }

    public Map<String, String> getFundingTotals() {
        if(fundingTotals==null) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = bindings.getOperationBinding("getTotalAmounts");
            exec.execute();
            fundingTotals=((Map<String,String>)exec.getResult());
        }
        return fundingTotals;
    }
    public ActionNavigator() {
        super();
        SecurityContext sc=ADFContext.getCurrent().getSecurityContext();
        if(sc.isUserInRole("APP_FINANCE"))
             showColumns="Details";
        else if(sc.isUserInRole("APP_GRANT_UNIT"))
                showColumns="AwardTracking";
    }
    public void setShowColumns(String showColumns) {
        this.showColumns = showColumns;
    }

    public String getShowColumns() {
        return showColumns;
    }

    public void setReportFormat(String reportFormat) {
        this.reportFormat = reportFormat;
    }

    public String getReportFormat() {
        return reportFormat;
    }
    public void showColumnsActionListener(ActionEvent ae) {
        UIComponent uic = ae.getComponent();
        showColumns = (String) (uic.getAttributes()).get("showColumn");
        AdfFacesContext.getCurrentInstance().addPartialTarget(uic.getParent());
    }

    public void totalsPopupFetchLsnr(PopupFetchEvent popupFetchEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("getTotalAmounts");
        exec.execute();
        this.setFundingTotals((Map<String,String>)exec.getResult());
    }

    public void actionsQueryLsnr(QueryEvent qe) {
        QueryDescriptor qd=qe.getDescriptor();
        Boolean callSet=false;
        String setID=null;
        /**Reference-Frank Nimphius Example- ADF Code Corner
        * http://www.oracle.com/technetwork/developer-tools/adf/learnmore/85-querycomponent-fieldvalidation-427197.pdf
        * */
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        ConjunctionCriterion conCrit = qd.getConjunctionCriterion();
        List<Criterion> cList=conCrit.getCriterionList();
        for(Criterion c:cList) {
            AttributeCriterion ac=(AttributeCriterion) c;
            if("Set ID".equals(ac.getAttribute().getLabel()) &&ac.getValues().get(0)!=null)
            {
              setID=(String)ac.getValues().get(0);
              if(setID!=null && setID.length()>0)
                  callSet=true;
              break;
            }
        }
        if(!callSet)
        {
        invokeEL("#{bindings.AllActionsQuery.processQuery}", new Class[] { QueryEvent.class }, new Object[] {
                 qe });
        }
        else {

            OperationBinding exec = bindings.getOperationBinding("setWhereClauseByID");
            exec.getParamsMap().put("setID", setID);
            exec.execute();
        }
        
        OperationBinding exec = bindings.getOperationBinding("getTotalAmounts");
        exec.execute();
        this.setFundingTotals((Map<String,String>)exec.getResult());
    }

    public void actionQueryOperationLsnr(QueryOperationEvent queryOperationEvent) {
        invokeEL("#{bindings.AllActionsQuery.processQueryOperation}", new Class[] { QueryOperationEvent.class }, new Object[] {
                 queryOperationEvent });
        if(queryOperationEvent.getOperation().equals(QueryOperationEvent.Operation.RESET))
        {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = bindings.getOperationBinding("getTotalAmounts");
            exec.execute();
            this.setFundingTotals((Map<String,String>)exec.getResult());
        }
    }

    public void deleteActionDialogLsnr(DialogEvent de) {
            if (DialogEvent.Outcome.yes == de.getOutcome()) {
                DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding exec = bindings.getOperationBinding("deleteAction");
                exec.execute();
                FacesContext fc = FacesContext.getCurrentInstance();
                String errorMsg = (String) exec.getResult();
                if(errorMsg==null)
                    fc.addMessage(null,
                                  new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                   "Action deleted successfully.", null));
                else
                {
                  if(errorMsg.contains("Unexpected Error")) {
                      fc.addMessage(null,
                                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                     "Unexpected Error. Please contact Abacus Team (abacus@ofda.gov) with action information.", null));
                      MessageBean mb=new MessageBean();
                      mb.sendAdminEmail("Error Delete Action", errorMsg);
                  }
                  else
                    fc.addMessage(null,
                                  new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                   errorMsg, null));
                }
            }
    }
   public String getDeleteActionValid() {
       DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
       OperationBinding exec = bindings.getOperationBinding("deleteActionCheck");
       exec.execute();
       String errorMsg = (String) exec.getResult();
       Map viewScope=AdfFacesContext.getCurrentInstance().getViewScope();
       viewScope.put("deleteActionErrorMsg", errorMsg);
       return errorMsg;
   }
}
