package gov.ofda.abacus.view.bean.message;

import gov.ofda.abacus.view.base.AbacusConstants;
import gov.ofda.abacus.view.base.AbacusEmail;
import gov.ofda.abacus.view.base.UIControl;
import gov.ofda.abacus.view.base.UIControl.GrowlType;
import gov.ofda.abacus.view.bean.AppBean;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import java.net.MalformedURLException;
import java.net.URL;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.Email;

public class MessageBean extends UIControl implements Serializable {
    @SuppressWarnings("compatibility:7758371343793603996")
    private static final long serialVersionUID = 1L;
    Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
    AbacusEmail email = new AbacusEmail();
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.view.bean.message.MessageBean.class);


    public MessageBean() {
        super();
    }

    /* private PasswordCredential getReportCredentials() {
        ServiceLocator locator = JpsServiceLocator.getServiceLocator();
        CredentialStore store = null;
        PasswordCredential pcred = null;
        try {
            store = locator.lookup(CredentialStore.class);
            pcred = (PasswordCredential) store.getCredential("OracleReports", "ReportsOIDCred");

        } catch (Exception e) {
            logger.log(ADFLogger.ERROR, e.getMessage());
            sendAdminEmail("Error while getting Report Credentials", e.getMessage());
        }
        return pcred;
    }*/

    public void sendActivityFundTransferEmail(String result, Map td, Boolean isSendEmail) {
        FacesContext context = FacesContext.getCurrentInstance();
        if ("true".equals(result)) {
            /**
             * Sendemail and add message if email was sent properly.
             * If from response fund sub := 'Abacus: Fund Allocation from Response Fund';
             * Else sub := 'Abacus: Fund Allocation to Project';
             * to_email := user_email ; abacus team email ; finance_email;
             *
             **/
            if (isSendEmail != null && isSendEmail) {
                //  Integer budgetFY, String divisionCode,String projectNbr,String fundCode,String from
                Map<String, String> emailMap = getActivityFundTransferEmailList((String)td.get("budgetFY"),(String) td.get("projectNbr"));
                String username = ADFContext.getCurrent().getSecurityContext().getUserName();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                String fdate = sdf.format(new Date());

                StringBuilder builder = new StringBuilder();
                builder.append("<table border=\"1\" style=\"border-collapse:collapse; width: 500px;\" cellpadding=\"5\">" +
                               "<tbody>");
                //if("OFDA".equals(transferLevel)){
                builder.append("<tr>" + "<td colspan=\"2\" style=\"text-align:left\"> <b> From</b> </td>" + "</tr>");

                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Fiscal Year</td>" +
                               "<td style=\"text-align:left\">" + td.get("budgetFY") + "</td>" + "</tr>");

                if("Bureau".equals(td.get("from")))
                {
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">From</td>" +
                               "<td style=\"text-align:left\">" +"Bureau " + td.get("bureauCode")+
                               "</td>" + "</tr>");
                }
                if("Office".equals(td.get("from")))
                {
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">From</td>" +
                               "<td style=\"text-align:left\">" +"Office "+ td.get("officeCode") +
                               "</td>" + "</tr>");
                }
                if("Division".equals(td.get("from")))
                {
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">From</td>" +
                               "<td style=\"text-align:left\">" +"Division " + td.get("divisionCode") +
                               "</td>" + "</tr>");
                }
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Fund Code</td>" +
                               "<td style=\"text-align:left\">" + td.get("fundCode") + "</td>" + "</tr>");
                builder.append("<tr>" +
                    // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" +
                    "<td style=\"text-align:right; width:30%;\">" + ("Available Fund Amt") + "</td>" +
                    "<td style=\"text-align:left\">" + td.get("fundsAvailable") + "</td>" + "</tr>");

                builder.append("<tr>" + "<td colspan=\"2\" style=\"text-align:left\"> <b>To</b> </td>" + "</tr>");
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Fiscal Year</td>" +
                               "<td style=\"text-align:left\">" + td.get("budgetFY") + "</td>" + "</tr>");
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Bureau</td>" +
                               "<td style=\"text-align:left\">" + td.get("bureauCode") + "</td>" + "</tr>");
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Office</td>" +
                               "<td style=\"text-align:left\">" + td.get("officeCode") + "</td>" + "</tr>");
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Division</td>" +
                               "<td style=\"text-align:left\">" + td.get("divisionCode") + "</td>" + "</tr>");
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Project</td>" +
                               "<td style=\"text-align:left\">" + td.get("projectNbr") + ", " + td.get("projectName") +
                               "</td>" + "</tr>");
                    builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Fund Code</td>" +
                                   "<td style=\"text-align:left\">" + td.get("fundCode") + "</td>" + "</tr>");



                builder.append("<tr>" +
                    // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" +
                    "<td style=\"text-align:right; width:30%;\">" + ("Fund Amt") + "</td>" +
                    "<td style=\"text-align:left\">" + td.get("transferAmt") + "</td>" + "</tr>");
                builder.append("<tr>" +
                    // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" +
                    "<td style=\"text-align:right; width:30%;\">" + ("Available Fund Amt") + "</td>" +
                    "<td style=\"text-align:left\">" + td.get("projectFundsAvailable") + "</td>" + "</tr>");
                builder.append("<tr>" +
                               "<td colspan=\"2\" style=\"text-align:left; word-wrap: break-word;\"> <b>Comments: </b> " +
                               td.get("comments") + "</td>" + "</tr>");

                builder.append("<tr>" +
                               "<td colspan=\"2\" style=\"text-align:left; word-wrap: break-word;\"> <b>Updated By </b>" +
                               username + " <b> On </b>" + fdate + "</td>" + "</tr>" + "</tbody>" + "</table>");


                String msg = builder.toString();


                //         String msg="<b>Transfer Details</b><br>" +
                //             "<table border=1 style=\"border-collapse:collapse\" cellpadding=\"5\">"+
                //             "<tr><td rowspan=3 style=\"text-align:left; vertical-align:top\"> <b> From </b> </td> "+
                //             "<td style=\"text-align:right\"><b>Year</b></td><td style=\"text-align:left\">"+td.get("budgetFY")+"</td></tr>" +
                //             "<tr><td style=\"text-align:right\"><b>Source</b></td><td style=\"text-align:left\">"+("Division".equals(td.get("from"))? "Division " + td.get("divisionCode") : "Response Fund")+"</td></tr>" +
                //             "<tr><td style=\"text-align:right\"><b>Fund Code</b></td><td style=\"text-align:left\">"+td.get("fundCode")+"</td></tr>" +
                //
                //             "<tr><td rowspan=3 style=\"text-align:left; vertical-align:top\"> <b> To </b> </td> "+
                //             "<td style=\"text-align:right\"><b>Project</b></td><td style=\"text-align:left;width:180px\">"+td.get("projectNbr")+", "+td.get("projectName")+"</td></tr>" +
                //
                //             "<tr><td  style=\"text-align:right\"><b>Fund Amt</b></td><td style=\"text-align:right\">"+td.get("transferAmt")+"</td></tr>" +
                //             "<tr><td  style=\"text-align:right;width:115px\"><b>Available Fund Amt</b></td><td style=\"text-align:right\">"+td.get("projectAvailFunds")+"</td></tr>" +
                //             "<tr><td colspan=3 style=\"text-align:left\"><b>Comments</b>" + " " +td.get("comments")+"</td></tr>" +
                //             "<tr><td colspan=3 style=\"text-align:left\"><b>Updated By</b> "+ " " +username+" <b>On</b>" + " " +fdate+"</td></tr></table>";

                Boolean isMailSent =
                    email.sendHtmlEmail(emailMap.get("toAdd"), emailMap.get("ccAdd"), "Project Add Funds", msg, null,
                                        null);
                String emailList = emailMap.get("emailList");
                if (isMailSent)
                    /*                     context.addMessage(null,
                                       new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                        "Funds added successfully. Email sent to " + emailList, null)); */
                {
                    this.addToGrowl(GrowlType.notice, "Funds added successfully", 0, 5000);
                    this.addToGrowl(GrowlType.mailok, "Email sent to " + emailList, 50, 10000);
                    }
                else
                    /*                     context.addMessage(null,
                                       new FacesMessage(FacesMessage.SEVERITY_WARN,
                                                        "Funds added successfully. Email to " + emailList +
                                                        " has failed", null)); */
                    {
                        this.addToGrowl(GrowlType.notice, "Funds added successfully", 0, 5000);
                        this.addToGrowl(GrowlType.mailerror, "Email sent to " + emailList+" has failed!", 50, 10000);
                    }
            } else
                /*                 context.addMessage(null,
                                   new FacesMessage(FacesMessage.SEVERITY_INFO, "Funds added successfully", null)); */
            this.addToGrowl(GrowlType.notice, "Funds added successfully", 0, 5000);
        } else if ("false".equals(result)) {
            /*             context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                "Transfer validation failed. Please try again", null)); */
            this.addToGrowl(GrowlType.error, "Transfer validation failed. Please try again", 0, 5000);
        } else if (result != null)
            /* context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, result, null)); */
            this.addToGrowl(GrowlType.error, result, 0, 10000);
    }

    public void sendActivityBudgetTransferEmail(String result, Map transferDetails, Boolean isSendEmail) {
        /**
         * Sendemail and add message if email was sent properly.
         * If from response fund
         * sub := 'Abacus: Budget Transfer from Response Fund';
         * Else  'Abacus: Budget Transfer�
         * email := user_email ; abacus team email (abacus-teamcode@ofda.gov) ;
         * cc_email := finance_email; smt_email (get from parameters).
         * Content: Check latest email and use Html tags to order properly
         **/
        FacesContext context = FacesContext.getCurrentInstance();
        if ("true".equals(result)) {
            if (null != isSendEmail && isSendEmail) {
                Map<String, String> emailMap = getActivityBudgetTransferEmailList(""+transferDetails.get("budgetFY"),(String) transferDetails.get("projectNbr"));
                String username = ADFContext.getCurrent().getSecurityContext().getUserName();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                String fdate = sdf.format(new Date());
                String from = (String) transferDetails.get("from");
                String sub = "";
                StringBuilder builder = new StringBuilder();
                builder.append("<table border=\"1\" style=\"border-collapse:collapse\" cellpadding=\"5\">" + "<tbody>" +
                               "<tr> " +
                               "<td rowspan=\"3\" style=\"text-align:left; vertical-align:top\"> <b> From </b> </td> " +
                               "<td style=\"text-align:right\"> <b>Year</b> </td> " + "<td style=\"text-align:left\">" +
                               transferDetails.get("budgetFY") + "</td> " + "</tr>");
                builder.append("<tr>");
                switch(from) {
                case "Bureau":
                    builder.append("<td style=\"text-align:right\"><b>Source</b> </td> " +
                                   "<td style=\"text-align:left\"> Response Fund Budget </td>");
                    sub = transferDetails.get("projName") + " got new budget from Response Fund";
                    break;
                case "Office":
                    builder.append("<td style=\"text-align:right\"><b>Source</b> </td> " +
                                   "<td style=\"text-align:left\"> "+from +" "+ transferDetails.get("officeCode")+" Budget </td>");
                    sub =transferDetails.get("projName") + " got new budget from " +transferDetails.get("officeCode");
                    break;
                case "Division":
                    builder.append("<td style=\"text-align:right\"><b>Source</b> </td> " +
                                   "<td style=\"text-align:left\"> "+from +" "+ transferDetails.get("ofdaDivisionCode")+" Budget </td>");
                    sub =transferDetails.get("projName") + " got new budget from " +transferDetails.get("ofdaDivisionCode");
                    break;
                case "Team":
                    builder.append("<td style=\"text-align:right\"><b>Source</b> </td> " +
                                   "<td style=\"text-align:left\"> "+from +" "+ transferDetails.get("ofdaTeamCode")+" Budget </td>");
                    sub =transferDetails.get("projName") + " got new budget from " +transferDetails.get("ofdaTeamCode");
                    break;
                }
                builder.append("</tr>");
                builder.append( "<tr>" + "<td style=\"text-align:right\"><b>Fund Account Type</b></td>" +
                                "<td style=\"text-align:right\">" + transferDetails.get("fundAcctTypeName") + "</td>" +
                                "</tr>" );
                builder.append("<tr> " +
                               "<td rowspan=\"4\" style=\"text-align:left; vertical-align:top \"> <b> To </b> </td>" +
                               "<td style=\"text-align:right\"><b>Project</b></td>" + "<td style=\"text-align:left\">" +
                               transferDetails.get("projectNbr") + ",  " + transferDetails.get("projName") + "</td>" +
                                "</tr>" + 
                                "<tr>" + "<td style=\"text-align:right\"><b>Budget Source</b></td>" +
                                "<td style=\"text-align:right\">" + transferDetails.get("budgetSource") + "</td>" +
                                "</tr>" + 
                               "</tr>" + 
                               "<tr>" + "<td style=\"text-align:right\"><b>Transferred Amt</b></td>" +
                               "<td style=\"text-align:right\">" + transferDetails.get("transferAmt") + "</td>" +
                               "</tr>" + 
                               "<tr>" + "<td style=\"text-align:right\"> <b>Available Budget</b><br> ("+transferDetails.get("fundAcctTypeName")+") </td>" +
                               "<td style=\"text-align:right\">" + transferDetails.get("availBudgetTo") + "</td>" +
                               "</tr>" + "<tr>" + "<td colspan=\"3\" style=\"text-align:left\"> <b>Comments </b>" +
                               transferDetails.get("comments") + "</td>" + "</tr>" + "<tr>" +
                               "<td colspan=\"3\" style=\"text-align:left\"> <b>Updated By </b>" + username +
                               " <b> On </b> " + fdate + "</td>" + "</tr>" + "</tbody>" + "</table>");
                
                String htmlMsg = builder.toString();
                Boolean isMailSent = email.sendHtmlEmail(emailMap.get("toAdd"), emailMap.get("ccAdd"), sub, htmlMsg, null, null);
                String emailList = emailMap.get("emailList");
                if (isMailSent)
                {
                    this.addToGrowl(GrowlType.notice, "Budget added successfully", 0, 5000);
                    this.addToGrowl(GrowlType.mailok, "Email sent to "  + emailList, 50, 10000);
                }
                else
                {
                    this.addToGrowl(GrowlType.notice, "Budget added successfully", 0, 5000);
                    this.addToGrowl(GrowlType.mailerror, "Email sent to "  + emailList+" has failed!", 50, 10000);
                }
            } else
                this.addToGrowl(GrowlType.notice, "Budget added successfully", 0, 5000);
        } else if ("false".equals(result)) {
            this.addToGrowl(GrowlType.error, "Transfer validation failed. Please try again", 0, 5000);
        } else if (result != null)
        this.addToGrowl(GrowlType.error, result, 0, 10000);
    }

    public boolean sendProposalReviewEmail(String subject, String message, String actionID) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        StringBuilder baseURL = new StringBuilder();

        baseURL.append(appScope.get("REPORT_SERVER_DIRECT_URL") + "?Rep" + appScope.get("INSTANCE") +
                       "Pdf+report=PROPOSAL_SUMMARY.RDF+P_ACTION_ID=" + actionID + "+DESNAME=proposalReview.pdf");
        /*         PasswordCredential pc=getReportCredentials();
        if(pc!=null)
            baseURL.append("+authid="+pc.getName()+"/"+pc.getPassword()); */
        try {
            URL proposallink = new URL(baseURL.toString());
            File pFile = new File("Application_Review_" + actionID + ".pdf");
            FileUtils.copyURLToFile(proposallink, pFile);
            File fs[] = { pFile };
            boolean result =
                email.sendHtmlEmail((String) appScope.get("PROPOSAL_REVIEW_EMAIL"), getUserEmail(), subject,
                                    message + "<br><br>" + getActionDetails(actionID), "Application Review Email", fs);
            if (pFile != null)
                pFile.delete();
            if (result) {
                OperationBinding exec = dcBindingContainer.getOperationBinding("setProposalReviewEmailSent");
                exec.getParamsMap().put("actionID", actionID);
                exec.execute();
            }
            return result;
        } catch (MalformedURLException e) {
            sendAdminEmail("Error Sending Application Review Email", e.toString());
        } catch (IOException e) {
            sendAdminEmail("Error Sending Application Review Email", e.toString());
        }
        return false;
    }
    
    public boolean sendIssuesLetterEmail(String subject, String message, String actionID) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getIssueLetterEmailList");
        exec.getParamsMap().put("actionID", actionID);
        exec.execute();
        String[] list = (String[]) exec.getResult();
        return email.sendHtmlEmail(list[0], list[1], subject, message + "<br><br>" + getActionDetails(actionID),
                            "Issue Letter email", null);

    }

    public boolean sendActionApprovedEmail(String subject, String message, String actionID) {
        return email.sendHtmlEmail((String) appScope.get("FIN_EMAIL"), getUserEmail(), subject,
                            message + "<br><br>" + getActionDetails(actionID),
                            "Request for Ctrl Nbr or Appproved Amt changed", null);
    }

    public boolean sendContrlNbrGeneratedEmail(String subject, String message, String actionID,
                                            boolean isAttachChecklist) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getControlNbrGeneratedList");
        exec.getParamsMap().put("actionID", actionID);
        exec.execute();
        String[] list = (String[]) exec.getResult();
        if (isAttachChecklist) {
            StringBuilder baseURL = new StringBuilder();
            baseURL.append(appScope.get("REPORT_SERVER_DIRECT_URL") + "?Rep" + appScope.get("INSTANCE") +
                           "Pdf+report=ACTION_CHECKLIST.RDF+P_ACTION_ID=" + actionID +
                           "+DESNAME=AwardPackageChecklist.pdf");
            /*             PasswordCredential pc=getReportCredentials();
            if(pc!=null)
                baseURL.append("+authid="+pc.getName()+"/"+pc.getPassword()); */
            try {
                URL proposallink = new URL(baseURL.toString());
                File pFile = new File("AwardPackageChecklist_" + actionID + ".pdf");
                FileUtils.copyURLToFile(proposallink, pFile);
                File fs[] = { pFile };
               boolean t =email.sendHtmlEmail(list[0], list[1], subject, message + "<br><br>" + getActionDetails(actionID),
                                    "Control Nbr generated email", fs);
                if (pFile != null)
                    pFile.delete();
             return t;
            } catch (MalformedURLException e) {
                sendAdminEmail("Error sending Control Nbr generated email", e.toString());
            } catch (IOException e) {
                sendAdminEmail("Error sending Control Nbr generated email", e.toString());
            }
        } else
          return  email.sendHtmlEmail(list[0], list[1], subject, message + "<br><br>" + getActionDetails(actionID),
                                "Control Nbr generated or changed", null);
        return false;
    }

    public boolean sendActionDistributeAmtChangedEmail(String subject, String message, String actionID) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getDistributionAmtChangedList");
        exec.getParamsMap().put("actionID", actionID);
        exec.execute();
        String[] list = (String[]) exec.getResult();
        return email.sendHtmlEmail(list[0], list[1], subject, message + "<br><br>" + getActionDetails(actionID),
                            "Distribution Amount changed email", null);

    }

    public boolean sendAwardTrackingEmail(String subject, String message, String actionID) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getAwardTrackingList");
        exec.getParamsMap().put("actionID", actionID);
        exec.execute();
        String[] list = (String[]) exec.getResult();
        return email.sendHtmlEmail(list[0], list[1], subject, message + "<br><br>" + getActionDetails(actionID),
                            "Award Tracking email", null);

    }
    public boolean sendGUProposalReviewSpltEmail(String subject, String message, String actionID, String oldSpecialist) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getProposalReviewSpltList");
        exec.getParamsMap().put("actionID", actionID);
        exec.getParamsMap().put("oldPrSpecialist", oldSpecialist);
        exec.execute();
        String[] list = (String[]) exec.getResult();
        return email.sendHtmlEmail(list[0], list[1], subject, message + "<br><br>" + getActionDetails(actionID),
                            "Application Review Specialist Email", null);

    }

    public boolean sendObligatedAmtChangedEmail(String subject, String message, String actionID) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getObligatedAmtChgList");
        exec.getParamsMap().put("actionID", actionID);
        exec.execute();
        String[] list = (String[]) exec.getResult();
        return email.sendHtmlEmail(list[0], list[1], subject, message + "<br><br>" + getActionDetails(actionID),
                            "Award Tracking email", null);

    }

    public boolean sendAORLetterEmail(String subject, String message, String actionID) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getAORLetterList");
        exec.getParamsMap().put("actionID", actionID);
        exec.execute();
        String[] list = (String[]) exec.getResult();

        StringBuilder baseURL = new StringBuilder();
        baseURL.append(appScope.get("REPORT_SERVER_DIRECT_URL") + "?Rep" + appScope.get("INSTANCE") +
                       "Pdf+report=AOTRLETTER.RDF+P_ACTION_ID=" + actionID + "+DESNAME=proposalReview.pdf");
        /*             PasswordCredential pc=getReportCredentials();
            if(pc!=null)
                baseURL.append("+authid="+pc.getName()+"/"+pc.getPassword()); */
        try {
            URL proposallink = new URL(baseURL.toString());
            File pFile = new File("AOR_Letter_" + actionID + ".pdf");
            FileUtils.copyURLToFile(proposallink, pFile);
            File fs[] = { pFile };
            String aoremailto=(String) appScope.get("AOR_LETTER_EMAIL_TO");
            String aoremailcc=(String) appScope.get("AOR_LETTER_EMAIL_CC");
            boolean t=email.sendHtmlEmail(aoremailto+";"+list[0], aoremailcc+";"+list[1], subject,
                                message + "<br><br>" + getActionDetails(actionID), "AOR Letter email", fs);
            if (pFile != null)
                pFile.delete();
        return t;
        } catch (MalformedURLException e) {
            sendAdminEmail("Error sending AOR Letter email", e.toString());
        } catch (IOException e) {
            sendAdminEmail("Error sending AOR Letter email", e.toString());
        }
        return false;
    }

    public boolean sendAwardPkgIncompleteEmail(String subject, String message, String actionID) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getAwardPkgIncompleteList");
        exec.getParamsMap().put("actionID", actionID);
        exec.execute();
        String[] list = (String[]) exec.getResult();
        String gumgr=(String) appScope.get("GU_MGR");
        return email.sendHtmlEmail(list[0], gumgr+";"+list[1], subject, message + "<br><br>" + getActionDetails(actionID),
                            "Original/Revised Award Package Incomplete email", null);
    }

    public boolean sendGUPrintShopPkgCompleteEmail(String subject, String message, String actionID) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getGUPrintShopPkgCompleteList");
        exec.getParamsMap().put("actionID", actionID);
        exec.execute();
        String[] list = (String[]) exec.getResult();
        String actionAwardPackageFolderURL =
            "<br><a href=\"" + appScope.get("URL") +
            FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() +
            "/faces/viewAction?actionID=" + actionID + "&defaultActionTab=awardPackageFolder\">Click here to go to Award Package Folder.</a><br>";
        
        return email.sendHtmlEmail((String) appScope.get("GU_PRINTCENTER"), list[1], subject,
                            message+"<br><br>"+actionAwardPackageFolderURL + "<br><br>" + getActionDetails(actionID), "Award Package Incomplete email", null);
    }

    public boolean  sendAwardPkgCompleteEmail(String subject, String message, String actionID) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getAwardPkgCompleteList");
        exec.getParamsMap().put("actionID", actionID);
        exec.execute();
        String[] list = (String[]) exec.getResult();
        String actionAwardPackageFolderURL =
            "<br><a href=\"" + appScope.get("URL") +
            FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() +
            "/faces/viewAction?actionID=" + actionID + "&defaultActionTab=awardPackageFolder\">Click here to go to Award Package Folder.</a><br>";
        
        return email.sendHtmlEmail(list[0], list[1], subject,  message+"<br><br>"+actionAwardPackageFolderURL  + "<br><br>" + getActionDetails(actionID),
                            "Original/Revised Award Package is complete for your review", null);
    }
    
    public boolean  sendAwardPkgClearanceEmail(String subject, String message, String actionID,String oldUserid,String newUserid) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getAwardPkgClearanceList");
        exec.getParamsMap().put("actionID", actionID);
        exec.getParamsMap().put("oldUserid", oldUserid);
        exec.getParamsMap().put("newUserid", newUserid);
        exec.execute();
        String[] list = (String[]) exec.getResult();
        String actionAwardPackageFolderURL =
            "<br><a href=\"" + appScope.get("URL") +
            FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() +
            "/faces/viewAction?actionID=" + actionID + "&defaultActionTab=awardPackageFolder\">Click here to go to Award Package Folder.</a><br>";
        
        return email.sendHtmlEmail(list[0], list[1], subject,  message+"<br><br>"+actionAwardPackageFolderURL  + "<br><br>" + getActionDetails(actionID),
                            "Award Package Clearance", null);
    }
    
    public void sendApprovalLetterEmail(ArrayList<String> toAdd,ArrayList<String> ccAdd,String subject, String message, String letterMemoId) {
        String toAddress="";
        String ccAddress="";
        for(String i:toAdd)
            toAddress+=getEmailbyUsername(i)+";";
        for(String i:ccAdd)
            ccAddress+=getEmailbyUsername(i)+";";
        
        // if letter memo id is not null, construct url
        if(null != letterMemoId ){
            Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
             StringBuilder baseURL = new StringBuilder();
             baseURL.append(appScope.get("REPORT_SERVER_DIRECT_URL") + "?Rep" + appScope.get("INSTANCE") +
                            "Pdf+report=ART_DOCS.RDF+DESNAME=ART.pdf+P_LETTER_MEMO_ID=" +letterMemoId); 
                 
             // download the file
             
             URL proposallink;
                try {
                    proposallink = new URL(baseURL.toString());
                    File pFile = new File("Approval_Letter_Documents.pdf");
                    FileUtils.copyURLToFile(proposallink, pFile);
                    File[] attachements = {pFile};
                    email.sendHtmlEmail(toAddress, ccAddress, subject, message,subject, attachements);
                } catch (MalformedURLException e) {
                        sendAdminEmail("Error Sending Approval Letter Email", e.toString());
                } catch (IOException e) {
                        sendAdminEmail("Error Sending Approval Letter Email", e.toString());
                }    
        }else {
            
            email.sendHtmlEmail(toAddress, ccAddress, subject, message,subject, null);
        }
    }

    public String getActionDetails(String actionID) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getActionDetails");
        exec.getParamsMap().put("actionID", actionID);
        exec.execute();

        String actionURL =
            "<br><a href=\"" + appScope.get("URL") +
            FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() +
            "/faces/viewAction?actionID=" + actionID + "\">Click here to view action in Abacus.</a><br>";
        String actionDetails = (String) exec.getResult();
        return actionURL + actionDetails;
    }

    public String getEmailbyUsername(String username) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getEmailbyUsername");
        exec.getParamsMap().put("username", username);
        exec.execute();
        String email = (String) exec.getResult();
        return email;
    }

    public String getUserEmail() {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getUserEmail");
        exec.execute();
        String email = (String) exec.getResult();
        return email;
    }

    public void sendAdminEmail() {
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String servername = FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
        int serverport = FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort();
        String username = ADFContext.getCurrent().getSecurityContext().getUserName();
        String message = (String) pfp.get("message");
        String module = (String) pfp.get("module");
        if (message != null) {
            String tmp =
                "<table><tr><th>Error Message</th><td>" + message + "</td></tr><tr><th>Username</th><td>" + username +
                "</td></tr><tr><th>Server</th><td>" + servername + "</td></tr><tr><th>port</th><td>" + serverport +
                "</td></tr></table>";
            Thread thread = new Thread(new MySendMailRunnable(email, "Error in " + module, tmp));
            thread.start();
        }
    }

    public void sendAdminEmail(String subject, String message) {
        String servername = FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
        int serverport = FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort();
        String username = ADFContext.getCurrent().getSecurityContext().getUserName();
        if (message != null) {
            String tmp =
                "<table><tr><th>Error Message</th><td>" + message + "</td></tr><tr><th>Username</th><td>" + username +
                "</td></tr><tr><th>Server</th><td>" + servername + "</td></tr><tr><th>port</th><td>" + serverport +
                "</td></tr></table>";
            Thread thread = new Thread(new MySendMailRunnable(email, subject, tmp));
            thread.start();
        }
    }

    public Map<String, String> getActivityFundTransferEmailList(String fiscalYear,String projectNbr) {
        Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
        Map<String, String> emailMap = new HashMap<String, String>();
        StringBuilder builder = new StringBuilder();
        String userEmail = this.getUserEmail();
        builder.append(appScope.get("FIN_EMAIL"));
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
        bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getFundTransferEmailList");
        exec.getParamsMap().put("level", "Project");
        exec.getParamsMap().put("fiscalYear", fiscalYear);
        exec.getParamsMap().put("projectNbr", projectNbr);
        exec.execute();
        String teamEmailList = (String) exec.getResult();
        if(teamEmailList!=null)
            builder.append("; "+teamEmailList);
        emailMap.put("toAdd", builder.toString());
        if (userEmail != null) {
            emailMap.put("ccAdd", userEmail);
            emailMap.put("emailList", userEmail+"; "+builder.toString());
        }
        else
        {
            emailMap.put("ccAdd",null);
            emailMap.put("emailList", builder.toString());
        }
        return emailMap;
    }
    
    
    public Map<String, String> getActivityBudgetTransferEmailList(String fiscalYear,String projectNbr) {
        Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
        Map<String, String> emailMap = new HashMap<String, String>();
        StringBuilder builder = new StringBuilder();
        String userEmail = this.getUserEmail();
        builder.append(appScope.get("FIN_EMAIL"));
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
        bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getBudgetTransferEmailList");
        exec.getParamsMap().put("level", "Project");
        exec.getParamsMap().put("fiscalYear", fiscalYear);
        exec.getParamsMap().put("projectNbr", projectNbr);
        exec.execute();
        String teamEmailList = (String) exec.getResult();
        if(teamEmailList!=null)
            builder.append("; "+teamEmailList);
        emailMap.put("toAdd", builder.toString());
        if (userEmail != null) {
            emailMap.put("ccAdd", userEmail);
            emailMap.put("emailList", userEmail+"; "+builder.toString());
        }
        else
        {
            emailMap.put("ccAdd",null);
            emailMap.put("emailList", builder.toString());
        }
        return emailMap;
    }

    public void sendProjectCreatedEmail(String details) {
        Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
        String projectEmail = (String) appScope.get("PROJECT_EMAIL");
        String ccAddr = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("newActivityCCEmail");
        String teamLeadEmail = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("teamLeadEmail");
        FacesContext context = FacesContext.getCurrentInstance();
        if (ccAddr == null) {
            ccAddr = "";
        }
        String subject = "New Project Created";
        // send an email
        boolean isMailSent =
            email.sendHtmlEmail(projectEmail, getUserEmail()+";"+teamLeadEmail + ";" + ccAddr, subject, details, null, null);
        if (isMailSent) {
            /*context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                "Project created successfully. Email has been sent to " +
                                                appScope.get("PROJECT_EMAIL") + "; " + ccAddr, null));*/
            this.addToGrowl(GrowlType.notice,"Project created successfully.",0,5000);
            this.addToGrowl(GrowlType.mailok,"Email has been sent to " +appScope.get("PROJECT_EMAIL") + "; " + ccAddr, 50, 10000);
        } else {
           /* context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                "Project created successfully. Email to " +
                                                appScope.get("PROJECT_EMAIL") + "; " + ccAddr + " has failed", null));*/
            this.addToGrowl(GrowlType.warning,"Project created successfully.",0,5000);
            this.addToGrowl(GrowlType.mailerror,"Email to " +appScope.get("PROJECT_EMAIL") + "; " + ccAddr + " has failed!", 50, 10000);

        }
    }

    /**
     * this method sends an email after the budget recover is done successfully.
     * @param result
     * @param transferDetails
     * @param isSendEmail
     */
    public void sendBudgetRecoveryEmail(String result, Map transferDetails, Boolean isSendEmail) {
        // get the constants
        Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
        FacesContext context = FacesContext.getCurrentInstance();
        // check if the result is success
        if ("true".equalsIgnoreCase(result)) {
            // get the budget recovered to
            String recoveredTo = (String) transferDetails.get("to");
            if (null != isSendEmail && isSendEmail) {
                Map<String, String> emailMap = getActivityBudgetTransferEmailList(""+transferDetails.get("budgetFY"),(String) transferDetails.get("projectNbr"));
                String recoverToSub =
                    recoveredTo.equalsIgnoreCase("Division") ? "Division " + transferDetails.get("ofdaDivisionCode") :
                    "Team " + transferDetails.get("ofdaTeamCode");
                String subject =
                    recoveredTo.equalsIgnoreCase("Division") ?
                    "Budget Recovery from Project " + transferDetails.get("projName") + " to Division " +
                    transferDetails.get("ofdaDivisionCode") :
                    "Budget Recovery from Project " + transferDetails.get("projName") + " to Team " +
                    transferDetails.get("ofdaTeamCode");
                String username = ADFContext.getCurrent().getSecurityContext().getUserName();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                String fdate = sdf.format(new Date());
                String from = (String) transferDetails.get("from");

                // Construct email table
                StringBuilder builder = new StringBuilder();
                builder.append("<table border=\"1\" style=\"border-collapse:collapse\" cellpadding=\"5\">" + "<tbody>" +
                               "<tr> " + "<td style=\"text-align:right\"> <b>Year</b> </td> " +
                               "<td style=\"text-align:left\">" + transferDetails.get("budgetFY") + "</td> " + "</tr>" +
                               "<tr>");
                builder.append("<td style=\"text-align:right\"><b>From</b> </td> " + "<td style=\"text-align:left\">" +
                               transferDetails.get("projectNbr") + ",  " + transferDetails.get("projName") + "</td>");

                builder.append("</tr>" + "<tr> " + "<td style=\"text-align:right\"><b>To</b></td>" +
                               "<td style=\"text-align:left\">" + recoverToSub + "</td>" + "</tr>");
                builder.append("<tr> " + "<td style=\"text-align:right\"><b>Fund Account Type</b></td>" +
                               "<td style=\"text-align:right\">" + transferDetails.get("fundAcctTypeName") + "</td>" + "</tr>");
                builder.append("<tr> " + "<td style=\"text-align:right\"><b>Budget Source</b></td>" +
                               "<td style=\"text-align:right\">" + transferDetails.get("budgetSource") + "</td>" + "</tr>");
                builder.append("<tr> " + "<td style=\"text-align:right\"><b>Recovered Budget Amt</b></td>" +
                               "<td style=\"text-align:right\">" + transferDetails.get("recoverBudgetAmt") + "</td>" + "</tr>" +

                               "<tr>" + "<td colspan=\"3\" style=\"text-align:left\"> <b>Comments </b>" +
                               transferDetails.get("comments") + "</td>" + "</tr>" + "<tr>" +
                               "<td colspan=\"3\" style=\"text-align:left\"> <b>Updated By </b>" + username +
                               " <b> On </b> " + fdate + "</td>" + "</tr>" + "</tbody>" + "</table>");

                String htmlMsg = builder.toString();

                Boolean isMailSent = email.sendHtmlEmail(emailMap.get("toAdd"), emailMap.get("ccAdd"), subject, htmlMsg, null, null);
                String emailList = emailMap.get("emailList");
                if (isMailSent) {

                    this.addToGrowl(GrowlType.notice,"Budget recovered successfully.",0,5000);
                    this.addToGrowl(GrowlType.mailok,"Email sent to " + emailList, 50, 10000);
                } else {

                    this.addToGrowl(GrowlType.warning,"Budget recovered successfully.",0,5000); 
                    this.addToGrowl(GrowlType.mailerror,"Email to " + emailList + " has failed!", 50, 10000);

                }


            } else {

                this.addToGrowl(GrowlType.notice, "Budget recovered successfully", 0, 5000);

            }
        } else if ("false".equalsIgnoreCase(result)) {

            this.addToGrowl(GrowlType.error, "Transfer validation failed. Please try again", 0, 5000);
        }

        else if (result != null) {
            this.addToGrowl(GrowlType.error, result, 0, 5000);

        }

    }

    /**
     * This method sends an email based on the 'isSendEmail' parameter.
     * @param result
     * @param transferDetails
     * @param isSendEmail
     */
    public void sendFundRecoveryEmail(String result, Map transferDetails, Boolean isSendEmail) {

        Map appScope =
            ((AppBean) ADFContext.getCurrent().getApplicationScope().get(AbacusConstants.APP_BEAN)).getConstants();
        FacesContext context = FacesContext.getCurrentInstance();
        // check if the result is success
        if ("true".equalsIgnoreCase(result)) {
            // get the budget recovered to
            String recoveredTo = (String) transferDetails.get(AbacusConstants.RECOVER_TO);
            if (null != isSendEmail && isSendEmail) {
                Map<String, String> emailMap = getActivityFundTransferEmailList(""+ transferDetails.get(AbacusConstants.BUDGET_FY),(String) transferDetails.get(AbacusConstants.PROJECT_NUMBER));
                String emailList = emailMap.get("emailList");
                String subject = "Project Recover Funds";
                //recoveredTo.equalsIgnoreCase(AbacusConstants.DIVISION) ? "Funds Recovery from Project " +transferDetails.get(AbacusConstants.PROJECT_NAME) +" to Division " + transferDetails.get(AbacusConstants.OFDA_DIVISION_CODE) : "Funds Recovery from Project " +transferDetails.get(AbacusConstants.PROJECT_NAME) + " to OFDA";
                String username = ADFContext.getCurrent().getSecurityContext().getUserName();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                String fdate = sdf.format(new Date());


                StringBuilder builder = new StringBuilder();
                builder.append("<table border=\"1\" style=\"border-collapse:collapse; width: 500px;\" cellpadding=\"5\">" +
                               "<tbody>");
                //if("OFDA".equals(transferLevel)){
                builder.append("<tr>" + "<td colspan=\"2\" style=\"text-align:left\"> <b> To</b> </td>" + "</tr>");

                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Fiscal Year</td>" +
                               "<td style=\"text-align:left\">" + transferDetails.get(AbacusConstants.BUDGET_FY) +
                               "</td>" + "</tr>");
                if("Bureau".equals(recoveredTo))
                {
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">To</td>" +
                               "<td style=\"text-align:left\">" +"Bureau " + transferDetails.get(AbacusConstants.BUREAU_CODE)+
                               "</td>" + "</tr>");
                }
                if("Office".equals(recoveredTo))
                {
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">To</td>" +
                               "<td style=\"text-align:left\">" +"Office "+ transferDetails.get(AbacusConstants.OFFICE_CODE) +
                               "</td>" + "</tr>");
                }
                if("Division".equals(recoveredTo))
                {
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">To</td>" +
                               "<td style=\"text-align:left\">" +"Division " + transferDetails.get(AbacusConstants.OFDA_DIVISION_CODE) +
                               "</td>" + "</tr>");
                }
                
                builder.append("<tr>" + "<td colspan=\"2\" style=\"text-align:left\"> <b>" + ("From") + " </b> </td>" +
                               "</tr>");
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Fiscal Year</td>" +
                               "<td style=\"text-align:left\">" + transferDetails.get(AbacusConstants.BUDGET_FY) +
                               "</td>" + "</tr>");
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Bureau</td>" +
                               "<td style=\"text-align:left\">" + transferDetails.get(AbacusConstants.BUREAU_CODE) + "</td>" + "</tr>");
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Office</td>" +
                               "<td style=\"text-align:left\">" + transferDetails.get(AbacusConstants.OFFICE_CODE) + "</td>" + "</tr>");
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Division</td>" +
                               "<td style=\"text-align:left\">" +
                               transferDetails.get(AbacusConstants.OFDA_DIVISION_CODE) + "</td>" + "</tr>");
                builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Project</td>" +
                               "<td style=\"text-align:left\">" + transferDetails.get(AbacusConstants.PROJECT_NUMBER) +
                               ",  " + transferDetails.get(AbacusConstants.PROJECT_NAME) + "</td>" + "</tr>");
                /*builder.append("<tr>" + "<td style=\"text-align:right\">From</td>" + "<td style=\"text-align:left\">" +
                               recoverToSub + "</td>" + "</tr>");*/
                if (transferDetails.get(AbacusConstants.RECOVER_BY).equals("byFundCode")) {
                    builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">From</td>" +
                                   "<td style=\"text-align:left\">" + "Select Fund Code" + "</td>" + "</tr>");
                    builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">Fund Code</td>" +
                                   "<td style=\"text-align:left\">" +
                                   (
                                    transferDetails.get(AbacusConstants.FUND_CODE)) + "</td>" +
                                   "</tr>");
                } else {
                    builder.append("<tr>" + "<td style=\"text-align:right; width:30%;\">From</td>" +
                                   "<td style=\"text-align:left\">" + "All Fund Codes" + "</td>" + "</tr>");
                }
                
                builder.append("<tr>" +
                    // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" +
                    "<td style=\"text-align:right\">" + ("Fund Amt") + "</td>" + "<td style=\"text-align:left\">" +
                    transferDetails.get(AbacusConstants.RECOVER_AMT) + "</td>" + "</tr>");
                if (transferDetails.get(AbacusConstants.RECOVER_BY).equals("byFundCode")) {
                builder.append("<tr>" +
                    // "<td rowspan=\"1\" width = \"50px\" style=\"text-align:left; vertical-align:top \"> <b> </b> </td>" +
                    "<td style=\"text-align:right; width:30%;\">" + ("Available Fund Amt") + "</td>" + "<td style=\"text-align:left\">" +
                    transferDetails.get(AbacusConstants.FUNDS_AVAILABLE) + "</td>" + "</tr>");
                }
                builder.append("<tr>" +
                               "<td colspan=\"2\" style=\"text-align:left; word-wrap: break-word;\"> <b>Comments: </b> " +
                               transferDetails.get(AbacusConstants.COMMENTS) + "</td>" + "</tr>");

                builder.append("<tr>" +
                               "<td colspan=\"2\" style=\"text-align:left; word-wrap: break-word;\"> <b>Updated By </b>" +
                               username + " <b> On </b>" + fdate + "</td>" + "</tr>" + "</tbody>" + "</table>");

                String htmlMsg = builder.toString();

                Boolean isMailSent = email.sendHtmlEmail(emailMap.get("toAdd"), emailMap.get("ccAdd"), subject, htmlMsg, null, null);
                if (isMailSent) {
                   /* context.addMessage(null,
                                       new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                        "Funds recovered successfully. Email sent to " + toAdd + " " +
                                                        ccAdd, null));*/
                    this.addToGrowl(GrowlType.notice,"Funds recovered successfully.",0,5000);
                    this.addToGrowl(GrowlType.mailok,"Email sent to " + emailList, 0, 5000);
                } else {
                   /* context.addMessage(null,
                                       new FacesMessage(FacesMessage.SEVERITY_WARN,
                                                        "Funds recovered successfully. Email to " + toAdd + " " +
                                                        ccAdd + " has failed", null));*/
                    this.addToGrowl(GrowlType.warning,"Funds recovered successfully.",0,5000);
                    this.addToGrowl(GrowlType.mailerror,"Email to " + emailList + " has failed", 50, 10000);

                }


            } else {
               /* context.addMessage(null,
                                   new FacesMessage(FacesMessage.SEVERITY_INFO, "Funds recovered successfully", null));*/
                this.addToGrowl(GrowlType.notice, "Funds recovered successfully", 0, 5000);

            }
        } else if ("false".equalsIgnoreCase(result)) {
           /* context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                "Transfer validation failed. Please try again", null));*/
            this.addToGrowl(GrowlType.error, "Transfer validation failed. Please try again", 0, 5000);
        }

        else if (result != null) {
            //context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, result, null));
            this.addToGrowl(GrowlType.error, result, 0, 5000);

        }
    }

    public boolean sendCostSheetApplicableEmail(String subject, String message, String paxActivitiesId ){
             
        return email.sendHtmlEmail((String) appScope.get("CONTRIBUTION_EMAIL"),getUserEmail(), subject,
                            message + "<br><br>" + getContributionDetails(paxActivitiesId), "New Contribution Email", null);
    }
    
    public boolean sendCostSheetInternalReviewEmail(String subject, String message, String actionID ,String paxActivitiesId ){
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getCostSheetInternalReviewList");
        exec.getParamsMap().put("actionID", actionID); 
        exec.execute();
        String contributionEmailGroup=(String) appScope.get("CONTRIBUTION_EMAIL");
        String[] list = (String[]) exec.getResult();
        
        return email.sendHtmlEmail(contributionEmailGroup+";"+list[0], list[1], subject + " for " + list[2], message + "<br><br>" + getContributionDetails(paxActivitiesId),
                            "Contribution Internal Review Email", null);     
       
    }
    
    public boolean sendCostSheetApprovedEmail(String subject, String message, String actionID ){
        
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getCostSheetApprovedList");
        exec.getParamsMap().put("actionID", actionID); 
        exec.execute();
        String[] list = (String[]) exec.getResult();
             
        return email.sendHtmlEmail(list[0], list[1], subject,
                            message + "<br><br>" + getCostSheetDetails(list[2]), "Contribution Approved Email", null);
    }
    public String getContributionDetails(String paxActivitiedId) {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer dcBindingContainer =
            bindingContext.findBindingContainer("gov_ofda_abacus_view_appScopePageDef");
        OperationBinding exec = dcBindingContainer.getOperationBinding("getContributionDetails");
        exec.getParamsMap().put("paxActivitiesId", paxActivitiedId);
        exec.execute();

        String contributionURL =
            "<br><a href=\"" + appScope.get("URL") +
            FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() +
            "/faces/viewContribution?contributionNbr=" + paxActivitiedId + "\">Click here to view contribution in Abacus.</a><br>";
        String contributionDetails = (String) exec.getResult();
        return contributionURL + contributionDetails;
    }
    
    public String getCostSheetDetails(String paaId) { 
        String actionURL =
            "<br><a href=\"" + appScope.get("AAMP_URL") +appScope.get("AAMP_APPLICATION_LINK") +
            "?paaId=" + paaId + "\">Click here to view the Contribution in AAMP</a><br>";       
        return actionURL ;
    }
    
    private class MySendMailRunnable implements Runnable {
        private AbacusEmail email;
        private String subject;
        private String message;

        public String getSubject() {
            return subject;
        }

        public String getMessage() {
            return message;
        }


        public MySendMailRunnable(AbacusEmail email, String subject, String message) {
            super();
            this.subject = subject;
            this.message = message;
            this.email = email;
        }

        @Override
        public void run() {
            email.sendHtmlEmail("abacusadmin@ofda.gov", null, this.getSubject(), this.getMessage(), null, null);
        }
    }
}
