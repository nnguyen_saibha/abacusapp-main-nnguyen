package gov.ofda.abacus.view.bean.lookup;

import gov.ofda.abacus.model.abacusapp.lov.module.LOVEditModuleImpl;

import java.io.Serializable;

import java.util.Map;

import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class MoveAwardee implements Serializable {
    @SuppressWarnings("compatibility:6701553968753891583")
    private static final long serialVersionUID = 1L;
    
    private static ADFLogger logger =
        ADFLogger.createADFLogger(MoveAwardee.class);

    public void selectFromAwardeePopupReturnListener(ReturnPopupEvent returnPopupEvent) {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("moveAwardeeLookupIterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row row = rsi.getCurrentRow();
        if (row != null) {
            pageFlowMap.put("fromAwardeeCode", row.getAttribute("AwardeeCode"));
            pageFlowMap.put("fromAwardeeTypeCode", row.getAttribute("AwardeeTypeCode1"));
            pageFlowMap.put("fromAwardeeName", row.getAttribute("AwardeeName"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent());
        }
            rsi.closeRowSetIterator();
            //ExecuteWithParams1
            OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
            exec.getParamsMap().put("iAwardeCode1", pageFlowMap.get("fromAwardeeCode"));
            exec.execute();
    }

    public void selectToAwardeePopupReturnListener(ReturnPopupEvent returnPopupEvent) {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("MoveAwardeeLookup2Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row row = rsi.getCurrentRow();
        if (row != null) {
            pageFlowMap.put("toAwardeeCode", row.getAttribute("AwardeeCode"));
            pageFlowMap.put("toAwardeeTypeCode", row.getAttribute("AwardeeTypeCode1"));
            pageFlowMap.put("toAwardeeName", row.getAttribute("AwardeeName"));
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent().getParent());
        }
            rsi.closeRowSetIterator();
            //ExecuteWithParams1
            OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams1");
            exec.getParamsMap().put("iAwardeCode1", pageFlowMap.get("toAwardeeCode"));
            exec.execute();
    }

    public void moveAwardee(String fromAwardee, String toAwardee, String operation){
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(fromAwardee.equalsIgnoreCase(toAwardee)){
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "From Awardee and To Awardee can not be same. Please check and try again!" , "");
            FacesContext.getCurrentInstance().addMessage(null, message); 
        }else{
            // call move awardee
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = bindings.getOperationBinding("moveAwardee");
            exec.getParamsMap().put("oldAwardee", fromAwardee);
            exec.getParamsMap().put("newAwardee", toAwardee);
            exec.getParamsMap().put("operation", operation);
            exec.execute();
                
            if(exec.getErrors().isEmpty()){
                exec = bindings.getOperationBinding("Commit");
                exec.execute();
                if(exec.getErrors().isEmpty()){
                    if("Move".equalsIgnoreCase(operation)){
                        FacesMessage message =
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully moved all data to " + pageFlowMap.get("toAwardeeName")+ ".", "");
                        FacesContext.getCurrentInstance().addMessage(null, message); 
                        
                    }else{
                        FacesMessage message =
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully moved all data to "+pageFlowMap.get("toAwardeeName") +" and deactivated " +pageFlowMap.get("fromAwardeeName")+"." , "");
                        FacesContext.getCurrentInstance().addMessage(null, message); 
                    }
                    
                }else{
                    exec = bindings.getOperationBinding("Rollback");
                    exec.execute();
                    logger.log(logger.ERROR, "Error occured while moving Awardee " + exec.getErrors());
                    FacesMessage message =
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,  "Failed to move Actions From: " + fromAwardee + " To: " + toAwardee +". Please contact abacus team @ abacus@ofda.gov" , "");
                    FacesContext.getCurrentInstance().addMessage(null, message); 
                }
            } else{
                logger.log(logger.ERROR, "Error occured while moving Awardee " + exec.getErrors());
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to move Actions From: " + fromAwardee + " To: " + toAwardee +". Please contact abacus team @ abacus@ofda.gov" , "");
                FacesContext.getCurrentInstance().addMessage(null, message); 
            }
        }
    }

    public void moveAndDeleteAwardeeConfirmationDialogListener(DialogEvent dialogEvent) {
        if(DialogEvent.Outcome.ok == dialogEvent.getOutcome()){
            Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            String fromAwardee = (String) pageFlowMap.get("fromAwardeeCode");
            String toAwardee = (String) pageFlowMap.get("toAwardeeCode");
            moveAwardee(fromAwardee, toAwardee, "MoveDelete");
        }
    }

    public void moveAwardeeConfirmationDialogListener(DialogEvent dialogEvent) {
        if(DialogEvent.Outcome.ok == dialogEvent.getOutcome()){
            Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            String fromAwardee = (String) pageFlowMap.get("fromAwardeeCode");
            String toAwardee = (String) pageFlowMap.get("toAwardeeCode");
            moveAwardee(fromAwardee, toAwardee, "Move");
        }
    }
}
