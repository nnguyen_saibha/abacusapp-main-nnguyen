package gov.ofda.abacus.view.bean;

import java.io.Serializable;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.binding.OperationBinding;

public class ViewAward implements Serializable {
    @SuppressWarnings("compatibility:-7857314168839446022")
    private static final long serialVersionUID = -8408347504631805678L;

    public ViewAward() {
        super();
    }


    public void refreshAwardInfo(ActionEvent actionEvent) {       
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Execute");
        exec.execute();
        exec = bindings.getOperationBinding("ActionExecute");
        exec.execute();
    }
}
