package gov.ofda.abacus.view.bean.lettermemo;

import java.io.Serializable;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySetImpl;

public class ViewLetterMemoBean implements Serializable{
    @SuppressWarnings("compatibility:3528369270711905387")
    private static final long serialVersionUID = 1L;
    private RowKeySetImpl newDisclosedTreeTableKeys = null;

    public void setNewDisclosedTreeTableKeys(RowKeySetImpl newDisclosedTreeTableKeys) {
        this.newDisclosedTreeTableKeys = newDisclosedTreeTableKeys;
    }

    public RowKeySetImpl getNewDisclosedTreeTableKeys() {
        if (newDisclosedTreeTableKeys == null) {
            newDisclosedTreeTableKeys = new RowKeySetImpl();
            FacesContext fctx = FacesContext.getCurrentInstance();
            UIViewRoot root = fctx.getViewRoot();
            //lookup the tree table component by its component ID
            RichTreeTable treeTable = (RichTreeTable) this.findComponent(root, "tt1");
            //System.out.println("Treetable value: "+treeTable);
            //if tree table is found
            if (treeTable != null) {
                //get the collection model to access the ADF binding layer for
                ////the tree binding used
                CollectionModel model = (CollectionModel) treeTable.getValue();
                if (model != null && model.getWrappedData() != null) {
                    JUCtrlHierBinding treeBinding = (JUCtrlHierBinding) model.getWrappedData();
                    JUCtrlHierNodeBinding nodeBinding = treeBinding.getRootNodeBinding();
                    expandAllNodes(nodeBinding, newDisclosedTreeTableKeys, 0, 2);
                }
            }
        } 
        return newDisclosedTreeTableKeys;
    }
    /**
     * Called to add keys of children in a tree to be disclosed.
     * This is a recursive call that uses maxExpandLevel to determine when to stop.
     * @param nodeBinding
     * @param disclosedKeys
     * @param currentExpandLevel
     * @param maxExpandLevel
     */
    private void expandAllNodes(JUCtrlHierNodeBinding nodeBinding, RowKeySetImpl disclosedKeys, int currentExpandLevel,
                                int maxExpandLevel) {
        if (currentExpandLevel <= maxExpandLevel) {
            List<JUCtrlHierNodeBinding> childNodes = (List<JUCtrlHierNodeBinding>) nodeBinding.getChildren();
            ArrayList newKeys = new ArrayList();
            if (childNodes != null) {
                for (JUCtrlHierNodeBinding _node : childNodes) {                   
                    newKeys.add(_node.getKeyPath());
                    expandAllNodes(_node, disclosedKeys, currentExpandLevel + 1, maxExpandLevel);
                }
            }          
            disclosedKeys.addAll(newKeys);
        }
    }
    /**
     * Locate an UIComponent from its root component.
     * @param base root Component (parent)
     * @param id UIComponent id
     * @return UIComponent object
     */
    public static UIComponent findComponent(UIComponent base, String id) {
        if (id.equals(base.getId()))
            return base;

        UIComponent children = null;
        UIComponent result = null;
        Iterator childrens = base.getFacetsAndChildren();
        while (childrens.hasNext() && (result == null)) {
            children = (UIComponent) childrens.next();
            if (id.equals(children.getId())) {
                result = children;
                break;
            }
            result = findComponent(children, id);
            if (result != null) {
                break;
            }
        }
        return result;
    }

    public static UIComponent findParentComponent(UIComponent base) {
        UIComponent result = null;
        String clientIDStr = base.getClientId();
         int regionStartIndx  = clientIDStr.indexOf("region");
        int regionValidateIndx = clientIDStr.indexOf(":",regionStartIndx);
       
        if (regionValidateIndx == -1) {
            result = base;

        } else {
            if (base.getParent() != null) {
                result = findParentComponent(base.getParent());
            } else {
                result = base;
            }
        }
        return result;
    }
    public ViewLetterMemoBean() {
    }

public void setApplicableLetterMemoView() {
    Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
    DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
    OperationBinding exec = bindings.getOperationBinding("setApplicableLetterMemoVC");
    Boolean isGUApprovalLetters = (Boolean) pfm.get("isGUApprovalLetters");
    exec.getParamsMap().put("isGUApprovalLetters",isGUApprovalLetters==null?false:isGUApprovalLetters);
    exec.execute();
    exec = bindings.getOperationBinding("ExecuteWithParams");
    exec.execute();
}
    public String setLetterMemoIfExists() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfm.put("skipSelectLetterMemo", "Y");
        if(pfm.get("letterMemoID")!=null)
         return "next";
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("LetterMemoListView2Iterator");
        if(iter.getEstimatedRowCount()<=1)
        {
          Row r=iter.getCurrentRow();
          if(r!=null)
            pfm.put("letterMemoID", r.getAttribute("LetterMemoId"));
          return "next";
        }
        else {
            pfm.put("errorMessage","More than one match found for this letter type: "+pfm.get("letterMemoTypeCode")+" "+pfm.get("linkCol"));
        }
        return "error";
    }
  public void resetApplicableList() {
      Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
      pfm.put("defaultLetterMemoTypeCode", null);
      DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
      OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
      exec.execute();
  }

    public void setActionAwardDetails() {
        /**TODO
         * Set Award Details when called from Award
         * */
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        String id = (String) pfm.get("id");
        OperationBinding exec = bindings.getOperationBinding("ActionDetailsExecute");
        exec.getParamsMap().put("p_action_id", id);
        exec.execute();
        exec = bindings.getOperationBinding("AwardDetailsExecute");
        exec.getParamsMap().put("p_award_seq", id);
        exec.execute();
    }

    public void setLetterMemoValidDocTypes() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfm.put("skipSelectLetterMemo", "N");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        String id = (String) pfm.get("id");
        String type="ACTION";
        DCIteratorBinding iter1 = bindings.findIteratorBinding("ApplicableLetterMemoView2Iterator1");
        Row r = iter1.getCurrentRow();
        if(r!=null)
           type = (String) r.getAttribute("LetterMemoGroupCode");
        String inSourceType = "3";
        if ("AWARD".equals(type)) {
            DCIteratorBinding iter2 = bindings.findIteratorBinding("AwardDetailsView1Iterator");
            Row rd = iter2.getCurrentRow();
            if (rd != null)
                id = ((BigDecimal) rd.getAttribute("AwardId")).toString();
            inSourceType = "2";
        }
        pfm.put("inSourceType", inSourceType);
        pfm.put("inSourceId", id);

        OperationBinding execDocs = bindings.getOperationBinding("getValidDocTypeCodeList");
        execDocs.execute();
        List<String> dList = (List<String>) execDocs.getResult();
        pfm.put("validDocTypeList", dList);
    }

    public void docsPopupFetchLsnr(PopupFetchEvent popupFetchEvent) {
        setLetterMemoValidDocTypes();
    }

    
    public void closeViewLettersDocsPopupListner(ActionEvent actionEvent) {
        RichPopup popup =
            (RichPopup) actionEvent.getComponent().getParent().getParent();
        popup.hide();
    }
}
