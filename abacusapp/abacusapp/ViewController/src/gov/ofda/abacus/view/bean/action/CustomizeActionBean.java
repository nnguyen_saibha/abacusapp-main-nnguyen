package gov.ofda.abacus.view.bean.action;

import gov.ofda.abacus.view.base.UIControl;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.LaunchPopupEvent;
import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

public class CustomizeActionBean extends UIControl{
    @SuppressWarnings("compatibility:910056429648743649")
    private static final long serialVersionUID = 1L;

    public CustomizeActionBean() {
        super();
    }

    public String initializeCustomizeAction() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        OperationBinding exec = bindings.getOperationBinding("setAction");
        exec.execute();
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        if (iter.getEstimatedRowCount() > 0) {

           initializeChangeProject();
           initializeChangeFAT();
           initializeCategoryActionType();
            pfm.put("selectCustomizeOption", "FUNDING");
            return "next";
        }
        return "close";
    }

    public String initializeChangeProject() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfm.put("projectMessage", null);
        pfm.put("projectApplicable", null);
        pfm.put("budgetFy", null);
        pfm.put("projectNbr", null);
        pfm.put("projectName", null);
        pfm.put("projectKey", null);
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("setAction");
        exec.execute();
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        OperationBinding execProjectVC = bindings.getOperationBinding("setProjectLOV");
        BigDecimal planned = new BigDecimal(1); //
        Row r = iter.getCurrentRow();
        boolean projectApplicable = false;
        if (r != null) {
            String actionTypeCode = (String) r.getAttribute("ActionTypeCode");
            String commitFlag = (String) r.getAttribute("CommitFlag");
            BigDecimal actionAmtStatus=(BigDecimal)r.getAttribute("ActionAmtStatus");
            String isCheckbook=(String)r.getAttribute("IsCheckbook");
            if("L".equals(actionTypeCode)) {
                pfm.put("projectMessage", "Project cannot be changed for balance record.");
            }
            else if (planned.compareTo(actionAmtStatus) == 0) {
                execProjectVC.getParamsMap().put("p_fy", null);
                if (("N".equals(commitFlag) && ("D".equals(actionTypeCode) || "Z".equals(actionTypeCode))) ||
                    ("Y".equals(commitFlag) && "P".equals(isCheckbook) &&("N".equals(actionTypeCode) || "M".equals(actionTypeCode)|| "U".equals(actionTypeCode)))) {
                    pfm.put("projectMessage", "Project cannot be changed for Regular, Mission and Embassy set actions.");
                } else {
                    projectApplicable = true;
                    if (("Y".equals(commitFlag) &&("D".equals(actionTypeCode) || "Z".equals(actionTypeCode))) ||
                        ("N".equals(commitFlag) && "P".equals(isCheckbook) &&("N".equals(actionTypeCode) ||"M".equals(actionTypeCode)))) {
                        execProjectVC.getParamsMap().put("p_fy", r.getAttribute("BudgetFy"));
                        pfm.put("projectMessage",
                                "Projects are restricted to action fiscal year since action is part of set.");
                    }
                }
                //Remove current Project from list
                execProjectVC.getParamsMap().put("p_not_fy",r.getAttribute("BudgetFy"));
                execProjectVC.getParamsMap().put("p_not_pnbr",r.getAttribute("ProjectNbr"));
                execProjectVC.execute();
            } else
                pfm.put("projectMessage", "Project can only be changed for planned action.");
        }
        pfm.put("projectApplicable", projectApplicable);
        return null;
    }

    public void projectLOVReturnLnsr(ReturnPopupEvent returnPopupEvent) {
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding activityIter = bindings.findIteratorBinding("ActivityLOV1Iterator");
        RowSetIterator activityRSIter = activityIter.getRowSetIterator();
        Row r = activityRSIter.getCurrentRow();
        if (r != null) {
            String budgetFY = (String) r.getAttribute("BudgetFy");
            String projectNbr = (String) r.getAttribute("ProjectNbr");
            String projectName = (String) r.getAttribute("ProjectName");
            pageFlowScope.put("budgetFy", budgetFY);
            pageFlowScope.put("projectNbr", projectNbr);
            pageFlowScope.put("projectName", projectName);
            pageFlowScope.put("projectKey", r.getAttribute("ProjectKey"));
        }
    }

    public String changeProject() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if (pfm.get("budgetFy") != null && pfm.get("projectNbr") != null) {
            OperationBinding exec = bindings.getOperationBinding("changeProjectForAction");
            exec.execute();
            Boolean result = (Boolean) exec.getResult();
            if(!result) {
                exec = bindings.getOperationBinding("Rollback");
                exec.execute();
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Unexpected error while changing project. Please try again. If issue persists, contact Abacus Team."));
            }
            else {
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Project successfully changed to "+pfm.get("projectKey")+", "+pfm.get("projectName")+"."));

            }
            this.initializeChangeProject();
        } else {
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Missing project details."));
            
        }
        return null;
    }
    public String initializeChangeFAT() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfm.put("fatMessage", null);
        pfm.put("fatApplicable", null);
        pfm.put("fatMap",null);
        pfm.put("fundingActionType", null);
        pfm.put("isOriginalAward",null);
        Map<String,String> fatMap=new LinkedHashMap<String,String>();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("setAction");
        exec.execute();
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        BigDecimal planned = new BigDecimal(1); //Planned
        BigDecimal obligated = new BigDecimal(5); //Obligated
        Row r = iter.getCurrentRow();
        boolean fatApplicable = false;
        StringBuilder fatMessage=new StringBuilder();
        if (r != null) {
            //getFATMap
            exec = bindings.getOperationBinding("getFATMap");
            exec.execute();
            fatMap = (Map<String, String>) exec.getResult();
            exec = bindings.getOperationBinding("isActionOriginalAward");
            exec.execute();
            Boolean isOriginalAward=(Boolean)exec.getResult();
            pfm.put("isOriginalAward",isOriginalAward);
            String actionTypeCode = (String) r.getAttribute("ActionTypeCode");
            String commitFlag = (String) r.getAttribute("CommitFlag");
            String fat=(String)r.getAttribute("FundingActionType");
            BigDecimal actionAmtStatus=(BigDecimal)r.getAttribute("ActionAmtStatus");
            if("L".equals(actionTypeCode)) {
                pfm.put("fatMessage", "Funding Type cannot be changed for balance record.");
            }
            else if ("N".equals(actionTypeCode)|| "D".equals(actionTypeCode)) {
                if ("N".equals(commitFlag) &&"N".equals(actionTypeCode)) {
                    fatMap.keySet().retainAll(Arrays.asList("01","02","03","08","17"));
                    fatMessage.append("<li>Only Grant, COOP, Interagency, Contract and Solicitation are valid for Marco/Solicitation parent.</li>");
                } 
                if ("D".equals(actionTypeCode)) {
                    fatMap.keySet().removeAll(Arrays.asList("06","12","17"));
                    fatMessage.append("<li>Mission, Embassy and Solicitation are not valid for detail action.</li>");
                }
                if("Y".equals(commitFlag) &&"N".equals(actionTypeCode)) {
                    fatMap.keySet().remove("17");
                    fatMessage.append("<li>Change action to Macro to enable Solicitation.</li>");
                }
                
                if("Y".equals(commitFlag) && obligated.compareTo(actionAmtStatus) == 0 && !("01".equals(fat)||"03".equals(fat)||"08".equals(fat))) {
                    fatMap.keySet().removeAll(Arrays.asList("01","03","08"));
                    fatMessage.append("<li>Obligated action (funded) cannot be changed to Grant, COOP or Interagency.</li>");
                }
                fatMap.keySet().remove(fat);
                fatApplicable=true;
                if(fatMessage.length()>0) {
                    pfm.put("fatMessage", "<ul class=\"ulPadding\" style=\"padding-left:10px;\">"+fatMessage.toString()+"</ul>");
                }
                if(isOriginalAward) {
                    fatMap.keySet().removeAll(Arrays.asList("06","12","17"));
                    fatMessage.append("<li>This is an original award with mod action(s). Mission, Embassy or Solicitation funding type cannot have mod action(s).</li>");
                    pfm.put("fatWarningMessage", "Changing funding type of original will change funding type of all funded and unfunded mods");
                }

            } else
                pfm.put("fatMessage", "Funding type can only be changed for New or Detail action. To change funding type for any modifications, change original action funding type.");
        }
        pfm.put("fatApplicable", fatApplicable);

        pfm.put("fatMap",fatMap);
        return null;
    }

    public String changeFAT() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        OperationBinding exec2 = bindings.getOperationBinding("isActionOriginalAward");
        exec2.execute();
        Boolean isOriginalAward=(Boolean)exec2.getResult();
        if (pfm.get("fundingActionType") != null) {
            OperationBinding exec = bindings.getOperationBinding("changeFATForAction");
            exec.execute();
            Boolean result = (Boolean) exec.getResult();
            if(!result) {
                exec = bindings.getOperationBinding("Rollback");
                exec.execute();
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Unexpected error while changing funding type. Please try again. If issue persists, contact Abacus Team."));
            }
            else {
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Funding type successfully changed"+(isOriginalAward?" for original and mod actions.":".")));

            }
            this.initializeChangeFAT();
        } else {
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Missing funding type."));
            
        }
        return null;
    }
    public String initializeCategoryActionType() {
        Map<String,String> categoryMap=new LinkedHashMap<String,String>();
        Map<String,String> regularATCMap=new LinkedHashMap<String,String>();
        Map<String,String> macroATCMap=new LinkedHashMap<String,String>();
        Map<String,String> missionATCMap=new LinkedHashMap<String,String>();
        Map<String,String> embassyATCMap=new LinkedHashMap<String,String>();
        Map<String,String> solicitationATCMap=new LinkedHashMap<String,String>();
        
        categoryMap.put("R", "Regular/Mission/Embassy");
        categoryMap.put("M", "Macro/Solicitation");
        
        regularATCMap.put("N", "New");
        regularATCMap.put("M", "Mod");
        regularATCMap.put("U", "Unfunded Mod");
        regularATCMap.put("D", "New Detail");
        regularATCMap.put("Z", "Mod Detail");
        
        macroATCMap.put("N", "New");
        macroATCMap.put("M", "Mod");
        macroATCMap.put("D", "New Detail");
        macroATCMap.put("Z", "Mod Detail");
        
        missionATCMap.put("D", "New Detail");
        missionATCMap.put("Z", "Mod Detail");
        
        embassyATCMap.put("D", "New Detail");
        embassyATCMap.put("Z", "Mod Detail");
        
        solicitationATCMap.put("D", "New Detail");
        solicitationATCMap.put("Z", "Mod Detail");
        
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfm.put("catApplicable", null);
        pfm.put("catMessage", null);
        pfm.put("categoryMap",null);
        pfm.put("macroChoiceATCMap",null);
        pfm.put("regularChoiceATCMap",null);
        pfm.put("macroATCMap",null);
        pfm.put("regularATCMap",null);
        pfm.put("missionATCMap",null);
        pfm.put("embassyATCMap",null);
        pfm.put("solicitationATCMap",null);
        pfm.put("isOriginalAward",null);
        pfm.put("categoryCode",null);
        pfm.put("parentCategoryCode",null);
        pfm.put("actionTypeCode",null);
        pfm.put("isParentRefReadOnly", false);
        
        
        pfm.remove("modActionID");
        pfm.remove("mBudgetFy");
        pfm.remove("mActionSeqNbr");
        pfm.remove("mProjectName");
        pfm.remove("awardee");
        pfm.remove("awardeeCode");
        pfm.remove("mAwardee");
        pfm.remove("mAwardeeCode");
        pfm.remove("mAwardNbr");
        pfm.remove("mActionAmt");
        pfm.remove("mFundingType");
        
        pfm.remove("parentActionID");
        pfm.remove("pOfdaDivisionCode");
        pfm.remove("pBudgetFy");
        pfm.remove("pProjectNbr");
        pfm.remove("pActionSeqNbr");
        pfm.remove("pProjectName");
        pfm.remove("pAwardee");
        pfm.remove("pFundingActionType");
        pfm.remove("pAwardNbr");
        pfm.remove("pActionAmt");
        
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("setAction");
        exec.execute();
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        BigDecimal planned = new BigDecimal(1); //Planned
        BigDecimal obligated = new BigDecimal(5); //Obligated
        Row r = iter.getCurrentRow();
        boolean catApplicable = false;
        StringBuilder catMessage=new StringBuilder();
        if (r != null) {
            exec = bindings.getOperationBinding("isActionOriginalAward");
            exec.execute();
            Boolean isOriginalAward=(Boolean)exec.getResult();
            String atc = (String) r.getAttribute("ActionTypeCode");
            String cf = (String) r.getAttribute("CommitFlag");
            String fat=(String)r.getAttribute("FundingActionType");
            BigDecimal status=(BigDecimal)r.getAttribute("ActionAmtStatus");
            String cb=(String)r.getAttribute("IsCheckbook");
                if("L".equals(atc)) {
                    catMessage.append("Category / Action Type cannot be changed for balance record.");
                }
                else if ("06".equals(fat)||"12".equals(fat)||"17".equals(fat)) {
                    catMessage.append("Category / Action Type cannot be changed for Mission, Embassy and Solicitation.");
                }

                else 
                {
                    //Unfunded Mod
                    if("U".equals(atc) && planned.compareTo(status)!=0) {
                        categoryMap.keySet().removeAll(Arrays.asList("M"));
                        regularATCMap.keySet().retainAll(Arrays.asList("U"));
                        missionATCMap.keySet().retainAll(Arrays.asList("U"));
                        embassyATCMap.keySet().retainAll(Arrays.asList("U"));
                        catMessage.append("<li>Category / Action type for unfunded mod can only be changed when action is planned.</li>");
                    }
                    if(!("D".equals(atc)||"Z".equals(atc))) {
                        catMessage.append("<li>Parent-Detail Set must have same Guidelines Version.</li>");
                    }
                    //Regular Details
                    if ("N".equals(cf) && ("D".equals(atc)||"Z".equals(atc))&& planned.compareTo(status)!=0)
                     {
                        categoryMap.keySet().removeAll(Arrays.asList("M"));
                        regularATCMap.keySet().retainAll(Arrays.asList("D","Z"));
                        catMessage.append("<li>Category / Action Type (other than Detail<->Mod Detail) for Regular Detail/Mod Detail can only be change when action is planned.</li>");
                     }
                    if(planned.compareTo(status)!=0) {
                        //Regular Parent,Details
                        if((("N".equals(atc)||"M".equals(atc)||"U".equals(atc)) && "Y".equals(cf))||
                           (("D".equals(atc)||"Z".equals(atc)) && "N".equals(cf))) {
                            categoryMap.keySet().removeAll(Arrays.asList("M"));
                            catMessage.append("<li>Category can be changed only for planned action.</li>");
                        }
                        //Macro Parent, Details
                      /*  if((("N".equals(atc)||"M".equals(atc)) && "N".equals(cf))||
                           (("D".equals(atc)||"Z".equals(atc)) && "Y".equals(cf))) {
                            categoryMap.keySet().removeAll(Arrays.asList("R"));
                            catMessage.append("<li>Category can be changed only for planned action.</li>");
                        }*/
                        if(!"U".equals(atc))
                        {
                         regularATCMap.keySet().removeAll(Arrays.asList("U"));
                         embassyATCMap.keySet().removeAll(Arrays.asList("U"));
                         missionATCMap.keySet().removeAll(Arrays.asList("U"));
                         catMessage.append("<li>Only planned action can be changed to unfunded mod.</li>");
                        }
                    }
                    if("D".equals(atc)||"Z".equals(atc))
                    {
                        pfm.put("isParentRefReadOnly", true);
                        setParentReference((BigDecimal)r.getAttribute("ParentActionId"));
                        pfm.put("isParentRefReadOnlyMessage", "Parent Reference cannot be changed directly. Try changing the action to New/Mod and then link to new Parent");
                      /*  if("N".equals(cf)) {
                            categoryMap.keySet().removeAll(Arrays.asList("M"));
                        }
                        else
                            categoryMap.keySet().removeAll(Arrays.asList("R"));
                        catMessage.append("<li>Category cannot be changed for Detail action.</li>");*/
                    }
                    if("P".equals(cb)) {
                        regularATCMap.keySet().removeAll(Arrays.asList("D","Z"));
                        macroATCMap.keySet().removeAll(Arrays.asList("D","Z"));
                        missionATCMap.keySet().removeAll(Arrays.asList("D","Z"));
                        embassyATCMap.keySet().removeAll(Arrays.asList("D","Z"));
                        solicitationATCMap.keySet().removeAll(Arrays.asList("D","Z"));
                        
                        catMessage.append("<li>This is a parent action and cannot be Detail or Mod Detail to another action.</li>");
                        //Category cannot be changed for parent
                        if("Y".equals(cf)) {
                            categoryMap.keySet().removeAll(Arrays.asList("M"));
                        }
                        else
                            categoryMap.keySet().removeAll(Arrays.asList("R"));
                        catMessage.append("<li>Category cannot be changed for Parent action.</li>");
                    }
                    if(isOriginalAward){
                        regularATCMap.keySet().removeAll(Arrays.asList("M","U","Z"));
                        macroATCMap.keySet().removeAll(Arrays.asList("M","U","Z"));
                        missionATCMap.keySet().removeAll(Arrays.asList("M","U","Z"));
                        embassyATCMap.keySet().removeAll(Arrays.asList("M","U","Z"));
                        solicitationATCMap.keySet().removeAll(Arrays.asList("M","U","Z"));
                        catMessage.append("<li>Action is an original action and cannot be mod (Mod, Unfunded Mod or Mod Detail) to another action.</li>");
                    }
                    if(("N".equals(atc)||"M".equals(atc)) && planned.compareTo(status)!=0) {
                        regularATCMap.keySet().removeAll(Arrays.asList("U","D","Z"));
                        macroATCMap.keySet().removeAll(Arrays.asList("U","D","Z"));
                        missionATCMap.keySet().removeAll(Arrays.asList("U","D","Z"));
                        embassyATCMap.keySet().removeAll(Arrays.asList("U","D","Z"));
                        solicitationATCMap.keySet().removeAll(Arrays.asList("U","D","Z"));
                        catMessage.append("<li>Only planned action can be changed to Unfunded Mod, Detail or Mod Detail.</li>");
                    }
                    if(("D".equals(atc)||"Z".equals(atc)) && "Y".equals(cf)&& planned.compareTo(status)!=0) {
                        macroATCMap.keySet().removeAll(Arrays.asList("M","N"));
                        regularATCMap.keySet().removeAll(Arrays.asList("U","D","Z"));
                        solicitationATCMap.keySet().removeAll(Arrays.asList("M","N"));
                        missionATCMap.keySet().removeAll(Arrays.asList("U","D","Z"));
                        embassyATCMap.keySet().removeAll(Arrays.asList("U","D","Z"));
                        catMessage.append("<li>Macro details can only be changed to Regular New/Mod or Macro Detail/Mod Detail.</li>");
                    }
                    //Macro Parent is restricted to Grant, Contract, COOP and Interagency
                    List l1=Arrays.asList("01","02","03","08");
                    if(!l1.contains(fat)) {
                        macroATCMap.keySet().removeAll(Arrays.asList("M","N"));
                        solicitationATCMap.keySet().removeAll(Arrays.asList("M","N"));
                        catMessage.append("<li>Macro New and Mod are applicable only to Grant, Contract, COOP and Interagency.</li>");
                    }
                   /* //Remove current action type from available list.
                    //Regular Parent Detail
                    if((("N".equals(atc)||"M".equals(atc)||"U".equals(atc)) && "Y".equals(cf))||
                       (("D".equals(atc)||"Z".equals(atc)) && "N".equals(cf))) {
                         regularATCMap.keySet().remove(atc);
                        
                    }
                    //Macro Parent, Details
                    if((("N".equals(atc)||"M".equals(atc)) && "N".equals(cf))||
                       (("D".equals(atc)||"Z".equals(atc)) && "Y".equals(cf))) {
                        macroATCMap.keySet().remove(atc);
                    }*/
                 pfm.put("macroChoiceATCMap",new LinkedHashMap(macroATCMap));
                 pfm.put("regularChoiceATCMap",new LinkedHashMap(regularATCMap));
                 BigDecimal actionCAT=(BigDecimal)r.getAttribute("ActionCategory");
                 if(actionCAT!=null ) {
                     int cat=actionCAT.intValue();
                     if("D".equals(atc)||"Z".equals(atc))
                     {
                         if(cat!=8)
                             regularATCMap.keySet().removeAll(Arrays.asList("D","Z"));
                         if(cat!=13)
                             missionATCMap.keySet().removeAll(Arrays.asList("D","Z"));
                         if(cat!=14)
                             embassyATCMap.keySet().removeAll(Arrays.asList("D","Z"));
                         if(cat!=9)
                             macroATCMap.keySet().removeAll(Arrays.asList("D","Z"));
                         if(cat!=11)
                             solicitationATCMap.keySet().removeAll(Arrays.asList("D","Z"));
                         catMessage.append("<li>Detail/Mod Detail cannot be changed to another detail.</li>");
                     }
                    switch (cat) {
                    case 8:
                        regularATCMap.keySet().removeAll(Arrays.asList(atc));
                        break;
                    case 13:
                        missionATCMap.keySet().removeAll(Arrays.asList(atc));
                        break;
                    case 14:
                        embassyATCMap.keySet().removeAll(Arrays.asList(atc));
                        break;
                    case 9:
                        macroATCMap.keySet().removeAll(Arrays.asList(atc));
                        break;
                    case 11:
                        solicitationATCMap.keySet().removeAll(Arrays.asList(atc));
                        break;
                    }
                 }
                
                    catApplicable=true;
                    //FAT and Action ID set in Binding
                    exec = bindings.getOperationBinding("setModReference");
                    exec.execute();
                    if(catMessage.length()>0) {
                        pfm.put("catMessage", "<ul class=\"ulPadding\" style=\"padding-left:10px;\">"+catMessage.toString()+"</ul>");
                    }
                }
        }
        pfm.put("catApplicable", catApplicable);
        if(!catApplicable)
            pfm.put("catMessage", catMessage.toString());
        pfm.put("macroATCMap",macroATCMap);
        pfm.put("regularATCMap",regularATCMap);
        pfm.put("missionATCMap",missionATCMap);
        pfm.put("embassyATCMap",embassyATCMap);
        pfm.put("solicitationATCMap",solicitationATCMap);
        pfm.put("categoryMap",categoryMap);
        return null;
    }

    public String changeCAT() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if (pfm.get("actionTypeCode") != null && pfm.get("categoryCode")!=null) {
            DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
            Row r = iter.getCurrentRow();
            //Check Parent Action ID and/or Mod Action ID based on action type selected.
            if(r==null) {
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Unexpected error"));
                return null;
            }
            String atc = (String) pfm.get("actionTypeCode");
            BigDecimal oldpaid=(BigDecimal)r.getAttribute("ParentActionId");
            BigDecimal oldmaid=(BigDecimal)r.getAttribute("ModActionId");
            String newmaid=(String)pfm.get("modActionID");
            String newpaid=(String)pfm.get("parentActionID");
            if(("M".equals(atc)||"U".equals(atc)||"Z".equals(atc)) && (oldmaid==null && newmaid==null)) {
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Missing Mod Reference"));
                return null;
            }
            if(("D".equals(atc)||"Z".equals(atc)) && (oldpaid==null && newpaid==null)) {
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Missing Parent Reference"));
                return null;
            }
            OperationBinding exec = bindings.getOperationBinding("changeCATForAction");
            exec.execute();
            if(!exec.getErrors().isEmpty()) {
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Unexpected error."+exec.getErrors().toString()));
                return null;
            }
            Boolean result = (Boolean) exec.getResult();
            if(!result) {
                exec = bindings.getOperationBinding("Rollback");
                exec.execute();
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Unexpected error. Please try again. If issue persists, contact Abacus Team."));
            }
            else {
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Successfully changed Category / Action Type"));
                

            }
                this.initializeCategoryActionType();
        } else {
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Missing Category or Action Type"));
        }
        return null;
    }

    public void modActionIdReturnLsnr(ReturnPopupEvent returnPopupEvent) {
        Map<String, Object> pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding modIter = bindings.findIteratorBinding("PAModRefView1Iterator");
        RowSetIterator modRSIter = modIter.getRowSetIterator();
        Row r = modRSIter.getCurrentRow();
        if (r != null) {

            BigDecimal modId = (BigDecimal) r.getAttribute("ModActionId");
            if (modId == null)
                modId = (BigDecimal) r.getAttribute("ActionId");
            pfm.put("modActionID", modId);
            pfm.put("mBudgetFy", r.getAttribute("BudgetFy"));
            pfm.put("mActionSeqNbr", r.getAttribute("ActionSeqNbr"));
            pfm.put("mProjectName", r.getAttribute("ProjectName"));
            pfm.put("awardee", r.getAttribute("Awardee"));
            pfm.put("awardeeCode", r.getAttribute("AwardeeCode1"));
            pfm.put("mAwardee", r.getAttribute("Awardee"));
            pfm.put("mAwardeeCode", r.getAttribute("AwardeeCode1"));
            pfm.put("mAwardNbr", r.getAttribute("AwardNbr"));
            pfm.put("mActionAmt", currencyFormat((BigDecimal) r.getAttribute("ActionAmt")));
            pfm.put("mFundingType", r.getAttribute("FundingActionType"));
            pfm.put("fundingActionType", r.getAttribute("FundingActionType"));
    }
    }

    public void parentRefLaunchLsnr(LaunchPopupEvent launchPopupEvent) {
        Map<String, Object> pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        Row r=iter.getCurrentRow();
        exec = bindings.getOperationBinding("SetParentRef");
        String fat = (String) r.getAttribute("FundingActionType");
        String pnbr = (String) r.getAttribute("ProjectNbr");
        String cf= (String)r.getAttribute("CommitFlag");
        String atc = (String) pfm.get("actionTypeCode");
        String categoryCode=(String)pfm.get("categoryCode");
        //Year and action set in binding
        /*         if("Z".equals(atc))
            exec.getParamsMap().put("p_restrict_fat",fat); */
        if(("D".equals(atc)||"Z".equals(atc)) && ("R".equals(categoryCode) ||"N".equals(cf)))
            exec.getParamsMap().put("p_restrict_pnbr",pnbr);
        if(pfm.get("parentCategoryCode")!=null)
            exec.getParamsMap().put("p_restrict_category",pfm.get("parentCategoryCode"));
        else
            exec.getParamsMap().put("p_restrict_category",null);
        exec.execute();
    }

    public void parentRefReturnLnsr(ReturnPopupEvent returnPopupEvent) {
        Row r = null;
        Map<String, Object> pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding parentIter = bindings.findIteratorBinding("PAParentRefView1Iterator");
        RowSetIterator parentRSIter = parentIter.getRowSetIterator();
        r = parentRSIter.getCurrentRow();
        if (r != null) {
            String parentId = (String) r.getAttribute("ParentActionId");
            if (parentId == null)
                parentId = r.getAttribute("ActionId").toString();
            pageFlowScope.put("parentActionID", parentId);
            String divCode = (String) r.getAttribute("OfdaDivisionCode");
            String budgetFy = (String) r.getAttribute("BudgetFy");
            String projectNbr = (String) r.getAttribute("ProjectNbr1");
            pageFlowScope.put("pOfdaDivisionCode", divCode);
            pageFlowScope.put("pBudgetFy", budgetFy);
            pageFlowScope.put("pProjectNbr", projectNbr);
            pageFlowScope.put("pActionSeqNbr", r.getAttribute("ActionSeqNbr"));
            pageFlowScope.put("pProjectName", r.getAttribute("ProjectName"));
            pageFlowScope.put("pAwardee", r.getAttribute("Awardee"));
            pageFlowScope.put("pAwardNbr", r.getAttribute("AwardNbr"));
            pageFlowScope.put("pFundingActionType", r.getAttribute("FundingActionType"));
            pageFlowScope.put("pActionAmt", currencyFormat((BigDecimal) r.getAttribute("ActionAmt")));
        }
    }
  public void setParentReference(BigDecimal parentRef) {
      OperationBinding exec = null;
      DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
      exec = bindings.getOperationBinding("SetParentRef");
      exec.getParamsMap().put("p_id",parentRef);
      exec.execute();
      parentRefReturnLnsr(null);
  }

    public void categoryChangeLsnr(ValueChangeEvent valueChangeEvent) {
        Map<String, Object> pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(!(Boolean)pfm.get("isParentRefReadOnly"))
        {
            pfm.remove("parentActionID");
            pfm.remove("pOfdaDivisionCode");
            pfm.remove("pBudgetFy");
            pfm.remove("pProjectNbr");
            pfm.remove("pActionSeqNbr");
            pfm.remove("pProjectName");
            pfm.remove("pAwardee");
            pfm.remove("pFundingActionType");
            pfm.remove("pAwardNbr");
            pfm.remove("pActionAmt");
        }
    }

    public void actionTypeChangeLsnr(ValueChangeEvent valueChangeEvent) {
        Map<String, Object> pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfm.remove("modActionID");
        pfm.remove("mBudgetFy");
        pfm.remove("mActionSeqNbr");
        pfm.remove("mProjectName");
        pfm.remove("awardee");
        pfm.remove("awardeeCode");
        pfm.remove("mAwardee");
        pfm.remove("mAwardeeCode");
        pfm.remove("mAwardNbr");
        pfm.remove("mActionAmt");
        pfm.remove("mFundingType");
        if(!(Boolean)pfm.get("isParentRefReadOnly"))
        {
            pfm.remove("parentActionID");
            pfm.remove("pOfdaDivisionCode");
            pfm.remove("pBudgetFy");
            pfm.remove("pProjectNbr");
            pfm.remove("pActionSeqNbr");
            pfm.remove("pProjectName");
            pfm.remove("pAwardee");
            pfm.remove("pFundingActionType");
            pfm.remove("pAwardNbr");
            pfm.remove("pActionAmt");
        }
    }

    public void projectDisclosureLnsr(DisclosureEvent disclosureEvent) {
        this.initializeChangeProject();
    }

    public void fundingTypeDisclosureLnsr(DisclosureEvent disclosureEvent) {
        this.initializeChangeFAT();
    }

    public void CategoryActionTypeDisclosureLsnr(DisclosureEvent disclosureEvent) {
        this.initializeCategoryActionType();
    }

    public String initializeCATChangeIfNeeded() {
        Map<String, Object> pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        //if(!"ACTIONTYPE".equals(pfm.get("selectCustomizeOption"))) {
            String actionTypeCode = (String) pfm.get("actionTypeCode");
            String categoryCode = (String) pfm.get("categoryCode");
            String parentCategoryCode = (String) pfm.get("parentCategoryCode");
            String parentCategoryName = (String) pfm.get("parentCategoryName");
            this.initializeCategoryActionType();
            pfm.put("actionTypeCode", actionTypeCode);
            pfm.put("categoryCode", categoryCode);
            pfm.put("parentCategoryCode", parentCategoryCode);
            pfm.put("parentCategoryName", parentCategoryName);
            pfm.put("selectCustomizeOption","ACTIONTYPE");
            
       // }
        return null;
    }

    public String resetAll() {
        this.initializeChangeProject();
        this.initializeChangeFAT();
        this.initializeCategoryActionType();
        return null;
    }
}
