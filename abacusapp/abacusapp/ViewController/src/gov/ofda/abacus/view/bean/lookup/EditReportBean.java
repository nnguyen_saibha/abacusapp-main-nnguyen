package gov.ofda.abacus.view.bean.lookup;

import gov.ofda.abacus.view.base.JSFUtils;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.PopupCanceledEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.*;

public class EditReportBean {
    private RichPopup addOrEditReportPopup;

    public EditReportBean() {
    }

    public void deleteDialogLsnr(DialogEvent dialogEvent) {
        //Key rowKey = null;
        if (DialogEvent.Outcome.yes == dialogEvent.getOutcome()) {

            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding abacusReportIter = bindings.findIteratorBinding("AbacusReportsView1Iterator");
            
            OperationBinding exec = bindings.getOperationBinding("Delete1");
            if (exec.isOperationEnabled()) {
                exec.execute();
                exec = bindings.getOperationBinding("Commit");
                exec.execute();
                if (exec.getErrors().isEmpty()) {


                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Deleted successfully", "");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                } else {
                    exec = bindings.getOperationBinding("Rollback");
                    exec.execute();
                    
                    FacesMessage message =
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Cannot be Deleted as child record exists", "");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                }
                
            }
        }
        return;
    }


    public void showSaveSuccessMessage() {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully", "");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void showCancelledMessage() {

        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancelled successfully", "");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void createReportParamActionLsnr(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();

        OperationBinding exec = bindings.getOperationBinding("Create1");
        exec.execute();
        if (exec.getErrors().isEmpty()) {
            exec = bindings.getOperationBinding("getAbacusReportsParamsSeqIdlatest");
            exec.execute();
            Integer maxValue = (Integer) exec.getResult();
            DCIteratorBinding iter = bindings.findIteratorBinding("AbacusReportsParamsView1Iterator");
            Row r = iter.getCurrentRow();
            if (r != null && maxValue != null) {
                r.setAttribute("SeqId", maxValue);
            }
        }
    }

    public void addNewReportActionListener(ActionEvent actionEvent) {
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("addNew", "true");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding createInsertExec = bindings.getOperationBinding("CreateInsert");
        createInsertExec.execute();
        if (createInsertExec.getErrors().isEmpty()) {
            DCIteratorBinding iter = bindings.findIteratorBinding("AbacusReportsView1Iterator");
            Row r = iter.getCurrentRow();
            r.setAttribute("Isnew", "Y");
        }
        RichPopup popup = (RichPopup) this.getAddOrEditReportPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);

    }

    public void setAddOrEditReportPopup(RichPopup addOrEditReportPopup) {
        this.addOrEditReportPopup = addOrEditReportPopup;
    }

    public RichPopup getAddOrEditReportPopup() {
        return addOrEditReportPopup;
    }

    public void editReportActionListener(ActionEvent actionEvent) {
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("addNew", "false");
        RichPopup popup = (RichPopup) this.getAddOrEditReportPopup();
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        popup.show(ph);
    }


    public void commitActionLsnr(ActionEvent actionEvent) {

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding abacusReportIter = bindings.findIteratorBinding("AbacusReportsView1Iterator");
        Key rowKey = null;
        if (abacusReportIter.getCurrentRow() != null) {
            rowKey = abacusReportIter.getCurrentRow().getKey();
            RowSetIterator rsi = abacusReportIter.getRowSetIterator();

            OperationBinding rollbackOper = bindings.getOperationBinding("Commit");
            rollbackOper.execute();
            if (rollbackOper.getErrors().isEmpty()) {

                if (rowKey != null && !rowKey.isNull() && rsi.findByKey(rowKey, 1).length > 0) {
                    abacusReportIter.setCurrentRowWithKey(rowKey.toStringFormat(true));
                }
                RichPopup popup = (RichPopup) this.getAddOrEditReportPopup();
                popup.hide();
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }
    }


    public void rollbackChangesActionLsnr(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding abacusReportIter = (DCIteratorBinding) bindings.get("AbacusReportsView1Iterator");
        Key rowKey = null;
        if (abacusReportIter.getCurrentRow() != null) {
            rowKey = abacusReportIter.getCurrentRow().getKey();
            RowSetIterator rsi = abacusReportIter.getRowSetIterator();

            OperationBinding rollbackOper = bindings.getOperationBinding("Rollback");
            rollbackOper.execute();
            if (rollbackOper.getErrors().isEmpty()) {

                if (rowKey != null && !rowKey.isNull() && rsi.findByKey(rowKey, 1).length > 0) {
                    abacusReportIter.setCurrentRowWithKey(rowKey.toStringFormat(true));
                }
                RichPopup popup = (RichPopup) this.getAddOrEditReportPopup();
                popup.hide();
                /* AdfFacesContext.getCurrentInstance().addPartialTarget(popupCanceledEvent.getComponent().getParent());*/
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancelled successfully", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }
    }
}
