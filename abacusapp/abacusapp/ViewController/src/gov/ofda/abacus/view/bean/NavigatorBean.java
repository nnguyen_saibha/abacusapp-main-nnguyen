package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.view.base.JSFUtils;
import gov.ofda.abacus.view.base.UIControl;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.event.QueryEvent;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class NavigatorBean extends UIControl implements Serializable {
    @SuppressWarnings("compatibility:3304034513956110211")
    private static final long serialVersionUID = -1602923911778903768L;
    private String selectNav = "Action";

    public void setSelectNav(String selectNav) {
        this.selectNav = selectNav;
    }

    public String getSelectNav() {
        return selectNav;
    }

    public NavigatorBean() {
    }

    public void commonSearchQueryLsnr(QueryEvent queryEvent) {
        invokeEL("#{bindings.commonsearchQuery.processQuery}", new Class[] { QueryEvent.class }, new Object[] {
                 queryEvent });
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("setOtherViewsCriteria");
        exec.execute();
        exec = bindings.getOperationBinding("publishEvent");
        exec.execute();
        String tmp="pt1:l1";
        switch (selectNav) {
        case "Action":
            tmp="pt1:l1";
            break;
        case "Project":
            tmp="pt1:l5";
            break;
        case "Award":
            tmp="pt1:l21";
            break;
        case "Document":
            tmp="pt1:l4";
            break;
        }
        FacesContext context=FacesContext.getCurrentInstance();
        ExtendedRenderKitService service =Service.getRenderKitService(context, ExtendedRenderKitService.class);
        service.addScript(context, "var link = AdfPage.PAGE.findComponentByAbsoluteId('"+tmp+"'); AdfActionEvent.queue(link,true);");
    }
}
