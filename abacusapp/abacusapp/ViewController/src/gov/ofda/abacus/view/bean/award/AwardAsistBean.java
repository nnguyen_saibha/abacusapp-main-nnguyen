package gov.ofda.abacus.view.bean.award;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.domain.DBSequence;
import oracle.jbo.uicli.binding.JUCtrlListBinding;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

public class AwardAsistBean {
    
    private  Integer counterTotalAOR = 0;
    private Integer counterSetAOR = 0;
    private Integer counterTotalGU = 0;
    private Integer counterSetGU = 0;
    private Map<String, Map<String, String>> selectedAllList=new HashMap<String, Map<String, String>>();
    private Map<String, Map<String, String>> selectedAORList=new HashMap<String, Map<String, String>>();
    private Map<String, Map<String, String>> selectedGUList=new HashMap<String, Map<String, String>>();
    private Map<String, Map<String, String>> selectedTotalAllList=new HashMap<String, Map<String, String>>();
    private Map<String, Map<String, String>> selectedTotalAORList=new HashMap<String, Map<String, String>>();
    private Map<String, Map<String, String>> selectedTotalGUList=new HashMap<String, Map<String, String>>();
    private UIComponent allDocumentsTable;
    private UIComponent aorDocumentsTable;
    private UIComponent guDocumentsTable;


    public void setCounterTotalAOR(Integer counterTotalAOR) {
        this.counterTotalAOR = counterTotalAOR;
    }

    public Integer getCounterTotalAOR() {
        return counterTotalAOR;
    }

    public void setCounterSetAOR(Integer counterSetAOR) {
        this.counterSetAOR = counterSetAOR;
    }

    public Integer getCounterSetAOR() {
        return counterSetAOR;
    }

    public void setCounterTotalGU(Integer counterTotalGU) {
        this.counterTotalGU = counterTotalGU;
    }

    public Integer getCounterTotalGU() {
        return counterTotalGU;
    }

    public void setCounterSetGU(Integer counterSetGU) {
        this.counterSetGU = counterSetGU;
    }

    public Integer getCounterSetGU() {
        return counterSetGU;
    }
   
    
    public AwardAsistBean() {
    }



    
    public Map<String, Map<String, String>> getSelectedAllList() {
        //if(size>0)
            return selectedAllList;
       // else {
            //Iterate and add
      //  }
    }

    public void setSelectedAllList(Map<String, Map<String, String>> selectedAllList) {
        this.selectedAllList = selectedAllList;
    }

    public void setSelectedAORList(Map<String, Map<String, String>> selectedAORList) {
        this.selectedAORList = selectedAORList;
    }

    public Map<String, Map<String, String>> getSelectedAORList() {
        return selectedAORList;
    }

    public void setSelectedGUList(Map<String, Map<String, String>> selectedGUList) {
        this.selectedGUList = selectedGUList;
    }

    public Map<String, Map<String, String>> getSelectedGUList() {
        return selectedGUList;
    }


    public void setSelectedTotalAllList(Map<String, Map<String, String>> selectedTotalAllList) {
        this.selectedTotalAllList = selectedTotalAllList;
    }

    public Map<String, Map<String, String>> getSelectedTotalAllList() {
        return selectedTotalAllList;
    }

    public void setSelectedTotalAORList(Map<String, Map<String, String>> selectedTotalAORList) {
        this.selectedTotalAORList = selectedTotalAORList;
    }

    public Map<String, Map<String, String>> getSelectedTotalAORList() {
        return selectedTotalAORList;
    }

    public void setSelectedTotalGUList(Map<String, Map<String, String>> selectedTotalGUList) {
        this.selectedTotalGUList = selectedTotalGUList;
    }

    public Map<String, Map<String, String>> getSelectedTotalGUList() {
        return selectedTotalGUList;
    }

    public void selectFileChgLsnr(ValueChangeEvent vce) {

        selectFile(vce,selectedAllList);
    }
    
    
    public void selectAORFileChgLsnr(ValueChangeEvent vce) {
        selectFile(vce,selectedAORList);      
    }
    
    
    public void selectGUFileChgLsnr(ValueChangeEvent vce) {
          selectFile(vce,selectedGUList);
       
      }


    @SuppressWarnings("unchecked")
    private void selectFile(ValueChangeEvent vce,Map selectedList) {
            Map attrs=vce.getComponent().getAttributes();
            Map<String, String> k = new HashMap<String, String>();
         String docId= attrs.get("docId").toString();
         String sourceType  = attrs.get("sourceType").toString();
        String fileName  = ((String)attrs.get("fileName"));
        k.put("docId", docId);
        k.put("sourceType", sourceType);
        k.put("fileName", fileName);
        k.put("sec",((String)attrs.get("sec")) );
        k.put("subsec",((String)attrs.get("subsec")) );
        String Id = sourceType+"-"+docId;
        //  Map<String, Map<String, String>> selectedList =  new HashMap<String, Map<String, String>>();
        
           if((Boolean)vce.getNewValue()) {
               selectedList.put(Id,k);
           }
           else {
               selectedList.remove(Id);
           }                   
        }
  
    
    
    

    public void aorFileUploadChngLsnr(ValueChangeEvent vce){
        
        if((Boolean)vce.getNewValue()) {
            counterSetAOR++;
            }    else{
                counterSetAOR--;
            }
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
     AttributeBinding attr = (AttributeBinding)bindings.getControlBinding("AsistAorStatus");
        if (counterTotalAOR == counterSetAOR){
       
        attr.setInputValue(60);
        }else{
            attr.setInputValue(59);
        }
        
    }
    
    public void guFileUploadChngLsnr(ValueChangeEvent vce){
        if((Boolean)vce.getNewValue()) {
            counterSetGU++;
            }    else{
                counterSetGU--;
            }
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        AttributeBinding attr = (AttributeBinding)bindings.getControlBinding("AsistGuStatus");
        if (counterTotalGU == counterSetGU){
        
        attr.setInputValue(60);
        }else{
            attr.setInputValue(59);
        }
    }
    
    
    
    
    
    
    
    public void saveAwardAsistDetails() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec1 = bindings.getOperationBinding("saveASISTDetails");
        if(exec1.isOperationEnabled()){
            exec1.execute();     
        }
        OperationBinding exec = bindings.getOperationBinding("Commit"); 
         if(exec.isOperationEnabled()){
            exec.execute();
           OperationBinding exec3 = bindings.getOperationBinding("Execute");
            exec3.execute();
            
         OperationBinding  exec2 = bindings.getOperationBinding("Execute1");
            exec2.execute();
            List errors = exec.getErrors();
            //System.out.println("******** Errors"+errors.toString());
            if(exec.getErrors().isEmpty() ){
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }else{
             if(!exec.getErrors().isEmpty() ){
                 FacesMessage message =
                     new FacesMessage(FacesMessage.SEVERITY_INFO, "Unable to Save Changes", "");
                 FacesContext.getCurrentInstance().addMessage(null, message);
             } else{
            FacesMessage message =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "No changes to save", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        } 
        
    }
    
    
    public void cancelAwardAsistDetails() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Rollback");
        
      exec.execute();
            if(exec.getErrors().isEmpty()){
                FacesMessage message =
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancelled successfully", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
    
    }

    public void setInitialValues() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
        if(exec.isOperationEnabled()){
            exec.execute();     
        }
        
        exec = bindings.getOperationBinding("ExecuteWithParams1");
        if(exec.isOperationEnabled()){
            exec.execute();     
        }
        
        exec = bindings.getOperationBinding("counterListOnASIST");
        
        if(exec.isOperationEnabled()){
            List<Object> counterList = (List<Object>) exec.execute();  
            this.counterTotalAOR = (Integer)counterList.get(0);
            this.counterSetAOR = (Integer)counterList.get(1);
            this.counterTotalGU = (Integer)counterList.get(2);
            this.counterSetGU = (Integer)counterList.get(3);
            this.selectedTotalAllList  = (Map<String, Map<String, String>>)counterList.get(4);
            this.selectedTotalAORList = (Map<String, Map<String, String>>) counterList.get(5);
            this.selectedTotalGUList = (Map<String, Map<String, String>>) counterList.get(6);
        }
        
        pageFlowMap.put("topHeight", "160");
        pageFlowMap.put("detailsDisclosed",true);
        
        
    }

    public void setAllDocumentsTable(UIComponent  allDocumentsTable) {
        this.allDocumentsTable = allDocumentsTable;
    }

    public UIComponent getAllDocumentsTable() {
        return allDocumentsTable;
    }

    public void setAorDocumentsTable(UIComponent  aorDocumentsTable) {
        this.aorDocumentsTable = aorDocumentsTable;
    }

    public UIComponent getAorDocumentsTable() {
        return aorDocumentsTable;
    }

    public void setGuDocumentsTable(UIComponent guDocumentsTable) {
        this.guDocumentsTable = guDocumentsTable;
    }

    public UIComponent getGuDocumentsTable() {
        return guDocumentsTable;
    }
    
    public void viewDisclosureLnsr(DisclosureEvent disclosureEvent) {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(disclosureEvent.isExpanded()){
            pageFlowMap.put("topHeight", "160");
            pageFlowMap.put("detailsDisclosed",true);
            
        }else{
            pageFlowMap.put("topHeight", "65");
            pageFlowMap.put("detailsDisclosed",false);
        }
    }
}
