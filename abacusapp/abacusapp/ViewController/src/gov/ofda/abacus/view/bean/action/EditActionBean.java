package gov.ofda.abacus.view.bean.action;


import gov.ofda.abacus.model.abacusapp.pojo.CustomPurposeItem;
import gov.ofda.abacus.view.base.AbacusConstants;
import gov.ofda.abacus.view.base.JSFUtils;
import gov.ofda.abacus.view.base.UIControl;
import gov.ofda.abacus.view.base.UIControl.GrowlType;
import gov.ofda.abacus.view.bean.AppBean;
import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import javax.swing.Popup;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.TaskFlowContext;
import oracle.adf.controller.TaskFlowId;
import oracle.adf.controller.TaskFlowTrainModel;
import oracle.adf.controller.TaskFlowTrainStopModel;
import oracle.adf.controller.ViewPortContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.model.TrainStopModel;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.event.DisclosureEvent;
import org.apache.myfaces.trinidad.event.PollEvent;
import org.apache.myfaces.trinidad.event.ReturnEvent;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.ComponentReference;
import org.apache.myfaces.trinidad.util.Service;


/**
 * Used by Edit Action Task flow for initialization, Save, Cancel and Post Commit.
 */
public class EditActionBean extends UIControl implements Serializable {
    @SuppressWarnings("compatibility:-1586541009179538502")
    private static final long serialVersionUID = 1L;
    public static final String IGNORE_SECTOR_DETAILS = "ignoreSectorDetails";
    public static final String IGNORE_CABLES = "ignoreCables";
    public static final String IGNORE_PROPOSAL_RECEIVED = "ignoreProposalReceived";
    public static final String IGNORE_COSTSHEET = "ignoreCostsheet";
    public static final String IGNORE_PROPOSAL_REVIEW = "ignoreProposalReview";
    public static final String IGNORE_ACTION_REVIEW = "ignoreActionReview";
    public static final String IGNORE_APPROVED_ACTION = "ignoreApprovedAction";
    public static final String IGNORE_AWARD_PACKAGE = "ignoreAwardPackage";
    public static final String IGNORE_FUNDING = "ignoreFunding";
    public static final String IGNORE_FUNDS = "ignoreFunds";
    public static final String IGNORE_AWARD_TRACKING = "ignoreAwardTracking";
    public static final String IGNORE_M_AND_E = "ignoreMAndE";
    public static final String IGNORE_POST_AWARD = "ignorePostAward";
    public static final String IGNORE_PROJECT = "ignoreProject";
    public static final String IGNORE_PLANNING = "ignorePlanning";
    public static final String IGNORE_SECTORS = "ignoreSectors";
    public static final String IGNORE_DOCUMENTS = "ignoreDocuments";
    public static final String IGNORE_NEG_MEMO = "ignoreNegMemo";
    public static final String IGNORE_APPLICATION_REVIEW_DOCS = "ignoreApplicationReviewDocuments";
    public static final String IGNORE_AWARDPKG_GU_DOCS = "ignoreAwardPkgGUDocuments";
    public static final String IGNORE_PURPOSE = "ignorePurpose";
    private  ComponentReference warningPopup;
    private int delay=1;
    Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
    private String taskFlowId = "/WEB-INF/tflows/action/call-edit-action-tf.xml#call-edit-action-tf";

    public EditActionBean() {
    }

    /**
     * Depending on Action Type, certain task flow train stops are ignored.
     * Commit Flag, Action Type, Funding Type and Checkbook are used to determine which stop are valid and which can be ignored.
     */
    public void editActionInitializer() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.execute();
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        //Disabling by defaulst
        pfm.put(IGNORE_SECTOR_DETAILS, true);
        pfm.put(IGNORE_CABLES, true);
        pfm.put(IGNORE_PROPOSAL_RECEIVED, true);
        pfm.put(IGNORE_COSTSHEET, true);    
        pfm.put(IGNORE_PROPOSAL_REVIEW, true);
        pfm.put(IGNORE_APPLICATION_REVIEW_DOCS, true);
        pfm.put(IGNORE_ACTION_REVIEW, true);
        pfm.put(IGNORE_APPROVED_ACTION, true);
        pfm.put(IGNORE_AWARD_PACKAGE, true);
        pfm.put(IGNORE_AWARDPKG_GU_DOCS, true);
        pfm.put(IGNORE_FUNDING, true);
        pfm.put(IGNORE_FUNDS, true);
        pfm.put(IGNORE_AWARD_TRACKING, true);
        pfm.put(IGNORE_M_AND_E,true);
        pfm.put(IGNORE_POST_AWARD, true);
        pfm.put(IGNORE_NEG_MEMO, true);
        pfm.put(IGNORE_DOCUMENTS, true);

        //Valid for all actions
        pfm.put(IGNORE_PROJECT, false);
        pfm.put(IGNORE_PLANNING, false);
        pfm.put(IGNORE_SECTORS, false);
        pfm.put(IGNORE_PURPOSE, true);

        Row rw = iter.getCurrentRow();
        if (rw != null) {
            String commitFlag = (String) rw.getAttribute("CommitFlag");
            String actionType = (String) rw.getAttribute("ActionTypeCode");
            String fundingType = (String) rw.getAttribute("FundingActionType");
            String checkbook = (String) rw.getAttribute("IsCheckbook");
            BigDecimal pgRevId=(BigDecimal)rw.getAttribute("PgRevId");
            Integer budgetFY=(Integer)rw.getAttribute("BudgetFy");
            Timestamp prd = (Timestamp) rw.getAttribute("ProposalReviewDate");
            String isPFReview = (String) rw.getAttribute("IsProceedWithReview");
            /****************************Cables******************************************/
            //Valid for Mission and Embassy
            if ("Y".equals(commitFlag) && ("06".equals(fundingType) || "12".equals(fundingType)))
                pfm.put(IGNORE_CABLES, false);

            /****************************Prposal Received/Contribution(Cost Sheet)******************************************/
            // Funded Grant COOP
            if ("Y".equals(commitFlag) && ("01".equals(fundingType) || "03".equals(fundingType)))
            {
                pfm.put(IGNORE_PROPOSAL_RECEIVED, false);
                pfm.put(IGNORE_COSTSHEET, false); 
            }
            if ("Y".equals(commitFlag) && "02".equals(fundingType))
            {
                pfm.put(IGNORE_COSTSHEET, false); 
            }
            //Details Grant COOP
            if ("N".equals(commitFlag) && ("01".equals(fundingType) || "03".equals(fundingType)) &&
                ("D".equals(actionType) || "Z".equals(actionType) || "L".equals(actionType)))
                pfm.put(IGNORE_PROPOSAL_RECEIVED, false);
            
            /* Valid for funded details only, Removed action Review as per Lalitha Sep 4th 2015*/
            /*if ( (!("01".equals(fundingType) || "03".equals(fundingType)))&&("N".equals(commitFlag))&& ("Z".equals(actionType) || "D".equals(actionType)))
                pfm.put(IGNORE_ACTION_REVIEW, false);*/
            /****************************Prposal Review, Approved Action, Funding, Funds******************************************/
            //Valid except for Solicitation Balance and Macro Parent
            if (!("Y".equals(commitFlag) && "L".equals(actionType) && "17".equals(fundingType)) &&
                !("N".equals(commitFlag) && ("N".equals(actionType) || "M".equals(actionType)))) {
                if ("01".equals(fundingType) || "03".equals(fundingType)) {
                    if("Y".equals(commitFlag))
                    {
                        pfm.put(IGNORE_PROPOSAL_REVIEW, false);
                        pfm.put(IGNORE_AWARD_PACKAGE, false);
                        
                        if(budgetFY<2020)
                            pfm.put(IGNORE_POST_AWARD, false);  
                        
                        
                        
                        /* Application Review Documents and GU Documents will be enabled similar to proposal review and award package*/
                        pfm.put(IGNORE_APPLICATION_REVIEW_DOCS,false);    
                        pfm.put(IGNORE_AWARDPKG_GU_DOCS,false);
                    }
                } 

                pfm.put(IGNORE_APPROVED_ACTION, false);
                pfm.put(IGNORE_FUNDING, false);


                if (!"U".equals(actionType))
                    pfm.put(IGNORE_FUNDS, false);
                else
                    pfm.put(IGNORE_PROPOSAL_REVIEW, true);
            }
            /****************************Purpose******************************************/
            if (("01".equals(fundingType) || "03".equals(fundingType))&& (pgRevId!=null && pgRevId.longValue()>3)) {
                pfm.put(IGNORE_PURPOSE, false);
                
            }
            /****************************Award Tracking******************************************/
            if ("Y".equals(commitFlag) &&
                ("01".equals(fundingType) || "03".equals(fundingType) || "08".equals(fundingType))) {
                pfm.put(IGNORE_AWARD_TRACKING, false);
                if (!"L".equals(actionType))
                    pfm.put(IGNORE_NEG_MEMO, false);
            }
            /****************************M & E******************************************/
            if ("Y".equals(commitFlag) && pgRevId!=null && pgRevId.longValue()>=2 &&
                ("01".equals(fundingType) || "03".equals(fundingType) || "08".equals(fundingType))) {
                pfm.put(IGNORE_M_AND_E, false);
            }
            /****************************Sector Details******************************************/
            if ("01".equals(fundingType) || "03".equals(fundingType) || "08".equals(fundingType) ||
                "06".equals(fundingType) || "12".equals(fundingType) || "02".equals(fundingType))
                pfm.put(IGNORE_SECTOR_DETAILS, false);
            if ("Y".equals(commitFlag))
                pfm.put("proposalLabel", "Application ");
            else
                pfm.put("proposalLabel", "Detail ");

            if (!"L".equals(actionType))
                pfm.put(IGNORE_DOCUMENTS, false);
            if("N".equals(pfm.get("isEditMode")))
                pfm.put("topHeight", 165);
            else
                pfm.put("topHeight",170);
            pfm.put("detailsDisclosed",true);
        }
    }

    public String setActionID() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        SecurityContext scntx = ADFContext.getCurrent().getSecurityContext();
        OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.execute();
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        if (iter.getEstimatedRowCount() > 0)
        {
            Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            exec = bindings.getOperationBinding("setPAEditMode");
            if("N".equals(pfm.get("isEditMode")))
            {
                exec.getParamsMap().put("readOnly","Y");
            }
            else
            {
                if(scntx.isUserInRole("APP_USER"))
                    pfm.put("isEditMode","Y");
                exec.getParamsMap().put("readOnly","N");
            }
            exec.execute();
            return "next";
        }
        return "close";
    }

    /**
     * Called during task flow initilization to keep track of certain attributes which are used in
     * post commit stage for email processing.
     */
    public void setInitialTrackingValues() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        Row r = iter.getCurrentRow();
        if (r != null) {
            Timestamp prd = (Timestamp) r.getAttribute("ProposalReviewDate");
            pfm.put("oldPRD", prd);
            pfm.put("isCheckbook", r.getAttribute("IsCheckbook"));
            pfm.put("oldPlannedAmt", r.getAttribute("PlannedAmt"));
            pfm.put("oldApprovedAmt", r.getAttribute("ApprovedAmt"));
            pfm.put("oldApprovedDate", r.getAttribute("ApprovedDate"));
            pfm.put("oldOfdaControlNbr", r.getAttribute("OfdaControlNbr"));
            pfm.put("oldPhDistributedAmt", r.getAttribute("PhDistributedAmt"));
            pfm.put("oldPrSpecialist", r.getAttribute("PrSpecialist"));
            pfm.put("oldSpecialist", r.getAttribute("Specialist"));
            pfm.put("oldOaa", r.getAttribute("Oaa"));
            pfm.put("oldContractOfficer", r.getAttribute("ContractOfficer"));
            pfm.put("oldCommittedAmt",r.getAttribute("CommitedAmt"));
            pfm.put("oldObligatedAmt", r.getAttribute("ObligatedAmt"));
            pfm.put("oldCtoAwardLtrSentDate", r.getAttribute("CtoAwardLtrSentDate"));

            pfm.put("oldPkgCompleteInSpDate", r.getAttribute("PkgCompleteInSpDate"));
          
            pfm.put("oldPkgComplete", r.getAttribute("PkgComplete"));
            pfm.put("oldPkgCompleteDate", r.getAttribute("PkgCompleteDate"));
            pfm.put("oldPkgCompleteComments", r.getAttribute("PkgCompleteComments"));
            
            pfm.put("oldRevPkgCompleteInSpDate", r.getAttribute("RevPkgCompleteInSpDate"));
            
            pfm.put("oldIsRevPkgComplete", r.getAttribute("IsRevPkgComplete"));
            pfm.put("oldRevPkgIncompleteDate", r.getAttribute("RevPkgIncompleteDate"));
            pfm.put("oldRevPkgIncompleteComments", r.getAttribute("RevPkgIncompleteComments"));
            
            pfm.put("oldCompletePkgReviewedDate", r.getAttribute("CompletePkgReviewedDate"));
            String isCostsheetApplicable = (String)r.getAttribute("IsCostsheetApplicable");            
            pfm.put("isCostsheetApplicable",isCostsheetApplicable);
            pfm.put("oldIsCostsheetApplicable",isCostsheetApplicable);
            
            pfm.put("oldClr1UserId", r.getAttribute("Clr1UserId"));
            pfm.put("oldClr1DateSentForReview", r.getAttribute("Clr1DateSentForReview"));
            pfm.put("oldClr1Date", r.getAttribute("Clr1Date"));
            
            pfm.put("oldClr2TypeId", r.getAttribute("Clr2TypeId"));
            pfm.put("oldClr2UserId", r.getAttribute("Clr2UserId"));
            pfm.put("oldClr2DateSentForReview", r.getAttribute("Clr2DateSentForReview"));
            pfm.put("oldClr2Date", r.getAttribute("Clr2Date"));
            
            pfm.put("oldIssueLtr1RecdDate", r.getAttribute("IssueLtr1RecdDate"));
            pfm.put("oldIssueLtr1SentDate", r.getAttribute("IssueLtr1SentDate"));
            
            pfm.put("oldIssueLtr2RecdDate", r.getAttribute("IssueLtr2RecdDate"));
            pfm.put("oldIssueLtr2SentDate", r.getAttribute("IssueLtr2SentDate"));
            
            pfm.put("oldIssueLtr3RecdDate", r.getAttribute("IssueLtr3RecdDate"));
            pfm.put("oldIssueLtr3SentDate", r.getAttribute("IssueLtr3SentDate"));

        }
    }
    public void autoCloseMessage(String clientID) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExtendedRenderKitService erks = Service.getRenderKitService(ctx, ExtendedRenderKitService.class);
         StringBuilder builder= new StringBuilder();
         builder.append("setTimeout(function(){AdfPage.PAGE.clearMessages('"+clientID+"');},1234)");
         erks.addScript(ctx, builder.toString());
    }
    public void checkBeforeSaveChanges(ActionEvent ae) {
        List<String> errorMsg = new ArrayList<String>();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        SecurityContext scntx = ADFContext.getCurrent().getSecurityContext();
        Boolean isCommitEnabled=(Boolean)JSFUtils.resolveExpression("#{bindings.Commit.enabled}");
        Boolean isLegacyCommitEnabled=(Boolean)JSFUtils.resolveExpression("#{bindings.LegacyCommit.enabled}");
        if(!(isCommitEnabled || isLegacyCommitEnabled)) {
            //pfm.put("showMessage","No changes to save");
            //FacesContext fc = FacesContext.getCurrentInstance();
           // fc.addMessage(ae.getComponent().getClientId(),new FacesMessage(FacesMessage.SEVERITY_INFO, null, "No changes to save"));
           // autoCloseMessage(ae.getComponent().getClientId());
           addToGrowl(GrowlType.notice,"No changes to save",0,5000);
            return;
        }
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        Row r = iter.getCurrentRow();
        OperationBinding exec = null;
        if (r != null) {
                String actionType = (String) r.getAttribute("ActionTypeCode");
                /*****Pre Commit Checklist********/
                //Sector amount check if not unfunded mod
                if (!"U".equals(actionType)) {
                    exec = bindings.getOperationBinding("checkSectorAmounts");
                    exec.execute();
                    String result = (String) exec.getResult();
                    if (result != null)
                        errorMsg.add(result);
                    
                    exec = bindings.getOperationBinding("checkPABudgetPlannedAmt");
                    exec.execute();
                    result = (String) exec.getResult();
                    if (result != null)
                        errorMsg.add(result);
                    
                    exec = bindings.getOperationBinding("checkPABudgetApprovedAmt");
                    exec.execute();
                    result = (String) exec.getResult();
                    if (result != null)
                        errorMsg.add(result);
                
                }
                String isCostsheetApplicable = (String) r.getAttribute("IsCostsheetApplicable");
                BigDecimal paxActivitiesId = (BigDecimal) r.getAttribute("PaxActivitiesId");
                BigDecimal contributionLinktypeId = (BigDecimal) r.getAttribute("ContributionLinktypeId");
                
                if( "Y".equals(isCostsheetApplicable) && paxActivitiesId == null)
                errorMsg.add("Please select Contribution # in Planning Tab.");
                 if( "Y".equals(isCostsheetApplicable) && contributionLinktypeId == null)
                    errorMsg.add("Please select Contribution Link Type in Planning Tab.");
                /*********Commit******************/
                if (errorMsg.size()== 0)
                {
                    StringBuilder warningMsg=new StringBuilder();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    cal.set(Calendar.HOUR_OF_DAY, 0);
                    cal.set(Calendar.MINUTE, 0);
                    cal.set(Calendar.SECOND, 0);
                    cal.set(Calendar.MILLISECOND, 0);
                    Date today = cal.getTime();
                    //Check for warnings
                    Integer budgetFY = (Integer) r.getAttribute("BudgetFy");
                    String fundingType = (String) r.getAttribute("FundingActionType");
                    String commitFlag = (String) r.getAttribute("CommitFlag");
                    String checkbook = (String) r.getAttribute("IsCheckbook");
                    String isPFReview = (String) r.getAttribute("IsProceedWithReview");
                    Timestamp prd = (Timestamp) r.getAttribute("ProposalReviewDate");
                    BigDecimal approvedAmt = (BigDecimal) r.getAttribute("ApprovedAmt");
                    BigDecimal distributedAmt = (BigDecimal) r.getAttribute("PhDistributedAmt");
                    Timestamp prdOld = (Timestamp) pfm.get("oldPRD");
                    BigDecimal oldPlannedAmt = (BigDecimal) pfm.get("oldPlannedAmt");
                    BigDecimal oldApprovedAmt = (BigDecimal) pfm.get("oldApprovedAmt");
                    
                    BigDecimal committedAmt = (BigDecimal) r.getAttribute("CommitedAmt");
                    BigDecimal obligatedAmt = (BigDecimal) r.getAttribute("ObligatedAmt");
                    BigDecimal oldDistributedAmt = (BigDecimal) pfm.get("oldPhDistributedAmt");
                    BigDecimal oldCommittedAmt = (BigDecimal) pfm.get("oldCommittedAmt");
                    BigDecimal oldObligatedAmt = (BigDecimal) pfm.get("oldObligatedAmt");
                    
                    Timestamp obligatedDate = (Timestamp) r.getAttribute("ObligatedDate");
                    Timestamp pkgReceivedFromOp = (Timestamp) r.getAttribute("PkgReceivedFromOp");
                    Timestamp awardStartDate = (Timestamp) r.getAttribute("AwardStartDate");
                    Timestamp awardEndDate = (Timestamp) r.getAttribute("AwardEndDate");
                if (scntx.isUserInRole("APP_USER")) {
                    if (prd != null && (prdOld == null || !prd.equals((prdOld))) && "Y".equals(isPFReview) &&
                        "Y".equals(commitFlag) && ("01".equals(fundingType) || "03".equals(fundingType)) &&
                        ((!"U".equals(actionType) && oldApprovedAmt == null)) &&
                        !"N".equals(checkbook) &&
                        prd.compareTo(today) >= 0) 
                    {
                        exec = bindings.getOperationBinding("getCurrentFY");
                        exec.execute();
                        Integer currentFY = (Integer) exec.getResult();
                        if (budgetFY >= currentFY) {
                        warningMsg.append("<li>Abacus is about to send an Bureau wide application review email. Make sure this action is linked to the correct application, all details (Sectors, Keywords, Locations, Subsectors and Indicators) are entered and Application Review Site is setup with all required documents</li>");
                        }
                    } 
                    if ((approvedAmt != null && oldApprovedAmt == null && distributedAmt==null)
                               && !"N".equals(checkbook)
                               && "Y".equals(commitFlag) && ("01".equals(fundingType) || "03".equals(fundingType))
                          ) {
                            warningMsg.append("<li>Ensure all detail information has been updated based on the final application.  This includes (budget, sectors, subsectors, and indicators)</li>");

                    }
                }
                    if(scntx.isUserInRole("APP_FINANCE"))
                    {
                    if (approvedAmt != null && distributedAmt != null && distributedAmt.compareTo(approvedAmt) < 0 &&
                        (oldApprovedAmt==null || oldDistributedAmt==null || approvedAmt.compareTo(oldApprovedAmt) != 0 || distributedAmt.compareTo(oldDistributedAmt) != 0))
                        warningMsg.append("<li>Distributed Amount "+currencyFormat(distributedAmt)+" is less than Approved Amount "+currencyFormat(approvedAmt)+"</li>");
                    if (distributedAmt != null && committedAmt != null && committedAmt.compareTo(distributedAmt) < 0 &&
                        (oldDistributedAmt==null || oldCommittedAmt==null || distributedAmt.compareTo(oldDistributedAmt) != 0 || committedAmt.compareTo(oldCommittedAmt) != 0))
                        warningMsg.append("<li>Committed Amount "+currencyFormat(committedAmt)+" is less than Distributed Amount "+currencyFormat(distributedAmt)+"</li>");
                    if (committedAmt != null && obligatedAmt != null && obligatedAmt.compareTo(committedAmt) < 0 &&
                        (oldCommittedAmt==null || oldObligatedAmt==null || committedAmt.compareTo(oldCommittedAmt) != 0 || obligatedAmt.compareTo(oldObligatedAmt) != 0))
                        warningMsg.append("<li>Obligated Amount "+currencyFormat(obligatedAmt)+" is less than Committed Amount "+currencyFormat(committedAmt)+"</li>");
                    }
                    if (scntx.isUserInRole("APP_USER")) {
                        if(budgetFY>2015 && "Y".equals(commitFlag) && ("01".equals(fundingType) || "03".equals(fundingType)) && (awardStartDate==null || awardEndDate==null)&&
                            (
                              (!"U".equals(actionType) && obligatedAmt!=null) 
                               ||
                              ("U".equals(actionType) && (obligatedDate!=null || pkgReceivedFromOp!=null))
                            )
                          )
                        warningMsg.append("<li>Action Start Date or End Date is missing. Contact Jr.Grants (jrgrants@ofda.gov) to enter missing date(s).</li>");
                    }
                    
                    if(warningMsg.length()==0)
                    {
                        boolean result=saveChanges();
                        FacesContext fc = FacesContext.getCurrentInstance();
                        if(result)
                            addToGrowl(GrowlType.notice,"Saved successfully",0,5000);
                            //fc.addMessage(ae.getComponent().getClientId(), new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Saved successfully"));
                        else
                            addToGrowl(GrowlType.error,"Unable to save changes. Cancel your changes and try again",0,5000);
                            //fc.addMessage(ae.getComponent().getClientId(), new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Unable to save changes. Cancel your changes and try again."));
                        autoCloseMessage(ae.getComponent().getClientId());
                    }
                    else {
                        pfm.put("beforeSaveWarningMsg", "<ul>"+warningMsg.toString()+"</ul>");
                        RichPopup popup = (RichPopup) this.getWarningPopup();
                        popup.show(new RichPopup.PopupHints());
                    }
                }
                else 
                {
                    FacesContext fc = FacesContext.getCurrentInstance();
                    //fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, errorMsg.toString()));
                    for(int m=0;m<errorMsg.size();m++) {
                        addToGrowl(GrowlType.error,errorMsg.get(m),300*(m),(1000*((m+1)*5)));
                    }
                }
        }
    }
    /**
     * @return
     * Do Pre-commit check,save changes and call post commit for any emails
     */
    public boolean saveChanges() {
    DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
    OperationBinding exec = null;
    OperationBinding legacyExec = null;
    FacesContext fc = FacesContext.getCurrentInstance();
    Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
    DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
    Row r = iter.getCurrentRow();
    if (r != null) {
        BigDecimal approvedAmt = (BigDecimal) r.getAttribute("ApprovedAmt");
        String checkbook = (String) r.getAttribute("IsCheckbook");
        if (approvedAmt !=null && !"N".equals(checkbook)) {
            exec = bindings.getOperationBinding("getChangedSectors");
            exec.execute();
            String result = (String) exec.getResult();
            pfm.put("sectorChangesList", result);
        exec = bindings.getOperationBinding("getChangedBudgetDetails");
        exec.execute();
        result = (String) exec.getResult();
        pfm.put("budgetChangesList", result);
    }
    }
    exec = bindings.getOperationBinding("Commit");
    exec.execute();
    legacyExec = bindings.getOperationBinding("LegacyCommit");
    legacyExec.execute();
    if (exec.getErrors().isEmpty() && legacyExec.getErrors().isEmpty()) {
        exec = bindings.getOperationBinding("publishSubSectorChangesEvent");
        exec.execute();
        exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.execute();
        /*****Post Commit Checklist*******/
        postCommit();
        return true;
    }
    return false;
    }

    /**
     * @return
     * Email generation logic goes here
     *
     */
    public String postCommit() {
        delay=1;
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
        Row r = iter.getCurrentRow();
        OperationBinding exec = null;
        if (r != null) {
            String fundingType = (String) r.getAttribute("FundingActionType");
            String commitFlag = (String) r.getAttribute("CommitFlag");
            String checkbook = (String) r.getAttribute("IsCheckbook");
            Integer budgetFY = (Integer) r.getAttribute("BudgetFy");
            String actionType = (String) r.getAttribute("ActionTypeCode");

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            SimpleDateFormat sdtf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
            MessageBean mb = new MessageBean();
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            Date today = cal.getTime();

            /*****Set New and Old Values*********/
            BigDecimal actionID = (BigDecimal) r.getAttribute("ActionId");
            String isPFReview = (String) r.getAttribute("IsProceedWithReview");
            Timestamp prd = (Timestamp) r.getAttribute("ProposalReviewDate");
            BigDecimal approvedAmt = (BigDecimal) r.getAttribute("ApprovedAmt");
            Timestamp approvedDate = (Timestamp) r.getAttribute("ApprovedDate");
            BigDecimal distributedAmt = (BigDecimal) r.getAttribute("PhDistributedAmt");
            BigDecimal obligatedAmt = (BigDecimal) r.getAttribute("ObligatedAmt");
            String ofdaControlNbr = (String) r.getAttribute("OfdaControlNbr");
            String prSpecialist = (String) r.getAttribute("PrSpecialist");
            String specialist = (String) r.getAttribute("Specialist");
            String oaa = (String) r.getAttribute("Oaa");
            String contractOfficer = (String) r.getAttribute("ContractOfficer");
            Date ctoAwardLtrSentDate = (Date) r.getAttribute("CtoAwardLtrSentDate");
            Date pkgCompleteInSpDate = (Date) r.getAttribute("PkgCompleteInSpDate");
            Date pkgCompleteDate = (Date) r.getAttribute("PkgCompleteDate");
            String pkgComplete=(String)r.getAttribute("PkgComplete");            
            Timestamp revPkgCompleteInSpDate = (Timestamp) r.getAttribute("RevPkgCompleteInSpDate");            
            String isRevPkgComplete=(String)r.getAttribute("IsRevPkgComplete");
            Timestamp revPkgIncompleteDate = (Timestamp) r.getAttribute("RevPkgIncompleteDate");
            String revPkgIncompleteComments=(String)r.getAttribute("RevPkgIncompleteComments");
            Long paaId=(Long)r.getAttribute("PaaId");
            Date completePkgReviewedDate = (Date)r.getAttribute("CompletePkgReviewedDate");
            String isCostsheetApplicable=(String)r.getAttribute("IsCostsheetApplicable");
            
            
            String clr1UserId=(String)r.getAttribute("Clr1UserId");
            Date clr1DateSentForReview=(Date)r.getAttribute("Clr1DateSentForReview");
            Date clr1Date=(Date)r.getAttribute("Clr1Date");
            
            BigDecimal clr2TypeId =(BigDecimal)r.getAttribute("Clr2TypeId");
            String clr2UserId=(String)r.getAttribute("Clr2UserId");
            Date clr2DateSentForReview=(Date)r.getAttribute("Clr2DateSentForReview");
            Date clr2Date=(Date)r.getAttribute("Clr2Date");

            Date issueLtr1RecdDate=(Date)r.getAttribute("IssueLtr1RecdDate");
            Date issueLtr1SentDate=(Date)r.getAttribute("IssueLtr1SentDate");
            
            Date issueLtr2RecdDate=(Date)r.getAttribute("IssueLtr2RecdDate");
            Date issueLtr2SentDate=(Date)r.getAttribute("IssueLtr2SentDate");
            
            Date issueLtr3RecdDate=(Date)r.getAttribute("IssueLtr3RecdDate");
            Date issueLtr3SentDate=(Date)r.getAttribute("IssueLtr3SentDate");

            Timestamp prdOld = (Timestamp) pfm.get("oldPRD");
            BigDecimal oldPlannedAmt = (BigDecimal) pfm.get("oldPlannedAmt");
            BigDecimal oldApprovedAmt = (BigDecimal) pfm.get("oldApprovedAmt");
            Timestamp oldApprovedDate = (Timestamp) pfm.get("oldApprovedDate");
            String oldOfdaControlNbr = (String) pfm.get("oldOfdaControlNbr");
            BigDecimal oldDistributedAmt = (BigDecimal) pfm.get("oldPhDistributedAmt");
            String oldPrSpecialist = (String) pfm.get("oldPrSpecialist");
            String oldSpecialist = (String) pfm.get("oldSpecialist");
            String oldOaa = (String) pfm.get("oldOaa");
            String oldContractOfficer = (String) pfm.get("oldContractOfficer");
            BigDecimal oldObligatedAmt = (BigDecimal) pfm.get("oldObligatedAmt");
            Date oldCtoAwardLtrSentDate = (Date) pfm.get("oldCtoAwardLtrSentDate");            
            Date oldPkgCompleteInSpDate = (Date) pfm.get("oldPkgCompleteInSpDate");            
            String oldPkgComplete=(String)pfm.get("oldPkgComplete");
            String oldPkgCompleteComments = (String) pfm.get("oldPkgCompleteComments");
            Date oldPkgCompleteDate = (Date) pfm.get("oldPkgCompleteDate"); 
            Timestamp oldRevPkgCompleteInSpDate = (Timestamp) pfm.get("oldRevPkgCompleteInSpDate");
            String oldIsRevPkgComplete=(String)pfm.get("oldIsRevPkgComplete");
            String oldRevPkgIncompleteComments = (String) pfm.get("oldRevPkgIncompleteComments");
            Timestamp oldRevPkgIncompleteDate = (Timestamp) pfm.get("oldRevPkgIncompleteDate");
            
            Date oldCompletePkgReviewedDate = (Date) pfm.get("oldCompletePkgReviewedDate"); 
            String oldIsCostsheetApplicable = (String) pfm.get("oldIsCostsheetApplicable");
            
            String oldClr1UserId=(String)pfm.get("oldClr1UserId");
            Date oldClr1DateSentForReview=(Date)pfm.get("oldClr1DateSentForReview");
            Date oldClr1Date=(Date)pfm.get("oldClr1Date");
            
            BigDecimal oldClr2TypeId =(BigDecimal)pfm.get("oldClr2TypeId");
            String oldClr2UserId=(String)pfm.get("oldClr2UserId");
            Date oldClr2DateSentForReview=(Date)pfm.get("oldClr2DateSentForReview");
            Date oldClr2Date=(Date)pfm.get("oldClr2Date");
            
            Date oldIssueLtr1RecdDate=(Date)pfm.get("oldIssueLtr1RecdDate");
            Date oldIssueLtr1SentDate=(Date)pfm.get("oldIssueLtr1SentDate");
            
            Date oldIssueLtr2RecdDate=(Date)pfm.get("oldIssueLtr2RecdDate");
            Date oldIssueLtr2SentDate=(Date)pfm.get("oldIssueLtr2SentDate");
            
            Date oldIssueLtr3RecdDate=(Date)pfm.get("oldIssueLtr3RecdDate");
            Date oldIssueLtr3SentDate=(Date)pfm.get("oldIssueLtr3SentDate");
            
            //Copying new values to Old
            setInitialTrackingValues();
            String actionSubject = "" + r.getAttribute("ProjectName");
            if (r.getAttribute("AwardeeName") != null)
                actionSubject += " - " + r.getAttribute("AwardeeName");
            /*******Proposal Review Check*****************
             * Proposal Review Date (Deadline) is not null and >Today's Date
             * Proceed with Formal Review is Y
             * Valid for 01,03
             * CF=Y
             * FY>=Current FY
             * Action is in Planned (Checking for Approved Amount and Approved Date)
             * */
            if (prd != null && (prdOld == null || !prd.equals((prdOld))) && "Y".equals(isPFReview) &&
                "Y".equals(commitFlag) && ("01".equals(fundingType) || "03".equals(fundingType)) &&
                ((!"U".equals(actionType) && oldApprovedAmt == null) || oldApprovedDate == null) &&
                !"N".equals(checkbook) &&
                prd.compareTo(today) >= 0) {
                exec = bindings.getOperationBinding("getCurrentFY");
                exec.execute();
                Integer currentFY = (Integer) exec.getResult();
                String subject = null;
                String message = null;
                if (budgetFY >= currentFY) {
                    if (prdOld == null) {
                        subject = "Application Review for " + actionSubject;
                        message = "Application Review Email Deadline: " + sdf.format(prd);
                    } else {
                        subject = "Application Review deadline changed for " + actionSubject;
                        message =
                            "Application Review Email Deadline changed from " + sdf.format(prdOld) + " to " +
                            sdf.format(prd);
                    }
                    String splink=(String)JSFUtils.resolveExpression("#{viewScope.proposalReceivedBean.sharepointProposalLink}");

                    //message+="<br><br><a href='"+splink+"' target=_blank"+"> Click here to go to Application Review Site</a>";
                    String applReviewSiteURL =
                        "<br><br><a href=\"" + appScope.get("URL") +
                        FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() +
                        "/faces/viewAction?actionID=" + actionID + "&defaultActionTab=applReviewSite\">Click here to go to Application Review Site in Abacus.</a><br>";
                    message+=applReviewSiteURL;
                    if(paaId!=null)
                    {
                     String applicationURL =
                        "<br><br><a href=\"" + appScope.get("URL") +
                        FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() +
                        "/faces/applicationNavigator?paaId=" + paaId + "\">Click here to view application in Abacus.</a><br>";
                     message+=applicationURL;
                    }
                    boolean t=mb.sendProposalReviewEmail(subject, message, "" + actionID.intValue());
                    if(t) {
                        addToGrowl(GrowlType.mailok,"Application Review email sent.",300*delay++,(1000*(delay*5)));
                    }else
                        addToGrowl(GrowlType.mailerror,"Application Review email failed!",300*delay++,(1000*(delay*5)));
                    exec = bindings.getOperationBinding("ExecuteWithParams");
                    exec.execute();                    
                }
            }

            /*******Proposal Review Check******************/
            /*******Approved Amt Removed Email************************
             * Valid for Checkbook <>N
             * */
            if (approvedAmt == null && oldApprovedAmt != null && distributedAmt==null && !"N".equals(checkbook)) {
                String subject = null;
                String message = null;
                    subject="Approved Amount removed for "+actionSubject;
                    message="Approved Amount was removed. " +("Y".equals(commitFlag)?"Request for Control Number has been cancelled.":"Request to enter funding details has been cancelled");
                Boolean t=mb.sendActionApprovedEmail(subject, message, "" + actionID.intValue());
                if(t) {
                    addToGrowl(GrowlType.mailok,"Request to Cancel Control Number generation email sent to Budget Execution Team",300*delay++,(1000*(delay*5)));
                }else
                    addToGrowl(GrowlType.mailok,"Request to Cancel Control Number generation email failed to send to Budget Execution Team!",300*delay++,(1000*(delay*5)));
            }
            /*******Approved Removed Email*************************/
            /*******Budget / Sector changes check************************
             * Valid for Checkbook <>N
             * */
            if (approvedAmt !=null && !"N".equals(checkbook)) {
                String sresult = (String) pfm.get("sectorChangesList");
                String bresult = (String) pfm.get("budgetChangesList");
                if(oldApprovedAmt==null)
                    bresult="";
                if((sresult!=null && sresult.length()>0)||(bresult!=null && bresult.length()>0))
                {
                String subject = null;
                String message = null;
                    subject="Approved Amount / Sector information was changed: "+actionSubject;
                    message="Approved Amount /Sector information was changed. Please adjust funding details as required"+"<br>"+(bresult.length()>0?"<br><b>Approved Amount</b>":"")+bresult+"<br>"+(sresult.length()>0?"<br><b>Sector Information</b>":"")+sresult;
                boolean t=mb.sendActionApprovedEmail(subject, message, "" + actionID.intValue());
                    if(t) {
                        addToGrowl(GrowlType.mailok,"Updated Approved Amount / Sector Information email sent to Budget Execution Team",300*delay++,(1000*(delay*5)));
                    }else

                    addToGrowl(GrowlType.mailerror,"Updated Approved Amount / Sector Information email failed to send to Budget Execution Team",300*delay++,(1000*(delay*5)));
                }
            }
            /*******Budget / Sector changes check*************************/
            /*******Approved Email************************
             * Valid for Checkbook <>N
             * */
            if (((approvedAmt == null && oldApprovedAmt != null && distributedAmt!=null) ||
                 (approvedAmt != null && (oldApprovedAmt == null || approvedAmt.compareTo((oldApprovedAmt)) != 0))) && !"N".equals(checkbook)) {
                String subject = null;
                String message = null;
                if (oldApprovedAmt == null && oldDistributedAmt==null) {
                    if("Y".equals(commitFlag))
                        subject = "Request for Control Number for " + actionSubject;
                    else
                        subject="Request to enter funding details for "+actionSubject;
                    message = "Action Approved Amount: " + currencyFormat(approvedAmt);
                } else if (approvedAmt != null) {
                    subject = "Approved Amount Changed for " + actionSubject;
                    message =
                        "Action Approved Amount Changed from " + currencyFormat((oldApprovedAmt!=null?oldApprovedAmt:new BigDecimal(0))) + " to " +
                        currencyFormat(approvedAmt)+". ";
                    if (distributedAmt != null && approvedAmt.compareTo(distributedAmt)!=0)
                        message +=
                            "Adjust amounts in funding tab to match new Approved Amount.";
                    if(oldApprovedAmt!=null)
                      message+=" The difference is "+currencyFormat(approvedAmt.subtract(oldApprovedAmt))+".";
                }
                else if(approvedAmt==null && distributedAmt!=null) {
                    subject="Approved Amount Removed for "+actionSubject;
                    message="Approved Amount is removed. Adjust amounts in funding tab to match new Approved Amount.";
                }
                boolean t=mb.sendActionApprovedEmail(subject, message, "" + actionID.intValue());
                if(t) {
                    addToGrowl(GrowlType.mailok,subject.replace("for "+actionSubject,"")+" email sent to Budget Execution Team",300*delay++,(1000*(delay*5)));
                }else
                   addToGrowl(GrowlType.mailerror,subject.replace("for "+actionSubject,"")+" email failed to send to Budget Execution Team!!",300*delay++,(1000*(delay*5)));
            }
            /*******Approved Email*************************/
            /*******Control number generated/changed Email*************************/
            if (ofdaControlNbr != null && (oldOfdaControlNbr == null || !ofdaControlNbr.equals(oldOfdaControlNbr)) &&
                "Y".equals(commitFlag) && !"N".equals(checkbook)) {
                boolean sendChecklist = false;
                if (("01".equals(fundingType) || "03".equals(fundingType) || "08".equals(fundingType)))
                    sendChecklist = true;
                String subject = null;
                String message = null;
                if (oldOfdaControlNbr == null) {
                    subject = "Control Number Generated for " + actionSubject;
                    message = "Control Number Generated: " + ofdaControlNbr;
                } else {
                    subject = "Control Number Changed for " + actionSubject;
                    message = "Control Number Changed from " + oldOfdaControlNbr + " to " + ofdaControlNbr;
                }
                message+="<br><br><p>The Award Package Checklist report can be generated in view action on the tool bar or in view/edit action on the award package tab.</p>";
                boolean t=mb.sendContrlNbrGeneratedEmail(subject, message, "" + actionID.intValue(), sendChecklist);
                if(t) {
                    addToGrowl(GrowlType.mailok,subject.replace("for "+actionSubject,"")+" email sent to Requester",300*delay++,(1000*(delay*5)));
                }else
                   addToGrowl(GrowlType.mailerror,subject.replace("for "+actionSubject,"")+" email failed to send to Requester!",300*delay++,(1000*(delay*5)));
            }

            /*******Control number generated/changed Email*************************/
            /*******Distributed Amount Changed Email*************************/
            if (
                 (
                  (distributedAmt != null && oldDistributedAmt != null && distributedAmt.compareTo((oldDistributedAmt)) != 0)||
                  (distributedAmt==null &&oldDistributedAmt!=null)
                 )
                && "Y".equals(commitFlag) && !"N".equals(checkbook)) {
                String subject = null;
                String message = null;
                if(distributedAmt!=null)
                {
                subject = "Distributed Amount Changed for " + actionSubject;
                message =
                    "Action Distributed Amount Changed from " + currencyFormat(oldDistributedAmt) + " to " +
                    currencyFormat(distributedAmt);
                }
                else
                {
                    subject="Distribution Amount Removed for "+actionSubject;
                    message="Action Distribution Amount Removed";
                }
                 
               boolean t= mb.sendActionDistributeAmtChangedEmail(subject, message, "" + actionID.intValue());
                if(t) {
                    addToGrowl(GrowlType.mailok,subject.replace("for "+actionSubject,"")+" email sent to Requester",300*delay++,(1000*(delay*5)));
                }else
                    addToGrowl(GrowlType.mailerror,subject.replace("for "+actionSubject,"")+" email failed to send to Requester!",300*delay++,(1000*(delay*5)));
            }
            /*******Distributed Amount Changed Email*************************/
            /*******Proposal Review Specialist Assigned Email*************************/
            if ("Y".equals(commitFlag) && !"N".equals(checkbook) &&
                (
                  (prSpecialist != null && (oldPrSpecialist == null || !prSpecialist.equals(oldPrSpecialist)))
                ||(prSpecialist==null && oldPrSpecialist!=null)
                )) {
                String subject = null;
                String message = null;
                
                if(prSpecialist==null) {
                    subject= "Application Review Specialist Unassigned for ";
                    message =oldPrSpecialist+" Unassigned as Application Review Specialist";
                }
                else if(oldPrSpecialist==null) {
                    subject= "Application Review Specialist Assigned for ";
                    message =prSpecialist+" is assigned as Application Review Specialist";
                }
                else {
                    subject= "Application Review Specialist Changed for ";
                    message ="Application Review Specialist is Changed from "+oldPrSpecialist+" to "+prSpecialist;

                }
                subject+=actionSubject;
                boolean t=mb.sendGUProposalReviewSpltEmail(subject, message, "" + actionID.intValue(),oldPrSpecialist);
                if(t) {
                    addToGrowl(GrowlType.mailok,subject.replace("for "+actionSubject,"")+" email sent",300*delay++,(1000*(delay*5)));
                }else
                    addToGrowl(GrowlType.mailerror,subject.replace("for "+actionSubject,"")+" email failed!",300*delay++,(1000*(delay*5)));
            }
            /*******Proposal Review Specialist Assigned Email*************************/
            /*******Award Tracking Email*************************/
            if ("Y".equals(commitFlag) && !"N".equals(checkbook) &&
                ((specialist != null && (oldSpecialist == null || !specialist.equals(oldSpecialist))) ||
                 (oaa != null && (oldOaa == null || !oaa.equals(oldOaa))) ||
                 (contractOfficer != null &&
                  (oldContractOfficer == null || !contractOfficer.equals(oldContractOfficer))))) {
                String subject = "Award Tracking changes for " + actionSubject;
                StringBuilder message = new StringBuilder();
                if (specialist != null)
                    if (oldSpecialist == null)
                        message.append("<br>Specialist Assigned: " + specialist);
                    else if (!specialist.equals(oldSpecialist))
                        message.append("<br>Specialist changed from  " + oldSpecialist + " to " + specialist);
                if (oaa != null)
                    if (oldOaa == null)
                        message.append("<br>M/OAA Negotiator Assigned: " + oaa);
                    else if (!oaa.equals(oldOaa))
                        message.append("<br>M/OAA Negotiator Changed from  " + oldOaa + " to " + oaa);
                if (contractOfficer != null)
                    if (oldContractOfficer == null)
                        message.append("<br>Contract Officer Assigned: " + contractOfficer);
                    else if (!contractOfficer.equals(oldContractOfficer))
                        message.append("<br>Contract Officer Changed from  " + oldContractOfficer + " to " +
                                       contractOfficer);
                boolean t=mb.sendAwardTrackingEmail(subject, message.toString(), "" + actionID.intValue());
                if(t) {
                    addToGrowl(GrowlType.mailok,subject.replace("for "+actionSubject,"")+" email sent",300*delay++,(1000*(delay*5)));
                }else
                    addToGrowl(GrowlType.mailerror,subject.replace("for "+actionSubject,"")+" email failed!",300*delay++,(1000*(delay*5)));
            }
            /*******Award Tracking Email*************************/
            /*******Obligated Email************************
             * Valid for Commit Flag=Y and Checkbook <>N
             * */
            if (obligatedAmt != null && (oldObligatedAmt == null || obligatedAmt.compareTo((oldObligatedAmt)) != 0) &&
                "Y".equals(commitFlag) && !"N".equals(checkbook)) {
                String subject = null;
                String message = null;
                if (oldObligatedAmt == null) {
                    subject = "Action Obligated for " + actionSubject;
                    message = "Action Obligated Amount: " + currencyFormat(obligatedAmt);
                } else {
                    subject = "Obligated Amount Changed for " + actionSubject;
                    message =
                        "Action Obligated Amount Changed from " + currencyFormat(oldObligatedAmt) + " to " +
                        currencyFormat(obligatedAmt);

                }
                boolean t=mb.sendObligatedAmtChangedEmail(subject, message, "" + actionID.intValue());
                    if(t) {
                        addToGrowl(GrowlType.mailok,subject.replace("for "+actionSubject,"")+" email sent",300*delay++,(1000*(delay*5)));
                    }else
                        addToGrowl(GrowlType.mailerror,subject.replace("for "+actionSubject,"")+" email failed!",300*delay++,(1000*(delay*5)));
            }
            /*******Obligated Email*************************/
            /*******AOR Letter Email*************************/
            if (ctoAwardLtrSentDate != null &&
                (oldCtoAwardLtrSentDate == null || !ctoAwardLtrSentDate.equals((oldCtoAwardLtrSentDate))) &&
                "Y".equals(commitFlag) && !"N".equals(checkbook) &&
                ("01".equals(fundingType) || "03".equals(fundingType) || "08".equals(fundingType)) &&
                r.getAttribute("AwardeeCode") != null && r.getAttribute("CtoAwardLtr") != null &&
                "Y".equals(r.getAttribute("CtoAwardLtr")) && r.getAttribute("ProposedAotr") != null &&
                r.getAttribute("ProposedAlternateAotr") != null && r.getAttribute("AwardNbr") != null) {
                boolean t=mb.sendAORLetterEmail("AOR Letter for " + actionSubject+" attached", "Sign and email to "+appScope.get("AOR_LETTER_EMAIL_TO")+" as soon as possible.", "" + actionID.intValue());
                if(t) {
                    addToGrowl(GrowlType.mailok,"AOR Letter email sent",300*delay++,(1000*(delay*5)));
                }else
                    addToGrowl(GrowlType.mailerror,"AOR Letter email failed!",300*delay++,(1000*(delay*5)));
                
            }
            /*******AOR Letter Email*************************/
            /*******Pkg Incomplete Email*************************/
            if ("N".equals(pkgComplete) 
                && pkgCompleteDate!=null && r.getAttribute("PkgCompleteComments") != null &&
                (oldPkgCompleteDate == null || pkgCompleteDate.compareTo(oldPkgCompleteDate)!=0 ||oldPkgCompleteComments==null || !oldPkgCompleteComments.equals(r.getAttribute("PkgCompleteComments"))
                 ||oldPkgComplete==null || !pkgComplete.equals(oldPkgComplete))
                ) {
                String subject = null;
                String message = null;
                subject = "Original Award Package Incomplete for " + actionSubject;
                message =
                    "Original Award Package is Incomplete, please see comments for missing information.<br>" + "Date: " + sdf.format(r.getAttribute("PkgCompleteDate")) +
                    "<br>Comments: " + r.getAttribute("PkgCompleteComments");
                boolean t=mb.sendAwardPkgIncompleteEmail(subject, message, "" + actionID.intValue());
                if(t) {
                    addToGrowl(GrowlType.mailok,subject.replace("for "+actionSubject,"")+" email sent to Requester",300*delay++,(1000*(delay*5)));
                }else
                    addToGrowl(GrowlType.mailerror,subject.replace("for "+actionSubject,"")+" email failed!",300*delay++,(1000*(delay*5)));
            }
            /*******Pkg Incomplete Email*************************/
            /*******GU Print Shop Complete Pkg email*************************/
            if ("Y".equals(pkgComplete) && ("N".equals(r.getAttribute("IsPkgHardCopy"))) &&(oldPkgComplete==null || !pkgComplete.equals(oldPkgComplete))) {
                String subject = null;
                String message = null;
                subject = "Award Package Complete for " + actionSubject;
                message =
                    "Award Package is Complete<br> Date Package Reviewed:";
                if(r.getAttribute("PkgReviewedDate")!=null)
                    message+=sdf.format(r.getAttribute("PkgReviewedDate"));
               boolean t= mb.sendGUPrintShopPkgCompleteEmail(subject, message, "" + actionID.intValue());
                if(t) {
                    addToGrowl(GrowlType.mailok,subject.replace("for "+actionSubject,"")+" email sent to Grants Unit Print Center",300*delay++,(1000*(delay*5)));
                }else
                    addToGrowl(GrowlType.mailerror,subject.replace("for "+actionSubject,"")+" email failed!",300*delay++,(1000*(delay*5)));
            }
            /*******GU Print Shop Complete Original Pkg email*************************/
            /*******Original Award Pkg complete in SP Email*************************/
            if (
                (pkgCompleteInSpDate != null && (oldPkgCompleteInSpDate ==null || pkgCompleteInSpDate.compareTo(oldPkgCompleteInSpDate)!=0))
                 ||(pkgCompleteInSpDate==null && oldPkgCompleteInSpDate!=null)
               )
            {
                String subject = null;
                String message = null;
                String splink=(String)JSFUtils.resolveExpression("#{viewScope.proposalReceivedBean.sharepointProposalLink}");
                if (pkgCompleteInSpDate==null) {
                    subject = "Original Award Package No Longer Valid for Review: " + actionSubject;
                    message = "Original Award Package in Application Review Site is No Longer Valid for Your Review:<br>" + "Original Package Complete Date: " + sdf.format(oldPkgCompleteInSpDate)+" is removed";
                                    
                }
                else if(oldPkgCompleteInSpDate==null)
                {
                subject = "Original Award Package Complete for Review: " + actionSubject;
                message = "Original Award Package is Uploaded to Award Package Folder is Complete and Package is Ready For Your Review:<br><br>" + "Original Package Complete Date: " + sdf.format(pkgCompleteInSpDate);
                //message+="<br><br><a href='"+splink+"' target=_blank"+"> Click here to go to Application Review Site</a>";
                }
                else {
                    subject = "Original Award Package Updated for Review: " + actionSubject;
                    message = "Original Award Package is Updated with new files in Award Package Folder and Package is Ready For Your Review:<br><br>" 
                              + "Original Package Complete Date is changed from " + sdf.format(oldPkgCompleteInSpDate)+" to "+sdf.format(pkgCompleteInSpDate);
                 //   message+="<br><br><a href='"+splink+"' target=_blank"+"> Click here to go to Application Review Site</a>";    
                }
                boolean t=mb.sendAwardPkgCompleteEmail(subject, message, "" + actionID.intValue());
                if(t) {
                    addToGrowl(GrowlType.mailok,subject.replace(": "+actionSubject,"")+" email sent to Grants Unit",300*delay++,(1000*(delay*5)));
                }else
                    addToGrowl(GrowlType.mailerror,subject.replace(": "+actionSubject,"")+" email failed!",300*delay++,(1000*(delay*5)));
            }
            /*******Original Award Pkg complete in SP Email*************************/
            /*******Revised Award Pkg complete in SP Email*************************/
            if (
                (revPkgCompleteInSpDate != null && (oldRevPkgCompleteInSpDate ==null || revPkgCompleteInSpDate.compareTo(oldRevPkgCompleteInSpDate)!=0))
                 ||(revPkgCompleteInSpDate==null && oldRevPkgCompleteInSpDate!=null)
               )
            {
                String subject = null;
                String message = null;
                String splink=(String)JSFUtils.resolveExpression("#{viewScope.proposalReceivedBean.sharepointProposalLink}");
                if (revPkgCompleteInSpDate==null) {
                    subject = "Revised Award Package No Longer Valid for Review: " + actionSubject;
                    message = "Revised Award Package in Award Package Folder is No Longer Valid for Your Review:<br>" + "Revised Package Complete Date: " + sdtf.format(oldRevPkgCompleteInSpDate)+" is removed";
                                    
                }
                else if(oldRevPkgCompleteInSpDate==null)
                {
                subject = "Revised Award Package Complete for Review: " + actionSubject;
                message = "Revised Award Package is Uploaded to  Award Package Folder is Complete and Package is Ready For Your Review:<br><br>" + "Revised Package Complete Date: " + sdtf.format(revPkgCompleteInSpDate);
              //  message+="<br><br><a href='"+splink+"' target=_blank"+"> Click here to go to Application Review Site</a>";
                }
                else {
                    subject = "Revised Award Package Updated for Review: " + actionSubject;
                    message = "Revised Award Package is Updated with new files in  Award Package Folder and Package is Ready For Your Review:<br><br>" 
                              + "Revised Package Complete Date is changed from " + sdtf.format(oldRevPkgCompleteInSpDate)+" to "+sdtf.format(revPkgCompleteInSpDate);
               //     message+="<br><br><a href='"+splink+"' target=_blank"+"> Click here to go to Application Review Site</a>";    
                }
                boolean t=mb.sendAwardPkgCompleteEmail(subject, message, "" + actionID.intValue());
                if(t) {
                    addToGrowl(GrowlType.mailok,subject.replace(": "+actionSubject,"")+" email sent to Grants Unit",300*delay++,(1000*(delay*5)));
                }else
                   addToGrowl(GrowlType.mailerror,subject.replace(": "+actionSubject,"")+" email failed!",300*delay++,(1000*(delay*5)));
            }
            /*******Revised Award Pkg complete in SP Email*************************/
            /*******Revised Pkg Incomplete Email*************************/
            if ("N".equals(isRevPkgComplete) 
                && revPkgIncompleteDate!=null && r.getAttribute("RevPkgIncompleteComments") != null &&
                (oldRevPkgIncompleteDate == null || revPkgIncompleteDate.compareTo(oldRevPkgIncompleteDate)!=0 ||oldRevPkgIncompleteComments==null || !oldRevPkgIncompleteComments.equals(r.getAttribute("RevPkgIncompleteComments"))
                 ||oldIsRevPkgComplete==null || !isRevPkgComplete.equals(oldIsRevPkgComplete))
                ) {
                String subject = null;
                String message = null;
                subject = "Revised Award Package Incomplete for " + actionSubject;
                message =
                    "Revised Award Package is Incomplete, please see comments for missing information.<br>" + "Date: " + sdtf.format(r.getAttribute("RevPkgIncompleteDate")) +
                    "<br>Comments: " + r.getAttribute("RevPkgIncompleteComments");
                boolean t=mb.sendAwardPkgIncompleteEmail(subject, message, "" + actionID.intValue());
                if(t) {
                    addToGrowl(GrowlType.mailok,subject.replace("for "+actionSubject,"")+" email sent sent to Requester",300*delay++,(1000*(delay*5)));
                }else
                   addToGrowl(GrowlType.mailerror,subject.replace("for "+actionSubject,"")+" email failed!",300*delay++,(1000*(delay*5)));

            }
            /*******Revised Pkg Incomplete Email*************************/
            /*******GU Print Shop Complete Revised Pkg email*************************/
            if ("N".equals(r.getAttribute("IsPkgHardCopy")) && completePkgReviewedDate!=null && !completePkgReviewedDate.equals(oldCompletePkgReviewedDate)) {
                String subject = null;
                String message = null;
                subject = "Revised Award Package Complete for " + actionSubject;
                message =
                    "Revised Award Package is Complete<br> Date Complete Package Reviewed:";
                if(r.getAttribute("CompletePkgReviewedDate")!=null)
                    message+=sdf.format(r.getAttribute("CompletePkgReviewedDate"));
                boolean t=mb.sendGUPrintShopPkgCompleteEmail(subject, message, "" + actionID.intValue());
                if(t) {
                    addToGrowl(GrowlType.mailok,subject.replace("for "+actionSubject,"")+" email sent to Grants Unit Print Center",300*delay++,(1000*(delay*5)));
                }else
                    addToGrowl(GrowlType.mailerror,subject.replace("for "+actionSubject,"")+" email failed!",300*delay++,(1000*(delay*5)));
            }
            /*******GU Print Shop Complete Revised Pkg email*************************/
            /*********************Award Package Clearance 1 Assigned/Changed/Removed*************************/
            if((clr1UserId != null && (oldClr1UserId == null || !clr1UserId.equals((oldClr1UserId))))|| (clr1UserId==null && oldClr1UserId!=null)
               ||(clr1DateSentForReview!=null && (oldClr1DateSentForReview ==null || clr1DateSentForReview.compareTo(oldClr1DateSentForReview)!=0))||(clr1DateSentForReview==null && oldClr1DateSentForReview!=null)) {
                    String subject = null;
                    subject = "Award Package for " + actionSubject+" Ready for Review";
                    StringBuilder message = new StringBuilder();
                    message.append(subject);
                    if (clr1UserId != null)
                    {
                        if (oldClr1UserId == null)
                            message.append("<br><br>Senior Team Member: " + clr1UserId);
                        else if (!clr1UserId.equals(oldClr1UserId))
                            message.append("<br><br>Senior Team Member changed from  " + oldClr1UserId + " to " + clr1UserId);
                    }
                    else
                        message.append("<br><br>Senior Team Member: Removed");
                    
                    if (clr1DateSentForReview != null)
                    {
                        if (oldClr1DateSentForReview == null)
                            message.append("<br><br>Date Sent for Review: " + sdf.format(clr1DateSentForReview));
                        else if (clr1DateSentForReview.compareTo(oldClr1DateSentForReview)!=0)
                            message.append("<br><br>Date Sent for Review changed from  " + sdf.format(oldClr1DateSentForReview) + " to " + sdf.format(clr1DateSentForReview));
                    }
                    else
                        message.append("<br><br>Date Sent for Review: Removed");
                    boolean t=mb.sendAwardPkgClearanceEmail(subject, message.toString(), "" + actionID.intValue(),oldClr1UserId,clr1UserId);
                    if(t) {
                        addToGrowl(GrowlType.mailok,subject.replace(": "+actionSubject,"")+" email sent",300*delay++,(1000*(delay*5)));
                    }else
                        addToGrowl(GrowlType.mailerror,subject.replace(": "+actionSubject,"")+" email failed!",300*delay++,(1000*(delay*5)));
                }
           
            /*********************Award Package Clearance 1 Assigned/Changed/Removed*************************/
            
            /*********************Award Package Clearance 1 Cleared*************************/
            if((clr1Date != null && (oldClr1Date ==null || clr1Date.compareTo(oldClr1Date)!=0))
             ||(clr1Date==null && oldClr1Date!=null)) {
                String subject = null;
                StringBuilder message = new StringBuilder();
                if (clr1Date != null)
                {
                    subject = "Award Package for " + actionSubject+" Cleared by Senior Team Member";
                    message.append(subject);
                    if (oldClr1Date== null)
                        message.append("<br><br>Date Cleared: " + sdf.format(clr1Date));
                    else if (clr1Date.compareTo(oldClr1Date)!=0)
                        message.append("<br><br>Date Cleared changed from  " + sdf.format(oldClr1Date) + " to " + sdf.format(clr1Date));
                }
                else
                {
                    subject = "Award Package for " + actionSubject+" Clearance by Senior Team Member: Clearance Date Removed";
                    message.append(subject);
                    message.append("<br><br>Clearance Date Removed");
                }
                boolean t=mb.sendAwardPkgClearanceEmail(subject, message.toString(), "" + actionID.intValue(),oldClr1UserId,clr1UserId);
                if(t) {
                    addToGrowl(GrowlType.mailok,subject.replace(": "+actionSubject,"")+" email sent",300*delay++,(1000*(delay*5)));
                }else
                    addToGrowl(GrowlType.mailerror,subject.replace(": "+actionSubject,"")+" email failed!",300*delay++,(1000*(delay*5)));
            }
            /*********************Award Package Clearance 1 Cleared*************************/
            
            /*********************Award Package Clearance 2 Assigned/Changed/Removed*************************/
            if((clr2UserId != null && (oldClr2UserId == null || !clr2UserId.equals((oldClr2UserId))))|| (clr2UserId==null && oldClr2UserId!=null)
               ||(clr2DateSentForReview!=null && (oldClr2DateSentForReview ==null || clr2DateSentForReview.compareTo(oldClr2DateSentForReview)!=0)) ||(clr2DateSentForReview==null && oldClr2DateSentForReview!=null)) {
                        String type=(String)r.getAttribute("Clr2TypeName");
                        String subject = null;
                        subject = "Award Package for " + actionSubject+" Ready for Additional Review";
                        StringBuilder message = new StringBuilder();
                        message.append(subject+" for "+type);
                        if (clr2UserId != null)
                        {
                            if (oldClr2UserId == null)
                                message.append("<br><br>"+type+": " + clr2UserId);
                            else if (!clr2UserId.equals(oldClr2UserId))
                                message.append("<br><br>"+type+" changed from  " + oldClr2UserId + " to " + clr2UserId);
                        }
                        else
                            message.append("<br><br>"+type+": Removed");
                        
                        if (clr2DateSentForReview != null)
                        {
                            if (oldClr2DateSentForReview == null)
                                message.append("<br><br>Date Sent for Review: " + sdf.format(clr2DateSentForReview));
                            else if (clr2DateSentForReview.compareTo(oldClr2DateSentForReview)!=0)
                                message.append("<br><br>Date Sent for Review changed from  " + sdf.format(oldClr2DateSentForReview) + " to " + sdf.format(clr2DateSentForReview));
                        }
                        else
                            message.append("<br><br>Date Sent for Review: Removed");
                        
                        boolean t=mb.sendAwardPkgClearanceEmail(subject, message.toString(), "" + actionID.intValue(),oldClr2UserId,clr2UserId);
                        if(t) {
                            addToGrowl(GrowlType.mailok,subject.replace(": "+actionSubject,"")+" email sent",300*delay++,(1000*(delay*5)));
                        }else
                            addToGrowl(GrowlType.mailerror,subject.replace(": "+actionSubject,"")+" email failed!",300*delay++,(1000*(delay*5)));
                    }
            /*********************Award Package Clearance 2 Assigned/Changed/Removed*************************/
            
            /*********************Award Package Clearance 2 Cleared*************************/
            if((clr2Date != null && (oldClr2Date ==null || clr2Date.compareTo(oldClr2Date)!=0))
             ||(clr2Date==null && oldClr2Date!=null)) {
                String type=(String)r.getAttribute("Clr2TypeName");
                String subject = null;
                StringBuilder message = new StringBuilder();
                if (clr2Date != null)
                {
                    subject = "Award Package for " + actionSubject+" Cleared by "+type;
                    message.append(subject);
                    message.append("<br><br>"+type);
                    if (oldClr2Date == null)
                        message.append("<br><br>Date Cleared: " + sdf.format(clr2Date));
                    else if (clr2Date.compareTo(oldClr2Date)!=0)
                        message.append("<br><br>Date Cleared changed from  " + sdf.format(oldClr2Date) + " to " + sdf.format(clr2Date));
                }
                else
                {
                    subject = "Award Package for " + actionSubject+" Clearance by "+type+": Clearance Date Removed";
                    message.append(subject);
                    message.append("<br><br>"+type+"<br><br>Clearance Date Removed");
                }
                boolean t=mb.sendAwardPkgClearanceEmail(subject, message.toString(), "" + actionID.intValue(),oldClr2UserId,clr2UserId);
                if(t) {
                    addToGrowl(GrowlType.mailok,subject.replace(": "+actionSubject,"")+" email sent",300*delay++,(1000*(delay*5)));
                }else
                    addToGrowl(GrowlType.mailerror,subject.replace(": "+actionSubject,"")+" email failed!",300*delay++,(1000*(delay*5)));
            }
            /*********************Award Package Clearance 2 Cleared*************************/

            sendIssueLetterEmail(oldIssueLtr1SentDate,
                                 issueLtr1SentDate,
                                 "1st Issue Letter Sent to "+r.getAttribute("AwardeeName")+" for "+r.getAttribute("ProjectName"),
                                 "BHA has sent 1st Issue Letter to "+r.getAttribute("AwardeeName")+" for "+r.getAttribute("ProjectName"),
                                 "1st Issue Letter Sent Date",
                                 actionID,
                                 mb);
            sendIssueLetterEmail(oldIssueLtr2SentDate,
                                 issueLtr2SentDate,
                                 "2nd Issue Letter Sent to "+r.getAttribute("AwardeeName")+" for "+r.getAttribute("ProjectName"),
                                 "BHA has sent 2nd Issue Letter to "+r.getAttribute("AwardeeName")+" for "+r.getAttribute("ProjectName"),
                                 "2nd Issue Letter Sent Date",
                                 actionID,
                                 mb);
            sendIssueLetterEmail(oldIssueLtr3SentDate,
                                 issueLtr3SentDate,
                                 "3rd Issue Letter Sent to "+r.getAttribute("AwardeeName")+" for "+r.getAttribute("ProjectName"),
                                 "BHA has sent 3rd Issue Letter to "+r.getAttribute("AwardeeName")+" for "+r.getAttribute("ProjectName"),
                                 "3rd Issue Letter Sent Date",
                                 actionID,
                                 mb);
            
            sendIssueLetterEmail(oldIssueLtr1RecdDate,
                                 issueLtr1RecdDate,
                                 "1st Issue Letter Response Received from "+r.getAttribute("AwardeeName")+" for "+r.getAttribute("ProjectName"),
                                 "BHA has received 1st Issue Letter Response from "+r.getAttribute("AwardeeName")+" for "+r.getAttribute("ProjectName"),
                                 "1st Issue Letter Received Date",
                                 actionID,
                                 mb);
            sendIssueLetterEmail(oldIssueLtr2RecdDate,
                                 issueLtr2RecdDate,
                                 "2nd Issue Letter Response Received from "+r.getAttribute("AwardeeName")+" for "+r.getAttribute("ProjectName"),
                                 "BHA has received 2nd Issue Letter Response from "+r.getAttribute("AwardeeName")+" for "+r.getAttribute("ProjectName"),
                                 "2nd Issue Letter Received Date",
                                 actionID,
                                 mb);
            sendIssueLetterEmail(oldIssueLtr3RecdDate,
                                 issueLtr3RecdDate,
                                 "3rd Issue Letter Response Received from "+r.getAttribute("AwardeeName")+" for "+r.getAttribute("ProjectName"),
                                 "BHA has received 3rd Issue Letter Response from "+r.getAttribute("AwardeeName")+" for "+r.getAttribute("ProjectName"),
                                 "3rd Issue Letter Received Date",
                                 actionID,
                                 mb);
        }
        return null;
    }

private void sendIssueLetterEmail(Date oldDate,Date newDate,String basicSubject, String basicMessage,String label,BigDecimal actionID, MessageBean mb) {
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    if((newDate != null && (oldDate ==null || newDate.compareTo(oldDate)!=0))
     ||(newDate==null && oldDate!=null)) {
        String subject = null;
        StringBuilder message = new StringBuilder();
        if (newDate != null)
        {
            subject = basicSubject;
            message.append(basicMessage);
            if (oldDate == null)
                message.append("<br><br>"+label+": " + sdf.format(newDate));
            else if (newDate.compareTo(oldDate)!=0)
                message.append("<br><br>"+label+"  changed from  " + sdf.format(oldDate) + " to " + sdf.format(newDate));
            String applReviewSiteURL =
                "<br><br><a href=\"" + appScope.get("URL") +
                FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() +
                "/faces/viewAction?actionID=" + actionID + "&defaultActionTab=applReviewSite\">Click here to go to Application Review Site in Abacus.</a><br>";
            message.append(applReviewSiteURL);
        }
        else
        {
            subject = basicSubject+": Date Removed";
            message.append("<br><br>"+label+" Removed");
        }
        boolean t=mb.sendIssuesLetterEmail(subject, message.toString(), "" + actionID.intValue());
        if(t) {
            addToGrowl(GrowlType.mailok,label+" Notification sent to Reviewers, Review Lead and Requester",300*delay++,(1000*(delay*5)));
        }else
            addToGrowl(GrowlType.mailerror,label+" Notification to Reviewers, Review Lead and Requester failed!",300*delay++,(1000*(delay*5)));
    }
}

    /**
     * @return
     * Rollback all changes.
     * Set initial values after rollback.
     */
    public void cancelChanges(ActionEvent ae) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Rollback");
        exec.execute();
        OperationBinding legacyExec = bindings.getOperationBinding("publishRollbackChangesEvent");
        legacyExec.execute();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        setInitialTrackingValues();
        FacesContext fc = FacesContext.getCurrentInstance();
        if (exec.getErrors().isEmpty() && legacyExec.getErrors().isEmpty())
        {
            //pfm.put("showMessage","Cancelled successfully");
            //fc.addMessage(ae.getComponent().getClientId(),new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Cancelled changes"));
            addToGrowl(GrowlType.notice,"Cancelled changes",0,5000);
        }
            //fc.addMessage(null,//ae.getComponent().getClientId(),new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Cancelled changes"));
        else
            //fc.addMessage(ae.getComponent().getClientId(),
              //            new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Error cancelling changes"));
        addToGrowl(GrowlType.error,"Error cancelling changes",0,5000);
        autoCloseMessage(ae.getComponent().getClientId());
        //pfm.put("errorMessages", null);
    }

    /**
     * When user changes the Region code, this method sets the country code to null,
     * so that user can pick respective Country code based on the Region.
     * @param valueChangeEvent
     */
    public void regionValueChangeListner(ValueChangeEvent valueChangeEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding activityIter = bindings.findIteratorBinding("PAEditView1Iterator");
        Row row = activityIter.getCurrentRow();
        row.setAttribute(AbacusConstants.COUNTRY_CODE, "");
    }

    /**
     * this method validates email distribution list.
     * @param facesContext
     * @param uIComponent
     * @param object
     */
    public void validateEmailDistributionList(FacesContext facesContext, UIComponent uIComponent, Object object) {
        String emailStr = null;

        if (null != object && object.toString().length() > 0) {

            emailStr = object.toString().replaceAll("\\s+", "");
            String[] email;
            if (emailStr.indexOf(",") > 0) {
                email = emailStr.split(",");
            } else {
                email = emailStr.split(";");
            }

            if (email.length > 0) {
                for (String mail : email) {
                    String regex = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{3,4}";
                    Pattern pattern = Pattern.compile(regex);
                    Matcher matcher = pattern.matcher(mail);
                    if (matcher.matches()) {

                    } else {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                      "Invalid Email address : " + object.toString()));
                    }
                }
            }
        }
    }

    /**
     *
     * @return
     */
    public boolean getPreviousDisabled() {
        TaskFlowTrainModel trainModel = getTrainModel();
        TaskFlowTrainStopModel currentStop = trainModel.getCurrentStop();
        TrainStopModel previoudStopModel = (TrainStopModel) trainModel.getPreviousStop(currentStop);
        if (previoudStopModel == null) {
            return true;
        }
        return false;
    }


    /**
     *
     * @return
     */
    public boolean getNextDisabled() {
        TaskFlowTrainModel trainModel = getTrainModel();
        TaskFlowTrainStopModel currentStop = trainModel.getCurrentStop();
        TrainStopModel nextStopModel = (TrainStopModel) trainModel.getNextStop(currentStop);
        if (nextStopModel == null) {
            return true;
        }
        return false;
    }


    private TaskFlowTrainModel getTrainModel() {
        ControllerContext controllerContext = ControllerContext.getInstance();
        ViewPortContext currentViewPortCtx = controllerContext.getCurrentViewPort();
        TaskFlowContext taskFlowCtx = currentViewPortCtx.getTaskFlowContext();

        TaskFlowTrainModel trainModel = taskFlowCtx.getTaskFlowTrainModel();
        return trainModel;
    }

    public void addBudgetReturnLsnr(ReturnEvent returnEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        FacesContext context = FacesContext.getCurrentInstance();
        String result = (String) pfp.get("confirmMessage");
        if ("true".equals(result)) {
            OperationBinding exec = bindings.getOperationBinding("Execute");
            exec.execute();
            exec = bindings.getOperationBinding("ExecutePABudget");
            exec.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnEvent.getComponent());
        }
        MessageBean message = new MessageBean();
        message.sendActivityBudgetTransferEmail(result, (Map) pfp.get("transferDetails"),
                                                (Boolean) pfp.get("isSendEmail"));
    }
    public void setWarningPopup(UIComponent warningPopup) {
        this.warningPopup = ComponentReference.newUIComponentReference(warningPopup);
    }

    public UIComponent getWarningPopup() {
        return warningPopup==null?null:warningPopup.getComponent();
    }
    public void warningDialogLsnr(DialogEvent de) {
        if (DialogEvent.Outcome.yes == de.getOutcome()) {
            boolean result=saveChanges();
            FacesContext fc = FacesContext.getCurrentInstance();
            if(result)
                addToGrowl(GrowlType.notice,"Saved successfully",0,5000);
            else
                addToGrowl(GrowlType.error,"Unable to save changes. Cancel your changes and try again",0,5000);
            autoCloseMessage(de.getComponent().getClientId());

        }
    }
    public void processSaveCancelPoll(PollEvent event) {
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfp.put("showMessage", null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(event.getComponent().getParent().getParent());
    }
    public void editDisclosureLnsr(DisclosureEvent disclosureEvent) {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(disclosureEvent.isExpanded()){
            pageFlowMap.put("topHeight", "170");    
            pageFlowMap.put("detailsDisclosed",true);
            
        }else{
            pageFlowMap.put("topHeight", "70");
            pageFlowMap.put("detailsDisclosed",false);
        }
    }
    public void viewDisclosureLnsr(DisclosureEvent disclosureEvent) {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(disclosureEvent.isExpanded()){
            pageFlowMap.put("topHeight", "165");
            pageFlowMap.put("detailsDisclosed",true);
            
        }else{
            pageFlowMap.put("topHeight", "50");
            pageFlowMap.put("detailsDisclosed",false);
        }
    }

    public void DeleteBudgetRowActionLsnr(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        exec=bindings.getOperationBinding("isDeleteBudget");
        exec.execute();
        Boolean check = (Boolean) exec.getResult();
        if(check)
        {
        exec = bindings.getOperationBinding("Delete");
        exec.execute();
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null,
                                                         new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                          "Fund Account Type is referred in details and cannot be deleted"));
    }
    }

    public TaskFlowId getDynamicTaskFlowId() {
        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }

    public void purposeDeleteDialogLnsr(DialogEvent de) {
        if (DialogEvent.Outcome.yes == de.getOutcome()) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = null;
            exec = bindings.getOperationBinding("deleteStandardAndCustomIndicators");
            exec.execute();
            exec = bindings.getOperationBinding("Delete");
            exec.execute();
        }
    }
    public List<CustomPurposeItem> getPurposeList() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        exec = bindings.getOperationBinding("getPurposeList");
        exec.execute();
        List<CustomPurposeItem> cpi = (List<CustomPurposeItem>) exec.getResult();
        return cpi;
    }
}

