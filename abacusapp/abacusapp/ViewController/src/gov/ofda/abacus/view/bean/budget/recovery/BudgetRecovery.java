package gov.ofda.abacus.view.bean.budget.recovery;

import gov.ofda.abacus.model.abacusapp.pojo.CustomListItem;
import gov.ofda.abacus.view.base.AbacusConstants;
import gov.ofda.abacus.view.base.UIControl;
import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.Serializable;

import java.math.BigDecimal;

import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;

public class BudgetRecovery extends UIControl implements Serializable{
    @SuppressWarnings("compatibility:7446479982405604077")
    private static final long serialVersionUID = 1L;

    private static final String AVAL_AMOUNT_FROM_PROJECT_TO_DIV = "avalAmtFromProjToDiv";
    private static final String AVAL_AMOUNT_FROM_PROJECT_TO_DIV_FMT = "avalAmtFromProjToDiv_fmt";
    private static final String AVAL_AMOUNT_FROM_PROJECT_TO_TEAM = "avalAmtFromProjToTeam";
    private static final String AVAL_AMOUNT_FROM_PROJECT_TO_TEAM_FMT = "avalAmtFromProjToTeam_fmt";
    private static final String EXCESS_AMOUNT_FROM_PROJECT_TO_DIV = "excessAmtFromProjToDiv";
    private static final String EXCESS_AMOUNT_FROM_PROJECT_TO_DIV_FMT = "excessAmtFromProjToDiv_fmt";
    private static final String EXCESS_AMOUNT_FROM_PROJECT_TO_TEAM = "excessAmtFromProjToTeam";
    private static final String EXCESS_AMOUNT_FROM_PROJECT_TO_TEAM_FMT = "excessAmtFromProjToTeam_fmt";
    private static final String BUDGET_RECOVER_TO = "to";
    private static final String RECOVER_BUDGET_AMT = "recoverBudgetAmt";
    private static final String RECOVER_TO_DIVISION = "division";
    private static final String RECOVER_TO_TEAM = "team";
    private static final String SEND_EMAIL = "isSendEmail";
    private String emailList;
    private List<CustomListItem> fundAcctTypeList=new ArrayList<CustomListItem>();
    
    
    /**
     * this method retrieves the available, excess recoverable amounts 
     * for the given project, division and team.
     */
    public String initializeBudgetRecovery() {


        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        setActivityBudgetRecoveryInfoView();
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        // set the activity view 
        OperationBinding binding = bindingContainer.getOperationBinding("ExecuteWithParams");
        binding.execute();
        
        // set the destination category 'To' to null
        pageFlowMap.put(BUDGET_RECOVER_TO, null);
        pageFlowMap.put(RECOVER_BUDGET_AMT, null);
        
        // isSendEmail default to true
        pageFlowMap.put(SEND_EMAIL, true);
        
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        AttributeBinding attrBinding = (AttributeBinding) bindings.get("BudgetSourceType");
        String budgetSourceType = (String) attrBinding.getInputValue();
        attrBinding = (AttributeBinding) bindings.get("BudgetSource");
        String budgetSource = (String) attrBinding.getInputValue();
        if(!"M".equals(budgetSourceType))
            pageFlowMap.put("budgetSource", budgetSource);
        pageFlowMap.put("projectBudgetSourceType", budgetSourceType);
        return "next";
    }

    /**
     * this method will be fired / called when user click on 'Copy Available Amt link' 
     * to copy available amount into recover budget amount.
     * @param clientEvent
     */
    public void copyAvailAmtServerListner(ClientEvent clientEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String recoverTo = (String) map.get(BUDGET_RECOVER_TO);
        if(null != recoverTo){
            if(RECOVER_TO_DIVISION.equalsIgnoreCase(recoverTo)){
                map.put(RECOVER_BUDGET_AMT, map.get(AVAL_AMOUNT_FROM_PROJECT_TO_DIV));
            }else if(RECOVER_TO_TEAM.equalsIgnoreCase(recoverTo)){
                map.put(RECOVER_BUDGET_AMT, map.get(AVAL_AMOUNT_FROM_PROJECT_TO_TEAM));            
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(clientEvent.getComponent().getParent().getParent().getParent());
    }
    /**
     * this method will be fired / called when user click on 'Copy Excess Amt link' 
     * to copy excess amount into recover budget amount.
     * @param clientEvent
     */
    public void copyExcessAmtServerListner(ClientEvent clientEvent) {
        Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String recoverTo = (String) map.get(BUDGET_RECOVER_TO);
        if(null != recoverTo){
            if(RECOVER_TO_DIVISION.equalsIgnoreCase(recoverTo)){
                map.put(RECOVER_BUDGET_AMT, map.get(EXCESS_AMOUNT_FROM_PROJECT_TO_DIV));    
            }else if(RECOVER_TO_TEAM.equalsIgnoreCase(recoverTo)){
                map.put(RECOVER_BUDGET_AMT, map.get(EXCESS_AMOUNT_FROM_PROJECT_TO_TEAM));            
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(clientEvent.getComponent().getParent().getParent().getParent());
        
    }
    
    /**
     * This method does the budget recovery for the given project to the associated team / division.
     */
    public String recoverBudget() {
        // Add event code here...
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        
        Map<String, Object> transferDetails = new HashMap<String, Object>();
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String recoverTo = (String) pageFlowMap.get(BUDGET_RECOVER_TO);
        String result;
            // make the division transfer
            if(RECOVER_TO_DIVISION.equalsIgnoreCase(recoverTo)){
                OperationBinding binding = bindingContainer.getOperationBinding("recoverFromProjectToDiv");
                binding.execute();
                result = (String) binding.getResult();
                pageFlowMap.put("confirmMessage", result);
            }else{
                OperationBinding binding = bindingContainer.getOperationBinding("recoverFromProjectToTeam");
                binding.execute();
                result = (String) binding.getResult();
                pageFlowMap.put("confirmMessage", result);
            }
        AttributeBinding attrBinding = (AttributeBinding) bindingContainer.get("OfdaTeamCode");
        String teamCode = (String) attrBinding.getInputValue();
        
        AttributeBinding attrBindingDiv = (AttributeBinding) bindingContainer.get("OfdaDivisionCode");
        String divCode = (String) attrBindingDiv.getInputValue();
        
        AttributeBinding attrBindingPName = (AttributeBinding) bindingContainer.get("ProjectName");
        String projName = (String) attrBindingPName.getInputValue();
        
        String bureauCode = (String) ((AttributeBinding) bindingContainer.get("BureauCode")).getInputValue();
        String officeCode = (String) ((AttributeBinding) bindingContainer.get("OfficeCode")).getInputValue();
        String fundAcctTypeName = (String) ((AttributeBinding) bindingContainer.get("FundAcctTypeName")).getInputValue();
        
        transferDetails.put("bureauCode", bureauCode);
        transferDetails.put("officeCode", officeCode);
        transferDetails.put("ofdaTeamCode", teamCode);
        transferDetails.put("ofdaDivisionCode", divCode);
        transferDetails.put(BUDGET_RECOVER_TO, recoverTo);
        transferDetails.put("fundAcctTypeName", fundAcctTypeName);
        transferDetails.put(RECOVER_BUDGET_AMT, currencyFormat((BigDecimal)pageFlowMap.get(RECOVER_BUDGET_AMT)));
        transferDetails.put(SEND_EMAIL, pageFlowMap.get(SEND_EMAIL));
        transferDetails.put("comments", pageFlowMap.get("comments"));
        transferDetails.put("projectNbr", pageFlowMap.get("projectNbr"));
        transferDetails.put("budgetFY", pageFlowMap.get("budgetFY"));
        transferDetails.put("projName", projName);
        DCIteratorBinding iter = bindingContainer.findIteratorBinding("BudgetSourceLOV1Iterator");
        Key key = new Key(new Object[] { pageFlowMap.get("budgetSource") });
        Row r[] = iter.getRowSetIterator().findByKey(key, 1);
        if(r.length==1)
        {
            transferDetails.put("budgetSource",r[0].getAttribute("BudgetSourceName") );
        }
        pageFlowMap.put("transferDetails", transferDetails);        
        return result;
    }

    /**
     * This method validates Budget recover amount against available amount.
     * @param facesContext
     * @param uIComponent
     * @param object
     */
    public void validateBudgetRecoverAmt(FacesContext facesContext, UIComponent uIComponent, Object object) {
        BigDecimal value = (BigDecimal) object;
        if(value != null){
            Map map = AdfFacesContext.getCurrentInstance().getPageFlowScope();
            String recoverTo = (String) map.get(BUDGET_RECOVER_TO);
            BigDecimal avalToRecover = null;
            if(null != recoverTo){
                if(RECOVER_TO_DIVISION.equalsIgnoreCase(recoverTo)){
                    avalToRecover = (BigDecimal) map.get(AVAL_AMOUNT_FROM_PROJECT_TO_DIV);
                } else if(RECOVER_TO_TEAM.equalsIgnoreCase(recoverTo)){
                    avalToRecover = (BigDecimal) map.get(AVAL_AMOUNT_FROM_PROJECT_TO_TEAM);
                }
            }
            
            // check if value is less than or equal to zero
            if(value.doubleValue() <= 0){
                //throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, null,  "You entered an amount of "+ NumberFormat.getCurrencyInstance().format(value) + ". Enter an amount that is at least $0.01"));
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, null,  "Recover amount must be greater than $0.00"));
            }
            
            if(null != avalToRecover){
                if(value.compareTo(avalToRecover) == 1){
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, null,  "You entered an amount of " + NumberFormat.getCurrencyInstance().format(value) + ". Enter an amount that is less than or equal to available amount " + currencyFormat(avalToRecover)));
                }
            }
        }
    }
    
    /**
     * This method represents the value change listner where the amount to be recoved, 
     * is it either Division Budget or Team budget.
     * based on the value, the available & excess amounts will be displayed.
     * @param valueChangeEvent
     */
    public void recoverToRadioValueChangeListner(ValueChangeEvent valueChangeEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("getValidFundAccountTypeList");
        exec.execute();
        fundAcctTypeList = (List<CustomListItem>) exec.getResult();
        AdfFacesContext.getCurrentInstance().addPartialTarget(valueChangeEvent.getComponent().getParent());
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put(RECOVER_BUDGET_AMT, null);
    }
    
    private Number getAmt(String attrName) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        AttributeBinding attrBinding = (AttributeBinding) bindings.get(attrName);
        String tmp = (String) attrBinding.getInputValue();
        Number availAmt=0;
        if(tmp!=null) {
            try {
                availAmt = NumberFormat.getCurrencyInstance().parse(tmp);
            } catch (Exception e) {
                availAmt=0;
            }
        }
        return availAmt;
    }
    
    /**
     * this method serves as a tooltip for send email check box.
     * @return
     */
    public String getEmailAddr(){
        if(emailList==null)
        {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        String budgetFY = ""+((AttributeBinding) bindings.get("BudgetFy")).getInputValue();
        String projectNbr = (String) ((AttributeBinding) bindings.get("ProjectNbr")).getInputValue();
        MessageBean mbean = new MessageBean();
        Map<String, String> emailMap = mbean.getActivityBudgetTransferEmailList(budgetFY,projectNbr);
        emailList=emailMap.get("emailList");
        }
        return (emailList);
    }
    public void fundAccountTypeChgLnsr(ValueChangeEvent vce) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        //isVActivityAmtValid
        pfp.put("fundAcctTypeCode", vce.getNewValue());
        setActivityBudgetRecoveryInfoView();
        String to = (String) pfp.get("to");
        if(to!=null)
        {
        BigDecimal avail=new BigDecimal(0);
        if(to.equalsIgnoreCase("Division")){
                avail = (BigDecimal)  pfp.get(AVAL_AMOUNT_FROM_PROJECT_TO_DIV);
            }else if(to.equalsIgnoreCase("Team")){
                avail = (BigDecimal)  pfp.get(AVAL_AMOUNT_FROM_PROJECT_TO_TEAM);
            }
        if(avail!=null && avail.equals(BigDecimal.ZERO))
           pfp.put("to", null);
        }
        pfp.put("recoverBudgetAmt", null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(vce.getComponent().getParent().getParent().getParent().getParent());
    }
        
    public void setActivityBudgetRecoveryInfoView() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        OperationBinding exec = bindings.getOperationBinding("setActivityBudgetRecoveryInfoView");
        exec.execute();
        if (exec.getErrors().isEmpty()) {
            DCIteratorBinding iter = bindings.findIteratorBinding("ActivityBudgetRecoverInfoView1Iterator");
            if (iter.getCurrentRow() != null) {
                Row r = iter.getCurrentRow();
                
                BigDecimal avalFromProjToDiv = (BigDecimal) r.getAttribute("DivisionAvailableAmt");
                pfp.put(AVAL_AMOUNT_FROM_PROJECT_TO_DIV, avalFromProjToDiv);
                pfp.put(AVAL_AMOUNT_FROM_PROJECT_TO_DIV_FMT, currencyFormat(avalFromProjToDiv));
                    
                BigDecimal avalFromProjToTeam = (BigDecimal)r.getAttribute("TeamAvailableAmt");
                pfp.put(AVAL_AMOUNT_FROM_PROJECT_TO_TEAM, avalFromProjToTeam);
                pfp.put(AVAL_AMOUNT_FROM_PROJECT_TO_TEAM_FMT, currencyFormat(avalFromProjToTeam));
                
                BigDecimal excessFromProjToDiv = (BigDecimal) r.getAttribute("DivisionExcessAmt");
                pfp.put(EXCESS_AMOUNT_FROM_PROJECT_TO_DIV, excessFromProjToDiv);
                pfp.put(EXCESS_AMOUNT_FROM_PROJECT_TO_DIV_FMT, currencyFormat(excessFromProjToDiv));
                
                BigDecimal excessFromProjToTeam = (BigDecimal)  r.getAttribute("TeamExcessAmt");
                pfp.put(EXCESS_AMOUNT_FROM_PROJECT_TO_TEAM, excessFromProjToTeam);
                pfp.put(EXCESS_AMOUNT_FROM_PROJECT_TO_TEAM_FMT, currencyFormat(excessFromProjToTeam));
                
                pfp.put("remainingPlannedAmt",  (BigDecimal)  r.getAttribute("RemainingPlannedAmt"));    
                pfp.put("projectBudgetReq",  (BigDecimal)  r.getAttribute("RequiredAmt"));
                
                
            }
        
        }
        
    }

    public List<CustomListItem> getFundAcctTypeList() {
        return fundAcctTypeList;
    }
}
