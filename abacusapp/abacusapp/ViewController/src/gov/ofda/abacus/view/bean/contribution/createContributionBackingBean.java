package gov.ofda.abacus.view.bean.contribution;

import gov.ofda.abacus.view.base.JSFUtils;
import gov.ofda.abacus.view.base.UIControl;

import gov.ofda.abacus.view.base.UIControl.GrowlType;

import gov.ofda.abacus.view.bean.message.MessageBean;

import java.math.BigDecimal;

import java.util.Map;

import oracle.adf.view.rich.context.AdfFacesContext;

public class createContributionBackingBean extends UIControl {
    public createContributionBackingBean() {
    }


    public void saveChanges() {
        invokeMethod("Commit");
        String paxActivitiesId = "" + JSFUtils.resolveExpression("#{bindings.PaxActivitiesId.inputValue}");
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfm.put("paxActivitiesId", paxActivitiesId);
        /**************** Contribution Created Email ************start*/
        String subject = "New Contribution has been created";
        String message = "There is a new Contribution which has been created ";
        MessageBean mb = new MessageBean();
        boolean t = mb.sendCostSheetApplicableEmail(subject, message, "" + paxActivitiesId);
        pfm.put("isEmailSent", t);
        /**************** Contribution Email *******end**/

    }
}
