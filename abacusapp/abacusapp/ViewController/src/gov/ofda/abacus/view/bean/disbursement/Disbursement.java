package gov.ofda.abacus.view.bean.disbursement;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import javax.faces.context.FacesContext;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.share.logging.ADFLogger;

import oracle.jbo.domain.BlobDomain;

public class Disbursement implements Serializable{
    
    @SuppressWarnings("compatibility:-1793450918326815002")
    private static final long serialVersionUID = 1L;
    private static ADFLogger logger = ADFLogger.createADFLogger(Disbursement.class);
    
    
    public void downloadFile(FacesContext facesContext, OutputStream outputStream) {
        DCBindingContainer bindingContainer =
            (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        
        AttributeBinding attrBinding = (AttributeBinding) bindingContainer.get("fileData");
        BlobDomain blob = (BlobDomain) attrBinding.getInputValue();
        if(null != blob){
            attrBinding = (AttributeBinding) bindingContainer.get("FileName");
            String fileName = (String) attrBinding.getInputValue();
            
            HttpServletResponse response =
                (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            
            response.setHeader("Cache-Control", "must-revalidate,post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setContentType("aplication/msexcel");
            response.setHeader("Content-Disposition", 
            "attachment;filename=" + fileName);
            
            InputStream is = blob.getInputStream();
            ServletOutputStream ouStream;
            try {
                ouStream = response.getOutputStream();
                byte[] buffer = new byte[10 * 1024];
                int nread;
                while ((nread = is.read(buffer)) != -1){
                    ouStream.write(buffer, 0, nread);    
                }
                ouStream.close();
            } catch (IOException exception) {
               logger.log(logger.ERROR, "Exception while downloading disbursement file :: " +exception.toString());
            }
        }
        
    }
}
