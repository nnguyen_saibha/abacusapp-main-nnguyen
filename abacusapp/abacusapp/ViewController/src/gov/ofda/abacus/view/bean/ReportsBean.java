package gov.ofda.abacus.view.bean;

import gov.ofda.abacus.view.base.UIControl;

import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichListView;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelSplitter;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.QueryEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.ComponentReference;
import org.apache.myfaces.trinidad.util.Service;


public class ReportsBean extends UIControl {
    @SuppressWarnings("compatibility:-2291033986237107010")
    private static final long serialVersionUID = -368200924450400707L;
    private static ADFLogger logger = ADFLogger.createADFLogger(ReportsBean.class);
    private static String configParam = "?_xpt=0&_xf=analyze&_xmode=2";
  
    private String repURL;
    private String repName;
    private String awardNbr;
    private String appURL;
    private String projectNbr;
    private String finalRepURL;
    private boolean biFlag;
    private String reportID;
    private String taskFlowId = "/WEB-INF/tflows/reports/search-reports-btf.xml#search-reports-btf";
    private ComponentReference reportsPanelGroup;
    private ComponentReference formsLayout;
    private ComponentReference publisherLayout;
    private String favoriteOutputFormat;
    private String favoriteID;
    private String favoriteFileName;

    private boolean _quickQueryVisible = true;
    private ComponentReference recentReportListView;

    boolean collapsed = false;
    private ComponentReference mainReportSplitter;
    private ComponentReference blankLayout;

    public void setCollapsed(boolean collapsed) {
        this.collapsed = collapsed;
    }

    public boolean isCollapsed() {
        return collapsed;
    }

    public void setQuickQueryVisible(boolean _quickQueryVisible) {
        this._quickQueryVisible = _quickQueryVisible;
    }

    public boolean isQuickQueryVisible() {
        return _quickQueryVisible;
    }

    public void setReportID(String reportID) {
        this.reportID = reportID;
    }

    public String getReportID() {
        return reportID;
    }

    public void setAppURL(String appURL) {
        this.appURL = appURL;
    }

    public String getAppURL() {
        return appURL;
    }

    public void setRepURL(String repURL) {
        this.repURL = repURL;
    }

    public String getRepURL() {
        return repURL;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getRepName() {
        return repName;
    }


    public void setAwardNbr(String awardNbr) {
        this.awardNbr = awardNbr;
    }

    public String getAwardNbr() {
        return awardNbr;
    }

    public void setProjectNbr(String projectNbr) {
        this.projectNbr = projectNbr;
    }

    public String getProjectNbr() {
        return projectNbr;
    }

    public ReportsBean() {
        super();
    }

    public void onSubjectAreaSelect(SelectionEvent selectionEvent) {
        this.onTreeTableSelect(selectionEvent);
        //  AdfFacesContext.getCurrentInstance().addPartialTarget(arg0);
    }

    public void openReport(ActionEvent actionEvent) {
        // Add event code here...
        logger.log("appURL: " + appURL);
        logger.log("URL: " + repURL);
        
        StringBuffer sb = new StringBuffer();
        sb.append(appURL);
        sb.append(repURL);
        sb.append(configParam);
        if(null != awardNbr){
            String awardConfigParam = "&_paramsP_AWARD_NBR="+awardNbr;
            sb.append(awardConfigParam);
        }
        if(null != projectNbr){
            String projConfigParam = "&_paramsP_PROJECT_NBR=" +projectNbr;
            sb.append(projConfigParam);
        }
        
        finalRepURL = sb.toString();
        biFlag = true;
        String script = "window.open('" + finalRepURL + "','_blank');";
        logger.log("final URL: " + finalRepURL);
        /*RichShowDetailItem pLayout = getPublisherLayout();
        pLayout.setDisclosed(true);
        RichShowDetailItem fLayout = getFormsLayout();
        fLayout.setDisclosed(false);
        RichShowDetailItem bLayout = getBlankLayout();
        bLayout.setDisclosed(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(getReportsPanelGroup());*/
        //publisherLayout.setVisible(true);


        logger.log("Script: " + script);
         ExtendedRenderKitService service =
            Service.getRenderKitService(FacesContext.getCurrentInstance(), ExtendedRenderKitService.class);
        service.addScript(FacesContext.getCurrentInstance(), script);

    }

    public void setFinalRepURL(String finalRepURL) {
        this.finalRepURL = finalRepURL;
    }

    public String getFinalRepURL() {
        return finalRepURL;
    }

    public void setBiFlag(boolean biFlag) {
        this.biFlag = biFlag;
    }

    public boolean isBiFlag() {
        return biFlag;
    }

    public TaskFlowId getDynamicTaskFlowId() {
        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }


    public void setReportsPanelGroup(RichPanelGroupLayout reportsPanelGroup) {
        this.reportsPanelGroup = ComponentReference.newUIComponentReference(reportsPanelGroup);
    }

    public RichPanelGroupLayout getReportsPanelGroup() {
        if (reportsPanelGroup != null) {
            return (RichPanelGroupLayout) reportsPanelGroup.getComponent();
        }
        return null;

    }

    public void setFormsLayout(RichShowDetailItem formsLayout) {
        this.formsLayout = ComponentReference.newUIComponentReference(formsLayout);
    }

    public RichShowDetailItem getFormsLayout() {
        if (formsLayout != null) {
            return (RichShowDetailItem) formsLayout.getComponent();
        }
        return null;
    }

    public void setPublisherLayout(RichShowDetailItem publisherLayout) {
        this.publisherLayout = ComponentReference.newUIComponentReference(publisherLayout);
    }

    public RichShowDetailItem getPublisherLayout() {
        if (publisherLayout != null) {
            return (RichShowDetailItem) publisherLayout.getComponent();
        }
        return null;

    }

    public void setBlankLayout(RichShowDetailItem blankLayout) {
        this.blankLayout = ComponentReference.newUIComponentReference(blankLayout);
    }

    public RichShowDetailItem getBlankLayout() {
        if (blankLayout != null) {
            return (RichShowDetailItem) blankLayout.getComponent();
        }
        return null;

    }

    public String quick2Advanced() {

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("resetRMTReportSearch");
        exec.execute();
        _quickQueryVisible = false;


        return null;
    }


    public void invokeApplet(ClientEvent clientEvent) {
        // Add event code here...
        if (clientEvent != null) {
            Boolean message = (Boolean) clientEvent.getParameters().get("formsTrigger");

            if (message != null && message.booleanValue()) {
                FacesContext context = FacesContext.getCurrentInstance();
                StringBuffer sb = new StringBuffer();
                sb.append("document.forms_applet.raiseEvent('refresh',");
                sb.append(reportID);
                sb.append(");");

                ExtendedRenderKitService erks = Service.getRenderKitService(context, ExtendedRenderKitService.class);
                erks.addScript(context, sb.toString());
            }
        }


    }


    public String advanced2Quick() {
        // Add event code here...

        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("resetAdvancedReportSearch");
        exec.execute();
        _quickQueryVisible = true;

        return null;
    }

    public void setFavoriteOutputFormat(String favoriteOutputFormat) {
        this.favoriteOutputFormat = favoriteOutputFormat;
    }

    public String getFavoriteOutputFormat() {
        return favoriteOutputFormat;
    }

    public void setFavoriteID(String favoriteID) {
        this.favoriteID = favoriteID;
    }

    public String getFavoriteID() {
        return favoriteID;
    }

    public void setFavoriteFileName(String favoriteFileName) {
        this.favoriteFileName = favoriteFileName;
    }

    public String getFavoriteFileName() {
        return favoriteFileName;
    }

    public String generateFavoriteReport() {

        //https://qpofdabc01.ofda.gov/reports/rwservlet?RepOFDADXls&report=PROJECT_DETAILS.RDF&P_FAV_ID=43078&saveas=PRO.xls
        if (favoriteOutputFormat != null && appURL != null && favoriteFileName != null && favoriteID != null) {
            StringBuffer sb = new StringBuffer();
            sb.append(appURL);
            sb.append("/reports/rwservlet?");
            if ("PDF".equalsIgnoreCase(favoriteOutputFormat)) {
                sb.append("RepOFDADPdf");
            } else if ("XLS".equalsIgnoreCase(favoriteOutputFormat)) {
                sb.append("RepOFDADXls");
            }
            sb.append("+report=");
            sb.append(favoriteFileName);
            sb.append("+P_FAV_ID=");
            sb.append(favoriteID);
            sb.append("+saveas=");
            if ("PDF".equalsIgnoreCase(favoriteOutputFormat)) {
                sb.append("REPORT.PDF");
            } else if ("XLS".equalsIgnoreCase(favoriteOutputFormat)) {
                sb.append("REPORT.XLS");
            }
            finalRepURL = sb.toString();

            StringBuilder strb = new StringBuilder("window.open('" + finalRepURL + "');");
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(), ExtendedRenderKitService.class);
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        }

        return null;
    }

    public void showOrHideUserRecentReportsListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        Boolean otherUserReportsFlg;
        if (valueChangeEvent != null) {
            otherUserReportsFlg = (Boolean) valueChangeEvent.getNewValue();
            if (otherUserReportsFlg.booleanValue()) {
                DCBindingContainer bindings =
                    (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding exec = bindings.getOperationBinding("removeUserRecentReports");
                exec.execute();
            } else {
                DCBindingContainer bindings =
                    (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
                OperationBinding exec = bindings.getOperationBinding("addUserRecentReports");
                exec.execute();
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(getRecentReportListView());
        }

    }

    public void setRecentReportListView(RichListView recentReportListView) {
        this.recentReportListView = ComponentReference.newUIComponentReference(recentReportListView);
    }

    public RichListView getRecentReportListView() {
        if (recentReportListView != null) {
            return (RichListView) recentReportListView.getComponent();
        }
        return null;

    }

    public void setMainReportSplitter(RichPanelSplitter mainReportSplitter) {
        this.mainReportSplitter = ComponentReference.newUIComponentReference(mainReportSplitter);
    }

    public RichPanelSplitter getMainReportSplitter() {
        if (mainReportSplitter != null) {
            return (RichPanelSplitter) mainReportSplitter.getComponent();
        }
        return null;
    }


    public String collapseSearch() {
        // Add event code here...
        collapsed = !collapsed;
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMainReportSplitter());
        return null;
    }


    public void quickQueryLsnr(QueryEvent queryEvent) {
        invokeEL("#{bindings.ReportViewCriteriaQuery.processQuery}", new Class[] { QueryEvent.class }, new Object[] {
                 queryEvent });
        setReportParameterRegion();
    }

    public void advancedQueryLnsr(QueryEvent queryEvent) {
        invokeEL("#{bindings.AdvancedQueryQuery.processQuery}", new Class[] { QueryEvent.class }, new Object[] {
                 queryEvent });
        setReportParameterRegion();
    }
    private void setReportParameterRegion() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ReportView1Iterator");
        Row r = iter.getCurrentRow();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        
        if(r!=null && iter.getEstimatedRowCount()==1) {
            pfm.put("reportID",r.getAttribute("RepId"));
        }
        else
           pfm.put("reportID",null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getReportsPanelGroup());
        
    }
}
