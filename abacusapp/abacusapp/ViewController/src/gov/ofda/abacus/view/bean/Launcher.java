/* Copyright (c) 2009, 2010, Oracle and/or its affiliates. All rights reserved. */
package gov.ofda.abacus.view.bean;


import gov.ofda.abacus.view.base.UIControl;
import gov.ofda.uishell.TabContext;

import java.io.Serializable;

import java.math.BigDecimal;

import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.ItemEvent;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.myfaces.trinidad.event.ReturnEvent;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


/**
 * Launcher is a backingBean-scope managed bean. The public methods are
 * available to EL. The methods call TabContext APIs available to the
 * Dynamic Tab Shell Template. The boolean value for _launchActivity
 * determines whether another tab instance is created or selected. Each tab
 * (i.e., task flow) is tracked by ID. The title is the tab label.
 */

public class Launcher extends UIControl implements Serializable {
    @SuppressWarnings("compatibility:7272515827196641235")
    private static final long serialVersionUID = 1572020828740056144L;

    public Launcher() {
    }
    
    private static final String ACTION_ID_REQUEST_PARAMETER = "actionId";
    private static final String ID = "id";
    private static final String BUDGET_YEAR = "budget_year";
    private static final String PROJECT_NUMBER = "project_number";
    private static final String SET_TAB_HEADER = "Set : ";

    /**
     * Example method to call a single instance task flow. Note the boolean
     * to create another tab instance is set to false. The taskflow ID is used
     * to track whether to create a new tab or select an existing one.
     */
    private void _launchActivity(String id, String title, String taskflowId, boolean newTab, Map map) {
    
        try {
            if (newTab) {
                TabContext.getCurrentInstance().addTab(id, title, taskflowId, map);
            } else {
                TabContext.getCurrentInstance().addOrSelectTab(id, title, taskflowId, map);
            }
        } catch (TabContext.TabOverflowException toe) {
            // causes a dialog to be displayed to the user saying that there are
            // too many tabs open - the new tab will not be opened...
            toe.handleDefault();
        }
    }

    public void viewAction(ActionEvent actionEvent) {
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String actionid = requestMap.get("actionid").toString();
        String id = requestMap.get("id").toString();
        Object val = requestMap.get("newTab");
        boolean isNewTab = false;
        if(null != val){
            if(val.toString().equalsIgnoreCase("true")){
                isNewTab = true;
            }
        }
        Map amap = new HashMap<String, Object>();
        amap.put("actionid", actionid);
        amap.put("showView","all");
        amap.put("showToolbarView","toolbar");
        amap.put("defaultReportType","Pdf");
        amap.put("defaultView","viewactionbtf");
        _launchActivity(id, "Action " + id, "/WEB-INF/tflows/action/view-action-flow.xml#view-action-flow", isNewTab, amap);
    }
    public void viewAction(ClientEvent clientEvent) {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(pfm.get("actionID")!=null)
        {
        String actionid = pfm.get("actionID").toString();
        String defaultActionTab =""+pfm.get("defaultActionTab");
        String id = pfm.get("actionID").toString();
        boolean isNewTab = false;
        if(actionid!=null)
        {
        Map amap = new HashMap<String, Object>();
        amap.put("actionid", actionid);
        amap.put("showView","all");
        amap.put("showToolbarView","toolbar");
        amap.put("defaultReportType","Pdf");
        amap.put("defaultView","viewactionbtf");
        amap.put("defaultActionTab",defaultActionTab);
        _launchActivity(id, "Action " + id, "/WEB-INF/tflows/action/view-action-flow.xml#view-action-flow", isNewTab, amap);
        }
        }
    }
    public void viewLetter(ClientEvent clientEvent) {
        SecurityContext scntx = ADFContext.getCurrent().getSecurityContext();
        boolean isNewTab = false;
        if(scntx.isUserInRole("APP_GRANT_UNIT"))
        {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String letterMemoId = pfm.get("letterMemoID").toString();

        if(letterMemoId!=null)
        {
        Map amap = new HashMap<String, Object>();
        amap.put("letterMemoID",letterMemoId);
        _launchActivity(letterMemoId, "Letter " + letterMemoId, "/WEB-INF/tflows/lettermemo/view-letter-memo-tf.xml#view-letter-memo-tf", isNewTab, amap);
        }
        }
        else
        {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Access to Approval Letter is restricted.", null));
        }
    }
    
    public void viewProject(ClientEvent clientEvent){
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String id = pfm.get("budgetFy")+"-"+pfm.get("projectNbr");
        String budgetFy=(String)pfm.get("budgetFy");
        String projectNbr=(String)pfm.get("projectNbr");
        String isEdit="false";
        Map map = new HashMap<String, Object>();
        map.put(BUDGET_YEAR, budgetFy);
        map.put(PROJECT_NUMBER, projectNbr);
        map.put("isNew",false);
        SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
        if (sc.isUserInRole("APP_FM") || sc.isUserInRole("APP_FINANCE"))
            map.put("trainStop", "Funds");
        else if (sc.isUserInRole("APP_BBL") || sc.isUserInRole("APP_OL") || sc.isUserInRole("APP_DL") || sc.isUserInRole("APP_TL"))
            map.put("trainStop", "Budget");
        else
            map.put("trainStop", "Basic");
        if("true".equals(isEdit))
            map.put("isEdit",true);   
        else
            map.put("isEdit",false);
        // now launch the tab
        _launchActivity(id,"Project "+id, "/WEB-INF/tflows/activity/view-activity-flow.xml#view-project-flow", false, map);
    }
    
    
    public void editAction(ActionEvent actionEvent) {
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String actionid = requestMap.get("actionid").toString();
        String id = requestMap.get("id").toString();
        Map amap = new HashMap<String, Object>();
        amap.put("actionid", actionid);
        amap.put("showView","all");
        amap.put("showToolbarView","toolbar");
        amap.put("defaultReportType","Pdf");
        amap.put("defaultView","edit");
        _launchActivity(id, "Action " + id, "/WEB-INF/tflows/action/view-action-flow.xml#view-action-flow", false, amap);
    }

    public void viewAward(ActionEvent actionEvent) {
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        BigDecimal awardId = (BigDecimal) requestMap.get("awardId");
        String awardNbr = requestMap.get("awardNbr").toString();
        Map amap = new HashMap<String, Object>();
        amap.put("defaultView",requestMap.get("defaultView"));
        amap.put("awardId", awardId);
        amap.put("tabContext", TabContext.getCurrentInstance());
        _launchActivity(awardId.toString(),"Award " + awardNbr , "/WEB-INF/tflows/award/view-award-flow.xml#view-award-flow", false, amap);
    }

    public void launchActionNavigator(ClientEvent clientEvent) {
        if (TabContext.getCurrentInstance().getSelectedTabIndex() == -1) {
            Map amap = new HashMap<String, Object>();
            amap.put("tabContext", TabContext.getCurrentInstance());
            _launchActivity("Action Navigator","Action Navigator", "/WEB-INF/tflows/action/search-actions-flow.xml#search-actions-flow", true,
                            amap);
            /*             this.writeJavaScriptToClient("document.getElementById(\"pt1:b1\").click();");
            FacesContext fctx = FacesContext.getCurrentInstance();
            ExtendedRenderKitService erks = null;
            erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class); */
        }
    }
    
    public void launchAwardNavigator(ClientEvent clientEvent) {
        if (TabContext.getCurrentInstance().getSelectedTabIndex() == -1) {
            Map amap = new HashMap<String, Object>();
            amap.put("tabContext", TabContext.getCurrentInstance());
            _launchActivity("Award Navigator","Award Navigator", "/WEB-INF/tflows/search-awards-alternate.xml#search-awards-alternate", true, amap);
        }
    }

    public void launchDocNavigator(ClientEvent clientEvent) {
        if (TabContext.getCurrentInstance().getSelectedTabIndex() == -1) {
            Map amap = new HashMap<String, Object>();
            amap.put("tabContext", TabContext.getCurrentInstance());
            _launchActivity("Document Navigator","Document Navigator", "/WEB-INF/tflows/search-documents-flow.xml#search-documents-flow", true, amap);
        }
    }

    public void launchProjectNavigator(ClientEvent clientEvent) {
        if (TabContext.getCurrentInstance().getSelectedTabIndex() == -1) {
            Map amap = new HashMap<String, Object>();
            amap.put("tabContext", TabContext.getCurrentInstance());
            _launchActivity("Project Navigator","Project Navigator", "/WEB-INF/tflows/activity/search-activity-flow.xml#search-project-flow", false, amap);
        }
    }

    public void navigationItemLsnr(ItemEvent itemEvent) {
        if ("remove".equals(itemEvent.getType().toString())) {
            UIComponent ui = (UIComponent) itemEvent.getSource();
            Map<String, Object> mapattrs = ui.getAttributes();
            Integer i = (Integer) mapattrs.get("tabIndex");
            TabContext.getCurrentInstance().removeTab(i.intValue());
        }
    }
    public void launchActivity(ActionEvent actionEvent) {
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String activity = requestMap.get("callActivity").toString();
        Map amap = new HashMap<String, Object>();
        amap.put("tabContext", TabContext.getCurrentInstance());
        switch (activity) {
        case "ActionSearch":
            
           
            _launchActivity("Action Navigator","Action Navigator", "/WEB-INF/tflows/action/search-actions-flow.xml#search-actions-flow", false,
                            amap);
            break;
        
        case "AwardSearch":            
           
            _launchActivity("Award Navigator","Award Navigator", "/WEB-INF/tflows/search-awards-flow.xml#search-awards-flow", false, amap);
            break;
        
        case "AwardNoTreeSearch":
            
            _launchActivity("Award Navigator","Award Navigator", "/WEB-INF/tflows/search-awards-alternate.xml#search-awards-alternate", false, amap);
            break;
        
        // search Category Actions view
        case "CategorySearch":
            _launchActivity("Category Navigator","Category Navigator","/WEB-INF/tflows/action/search-category-actions.xml#search-category-actions",
                            false, amap);
            break;    
        
        // search Projects Action view
        case "ProjectSearch":
            _launchActivity("Project Navigator","Project Navigator", "/WEB-INF/tflows/activity/search-activity-flow.xml#search-project-flow", false, amap);
            break;
        case "DocumentSearch":
                _launchActivity("Document Navigator","Document Navigator", "/WEB-INF/tflows/search-documents-flow.xml#search-documents-flow", false, amap);
                break;
        case "DisbursementSearch":
            _launchActivity("Disbursement","Disbursement", "/WEB-INF/tflows/disbursement/searchDisbursements-btf.xml#searchDisbursements", false, amap);
                break;
        }
        
        
    }
    
    /**
     * This method triggers the view Action by Category tab.
     * @param actionEvent
     */
    public void viewActionByCategoty(ActionEvent actionEvent){
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        BigDecimal actionId = (BigDecimal) requestMap.get(ACTION_ID_REQUEST_PARAMETER);
        String id = (String) requestMap.get(ID);
        Map map = new HashMap<String, Object>();
        map.put(ACTION_ID_REQUEST_PARAMETER, actionId);
        _launchActivity(id,SET_TAB_HEADER +id, "/WEB-INF/tflows/action/view-category-action-flow.xml#view-category-action-flow", false, map);
        
    }
    /**
     * This method triggers the Project Activity view by selecting / clicking on the Id on the project view tab.
     * @param event
     */
    public void viewProjectActivity(ActionEvent event){
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String id = (String) requestMap.get("id");
        Integer budgetFy=(Integer)requestMap.get("budgetFy");
        String projectNbr=(String)requestMap.get("projectNbr");
        String isEdit=(String)requestMap.get("isEdit");
        Map map = new HashMap<String, Object>();
        map.put(BUDGET_YEAR, budgetFy);
        map.put(PROJECT_NUMBER, projectNbr);
        map.put("isNew",false);
        SecurityContext sc = ADFContext.getCurrent().getSecurityContext();
        if (sc.isUserInRole("APP_FM") || sc.isUserInRole("APP_FINANCE"))
            map.put("trainStop", "Funds");
        else if (sc.isUserInRole("APP_BBL") || sc.isUserInRole("APP_OL") || sc.isUserInRole("APP_DL") || sc.isUserInRole("APP_TL"))
            map.put("trainStop", "Budget");
        else
            map.put("trainStop", "Basic");
        if("true".equals(isEdit))
            map.put("isEdit",true);   
        else
            map.put("isEdit",false);
        // now launch the tab
        _launchActivity(id,"Project "+id, "/WEB-INF/tflows/activity/view-activity-flow.xml#view-project-flow", false, map);
    }
    
    public String launchAwardNavigatorTab(){
        if(null == TabContext.getCurrentInstance().getContentArea() || null == TabContext.getCurrentInstance().getInnerToolbarArea() || null == TabContext.getCurrentInstance().getTabsNavigationPane()){
            return "award";
        }

//            ArrayList<SelectItem>  tabList = (ArrayList<SelectItem>) TabContext.getCurrentInstance().getTabList();
                Map amap = new HashMap<String, Object>();
                amap.put("tabContext", TabContext.getCurrentInstance());
                _launchActivity("Award Navigator","Award Navigator", "/WEB-INF/tflows/search-awards-alternate.xml#search-awards-alternate", false, amap);
 
        return "";
    }
    
    public String launchActionNavigatorTab(){
        Map amap = new HashMap<String, Object>();
        if(null == TabContext.getCurrentInstance().getContentArea() || null == TabContext.getCurrentInstance().getInnerToolbarArea() || null == TabContext.getCurrentInstance().getTabsNavigationPane()){
            return "action";
        }
        amap.put("tabContext", TabContext.getCurrentInstance());
        _launchActivity("Action Navigator","Action Navigator", "/WEB-INF/tflows/action/search-actions-flow.xml#search-actions-flow", false,
                        amap);
        this.writeJavaScriptToClient("document.getElementById(\"pt1:b1\").click();");
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExtendedRenderKitService erks = null;
        erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
        
        return "";
    }
    
    public String launchPorjectNavigatorTab(){
        
        if(null == TabContext.getCurrentInstance().getContentArea() || null == TabContext.getCurrentInstance().getInnerToolbarArea() || null == TabContext.getCurrentInstance().getTabsNavigationPane()){
            return "project";
        }

        Map amap = new HashMap<String, Object>();
        amap.put("tabContext", TabContext.getCurrentInstance());
        _launchActivity("Project Navigator","Project Navigator", "/WEB-INF/tflows/activity/search-activity-flow.xml#search-project-flow", false, amap);
        return "";
    }
    
    public String launchDisbursementNavigatorTab(){
        
        if(null == TabContext.getCurrentInstance().getContentArea() || null == TabContext.getCurrentInstance().getInnerToolbarArea() || null == TabContext.getCurrentInstance().getTabsNavigationPane()){
            return "disbursement";
        }

        Map amap = new HashMap<String, Object>();
        amap.put("tabContext", TabContext.getCurrentInstance());
        _launchActivity("Disbursement Navigator","Disbursements", "/WEB-INF/tflows/disbursement/searchDisbursements-btf.xml#searchDisbursements", false, amap);
        return "";
    }
    
    public void launchDisbursementNavigator(ClientEvent clientEvent) {
        if (TabContext.getCurrentInstance().getSelectedTabIndex() == -1) {
            Map amap = new HashMap<String, Object>();
            amap.put("tabContext", TabContext.getCurrentInstance());
            _launchActivity("Disbursement Navigator","Disbursements", "/WEB-INF/tflows/disbursement/searchDisbursements-btf.xml#searchDisbursements", false, amap);
        }
    }

    public void createActionReturnLsnr(ReturnEvent returnEvent) {
        Map<String, Object> pfs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(pfs.get("newActionID")!=null)
        {
        String actionid = pfs.get("newActionID").toString();
        String id = pfs.get("id").toString();
        Map amap = new HashMap<String, Object>();
        amap.put("actionid", actionid);
        amap.put("showView","all");
        amap.put("showToolbarView","toolbar");
        amap.put("defaultReportType","Pdf");
        amap.put("defaultView","edit");
        _launchActivity(id, "Action " + id, "/WEB-INF/tflows/action/view-action-flow.xml#view-action-flow", false, amap);
        }
    }

    public void launchDashboard() {
        if (TabContext.getCurrentInstance().getSelectedTabIndex() == -1) {
            Map amap = new HashMap<String, Object>();
            amap.put("tabContext", TabContext.getCurrentInstance());
            _launchActivity("Dashboard","Dashboard", "/WEB-INF/tflows/dashboard/ofda-dashboard-flow.xml#ofda-dashboard-flow", true,
                            amap);
        }
    }
    public void launchDashboard(ClientEvent clientEvent) {
        if (TabContext.getCurrentInstance().getSelectedTabIndex() == -1) {
            Map amap = new HashMap<String, Object>();
            amap.put("tabContext", TabContext.getCurrentInstance());
            _launchActivity("Dashboard","Dashboard", "/WEB-INF/tflows/dashboard/ofda-dashboard-flow.xml#ofda-dashboard-flow", true,
                            amap);
        }
    }

    public void launchAsist(ClientEvent clientEvent) {
        if (TabContext.getCurrentInstance().getSelectedTabIndex() == -1) {
            Map amap = new HashMap<String, Object>();
            amap.put("tabContext", TabContext.getCurrentInstance());
            amap.put("defaultVC", "ASIST");
            _launchActivity("ASIST Navigator","ASIST Navigator", "/WEB-INF/tflows/search-awards-alternate.xml#search-awards-alternate", false, amap);
        }
    }
        public void launchAsist() {
            if (TabContext.getCurrentInstance().getSelectedTabIndex() == -1) {
                Map amap = new HashMap<String, Object>();
                amap.put("tabContext", TabContext.getCurrentInstance());
                amap.put("defaultVC", "ASIST");
                _launchActivity("ASIST Navigator","ASIST Navigator", "/WEB-INF/tflows/search-awards-alternate.xml#search-awards-alternate", false, amap);
            }
    }
    public void launchContributionNavigator(ClientEvent clientEvent) {
        if (TabContext.getCurrentInstance().getSelectedTabIndex() == -1) {
            Map amap = new HashMap<String, Object>();
            amap.put("tabContext", TabContext.getCurrentInstance());
            _launchActivity("Contribution Navigator","Contribution Navigator", "/WEB-INF/tflows/contribution/search-contributions-flow.xml#search-contributions-flow", false, amap);
        }
    }
    public String launchContributionNavigatorTab(){
           Map amap = new HashMap<String, Object>();
           if(null == TabContext.getCurrentInstance().getContentArea() || null == TabContext.getCurrentInstance().getInnerToolbarArea() || null == TabContext.getCurrentInstance().getTabsNavigationPane()){
               return "contribution";
           }
           amap.put("tabContext", TabContext.getCurrentInstance());
           _launchActivity("Contribution Navigator","Contribution Navigator", "/WEB-INF/tflows/contribution/search-contributions-flow.xml#search-contributions-flow", false,
                           amap);
           this.writeJavaScriptToClient("document.getElementById(\"pt1:b1\").click();");
           FacesContext fctx = FacesContext.getCurrentInstance();
           ExtendedRenderKitService erks = null;
           erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
           
           return "";
       }
    public void viewContribution(ActionEvent actionEvent) {
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String paxActivitiesId = requestMap.get("paxActivitiesId").toString();
        String id = requestMap.get("id").toString();
        Object val = requestMap.get("newTab");
        boolean isNewTab = false;
        if(null != val){
            if(val.toString().equalsIgnoreCase("true")){
                isNewTab = true;
            }
        }
        Map amap = new HashMap<String, Object>();
        amap.put("paxActivitiesId", paxActivitiesId);
        amap.put("defaultReportType","Pdf");
        amap.put("defaultView","viewcontributionbtf");
        _launchActivity(id, "Contribution " + id, "/WEB-INF/tflows/contribution/view-contribution-flow.xml#view-contribution-flow", isNewTab, amap);
    }
    public void viewContribution(ClientEvent clientEvent) {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String paxActivitiesId = pfm.get("paxActivitiesId").toString();
        String id = pfm.get("paxActivitiesId").toString();
        boolean isNewTab = false;
        if(paxActivitiesId!=null)
        {
        Map amap = new HashMap<String, Object>();
        amap.put("paxActivitiesId", paxActivitiesId);
        amap.put("showToolbarView","toolbar");
        amap.put("defaultReportType","Pdf");
        amap.put("defaultView","viewcontributionbtf");
        _launchActivity(id, "Contribution " + id, "/WEB-INF/tflows/contribution/view-contribution-flow.xml#view-contribution-flow", isNewTab, amap);
        }
    }
    
    public void editContribution(ActionEvent actionEvent) {
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
        String paxActivitiesId = requestMap.get("paxActivitiesId").toString();
        String id = requestMap.get("id").toString();
        Object val = requestMap.get("newTab");
        boolean isNewTab = false;
        if(null != val){
            if(val.toString().equalsIgnoreCase("true")){
                isNewTab = true;
            }
        }
        Map amap = new HashMap<String, Object>();
        amap.put("paxActivitiesId", paxActivitiesId);
        amap.put("showToolbarView","toolbar");
        amap.put("defaultReportType","Pdf");
        amap.put("defaultView","edit");
        _launchActivity(id, "Contribution " + id, "/WEB-INF/tflows/contribution/view-contribution-flow.xml#view-contribution-flow", isNewTab, amap);
    }
    public void createContributionReturnLsnr(ReturnEvent returnEvent) {
        Map<String, Object> pfs = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(pfs.get("newPaxActivitiesId")!=null)
        {
        String paxActivitiesId = pfs.get("newPaxActivitiesId").toString();
        String id = pfs.get("newPaxActivitiesId").toString();
        Map amap = new HashMap<String, Object>();
        amap.put("paxActivitiesId", paxActivitiesId);
        amap.put("showToolbarView","toolbar");
        amap.put("defaultReportType","Pdf");
        amap.put("defaultView","edit");
       _launchActivity(id, "Contribution " + id, "/WEB-INF/tflows/contribution/view-contribution-flow.xml#view-contribution-flow",  false, amap);
       if(pfs.get("isEmailSent")!=null) {
           int delay=5;
           Boolean emailSent = (Boolean) pfs.get("isEmailSent");
           if (emailSent) {
               addToGrowl(GrowlType.mailok, "Contribution created notification sent.", 300 * delay++, (1000 * (delay * 5)));
           } else
               addToGrowl(GrowlType.mailerror, "Contribution created notification email failed!", 300 * delay++,
                          (1000 * (delay * 5)));
       }
        }
    }
}
