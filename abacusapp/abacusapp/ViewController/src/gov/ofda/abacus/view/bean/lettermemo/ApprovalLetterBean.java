package gov.ofda.abacus.view.bean.lettermemo;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.component.rich.input.RichInputListOfValues;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.LaunchPopupEvent;
import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class ApprovalLetterBean {
    public ApprovalLetterBean() {
    }

    public void approvalWithDrawnDateLnsr(ValueChangeEvent vce) {
        if(vce.getNewValue()==null) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
            Row r = iter.getCurrentRow();
            if(r!=null)
                r.setAttribute("ApvWithdrawComments", null);
        }
    }

    public void approvalRejectDateLsnr(ValueChangeEvent vce) {
        if(vce.getNewValue()==null) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iter = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
            Row r = iter.getCurrentRow();
            if(r!=null)
                r.setAttribute("ApvRejectComments", null);
            }
    }
    public void apvSplLaunchPopupLnsr(LaunchPopupEvent launchPopupEvent) {
        launchPopupLsnr(launchPopupEvent,"ApvSpl");
    }

    public void apvSplReturnPopupLnsr(ReturnPopupEvent returnPopupEvent) {
        returnPopupLnsr(returnPopupEvent);
    }
    public void ApvReviewIdLaunchPopupLnsr(LaunchPopupEvent launchPopupEvent) {
        launchPopupLsnr(launchPopupEvent,"ApvReviewSpl");
    }

    public void ApvReviewIdReturnPopupLnsr(ReturnPopupEvent returnPopupEvent) {
        returnPopupLnsr(returnPopupEvent);
    }
    public void ApvClearIdLaunchPopupLnsr(LaunchPopupEvent launchPopupEvent) {
        launchPopupLsnr(launchPopupEvent,"ApvClearanceSpl");
    }

    public void ApvClearIdReturnPopupLnsr(ReturnPopupEvent returnPopupEvent) {
        returnPopupLnsr(returnPopupEvent);
    }
    private void launchPopupLsnr(LaunchPopupEvent launchPopupEvent, String spl) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
        Row r = iter.getCurrentRow();
        if (r != null) {
            OperationBinding exec = bindings.getOperationBinding("setSpecialistLOV");
            exec.getParamsMap().put("p_user_id",r.getAttribute(spl));
            exec.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(launchPopupEvent.getComponent().getParent());
        }
    }
    private void returnPopupLnsr(ReturnPopupEvent returnPopupEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("ApprovalLettersSpecialistEditLOV1Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row r = rsi.getCurrentRow();
        if (r != null) {
            ((RichInputListOfValues)returnPopupEvent.getComponent()).setValue(r.getAttribute("Userid"));
            ((RichInputListOfValues)returnPopupEvent.getComponent()).processUpdates(FacesContext.getCurrentInstance());
            AdfFacesContext.getCurrentInstance().addPartialTarget(returnPopupEvent.getComponent());
        }
        rsi.closeRowSetIterator();
    }
}
