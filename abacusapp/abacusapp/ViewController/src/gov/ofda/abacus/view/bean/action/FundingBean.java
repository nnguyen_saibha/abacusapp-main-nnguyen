package gov.ofda.abacus.view.bean.action;

import gov.ofda.abacus.view.base.UIControl;

import gov.ofda.abacus.view.bean.message.MessageBean;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.event.ReturnEvent;

/**
 * Used by Funding tab to manage funds
 */
public class FundingBean extends UIControl {
    @SuppressWarnings("compatibility:3165101812967795879")
    private static final long serialVersionUID = 1L;

    public FundingBean() {
        super();
    }

    /**
     * @param actionEvent
     * Check for detail reference (Valid only for Parent in Set) to fund before delete
     */
    public void deleteFundCode(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        exec=bindings.getOperationBinding("isDeleteFund");
        exec.execute();
        Boolean check = (Boolean) exec.getResult();
        if(check)
        {
        exec = bindings.getOperationBinding("Delete");
        exec.execute();
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null,
                                                         new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                          "Fund Code is referred in details and cannot be deleted"));
        }
        
    }
    public void addFundsReturnLsnr(ReturnEvent returnEvent) {
        Map pfp=AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String result = (String) pfp.get("confirmMessage");
        MessageBean mb=new MessageBean();
        mb.sendActivityFundTransferEmail(result, (Map) pfp.get("transferDetails"),(Boolean) pfp.get("isSendEmail"));
    }
}
