package gov.ofda.abacus.view.bean.lettermemo;

import gov.ofda.abacus.view.base.JSFUtils;
import gov.ofda.abacus.view.base.UIControl;
import gov.ofda.abacus.view.base.UIControl.GrowlType;
import gov.ofda.abacus.view.bean.AppBean;

import gov.ofda.abacus.view.bean.message.MessageBean;

import java.io.File;

import java.io.IOException;

import java.math.BigDecimal;

import java.net.MalformedURLException;
import java.net.URL;

import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.LaunchPopupEvent;

import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowIterator;
import oracle.jbo.RowSetIterator;

import org.apache.commons.io.FileUtils;
import org.apache.myfaces.trinidad.event.DisclosureEvent;
import org.apache.myfaces.trinidad.event.PollEvent;
import org.apache.myfaces.trinidad.event.ReturnEvent;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class EditLetterMemoBean extends UIControl{
    public static final String DEFAULT_FORMAT="Pdf";
    public static final String DEFAULT_REPORT="GENERATE_LETTER.RDF";

    public static ADFLogger logger = ADFLogger.createADFLogger(EditLetterMemoBean.class);

    public EditLetterMemoBean() {
        super();
    }

    public String checkLetterMemoValidity() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        String linkID = (String) pfm.get("linkID");
        String letterMemoTypeCode = (String) pfm.get("letterMemoTypeCode");
        Object letterMemoID = (Object) pfm.get("letterMemoID");
        if (letterMemoID == null && (linkID == null || letterMemoTypeCode == null)) {
            pfm.put("errorMessage",
                    "Missing required parameters: Letter ID=" + letterMemoID + " Link Details:" + linkID +
                    " Letter Type: " + letterMemoTypeCode);
            return "error";
        }
        if (letterMemoID == null) {
            String t[] = linkID.split("-");
            if (t.length != 2 || !("AWARD".equals(t[0]) || "ACTION".equals(t[0]))) {
                pfm.put("errorMessage",
                        "Invalid format for Link Details(GROUPNAME-LINKSEQ) Supported groups are AWARD and ACTION:" +
                        linkID);
                return "error";
            }
            //Check if passed combination of group and letter type code is valid
            OperationBinding exec = bindings.getOperationBinding("checkLetterMemoTypeCode");
            exec.getParamsMap().put("p_letterTypeCode", letterMemoTypeCode);
            exec.getParamsMap().put("p_leterMemoGroupCode", t[0]);
            exec.execute();
            DCIteratorBinding iter = bindings.findIteratorBinding("LetterMemoTypeCheckView1Iterator");
            if (iter.getCurrentRow() == null) {
                pfm.put("errorMessage",
                        "Invalid Letter Memo Group(" + t[0] + ") and Letter Memo Code(" + letterMemoTypeCode +
                        ") combination");
                return "error";
            }

        }

        return "next";
    }

    public String checkIsValid() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        String linkID = (String) pfm.get("linkID");
        String letterMemoTypeCode = (String) pfm.get("letterMemoTypeCode");
        String t[] = linkID.split("-");
        if (t.length == 2) {
            OperationBinding exec = bindings.getOperationBinding("isLetterMemoValid");
            exec.getParamsMap().put("letterMemoType", letterMemoTypeCode);
            exec.getParamsMap().put("letterMemoGroupCode", t[0]);
            exec.getParamsMap().put("id", t[1]);
            exec.execute();
            String result = (String) exec.getResult();
            if (letterMemoTypeCode != null && letterMemoTypeCode.equals(result))
                return "yes";
            else
                pfm.put("errorMessage", result);
        } else
            pfm.put("errorMessage", "Invalid link ID:" + linkID);
        return "no";
    }

    public String setLetterMemoID() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
        exec.execute();
        DCIteratorBinding iter = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
        Row r = iter.getCurrentRow();
        if (r != null)
            return "next";
        else {
            pfm.put("errorMessage", "Letter Memo not found:" + pfm.get("letterMemoID"));
        }
        return "error";
    }

    public String createNewLetterMemo() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if (pfm.get("letterMemoTypeCode") != null && pfm.get("linkID") != null) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding exec = bindings.getOperationBinding("CreateInsert");
            exec.execute();
            DCIteratorBinding iter = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
            Row r = iter.getCurrentRow();
            r.setAttribute("LetterMemoTypeCode", pfm.get("letterMemoTypeCode"));
            exec = bindings.getOperationBinding("createLink");
            exec.getParamsMap().put("value", pfm.get("linkID"));
            exec.execute();
            r.setAttribute("LinkCol", pfm.get("linkID"));
            String isAwardeeValid = (String) r.getAttribute("IsAwardeeValid");
            if ("Y".equals(isAwardeeValid))
            {
                exec = bindings.getOperationBinding("Commit");
                exec.execute();
                pfm.put("letterMemoID", r.getAttribute("LetterMemoId"));
                return "next";
            }
            else
                pfm.put("errorMessage", isAwardeeValid);
        } else
            pfm.put("errorMessage", "Missing Letter Type Code or Letter Memo Link Information");
        return "error";
    }

    public String checkAwardeeAndSetAOR() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        SecurityContext scntx = ADFContext.getCurrent().getSecurityContext();
        DCIteratorBinding iter = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
        Row r = iter.getCurrentRow();
        if(r!=null)
        {
            pfm.put("defaultReportType",r.getAttribute("DefaultFormat"));
            pfm.put("isMultiple",r.getAttribute("IsMultiple"));
            if("APV".equals(r.getAttribute("LetterMemoTypeCode")) && !scntx.isUserInRole("APP_GRANT_UNIT")) {
                pfm.put("isEditMode",false);
                pfm.put("isReadOnly",false);
            }
            else if(pfm.get("isReadOnly")!=null && (Boolean)pfm.get("isReadOnly"))
            {
                pfm.put("isEditMode",false);
            }
            else
            {
                pfm.put("isEditMode",r.getAttribute("IsEditable"));
                pfm.put("isReadOnly",false);
            }
            pfm.put("letterMemoType",r.getAttribute("LetterMemoTypeCode"));
        }
        else
            pfm.put("defaultReportType",DEFAULT_FORMAT);
        OperationBinding execDocs = bindings.getOperationBinding("getValidDocTypeCodeList");
        execDocs.execute();
        List<String> dList = (List<String>) execDocs.getResult();
        pfm.put("validDocTypeList",dList);
        if (r != null) {
            String isAwardeeValid = (String) r.getAttribute("IsAwardeeValid");
            String awardeeCode = (String) r.getAttribute("AwardeeCode");

            if ("Y".equals(isAwardeeValid)) {
                if (awardeeCode == null) {
                    pfm.put("errorMessage", "Missing Awardee");
                    return "error";
                }
                String t = (String) r.getAttribute("LinkCol");
                pfm.put("linkID", t);
                if (t != null && t.split("-").length == 2) {
                    String tmp[] = t.split("-");
                    if ("AWARD".equals(tmp[0])) {
                        OperationBinding exec = bindings.getOperationBinding("setAwardDetailsView");
                        exec.getParamsMap().put("p_award_seq", tmp[1]);
                        exec.execute();
                        if ((Boolean) r.getAttribute("IsEditable") &&
                            !"PAL".equals(r.getAttribute("LetterMemoTypeCode"))) {
                            DCIteratorBinding iter2 = bindings.findIteratorBinding("AwardDetailsView2Iterator");
                            Row rd = iter2.getCurrentRow();
                            if (rd != null) {
                                exec = bindings.getOperationBinding("setAORListView");
                                exec.getParamsMap().put("p_action_id", rd.getAttribute("AwardId"));
                                exec.execute();
                            }
                        }
                        pfm.put("letterMemoGroupCode", "AWARD");
                    } else if ("ACTION".equals(tmp[0])) {
                        OperationBinding exec = bindings.getOperationBinding("setActionDetailsView");
                        exec.getParamsMap().put("p_action_id", tmp[1]);
                        exec.execute();
                        if ((Boolean) r.getAttribute("IsEditable") &&
                            !"PAL".equals(r.getAttribute("LetterMemoTypeCode"))) {
                            DCIteratorBinding iter2 = bindings.findIteratorBinding("ActionDetailsView2Iterator");
                            Row rd = iter2.getCurrentRow();
                            exec = bindings.getOperationBinding("setAORListView");
                            if("SGA".equals(r.getAttribute("LetterMemoTypeCode")))
                                exec.getParamsMap().put("p_action_id", rd.getAttribute("ParentActionId"));
                            else
                            exec.getParamsMap().put("p_action_id", tmp[1]);
                            exec.execute();
                        }
                        pfm.put("letterMemoGroupCode", "ACTION");
                    } else {
                        pfm.put("errorMessage", "Invalid Link Col:" + t);
                        return "error";
                    }
                } else {
                    pfm.put("errorMessage", "Invalid Link Col:" + t);
                    return "error";
                }
                return "next";
            }

            else
                pfm.put("errorMessage", isAwardeeValid);
        }
        return "error";
    }

    public String setPALPMLDetails() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();

        String t = (String) pfm.get("linkID");
        if (t != null && t.split("-").length == 2) {
            String tmp[] = t.split("-");
            OperationBinding exec = bindings.getOperationBinding("ExecuteWithParams");
            exec.getParamsMap().put("p_action_id", tmp[1]);
            exec.execute();
            DCIteratorBinding iter = bindings.findIteratorBinding("PAEditView1Iterator");
            if (iter.getCurrentRow() == null) {
                pfm.put("errorMessage", "Unable to find Action: " + t);
                return "error";
            }
            return "next";
        }
        pfm.put("errorMessage", "Invalid Link ID: " + t);
        return "error";
    }

    public String setSGADetails() {
        String result = setPALPMLDetails();
        if (result.equals("error"))
            return "error";
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterLetterEdit = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
        Row r = iterLetterEdit.getCurrentRow();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String t = (String) pfm.get("linkID");
        String actionIdStr = null;
        if (t != null && t.split("-").length == 2) {
            String tmp[] = t.split("-");
            actionIdStr = tmp[1];
        }
        if (r != null) {
            if ((Boolean) r.getAttribute("IsEditable")) {
                OperationBinding exec = bindings.getOperationBinding("ExecuteSGA");
                exec.execute();
                DCIteratorBinding iter = bindings.findIteratorBinding("PaSubgrantLetterView1Iterator");
                if (iter.getEstimatedRowCount()==0 && iter.getCurrentRow() == null) {
                    exec = bindings.getOperationBinding("CreateInsert");
                    exec.execute();
                    Row rsga=iter.getCurrentRow();
                    if(rsga!=null){
                        rsga.setAttribute("LetterDate", new Date());
                        rsga.setAttribute("ActionId",new BigDecimal(actionIdStr));
                    }
                    exec = bindings.getOperationBinding("Commit");
                    exec.execute();
                    pfm.put("letterMemoID", r.getAttribute("LetterMemoId"));
                    }
                }
            return "next";
        }
        return "error";
    }

    public String setTADetails() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCIteratorBinding iterLetterEdit = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
        Row r = iterLetterEdit.getCurrentRow();
        if (r != null) {
            if ((Boolean) r.getAttribute("IsEditable")) {
                OperationBinding exec = bindings.getOperationBinding("ExecuteTAApproval");
                exec.execute();
                DCIteratorBinding iter = bindings.findIteratorBinding("PaTaApprovalView1Iterator");
                if (iter.getEstimatedRowCount()==0 && iter.getCurrentRow() == null) {
                    exec = bindings.getOperationBinding("CreateInsert");
                    exec.execute();
                    r = iter.getCurrentRow();
                    if (r != null)
                        r.setAttribute("LetterDate", new Date());
                    exec = bindings.getOperationBinding("Commit");
                    exec.execute();
                    pfm.put("letterMemoID", r.getAttribute("LetterMemoId"));
                    }

                }
            return "next";
        }
        return "error";
    }
    
    public String setACLDetails() {
    Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
    pfm.put("showACLNotes", true);
    DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
    DCIteratorBinding iterLetterEdit = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
    Row r = iterLetterEdit.getCurrentRow();
    if (r != null) {
        if ((Boolean) r.getAttribute("IsEditable")) {
            OperationBinding exec = bindings.getOperationBinding("ExecuteACLDetails");
            exec.execute();
            DCIteratorBinding iter = bindings.findIteratorBinding("ActivityChecklistView1Iterator");
            if (iter.getEstimatedRowCount()==0 && iter.getCurrentRow() == null) {
                exec = bindings.getOperationBinding("setNewACL");
                exec.execute();
                exec = bindings.getOperationBinding("Commit");
                exec.execute();
                pfm.put("letterMemoID", r.getAttribute("LetterMemoId"));
                exec = bindings.getOperationBinding("ExecuteACLDetails");
                exec.execute();
            }
        }
        return "next";
    }
    return "error";
    }
    
    public String setINADetails() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        Map ssm=ADFContext.getCurrent().getSessionScope();
        /*         if(ssm.get("width")!=null && Integer.parseInt((String)ssm.get("width"))>1366)
            pfm.put("showINANotes", false);
        else */
        pfm.put("showINANotes", true);
        DCIteratorBinding iterLetterEdit = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
        Row r = iterLetterEdit.getCurrentRow();
        if (r != null) {
            if ((Boolean) r.getAttribute("IsEditable")) {
                
                pfm.put("oldAor", r.getAttribute("Aor"));
                
                OperationBinding exec = bindings.getOperationBinding("ExecutePAInaSection");
                exec.execute();
                exec = bindings.getOperationBinding("ExecutePaInaColumn");
                exec.execute();
                exec = bindings.getOperationBinding("ExecutePADetails");
                exec.execute();
                DCIteratorBinding iter = bindings.findIteratorBinding("PaInaSectionView1Iterator");
                if (iter.getEstimatedRowCount()==0 && iter.getCurrentRow() == null) {
                    exec = bindings.getOperationBinding("setNewINA");
                    exec.execute();
                    exec = bindings.getOperationBinding("Commit");
                    exec.execute();
                    pfm.put("letterMemoID", r.getAttribute("LetterMemoId"));
                    
                    exec = bindings.getOperationBinding("ExecutePAInaSection");
                    exec.execute();
                    exec = bindings.getOperationBinding("ExecutePaInaColumn");
                    exec.execute();
                    exec = bindings.getOperationBinding("ExecutePADetails");
                    exec.execute();
                    
                }
            }
            OperationBinding exec = bindings.getOperationBinding("ExecuteInaPAMainSection");
            DCIteratorBinding painadetails = bindings.findIteratorBinding("InaPADetails1Iterator");
            Row parow = painadetails.getCurrentRow();
            if(parow!=null)
            {
            exec.getParamsMap().put("p_awardee_type_code", parow.getAttribute("AwardeeTypeCode"));
            exec.getParamsMap().put("p_action_type_code", parow.getAttribute("ActionTypeCode"));
            exec.getParamsMap().put("p_fat", parow.getAttribute("FundingActionType"));    
            }
            exec.execute();
            exec=bindings.getOperationBinding("checkMissingINADetails");
            exec.execute();
            List incompleteINASec = (List) exec.getResult();
            pfm.put("missingSectionList", incompleteINASec);
            return "next";
        }
        return "error";
    }

    public void generateLetterMemo(ActionEvent ae) {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String missingValues=checkRequiredValues();
        if(missingValues!=null&& missingValues.length()==0)
        {
        StringBuilder sb = new StringBuilder();
        String redirectURL = null;
        AppBean appBean = (AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean");
        String url = appBean.getConstants().get("URL");
        String instance = appBean.getConstants().get("INSTANCE");
        if (null != url && null != instance) {
            DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
            DCIteratorBinding iterLetterEdit = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
            Row r = iterLetterEdit.getCurrentRow();
            String letterTypeCode=null;
            if(r!=null)
            {
                letterTypeCode = (String) r.getAttribute("LetterMemoTypeCode");
                if("INA".equals(letterTypeCode)) {
                    NavigationHandler  nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
                    if(nvHndlr!=null)
                        nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "printINA");
                    return;
                }
                
                String reportName=DEFAULT_REPORT;
                String linkCol=(String)r.getAttribute("LinkCol");
                sb = new StringBuilder();
                if(r.getAttribute("ReportName")!=null)
                    reportName = (String) r.getAttribute("ReportName");
                sb.append(url);
                sb.append("/reports/rwservlet?Rep"+instance);
                String tmpFormat=DEFAULT_FORMAT;
                if(pfm.get("defaultReportType")!=null)
                {
                    sb.append(pfm.get("defaultReportType"));
                    tmpFormat = (String) pfm.get("defaultReportType");
                }
                else
                    sb.append(DEFAULT_FORMAT);
                sb.append("+report="+reportName);
                String tmp[]=linkCol.split("-");
                if(tmp.length==2)
                {
                  sb.append("+P_"+tmp[0]+"_ID="+tmp[1]);
                  sb.append("+P_"+tmp[0]+"_SEQ="+tmp[1]);
                        switch (letterTypeCode) {
                        case "PAL":
                            sb.append("+p_awardee_contact_seq=");
                            sb.append(r.getAttribute("AwardeeContactSeq"));
                            sb.append("+p_awardee_add_seq=");
                            sb.append(r.getAttribute("AwardeeAddSeq"));
                            sb.append("+p_aotr_id=");
                            sb.append(r.getAttribute("ContractOfficer"));
                            sb.append("+p_letter_type=" + letterTypeCode);
                            break;
                        case "PML":
                            sb.append("+p_awardee_contact_seq=");
                            sb.append(r.getAttribute("AwardeeContactSeq"));
                            sb.append("+p_awardee_add_seq=");
                            sb.append(r.getAttribute("AwardeeAddSeq"));
                            sb.append("+p_aotr_id=");
                           // sb.append(r.getAttribute("Aor"));
                           sb.append(r.getAttribute("ContractOfficer"));
                            sb.append("+p_letter_type=" + letterTypeCode);
                            break;
                        case "NCE":
                            sb.append("+p_awardee_contact_seq=");
                            sb.append(r.getAttribute("AwardeeContactSeq"));
                            sb.append("+p_awardee_add_seq=");
                            sb.append(r.getAttribute("AwardeeAddSeq"));
                            sb.append("+p_aotr_id=");
                            sb.append(r.getAttribute("Aor"));
                            sb.append("+p_letter_type=" + letterTypeCode);
                            break;
                        case "TA":
                            sb.append("+p_awardee_contact_seq=");
                            sb.append(r.getAttribute("AwardeeContactSeq"));
                            sb.append("+p_awardee_add_seq=");
                            sb.append(r.getAttribute("AwardeeAddSeq"));
                            sb.append("+p_aotr_id=");
                            sb.append(r.getAttribute("Aor"));
                            DCIteratorBinding taIter = bindings.findIteratorBinding("PaTaApprovalView1Iterator");
                            Row taRow = taIter.getCurrentRow();
                            if(taRow!=null) {
                                sb.append("+p_ta_approval_seq="+taRow.getAttribute("TaApprovalSeq"));
                            }
                            sb.append("+p_letter_type=" + letterTypeCode);
                            break;
                        case "SGA":
                            sb.append("+p_awardee_contact_seq=");
                            sb.append(r.getAttribute("AwardeeContactSeq"));
                            sb.append("+p_awardee_add_seq=");
                            sb.append(r.getAttribute("AwardeeAddSeq"));
                            sb.append("+p_aotr_id=");
                            sb.append(r.getAttribute("Aor"));
                            sb.append("+p_letter_type=" + letterTypeCode);
                            break;
                        case "ACL":
                            break;
                        case "INA":
                            sb.append("+p_aotr_id=");
                            sb.append(r.getAttribute("Aor"));
                            sb.append("+p_letter_type=" + letterTypeCode);
                            break;
                        default:
                            sb=new StringBuilder();

                        }
                        if(sb.length()>0)
                            sb.append("+DESNAME=" + letterTypeCode + "."+tmpFormat);
                redirectURL = sb.toString();
                }
            }
            if (redirectURL != null && redirectURL.length()>0) {
                FacesContext fctx = FacesContext.getCurrentInstance();
                ExtendedRenderKitService erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
                StringBuilder script = new StringBuilder();
                script.append("window.open('" + redirectURL + "', '_blank');");
                erks.addScript(FacesContext.getCurrentInstance(), script.toString());
            }
        }

        }
        else {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Missing required values","Missing required values: "+missingValues);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public String checkRequiredValues() {
        StringBuilder sb = new StringBuilder();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterLetterEdit = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
        Row r = iterLetterEdit.getCurrentRow();
        DCIteratorBinding iterPAEdit = bindings.findIteratorBinding("PAEditView1Iterator");
        Row paRow = iterPAEdit.getCurrentRow();

        if (r != null) {
            String letterTypeCode = (String) r.getAttribute("LetterMemoTypeCode");
            switch (letterTypeCode) {
            case "PAL":
                if (paRow == null)
                    sb.append("Missing Action Details");
                else {
                    if (paRow.getAttribute("ProposalName") == null)
                        sb.append("Activity Name, ");
                    if (paRow.getAttribute("ProposalRecDate") == null)
                        sb.append("Application Received Date, ");
                    if (paRow.getAttribute("PalAmt") == null)
                        sb.append("PAL Amount, ");
                    if (paRow.getAttribute("PalStartDate") == null)
                        sb.append("PAL Start Date, ");
                }
                if (r.getAttribute("AwardeeContactSeq") == null)
                    sb.append("Awardee Contact, ");
                if (r.getAttribute("AwardeeAddSeq") == null)
                    sb.append("Awardee Address, ");
                if (r.getAttribute("ContractOfficer") == null)
                    sb.append("Agreement Officer.");
                break;
            case "PML":
                if (paRow == null)
                    sb.append("Missing Action Details");
                else {
                    if (paRow.getAttribute("PmlReqDate") == null)
                        sb.append("PML Request Date, ");
                    if (paRow.getAttribute("PmlEndDate") == null)
                        sb.append("PML End Date, ");
                }
                if (r.getAttribute("AwardeeContactSeq") == null)
                    sb.append("Awardee Contact, ");
                if (r.getAttribute("AwardeeAddSeq") == null)
                    sb.append("Awardee Address, ");
               // if (r.getAttribute("Aor") == null)
               //   sb.append("AOR, ");
               if (r.getAttribute("ContractOfficer") == null)
                                sb.append("Agreement Officer.");
                break;
            case "NCE":

                if (paRow == null)
                    sb.append("Missing Action Details");
                else {
                    if (paRow.getAttribute("NceDate") == null)
                        sb.append("NCE Date, ");
                }
                if (r.getAttribute("AwardeeContactSeq") == null)
                    sb.append("Awardee Contact, ");
                if (r.getAttribute("AwardeeAddSeq") == null)
                    sb.append("Awardee Address, ");
                if (r.getAttribute("Aor") == null)
                    sb.append("AOR, ");
                break;
            case "TA":

                DCIteratorBinding taIter = bindings.findIteratorBinding("PaTaApprovalView1Iterator");
                Row taRow = taIter.getCurrentRow();
                if(taRow!=null) {
                    DCIteratorBinding taDetailsIter = bindings.findIteratorBinding("PaTaApprovalDetailsView1Iterator");
                    long ct=taDetailsIter.getEstimatedRowCount();
                    if(taRow.getAttribute("LetterDate")==null)
                        sb.append("Letter Date, ");
                    if(taRow.getAttribute("RefDate")==null)
                        sb.append("Reference Date, ");
                    if(taRow.getAttribute("RefSubject")==null)
                        sb.append("Reference Subject, ");
                    if(taRow.getAttribute("Purpose")==null)
                        sb.append("Purpose, ");
                    if(ct<=0)
                        sb.append("Trip Details, ");
                }
                else
                    sb.append("Missing travel details");
                if (r.getAttribute("AwardeeContactSeq") == null)
                    sb.append("Awardee Contact, ");
                if (r.getAttribute("AwardeeAddSeq") == null)
                    sb.append("Awardee Address, ");
                if (r.getAttribute("Aor") == null)
                    sb.append("AOR, ");
                
                break;
            case "SGA":

                if (paRow == null)
                    sb.append("Missing Action Details");
                else {
                    if (paRow.getAttribute("ApprovedAmt") == null)
                        sb.append("Approved Amount, ");
                }
                DCIteratorBinding sgIter = bindings.findIteratorBinding("PaSubgrantLetterView1Iterator");
                Row sgRow = sgIter.getCurrentRow();
                if(sgRow!=null) {
                    if(sgRow.getAttribute("LetterDate")==null)
                        sb.append("Letter Date, ");
                    if(sgRow.getAttribute("Refa")==null)
                        sb.append("Reference A, ");
                    if(sgRow.getAttribute("Refb")==null)
                        sb.append("Reference B, ");
                    if(sgRow.getAttribute("Purpose")==null)
                        sb.append("Purpose, ");
                    if(sgRow.getAttribute("Details")==null)
                        sb.append("Details, ");
                    if(sgRow.getAttribute("EffectiveDate1")==null)
                        sb.append("Effective Date, ");
                }
                else
                    sb.append("Missing details");
                if (r.getAttribute("AwardeeContactSeq") == null)
                    sb.append("Awardee Contact, ");
                if (r.getAttribute("AwardeeAddSeq") == null)
                    sb.append("Awardee Address, ");
                if (r.getAttribute("Aor") == null)
                    sb.append("AOR, ");
                break;
            case "INA":
                if (r.getAttribute("Aor") == null)
                    sb.append("AOR, ");
                OperationBinding exec = bindings.getOperationBinding("checkMissingINADetails");
                exec.execute();
                List<String> missingSecList=(List<String>)exec.getResult();
                for(String m:missingSecList)
                  sb.append(m+", ");                
                break;
            case "ACL":
                break;
            default:
                sb.append("Unknown Letter Type:" + letterTypeCode);
            }

        }
        return sb.toString();
    }
    
    public void deleteDialogLsnr(DialogEvent de) {
            if (DialogEvent.Outcome.yes == de.getOutcome()) {
                NavigationHandler  nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
                if(nvHndlr!=null)
                    nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "delete");
            }
    }
    public void processSaveCancelPoll(PollEvent event) {
        Map pfp = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        pfp.put("showMessage", null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(event.getComponent().getParent().getParent());
    }

    public void saveChanges(ActionEvent ae) {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        Boolean isCommitEnabled = (Boolean) JSFUtils.resolveExpression("#{bindings.Commit.enabled}");
        if (!isCommitEnabled) {
            //pfm.put("showMessage", "No changes to save");
            this.addToGrowl(GrowlType.notice, "No changes to save", 0, 5000);
            return;
        }
        exec = bindings.getOperationBinding("Commit");
        exec.execute();
        if (exec.getErrors().isEmpty()) {
            //pfm.put("showMessage", "Saved successfully");
            this.addToGrowl(GrowlType.notice, "Saved successfully", 0, 5000);
            pfm.put("errorMessages", null);
            DCIteratorBinding iter = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
            Row r = iter.getCurrentRow();
            String letterTypeCode = (String) r.getAttribute("LetterMemoTypeCode");
            pfm.put("oldAor", r.getAttribute("Aor")); 
            if("INA".equals(letterTypeCode))
            {
                exec=bindings.getOperationBinding("checkMissingINADetails");
                exec.execute();
                List<String> incompleteINASec = (List<String>) exec.getResult();
                pfm.put("missingSectionList", incompleteINASec);
            }
            if (pfm.get("letterMemoID") == null|| "APV".equals(letterTypeCode)) {
                if (r != null) {
                    pfm.put("letterMemoID", r.getAttribute("LetterMemoId"));
                    OperationBinding exec1 = bindings.getOperationBinding("ExecuteLetterMemoEdit");
                    exec1.execute();
                }
            }
            if ("APV".equals(letterTypeCode)) {
                approvalLettersPostCommit();
            } 
        }
    }
    public void cancelChanges(ActionEvent ae) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Rollback");
        exec.execute();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if (exec.getErrors().isEmpty())
        {
            //pfm.put("showMessage","Cancelled successfully");
            this.addToGrowl(GrowlType.notice, "Cancelled successfully", 0, 5000);
            NavigationHandler  nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            DCIteratorBinding iterLetterEdit = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
            Row r = iterLetterEdit.getCurrentRow();
            String letterTypeCode = (String) r.getAttribute("LetterMemoTypeCode");
            if("APV".equals(letterTypeCode))
            {
                setInitialTrackingValues();
            }
            if(nvHndlr!=null)
                nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "cancel");
        }
    }
    
    public void addNewAddressButtonReturnListner(ReturnEvent returnEvent){
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String addressSeq = (String) pageFlowMap.get("newAwardeeAddressSeq");
        
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = (DCIteratorBinding) bindings.findIteratorBinding("LetterMemoEditView1Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row row = rsi.getCurrentRow();
        row.setAttribute("AwardeeAddSeq", addressSeq);
        AdfFacesContext.getCurrentInstance().addPartialTarget(returnEvent.getComponent().getParent().getParent());
    }
    
    public void addNewContactButtonReturnListner(ReturnEvent returnEvent){
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String contactSeq = (String) pageFlowMap.get("newAwardeeContactSeq");
        
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = (DCIteratorBinding) bindings.findIteratorBinding("LetterMemoEditView1Iterator");
        RowSetIterator rsi = iter.getRowSetIterator();
        Row row = rsi.getCurrentRow();
        row.setAttribute("AwardeeContactSeq", contactSeq);
        AdfFacesContext.getCurrentInstance().addPartialTarget(returnEvent.getComponent().getParent().getParent());
    }

    public void setDocumentDetails() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        String t = (String) pfm.get("linkID");
        if (t != null && t.split("-").length == 2) {
            String inSourceType="3";
            String tmp[] = t.split("-");
            if("AWARD".equals(tmp[0]))
            {
            DCIteratorBinding iter2 = bindings.findIteratorBinding("AwardDetailsView2Iterator");
            Row rd = iter2.getCurrentRow();
            if (rd != null) 
                 tmp[1] = ((BigDecimal)rd.getAttribute("AwardId")).toString();
            inSourceType="2";
            }
            pfm.put("inSourceType", inSourceType);
            pfm.put("inSourceId",tmp[1]);
        }
        OperationBinding execDocs = bindings.getOperationBinding("getValidDocTypeCodeList");
        execDocs.execute();
        List<String> dList = (List<String>) execDocs.getResult();
        pfm.put("validDocTypeList",dList);
    }
    
    public void closeDocuments(ActionEvent actionEvent){
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("Commit");
        if(exec.isOperationEnabled()){
            FacesContext.getCurrentInstance().addMessage(null,
                                                         new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                                                          "Unsaved changes. Please save or cancel to proceed."));            
        }else{
            
            RichPopup popup =
                (RichPopup) actionEvent.getComponent().getParent().getParent().getParent().getParent();
        popup.hide();   
    }
    }
    
    public void detailsDisclosureListner(DisclosureEvent disclosureEvent) {
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if(disclosureEvent.isExpanded()){
            pageFlowMap.put("topHeaderWidth", "145");    
            
        }else{
            pageFlowMap.put("topHeaderWidth", "75");
        }
    }
    
    public void awardeeAddressButtonReturnListner(ReturnEvent returnEvent) {
        // Add event code here... executeAwardeeAddressContactIterator
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("executeAwardeeAddressContactIterator");
        exec.execute();
    }

    public void setOtherDetails() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        Object letterMemoID = (Object) pfm.get("letterMemoID");
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterLetterEdit = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
        Row r = iterLetterEdit.getCurrentRow();
        String letterTypeCode = (String) r.getAttribute("LetterMemoTypeCode");
        if("APV".equals(letterTypeCode))
        {
            if(r.getAttribute("ApvStatus")==null)
            {
                r.setAttribute("ApvStatus", new BigDecimal(37));
                OperationBinding exec = bindings.getOperationBinding("Commit");
                exec.execute();
                //pfm.put("letterMemoID", r.getAttribute("LetterMemoId"));
            }
            setInitialTrackingValues();
        }
        
    }
    
    public String getUploadDocSource(){
        String linkId = (String) AdfFacesContext.getCurrentInstance().getPageFlowScope().get("linkID");
            String[] token = linkId.split("-");
            if(token.length != 2 ) {
                logger.log(logger.ERROR, "Edit Letter Memo Bean : wrong LinkID format " );
               return "";
            }
            if("ACTION".equalsIgnoreCase(token[0])){
                return "1";
            }else{
                return "3";
        }
    }
    /**
     * Called during task flow initilization to keep track of certain attributes which are used in
     * post commit stage for email processing.
     */
    public void setInitialTrackingValues() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry(); 
        DCIteratorBinding iter = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
        Row r = iter.getCurrentRow();
        if (r != null) {
            pfm.put("oldApvSpl", r.getAttribute("ApvSpl"));
            pfm.put("oldApvDistributionDate", r.getAttribute("ApvDistributionDate"));
            
        }
    }
    public void approvalLettersPostCommit() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        SecurityContext scntx = ADFContext.getCurrent().getSecurityContext();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
        DCIteratorBinding awardIter = bindings.findIteratorBinding("AwardDetailsView2Iterator");
        Row awardRow = awardIter.getCurrentRow();
        Row r = iter.getCurrentRow();
        if (r != null && awardRow!=null) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            MessageBean mb = new MessageBean();
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            Date today = cal.getTime();
            Timestamp apvDistributionDate = (Timestamp) r.getAttribute("ApvDistributionDate");
            Timestamp oldApvDistributionDate = (Timestamp)pfm.get("oldApvDistributionDate");
            
            String apvSpl=(String)r.getAttribute("ApvSpl");
            String oldApvSpl=(String)pfm.get("oldApvSpl");
            setInitialTrackingValues();
            if (
                 (apvSpl != null && (oldApvSpl == null || !apvSpl.equals((oldApvSpl))))||
                 (apvSpl==null && oldApvSpl!=null)
               )
            {
                ArrayList<String> toAdd=new ArrayList<String>();
                ArrayList<String> ccAdd=new ArrayList<String>();
                String subject = "";
                if(awardRow.getAttribute("AwardNbr")!=null)
                    subject+=awardRow.getAttribute("AwardNbr")+" ";
                if(r.getAttribute("LetterName")!=null)
                    subject+=r.getAttribute("LetterName")+" ";
                String message = null;
                ccAdd.add(scntx.getUserName());
                if(apvSpl==null) {
                    subject+= "Approval Letter Specialist Unassigned";
                    message =oldApvSpl+" unassigned as Approval Letter Specialist";
                    toAdd.add(oldApvSpl);
                }
                else if(oldApvSpl==null) {
                    subject+= "Approval Letter Specialist Assigned";
                    message =apvSpl+" is assigned as Approval Letter Specialist";
                    toAdd.add(apvSpl);
                }
                else {
                    subject+= "Approval Letter Specialist Changed";
                    message ="Approval Letter Specialist is changed from "+oldApvSpl+" to "+apvSpl;
                    toAdd.add(apvSpl);
                    toAdd.add(oldApvSpl);
                }
                message+="<br><br>"+this.getApprovalLetterDetails();
                mb.sendApprovalLetterEmail(toAdd,ccAdd,subject,message, String.valueOf(r.getAttribute("LetterMemoId")));
            }
            if ( 
                (apvDistributionDate != null && (oldApvDistributionDate == null || apvDistributionDate.compareTo(oldApvDistributionDate)!=0)) ||
                (oldApvDistributionDate != null && apvDistributionDate==null)
               )
            {
                ArrayList<String> toAdd=new ArrayList<String>();
                ArrayList<String> ccAdd=new ArrayList<String>();
                String subject = "";
                String message = "";
                if(r.getAttribute("ApvSpl")!=null)
                    toAdd.add((String)r.getAttribute("ApvSpl"));
                if(r.getAttribute("ApvReviewSpl")!=null)
                    toAdd.add((String)r.getAttribute("ApvReviewSpl"));
                if(r.getAttribute("ApvClearanceSpl")!=null)
                    toAdd.add((String)r.getAttribute("ApvClearanceSpl"));
                // ABACUS-394 Removing approving officer from email list.
                 /*                  if(r.getAttribute("ApvAorConcur")!=null)
                    toAdd.add((String)r.getAttribute("ApvAorConcur")); */
               
                if(toAdd.size()==0)
                    toAdd.add(scntx.getUserName());
                else
                    ccAdd.add(scntx.getUserName());
                
                if(apvDistributionDate==null) {
                    subject+= "Approval Letter Distribution Date Removed";
                    message ="Approval Letter Distribution Date is removed";
                }
                else if(oldApvDistributionDate==null) {
                    subject+= "Approval Letter Distributed";
                    message ="Approval Letter is distributed on "+sdf.format(apvDistributionDate);
                }
                else {
                    subject+= "Approval Letter Distribution Date Changed";
                    message ="Approval Letter Distribution Date is changed from "+sdf.format(oldApvDistributionDate)+" to "+sdf.format(apvDistributionDate);
                }
                message+="<br><br>"+this.getApprovalLetterDetails();
                if(awardRow.getAttribute("AwardNbr")!=null)
                    subject+=" for "+awardRow.getAttribute("AwardNbr");
                if(r.getAttribute("LetterName")!=null)
                    subject+=" "+r.getAttribute("LetterName");
                if(r.getAttribute("ApvSpl")!=null)
                    mb.sendApprovalLetterEmail(toAdd,ccAdd,subject,message, String.valueOf(r.getAttribute("LetterMemoId")));
            }
        }
    }
 public String getApprovalLetterDetails() {
     Map appScope = ((AppBean) ADFContext.getCurrent().getApplicationScope().get("appBean")).getConstants();
     DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
     DCIteratorBinding iter = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
     Row r = iter.getCurrentRow();
     DCIteratorBinding awardIter = bindings.findIteratorBinding("AwardDetailsView2Iterator");
     Row awardRow = awardIter.getCurrentRow();
     StringBuilder builder=new StringBuilder();
     SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
     String letterURL ="";
     if(r!=null && awardRow!=null) {
         
             letterURL="<br><a href=\"" + appScope.get("URL") +
             FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() +
             "/faces/viewLetter?letterMemoID=" + r.getAttribute("LetterMemoId") + "\">Click here to view Approval Letter in Abacus.</a><br>";
             


            builder.append("<table><tr><td colspan=2 style=\"text-align:left\"><h3><b><u>Award Details</u></b></h3></td> </tr>");
         builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Awardee</b> </td> " +
                        "<td style=\"text-align:left\">"+(awardRow.getAttribute("AwardeeName")!=null?awardRow.getAttribute("AwardeeName"):"")+" </td></tr>");
         builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Award #</b> </td> " +
                        "<td style=\"text-align:left\">"+(awardRow.getAttribute("AwardNbr")!=null?awardRow.getAttribute("AwardNbr"):"")+" </td></tr>");
         //builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Project #</b> </td> " +
                        //"<td style=\"text-align:left\">"+(awardRow.getAttribute("ProjectNbr")!=null?awardRow.getAttribute("ProjectNbr"):"")+" </td></tr>");
         builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Project</b> </td> " +
                        "<td style=\"text-align:left\">"+(awardRow.getAttribute("ProjectName")!=null?awardRow.getAttribute("ProjectName"):"")+" </td></tr>");
        /* builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Award Start Date</b> </td> " +
                        "<td style=\"text-align:left\">"+(awardRow.getAttribute("AwardStartDate")!=null?sdf.format(awardRow.getAttribute("AwardStartDate")):"")+" </td></tr>");
         builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Award End Date</b> </td> " +
                        "<td style=\"text-align:left\">"+(awardRow.getAttribute("AwardEndDate")!=null?sdf.format(awardRow.getAttribute("AwardEndDate")):"")+" </td></tr>");
         builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Distributed Amt</b> </td> " +
                        "<td style=\"text-align:left\">"+(awardRow.getAttribute("DistributedAmt")!=null?currencyFormat((BigDecimal)awardRow.getAttribute("DistributedAmt")):"")+" </td></tr>");
         builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Committed Amt</b> </td> " +
                        "<td style=\"text-align:left\">"+(awardRow.getAttribute("CommittedAmt")!=null?currencyFormat((BigDecimal)awardRow.getAttribute("CommittedAmt")):"")+" </td></tr>");
         builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Obligated Amt</b> </td> " +
                        "<td style=\"text-align:left\">"+(awardRow.getAttribute("ObligatedAmt")!=null?currencyFormat((BigDecimal)awardRow.getAttribute("ObligatedAmt")):"")+" </td></tr>");*/
         builder.append("<tr><td colspan=2 style=\"text-align:left\"><h3><b><u></u></b></h3></td> </tr>");
         builder.append("<tr><td colspan=2 style=\"text-align:left\"><h3><b><u>Letter Info</u></b></h3></td> </tr>");
         builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Fiscal Year</b> </td> " +
                        "<td style=\"text-align:left\">"+(r.getAttribute("BudgetFy")!=null?r.getAttribute("BudgetFy"):"")+" </td></tr>");
         builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Name</b> </td> " +
                        "<td style=\"text-align:left\">"+(r.getAttribute("LetterName")!=null?r.getAttribute("LetterName"):"")+" </td></tr>");
         builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Description</b> </td> " +
                        "<td style=\"text-align:left\">"+(r.getAttribute("Description")!=null?r.getAttribute("Description"):"")+" </td></tr>");
         builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Specialist</b> </td> " +
                        "<td style=\"text-align:left\">"+(r.getAttribute("ApvSpl")!=null?r.getAttribute("ApvSpl"):"")+" </td></tr>");
         builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Updated By</b> </td> " +
                        "<td style=\"text-align:left\">"+(r.getAttribute("UpdId")!=null?r.getAttribute("UpdId"):"")+" </td></tr>");
         builder.append("<tr><td style=\"text-align:right;padding:5px\"><b>Updated Date</b> </td> " +
                        "<td style=\"text-align:left\">"+(r.getAttribute("UpdDate")!=null?new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a").format(r.getAttribute("UpdDate")):"")+" </td></tr></table>");

     }
     return letterURL+"<br><br>"+builder.toString();
 }
 
    public void AorCorValueChangeListner(ValueChangeEvent valueChangeEvent) {
         
    Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
    SecurityContext scntx = ADFContext.getCurrent().getSecurityContext();
    DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
    DCIteratorBinding iter = bindings.findIteratorBinding("LetterMemoEditView1Iterator");
    Row r = iter.getCurrentRow();
    if (r != null) { 
        String aor = valueChangeEvent.getNewValue().toString();
        String oldAor=(String)pfm.get("oldAor");
       
        if ((aor != null && (oldAor == null || !aor.equals((oldAor)))) || (aor==null && oldAor!=null))
        {
            r.setAttribute("AorConcurredId", (scntx.getUserName()).toUpperCase());
            r.setAttribute("AorConcurredDate",r.getAttribute("CurrentDate"));
        }
    }
 }
     
}
