package gov.ofda.abacus.view.bean.funds;

import gov.ofda.abacus.view.base.JSFUtils;

import java.io.Serializable;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class FundTransactions implements Serializable {
    
    public String initialSetUp() {
        // Add event code here...
        Map pageFlowMap = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String from = (String) pageFlowMap.get("level");
        SecurityContext ctx=ADFContext.getCurrent().getSecurityContext();
            if(ctx.isUserInRole("APP_BFL")||ctx.isUserInRole("APP_FM")){
                pageFlowMap.put("isEditable",true);
            }else{
                pageFlowMap.put("isEditable",false);
            }
        return from;
    }
    
    public void saveChanges() {
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = null;
        Boolean isCommitEnabled = (Boolean) JSFUtils.resolveExpression("#{bindings.FundsCommit.enabled}");
        if (!isCommitEnabled) {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "No changes","No changes to save");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;
        }
        exec = bindings.getOperationBinding("FundsCommit");
        exec.execute();
        if (exec.getErrors().isEmpty()) {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved successfully","Saved successfully");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    public void cancelChanges() {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding exec = bindings.getOperationBinding("FundsRollback");
        exec.execute();
        Map pfm = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        if (exec.getErrors().isEmpty())
        {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancelled successfully","Cancelled successfully");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            NavigationHandler  nvHndlr = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            if(nvHndlr!=null)
                nvHndlr.handleNavigation(FacesContext.getCurrentInstance(), null, "cancel");
        }
    }
}
