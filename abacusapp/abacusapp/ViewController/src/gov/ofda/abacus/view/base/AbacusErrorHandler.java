package gov.ofda.abacus.view.base;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.Calendar;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.ViewPortContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.share.logging.ADFLogger;

import oracle.binding.OperationBinding;

/**
 * AbacusErrorHandler.java
 * Purpose: To handle exceptions and show a message.
 * @version 1.0 09/09/2012
 */
public class AbacusErrorHandler {
    public AbacusErrorHandler() {
        super();
    }
    private static ADFLogger logger = ADFLogger.createADFLogger(gov.ofda.abacus.view.base.AbacusErrorHandler.class);

    /**
     * Displays an exception if present as a message.
     */
    public void controllerExceptionHandler() {
        
        ControllerContext c=ControllerContext.getInstance();
        ViewPortContext currentRootViewPort=c.getCurrentRootViewPort();
        Exception exceptionData= currentRootViewPort.getExceptionData();
        if(currentRootViewPort.isExceptionPresent()) {
            exceptionData.printStackTrace();
            currentRootViewPort.clearException();
            Calendar cal=Calendar.getInstance();
            cal.setTime(new Date());
            long ts=cal.getTimeInMillis();
            logger.log(logger.ERROR, ts+":"+exceptionData.toString());
            FacesContext fc=FacesContext.getCurrentInstance();
            fc.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Unexpected Error: "+ts,null));
        }
    }

    /**
     * Returns the stack trace of the current exception.
     * @return
     */
    public String getStacktrace() {
      ControllerContext c=ControllerContext.getInstance();
      ViewPortContext currentRootViewPort=c.getCurrentRootViewPort();
      if(currentRootViewPort.isExceptionPresent()) {
          StringWriter sw=new StringWriter();
          PrintWriter pw=new PrintWriter(sw);
          currentRootViewPort.getExceptionData().printStackTrace(pw);
          return sw.toString();
      }
    return null;
  }
    
    public String getError() {
      ControllerContext c=ControllerContext.getInstance();
      ViewPortContext currentRootViewPort=c.getCurrentRootViewPort();
      if(currentRootViewPort.isExceptionPresent()) {
          String msg[]=currentRootViewPort.getExceptionData().getMessage().split(":");
          if(msg.length>1)
              return msg[1];
      }
    return null;
    }
}
